package api;

import java.util.List;

import gui.PosConstraint;
import gui.PosConstraintContainer;
import module.Anim;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.Output;
import pr.Module;
import pr.Positionable;

public class ModuleAPI {
	private static ClassBuilder module, animModule, posConstraintModule, audioModule, audioStack, math;
	
	static public ObjectVariable instanciate(Module s, Parser parser) {
		ClassBuilder v = null;
		if(s instanceof Anim)
			v = animBuilder(parser);
		else if(s instanceof PosConstraintContainer)
			v = constraintBuilder(parser);
		else if(s instanceof Arithmetic)
			v = mathBuilder(parser);
		else
			v = moduleBuilder(parser);
		
		ObjectVariable ov = parser.getVariable(s);
		if(ov == null) {
			try {
				ov = v.initialize(s);
			} catch (RuntimeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			parser.addData(s, ov);
		}
		return ov;
	}
	
	public static ClassBuilder moduleBuilder(Parser p) {
		if(module == null) {
			module = new ClassBuilder("Node");
		}else
			return module;
		module.addConstructor(
				new AbstractFunctionDefinition.FunctionDefinition() {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) throws RuntimeException {
						self.addVariable("name", ((Module) self.getData()).getName());
						Module m = (Module) self.getData();
						List<Input> inSlots = m.getInputs();
						List<Output> outSlots = m.getOutputs();
						if(outSlots != null) {
							Array arr = Array.getArrayClassBuilder().initializeArray();
							for (Output os : outSlots) 
								arr.add(SlotAPI.instanciate(os));
							self.addVariable("outputs", arr);
						}
						if(inSlots != null) {
							Array arr = Array.getArrayClassBuilder().initializeArray();
							for (Input is : inSlots) 
								arr.add(SlotAPI.instanciate(is));
							self.addVariable("inputs", arr);
						}
						return null;
					}
				}
			);
		module.addFunction("getInput", 
				new AbstractFunctionDefinition.FunctionDefinition(1) {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
						Object o = args[0];
						Array is = (Array) self.getVariable("inputs");
						int index = -1;
						if(o instanceof Number) {
							index = ((Number)o).intValue();
						}else if(o instanceof String) {
							String s1 = (String) o;
							for (int i = 0; i < is.size(); i++) {
								String s2 = ((InOutInterface)((ObjectVariable)is.get(i)).getData()).getName();
								if(s1.equals(s2)) {
									index = i;
									break;
								}
							}
						}
						return is.get(index);
					}
				}
				);
		module.addFunction("getOutput", 
				new AbstractFunctionDefinition.FunctionDefinition(1) {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
						Object o = args[0];
						Array is = (Array) self.getVariable("outputs");
						int index = -1;
						if(o instanceof Number) {
							index = ((Number)o).intValue();
						}else if(o instanceof String) {
							String s1 = (String) o;
							for (int i = 0; i < is.size(); i++) {
								String s2 = ((InOutInterface)((ObjectVariable)is.get(i)).getData()).getName();
								if(s1.equals(s2)) {
									index = i;
									break;
								}
							}
						}
						return is.get(index);
					}
				}
				);
		module.addFunction("delete", 
				new AbstractFunctionDefinition.FunctionDefinition() {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
						Module m = (Module) self.getData();
						m.delete(ModuleDeleteMode.PATCH);
						return null;
					}
				}
				);
		
    	return module;
	}
	
	public static ClassBuilder animBuilder(Parser parser) {
		if(animModule == null) {
			animModule = new ClassBuilder("Anim");
			animModule.setSuperClass(moduleBuilder(parser));
		}else
			return animModule;
		
		//animModule.
		
		
		return animModule;
	}
	
	public static ClassBuilder constraintBuilder(final Parser parser) {
		if(posConstraintModule == null) {
			posConstraintModule = new ClassBuilder("MotionPath");
			posConstraintModule.setSuperClass(moduleBuilder(parser));
		
			posConstraintModule.addFunction("add", 
					new AbstractFunctionDefinition.FunctionDefinition(2) {
						@Override
						public Object invokeExtern(ObjectVariable self, Object[] args) {
							Positionable p = (Positionable)((ObjectVariable)args[0]).getData();
							double d = (Double) args[1];
							PosConstraint cons = ((PosConstraintContainer) self.getData()).getPosConstraint();
							cons.addFollower(p, d);
							return null;
						}
					}
					);
			posConstraintModule.addFunction("getNodes", 
					new AbstractFunctionDefinition.FunctionDefinition() {
						@Override
						public Object invokeExtern(ObjectVariable self, Object[] args) throws RuntimeException {
							PosConstraint cons = ((PosConstraintContainer) self.getData()).getPosConstraint();
							Array arr = Array.getArrayClassBuilder().initializeArray();
							for (Module m : cons.getContainedModules()) {
								ObjectVariable v = ModuleAPI.instanciate(m, parser);
								arr.add(v);
							}
							return arr;
						}
					}
					);
			posConstraintModule.addFunction("detach", 
					new AbstractFunctionDefinition.FunctionDefinition(1) {
						@Override
						public Object invokeExtern(ObjectVariable self, Object[] args) {
							PosConstraint cons = ((PosConstraintContainer) self.getData()).getPosConstraint();
							Object o = args[0];
							Positionable m = null;
							if(o instanceof Number) {
								m = (Positionable) cons.getContainedModules().get(((Number)o).intValue());
							}else if(o instanceof Positionable){
								m = (Positionable) o;
							}
							cons.removeFollower((Positionable) m);
							return null;
						}
					}
					);
		}
		
		return posConstraintModule;
	}
	
	public static ClassBuilder mathBuilder(Parser parser) {
		if(math == null) {
			math = new ClassBuilder("Math");
			math.setSuperClass(moduleBuilder(parser));
		}
		math.addFunction("setMode", 
				new AbstractFunctionDefinition.FunctionDefinition(1) {
					@Override
					public Object invokeExtern(ObjectVariable self, Object[] args) {
						Arithmetic m = (Arithmetic) self.getData();
						String str = (String) args[0];
						m.setMode(str.toUpperCase());
						return null;
					}
				}
				);
		math.addFunction("setValue", 
				new AbstractFunctionDefinition.FunctionDefinition(1) {
					@Override
					public Object invokeExtern(ObjectVariable self, Object[] args) {
						Arithmetic m = (Arithmetic) self.getData();
						String str = (String) args[0];
						
						return null;
					}
				}
				);
		return math;
	}
}
