package api;

import constants.Timer;
import module.loading.DynClassLoader;
import pr.Module;

public class API {
	
	static public void loadLib(final Parser parser) {
		parser.addVariable("loadNode", 
			new FunctionWrapper(null, 
				new AbstractFunctionDefinition.FunctionDefinition(1) {
				@Override
				public Object invokeExtern(ObjectVariable self, Object[] args) {
					String name = (String)args[0];
					String[][] names = DynClassLoader.classNames;
					int i = 0, j = 0;
					
					while (i < names.length) {
						if(j < names[i].length) {
							String s = names[i][j];
							s = s.substring(s.lastIndexOf('.') + 1, s.length());
							if(s.equals(name)) 
								break;
							j++;
						}else{
							i++;
							j = 0;
						}
					}
					Module m = DynClassLoader.getModule(names[i][j]);
					m.getGui().setPos(100, 100);
					return ModuleAPI.instanciate(m, parser);
				}
			}
		));
		
		parser.addVariable("addListener", 
				new FunctionWrapper(null, 
					new AbstractFunctionDefinition.FunctionDefinition(2) {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
						String s = (String) args[0];
						FunctionWrapper foo = (FunctionWrapper) args[1];
						try {
							parser.callFunction(foo);
						} catch (RuntimeException e) {
							e.printStackTrace();
						}
						return null;
					}
				}
			));
		
		
		
		ClassBuilder timer = new ClassBuilder("Timer"); 
		timer.addConstructor(
				new AbstractFunctionDefinition.FunctionDefinition(2) {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
						int millis = ((Number)args[0]).intValue();
						final FunctionWrapper w = (FunctionWrapper) args[1];
						self.setData(new Timer(millis, null) {
							@Override public void timerEvent() {
								try {
									parser.callFunction(w);
								} catch (RuntimeException e) {
									e.printStackTrace();
								}
							}
						}
						);
						return null;
					}
				});
		timer.addConstructor(
				new AbstractFunctionDefinition.FunctionDefinition(3) {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
						int millis = ((Number)args[0]).intValue();
						final FunctionWrapper w = (FunctionWrapper) args[1];
						int repeat = ((Number) args[2]).intValue();
						self.setData(new Timer(millis, repeat, null) {
							@Override public void timerEvent() {
								try {
									parser.callFunction(w);
								} catch (RuntimeException e) {
									e.printStackTrace();
								}
							}
						}
						);
						return null;
					}
				});
		timer.addFunction("start", 
				new AbstractFunctionDefinition.FunctionDefinition() {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
						Timer t = (Timer) self.getData();
						t.start();
						return null;
					}
		});
		timer.addFunction("pause", 
				new AbstractFunctionDefinition.FunctionDefinition() {
					@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
						Timer t = (Timer) self.getData();
						t.pause();
						return null;
					}
		});
		
		parser.addVariable("Timer", timer);
				
				
		
		
	}
	
	
	
}
