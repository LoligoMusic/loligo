package api;

import module.inout.ColorInputIF;
import module.inout.InOutInterface;
import module.inout.TriggerInputIF;
import pr.ModuleManager;

public class SlotAPI {
	
	static private ClassBuilder slot, inSlot, outSlot, trigger, color;
	
	static public ObjectVariable instanciate(InOutInterface s) {
		ClassBuilder v = null;
		if(s instanceof TriggerInputIF)
			v = trigger();
		else if(s instanceof ColorInputIF)
			v = color();
		else
			v = slot();
		
		try {
			return v.initialize(s);
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	static public ClassBuilder slot() {
		if(slot == null) {
			slot = new ClassBuilder("Slot");
			
			slot.addConstructor(
					new AbstractFunctionDefinition.FunctionDefinition() {
						@Override
						public Object invokeExtern(ObjectVariable self, Object[] args) {
							self.addVariable("name", ((InOutInterface) self.getData()).getName());
							return null;
						}
					}
				);
			slot.addFunction("getValue", 
					new AbstractFunctionDefinition.FunctionDefinition() {
						@Override
						public Object invokeExtern(ObjectVariable self, Object[] args) {
							InOutInterface slot = (InOutInterface) self.getData();
							return null; //no getValue for abstract slot
						}
					}
					);
			slot.addFunction("setValue",
					new AbstractFunctionDefinition.FunctionDefinition(1) {
						@Override
						public Object invokeExtern(ObjectVariable self, Object[] args) {
							Object o = self.getData();
							//InSlot slot = (InSlot) self.getData();
							//slot.setInput(((Number)args[0]).doubleValue());
							return null; //no getValue for abstract slot
						}
					});
			slot.addFunction("connect", 
					new AbstractFunctionDefinition.FunctionDefinition(1) {
						@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
							InOutInterface s1 = (InOutInterface) self.getData(),
										   s2 = (InOutInterface) ((ObjectVariable) args[0]).getData();
							ModuleManager.connect(s1, s2);
							return null;
						}
					});
		}
		return slot;
	}
	
	static public ClassBuilder trigger() {
		if(trigger == null) {
			trigger = new ClassBuilder("Trigger");
			trigger.setSuperClass(slot());
			
			trigger.addFunction("trigger", 
					new AbstractFunctionDefinition.FunctionDefinition() {
						@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
							//((InSlotTrigger)self.getData()).;
							return null;
						}
					});
		}
		return trigger;
	}
	
	static public ClassBuilder color() {
		if(color == null) {
			color = new ClassBuilder("ColorSlot");
			color.setSuperClass(slot());
			
			color.addFunction("setColor", 
					new AbstractFunctionDefinition.FunctionDefinition(4) {
						@Override public Object invokeExtern(ObjectVariable self, Object[] args) {
							//((InSlotColor)self.getData()).;
							return null;
						}
					});
		}
		return color;
	}
}
