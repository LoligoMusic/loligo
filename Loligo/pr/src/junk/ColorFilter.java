package module.modules.anim;

import gui.ImageFilter;
import module.Module;
import module.inout.NumberInputIF;
import pr.DisplayObject;
import processing.core.PGraphics;
import processing.core.PImage;


public class ColorFilter extends ImageFilter {
	
	private static final long serialVersionUID = 1L;
	
	final private NumberInputIF in_red, in_blue, in_green;
	private double redFac, greenFac, blueFac;
	
	
	public ColorFilter() {
		
		in_red = addInput("Red");
		in_green = addInput("Green");
		in_blue = addInput("Blue");
		
	}
	
	
	@Override
	public DisplayObject createFilter() {
		
		DisplayObject a = new DisplayObject() {
			
			int r, g, b, p;
			final int ox255 = 0xFF000000;
			transient PImage image;
			
			@Override public void render(PGraphics g) {
				
				if(getX() + getWidth() > 0 && getY() + getHeight() > 0 && getX() < PROC.width && getY() < PROC.height) {
					
					image = PROC.get((int)getX(), (int)getY(), getWidth(), getHeight());
					image.loadPixels();
					
					for (int i = 0; i < image.pixels.length; i++) {
						
						p = image.pixels[i];
						r = (int)(redFac * ((p >> 16) & 0xFF));
						g = (int)(greenFac * ((p >> 8) & 0xFF));
						b = (int)(blueFac * (p & 0xFF));
						image.pixels[i] = ox255 | r << 16 | g << 8 | b;
					}
					image.updatePixels();
					PROC.image(image, getX(), getY());
				}
			}
		};
		
		return a;
	}
	
	
	@Override
	public void processIO() {
		
		if(in_red.changed())
			redFac = in_red.getInput();
		if(in_green.changed())
			greenFac = in_green.getInput();
		if(in_blue.changed())
			blueFac = in_blue.getInput();
		
	}
	

	@Override
	public Module createInstance() {
		
		return new ColorFilter();
	}

	
}
