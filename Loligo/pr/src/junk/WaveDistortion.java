package module.modules.anim;

import gui.ImageFilter;
import module.Module;
import module.inout.NumberInputIF;
import patch.Loligo;
import pr.DisplayObject;
import processing.core.PConstants;


public class WaveDistortion extends ImageFilter {
	
	private static final long serialVersionUID = 1L;
	
	final private NumberInputIF in_amp, in_phase, in_freq;
	private float amp, phase, freq;
	
	
	public WaveDistortion() {
		
		in_amp = addInput("Amp");
		in_phase = addInput("Phase");
		in_freq = addInput("Freq");
	}
	
	
	public DisplayObject createFilter() {
		
		ImageFunction foo = new ImageFunction(true) {
			
			@Override
			protected void processImg() {
				
				for (int i = 0; i < image.pixels.length; i++) {
					
					float w2 = w + ((Loligo.sin(phase + h / freq)) * amp);
					
					//System.out.println(w2 + "     " + (w2 - w));
					
					w2 = w2 >= 0 && w2 < image.width ? w2 : w;
					
					int i2 = (int)w2 + h * image.width;
					i2 = i2 >= 0 && i2 < image.pixels.length ? i2 : i;
					
					image.pixels[i] = w2 < image.width - 1 ? PROC.lerpColor(pixels[i2], pixels[i2 + 1], w2 - (int)w2) : 
						   									 pixels[i2];
					
					countCoo();
				}
			}
			
		};
		
		return foo;
		
	}
	
	
	@Override
	public void processIO() {
		
		if(in_amp.changed())
			amp = (float)in_amp.getInput() * 10f;
		if(in_phase.changed())
			phase = (float)in_phase.getInput() * PConstants.TWO_PI;
		if(in_freq.changed())
			freq = (float)in_freq.getInput()  * PConstants.TWO_PI;
			
	}
	

	@Override
	public Module createInstance() {
		
		return new WaveDistortion();
	}

	
}
