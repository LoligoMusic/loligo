package junk;

import java.util.ArrayList;

import gui.GUIBox;
import module.AbstractModule;
import module.Module;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import pr.DisplayObject;
import processing.core.PGraphics;

public class Loop extends AbstractModule {
	private static final long serialVersionUID = 3286233410211258003L;
	private NumberInputIF in, in_speed;
	private NumberOutputIF out;
	transient private DisplayObject recordButton, playButton;
	private boolean recording = false, playing = false, flat = false;
	private ArrayList<Double> track = new ArrayList<Double>();
	private double playPos = 0, speed = 1;
	//private LinkedList<TrackCue> cues = new LinkedList<TrackCue>();
	//private TrackCue lastCue;
	//private int cueIndex = 0;
	
	public Loop() {
		
		in = addInput("in");
		in_speed = addInput("speed");
		in_speed.setValue(.25);
		out = addOutput("out");
	}
	
	@Override
	public DisplayObject createGUI() {
		
		box = (GUIBox) super.createGUI();
		
		recordButton = new DisplayObject() {
			
			private int cc = 0, on = PROC.rgb(200, 100, 100), off = PROC.rgb(160, 80, 80);
			private boolean blink = false;
			
			@Override public void  render(PGraphics g) {
				cc++;
				if(cc > 15) {
					blink = !blink;
					cc = 0;
				}
				if(recording) {
					setColor(blink ? on : off);
				}
				super.render(g);
			}
			@Override public void mouseClicked() {
				recording = !recording;
				if(recording) {
					setColor(PROC.rgb(200, 100, 100));
					startRecord();
				}else{
					setColor(PROC.rgb(100, 100, 200));
					endRecord();
				}
			}
		};
		playButton = new DisplayObject() {
			private static final long serialVersionUID = -6532237317935629559L;

			@Override public void mouseClicked() {
				if(track.size() > 0) {
					playing = !playing;
					if(playing) {
						setColor(PROC.rgb(200, 100, 200));
						play();
					}else{
						setColor(PROC.rgb(100, 100, 200));
						endRecord();
						stop();
					}
				}
			}
		};
		box.addRow(recordButton, playButton);
		return box;
	}
	
	public void startRecord() {
		track.clear();
		playPos = 0;
	}
	
	public void endRecord() {
		
	}
	
	public void play() {
		
	}
	
	public void stop() {
		
	}
	
	@Override
	public void processIO() {
		
		if(in_speed.changed())
			speed = in_speed.getInput() * 4;
		if(recording) {
			/*
			double s = in.getInput();
			
			if(flat) {
				if(s == lastCue.value)
					lastCue.length++;
				else{
					flat = false;
					lastCue.end = track.size() - 1;
				}
			}else if(track.size() > 0 && s == track.get(track.size() - 1)) {
				flat = true;
				cues.add(new TrackCue(track.size() - 1, s));
			}
			*/
			track.add(in.getInput());
		}else if(playing && speed != 0) {
			
			playPos += speed;
			if(playPos >= track.size()) 
				playPos = 0;
			out.setValue(getValueAt(playPos));
		}
	}
	
	private double getValueAt(double pos) {
		double v1, inter;
		int index = (int) pos;
		v1 = track.get(index);
		inter = pos - index;
		if(inter == 0)
			return v1;
		else {
			double v2 = track.get(index + 1 < track.size() ? index + 1 : 0);
			return v1 + (v2 - v1) * inter; 
		}
	}

	@Override
	public Module createInstance() {
		return new Loop();
	}
	
	/*
	class TrackCue {
		int start, end, length = 0;
		double value;
		public TrackCue(int start, double value) {
			this.start = start;
			lastCue = this;
			this.value = value;
		}
	}
	*/
}
