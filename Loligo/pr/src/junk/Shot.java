package junk;

import constants.DisplayFlag;
import gui.GUIBox;
import lombok.Getter;
import lombok.Setter;
import module.AbstractModule;
import module.Module;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.TriggerInputIF;
import module.loading.InternalNode;
import module.loading.NodeType;
import pr.DisplayObject;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;
import save.RestoreField;
import save.SaveDataContext;

@InternalNode(type = NodeType.TOOLS)

public class Shot extends AbstractModule {
	
	private static final long serialVersionUID = 1L;
	private NumberOutputIF out;
	private NumberInputIF in_time;
	private TriggerInputIF in_trigger;
	
	private int duration = 30, stepNew = 0, maxDuration = 150;
	
	@Getter@Setter@RestoreField
	private double position = 0;
	
	private double step = 0;
	private boolean shooting = false;
	transient private DisplayObject display;
	
	
	public Shot() {
		in_trigger = addTrigger();
		in_time = addInput("Duration");
		out = addOutput("Out");
	}
	
	@Override
	public DisplayObject createGUI() {
		GUIBox gb = (GUIBox) super.createGUI();
		
		display = new DisplayObject() {
			
			int red = 0xffcc1111, white = 0xffcccccc;
			float mx;
			@Override public void render(PGraphics g) {
				getDm().g.stroke(white);
				getDm().g.strokeWeight(2);
				getDm().g.line(getX(), getY(), getX() + getWidth(), getY());
				getDm().g.stroke(red);
				getDm().g.strokeWeight(1);
				mx = (float)(getX() + getWidth() * position);
				getDm().g.line(mx, getY() + 3, mx, getY() - 3);
			}
		};
		display.setFlag(DisplayFlag.RECT, true); 
		display.setWH(40, 6);
		
		gb.addRow(display);
		
		return gb;
	}
	
	@Override
	public void processIO() {
		if(in_time.changed()) {
			duration = (int)(in_time.getInput() * maxDuration);
			if(duration < 1)
				duration = 1;
			stepNew = (int)(position * duration) + 1;
		}
		
		if(in_trigger.triggered()) {
			shooting = true;
			stepNew = 0;
		}
		
		if(shooting) {
			stepNew++;
			position = (double)stepNew / duration;
			if(stepNew >= duration) {
				stepNew = 0;
				position = 0;
				shooting = false;
			}
			out.setValue(position);
		}
	}
	
	@Override
	public Module createInstance() {
		return new Shot();
	}
	
	

	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {

		ModuleData md = super.doSerialize(sdc);
		SaveDataContext.saveByAnnotations(md, Shot.class);

		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {

		super.doDeserialize(data, rdc);
		SaveDataContext.restoreByAnnotation(data, Shot.class);
	}
}
