package junk;

import audio.AudioModule;
import module.modules.reader.Reader;
import module.sloatBack.slot.AbstractSlot;
import module.slot.OutSlot;
import pr.RootClass;
import processing.core.PImage;

public class NeuroReader extends Reader {
	Network network;
	final public float[] buffer;
	
	public NeuroReader() {
		outSlots = new OutSlot[1];
		outSlots[0] = new OutSlot(this, "buffer");
		
		gui.width = 10;
		gui.height = 10;
		
		network = new Network();
		network.addInputLayer(32 - 1);
		network.addInputLayer(12);
		network.addInputLayer(gui.width * gui.height);
		network.connect();
		buffer = new float[RootClass.audioManager().getAC().getBufferSize()];
		for (int i = 0; i < buffer.length; i++) 
			buffer[i] = 0;
		
	}
	
	@Override
	public void processIO(){
		PImage img = PROC.get((int)gui.x, (int)gui.y, gui.width, gui.height);
		img.loadPixels();
		for (int i = 0; i < img.pixels.length; i++) 
			network.inputLayer.neurons[i].input = PROC.brightness(img.pixels[i]);
		network.step();
		int n = network.outputLayer.neurons.length - 2, interStep = buffer.length / n, j = 0, neuroCount = 1;
		float inter = network.outputLayer.neurons[0].output / interStep, bufferValue = 0;
		for (int i = 0; i < buffer.length; i++) {
			if(j == interStep - 1){
				j = 0;
				neuroCount++;
				inter = -(bufferValue - network.outputLayer.neurons[neuroCount].output) / interStep;
			}else{
				j++;
			}
			bufferValue += inter;
			buffer[i] = network.outputLayer.neurons[neuroCount - 1].output + inter * j;
			//System.out.println(buffer[i]);
		}
		
		
		/*
		for (int i = 0; i < n - 1; i++) {
			inter = (network.outputLayer.neurons[i].output - network.outputLayer.neurons[i + 1].output) / (buffer.length / n);
			for (int j = i * interStep; j < i * interStep + n; j++) 
				buffer[j] = inter * j;
		}
		*/
		//PROC.println(buffer);
	}
	
	@Override
	public void onSlotConnect(AbstractSlot s1, AbstractSlot s2){
		System.out.println(s1.type + " " + s2.type);
		if(s1.type == "buffer" && s2.type == "buffer")
			((AudioModule)s2.master).buffer = buffer;
			
	}

}
