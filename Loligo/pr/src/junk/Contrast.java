package module.modules.anim;

import gui.ImageFilter;
import module.Module;
import module.inout.NumberInputIF;
import pr.DisplayObject;


public class Contrast extends ImageFilter {
	
	private static final long serialVersionUID = 1L;
	
	final private NumberInputIF in_fac;
	private float fac;
	private final int maxFrames = 30;
	
	public Contrast() {
		
		in_fac = addInput("Fac");
		
	}
	
	
	@Override
	public DisplayObject createFilter() {
		
		DisplayObject a = new ImageFunction() {
			
			int cntr = 255 / 2;
			final int ox255 = 0xFF000000;
			
			@Override public void processImg() {
				
				for (int i = 0; i < image.pixels.length; i++) {
						
					int p = image.pixels[i];
					
					int   r =  (p >> 16) & 0xFF,
						  g =  (p >> 8)  & 0xFF,
						  b =   p 	     & 0xFF;
					
					r = (int)((r - cntr) * fac) + cntr;
					g = (int)((g - cntr) * fac) + cntr;
					b = (int)((b - cntr) * fac) + cntr;
					
					r = r > 255 ? 255 : r;
					g = g > 255 ? 255 : g;
					b = b > 255 ? 255 : b;
					
					image.pixels[i] = ox255 | r << 16 | g << 8 | b;
				}
			}
		};
		
		return a;
	}
	
	
	@Override
	public void processIO() {
		
		if(in_fac.changed())
			fac = (float)(in_fac.getInput() * 10);
		
	}
	

	@Override
	public Module createInstance() {
		
		return new Contrast();
	}

	
}
