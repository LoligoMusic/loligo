package module.modules.anim;

import gui.ImageFilter;
import module.Module;
import module.inout.NumberInputIF;
import pr.DisplayObject;


public class Invert extends ImageFilter {
	
	private static final long serialVersionUID = 1L;
	
	final private NumberInputIF in_fac;
	private float fac;
	
	
	public Invert() {
		
		in_fac = addInput("Fac");
		
	}
	
	
	@Override
	public DisplayObject createFilter() {
		
		DisplayObject a = new ImageFunction() {
			
			int r, g, b, p;
			final int ox255 = 0xFF000000;
			
			@Override public void processImg() {
				
				for (int i = 0; i < image.pixels.length; i++) {
						
					p = image.pixels[i];
					r = 255 - (int)(((p >> 16) & 0xFF));
					g = 255 - (int)(((p >> 8)  & 0xFF));
					b = 255 - (int) ((p 	   & 0xFF));
					
					int p2 = ox255 | r << 16 | g << 8 | b;
					
					image.pixels[i] = PROC.lerpColor(p, p2, fac);
				}
					
			}
		};
		
		return a;
	}
	
	
	@Override
	public void processIO() {
		
		if(in_fac.changed())
			fac = (float)in_fac.getInput();
		
	}
	

	@Override
	public Module createInstance() {
		
		return new Invert();
	}

	
}
