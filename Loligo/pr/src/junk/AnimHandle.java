package gui;

import pr.DisplayObject;

public class AnimHandle extends DisplayObject {
	private static final long serialVersionUID = -9062036943037607649L;
	final public DisplayObject target;
	
	public AnimHandle(final DisplayObject _target) {
		target = _target;
		size = 8;
		color = dm.g.rgb(220, 200, 20);
		snapsToPath = true;
	}
	
	@Override
	public void mouseDragged() {
		target.x = x;
		target.y = y;
	}
	
	
}
