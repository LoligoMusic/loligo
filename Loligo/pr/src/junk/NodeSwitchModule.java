package module.modules.graphs;

import constants.DisplayFlag;
import constants.Flow;
import constraints.PathTreeConstraint;
import constraints.PathTreeConstraint.NodeSwitch;
import gui.Position;
import gui.paths.Node;
import gui.paths.NullPathObject;
import gui.paths.Path;
import module.AbstractPositionableModule;
import module.Module;
import module.inout.NumberInputIF;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import processing.core.PGraphics;

public class NodeSwitchModule extends AbstractPositionableModule {

	private final NodeSwitch nodeSwitch;
	private final Node node;
	private final PathTreeConstraint constraint;
	private final NumberInputIF in;
	
	
	public NodeSwitchModule(PathTreeConstraint constraint, NodeSwitch nodeSwitch) {
		
		this.constraint = constraint;
		this.nodeSwitch = nodeSwitch;
		this.node = nodeSwitch.getNode();
		
		setPosObject(new Position.PositionTarget(node));
		
		in = addInput("Select");
		flowFlags.add(Flow.SINK);
	}
	
	@Override
	public ModuleGUI createGUI() {
		
		ModuleGUI d = super.createGUI();
		
		d.setFlag(DisplayFlag.clickable, false);
		
		DisplayObjectIF p = new DisplayObject() {
			
			NullPathObject np = new NullPathObject();
			
			@Override
			public void render(PGraphics g) {
				Node n = nodeSwitch.getNode();
				Path p = nodeSwitch.getPath();
				
				np.setPath(p);
				p.goToPos(np, n == p.getNodeIn() ? 0. : 1.);
				//getDm().g.noFill();
				final PGraphics g = getDm().g;
				g.fill(getColor());
				//getDm().g.strokeWeight(3);
				
				double w = np.getTangent() - 1.57 + (n == p.getNodeIn() ? 0 : Math.PI);
				
				g.pushMatrix();
				
				g.translate(getX(), getY());
				g.rotate((float) w);
				
				g.triangle(-3.5f, 8, 3.5f, 8, 0, 17);
				
				g.popMatrix();
				
				//getDm().g.line(getX(), getY(), getX() + (float)Math.sin(w) * 10, getY() + (float)Math.cos(w) * 10);
				
				//getDm().g.rotate((float) -w);
				//getDm().g.translate(-getX(), -getY());
				
				
				//getDm().g.noStroke();
			}
		};
		p.fixedToParent(true);
		p.setFlag(DisplayFlag.clickable, false);
		p.setColor(0xffff00ff);
		d.addChild(p);
		
		return d;
	}
	
	@Override
	public void processIO() {
		
		if(in.changed()) {
			
			int i = (int)((node.numLinks() - 1) * in.getInput());
			
			setExitPath(i);
		}
	}


	public void setExitPath(int i) {
		
		if(node.numLinks() > i && i >= 0) {
			
			node.getLink(i);
			nodeSwitch.setPath(i);
		}
	}
	

	@Override
	public Module createInstance() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
