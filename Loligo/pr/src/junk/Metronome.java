package junk;

import module.AnimObject;
import module.sloatBack.slot.InSlot;
import pr.DisplayObject;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PImage;

public class Metronome extends AnimObject {
	private float rotation = 0, phase, speed = 1;
	final InSlot in_angle;
	PGraphics buf = PROC.createGraphics(100, 50, PConstants.P2D);
	PImage img;
	
	public Metronome() {
		flowType = FLOW_OUTPUT;
		isOutput = true;
		
		inSlots = new InSlot[1];
		inSlots[0] = in_angle = new InSlot(this, "angle", 6.28);
		
		buf.beginDraw();
		buf.fill(200);
		buf.ellipse(10, 30, 20, 10);
		buf.rect(60, 10, 40, 40);
		buf.endDraw();
		img = buf.get(0, 0, buf.width, buf.height);
		
		anim = new DisplayObject() {
			@Override
			public void render(PGraphics g) {
				if(in_angle.partner == null) {
					phase  += PConstants.PI * .01 * speed;
					rotation = 2 + PApplet.sin(phase) * 2;
				}
				dm.g.fill(color);
				dm.g.translate(x , y);
				dm.g.rotate(rotation);
				dm.g.rect(0, 0, 40, 120);
				dm.g.rotate(-rotation);
				dm.g.translate(-x , -y);
			}
		};
		anim.color = PROC.rgb(PROC.random(255), PROC.random(255), PROC.random(255));
	}
	
	@Override
	public void processIO() {
		if(in_angle.changed)
			rotation = (float) in_angle.getInput();
	}
	
		
}
