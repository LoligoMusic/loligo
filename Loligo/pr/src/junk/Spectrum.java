package junk;

import audio.AudioModule;
import module.AnimObject;
import module.sloatBack.slot.AbstractSlot;
import module.sloatBack.slot.InSlot;
import module.slot.OutSlot;
import pr.DisplayObject;
import processing.core.PGraphics;

public class Spectrum extends AnimObject {
	float[] buffer;
	float step;
	
	public Spectrum() {
		flowType = FLOW_OUTPUT;
		isOutput = true;
		
		inSlots = new InSlot[1];
		inSlots[0] = new InSlot(this, "buffer");
		inSlots[0].format = AbstractSlot.ARRAY;
		outSlots = new OutSlot[1];
		outSlots[0] = new OutSlot(this, "buffer");
	
		anim = new DisplayObject() {
			@Override
			public void render(PGraphics g){
				PROC.fill(200);
				if(buffer != null)
					for (int i = 0; i < buffer.length; i++) 
						PROC.ellipse(x + step * i, y, 1, buffer[i] * 20);
			}
		};
		anim.width = 300;
		anim.height = 80;
	}
	
	@Override
	public void processIO() {
		
	}
	
	
	
	@Override
	public void onSlotConnect(AbstractSlot s1, AbstractSlot s2){
		if(s2.format == AbstractSlot.ARRAY){
			buffer = ((AudioModule)s2.master).beadOut.getOutBuffer(0);
			step = buffer.length / gui.width;
		}
	}
	
}
