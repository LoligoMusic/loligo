package module.modules.math;

import module.AbstractModule;
import module.Module;
import spreads2.NumSpreadOutput;
import spreads2.NumSpreadOutputIF;

//@InternalNode(type = NodeType.MATH)

public class LinearSpread extends AbstractModule {

	private static final long serialVersionUID = 1L;
	
	private NumSpreadOutputIF out;
	
	public LinearSpread() {
		
		out = new NumSpreadOutput("Spread", this);
		
		out.setSpread(new double[]{0, 0.1, 0.2, 0.4, 0.7, 0.9});
		
		addSlot(out);
	}

	@Override
	public void processIO() {
		
		
		
	}

	@Override
	public Module createInstance() {
		
		return new LinearSpread();
	}

}
