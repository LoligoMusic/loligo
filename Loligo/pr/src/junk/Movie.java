package module.modules.anim;

import java.io.File;
import gui.FileInputField;
import gui.NodeInspector;
import lombok.var;
import module.AnimObject;
import module.inout.NumberAttributes;
import module.inout.NumberInputIF;
import module.inout.NumericType;
import module.loading.InternalNode;
import module.loading.NodeType;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;


//@InternalNode(type = NodeType.ANIM)
public class Movie extends AnimObject {

	private processing.video.Movie movie;
	
	private final NumberInputIF in_play;
	
	
	public Movie() {

		in_play = addInput("Play", NumberAttributes.BOOL);
	}
	
	
	public void loadMovie(File path) {
		loadMovie(path.getAbsolutePath());
	}
	
	
	public void loadMovie(String path) {
		movie = new processing.video.Movie(getDomain().getPApplet(), path);
		movie.loop();
	}

	
	public File getFile() {
		return new File(movie != null ? movie.filename : "");
	}
	
	
	@Override
	public DisplayObjectIF createGUISpecial() {

		NodeInspector ni = NodeInspector.newInstance(this, n -> {
			var f = new FileInputField(this::loadMovie, this::getURL);
			n.addInspectorElement(f);
		});
		return ni;
	}
	
	
	@Override
	public DisplayObjectIF createAnim() {
		
		var a = new DisplayObject() {
			
			@Override
			public void render(PGraphics g) {
				
				if(movie != null) {
					if(movie.available())
						movie.read();
					g.image(movie, getX(), getY());
				}
			}
		};
		
		return a;
	}

	
	@Override
	public void processIO() {
		
		if(movie != null && in_play.changed()) {
			if(NumericType.toBool(in_play.getInput())) {
				movie.loop();
			}else {
				movie.noLoop();
			}
		}
	}

	
	@Override
	public int pipette(int x, int y) {
		return 0;
	}
}
