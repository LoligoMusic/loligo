package junk;

import gui.GuiReceiver;
import gui.GuiSender;
import gui.IntegerPicker;
import module.AbstractModule;
import module.Module;
import module.sloatBack.slot.InSlot;
import module.slot.OutSlot;
import pr.DisplayObject;
import processing.core.PGraphics;

public class Step extends AbstractModule implements GuiReceiver{

	private final InSlot in;
	private final OutSlot out;
	private IntegerPicker picker;
	private int steps = 2;
	private double input = 0, output;
	
	public Step() {
		in = addInput("In");
		out = addOutput("Out");
	}
	
	@Override
	public DisplayObject createGUI() {
		DisplayObject d = super.createGUI();
		picker = new IntegerPicker(this, "Steps");
		picker.setFloor(2);
		box.addRow(picker);
		
		DisplayObject s = new DisplayObject() {
			@Override public void render(PGraphics g) {
				float xp = getX() + getWidth() * (float)input;
				getDm().g.fill(50);
				getDm().g.rect(getX(), getY(), getWidth(), getHeight());
				getDm().g.fill(100);
				getDm().g.rect(getX(), getY(), getWidth() * (float)output, getHeight());
				getDm().g.stroke(200, 10, 10);
				getDm().g.line(xp, getY(), xp, getY() + getHeight());
				getDm().g.noStroke();
			}
		};
		s.setWH(60, 10);
		
		box.addRow(s);
		return d;
	}
	
	public void setNumSteps(int steps) {
		this.steps = steps > 2 ? steps : 2;
	}
 	
	@Override
	public void processIO() {
		if(in.changed) {
			input = in.getInput();
			output = Math.floor(input * steps) / steps;
			out.setValue(output);
		}
	}

	@Override
	public void guiMessage(GuiSender<?> s) {
		if(s == picker)
			setNumSteps(picker.getValue());
			
	}

	@Override
	public void updateSender(GuiSender<?> s) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Module createInstance() {
			return new Step();
		}
}
