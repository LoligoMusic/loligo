package module.modules.anim;

import constraints.Constrainable;
import constraints.FollowerObject;
import constraints.PathTreeConstraint;
import gui.DisplayObjects;
import gui.Position;
import gui.paths.Node;
import gui.paths.NullPathObject;
import gui.paths.Path;
import gui.paths.PathEvent;
import gui.paths.PathListener;
import gui.paths.PathObject;
import gui.paths.SegmentType;
import gui.paths.PathMementoFactory.ConstraintMemento;
import module.Module;
import module.inout.TriggerInputIF;
import module.loading.InternalNode;
import module.loading.NodeType;
import gui.paths.PathEventEnum;
import gui.paths.PathEventManager;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import save.ConstrainableData;
import save.ModuleData;
import save.PathData;
import save.RestoreDataContext;
import save.SaveDataContext;

@InternalNode(type=NodeType.PATH)

public class GraphPath extends AbstractPathModule implements PathListener{
	
	private final PathTreeConstraint constraint;
	//private final List<InputSwitch> switches = new ArrayList<>();
	private final TriggerInputIF trigger;
	
	
	public GraphPath() {
		
		PathEventManager.instance().addListener(this);
		
		super.constraint = constraint = new PathTreeConstraint(this);
		
		trigger = addTrigger();
		
		constraint.getPathTree().setPathListener(PathEventManager.instance());
	}

	@Override
	public void defaultSettings() {
	
		super.defaultSettings();
		
		makeTree();
	}
	
	private void makeTree() {
		
		Node n1 = constraint.newNode();
		Node n2 = constraint.newNode();
		Node n3 = constraint.newNode();
		Node n4 = constraint.newNode();
		
		n1.setPos(10, 10);
		n2.setPos(100, 100);
		n3.setPos(200, 150);
		n4.setPos(10, 150);
		
		gui.paths.Path p = 
		constraint.addPath(n1, n2, SegmentType.BEZIER);
		constraint.entryPoint(p, 0);
		
		constraint.addPath(n2, n3, SegmentType.BEZIER);
		constraint.addPath(n2, n4, SegmentType.BEZIER);
		constraint.addPath(n4, n1, SegmentType.BEZIER);
		constraint.addPath(n3, n1, SegmentType.BEZIER);
	}
	
	/*
	public void addSwitch(NodeSwitch sw) {
		
		NumberInputIF in = addInput("Switch" + (switches.size() + 1));
		switches.add(new InputSwitch(sw, in));
	}
	*/
	
	@Override
	public void processIO() {
		
		if(trigger.triggered()) {
			
			PathParticle p = new PathParticle();
			constraint.gui.addChild(p);
			constraint.addFollower(p);
		}
		/*
		for (InputSwitch sw : switches) {
			
			if(sw.input.changed()) {
				
				int i = (int)(sw.input.getInput() * (sw.nodeSwitch.numPaths() - 1));
				sw.nodeSwitch.setPath(i);
			}
		}
		*/
	}

	
	@Override
	public Module createInstance() {
		
		return new GraphPath();
	}

	
	@Override
	public void onPathEvent(PathEvent e) {
		
		if(e.type == PathEventEnum.PATH_CREATE) {
			
			PathEvent.PathAddRemove pe = (PathEvent.PathAddRemove) e;
			
			for (Node n : new Node[]{pe.path.getNodeIn(), pe.path.getNodeOut()}) {
				
				//if(n.numLinks() == 2)
				//	addSwitch(constraint.findSwitch(n));
			}
			
		}else
		if(e.type == PathEventEnum.PATH_DELETE) {
			
			
		}
	}
	
	/*
	private class InputSwitch {
		
		NodeSwitch nodeSwitch;
		NumberInputIF input;
		
		public InputSwitch(NodeSwitch nodeSwitch, NumberInputIF input) {
			this.nodeSwitch = nodeSwitch;
			this.input = input;
			nodeSwitch.setPath(0);
		}
	}
	*/

	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc);
		
		md.createMap().put("constraint", constraint.toMemento());
		
		return md;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
	
		super.doDeserialize(data, rdc);
		ConstraintMemento cm = (ConstraintMemento) data.getMapValue("constraint");
		constraint.restoreMemento(cm);
	}
	
	public static class PathParticle extends DisplayObject implements Constrainable {

		private FollowerObject fo;
		
		
		public PathParticle() {
			setColor(0xffffffff);
		}
		

		@Override
		public FollowerObject getFollowerObject() {
			
			return fo;
		}

		@Override
		public void setFollowerObject(FollowerObject fo) {
			
			this.fo = fo;
		}

		@Override
		public void updatePosition() {
			
		}

		@Override
		public void onRemoveFromConstraint() {
			
			DisplayObjects.removeFromDisplay(this);
		}

		@Override
		public DisplayObjectIF createGUI() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public DisplayObjectIF getGui() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void setGui(DisplayObjectIF gui) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public ConstrainableData toConstrainableMemento(SaveDataContext sdc, PathData pd) {
			// TODO Auto-generated method stub
			return null;
		}


		@Override
		public void restoreMemento(ConstrainableData cd) {
			// TODO Auto-generated method stub
			
		}
	}
	
}
