package module.modules.anim;

import gui.Button;
import gui.GuiReceiver;
import gui.GuiSender;
import javafx.scene.control.Cell;
import module.AbstractModule;
import module.AnimObject;
import module.Module;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;

public class Conway extends AnimObject implements GuiReceiver{
	private static final long serialVersionUID = 1L;
	
	private final GameOfLife game;
	private int cellSize = 5, drawIntervall = 5;
	private boolean play;
	private Button playButton;
	
	public Conway() {
		
		game = new GameOfLife(50, 40);
		
		
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		final DisplayObject sq = new DisplayObject() {
			private static final long serialVersionUID = 1L;
			
			@Override public void render(PGraphics g) {
				getDm().g.noFill();
				getDm().g.stroke(getColor());
				getDm().g.strokeWeight(1);
				getDm().g.rect(getX(), getY(), cellSize, cellSize);
			}
		};
		sq.setColor(PROC.rgba(200, 200, 20, 100));
		sq.clickable = false;
		sq.fixedToParent = true;
		
		playButton = new Button(this, "Play/Pause", Button.CLICK);
		playButton.fixedToParent = true;
		playButton.setPos(-10, 15);
		playButton.setWidth(playButton.setHeight(10));
		getGui().addChild(playButton);
		
		
		setAnim(new DisplayObject() {
			private static final long serialVersionUID = 1L;
			int ix, iy, cc = 0;
			DisplayObject selectSquare = sq;
			
			@Override public void render(PGraphics g) {
				getDm().g.stroke(getColor());
				getDm().g.noFill();
				getDm().g.rect(getX(), getY(), getWidth(), getHeight());
				
				if(play) {
					cc++;
					if(cc >= drawIntervall) {
						cc = 0;
						game.step();
					}
				}
						
				int len = game.getWidth() * game.getHeight();
				int px = 0, py = 0;
				Cell c;
				for (int i = 0; i < len; i++) {
					c = game.cells[px][py];

					if(c.alive) {
						getDm().g.noStroke();
						getDm().g.fill(getColor());
						getDm().g.rect(getX() + px * cellSize , getY() + py * cellSize, cellSize, cellSize);
					}

					px++;
					if(px >= game.getWidth()) {
						px = 0;
						py++;
					}
				}
				
			}
		
			@Override public void mouseOver() {
				getCellIndex(getDm().getInput().getMouseX(), getDm().getInput().getMouseY());
				addChild(selectSquare);
				selectSquare.setPos(ix * cellSize, iy * cellSize); 
				
			}
			
			@Override public void overStop() {
				selectSquare.removeFromDisplay();
			}
			
			@Override public void mouseClicked() {
				getCellIndex(getDm().getInput().getMouseX(), getDm().getInput().getMouseY());
				game.toggle(ix, iy);
			}
			
			private void getCellIndex(int px, int py) {
				ix = (int)(((px - getX()) / getWidth()) * game.getWidth());
				iy = (int)(((py - getY()) / getHeight()) * game.getHeight());
			}
		});
		getAnim().setWidth(game.getWidth() * cellSize);
		getAnim().setHeight(game.getHeight() * cellSize);
		getAnim().setColor(PROC.grey(230));
		getAnim().form = DisplayObject.FORM_RECT;
		
		return null;
	}
	
	@Override
	public void processIO() {
	}

	@Override
	public void guiMessage(GuiSender<?> s) {
		if(s == playButton) 
			play = !play;
	}
	
	@Override
	public void updateSender(GuiSender<?> s) {}
	
	public void play(boolean p) {
		play = p;
	}
	
	public void setCellSize(int size) {
		cellSize = size;
		getAnim().setWidth(game.getWidth() * cellSize);
		getAnim().setHeight(game.getHeight() * cellSize);
	}
	
	public void setDrawIntervall(int interv) {
		drawIntervall = interv;
	}
	
	@Override
	public AbstractModule duplicate() {
		Conway c = (Conway) super.duplicate();
		
		if(c.game.getWidth() != game.getWidth() || c.game.getHeight() != game.getHeight())
			c.game.initializeGrid(game.getWidth(), game.getHeight());
			
		for (int i = 0; i < game.getWidth(); i++) 
			for (int j = 0; j < game.getHeight(); j++) 
				c.game.cells[i][j].alive = game.cells[i][j].alive;
		
		c.play(play);
		c.setCellSize(cellSize);
		c.setDrawIntervall(drawIntervall);
		return c;
	}

	@Override
	protected Module createInstance() {
		return new Conway();
	}
	
	

}
