package module.modules.anim;

import constants.DisplayFlag;
import lombok.var;
import module.AnimObject;
import module.ModuleDeleteMode;
import module.inout.ColorInputIF;
import module.inout.NumberInputIF;
import module.loading.InternalNode;
import module.loading.NodeType;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

//@InternalNode(type = NodeType.ANIM)

public class Line extends AnimObject {

	private ModuleGUI drag1;
	private DisplayObjectIF drag2;
	private ColorInputIF in_col;
	private NumberInputIF in_weight;
	private int stroke;
	private float weight;
	private AnimObject mod;
	private boolean deleted = false;
	
	
	public Line() {
		
		in_col = addColorInput();
		in_weight = addInput("Weight").setDefaultValue(.2);
		
		mod = new AnimObject() {
			
			@Override public void processIO() {}
			
			@Override public DisplayObjectIF createAnim() {
				
				DisplayObjectIF d = new DisplayObject();
				d.setFlag(DisplayFlag.drawable, false);
				return d;
			}
			
			@Override public void onDelete(ModuleDeleteMode mode) {
				
				super.onDelete(mode);
				
				if(!deleted)
					Line.this.delete(ModuleDeleteMode.PATCH);
			}
			
			@Override public boolean isSubModule() {
				return true;
			}
			
			@Override public int pipette(int x, int y) {
				return 0;
			}
			
			@Override public ModuleData doSerialize(SaveDataContext sdc) {
				return Line.this.doSerialize(sdc);
			}
			
			@Override public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
				Line.this.doDeserialize(data, rdc);
			}
		};
		
		mod.setPos(100, 100);
		
		
	}
	
	@Override
	public void postInit() {
		super.postInit();
		
		getDomain().addModule(mod);
		drag2 = mod.getGui();
	}
	
 	
	@Override
	public ModuleGUI createGUI() {
		
		ModuleGUI mg = super.createGUI();
		drag1 = mg;
		//drag1.addChild(drag2);
		//domain.getDisplayManager().GUIContainer.addChild(drag2);
		return mg;
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		
		var d = new DisplayObject() {
			public void render(PGraphics g) {
				
				g.stroke(stroke);
				g.strokeWeight(weight);
				g.line(drag1.getX(), drag1.getY(), drag2.getX(), drag2.getY());
				g.noStroke();
			}
		};
		
		return d;
	}

	
	@Override
	public void processIO() {
		
		if (in_col.changed()) 
			stroke = in_col.getColorInput();
		
		if(in_weight.changed())
			weight = (float) in_weight.getInput() * 20f;
		
	}

	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		deleted = true;
		mod.delete(ModuleDeleteMode.PATCH);
	}

	@Override
	public int pipette(int x, int y) {
		return 0;
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {

		ModuleData md = super.doSerialize(sdc);
		md.putMapValue("x2", drag2.getX());
		md.putMapValue("y2", drag2.getY());
		
		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);

		float x2 = ((Number) data.getMapValue("x2")).floatValue(),
			  y2 = ((Number) data.getMapValue("y2")).floatValue();
		drag2.setPos(x2, y2);
	}

	
}
