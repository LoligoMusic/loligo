package module.modules.anim;

import gui.ImageFilter;
import module.Module;
import module.inout.NumberInputIF;
import pr.DisplayObject;


public class Pixelate extends ImageFilter {
	
	private static final long serialVersionUID = 1L;
	
	final private NumberInputIF in_amp;
	private int amp;
	private final int maxPixelSize = 100;
	
	public Pixelate() {
		
		in_amp = addInput("Size");
		
	}
	
	public void setPixelSize(int s) {
		
		this.amp = s > 1 ? s : 1;
	}
	
	public DisplayObject createFilter() {
		
		ImageFunction foo = new ImageFunction() {
			
			@Override
			protected void processImg() {
				
				int quad = amp, numquadpx = quad * quad;
				int numpx = getWidth() / quad;
				int numpy = getHeight() / quad;
				
				int nump = numpx * numpy;
				int jx, jy;
				
				for (int i = 0; i < nump; i++) {
					
					jx = jy = 0;
					
					int r = 0, g = 0, b = 0;
					
					for (int j = 0; j < numquadpx; j++) {
					
						int p = w + jx + (h + jy) * image.width;
						int c = image.pixels[p];
						
						r += (c >> 16) & 0xFF;
						g += (c >> 8)  & 0xFF;
						b +=  c 	   & 0xFF;
						
						if(jx < quad)
							jx++;
						else{
							jx = 0;
							if(jy < quad)
								jy++;
						}
					}
					
					r /= numquadpx;
					g /= numquadpx;
					b /= numquadpx;
					
					int qc = 0xFF000000 | r << 16 | g << 8 | b;
					
					getDm().g.fill(qc);
					getDm().g.rect(getX() + w, getY() + h, quad, quad);
					
					if(w < image.width)
						w += quad;
					else{
						w = 0;
						if(h < image.height)
							h += quad;
					}
				}
			}
			
			@Override
			protected boolean processCondition() {
				
				return super.processCondition() && amp > 1;
			}
			
			@Override
			protected void postProcess() {
				
			}
			
		};
		
		return foo;
		
	}
	
	
	@Override
	public void processIO() {
		
		if(in_amp.changed()) 
			setPixelSize((int)(in_amp.getInput() * maxPixelSize));
			
			
		
	}
	

	@Override
	public Module createInstance() {
		
		return new Pixelate();
	}

	
}
