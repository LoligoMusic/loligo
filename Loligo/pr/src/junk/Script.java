package junk;

import api.API;
import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;
import module.AbstractModule;
import module.Module;
import module.inout.TriggerInputIF;
import parser.Parser;

public class Script extends AbstractModule {
	
	private static final long serialVersionUID = 2994057147597839584L;
	
	final private TriggerInputIF trigger;
	final private Parser parser = new Parser();

	public Script() {
		
		trigger = addTrigger();
		
		API.loadLib(parser);
		
		try {
			parser.loadPath("C:\\Users\\roman\\Desktop\\eclipse\\workspace\\MathParser\\code2.txt");
		} catch (SyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		parser.evaluate();
		//ps.close();
	}

	@Override
	public void processIO() {
		
		if(trigger.triggered())
			try {
				parser.callFunction("draw");
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
	}

	@Override
	public Module createInstance() {
		return new Script();
	}

}
