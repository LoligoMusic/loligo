package junk;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import junk.SaveFile.FieldObject;

import module.loader.DynClassLoader;

import com.sun.xml.internal.bind.v2.TODO;

import pr.AbstractModule;
import pr.Saveable;

public class Save2 {
	private static ArrayList<Saveable> fieldObjects = new ArrayList<Saveable>();
	private static ArrayList<SaveFile> files = new ArrayList<SaveFile>();
	
	static public void save() {
		fieldObjects.addAll(AbstractModule.abstractModules);
		int i = 0;
		while(i < fieldObjects.size()) {
			SaveFile file = new SaveFile();
			files.add(file);
			file.saveObject = fieldObjects.get(i);
			file.cl = file.saveObject.getClass();
			file.className = file.cl.getName();
			
			
			
			Class<?> superCl = file.cl;	
			Field[] fields = superCl.getDeclaredFields();
			
			
			SaveFile.SuperClass superClass = file.new SuperClass();
			int fieldC = 0;
			while(true) {
				Object o = null;
				try {
					fields[fieldC].setAccessible(true);
					o = fields[fieldC].get(file.saveObject);
				} catch (IllegalArgumentException e) {e.printStackTrace();
				} catch (IllegalAccessException e) {e.printStackTrace();}
				
				superClass.addField(fields[fieldC], o);
				
				if(o instanceof Saveable[]) {
					for (Object p : (Saveable[]) o) 
						if(!fieldObjects.contains(p))
							fieldObjects.add((Saveable) p);
				}else if(o instanceof List<?>)
					for (Object p : (List<?>) o) 
						if(p instanceof Saveable && !fieldObjects.contains(p))
							fieldObjects.add((Saveable) p);
					
				fieldC++;
				if(fieldC >= fields.length) {
					superCl = superCl.getSuperclass();
					if(superCl == Object.class)
						break;
					fields = superCl.getDeclaredFields();
					fieldC = 0;
					superClass = file.new SuperClass();
				}
			};
			i++;
		}
		
		for (FieldObject f : SaveFile.fields) 
			f.setRef();
		
		
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(123456 + ".ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(SaveFile.fields);
			out.close();
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	
	static public void load() {
		ArrayList<?> arr = null;
		try {
			FileInputStream fileIn = new FileInputStream(123456 + ".ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			arr = (ArrayList<?>) in.readObject();
			in.close();
			fileIn.close();
		}catch(IOException i) {
			i.printStackTrace();
		}catch(ClassNotFoundException c)
		{
			System.out.println("DisplayObject class not found");
			c.printStackTrace();
		}
		
		if(arr != null) {
			//ArrayList<AbstractModule> modules = new ArrayList<AbstractModule>();
			ArrayList<Object> objects = new ArrayList<Object>();
			for (Object o : arr) {
				SaveFile s = (SaveFile) o;
				Class<?> cl = null;
				try {
					cl = DynClassLoader.scl.loadClass(s.className);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				if(cl.isAssignableFrom(AbstractModule.class))
					objects.add(DynClassLoader.getModule(s.className));
				else {
					try {
						Constructor<?> cons = cl.get
						objects.add(cl.getC.newInstance());
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					
				}
			}
			
			
		}
		
		
	}
}

class SaveFile implements Serializable{
	

	private static final long serialVersionUID = -6723764566784747660L;
	
	static ArrayList<SaveFile> saveFiles = new ArrayList<SaveFile>();
	static ArrayList<Saveable> saveObjects = new ArrayList<Saveable>();
	String className;
	ArrayList<SuperClass> superClasses = new ArrayList<SuperClass>();
	static ArrayList<FieldObject> fields = new ArrayList<FieldObject>();
	transient Class<?> cl;
	transient Saveable saveObject;
	transient Field field;
	
	public SaveFile() {
		saveFiles.add(this);
	}
	
	/*
	public SaveFile(Saveable saveObject) {
		saveObjects.add(saveObject);
		this.saveObject = saveObject;
		cl = saveObject.getClass();
		className = cl.getClass().getName();
		
		List<SuperClass> liSup = new ArrayList<SuperClass>();
		Class<?> superCl = cl;
		do {
			Field[] fields = superCl.getDeclaredFields();
			List<RefField> li = new ArrayList<RefField>();
			List<NumField> liNum = new ArrayList<NumField>();
			
			for (Field f : fields) {
				Object value = null;
				try {
					value = f.get(saveObject);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				if(value instanceof Number)
					liNum.add(new NumField(f.getName(), (Number) value));
				else if(value instanceof Saveable) {
					SaveFile ss = null;
					for (SaveFile s : saveFiles) 
						if(value == s.saveObject) {
							ss = s;
							break;
						}
					if(ss == null) 
						ss = new SaveFile((Saveable) value);
										
					li.add(new RefField(f.getName(), ss));
				}
			}
			
			//SuperClass sc = new SuperClass(li.toArray(new RefField[li.size()]), 
			//							   liNum.toArray(new NumField[liNum.size()]));
			//liSup.add(sc);
			
			superCl = superCl.getSuperclass();
		} while(superCl != null);
		
		superClasses = new SuperClass[liSup.size()];
		liSup.toArray(superClasses);
	}
	*/
	
	
	@Override public boolean equals(Object o) {
		if(o instanceof SaveFile)
			return this == o;
		else if(o instanceof Saveable)
			return saveObject == o;
		else if(o instanceof Field)
			return field == o;
		else
			return false;
	}
	
	class SuperClass implements Serializable {
		private static final long serialVersionUID = 3245433818684133852L;
		final ArrayList<FieldObject> fieldObjects = new ArrayList<FieldObject>(); 
		 
		public SuperClass() {
			SaveFile.this.superClasses.add(this);
		}
		
		void addField(Field field, Object o) {
			Class<?> cl = field.getClass();
			FieldObject fo = null;
			boolean isNew = true;
			for (FieldObject s : SaveFile.fields) 
				if(s.obj == o) {
					fo = s;
					isNew = false;
					break;
				}
				
			if(fo == null) 
				if(o instanceof Saveable)
					fo = new RefField();
				else if(o instanceof Object[])
					fo = new ArrayField();
				else if(o instanceof List)
					fo = new ListField();
				else if(o instanceof Number) 
					fo = new NumField();
			
			if(fo != null) {
				fieldObjects.add(fo);
				if(isNew) {
					fo.obj = o;
					fo.field = field;
					fo.fieldName = field.getName();
					fieldObjects.add(fo);
					SaveFile.fields.add(fo);
				}
			}
			
		}
	}
	
	abstract class FieldObject implements Serializable{
		private static final long serialVersionUID = -4903030320667156543L;
		
		String fieldName;
		transient Object obj;
		transient Field field;
		
		public FieldObject() {
			
		}

		abstract void setRef(); 
	}
	
	class RefField extends FieldObject {
		private static final long serialVersionUID = -8373215480111449489L;
		SaveFile value;
		
		@Override
		void setRef() {
			for (SaveFile s : saveFiles) 
				if(s.saveObject == obj) {
					value = s;
					break;
				}
		}
	}
	
	class NumField extends FieldObject {
		private static final long serialVersionUID = 3680645696524555372L;
		Number value;
		
		@Override
		void setRef() {
			value = (Number)obj;
		}
	}
	
	class ArrayField extends FieldObject {//TODO check saveable content
		private static final long serialVersionUID = -759063890447146084L;
		SaveFile[] value;
		
		@Override
		void setRef() {
			Object[] objects = (Object[])obj;
			value = new SaveFile[objects.length];
			for (int i = 0; i < objects.length; i++) 
				for (SaveFile s : saveFiles) 
					if(s.saveObject == objects[i]) {
						value[i] = s;
						break;
					}
		}
	}
	
	class ListField extends FieldObject {//TODO check saveable content
		private static final long serialVersionUID = -759063890447146084L;
		List<SaveFile> value;
		
		@Override
		void setRef() {
			List<?> objects = (List<?>)obj;
			value = new ArrayList<SaveFile>(objects.size());
			for (int i = 0; i < objects.size(); i++) 
				for (SaveFile s : saveFiles) 
					if(s.saveObject == objects.get(i)) {
						value.add(s);
						break;
					}
		}
	}
		
		
		
	}