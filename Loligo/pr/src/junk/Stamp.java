package module.modules.anim;

import java.io.IOException;
import java.io.ObjectInputStream;

import module.AbstractModule;
import module.AnimObject;
import module.Canvas;
import module.slot.InSlotColor;
import module.slot.InSlotTrigger;
import pr.DisplayObject;
import processing.core.PGraphics;

public class Stamp extends AnimObject {
	private static final long serialVersionUID = 1L;
	private InSlotTrigger in_trigger;
	private InSlotColor in_color;
	private transient PGraphics graphics;

	public Stamp() {
		
		anim.size = 30;
		graphics = Canvas.getInstance().graphics;
		
		in_trigger = addTrigger();
		in_color = addColorInput();
	}
	
	@Override
	public DisplayObject createAnim() {
		anim = new DisplayObject();
		return anim;
	}
	
	@Override
	public void processIO() {
		//if(in_color.changed)
			anim.color = in_color.getColorInput();
		if(in_trigger.triggered())
			stamp();
	}
	
	private void stamp() {
		PGraphics tg = anim.dm.g;
		anim.dm.g = graphics;
		graphics.beginDraw();
		anim.render(null);
		graphics.endDraw();
		anim.dm.g = tg;
	}
	
	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
		ois.defaultReadObject();
		graphics = Canvas.getInstance().graphics;
	}
	
	@Override
	protected AbstractModule createInstance() {
		return new Stamp();
	}

	

}
