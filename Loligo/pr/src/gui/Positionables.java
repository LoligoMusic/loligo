package gui;

import pr.Positionable;
import processing.core.PMatrix2D;

public final class Positionables {

	private Positionables() {}
	
	/*
	 * Copies x + y values from pos to target.
	 */
	public static void copyPosition(Positionable pos, Positionable target) {
		
		target.setPos(pos.getX(), pos.getY());
	}
	
	
	public static Position position(float x, float y) {
		return new Position(x, y);
	}
	
	/*
	 * Copies x + y values from p to new Position Object.
	 */
	public static Position copyPosition(Positionable p) {
		
		return new Position(p.getX(), p.getY());
	}
	
	public static float distance(Positionable p1, float x, float y) {
		float dx = p1.getX() - x,
			  dy = p1.getY() - y;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}
	
	public static float distance(Positionable p1, Positionable p2) {
		return distance(p1, p2.getX(), p2.getY());
	}
	
	public static boolean withinRange(Positionable p1, Positionable p2, float range) {
		
		float dx = p1.getX() - p2.getX(),
			  dy = p1.getY() - p2.getY();
		
		return dx * dx + dy * dy <= range * range;
	}
	
	public static Positionable subtract(Positionable p1, Positionable p2) {
		p1.setPos(p1.getX() - p2.getX(), p1.getY() - p2.getY());
		return p1;
	}
	
	public static Positionable subtract(Positionable p1, float x, float y) {
		p1.setPos(p1.getX() - x, p1.getY() - y);
		return p1;
	}
	
	public static Positionable add(Positionable p1, Positionable p2) {
		p1.setPos(p1.getX() + p2.getX(), p1.getY() + p2.getY());
		return p1;
	}
	
	public static Positionable add(Positionable p1, float x, float y) {
		p1.setPos(p1.getX() + x, p1.getY() + y);
		return p1;
	}
	
	public static Positionable multiply(Positionable p1, float f) {
		p1.setPos(p1.getX() * f, p1.getY() * f);
		return p1;
	}
	
	public static Positionable divide(Positionable p1, float f) {
		p1.setPos(p1.getX() / f, p1.getY() / f);
		return p1;
	}
	
	public static Positionable apply(Positionable p1, PMatrix2D m) {
		float x = p1.getX(),
			  y = p1.getY();
		x = m.multX(x, y);
		y = m.multY(x, y);
		p1.setPos(x, y);
		return p1;
	}
	
}
