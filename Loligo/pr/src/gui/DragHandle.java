package gui;

import constants.DisplayFlag;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.Positionable;
import processing.core.PGraphics;
import util.Colors;

public class DragHandle extends DisplayObject {
	
	public static void RENDER(DisplayObjectIF d, PGraphics g) {
		
		int c = d.getColor();
		float x = d.getX();
		float y = d.getY();
		
		g.fill(c);
		g.noStroke();
		g.ellipse(x + .2f, y, 4 , 4);
		g.noFill();
		g.stroke(c);
		g.strokeWeight(1.5f);
		g.ellipse(x, y, d.getWidth(), d.getHeight());
		g.noStroke();
	}
	
	
	public int defaultColor =  Colors.rgb(200, 200, 10);
	
	public Positionable target;
	public GUIBox guiBox;
	
	public DragHandle() {
		this(null, null);
	}
	
	
	public DragHandle(final Positionable _target, final GUIBox _guiBox) {
		
		setFlag(DisplayFlag.DRAGABLE, true);
		setFlag(DisplayFlag.snapsToPath, true);
		setFlag(DisplayFlag.ROUND, true);
		
		target = _target;
		if(target != null)
			setPosObject(new Position.PositionTarget(target));
		guiBox = _guiBox;
		//if(guiBox != null)
		//	guiBox.enableToggleSize(false);
		
		setWH(12, 12);
		setColor(defaultColor);
	}
	
	@Override
	public void render(PGraphics g) {
		
		RENDER(this, g);
		
	}
	
	/*
	@Override
	public void mouseDragged() {
		if(target != null) {
			target.setPos(x, y);
			//target.mouseDragged();
		}
	}
	*/
	
	
	/*
	@Override
	public void mouseClicked() {
		if(guiBox != null){
			if(guiBox.isOpen)
				guiBox.hide();
			else {
				guiBox.setPos(getX() + getWidth() / 2, getY() - guiBox.getHeight());
				getDm().getInput().setRemovableTarget(guiBox);
				
			}
		}
	}
	*/
	
	@Override
	public void setSelected(boolean b) {
		
		setColor(b ? 0xffffffff : defaultColor);
	}
	
}
