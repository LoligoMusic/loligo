package gui.paths;

import constants.AbstractEventManager;
import gui.paths.PathEvent.PathObjectAddRemove;
import gui.paths.PathEvent.SegmentAddRemove;
import gui.paths.PathEvent.UpdateLengthEvent;
import gui.paths.PathEvent.UpdateObjectsEvent;


public class PathEventManager extends AbstractEventManager<PathListener> implements PathListener{

	static private PathEventManager instance;
	
	
	public static PathEventManager instance() {
		
		if(instance == null)
			instance = new PathEventManager();
		
		return instance;
	}
	
	private void dispatchEvent(PathEvent e) {
		
 		updateRemoved();
		
		for (PathListener p : listeners) 
			
			p.onPathEvent(e);
		
	}
	
	public void dispatchSegmentAddEvent(Segment s, Path p) {
		
		PathEvent e = new SegmentAddRemove(true, p, s);
		dispatchEvent(e);
	}
	
	public void dispatchSegmentRemoveEvent(Segment s, Path p) {
		
		PathEvent e = new SegmentAddRemove(false, p, s);
		dispatchEvent(e);
	}
	
	public void dispatchPathObjectAddedEvent(PathObject po, Path path) {
		
		PathEvent e = new PathObjectAddRemove(true, po, path);
		dispatchEvent(e);
	}
	
	public void dispatchPathObjectRemovedEvent(PathObject po, Path path) {
		
		PathEvent e = new PathObjectAddRemove(false, po, path);
		dispatchEvent(e);
	}
	
	/*
	public void dispatchPathCreatedEvent(Path path) {
		
		PathEvent e = new PathEvent(path, PATH_CREATE);
		dispatchEvent(e);
	}
	
	public void dispatchPathDeletedEvent(Path path) {
		
		PathEvent e = new PathEvent(path, PATH_DELETE);
		dispatchEvent(e);
	}
	*/
	
	public void dispatchUpdateLengthEvent(Segment segment) {
		
		PathEvent e = new UpdateLengthEvent(segment);
		dispatchEvent(e);
	}
	
	public void dispatchUpdateObjectsEvent(Segment segment) {
		
		PathEvent e = new UpdateObjectsEvent(segment);
		dispatchEvent(e);
	}

	@Override
	public void onPathEvent(PathEvent e) {
		
		dispatchEvent(e);
	}
	
}
