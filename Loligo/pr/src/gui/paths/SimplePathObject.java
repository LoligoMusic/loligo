package gui.paths;

public class SimplePathObject extends AbstractPathObject {

	private Mapping mapping = Mapping.WRAP;
	
	
	
	public double pathPosition() { //Override this to generate new pos
		
		return 0;
	}
	
	
	public void setPositionMapped(double d) {
		
		goToPathPosition(mapping.map(d));
	}
	
	
	@Override
	public void updatePosition() {
		
		goToPathPosition(
				mapping.map(
						pathPosition()));
	}
	
	
	public Mapping getMapping() {
		
		return mapping;
	}

	
	public void setMapping(Mapping mapping) {
		
		this.mapping = mapping;
	}
	
	
	public static enum Mapping {
		
		WRAP {
			@Override
			double map(double d) {
				
				return d < 0 ? d % 1 + 1 : d % 1;
			}
		},
		CLAMP {
			@Override
			double map(double d) {
			
				return d > 1 ? 1:
					   d < 0 ? 0:
						   	   d;
			}
		},
		MIRROR {
			@Override
			double map(double d) {
				
				return ((int) d & 1) == 0 ? WRAP.map(d) :
											1 - WRAP.map(d);
			}
		};
		
		abstract double map(double d);
	
	}

}
