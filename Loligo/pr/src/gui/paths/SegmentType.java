package gui.paths;

public enum SegmentType {

	LINEAR,
	BEZIER,
	CURVE,
	CIRCULAR,
	PATH,
	BUFFER
	;
}
