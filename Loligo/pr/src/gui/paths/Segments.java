package gui.paths;

import gui.paths.gui.PathGUI;
import gui.paths.gui.SegmentGUI;

public class Segments {

	private Segments() {}
	
	
	public static final float HIT_DISTANCE = 5;
	
	public static Path rootSegment(Segment s) {
		
		Segment p = s;
		
		while(true) {
			
			Segment pp = p.getSuperSegment();
			
			if(pp == null || p == pp)
				break;
			else
				p = pp;
		}
		
		return (Path) p;
	}
	
	public static void linearPos(PathObject po, float p1x, float p1y, float dx, float dy, double fac) {
		
		po.setPos(
				lerp(p1x, dx, fac),
				lerp(p1y, dy, fac)
			);
	}
	
	/**
	 * Linear interpolation, absolute
	 */
	public static float lerp2(float a, float b, float f)
	{
	    return a + f * (b - a);
	}
	
	/**
	 * Linear interpolation, second arg is diff
	 */
	public static float lerp(float p, float d, double fac) {
		
		return p + d * (float) fac;
	}
	
	public static float perpendicularPoint(float p1x, float p1y, float dx, float dy, float x, float y) {
		
		float skal = (dy * (y - p1y) + dx * ( x - p1x)) / (dx * dx + dy * dy);
		
		return skal; 
	}
	
	public static boolean hitTest(float p1x, float p1y, float dx, float dy, float x, float y) {
		
		float skal = perpendicularPoint(p1x, p1y, dx, dy, x, y);
		return hitTest(skal, p1x, p1y, dx, dy, x, y);
	}
	
	public static boolean hitTest(float skal, float p1x, float p1y, float dx, float dy, float x, float y) {
		
		if(skal >= 0 && skal <= 1) {
			
			float nX = p1x + dx * skal - x;
			float nY = p1y + dy * skal - y;
			
			float dsqrt = nX * nX + nY * nY;
			
			return dsqrt <= HIT_DISTANCE * HIT_DISTANCE;
		}
		return false;
	}
	
	public static double toParentPos(double p, Segment child) {
		
		Segment ss = child.getSuperSegment();
		
		if(ss == null)
			return p;
		
		return ss.getLength() == 0 ? 0 :
									 (child.getParentOffset() + p * child.getLength()) / ss.getLength();

	}
	
	public static double toChildPos(double fac, Path pa, Segment seg) {
		
		return pa.getLength() == 0 ? 0 : 
									(fac * pa.getLength() - seg.getParentOffset()) / seg.getLength();
	}
	
	public static double absLength(Segment s, double pos1, double pos2) {
		
		return Math.abs(pos2 - pos1) * s.getLength(); 
	}
	
	public static void checkNormalized(double pos) {
		
		assert pos <= 1 && pos >= 0;
		//	throw new IllegalArgumentException("Argument out of normalized range: " + pos);
	}
	
	/**
	 * Creates new Segment of the specified type. Doesn't initialize with Segment.init()
	 * @param posConstraint
	 * @param n1
	 * @param n2
	 * @param type
	 * @return
	 */
	public static Segment createSegmentByType(PosConstraint posConstraint, Node n1, Node n2, SegmentType type) {
		
		Segment s = 
			type == SegmentType.PATH   ? new PathSegment	(posConstraint) 				:
			type == SegmentType.LINEAR ? new LinearSegment	(posConstraint, null, n1, n2) 	:
			type == SegmentType.BEZIER ? new BezierSegment	(posConstraint, null, n1, n2) 	:
			type == SegmentType.CURVE  ? new CurveSegment	(posConstraint, null, n1, n2) 	:
						 			 	 new OrthoSegment	(posConstraint, null, n1, n2)  	;
		
		return s;
	}
	
	
	
	public static SegmentGUI createGUI(Segment seg) {
		
		SegmentType t = seg.getSegmentType();
		
		if(t == SegmentType.PATH) {
			
			PathGUI pg = new PathGUI((PathSegment) seg);
			
			return pg;
		}
		
		SegmentGUI sg = 
			t == SegmentType.LINEAR   ? new SegmentGUI.Linear((LinearSegment) seg) :
			t == SegmentType.BEZIER   ? new SegmentGUI.Bezier((BezierSegment) seg) :
			t == SegmentType.CURVE    ? new SegmentGUI.Curve((CurveSegment) seg) :
			t == SegmentType.CIRCULAR ? new SegmentGUI.Circular((CircularSegment) seg) :
			t == SegmentType.BUFFER	  ? new SegmentGUI.Buffer((BufferedSegment) seg) :
			t == SegmentType.PATH	  ? null : null;
			
		return sg;
	}

	public static void removePathFromNode(Node n, Segment s) {
		int i = indexOf(n, s);
		if(i >= 0)
			n.removeLink(i);
	}

	private static int indexOf(Node n, Segment s) {
		
		Segment ss;
		
		for (int i = 0; i < n.numLinks(); i++) {
			
			ss = n.getLink(i);
			
			while(ss != null) {
				
				if(ss == s) {
					return i;
				}
				ss = ss.getSuperSegment();
			}
		}
		return -1;
	}
	
	/*
	static public void connectSegment(Node n, Segment s) {
		
		n.addLink(s);
		s.setNodeIn(n);
	}
	*/
	 
	static public Node splitSubSegment(Path path, double pos) {
		
		checkNormalized(pos);
		
		PosConstraint po = path.getPosConstraint();
		
		Segment seg = path.lowestSegmentAt(pos);
		
		Node node = po.newNode();
		SegmentPair sp = seg.split(toChildPos(pos, path, seg), node);
		
		if(sp != null) {
			int i = path.indexOf(seg);
			path.removeSegment(i);
			path.addSegment(i	 , sp.getSegment1());
			path.addSegment(i + 1, sp.getSegment2());
		}
		
		return node;
	}
	
	
	static public Segment removeVertex(Path path, Node node) {
		
		int n = node.numLinks();
		
		if(n <= 0) {
			return null;
		}
		
		Segment[] segs = new Segment[n];
		
		int idx = 0;
		
		for (int i = path.numSegments() - 1; i >= 0; i--) {
			Segment s = path.getSegment(i);
			if(s.getNodeOut() == node) {
				idx = i;
				break;
			}
		}
		
		for (int i = n - 1; i >= 0; i--) {
			Segment s = segs[i] = node.getLink(i);
			path.removeSegment(s);
			s.getNodeIn().removeLink(s); 
			s.getNodeOut().removeLink(s); 
		}
		
		//idx = Math.max(idx - 1, 0);
		
		if(n > 1) {
			
			Segment s1 = segs[0],
					s2 = segs[1];
			Node n1 = s1.getNodeIn() == node ? s1.getNodeOut() : s1.getNodeIn(),
				 n2 = s2.getNodeIn() == node ? s2.getNodeOut() : s2.getNodeIn();
			
			Segment seg = path.getPosConstraint().createSegment(n1, n2, s1.getSegmentType());
			
			idx = Math.min(idx, path.numSegments());
			path.addSegment(idx, seg);
			
			return seg;
		}
		
		return null;
	}
	
	
	static public SegmentPair splitSegment(Segment oldSeg, Node newNode, double pos) {
		
		PosConstraint pc = oldSeg.getPosConstraint();
		
		NullPathObject no = new NullPathObject();
		oldSeg.goToPos(no, pos);
		newNode.setPos(no.getX(), no.getY());
		
		oldSeg.getNodeIn().removeLink(oldSeg);
		oldSeg.getNodeOut().removeLink(oldSeg);
		
		Segment seg1 = pc.createSegment(oldSeg.getNodeIn(), newNode, 			 oldSeg.getSegmentType()),
				seg2 = pc.createSegment(newNode, 			oldSeg.getNodeOut(), oldSeg.getSegmentType());
		
		SegmentPair pair = new SegmentPair(seg1, seg2);
		
		
		
		//int i1 = indexOf(oldSeg.getNodeIn() , oldSeg),
		//	i2 = indexOf(oldSeg.getNodeOut(), oldSeg);
		
		
		
		/*
		seg1.getNodeIn().addLink(seg1, i1);
		seg1.getNodeOut().addLink(seg1);
		
		seg2.getNodeIn().addLink(seg2);
		seg2.getNodeOut().addLink(seg2, i2);
		*/
		return pair;
	}

	
	
}
