package gui.paths;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;


public class PathMementoFactory {

	private List<VertexMemento> vertices;
	private List<SegmentMemento> segments;
	private PosConstraint constraint;
	
	
	public PathMementoFactory(PosConstraint constraint) {
		this.constraint = constraint;
	}
	
	
	public void restoreConstraint(ConstraintMemento cm) {
		
		for (SegmentMemento sm : cm.paths) {
			
			Path p = new PathSegment(constraint);
			p.restoreMemento(sm, this);
			constraint.addPath(p);
		}
	}
	
	/*
	private Node initVertex(VertexMemento vm) {
		
		if(vm.node == null) {
			
			vm.node = constraint.newNode();
			vm.node.setPos(vm.x, vm.y);
		}
		
		return vm.node;
	}
	*/
	
	public Segment createSegment(SegmentMemento sm) {
		
		Segment s = constraint.createSegment(sm.vertex1.node, sm.vertex2.node, sm.segmentType);
		s.restoreMemento(sm, this);
		return s;
	}
	
	
	public ConstraintMemento toMemento() {
		
		//List<Node> nodes = constraint.getNodes();
		vertices = new ArrayList<>();
		
		//for (Node node : nodes) 
		//	findVertexMemento(node);
		
		List<Path> pms = constraint.getPaths();
		segments = new ArrayList<>(pms.size());
		
		for (Path p : pms) 
			segments.add(p.toMemento(this));
		
		ConstraintMemento cm = new ConstraintMemento(segments, vertices);
		constraint.toConstraintMemento(cm);
		
		return cm;
	}
	
	
	public VertexMemento findVertexMemento(Node n) {
		
		for (VertexMemento v : vertices) 
			if(v.node == n) {
				return v;
			}
		
		VertexMemento vm = new VertexMemento(n);
		vertices.add(vm);
		
		return vm;
	}
	
	
	public static class ConstraintMemento {
		
		public final List<VertexMemento> vertices;
		public final List<SegmentMemento> paths;
		
		public ConstraintMemento() {
			
			vertices = new ArrayList<>();
			paths = new ArrayList<>();
		}
		
		public ConstraintMemento(List<SegmentMemento> paths, List<VertexMemento> vertices) {
			this.paths = paths;
			this.vertices = vertices;
		}
	}
	
	
	public static class SegmentMemento {
		
		@JsonIgnoreProperties(ignoreUnknown = true)
		
		@Getter@Setter
		private SegmentMemento[] subSegments;
		public SegmentType segmentType;
		public VertexMemento vertex1, vertex2;
		@Getter
		private float[] controlPoints;
		
		public SegmentMemento() {}
		
		public SegmentMemento(SegmentType type, VertexMemento v1, VertexMemento v2) {
			
			this.vertex1 = v1;
			this.vertex2 = v2;
			segmentType = type;
			
		}

		public void setControlPoints(float x1, float y1, float x2, float y2) {
			
			controlPoints = new float[] {x1, y1, x2, y2};
		}
		
	}
	
	/*
	@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@vert")
	public static class VertexMemento {
		
		transient public Node node;
		public final float x, y;
		 
		public VertexMemento(Node n) {
			
			x = n.getX();
			y = n.getY();
		}
	}
	*/
	
}
