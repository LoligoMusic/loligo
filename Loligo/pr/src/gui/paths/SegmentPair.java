package gui.paths;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
public class SegmentPair {

	@Getter
	private final Segment segment1, segment2;
	
	public SegmentPair(Segment seg1, Segment seg2) {
		
		this.segment1 = seg1;
		this.segment2 = seg2;
	}
	
	
	public static SegmentPair of(Segment seg1, Segment seg2) {
		
		return new SegmentPair(seg1, seg2);
	}
	
}
