package gui.paths;

/**
 * 1D line, position of followers defined by a single float
 */

public interface PathConstraint extends PosConstraint {

	
	public void setOffset(double offset);
	
	public double getOffset();

	public Path getPath();
}
