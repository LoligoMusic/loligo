package gui.paths;

import gui.paths.PathMementoFactory.SegmentMemento;

public class LinearSegment extends AbstractSegment {

	private float dx, dy;
	private double tangent;
	
	public LinearSegment(PosConstraint posConstraint, Path path, Node in, Node out) {
		
		super(posConstraint, path, in, out);
	}

	
	@Override
	public boolean hitTest(float x, float y) {
		
		Node n1 = getNodeIn();
		
		float d = Segments.perpendicularPoint(
				n1.getX(), 
				n1.getY(), 
				dx, 
				dy, 
				x, y);
		
		return Segments.hitTest(d, n1.getX(), n1.getY(), dx, dy, x, y);
	}
	
	@Override
	public double nearestPoint(float x, float y) {
		
		Node n1 = getNodeIn();
		
		double d = Segments.perpendicularPoint(
					n1.getX(), 
					n1.getY(), 
					dx, 
					dy, 
					x, y);
		
		d = d < 0 ? 0 :
			d > 1 ? 1 :
					d ;
		
		return d;
	}

	@Override
	public void goToPos(PathObject po, double fac) {
	
		Node n = getNodeIn();
		
		Segments.linearPos(
				po, 
				n.getX() - getPosConstraint().getX(), 
				n.getY() - getPosConstraint().getY(), 
				dx, dy, fac);
		
		po.setTanget(tangent);
	}

	
	@Override
	public Segment getSuperSegment() {
		
		return getPath();
	}

	@Override
	public double updateLength() {
		
		Node n1 = getNodeIn(),
			 n2 = getNodeOut();
		
		dx = n2.getX() - n1.getX();
		dy = n2.getY() - n1.getY();
		
		double len = Math.sqrt(dx * dx + dy * dy);
		
		tangent = Math.atan2(dy, dx);
		
		setLength(len);
		return len;
	}


	@Override
	public SegmentType getSegmentType() {
	
		return SegmentType.LINEAR;
	}

	@Override
	public void restoreMemento(SegmentMemento sm, PathMementoFactory pmf) {
		
	}

}
