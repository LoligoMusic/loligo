package gui.paths;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import gui.Position;
import gui.paths.PathMementoFactory.ConstraintMemento;
import lombok.Getter;
import lombok.Setter;

public class SinglePathConstraint implements PosConstraint {

	@Getter@Setter
	private SegmentFactory segmentFactory = SegmentFactory.defaultFactory;
	private PathListener pathListener = e -> {};
	private Position pos = new Position();
	private Path path;
	
	public SinglePathConstraint() {
		
		path = new PathSegment(this);
	}
	
	public SinglePathConstraint(Path path) {
		
		this.path = path;
	}

	public void setPath(Path p) {
		
		path = p;
	}
	
	public Path getPath() {
		return path;
	}
	
	@Override
	public float getX() {
		
		return pos.getX();
	}

	@Override
	public float getY() {
		
		return pos.getY();
	}

	@Override
	public void setPos(float x, float y) {
		
		pos.setPos(x, y);
	}

	@Override
	public Position getPosObject() {
		
		return pos;
	}

	@Override
	public void setPosObject(Position p) {
		
		pos = p;
	}

	@Override
	public void updatePosConstraint() {
		
		path.updateLength();
	}

	@Override
	public void setPathListener(PathListener pathListener) {
		
		this.pathListener = pathListener;
	}

	@Override
	public void dispatchEvent(PathEvent e) {
		
		if(pathListener != null)
			pathListener.onPathEvent(e);
	}

	@Override
	public List<Node> getNodes() {
		
		List<Node> n = new ArrayList<>(2);
		if(path.getNodeIn() != null)
			n.add(path.getNodeIn());
		if(path.getNodeOut() != null)
			n.add(path.getNodeOut());
		
		return n;
	}

	@Override
	public List<Path> getPaths() {
		
		return Collections.singletonList(path);
	}

	@Override
	public void removeNode(Node n) {
		
		//throw new UnsupportedOperationException();
		Segments.removeVertex(path, n);
		pathListener.onPathEvent(new PathEvent.NodeAddRemove(false, n));
	}

	@Override
	public void removePath(Path p) {
		
		throw new UnsupportedOperationException();
	}

	@Override
	public Path addPath(Node n1, Node n2, SegmentType type) {
		
		throw new UnsupportedOperationException();
	}

	@Override
	public Path addPath(Path path) {
		
		setPath(path);
		
		pathListener.onPathEvent(new PathEvent.PathAddRemove(PathEvent.ADD, path));
		
		return path;
	}

	
	@Override
	public Segment createSegment(Node n1, Node n2, SegmentType type) {
		
		return getSegmentFactory().createSegmentByType(this, n1, n2, type);
	}
	
	@Override
	public Node newNode() {
		
		Vertex v = new Vertex(this);
		addNode(v);
		return v;
	}

	@Override
	public void addNode(Node n) {
		
		n.setPosObject(new Position.PositionOffset(this));
		pathListener.onPathEvent(new PathEvent.NodeAddRemove(PathEvent.ADD, n));
	}

	
	@Override
	public void toConstraintMemento(ConstraintMemento cm) {
	
	}

	@Override
	public void restoreConstraintMemento(ConstraintMemento cm) {
		
		//path.restoreMemento(cm.paths.get(0), null);
	}

}
