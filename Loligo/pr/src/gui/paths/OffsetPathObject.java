package gui.paths;

import constraints.PosConstraintOffset;
import gui.Position;

public class OffsetPathObject extends SimplePathObject {
	
	private final PosConstraintOffset constraint;
	private double offset;

	
	public OffsetPathObject(PosConstraintOffset constraint) {
		
		this(constraint, 0);
	}
	
	
	public OffsetPathObject(PosConstraintOffset constraint, double offset) {
		
		this.offset = offset;
		this.constraint = constraint;
		setPath(constraint.getPath());
		
		setPosObject(new Position.PositionOffset(constraint));
	}
	
	
	@Override
	public double pathPosition() {
		
		return constraint.getOffset() + offset;
	}
	
	
	public double getOffset() {
		return offset;
	}
	

	public void setOffset(double offset) {
		this.offset = offset;
	}
	
	@Override
	public void copyPositionTo(PathObject po) {
		
		//super.copyPositionTo(po);
		
		System.out.println(getPathPosition() + " " + offset);
		
		po.goToPathPosition(offset);
	}
}
