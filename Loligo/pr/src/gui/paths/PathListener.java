package gui.paths;

public interface PathListener {

	public void onPathEvent(PathEvent e);
}
