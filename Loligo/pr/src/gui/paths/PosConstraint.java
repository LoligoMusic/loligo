package gui.paths;

import java.util.List;

import gui.paths.PathMementoFactory.ConstraintMemento;
import pr.Positionable;


public interface PosConstraint extends Positionable {

	//public List<Constrainable> getConstrainables();
	public void updatePosConstraint();
	
	public void setPathListener(PathListener pathListener);
	
	public void dispatchEvent(PathEvent e);
	
	public List<Node> getNodes();
	
	public List<Path> getPaths();

	void removeNode(Node n);

	void removePath(Path p);

	Path addPath(Node n1, Node n2, SegmentType type);
	
	/*
	 * throws IllegalArgumentException if path wasn't initialized with this PosConstraint.
	 */
	Path addPath(Path path);

	Node newNode();

	void addNode(Node n);
	
	void toConstraintMemento(ConstraintMemento cm);
	
	void restoreConstraintMemento(ConstraintMemento cm);
	
	Segment createSegment(Node n1, Node n2, SegmentType type);
	
	void setSegmentFactory(SegmentFactory sf);
	
	SegmentFactory getSegmentFactory();
	
	
	/**
	 * Add PathObject and snap to the right segment/position
	 * 
	 * @param po
	 */
	//public void positionPathObject(PathObject po);
	
	//public void removePathObject(PathObject po);
	
}
