package gui.paths;

import java.util.EnumSet;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayGuiSender;
import gui.DropDownMenu;
import gui.GuiSenderObject;
import gui.Position;
import gui.selection.GuiType;
import module.inout.IOEnum;
import pr.DisplayContainerIF;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public class EnumDropDownMenu <T extends Enum<T>> implements DisplayGuiSender<T>{

	private final DropDownMenu dropDownMenu;
	private final GuiSenderObject<T> sender;
	private final EnumSet<T> enums;
	
	
	public EnumDropDownMenu(String name, Consumer<T> action, Supplier<T> updateAction, T theEnum) {
		
		sender = new GuiSenderObject<T>(this, action, updateAction, theEnum);
		
		
		enums = EnumSet.allOf(theEnum.getDeclaringClass());
		
		String[] items = IOEnum.enumToStringArray(theEnum);
		
		Supplier<Integer> sup = () -> {
			sender.update();
			return getEnumIndex();
		};
		
		dropDownMenu = new DropDownMenu(this::setEnumIndex, sup, items);
		dropDownMenu.singleThread();
		
		sender.onUpdate(s -> dropDownMenu.setValue(getEnumIndex()));
		
	}
	
	
	public void setEnumIndex(int i) {
		
		T en = enums.stream().filter(e -> e.ordinal() == i).findAny().get();
		sender.message(en);
	}
	
	
	public int getEnumIndex() {
		return sender.getValue().ordinal();
	}

	public T getValue() {
		return sender.getValue();
	}


	public void setValue(T v) {
		sender.setValue(v);
	}
	
	// Delegate methods ---------------------------------------------------------------------

	
	public void update() {
		//dropDownMenu.update();
		sender.update();
	}


	public void disable(boolean b) {
		dropDownMenu.disable(b);
	}


	public DisplayContainerIF getDm() {
		return dropDownMenu.getDm();
	}


	public void singleThread() {
		dropDownMenu.singleThread();
	}


	public void mouseEvent(InputEvent e, UserInput input) {
		dropDownMenu.mouseEvent(e, input);
	}


	public void addedToDisplay() {
		dropDownMenu.addedToDisplay();
	}


	public void removedFromDisplay() {
		dropDownMenu.removedFromDisplay();
	}


	public boolean hitTest(float mx, float my) {
		return dropDownMenu.hitTest(mx, my);
	}


	public void render(PGraphics g) {
		dropDownMenu.render(g);
	}


	public void addChild(DisplayObjectIF d) {
		dropDownMenu.addChild(d);
	}


	public void addChild(DisplayObjectIF... dArr) {
		dropDownMenu.addChild(dArr);
	}


	public void removeChild(DisplayObjectIF d) {
		dropDownMenu.removeChild(d);
	}


	public void removeChild(DisplayObjectIF[] dArr) {
		dropDownMenu.removeChild(dArr);
	}


	public void setSelectionObject(Consumer<PGraphics> cons) {
		dropDownMenu.setSelectionObject(cons);
	}


	public void setSelected(boolean b) {
		dropDownMenu.setSelected(b);
	}


	public void fixedToParent(boolean b) {
		dropDownMenu.fixedToParent(b);
	}


	public float getX() {
		return dropDownMenu.getX();
	}


	public float getY() {
		return dropDownMenu.getY();
	}


	public void setPos(float x, float y) {
		dropDownMenu.setPos(x, y);
	}


	public void setGlobal(float x, float y) {
		dropDownMenu.setGlobal(x, y);
	}


	public Position getPosObject() {
		return dropDownMenu.getPosObject();
	}


	public void setPosObject(Position p) {
		dropDownMenu.setPosObject(p);
	}


	public DisplayObjectIF getParent() {
		return dropDownMenu.getParent();
	}


	public void setParent(DisplayObjectIF parent) {
		dropDownMenu.setParent(parent);
	}


	public List<DisplayObjectIF> getChildren() {
		return dropDownMenu.getChildren();
	}


	public void setDm(DisplayContainerIF dm) {
		dropDownMenu.setDm(dm);
	}


	public boolean checkFlag(DisplayFlag flag) {
		return dropDownMenu.checkFlag(flag);
	}


	public void setFlag(DisplayFlag f, boolean b) {
		dropDownMenu.setFlag(f, b);
	}


	public int getColor() {
		return dropDownMenu.getColor();
	}


	public int setColor(int color) {
		return dropDownMenu.setColor(color);
	}


	public int getStrokeColor() {
		return dropDownMenu.getStrokeColor();
	}


	public void setStrokeColor(int strokeColor) {
		dropDownMenu.setStrokeColor(strokeColor);
	}


	public int getWidth() {
		return dropDownMenu.getWidth();
	}


	public void setWH(int width, int height) {
		dropDownMenu.setWH(width, height);
	}


	public int getHeight() {
		return dropDownMenu.getHeight();
	}


	public GuiType getGuiType() {
		return dropDownMenu.getGuiType();
	}


	public void setGuiType(GuiType guiType) {
		dropDownMenu.setGuiType(guiType);
	}


	public void mouseEventThis(InputEvent e, UserInput input) {
		dropDownMenu.mouseEventThis(e, input);
	}

	@Override
	public void mouseOverRest(boolean on) {
		dropDownMenu.mouseOverRest(false);
	}
	
}
