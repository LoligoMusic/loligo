package gui.paths;

public interface StepPathObject extends PathObject {

	public void doStep();
}
