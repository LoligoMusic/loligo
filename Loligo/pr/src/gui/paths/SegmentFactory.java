package gui.paths;

public interface SegmentFactory {

	Segment createSegmentByType(PosConstraint posConstraint, Node n1, Node n2, SegmentType type);

	/**
	 * Singleton factory used as default
	 */
	static final SegmentFactory defaultFactory = new SegmentFactory() {
		
		public Segment createSegmentByType(PosConstraint posConstraint, Node n1, Node n2, SegmentType type) {
			return Segments.createSegmentByType(posConstraint, n1, n2, type);
		}
	};
	
	
	
	
}
