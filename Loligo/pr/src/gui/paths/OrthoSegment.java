package gui.paths;

import gui.paths.PathMementoFactory.SegmentMemento;

public class OrthoSegment extends BufferedSegment {

	private boolean up;
	
	
	public OrthoSegment(PosConstraint posConstraint, Path path, Node in, Node out) {
		
		super(posConstraint, path, in, out);
		
		setNumVertices(3);
	}

	@Override
	public SegmentType getSegmentType() {
		
		return SegmentType.BUFFER;
	}

	@Override
	public void updateVertexBuffer() {
		
		SubVertex[] ss = getSubVertices();
		
		Node n1 = getNodeIn(),
		     n2 = getNodeOut();
		
		SubVertex s1 = ss[0];
		s1.x = 0;
		s1.y = 0;
		
		SubVertex s3 = ss[2];
		s3.x = n2.getX() - n1.getX();
		s3.y = n2.getY() - n1.getY();
		
		SubVertex s2 = ss[1];
		
		if(up) {
			s2.x = s3.x;
			s2.y = s1.y;
		}else{
			s2.x = s1.x;
			s2.y = s3.y;	
		}
	}
	
	public boolean isUp() {
		return up;
	}

	public void setUp(boolean up) {
		this.up = up;
	}

	@Override
	public void restoreMemento(SegmentMemento sm, PathMementoFactory pmf) {
		// TODO Auto-generated method stub
		
	}
	
}
