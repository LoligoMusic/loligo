package gui.paths;

public abstract class PathTreeRunner extends AbstractPathObject implements StepPathObject{

	//public PathTree getPathTree();

	private double speed;
	private boolean speedSign = true;
	

	abstract public Path chooseNextSegment(Node nodeCrossed);
	
	
	public Path chooseNextSegment() { //Override 

		return chooseNextSegment(null);
	}
	
	
	@Override
	public void doStep() {
		
		Path path = getPath();
		double segLen = path.getLength(),
			   dirSpeed = speed;
		
		double pos = getPathPosition() + getSpeed() / segLen;
		
		if(pos <= 1 && pos >= 0) {
			
			goToPathPosition(pos);
			
		}else{
			
			double p = getPathPosition() * segLen;
			dirSpeed = dirSpeed - (speedSign ? segLen - p : p) + segLen;
			
			while(dirSpeed > segLen) {
				
				dirSpeed -= segLen;
				
				Node nOut = speedSign ? path.getNodeOut() : path.getNodeIn(); 
				
				path = chooseNextSegment(nOut);
				segLen = path.getLength();
				
				speedSign = nOut == path.getNodeIn();
			}
			
			double pp = dirSpeed / segLen;
			setPath(path);
			goToPathPosition(speedSign ? pp : 1 - pp);
		}
		
		
		
	}
	
	
	public double getSpeed() {
		
		return speedSign ? speed : -speed;
	}
	

	public void setSpeed(double speed) {
		
		this.speed = Math.abs(speed);
		this.speedSign = speed >= 0;
	}
	
	
}
