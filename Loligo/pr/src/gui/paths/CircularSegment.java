package gui.paths;

import gui.paths.PathMementoFactory.SegmentMemento;

public class CircularSegment extends AbstractSegment {

	private double radius, sektor = 1;
	
	
	public CircularSegment(PosConstraint posConstraint, Path parent) {
		
		super(posConstraint, parent, new Vertex(posConstraint), new Vertex(posConstraint));
	}
	

	@Override
	public void goToPos(PathObject po, double fac) {
		
		double f = fac * sektor;
		double d = f * Math.PI * 2;
		
		po.setPos((float)(Math.sin(d) * radius), 
				  (float)(Math.cos(d) * radius));
		
		po.setTanget(f);
	}
	

	@Override
	public boolean hitTest(float x, float y) {
		
		float dx = x - getPosConstraint().getX();
		float dy = y - getPosConstraint().getY();
		boolean b = Math.abs(dx * dx + dy * dy - radius * radius) < 300;
		
		return b;
	}
	

	@Override
	public double nearestPoint(float x, float y) {
		
		float dx = x - getPosConstraint().getX(),
		 	  dy = y - getPosConstraint().getY();
		
		return (1.25 - (Math.atan2(dy, dx) / (Math.PI * 2))) % 1;
	}
	

	@Override
	public double updateLength() {
		double len = 2 * Math.PI * radius * sektor;
		setLength(len);
		return len;
	}
	

	@Override
	public SegmentType getSegmentType() {
		
		return SegmentType.CIRCULAR;
	} 
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		
		this.radius = radius;
		
		Path p = getPath();
		
		if(p == null)
			updateLength();
		else
			p.updateLength();
		
	}


	@Override
	public SegmentPair split(double pos, Node node) {
		
		return null;
		//throw new UnsupportedOperationException();
	}


	@Override
	public void restoreMemento(SegmentMemento sm, PathMementoFactory pmf) {
		// TODO Auto-generated method stub
		
	}
	
}
