package gui.paths;

import pr.Positionable;


public interface Node extends Positionable, PathListener{

	public final int VERTEX = 0, NODE = 1;
	
	public void updateSegments();
	
	public int numLinks();
	
	public Segment getLink(int i);
	
	public void addLink(Segment s);
	
	void addLink(Segment s, int idx);
	
	boolean removeLink(Segment s);
	
	public Segment removeLink(int i);

	public int nodeType();
	
	public PosConstraint getPosConstraint();
	
	@Override
	default void onPathEvent(PathEvent e) {}
	/*
	public int numOutSegments();
	
	public Segment getOutSegment(int i);
	
	public Segment getOutSegment();
	
	public Segment getInSegment();
	
	*/

	

	
	
}
