package gui.paths;

import static gui.paths.PathEventEnum.PATHOBJECT_ADD;
import static gui.paths.PathEventEnum.PATHOBJECT_REMOVE;
import static gui.paths.PathEventEnum.SEGMENT_ADD;
import static gui.paths.PathEventEnum.SEGMENT_REMOVE;

public class PathEvent {

	public final static boolean ADD = true, REMOVE = false;

	public final PathEventEnum type;
	public final PosConstraint constraint;
	
	public PathEvent(PathEventEnum type, PosConstraint constraint) {
		
		this.type = type;
		this.constraint = constraint;
	}

	public static class NodeAddRemove extends PathEvent {
		
		public final Node node;
		
		public NodeAddRemove(boolean addRemove, Node node) {
			
			super(addRemove ? PathEventEnum.NODE_CREATE : PathEventEnum.NODE_REMOVE, node.getPosConstraint());
			
			this.node = node;
		}
	}
	
	public static class NodeConnectDisconnect extends PathEvent {
		
		public final Path path;
		public final Node node1, node2;
		
		public NodeConnectDisconnect(boolean addRemove, Node node1, Node node2, Path path) {
			
			super(addRemove ? PathEventEnum.NODE_CONNECT : PathEventEnum.NODE_DISCONNECT, path.getPosConstraint());
			
			this.node1 = node1;
			this.node2 = node2;
			this.path = path;
		}
	}
	
	public static class PathAddRemove extends PathEvent {
		
		public final Path path;
		
		public PathAddRemove(boolean addRemove, Path path) {
			
			super(addRemove ? PathEventEnum.PATH_CREATE : PathEventEnum.PATH_DELETE, path.getPosConstraint());
			
			this.path = path;
		}
	}
	
	public static class SegmentAddRemove extends PathEvent {
		
		public final Path path;
		public final Segment segment;
		
		public SegmentAddRemove(boolean addRemove, Path path, Segment segment) {
			
			super(addRemove ? SEGMENT_ADD : SEGMENT_REMOVE, path.getPosConstraint());
			
			this.segment = segment;
			this.path = path;
		}
	}
	
	
	public static class PathObjectAddRemove extends PathEvent {
		
		public final Path path;
		public final PathObject pathObject;
		
		public PathObjectAddRemove(boolean addRemove, PathObject pathObject, Path path) {
			
			super(addRemove ? PATHOBJECT_ADD : PATHOBJECT_REMOVE, path.getPosConstraint());
			
			this.path = path;
			this.pathObject = pathObject;
		}
	}
	
	public static class UpdateLengthEvent extends PathEvent {
		
		public final Segment segment;
		
		public UpdateLengthEvent(Segment segment) {
		
			super(PathEventEnum.UPDATE_LENGTH, segment.getPosConstraint());
			this.segment = segment;
		}
	}
	
	public static class UpdateObjectsEvent extends PathEvent {
		
		public final Segment segment;
		
		public UpdateObjectsEvent(Segment segment) {
		
			super(PathEventEnum.UPDATE_OBJECTS, segment.getPosConstraint());
			this.segment = segment;
		}
	}
	
	public static class PathSplit extends PathEvent {

		public final Path originalPath, path1, path2;
		public final double originalLength, splitPos;
		
		public PathSplit(Path originalPath, double originalLength, Path path1, Path path2, double splitPos) {
			
			super(PathEventEnum.PATH_SPLIT, path1.getPosConstraint());
			
			this.originalPath = originalPath;
			this.path1 = path1;
			this.path2 = path2;
			this.originalLength = originalLength;
			this.splitPos = splitPos;
		}

		
		
	}
	
	/**
	 * Eventobject for general purpose. 
	 *
	 */
	public static class Message extends PathEvent {
		
		public final String name;
		public final Object message;
	
		public Message(PosConstraint constraint, String name, Object message) {
			super(PathEventEnum.MESSAGE, constraint);
			
			this.message = message;
			this.name = name;
		}
		
	}
	
 }


