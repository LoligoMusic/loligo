package gui.paths;

import javax.naming.OperationNotSupportedException;

import gui.path.PositionableObject;


public class Vertex extends PositionableObject implements Node{

	private Segment segment1, segment2; //  no in/out order!!
	private final PosConstraint posConstraint;
	
	public Vertex(PosConstraint posConstraint) {
	
		this.posConstraint = posConstraint;
	}
	
	
	@Override
	public void updateSegments() {
		
		int n = numLinks();
		
		for (int i = 0; i < n; i++) 
			
			getLink(i).rootSegment().updateLength();
		
	}
	
	
	@Override
	public int numLinks() {
		
		return (segment1 != null ? 1 : 0) + 
			   (segment2 != null ? 1 : 0);
	}
	
	
	@Override
	public Segment getLink(int i) {
		
		int n = numLinks();
		
		if(i < 0 || i >= n)
			throw new IndexOutOfBoundsException(Integer.toString(i));
		
		return 
				n == 1 ? (segment1 != null ? segment1 : segment2) :
						  i == 0 			? segment1 : segment2;
	}

	
	@Override
	public void addLink(Segment s) {
		
		if(segment1 == null && segment2 != s) {
			segment1 = s;
			//setPosObject(new Position.PositionOffset(segment1));
		}
		
		else
		if(segment2 == null && segment1 != s) 
			segment2 = s;
		
		else
			throw new RuntimeException(new OperationNotSupportedException());
	}
	
	@Override
	public boolean removeLink(Segment s) {
		
		if(segment1 == s) {
			
			segment1 = null;
			return true;
		}else
		if(segment2 == s) {
			
			segment2 = null;
			return true;
		}
		
		return false;
	}

	@Override
	public Segment removeLink(int i) {
		
		Segment s = null;
		
		if(i == 0) {
			
			s = segment1;
			segment1 = null;
		}else 
		if(i == 1) {
			
			s = segment2;
			segment2 = null;
		}else
			throw new IndexOutOfBoundsException();
		
		return s;
	}
	
	@Override
	public PosConstraint getPosConstraint() {
	
		return posConstraint;
	}

	@Override
	public int nodeType() {
		return VERTEX;
	}


	@Override
	public void addLink(Segment s, int idx) {
		// TODO Auto-generated method stub
		
	}

}
