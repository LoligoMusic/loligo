package gui.paths;

public final class Nodes {

	private Nodes() {}
	
	public static void removeLink(Node n, Segment s) {
		
		n.removeLink(indexOf(n, s));
	}
	
	public static int indexOf(Node n, Segment s) {
		
		for (int i = 0; i < n.numLinks(); i++) {
			
			if(n.getLink(i) == s)
				return i;
		}
		
		return -1;
	}
	
	public static boolean contains(Node n, Segment s) {
		
		return indexOf(n, s) != -1;
	}
	
	public static void disconnectAll(Node n) {
		
		for (int i = 0; i < n.numLinks(); i++)
			n.removeLink(0);
	}
	

}
