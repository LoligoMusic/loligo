package gui.paths;

import gui.paths.PathMementoFactory.SegmentMemento;

public abstract class AbstractSegment implements Segment {

	final private PosConstraint posConstraint;
	final private Node in, out;
	private Path path;
	private double length, offset;
	
	
	public AbstractSegment(PosConstraint posConstraint, Path path, Node in, Node out) {
	
		this.path = path;
		this.posConstraint = posConstraint;
		
		this.in = in;
		this.out = out;
	}

	@Override
	public void init() {
	
		in.addLink(this);
		out.addLink(this);
	}
	
	@Override
	public double getParentOffset() {
		
		return offset;
	}
	
	@Override
	public void setParentOffset(double offset) {
	
		this.offset = offset;
	}
	
	@Override
	public Node getNodeIn() {
		
		return in;
	}

	@Override
	public Node getNodeOut() {
		
		return out;
	}
	
	@Override
	public double getLength() {
		
		return length;
	}
	
	
	protected void setLength(double length) {
		
		this.length = length;
	}
	
	@Override
	public SegmentPair split(double pos, Node node) {
	
		return Segments.splitSegment(this, node, pos);
	}
	
	@Override
	public Path getPath() {
		
		return path;
	}
	
	@Override
	public void setPath(Path p) {
	
		path = p;
	}
	
	@Override
	public Segment getSuperSegment() {
		
		return path;
	}
	
	@Override
	public Path rootSegment() {
		
		return Segments.rootSegment(this);
		
		//return getPath() == null || getPath() == this ? this : getPath().rootSegment();
	}
	
	@Override
	public Segment lowestSegmentAt(double fac) {
		
		return this;
	}
	
	
	@Override
	public double ToParentPos(double p) {
		
		double d = Segments.toParentPos(p, this);
		
		Segment ss = getSuperSegment();
		
		return ss == null ? d : getSuperSegment().ToParentPos(d);
	}
	
	@Override
	public PosConstraint getPosConstraint() {
	
		return posConstraint;
	}
	
	@Override
	public SegmentMemento toMemento(PathMementoFactory pmf) {
		
		SegmentMemento sm = new SegmentMemento( getSegmentType(), 
												pmf.findVertexMemento(getNodeIn()), 
												pmf.findVertexMemento(getNodeOut()));
		
		return sm;
	}
	
	public void restoreMemento(SegmentMemento sm, PathMementoFactory pmf) {
		
		
	}
	
	/*
	@Override
	public float getX() {
		
		return path.getX();
	}
	
	@Override
	public float getY() {
	
		return path.getY();
	}
	
	@Override
	public void setPos(float x, float y) {
	
		path.setPos(x, y);
	}
	
	@Override
	public void setPosObject(Position p) {
	
		path.setPosObject(p);
	}
	
	@Override
	public Position getPosObject() {
	
		return path.getPosObject();
	}
	*/
}
