package gui.paths;

import gui.path.PositionableObject;
import pr.Positionable;

public class NullPathObject extends PositionableObject implements PathObject {

	private double pathPos = 0, tangent;
	private Path path;
	
	public NullPathObject() {
		
	}

	public NullPathObject(Positionable p) {
		
		super(p);
	}

	
	@Override
	public void updatePosition() {
		
	}

	@Override
	public Path getSegment() {
	
		return path;
	}

	@Override
	public void setSegment(Path path) {
		
		this.path = path;
	}

	@Override
	public double getPathPosition() {
		
		return pathPos;
	}

	@Override
	public void goToPathPosition(double d) {

		pathPos = d;
	}

	@Override
	public void setPath(Path path) {
		
		this.path = path;
	}

	@Override
	public Path getPath() {
		
		return path;
	}

	@Override
	public double getTangent() {
		
		return tangent;
	}

	@Override
	public void setTanget(double tangent) {
		
		this.tangent = tangent;
	}

	@Override
	public void onPathEvent(PathEvent e) {
		
	}

}
