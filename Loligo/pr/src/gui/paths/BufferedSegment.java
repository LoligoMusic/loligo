package gui.paths;

public abstract class BufferedSegment extends AbstractSegment {

	public SubVertex[] vertices;
	
	
	public BufferedSegment(PosConstraint posConstraint, Path path, Node in, Node out) {
		
		super(posConstraint, path, in, out);
	}
	
	/**
	 * Generates SubVertex positions relative to NodeIn
	 */
	abstract public void updateVertexBuffer();
	
	public SubVertex[] getSubVertices(){
		
		return vertices;
	}
	
	
	public void setSubVertices(SubVertex[] verts) {
		
		vertices = verts;
	}
	
	public void setNumVertices(int len) {
		
		if(vertices == null) {
			
			vertices = new SubVertex[len];
			
			for (int i = 0; i < len; i++) 
				vertices[i] = new SubVertex();
		
		}else
		if(vertices.length != len) {
			
			SubVertex[] arr = new SubVertex[len];
			
			updateLength();
			/*
			if(len > vertices.length) {
				
				System.arraycopy(vertices, 0, arr, 0, vertices.length);
				
				for (int i = vertices.length; i < arr.length; i++) 
					arr[i] = new SubVertex();
			
			}else{
				
				System.arraycopy(vertices, 0, arr, 0, arr.length);
			}
			*/
			
			setSubVertices(arr);
		}
	}
	
	
	@Override
	public void goToPos(PathObject po, double fac) {
		
		Segments.checkNormalized(fac);
		
		SubVertex v1 = null, v2 = null;
		double ff = fac * getLength();
		int i = vertices.length / 2, j = i;
		
		while(i >= 0 && i + 1 < vertices.length) {
			
			v1 = vertices[i];
			v2 = vertices[i + 1];
			
			if(ff >= v1.pos && ff <= v2.pos) 
				break;
			
			j = j > 2 ? j / 2 : 1;
			i += (ff > v1.pos ? j : -j);
		}
		
		if(v2 != null) {
		
			double f = (ff - v1.pos ) / v1.length;
			
			Segments.linearPos(	po,
								v1.x, // TODO figure out offsets..
								v1.y, 
								v2.x - v1.x, 
								v2.y - v1.y, 
								f);
			
			po.setTanget(v1.tangent + (v2.tangent - v1.tangent) * f);
		}
	}
	
	
	@Override
	public double nearestPoint(float x, float y) {
		
		x -= getPosConstraint().getX();
		y -= getPosConstraint().getY();
		
		float dd = Float.MAX_VALUE;
			 
		SubVertex v1 = null, 
			      v2 = null,
			      vv = null;
		
		float pos = 0;
		
		for (int i = 1; i < vertices.length; i++) {
			
			v1 = vertices[i - 1];
			v2 = vertices[i];
			
			float p = Segments.perpendicularPoint(v1.x, v1.y, v2.x - v1.x, v2.y - v1.y, x, y);
			
			float dx = Segments.lerp(v1.x, v2.x - v1.x, p) - x,
				  dy = Segments.lerp(v1.y, v2.y - v1.y, p) - y;
			
			float d = dx * dx + dy * dy;
			
			if(d < dd) {
				vv = v1;
				pos = p;
				dd = d;
			}
		}
		
		pos = (float) ((vv.pos + pos * vv.length) / getLength());
		
		pos = pos >= 1 ? 1 :
			  pos <= 0 ? 0 :
						 pos;
		
		return pos; 
	}
			
	
	@Override
	public boolean hitTest(float x, float y) {
		
		x -= getPosConstraint().getX();
		y -= getPosConstraint().getY();
		
		for (int i = 1; i < vertices.length; i++) {
			
			SubVertex v1 = vertices[i - 1],
					  v2 = vertices[i];
			
			if( Segments.hitTest(
								 v1.x, v1.y, 
								 v2.x - v1.x, 
								 v2.y - v1.y, 
								 x, y)) {
				
				return true;
			}
		}
		
		return false;
	}
	

	@Override
	public double updateLength() {
		
		updateVertexBuffer();
		
		SubVertex v1 = null, v2 = null;
		double len = 0;
		float dx, dy;
		
		for (int i = 1; i < vertices.length; i++) {
			
			v1 = vertices[i - 1];
			v2 = vertices[i];
			
			dx = v2.x - v1.x;
			dy = v2.y - v1.y;
			
			v1.length = Math.sqrt(dx * dx + dy * dy);
			len += v1.length;
			v2.pos = len;
			v1.tangent = Math.atan2(dy, dx);
		}
		
		v2.tangent = v1.tangent;
		setLength(len);
		return len;
	}
	
	
	
	public class SubVertex {
		
		public float x, y;
		double length, pos, tangent;
		
	}

}
