package gui.paths;

import lombok.Getter;
import patch.Loligo;
import pr.RootClass;

public class CurveSegment extends BufferedSegment {

	private static final int NUM_VERTICES = 50;
	
	@Getter
	private float curveTightness = 0; // 0 = squishy, 1 = linear
	
	
	public CurveSegment(PosConstraint posConstraint, Path path, Node in, Node out) {
		
		super(posConstraint, path, in, out);
		
		setNumVertices(NUM_VERTICES);
	}

	public void setCurveTightness(float t) {
		curveTightness = t;
		updateVertexBuffer();
	}
	
	@Override
	public void updateVertexBuffer() {
		verts();
	}
	
	public Node getControlPoint1() {
		Node in  = getNodeIn();
		return 
		   in.numLinks() <= 1 	 ? in :
		   in.getLink(0) != this ? in.getLink(0).getNodeIn(): 
			   					   in.getLink(1).getNodeIn();
	}
	
	public Node getControlPoint2() {
		Node out = getNodeOut();
		return out.numLinks() <= 1 	  ? out :
			   out.getLink(0) != this ? out.getLink(0).getNodeOut(): 
										out.getLink(1).getNodeOut();
	}
	
	private void verts() {
		
		Node in  = getNodeIn(),
			 out = getNodeOut(),
			 cp1 = getControlPoint1(),
			 cp2 = getControlPoint2(); 
		
		float cx  = getPosConstraint().getX(),
			  cy  = getPosConstraint().getY(),
			  inx = in.getX()  - cx,
			  iny = in.getY()  - cy,
			  oux = out.getX() - cx,
			  ouy = out.getY() - cy,
			  c1x = cp1.getX() - cx,
			  c1y = cp1.getY() - cy,
			  c2x = cp2.getX() - cx,
			  c2y = cp2.getY() - cy;
		
		SubVertex[] arr = getSubVertices();
		Loligo p = RootClass.mainProc();
		p.curveTightness(curveTightness);
		
		for (int i = 0; i < arr.length; i++) {
			
			float t = i / (float)(arr.length - 1);
			SubVertex v = arr[i];
			// TODO: no Processing dependencies! 
			v.x = p.curvePoint(c1x, inx, oux, c2x, t);
			v.y = p.curvePoint(c1y, iny, ouy, c2y, t);
		}
	}
	
	@Override
	public SegmentType getSegmentType() {
		return SegmentType.CURVE;
	}

}
