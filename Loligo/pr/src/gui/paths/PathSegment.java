package gui.paths;

import java.util.ArrayList;
import java.util.List;

import gui.paths.PathMementoFactory.SegmentMemento;


public class PathSegment extends AbstractSegment implements Path {

	private final List<Segment> segments = new ArrayList<>();
	
	//private Position pos = new Position();
	
	
	//private Node nodeIn, nodeOut;
	
	/*
	public Path(Path path, Node in, Node out) {
		
		super(path, in, out);
		
		setNodeIn(in);
		setNodeOut(out);
	}
	*/
	
	public PathSegment(PosConstraint posConstraint, Path path) {
		
		super(posConstraint, path, null, null);
	}
	
	public PathSegment(PosConstraint posConstraint) {
		
		super(posConstraint, null, null, null);
	}
	
	
	@Override
	public void init() {
	
	}
	
	public void addSegments(List<Segment> segments) {
		
		segments.addAll(segments);
		
		for (Segment s : segments) {
			s.setPath(this);
			PathEventManager.instance().dispatchSegmentAddEvent(s, this);
		}
		updateLength();
	}
	
	@Override
	public void addSegment(int index, Segment s) { // TODO looks broken
		
		if(index < 0 || index > segments.size())
			throw new IndexOutOfBoundsException(
					"index: " + index + " - size: " + segments.size());
		
		//Node n = segments.get(index).getNodeIn();
		
		if(index > 0) {
			
			//Segment pre = n.getLink(0);
			//Node v1 = new NodeVertex(getPosConstraint());
			
			
			//v1.setPosObject(new Position.PositionOffset(this));
			
			//pre.setNodeOut(v1);
			//s.setNodeIn(v1);
		
		}else{
		 
			//Vertex v1 = new Vertex();
			
			//s.setNodeIn(nodeIn);
			
		}
		
		if(index < segments.size()) {
			
			//Segment post = n.getLink(1);
			//Vertex v2 = new Vertex();
			//v2.setPosObject(new Position.PositionOffset(this));
			//s.setNodeOut(v2);
			//post.setNodeOut(v2);
		
		}else{
			 
			//s.setNodeOut(nodeOut);
		}
		
		s.setPath(this);
		
		segments.add(index, s);
		
		updateLength();
		
		PathEventManager.instance().dispatchSegmentAddEvent(s, this);
		
	}
	
	
	@Override
	public int numSegments() {
		
		return segments.size();
	}
	
	
	@Override
	public Segment getSegment(int index) {
		
		return segments.get(index);
	}


	@Override
	public void removeSegment(int index) {
		
		Segment s = segments.remove(index);
		s.setPath(null);
		s.getNodeIn().removeLink(s);
		s.getNodeOut().removeLink(s);
		
		PathEventManager.instance().dispatchSegmentRemoveEvent(s, this);
	}
	
	@Override
	public boolean removeSegment(Segment s) {
		
		int i = segments.indexOf(s);
		removeSegment(i);
		return i >= 0;
	}
	

	@Override
	public void goToPos(PathObject po, double fac) {
		
		Segments.checkNormalized(fac);
		
		double len = 0,
			   mult = getLength() * fac;
		
		Segment seg = null;
		
		for (Segment s : segments) {
			
			len = s.getParentOffset() + s.getLength();
			
			if(mult <= len) {
				
				seg = s;
				break;
			}
		}
		 
		if(seg != null) {
			
			double le = Segments.toChildPos(fac, this, seg);
			
			seg.goToPos(po, le);
		}
	}

	
	@Override
	public double nearestPoint(float x, float y) {
		
		double d = Double.MAX_VALUE;
		double nearest = 0;
		Segment seg = null;
		
		PathObject po = new NullPathObject();
		float dx, dy, dd;
		
		for (Segment s : segments) {
			
			double p = s.nearestPoint(x, y);
			s.goToPos(po, p);
			dx = po.getX() + getPosConstraint().getX() - x;
			dy = po.getY() + getPosConstraint().getY() - y;
			dd = dx * dx + dy * dy;
			
			if(dd < d) {
				d = dd;
			 	nearest = p;
			 	seg = s;
			}
		}
		
		return seg == null ? 0 : Segments.toParentPos(nearest, seg);
	}
	
	
	@Override
	public boolean hitTest(float x, float y) {
		
		for (Segment s : segments) {
			
			if(s.hitTest(x, y))
				return true;
		}
		
		return false;
	}
	
	
	@Override
	public Node getNodeIn() {
	
		return segments.isEmpty() ? null : segments.get(0).getNodeIn();
	}
	
	
	@Override
	public Node getNodeOut() {
	
		return segments.isEmpty() ? null : segments.get(segments.size() - 1).getNodeOut();
	}
	
	
	@Override
	public int indexOf(Segment s) {
		
		return segments.indexOf(s);
	}
	
	
	@Override
	public double updateLength() {
		
		double len = 0;
		
		for (Segment s : segments) {
			
			s.setParentOffset(len);
			s.updateLength();
			len += s.getLength();
		}
		
		setLength(len);
		
		PathEventManager.instance().dispatchUpdateObjectsEvent(this);
		
		return len;
	}


	@Override
	public SegmentType getSegmentType() {
		
		return SegmentType.PATH;
	}
	
	
	@Override
	public SegmentPair split(double pos, Node node) {
		
		Segments.checkNormalized(pos);
		
		if(pos == 0 || pos == 1)
			return null;
		
		NullPathObject po = new NullPathObject();
		
		goToPos(po, pos);
		
		int idx = findSegmentAt(pos);
		
		Segment sub = segments.get(idx);
		
		SegmentPair pairSub = sub.split(Segments.toChildPos(pos, this, sub), node);
		
		
		List<Segment> segs1 = idx > 0 ? new ArrayList<>(segments.subList(0, idx)) :
										new ArrayList<>();
		segs1.add(pairSub.getSegment1());
		
		PathSegment path1 = new PathSegment(getPosConstraint());
		path1.segments.addAll(segs1);
		path1.updateLength();
		
		
		List<Segment> segs2 = new ArrayList<>();//);
		segs2.add(pairSub.getSegment2());
		if(idx + 1 < segments.size())
			segs2.addAll(segments.subList(idx + 1, segments.size()));
		
		PathSegment path2 = new PathSegment(getPosConstraint());
		path2.segments.addAll(segs2);
		path2.updateLength();
		
		
		return new SegmentPair(path1, path2);
		
		/*
		List<Segment> newSegs = new ArrayList<>();
		newSegs.add(rest);
		
		if(idx < segments.size() - 1) {
			
			List<Segment> rightSegs = segments.subList(idx + 1, segments.size() - 1);
			newSegs.addAll(rightSegs);
			rightSegs.clear();
		}
		
		PosConstraint cons = getPosConstraint();
		
		PathSegment newPath = new PathSegment(cons);
		
		//Node nv = rest.getNodeIn();
		//nv.addLink(this);
		//nv.addLink(newPath);
		
		//nv.setPos(po.getX(), po.getY());
		
		//Segment ls = new LinearSegment(getPosConstraint(), newPath, nv, newSegs.get(0).getNodeIn());
		//newSegs.add(0, ls);
		
		newPath.segments.addAll(newSegs);
		
		newSegs.forEach(newPath::registerChild);
		
		cons.addPath(newPath);
		
		updateLength();
		newPath.updateLength();
		
		cons.dispatchEvent(new PathEvent.PathSplit(this, oLen, this, newPath, pos));
		return newPath;
		*/
		
	}
	

	protected Node nearestVertex(double pos) {
		
		Segments.checkNormalized(pos);
		
		double d = pos;
		Node v = getNodeIn();
		
		for (Segment s : segments) {
			
			double dd = pos - (s.getParentOffset() + s.getLength());
			
			if(dd < d) {
				
				d = dd;
				v = s.getNodeOut();
			}else
				break;
		}
		
		return v;
	}
	
	@Override
	public Segment lowestSegmentAt(double fac) {
	
		int i = findSegmentAt(fac);
		
		return i >= 0 ? segments.get(i) : null;
	}
	
	protected int findSegmentAt(double pos) {
		
		Segments.checkNormalized(pos);
		
		double ab = pos * getLength();
		
		for (int i = segments.size() - 1; i >= 0; i--) 
			
			if(ab >= segments.get(i).getParentOffset()) 
				
				return i;
				
		return -1;
	}

	@Override
	public SegmentMemento toMemento(PathMementoFactory pmf) {
		
		SegmentMemento sm = super.toMemento(pmf);
		
		SegmentMemento[] sms = segments.stream().map(s -> s.toMemento(pmf)).toArray(SegmentMemento[]::new);
		sm.setSubSegments(sms);
		
		return sm;
	}

	@Override
	public void restoreMemento(SegmentMemento sm, PathMementoFactory pmf) {
		
		for(SegmentMemento s : sm.getSubSegments()) {
			
			Segment ss = getPosConstraint().createSegment(
												s.vertex1.node, 
												s.vertex2.node, 
												s.segmentType);
			
			addSegment(numSegments(), ss);
			ss.restoreMemento(s, pmf);
		}
		
		updateLength();
	}
	
	/*
	@Override
	public float getX() {
	
		return pos.getX();
	}
	
	
	@Override
	public float getY() {
	
		return pos.getY();
	}
	
	
	@Override
	public void setPos(float x, float y) {
		
		pos.setPos(x, y);
	}
	
	
	@Override
	public void setPosObject(Position p) {
		
		pos = p;
	}
	
	
	@Override
	public Position getPosObject() {
	
		return pos;
	}
	*/
}
