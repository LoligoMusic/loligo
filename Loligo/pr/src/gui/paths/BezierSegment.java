package gui.paths;

import static gui.paths.Segments.lerp2;

import gui.Position;
import gui.paths.PathMementoFactory.SegmentMemento;
import pr.Positionable;
import pr.RootClass;

public class BezierSegment extends BufferedSegment {

	private static final int NUM_VERTICES = 50;
	
	private Positionable controlPoint1, controlPoint2;
	
	
	public BezierSegment(PosConstraint posConstraint, Path path, Node in, Node out) {
		
		super(posConstraint, path, in, out);
		
		controlPoint1 = newControlPoint(in);
		controlPoint2 = newControlPoint(out);
		
		setNumVertices(NUM_VERTICES);
	}

	
	@Override
	public void updateVertexBuffer() {
		
		verts();
	}
	
	private void verts() {
		
		Node in  = getNodeIn(),
			 out = getNodeOut();
		
		SubVertex[] arr = getSubVertices();
		
		float cx = getPosConstraint().getX(),
			  cy = getPosConstraint().getY(),
			  sx = in.getX() - cx,
			  sy = in.getY() - cy;
		 
		
		for (int i = 0; i < arr.length; i++) {
			
			float t = i / (float)(arr.length - 1);
			
			SubVertex v = arr[i];
			
			// TODO: no Processing dependencies! 
			v.x = RootClass.mainProc().bezierPoint(
					sx, 
					controlPoint1.getX() - cx, 
					controlPoint2.getX() - cx, 
					out.getX() - cx, 
					t);
			
			v.y = RootClass.mainProc().bezierPoint(
					sy, 
					controlPoint1.getY() - cy, 
					controlPoint2.getY() - cy, 
					out.getY() - cy, 
					t);
		}
		
	}
	
	public Positionable newControlPoint(Node n) {
		
		return new Position.PositionOffset(n);
	}
	
	@Override
	public SegmentPair split(double pos, Node node) {
		
		SegmentPair p = super.split(pos, node);
		
		//https://i.stack.imgur.com/I9wKC.png
		
		float v1x = getNodeIn().getX(),
			  v1y = getNodeIn().getY(),
			  v2x = getNodeOut().getX(),
			  v2y = getNodeOut().getY();
		float cx1 = controlPoint1.getX(),
			  cy1 = controlPoint1.getY(),
			  cx2 = controlPoint2.getX(),
			  cy2 = controlPoint2.getY();
		
		BezierSegment s1 = (BezierSegment) p.getSegment1(),
					  s2 = (BezierSegment) p.getSegment2();
		
		float f = (float) pos;
		
		float mx = lerp2(cx1, cx2, f),
			  my = lerp2(cy1, cy2, f);
		
		float vcx1 = lerp2(v1x, cx1, f),
			  vcy1 = lerp2(v1y, cy1, f),
			  vcx2 = lerp2(cx2, v2x, f),
			  vcy2 = lerp2(cy2, v2y, f);
		
		s1.controlPoint1.setPos(vcx1 - v1x, vcy1 - v1y);
		s2.controlPoint2.setPos(vcx2 - v2x, vcy2 - v2y);
		
		s1.controlPoint2.setPos(
			lerp2(vcx1, mx, f) - node.getX(),
			lerp2(vcy1, my, f) - node.getY()
		);
		s2.controlPoint1.setPos(
			lerp2(mx, vcx2, f) - node.getX(),
			lerp2(my, vcy2, f) - node.getY() 
		);
		
		return p;
	}
	
	@Override
	public double updateLength() {
		// TODO Optimized implementation
		return super.updateLength();
	}
	
	public Positionable getControlPoint1() {
		return controlPoint1;
	}

	public Positionable getControlPoint2() {
		return controlPoint2;
	}
	
	@Override
	public SegmentType getSegmentType() {
		
		return SegmentType.BEZIER;
	}


	@Override
	public SegmentMemento toMemento(PathMementoFactory pmf) {
		
		SegmentMemento sm = super.toMemento(pmf);
		
		sm.setControlPoints(
				controlPoint1.getX() - getNodeIn().getX(), 
				controlPoint1.getY() - getNodeIn().getY(), 
				controlPoint2.getX() - getNodeOut().getX(), 
				controlPoint2.getY() - getNodeOut().getY()); 
		
		return sm;
	}


	@Override
	public void restoreMemento(SegmentMemento sm, PathMementoFactory pmf) {
		
		super.restoreMemento(sm, pmf);
		
		float[] cp = sm.getControlPoints();
		
		controlPoint1.setPos(cp[0], cp[1]);
		controlPoint2.setPos(cp[2], cp[3]);
		
	}
	
}
