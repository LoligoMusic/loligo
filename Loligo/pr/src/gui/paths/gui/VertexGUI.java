package gui.paths.gui;

import static constants.DisplayFlag.ROUND;
import static constants.DisplayFlag.SUBSCRIBE_ON_SELECT;
import static constants.InputEvent.DRAG;
import static constants.InputEvent.DRAG_STOP;
import static constants.DisplayFlag.CLICKABLE;
import static constants.DisplayFlag.DRAGABLE;
import static constants.DisplayFlag.DROPABLE;

import java.util.List;

import com.jogamp.newt.event.KeyEvent;

import constants.InputEvent;
import gui.Position;
import gui.paths.Node;
import gui.paths.Path;
import gui.selection.GuiType;
import lombok.Getter;
import pr.DisplayObject;
import pr.userinput.UserInput;

public class VertexGUI extends DisplayObject {

	@Getter
	private final Node node;
	
	public VertexGUI(Node node) {
		
		setFlag(ROUND, true);
		setFlag(CLICKABLE, true);
		setFlag(DRAGABLE, true);
		setFlag(DROPABLE, true);
		setGuiType(GuiType.DRAGABLE_FREE);
		setColor(0xff00aabb);
		
		setWH(8, 8);
		
		this.node = node;
		
		setPosObject(new Position.PositionTarget(node));
		
		setFlag(SUBSCRIBE_ON_SELECT, true);
	}
	 
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(e == InputEvent.KEY && input.getLastKeyCode() == KeyEvent.VK_DELETE) {
			
			List<Path> ps = node.getPosConstraint().getPaths();
			if(ps != null && !ps.isEmpty() && ps.get(0).numSegments() > 1)
				node.getPosConstraint().removeNode(node);
		}
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(e == DRAG || e == DRAG_STOP)
			node.getPosConstraint().updatePosConstraint();
	}
	
}
