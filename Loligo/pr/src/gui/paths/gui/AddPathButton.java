package gui.paths.gui;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayObjects;
import gui.SnapBehaviour;
import gui.paths.Node;
import gui.paths.Path;
import gui.paths.PathObject;
import gui.paths.PosConstraint;
import gui.paths.SegmentType;
import gui.selection.GuiType;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;


class AddPathButton extends DisplayObject {
	
	private final VertexGUI vertexGui;
	
	public AddPathButton(VertexGUI vertexGui) {
		
		this.vertexGui = vertexGui;
		setGuiType(GuiType.DRAGABLE_FREE);
		setFlag(DisplayFlag.CLICKABLE, true);
		setFlag(DisplayFlag.DRAGABLE, true);
		setFlag(DisplayFlag.fixedToParent, true);
		setPos(15, 0);
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == InputEvent.DRAG_START) {
			//MouseEventContext.instance.addListener(this);
			
			SnapBehaviour.SnapToObjects<Node> sb = new SnapBehaviour.SnapToObjects<Node>(this);
			
			sb.addDropConsumer(this::createPath);
			
			sb.setSnapPoints(vertexGui.getNode().getPosConstraint().getNodes());
			sb.setAutoRemove(true);
			
			SnapBehaviour.SnapToPath sp = new SnapBehaviour.SnapToPath(this, vertexGui.getNode().getPosConstraint());
			sp.addDropConsumer(this::splitPath);
			sp.addEmptyDropConsumer(this::createPath);
			
			sb.addSnapBehaviour(sp);
			
			getDm().getInput().addInputEventListener(sb);
		}
	}
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(e == InputEvent.DRAG_STOP) {
			
			input.removeInputEventListener(this); //TODO blocks drag_stop in snapbehaviour?
			//createPath(null);
			DisplayObjects.removeFromDisplay(this);
		}else
		if(e == InputEvent.DROPPED) {
			
			input.removeInputEventListener(this);
			DisplayObjects.removeFromDisplay(this);
			
			DisplayObjectIF d = input.getDropTarget();
			
			if(d instanceof VertexGUI && d != this) {
				
				//VertexGUI vg = (VertexGUI) d;
				//createPath(vg.getNode());
			}
		}else
		if(e == InputEvent.PRESSED && input.getPressedTarget() == null) {
			
			DisplayObjects.removeFromDisplay(this);
		}
	}
	
	void createPath(float x, float y) {
		
		PosConstraint ptc = vertexGui.getNode().getPosConstraint();
		Node n = ptc.newNode();
		n.setPos(x - ptc.getX(), y - ptc.getY());
		
		createPath(n);
	}
	
	void createPath(Node n2) {
		
		if(n2 == null)
			throw new NullPointerException();
		
		PosConstraint ptc = vertexGui.getNode().getPosConstraint();
		
		ptc.addPath(vertexGui.getNode(), n2, SegmentType.BEZIER);
	}
	
	void splitPath(PathObject no) {
		
		Path p = no.getPath();
		Path p2 = (Path) p.split(no.getPathPosition(), null);
		if(p.getNodeIn() != vertexGui.getNode() && p2.getNodeOut() != vertexGui.getNode()) {
			
			createPath(p.getNodeOut());
		}
	}
}

