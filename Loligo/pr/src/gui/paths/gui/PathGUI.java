package gui.paths.gui;

import java.util.ArrayList;
import java.util.List;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.paths.PathEvent;
import gui.paths.PathEventEnum;
import gui.paths.PathListener;
import gui.paths.PathSegment;
import gui.paths.Segment;
import gui.paths.Segments;
import lombok.var;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;

public class PathGUI extends SegmentGUI implements PathListener{

	private final PathSegment path;
	private final List<SegmentGUI> segmentGUIs = new ArrayList<SegmentGUI>(); 
	private final List<VertexGUI> vertexGUIs = new ArrayList<>();
	
	public PathGUI(PathSegment path) {
	
		this.path = path;
		
		setFlag(DisplayFlag.CLICKABLE, false);
		setFlag(DisplayFlag.DRAWABLE, false);
		
		for (int i = 0; i < path.numSegments(); i++) {
			
			Segment s = path.getSegment(i);
			createSegmentGUI(s);
		}
		
	}
	
	@Override
	public void initialize() {
	
		super.initialize();
		
		segmentGUIs.forEach(SegmentGUI::initialize);
	}
	
	@Override
	public void onPathEvent(PathEvent e) {
		
		if(e.type == PathEventEnum.SEGMENT_ADD) {
			
			PathEvent.SegmentAddRemove pe = (PathEvent.SegmentAddRemove) e;
			createSegmentGUI(pe.segment);
			
		}else
		if(e.type == PathEventEnum.SEGMENT_REMOVE) {
				
			PathEvent.SegmentAddRemove pe = (PathEvent.SegmentAddRemove) e;
			removeSegmentGUI(pe.segment);
		}
		
	}
	
	public void createSegmentGUI(Segment seg) {
		
		SegmentGUI sg = Segments.createGUI(seg);
		
		segmentGUIs.add(sg);
		
		//sg.setPosObject(new Position.PositionTarget(this));
		sg.setFlag(DisplayFlag.fixedToParent, true);
		addChild(sg);
		//createVertexGui(seg)
	}
	
	
	public void removeSegmentGUI(Segment seg) {
		
		for (int i = 0; i < segmentGUIs.size(); i++) {
			
			SegmentGUI sg = segmentGUIs.get(i);
			
			if(sg.getSegment() == seg) {
				
				segmentGUIs.remove(i);
				removeChild(sg);
				
				break;
			}
		}
	}
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(e.isDragEvent) {
			
			boolean b = false;
			
			for (SegmentGUI sg : segmentGUIs) {
				
				Segment s = sg.getSegment();
				
				DisplayObjectIF d = input.getDragTarget();
				
				if(
						s.hitTest(d.getX(), d.getY())) {
					b = true;
					break;
				}
			}
			
			if(b) {
				
			}
		}
	}


	public void forceUpdateGui() {
		
		for (int i = 0; i < path.numSegments(); i++) {
			var s = path.getSegment(i);
			createVertexGui(s);
		}
	}
	
	/*
	private void createVertexGui(Node n) {
		
	}
	*/

	private void createVertexGui(Segment s) {
			
		boolean noIn = true, noOut = true;
		
		for (VertexGUI vg : vertexGUIs) {
			
			if(noIn && vg.getNode() == s.getNodeIn()) {
				
				noIn = false;
			}
			if(noOut && vg.getNode() == s.getNodeOut()) {
				
				noOut = false;
			}
		}
		
		if(noIn) {
			VertexGUI vg = new VertexGUI(s.getNodeIn());
			vertexGUIs.add(vg);
			addChild(vg);
		}
		if(noOut) {
			VertexGUI vg = new VertexGUI(s.getNodeOut());
			vertexGUIs.add(vg);
			addChild(vg);
		}
		
		
		
	}

	@Override
	public Segment getSegment() {
		
		return path;
	}
	
	@Override
	public int setColor(int color) {
	
		for (SegmentGUI sg : segmentGUIs) 
			sg.setColor(color);
		
		return super.setColor(color);
	}
	
	@Override
	public void setWeight(float w) {
		for (SegmentGUI sg : segmentGUIs) 
			sg.setWeight(w);
		
		super.setWeight(w);
	}
	
}
