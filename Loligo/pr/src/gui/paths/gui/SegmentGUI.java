package gui.paths.gui;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayObjects;
import gui.Position;
import gui.paths.BezierSegment;
import gui.paths.BufferedSegment;
import gui.paths.BufferedSegment.SubVertex;
import gui.paths.CircularSegment;
import gui.paths.CurveSegment;
import gui.paths.LinearSegment;
import gui.paths.Node;
import gui.paths.Segment;
import gui.selection.GuiType;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import processing.core.PGraphics;



public abstract class SegmentGUI extends DisplayObject {
 
	@Setter@Getter
	protected float weight = 1;
	
	public SegmentGUI() {
		
		setFlag(DisplayFlag.CLICKABLE, false);
		setFlag(DisplayFlag.DROPABLE, true);
		
	}
	
	public abstract Segment getSegment();
	
	public void initialize() {
		
	}
	
	@Override
	public boolean hitTest(float mx, float my) {
	
		return getSegment().hitTest(mx, my);
	}
	
	
	
	public static class Bezier extends SegmentGUI {
	
		private final BezierSegment seg;
		private DisplayObjectIF cp1, cp2;
		
		public Bezier(BezierSegment seg) {
			
			this.seg = seg;
			
			initialize();
			setColor(0xffffffff);
			
		}
		
		@Override
		public void addedToDisplay() {
			getDm().getInput().addInputEventListener(this);
		}
		
		@Override
		public void removedFromDisplay() {
			getDm().getInput().removeInputEventListener(this);
		}
		
		@Override
		final public void initialize() {
			
			if(cp1 != null)
				DisplayObjects.removeFromDisplay(cp1);
			if(cp2 != null)
				DisplayObjects.removeFromDisplay(cp2);
			
			cp1 = new CPHandle(0);
			cp2 = new CPHandle(1);
			
			addChild(cp1, cp2);
			
			//cp1.setPos(10, 10);
			//cp2.setPos(10, 10);
		}
		
		public void setControlPointsVisible(boolean visible, int idx) {
			
			DisplayObjectIF c = idx == 0 ? cp1 : cp2;
			
			if(visible) {
				
				addChild(c);
			}else{
				
				removeChild(c);
			}
		}
		
		
		@Override
		public void mouseEvent(InputEvent e, UserInput input) {
			
			if(e == InputEvent.SELECT_ADD ) {
				
				int i = 0;
				for (Node n : new Node[]{seg.getNodeIn(), seg.getNodeOut()}) {
					
					VertexGUI vg = PosConstraintGUI.getNodeGui(n);
					
					if(input.selection.contains(vg)) {
						
						setControlPointsVisible(true, i);
					}
					i++;
				}
			}else
			if(e == InputEvent.SELECT_REMOVE) {
				
				/*
				if(input.selection.getSelection().stream().noneMatch(d -> d instanceof CPHandle)) {
					
					setControlPointsVisible(false, 0);
					setControlPointsVisible(false, 1);
				}
				*/
				
			}
		}

		@Override
		public void render(PGraphics g) {
		
			g.noFill();
			g.stroke(getColor());
			g.strokeWeight(weight);
			
			g.bezier(
					seg.getNodeIn().getX(), 		seg.getNodeIn().getY(),
					seg.getControlPoint1().getX(), 	seg.getControlPoint1().getY(), 
					seg.getControlPoint2().getX(), 	seg.getControlPoint2().getY(),
					seg.getNodeOut().getX(), 		seg.getNodeOut().getY());
			
			g.noStroke();
			
			/*
			for (BufferedSegment.SubVertex s : seg.vertices) {
				
				getDm().g.fill(0xffff0000);
				getDm().g.ellipse(s.x, s.y, 5, 5);
			}
			*/
		}
		
		@Override
		public Segment getSegment() {
			
			return seg;
		}
		
		class CPHandle extends DisplayObject {
			
			final Node node;
			
			public CPHandle(int segId) {
				
				if(segId == 0) {
					this.node = seg.getNodeIn();
					setPosObject(new Position.PositionTarget(seg.getControlPoint1()));
					
					/*
					setPos((seg.getNodeOut().getX() - seg.getNodeIn().getX()) / 5, 
						   (seg.getNodeOut().getY() - seg.getNodeIn().getY()) / 5
							);
					*/
				}else{
					this.node = seg.getNodeOut();
					setPosObject(new Position.PositionTarget(seg.getControlPoint2()));
					/*
					setPos((seg.getNodeIn().getX() - seg.getNodeOut().getX()) / 5, 
						   (seg.getNodeIn().getY() - seg.getNodeOut().getY()) / 5
								);
					*/
				}
				
				setFlag(DisplayFlag.ROUND, true);
				setFlag(DisplayFlag.CLICKABLE, true);
				setFlag(DisplayFlag.DRAGABLE, true);
				setGuiType(GuiType.DRAGABLE_FREE);
				setWH(8, 8);
				setColor(0xffaaaaff);
				
				//ModuleManager.assignConstraintGui((Constrainable) controlPoint, this);
			}
			
			@Override
			public void render(PGraphics g) {
			
				g.noFill();
				g.stroke(0xff999999);
				g.strokeWeight(.5f);
				g.line(getX(), getY(), node.getX(), node.getY());
				g.noStroke();
				super.render(g);
			}
			
			@Override
			public void mouseEventThis(InputEvent e, UserInput input) {
				if(e == InputEvent.DRAG)
					seg.rootSegment().updateLength();
			}
		}
	}
	
	public static class Curve extends SegmentGUI {
		
		private final CurveSegment seg;
		
		public Curve(CurveSegment seg) {
			
			this.seg = seg;
		}

		@Override
		public void render(PGraphics g) {
		
			g.noFill();
			g.stroke(0xffffffff);
			g.strokeWeight(weight);
			
			Node cp1 = seg.getControlPoint1(),
				 cp2 = seg.getControlPoint2();
			
			g.curveTightness(seg.getCurveTightness());
			g.curve(
				cp1.getX(), cp1.getY(),
				seg.getNodeIn().getX(),  seg.getNodeIn().getY(),
				seg.getNodeOut().getX(), seg.getNodeOut().getY(),
				cp2.getX(), cp2.getY()
			);
			
			g.noStroke();
			
		}
		
		@Override
		public Segment getSegment() {
			return seg;
		}
	}
	
	
	public static class Linear extends SegmentGUI {
		
		private final LinearSegment seg;
		
		public Linear(LinearSegment seg) {
			
			this.seg = seg;
		}

		@Override
		public void render(PGraphics g) {
		
			g.noFill();
			g.stroke(0xffffffff);
			g.strokeWeight(weight);
			g.line(
					seg.getNodeIn().getX(),  seg.getNodeIn().getY(),
					seg.getNodeOut().getX(), seg.getNodeOut().getY());
			
			g.noStroke();
			
		}
		
		@Override
		public Segment getSegment() {
			
			return seg;
		}
	}
	
	public static class Buffer extends SegmentGUI {

		private final BufferedSegment seg;
		
		
		public Buffer(BufferedSegment seg) {
			
			this.seg = seg;
		}

		@Override
		public Segment getSegment() {
			
			return seg;
		}
		
		@Override
		public void render(PGraphics g) {
			
			SubVertex[] sv = seg.getSubVertices();
			
			SubVertex s1, s2;
			
			for (int i = 1; i < sv.length; i++) {
				
				g.noFill();
				g.stroke(0xffffffff);
				g.strokeWeight(weight);
				
				s1 = sv[i - 1];
				s2 = sv[i];
				
				g.line(s1.x, s1.y, s2.x, s2.y);
			}
		}
		
	}
	
	public static class Circular extends SegmentGUI {
		
		private final CircularSegment segment;
		
		public Circular(CircularSegment seg) {
			
			this.segment = seg;
			setColor(0xff999999);
		}
		
		@Override
		public void render(PGraphics g) {
			
			setPos(segment.getPosConstraint().getX(), segment.getPosConstraint().getY()); //hack..
			
			g.noFill();
			g.stroke(getColor());
			g.strokeWeight(weight);
			
			float r = (float) segment.getRadius() * 2f;
			
			g.ellipse(getX(), getY(), r, r);
			g.noStroke();
		}
		
		@Override
		public Segment getSegment() {
			
			return segment;
		}
	}
	
	
	
}
