package gui.paths.gui;

import gui.paths.Node;

public class PosConstraintGUIFactory {

	public VertexGUI createVertex(Node node) {
		
		return new VertexGUI(node);
	}
	
	
	//------------ Static Helpers ---------------
	
	static public PosConstraintGUIFactory simple() {
		return new PosConstraintGUIFactory(); 
	}
	
	static public PosConstraintGUIFactory singlePath() {
		return new SinglePath();
	}
	
	static public class SinglePath extends PosConstraintGUIFactory {
		
		@Override
		public VertexGUI createVertex(Node node) {
			
			return new SinglePathVertexGUI(node);
		}
	}
}
