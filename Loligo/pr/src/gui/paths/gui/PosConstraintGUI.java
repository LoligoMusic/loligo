package gui.paths.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import constants.DisplayFlag;
import constants.InputEvent;
import constraints.Constrainable;
import constraints.ConstraintContext;
import constraints.FollowerObject;
import constraints.LoligoPosConstraint;
import constraints.PosConstraintContainer;
import gui.DisplayObjects;
import gui.SnapBehaviour;
import gui.paths.Node;
import gui.paths.Path;
import gui.paths.PathEvent;
import gui.paths.PathEventManager;
import gui.paths.PathListener;
import gui.paths.PathSegment;
import gui.paths.Segment;
import gui.selection.GuiType;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;

public class PosConstraintGUI extends DisplayObject implements PathListener{
	
	private final static Map<Path, PathGUI> pathGuiMap = new WeakHashMap<>();
	private final static Map<Node, VertexGUI> nodeGuiMap = new WeakHashMap<>();
	
	private final PosConstraintGUIFactory guiFactory;
	
	
	public static PathGUI getPathGui(Path p) {
		return pathGuiMap.get(p);
	}
	
	public static VertexGUI getNodeGui(Node n) {
		return nodeGuiMap.get(n);
	}
	
	protected int SHINY = 0xffffffff, DARK = 0xff999999;
	final LoligoPosConstraint posConstraint;
	
	protected final List<VertexGUI> vertices = new ArrayList<>();
	protected final List<PathGUI> pathGUIs = new ArrayList<>();
	
	public PosConstraintGUI(LoligoPosConstraint pc) {
		
		this(pc, new PosConstraintGUIFactory());
	}
	
	public PosConstraintGUI(LoligoPosConstraint pc, PosConstraintGUIFactory guiFactory) {
		
		this.guiFactory = guiFactory;
		posConstraint = pc;
		PathEventManager.instance().addListener(this);
		setFlag(DisplayFlag.DRAWABLE, false);
		setGuiType(GuiType.CONSTRAINT); 
		setFlag(DisplayFlag.CLICKABLE, false);
		setFlag(DisplayFlag.DROPABLE, false);
	}
	
	@Override
	public void onPathEvent(PathEvent e) {
		
		if(e.constraint == posConstraint.getPosConstraint()) { 
			
			switch (e.type) {
			case NODE_CREATE:
				Node n = ((PathEvent.NodeAddRemove) e).node;
				addNode(n); 
				break;
			case NODE_REMOVE:
				Node n2 = ((PathEvent.NodeAddRemove) e).node;
				removeNode(n2);
				break;
			case PATH_CREATE:
				Path p = ((PathEvent.PathAddRemove) e).path;
				addPath(p);
				break;
			case PATH_DELETE:
				Path p2 = ((PathEvent.PathAddRemove) e).path;
				removePath(p2);
				break;
			case SEGMENT_ADD:
				PathEvent.SegmentAddRemove s1 = (PathEvent.SegmentAddRemove) e;
				addSegment(s1.segment);
				break;
			case SEGMENT_REMOVE:
				PathEvent.SegmentAddRemove s2 = (PathEvent.SegmentAddRemove) e;
				removeSegment(s2.segment, s2.path);
				break;
			case PATH_SPLIT:	
				PathEvent.PathSplit ps = (PathEvent.PathSplit) e;
				PathGUI pg = pathGuiMap.get(ps.path1);
				if(pg != null)
					pg.initialize();
				break;
			default:
				break;
			}
		
		}
	}
	
	public void addNode(Node n) {
		
		VertexGUI v = vertices.stream()
							  .filter(vg -> n == vg.getNode())
							  .findAny().orElseGet(() -> {
									VertexGUI vg = guiFactory.createVertex(n);
									vertices.add(vg);
									nodeGuiMap.put(n, vg);
									return vg;
							  });
		
		addChild(v);
		
	}
	
	
	public void removeNode(Node n) {
		
		for (int i = vertices.size() - 1; i >= 0 ; i--) {
			
			VertexGUI vg = vertices.get(i);
			if(vg.getNode() == n) {
				
				vertices.remove(i);
				removeChild(vg);
				break;
			}
		}
	}
	
	
	public void addPath(Path seg) {
	
		if(seg.getNodeIn() != null && seg.getNodeOut() != null && pathGUIs.stream().noneMatch(s -> seg == s.getSegment())) {
			
			PathGUI sg = new PathGUI((PathSegment) seg);
			pathGUIs.add(sg);
			addChild(sg);
			DisplayObjects.setChildIndex(sg, 0);
			
			pathGuiMap.put(seg, sg);
			
			addNode(seg.getNodeIn());
			addNode(seg.getNodeOut());
		}
	}
	 
	
	public void removePath(Path s) {
		
		for (int i = 0; i < pathGUIs.size(); i++) {
			
			PathGUI sg = pathGUIs.get(i);
			if(sg.getSegment() == s) {
				
				pathGUIs.remove(i);
				removeChild(sg);
				break;
			}
		}
		
	}
	
	
	public void addSegment(Segment s) {
		
		Path p = s.getPath();
		PathGUI pg = getPathGui(p);
		if(pg != null)
			pg.createSegmentGUI(s);
	}
	
	
	public void removeSegment(Segment s, Path parent) {
		
		PathGUI pg = getPathGui(parent);
		if(pg != null)
			pg.removeSegmentGUI(s);
	}
	
	
	public void forceUpdateGui() {
		
		for (Path p : posConstraint.getPaths()) {
			
			addPath(p);
		}
		
		for(PathGUI pg : pathGUIs)
			pg.forceUpdateGui();
	}
	
	
	@Override
	public int setColor(int color) {
	
		pathGUIs.forEach(s -> s.setColor(color));
		
		return color;
	}
	
	/*
	@Override
	public void mouseDragged() {
		posConstraint.updatePosConstraint();
	}
	*/
	
	public boolean noLoop(Constrainable p) {
	
		if(p instanceof PosConstraintContainer) {
			
			List<PosConstraintContainer> li = new ArrayList<>();
			
			li.add((PosConstraintContainer)p);
			int i = 0;
			PosConstraintContainer d;
			
			
			while(i < li.size()){
				
				d = li.get(i);
				
				for(Constrainable am : d.getConstrainables())
					
					if(am instanceof PosConstraintContainer) {
						
						PosConstraintContainer pos = (PosConstraintContainer) am;
						
						if(pos == posConstraint.getModule()) {
							return false;
						}
						
						li.add(pos);
					}
				i++;
			}
			
		}
		
		return true;
	}
		
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		forceUpdateGui();
		getDm().getInput().addInputEventListener(this);
	}
	
	@Override
	public void removedFromDisplay() {
		getDm().getInput().removeInputEventListener(this);
		super.removedFromDisplay();
	}
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(e == InputEvent.DRAG_START && input == getDm().getInput()) {
			
			//setColor(DARK);
			
			DisplayObjectIF d = input.getDragTarget();
			
			final Constrainable m = posConstraint.getDomain().getConstraintContext().getConstraintFromGui(d);
			
			if(m != null && m != posConstraint.getModule()) {
			
				SnapBehaviour.SnapToPath sb = new SnapBehaviour.SnapToPath(input.getDragTarget(), posConstraint) {
				
					@Override
					public boolean applySnap(UserInput in) {
						
						boolean n = super.applySnap(in);
						setColor(n ? SHINY : DARK);
						return n;
					}
					
					public void onEmptyDrop() {
						
						setColor(DARK);
					}
					
					public void onDrop(gui.paths.PathObject po) {
						
						Constrainable c = (Constrainable) m;
						
						if(noLoop(c) && !posConstraint.getConstrainables().contains(c)) {
							
							posConstraint.addFollowerAutoPos(
								c, 
								po.getPath(), 
								po.getPathPosition()
							);
						}
						
						setColor(DARK);
					}
				};
				
				sb.setAutoRemove(true);
				input.addInputEventListener(sb); 
			}
			
			boolean constraintUnSelected = !input.selection.contains(posConstraint.getModule().getGui());
			
			for(FollowerObject f : posConstraint.getFollowers()) {
			
				Constrainable c = ConstraintContext.get(posConstraint).getConstraintFromGui(input.getDragTarget());
						
				if(f.getTarget() == c && constraintUnSelected) {
					
					posConstraint.removeFollower(f);
					break;
				}
			}
			
		}
	}
			
	
	
	
}
