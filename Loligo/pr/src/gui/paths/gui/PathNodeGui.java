package gui.paths.gui;

import static gui.paths.Segments.HIT_DISTANCE;

import constants.DisplayFlag;
import constants.InputEvent;
import constants.Timer;
import constraints.LoligoPosConstraint;
import gui.paths.Node;
import gui.paths.NullPathObject;
import gui.paths.PathEvent;
import gui.paths.PathEventEnum;
import gui.paths.PathObject;
import gui.paths.Segments;
import module.modules.anim.Path;
import pr.userinput.UserInput;
import processing.core.PConstants;

public class PathNodeGui extends PosConstraintGUI{
	
	private final Path pathNode;
	
	private boolean editMode, mouseOverPath;
	private double splitPos;
	private PathObject po = new NullPathObject();
	
	
	public PathNodeGui(Path pathNode, LoligoPosConstraint pc) {
		super(pc);
		this.pathNode = pathNode;
	}

	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == InputEvent.DOUBLECLICK) {
			editMode(true);
		}
	}
	
	@Override public void mouseEvent(InputEvent e, UserInput input) {
		
		if(input.isCurrentTarget(pathNode.getGui(), InputEvent.DOUBLECLICK)) {
			
			editMode(true);
		}else
		if(editMode) {
			
			gui.paths.Path p = posConstraint.getPaths().get(0);
			if(e == InputEvent.DOUBLECLICK) {
				Timer.createFrameDelay(getDm().getDomain(), t -> editMode(false)).start();
			}else
			if(e == InputEvent.PRESSED) {
				if(mouseOverPath && input.getPressedTarget() == null) {
					Node ne = Segments.splitSubSegment(p, splitPos);
					VertexGUI vg = getNodeGui(ne);
					input.selection.removeSelectionRect();
					input.injectDragTarget(vg);
					
				}
				return;
			}else
			if(e == InputEvent.MOVE && input.getOverTarget() == null){
				float mx = input.getMouseX(), my = input.getMouseY();
				splitPos = p.nearestPoint(mx, my);
				p.goToPos(po, splitPos);
				float x = posConstraint.getX() + po.getX() - mx,
					  y = posConstraint.getY() + po.getY() - my;
				mouseOverPath = x * x + y * y < HIT_DISTANCE * HIT_DISTANCE;
				pathNode.getDomain().getPApplet().cursor(mouseOverPath ? PConstants.CROSS : PConstants.ARROW);
				
				return;
			}
			
		}
		
		super.mouseEvent(e, input);
		
	}
	
	public void editMode(boolean b) {
		editMode = b;
		
		vertices.forEach(v -> {
			v.setColor(editMode ? 0xff00ddaa : DARK);
			v.setFlag(DisplayFlag.CLICKABLE, editMode);
			int s = editMode ? 8 : 5;
			v.setWH(s, s);
		});
		
		setColor(editMode ? 0xff00ddaa : DARK);
		pathGUIs.forEach(p -> p.setWeight(editMode ? 1.5f : .8f));
	}
	
	
	@Override
	public void onPathEvent(PathEvent e) {
		
		super.onPathEvent(e);
		
		if(e.constraint == posConstraint.getPosConstraint() && e.type == PathEventEnum.NODE_CREATE) {
			//PathEvent.NodeAddRemove ne = (PathEvent.NodeAddRemove) e;
			
		}
	}
	
}
