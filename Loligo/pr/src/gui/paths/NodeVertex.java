package gui.paths;

import java.util.ArrayList;
import java.util.List;

import gui.path.PositionableObject;


public class NodeVertex extends PositionableObject implements Node {

	private final List<Segment> links = new ArrayList<>();
	private final PosConstraint posConstraint;
	
	
	public NodeVertex(PosConstraint posConstraint) {
		
		this.posConstraint = posConstraint;
		
	}

	
	public int numLinks() {
		
		return links.size();
	}
	
	
	public Segment getLink(int i) {
		
		return links.get(i);
	}
	
	/*
	public void addLinkOut(Path s) {
		
		if(!links.contains(s))
			links.add(s);
		
		Node n = s.getNodeIn();
		n.removeLink(Nodes.indexOf(n, s));
		
		//s.setNodeIn(this);
		
	}
	
	public void addLinkIn(Path s) {
		
		if(!links.contains(s))
			links.add(s);
		
		Node n = s.getNodeOut();
		n.removeLink(Nodes.indexOf(n, s));
		
		//s.setNodeOut(this);
	}
	*/
	
	@Override
	public boolean removeLink(Segment s) {
		
		return links.remove(s);
	}
	
	/*
	public void replaceNode(NodeVertex nodeOld) {
		
		for(Path s : nodeOld.links) {
			
			s.setNodeOut(this);
			this.addLink(s);
		}
		
		//TODO: support for loop segments - inNode == outNode
		
	}
	*/
	
	@Override
	public int nodeType() {
	
		return NODE;
	}

	@Override
	public void addLink(Segment s) {
		
		addLink(s, links.size());
	}
	
	@Override
	public void addLink(Segment s, int idx) {
		
		if(!links.contains(s))
			links.add(idx, s);
	}

	@Override
	public Segment removeLink(int i) {
		
		return links.remove(i);
	}


	@Override
	public void updateSegments() {
		
		links.forEach(s -> s.rootSegment().updateLength());
		
	}


	@Override
	public PosConstraint getPosConstraint() {
		
		return posConstraint;
	}
	
}
