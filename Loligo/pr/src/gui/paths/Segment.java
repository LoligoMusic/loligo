package gui.paths;

import gui.paths.PathMementoFactory.SegmentMemento;

public interface Segment {
 
	
	public void init();
	
	/**sets xy position of po relative to the path returned by getPath()
	 * @param po 
	 * @param fac
	 */
	public void goToPos(PathObject po, double fac); 
	
	/**
	 * If called from a Segment implementation that is made up of other segments, like PathSegment, this method returns 
	 * the underlying Segment-section at the specified position on this Segment. 
	 * @param fac
	 * @return
	 */
	public Segment lowestSegmentAt(double fac);
	public Path rootSegment();
	public Segment getSuperSegment();  //TODO getPath sollte genug sein
	public boolean hitTest(float x, float y);
	public double nearestPoint(float x, float y);
	public Node getNodeIn();
	public Node getNodeOut();
	public Path getPath(); 
	public void setPath(Path p);
	//public void nextSegment(SegmentInfo si);
	//public boolean hasNextSegment();
	//public void resetNextSegment();
	//public int numSegments();
	public double getLength();
	//public void setLength(double length);
	public double updateLength();
	public double getParentOffset();
	public void setParentOffset(double offset);
	public SegmentType getSegmentType();
	//public Segment nextSegment();	TODO needed?
	//public Segment previousSegment();
	double ToParentPos(double p);
	public PosConstraint getPosConstraint();
	
	/**
	 * Creates a new pair of Segments from this segment. Replaces this segment with the new ones.
	 * @param pos Position on this segment to split.
	 * @param node New Node to be inserted between the two created segments.
	 * @return SegmentPair containing Segment from NodeIn to node and another from node to NodeOut
	 */
	public SegmentPair split(double pos, Node node);
	
	
	
	public SegmentMemento toMemento(PathMementoFactory pmf);
	public void restoreMemento(SegmentMemento sm, PathMementoFactory pmf);
	
}
