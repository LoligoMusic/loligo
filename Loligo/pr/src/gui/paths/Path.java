package gui.paths;

public interface Path extends Segment {

	void addSegment(int index, Segment s);

	int numSegments();

	Segment getSegment(int index);

	void removeSegment(int index);

	boolean removeSegment(Segment s);
	
	int indexOf(Segment s);

}