package gui.paths;

import java.util.ArrayList;
import java.util.List;

import gui.Position;
import gui.path.PositionableObject;
import gui.paths.PathMementoFactory.ConstraintMemento;
import lombok.Getter;
import lombok.Setter;

public class PathTree extends PositionableObject implements PosConstraint{

	@Getter@Setter
	private SegmentFactory segmentFactory = SegmentFactory.defaultFactory;
	private PathListener pathListener = e -> {};
	private List<Path> segments = new ArrayList<>();
	private List<Node> nodes = new ArrayList<>();
	
	@Override
	public void removeNode(Node n) {
		
		for (int i = n.numLinks() - 1; i >= 0; i--) {
				
			removePath2(n.removeLink(i).rootSegment());
		}
		
		if(nodes.remove(n)) {
			
			pathListener.onPathEvent(new PathEvent.NodeAddRemove(PathEvent.REMOVE, n));
		}
	}
	
	private void removePath2(Path p) {
		
		if(segments.remove(p)) {
			
			Segments.removePathFromNode(p.getNodeIn() , p);
			Segments.removePathFromNode(p.getNodeOut(), p);
			
			pathListener.onPathEvent(new PathEvent.PathAddRemove(PathEvent.REMOVE, p));
		}
		
		
	}
	
	@Override
	public void removePath(Path p) {
		
		removePath2(p);
		
		if(p.getNodeIn().numLinks() == 0)
			removeNode(p.getNodeIn());
		if(p.getNodeOut().numLinks() == 0)
			removeNode(p.getNodeOut());
	
	}
	
	@Deprecated
	public Path addPath(SegmentType type) {
		
		Node n1 = new Vertex(this),
			 n2 = new Vertex(this);
		
		Path p = new PathSegment(this);
		
		Segment s = Segments.createSegmentByType(this, n1, n2, type);
		p.addSegment(0, s);
		
		return p;
	}
	
	@Override
	public Path addPath(Node n1, Node n2, SegmentType type) {
		
		addNode(n1);
		addNode(n2);
		
		Path p = new PathSegment(this);
		
		Segment s = Segments.createSegmentByType(this, n1, n2, type);
		p.addSegment(0, s);
		
		segments.add(p);
		
		n1.addLink(s);
		n2.addLink(s);
		
		pathListener.onPathEvent(new PathEvent.PathAddRemove(PathEvent.ADD, p));
		
		return p;
	}
	
	@Override
	public Path addPath(Path path) {
		
		if(path.getPosConstraint() != this)
			throw new IllegalArgumentException("path belongs to " + path.getPosConstraint());
		
		segments.add(path);
		pathListener.onPathEvent(new PathEvent.PathAddRemove(PathEvent.ADD, path));
		
		return path;
	}
	
	
	/**
	 * Creates a new NodeVertex and adds it to this Constraint
	 * @return the created Node
	 */
	@Override
	public Node newNode() {
		
		Node n = new NodeVertex(this);
		
		addNode(n);
		
		return n;
	}

	@Override
	public void addNode(Node n) {
		
		if(!nodes.contains(n)) {
			
			nodes.add(n);
			n.setPosObject(new Position.PositionOffset(this, 0, 0));
			
			pathListener.onPathEvent(new PathEvent.NodeAddRemove(PathEvent.ADD, n));
		}
	}
	
	@Override
	public void updatePosConstraint() {
		
	}
	
	public List<Path> getPaths() {
		
		return segments;
	}
	
	@Override
	public List<Node> getNodes() {
		
		return nodes;
	}

	@Override
	public void setPathListener(PathListener pathListener) {
		this.pathListener = pathListener;
	}

	@Override
	public void dispatchEvent(PathEvent e) {
		
		if(pathListener != null) {
			
			pathListener.onPathEvent(e);
		}
	}

	@Override
	public void toConstraintMemento(ConstraintMemento cm) {
		
	}

	@Override
	public void restoreConstraintMemento(ConstraintMemento cm) {
		
		
	}

	@Override
	public Segment createSegment(Node n1, Node n2, SegmentType type) {
	
		return addPath(n1, n2, type);
	}
	
	
}
