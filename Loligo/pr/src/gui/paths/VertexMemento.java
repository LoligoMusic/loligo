package gui.paths;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class VertexMemento {
	
	transient public Node node;
	
	public float x, y;
	 
	public VertexMemento() {}
	
	public VertexMemento(Node n) {
		
		node = n;
		x = n.getX() - n.getPosConstraint().getX();
		y = n.getY() - n.getPosConstraint().getY();
	}
}