package gui.paths;

import pr.Positionable;

public interface PathObject extends Positionable, PathListener{

	public Path getSegment();
	
	public void setSegment(Path path);

	public void setPath(Path path);
	
	public Path getPath();
	
	public double getPathPosition();
	
	public double getTangent();
	
	public void setTanget(double tangent);
	
	public void goToPathPosition(double d);
	
	public void updatePosition();
	
	public default void onAddToConstraint() {}
	
	public default void onRemoveFromConstraint() {}

	public default void copyPositionTo(PathObject po) {
		
		po.goToPathPosition(getPathPosition());
	}
}
