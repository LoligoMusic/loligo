package gui.paths;

import gui.Position;

public abstract class AbstractPathObject implements PathObject {

	private Position pos = new Position();
	private Path path;
	private double pathPos, tangent;
	
	@Override
	public void updatePosition() {
		
		goToPathPosition(pathPos);
	}
	
	@Override
	public float getX() {
		
		return pos.getX();
	}

	@Override
	public float getY() {
		
		return pos.getY();
	}

	@Override
	public void setPos(float x, float y) {
		
		pos.setPos(x, y);
	}

	@Override
	public Position getPosObject() {
		
		return pos;
	}

	@Override
	public void setPosObject(Position p) {
		
		pos = p;
	}

	@Override
	public Path getSegment() {
		
		return path;
	}

	@Override
	public void setSegment(Path path) {
		
		//TODO wtf is this for?
	}

	public void setPath(Path path) {

		this.path = path;
	}
	

	@Override
	public Path getPath() {
		
		return path;
	}


	@Override
	public double getPathPosition() {
		
		return pathPos;
	}

	@Override
	public void goToPathPosition(double d) {
		
		pathPos = d;
		
		if(path != null)
			path.goToPos(this, pathPos);
	}
	
	@Override
	public double getTangent() {
		
		return tangent;
	}
	
	@Override
	public void setTanget(double tangent) {
		
		this.tangent = tangent;
	}

	@Override
	public void onPathEvent(PathEvent e) {
		
		if(e.type == PathEventEnum.UPDATE_OBJECTS) {
			
			if(e.constraint == getSegment().getPosConstraint())
				updatePosition();
			
		}else
		if(e.type == PathEventEnum.PATH_SPLIT) {
			
			PathEvent.PathSplit pe = (PathEvent.PathSplit) e;
			
			if(path == pe.originalPath) {
				
				boolean b = pathPos < pe.splitPos;
				Path p = b ? pe.path1 : pe.path2;
				double off = b ? 0 : pe.splitPos;
				
				double d = (pathPos - off) * (p.getLength() / pe.originalLength);
				
				setPath(p);
				pathPos = d;
				
				updatePosition();
			}
		}
	}
}
