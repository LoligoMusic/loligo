package gui;

import java.util.ArrayList;

import constants.DisplayFlag;
import gui.selection.GuiType;
import gui.text.TextBox;
import lombok.Getter;
import lombok.Setter;
import module.Module;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import processing.core.PGraphics;
import util.Colors;


public class GUIBox extends DisplayObject{
	
	public final static int LEFT = 0, RIGHT = 1, CENTER = 2; 
	
	@Getter
	final private ArrayList<Row> rows = new ArrayList<Row>();
	
	private Row maxRow;
	public int margin = 5;
	public final int padding = 4;
	public int barHeight = 30, paddingBottom = 0;
	public final int minimumWidth = 80, minHeight = 32;
	private int top = margin, alignMode = RIGHT;
	@Setter
	private int topDefault = 1;
	
	public boolean maximized, isOpen, toggleSizeEnabled = true;
	private Button deleteButton;
	public Button toggleSize;
	private boolean defaultButtonsMode = true;
	
	private TextBox name;
	

	public GUIBox(Module m) {
		
		settings();
		//module = m;
		
		deleteButton = new Button(m::delete, null);
		toggleSize = new Button(this::toggleSize, null);
		addRow(false, deleteButton, toggleSize);
		defaultButtonsMode = false;
		
		setName(m.getName());
		setTop(2);
		align(LEFT);
		
	}
	
	public GUIBox() {
		
		defaultButtonsMode = false;
		settings();
		//module = null;
		setDimensions(8, 8);
		
		fill(Colors.rgb(110,  20,  20));
		//addRow(new Button(this, "hide", Button.CLICK));
		align(LEFT);
	}
	
	private void settings() {
		
		setFlag(DisplayFlag.DRAGABLE, true);
		setGuiType(GuiType.WINDOW);
		setWH(getWidth(), (padding + margin));
		setFlag(DisplayFlag.RECT, true);
		setColor(ModuleGUI.COLOR_DEFAULT);//RootClass.mainProc().color(30));
		setDimensions(8, 8);
	}
	
	@Override
	public void render(PGraphics g) {
		
		g.stroke(255, 20);
		g.fill(getColor());
		g.rect(getX(), getY(), getWidth(), getHeight());
		g.noStroke();
		//dm.g.fill(250, 100);
		//if(type != null)
		//	dm.g.text(type, getX() + 6, getY() + 14);
		//dm.g.fill(200);
		//dm.g.text(name, x + margin, y + 8);
	}
	
	public Row addRow( DisplayObjectIF...dArr) {
		
		return addRow(true, dArr);
	}
	
	
	
	protected Row addRow(boolean body, DisplayObjectIF...dArr) {
		
		addChild(dArr);
		
		Row r = new Row(body, dArr);
		rows.add(r);
		
		if(rows.size() > 1 && defaultButtonsMode)
			enableToggleSize(true);
		
		updateSize();
		
		setTop(topDefault);
		
		return r;
	}
	
	
	public void updateSize() {
		
		for(Row ro : rows) 
			ro.updateSize();
		
		int w = getWidth();
		
		for(Row ro : rows) {
			
			if(maxRow == null || ro.width > maxRow.width)
				maxRow = ro;
			
			if(maxRow != null)
				w = maxRow.width + 2 * (margin + padding);
			
			w = Math.max(w, minimumWidth);
		}
		
		int h = 0;
		
		if(!rows.isEmpty()) {
			Row last = rows.get(rows.size() - 1);
			h = last.y + last.height;
		}
		h = Math.max(h, minHeight) + paddingBottom;
		
		setWH(w, h);
		
		for(Row ro : rows) {
				
			//ro.updateSize();
			
			int xx;
			if(!ro.body && ro.dfm) {
				
				xx = getWidth();
				for(DisplayObjectIF d : ro.items) {
					xx -= d.getWidth() + 5;
					d.getPosObject().x = xx;
				}
				continue;
			}
			
			switch(ro.align) {
			case LEFT:
				
				xx = margin + padding;
				if(ro.items[0].checkFlag(DisplayFlag.ROUND)) 
					xx += ro.items[0].getWidth() / 4; // TODO 2 oder 4???
				
				for(DisplayObjectIF d : ro.items) {
					d.getPosObject().x = xx;
					xx += d.getWidth() + margin;
				}
				break;
			case RIGHT:
				
				xx = getWidth() - padding;
				if(ro.items[0].checkFlag(DisplayFlag.ROUND))
					xx -= ro.items[0].getWidth() / 2;
				
				for(int i = ro.items.length - 1; i >= 0; i--) {
					DisplayObjectIF d = ro.items[i];
					xx -= d.getWidth() + margin;
					d.getPosObject().x = xx;
				}
				break;
			case CENTER:
				xx = (getWidth() - ro.width) / 2;
				for(DisplayObjectIF d : ro.items) {
					d.getPosObject().x = xx;
					xx += d.getWidth() + margin;
				}
			}
			
		}
		
	}
	
	public void hideStandardButtons() {
		if(deleteButton != null)
			DisplayObjects.removeFromDisplay(deleteButton);
		enableToggleSize(false);
		defaultButtonsMode = false;
	}
	
	
	public void setTop(int t) {
		top = margin * t;	//top-abstand wird nach jeder neuen reihe zur�ckgesetzt
	}
	
	
	public void align(int a) {
		alignMode = a;
	}
	
	
	public class Row {
		
		public final DisplayObjectIF[] items;
		int y, width, height, top = GUIBox.this.top, align = alignMode;
		final boolean body, dfm;
		
		Row(final DisplayObjectIF[] dArr) {
			this(true, dArr);
		}
		
		Row(boolean body, DisplayObjectIF[] items) {
			
			this.items = items;
			this.body = body;
			dfm = defaultButtonsMode;
			
			//updateSize();
		}
		
		private void updateSize() {
			
			if(body) {
				
				int ind = rows.indexOf(this);
				
				if(ind > (rows.get(0).body ? 0 : 1)) { // TODO: Hack f�r boxen die kein body = false element am anfang haben
					
					Row r = rows.get(ind - 1);
					y = r.y + r.height + top;
					if(items[0] instanceof Button)
						System.out.println();
				}else{
					
					y = barHeight;
				}
			
			}else 
				y = margin / 2;
			
			width = margin * (items.length - 1);
			height = items[0].getHeight();
			for (DisplayObjectIF d : items) {
				d.fixedToParent(true);
				d.getPosObject().y = y;
				width += d.getWidth();
				if(d.getHeight() > height)
					height = d.getHeight();
			}

		}
	}
	
	public void open() {
		
		isOpen = true;
		getDm().getDomain().getDisplayManager().addGuiObject(this);
	}
	
	public void hide() {
		
		isOpen = false;
		DisplayObjects.removeFromDisplay(this);
	}
	
	public void clear() {
		
		for (int i = 0; i < rows.size(); i++) 
			removeChild(rows.get(i).items);
		
		rows.clear();
		
		maxRow = null;
	}
	
	public void toggleSize(boolean b) {
		
		if((b && !maximized) ||
		  (!b && maximized))
			toggleSize();
	}
	
	public void toggleSize() {
		/*
		if(toggleSizeEnabled) {
			Module module = ModuleManager.getModuleFromGui(this);
			if(maximized) {
				for (int i = 1; i < rows.size(); i++) 
					addChild(rows.get(i).items);
				setWH(getWidth(), endHight);
				
				updateSize();
				
				module.positionSlots();
				
			}else{
				
				for (int i = 1; i < rows.size(); i++) 
					removeChild(rows.get(i).items);
				
				float inh = 0, outh = 0;
				if(module.getInputs() != null)
					inh  = module.getInputs().get(module.getInputs().size() - 1).getGUI().getY() - module.getInputs().get(0).getGUI().getY();
				if(module.getOutputs() != null)
					outh = module.getOutputs().get(module.getOutputs().size() - 1).getGUI().getY() - module.getOutputs().get(0).getGUI().getY();
				setWH(getWidth(), barHeight + (int)Math.max(inh, outh));
				
				
				
				module.positionSlots();
			}
			maximized = !maximized;
		}*/
	}
	
	public void enableToggleSize(boolean t) {
		if(toggleSize != null) {
			toggleSizeEnabled = t;
			if(t) 
				addChild(toggleSize);
			else
				removeChild(toggleSize);
		}
	}
	 
	public void setName(String n) {
		if(name == null)
			name = new TextBox();
		name.setText(n);
		name.setFlag(DisplayFlag.fixedToParent, true);
		addChild(name);
		name.setPos(3, -2);
		
	}
	
	public Row getRowByElement(DisplayObjectIF d) {
		for(Row r : rows) {
			for(DisplayObjectIF d2 : r.items) {
				if(d2 == d)
					return r;
			}
		}
		return null;
	}
 	
}
