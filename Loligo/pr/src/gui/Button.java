package gui;

import static constants.InputEvent.MouseAction.CLICK;
import static constants.InputEvent.MouseAction.PRESSED;
import static constants.InputEvent.MouseAction.RELEASED;
import constants.DisplayFlag;
import constants.InputEvent;
import gui.text.TextBox;
import gui.text.TextObject;
import gui.text.TextBlock.AlignMode;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PImage;
import util.Colors;
import util.Images;

public class Button extends DisplayObject implements DisplayGuiSender<Boolean>, TextObject{
	@Getter
	private final TextBox textBox;
	private TextBox mouseOverTextBox;
	@Setter
	private String mouseOverText;
	//final GuiReceiver guiReceiver;
	private final Runnable action;
	//final ArrayList<Client> clients = new ArrayList<Client>();
	private PImage img, imgIdle, imgOver;
	@Getter@Setter
	private int mouseOverButtonColor = 0xffffffff, 
				buttonColor			 = 0xffaaaaaa, 
				disabledColor		 = 0xff444444, 
				textColor			 = 0xffbbbbbb, 
				disabledTextColor	 = 0xff555555,
				backgroundColor		 = 0x15ffffff,
				buttonPressedColor	 = 0xff9a9a9a;
	@Setter
	private InputEvent.MouseButton mouseButton = InputEvent.MouseButton.LMB;
	private boolean alphaMaskMode;
	private boolean enabled = true;
	private boolean singleThread = true;
	
	
	public Button(final Runnable action, final String name){
	
		if(name != null && name.length() > 0) {
			textBox = new TextBox(name, true);
			setWH(textBox.getWidth(), textBox.getHeight());
			
			textBox.setFlag(DisplayFlag.fixedToParent, true);
			addChild(textBox);
		}else
			textBox = null;
		
		this.action = action;
		
		setFlag(DisplayFlag.fixedToParent, true);
		setFlag(DisplayFlag.RECT, true);
		
	}
	
	public void mainThread() {
		singleThread = false;
	}
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		updateColor();
	}
	
	@Override
	public void render(PGraphics g) {
		
		if(img != null) {
			
			if(enabled && checkFlag(DisplayFlag.mouseOver)) {
				//g.fill(backgroundColor);
				//g.rect(getX(), getY(), getWidth(), getHeight());
			}
			g.tint(getColor());
			g.image(img, getX(), getY());
			g.tint(Colors.WHITE);
			g.noTint();
			
		}else
			super.render(g);
	}
	
	@Override
	public void mouseOverRest(boolean on) {
		if(on) {
			showMouseOverText();
		}else {
			removeMouseOverText();
		}
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(enabled) {
		
			if(e == mouseButton.forAction(CLICK)) {
				removeMouseOverText();
				action();
			}else
			if(e == InputEvent.OVER || e == mouseButton.forAction(RELEASED)) {
				setColor(mouseOverButtonColor);
				img = imgOver != null ? imgOver : img;
			}else
			if(e == InputEvent.OVER_STOP) {
				setColor(buttonColor);
				img = imgIdle;
			}else
			if(e == mouseButton.forAction(PRESSED)) {
				setColor(buttonPressedColor);
			}
			
		}
	}
	
	public void action() {
		if(action != null) {
			if(singleThread)
				action.run();
			else
				getDm().getDomain().getHistory().executeInMain(action);
		}
	}
	
	@Override
	public int setColor(int color) {
		int c = super.setColor(color);
		setAlphaMaskColor(color);
		return c;
	}
	
	public void updateColor() {
		
		int c = !enabled 						 ? disabledColor :
				checkFlag(DisplayFlag.mouseOver) ? mouseOverButtonColor :
												   buttonColor;
		setColor(c);
		
		if(textBox != null) {
			textBox.setTextColor(enabled ? textColor : disabledTextColor);
		}
	}
	
	@Override
	public void setWH(int width, int height) {
		super.setWH(width, height);
		if(textBox != null)
			textBox.setWH(width, height);
	}
	
	private void setAlphaMaskColor(int c) {
		
		if(img != null && alphaMaskMode)
			Images.fillAlphaImage(img, c);
			
	}
	
	/**
	 * Use button-image as alpha mask and fill with mouseOver-color
	 */
	public void alphaMaskMode() {
		alphaMaskMode = true;
		updateColor();
	}
	
	
	public void showMouseOverText() {
		
		removeMouseOverText();
		
		if(mouseOverText != null) {
			mouseOverTextBox = new TextBox(mouseOverText, true);
			mouseOverTextBox.bgBox = true;
			mouseOverTextBox.offX = 5;
			mouseOverTextBox.setColor(0xff171717);
			mouseOverTextBox.setPos(getX(), getY() - mouseOverTextBox.getHeight() - 4);
			mouseOverTextBox.setFlag(DisplayFlag.CLICKABLE, false);
			//getDm().getInput().setRemovableTarget(mouseOverTextBox);
			getDm().addChild(mouseOverTextBox);
			//Utils.holdInScreen(mouseOverTextBox);
		}
	}
	
	public void removeMouseOverText() {
		if(mouseOverTextBox != null) {
			DisplayObjects.removeFromDisplay(mouseOverTextBox);
			//getDm().getInput().removeRemovableTarget(mouseOverTextBox);
		}
	}
	
	@Override
	public void removedFromDisplay() {
		removeMouseOverText();
	}
	
	/*
	@Override
	public void mouseOver(){
		sendMessage(MOUSE_OVER);
		
	}
	
	@Override
	public void mousePressed(){
		sendMessage(PRESSED);
	}
	
	@Override
	public void mouseReleased(DisplayObjectIF mousePressedTarget){
		sendMessage(RELEASED);
	}
	
	@Override
	public void mouseClicked(){
		sendMessage(CLICK);
	}
	
	@Override
	public void mouseStartDrag(){
		sendMessage(START_DRAG);
	}
	
	@Override
	public void mouseDragged(){
		sendMessage(DRAG);
	}
	 */
	
	@Override
	public Boolean getValue() {
		return false;
	}
	
	@Override
	public void setValue(Boolean v) {}

	@Override
	public void disable(boolean b) {
		
		enabled = !b;
		updateColor();
		
	}

	@Override
	public void update() {}

	
	public void setImage(PImage idle) {
		img = imgIdle = idle;
		setWH(img.width, img.height);
		setAlphaMaskColor(getColor());
	}
	
	
	public void setImage(PImage idle, PImage over) {
		img = imgIdle = idle;
		imgOver = over;
		setWH(img.width, img.height);
		
		mouseOverButtonColor = buttonColor = Colors.WHITE;
	}

	@Override
	public void setText(String s) {
		textBox.setText(s);
	}

	@Override
	public String getText() {
		return textBox.getText();
	}

	@Override
	public void setAlignMode(AlignMode m) {
		textBox.setAlignMode(m);
	}

	@Override
	public void setFont(PFont f) {
		textBox.setFont(f);
	}
	
	
	
}

