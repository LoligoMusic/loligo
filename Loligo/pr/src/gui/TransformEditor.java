package gui;

import static java.lang.Math.sqrt;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import constants.Units;
import gui.selection.GuiType;
import gui.text.NumberInputField;
import lombok.var;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PMatrix2D;


public class TransformEditor extends DisplayObject implements DisplayGuiSender<PMatrix2D>, GuiReceiver {
	
	private final static int X = 0, Y = 1, SCALE_X = 2, SCALE_Y = 3, ROTATE = 4;
	
	private final GuiSenderObject<PMatrix2D> sender;
	private final NumberInputField[] fields = new NumberInputField[5];
	private final float[] values = {0, 0, 1, 1, 0};
	
	public TransformEditor(Consumer<PMatrix2D> cons, Supplier<PMatrix2D> sup) {

		sender = new GuiSenderObject<PMatrix2D>(this, cons, sup, new PMatrix2D());
		
		for (int i = 0; i < fields.length; i++) {
			final int j = i;
			var f = new NumberInputField(m -> setValue(j, m.floatValue()), () -> getValue(j));
			f.setFlag(DisplayFlag.fixedToParent, true);
			f.singleThread();
			fields[i] = f;
			addChild(f);
		}
		updateHeight();
		int h = updateHeight();
		setWH(fields[0].getWidth(), h);
		
		setColor(0xff555555);
		setFlag(DisplayFlag.RECT, true);
		setGuiType(GuiType.SIGNAL);
	}

	
	private int updateHeight() {
		int margin = 1;
		int height = margin;
		
		for (int i = 0; i < fields.length; i++) {
			NumberInputField f = fields[i];
			f.setPos(0, height);
			height += f.getHeight() + margin;
		};
		return height;
	}
	
	
	private void setValue(int idx, float v) {
		
		values[idx] = v;
		
		PMatrix2D m = sender.getValue();
		m.reset();
		m.rotate(values[ROTATE]);
		m.scale(values[SCALE_X], values[SCALE_Y]);
		
		float u = (float) Units.LENGTH.normFactor;
		m.translate(values[X] * u, values[Y] * u);
		
		sender.message(m);
	}
	
	private double getValue(int idx) {
		return values[idx];
	}
	 
	
	@Override
	public PMatrix2D getValue() {
		return sender.getValue();
	}

	@Override
	public void setValue(PMatrix2D v) {
		sender.setValue(v);
	}

	@Override
	public void update() {
		sender.update();
		
		var m = sender.getValue();
		
		float[] ar = new float[6];
		m.get(ar);
		
		float a = m.m00, c = m.m10;
		float b = m.m01, d = m.m11;
		
		//values[X] 		= (float) m.m02;
		//values[Y] 		= (float) m.m12;
		values[SCALE_X] = (float) sqrt(a * a + c * c);
		values[SCALE_Y] = (float) sqrt(b * b + d * d);
		values[ROTATE]  = (float) Math.atan(c / d);
		
		for (int i = 0; i < fields.length; i++) 
			fields[i].update();
		
	}

	@Override
	public void disable(boolean b) {
		
	}

	@Override
	public void guiMessage(GuiSender<?> s) {
	
	}

	@Override
	public void updateSender(GuiSender<?> s) {
		
	}
	
	@Override
	public void setWH(int width, int height) {
		int h = updateHeight();
		super.setWH(width, h);
		
		for (NumberInputField f : fields) {
			f.setWH(width, f.getHeight());
		}
	}
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		update();
	}
	
	
	public static class Preview extends DisplayObject implements DisplayGuiSender<PMatrix2D>{
		
		private final TransformEditor editor;
		
		public Preview(Consumer<PMatrix2D> c, Supplier<PMatrix2D> s) {

			editor = new TransformEditor(c, s);
			
			setWH(80, 20);
			setFlag(DisplayFlag.RECT, true);
			setFlag(DisplayFlag.CLICKABLE, true);
			setGuiType(GuiType.SIGNAL);
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			super.mouseEvent(e, input);
			
			if(input.isCurrentTarget(this, InputEvent.DOUBLECLICK)) {
				
				input.setRemovableTarget(editor, this);
				editor.setPos(getX(), getY() + getHeight() + 4); 
				editor.setWH(getWidth(), getHeight());
				DisplayObjects.holdInScreen(editor);
			}
		}

		
		@Override
		public PMatrix2D getValue() {
			return editor.getValue();
		}

		@Override
		public void setValue(PMatrix2D v) {
			editor.setValue(v);
		}

		@Override
		public void update() {
			editor.update();
		}

		@Override
		public void disable(boolean b) {
			editor.disable(b);
		}
	}

}
