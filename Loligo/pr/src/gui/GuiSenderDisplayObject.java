package gui;

import java.util.function.Consumer;
import java.util.function.Supplier;

import lombok.experimental.Delegate;
import pr.DisplayObject;

public class GuiSenderDisplayObject<T> extends DisplayObject implements DisplayGuiSender<T> {

	private final GuiSenderObject<T> sender;
	@Delegate
	private final MouseOverTextHandler mouseOverText;
	
	
	public GuiSenderDisplayObject(Consumer<T> action, Supplier<T> updateAction, T value) {
		
		sender = new GuiSenderObject<>(this, action, updateAction, value);
		mouseOverText = new MouseOverTextHandler(this);
	}

	/*
	@Override
	public void mouseOverRest(boolean on) {
		super.mouseOverRest(on);
		mouseOverText.mouseOverRest(on);
	}
	*/
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		update();
	}
	
	public void singleThread() {
		sender.singleThread();
	}

	public void message(T v) {
		sender.message(v);
	}

	public void message(T v, boolean undoable) {
		sender.message(v, undoable);
	}

	public T getValue() {
		return sender.getValue();
	}

	public void setValue(T v) {
		sender.setValue(v);
	}

	public void update() {
		sender.update();
	}

	public void disable(boolean b) {
		sender.disable(b);
	}

}
