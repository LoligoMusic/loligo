package gui;

import static util.Images.loadPImage;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import processing.core.PImage;


public class CheckBox extends GuiSenderDisplayObject<Boolean>{

	private static final int IMG_COLOR = 0xffaaaaaa;
	private static final PImage IMG_CHECK = loadPImage("/resources/check2.png"		, IMG_COLOR);
	private static final PImage IMG_EMPTY = loadPImage("/resources/check_empty.png"	, IMG_COLOR);
	
	
	public CheckBox(Consumer<Boolean> action, Supplier<Boolean> updateAction) {
		super(action, updateAction, false);
		setColor(COLOR_ACTIVE);
		setFlag(DisplayFlag.CLICKABLE, true);
		setFlag(DisplayFlag.RECT, true);
		setFlag(DisplayFlag.fixedToParent, true);
		setWH(20, 20);
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == InputEvent.CLICK) 
			message(!getValue());
	}
	
	
	@Override
	public void render(PGraphics g) {
		
		float x = getX() + getWidth()  / 2 - IMG_CHECK.width / 2,
			  y = getY() + getHeight() / 2 - IMG_CHECK.height / 2;
		
		g.image(IMG_EMPTY, x, y);
		
		if(getValue())
			g.image(IMG_CHECK, x, y);
	}


	@Override
	public void disable(boolean b) {
		super.disable(b);
		setColor(b ? color_disabled : COLOR_ACTIVE);
	}

}
