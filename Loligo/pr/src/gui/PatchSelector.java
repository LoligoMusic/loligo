package gui;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import lombok.Getter;
import lombok.var;
import patch.Domain;
import pr.DisplayContainerIF;
import pr.RootClass;

public class PatchSelector implements GuiSender<Domain>{

	private final GuiSenderObject<Domain> sender;
	
	private Domain patch;
	
	@Getter
	private final DropDownMenu patchMenu;
	
	private List<? extends Domain> patches = Collections.emptyList();
	
	
	public PatchSelector(Consumer<Domain> action, Supplier<Domain> updateAction) {
		
		patchMenu = new DropDownMenu(this::selectPatch, this::getPatchIndex, this::updatePatchList);
		patchMenu.singleThread();
		
		sender = new GuiSenderObject<>(this, action, updateAction, null);
		sender.onUpdate(g -> up());
	}
	
	private void up() {
		patchMenu.setValue(getPatchIndex());
	}
	
	private void selectPatch(int i) {
		var p = patches.get(i);
		selectPatch(p);
	}
	
	public void selectPatch(Domain p) {
		var patch = sender.getValue();
		if(patch != p) {
			patch = p;
			sender.setValue(p);
			sender.message(p);
			//patchMenu.update();//setValue(patches.indexOf(patch));
			
		}
	}
	
	public int getPatchIndex() {
		return patches.indexOf(patch);
	}
	
	private String[] updatePatchList() {
		patches = RootClass.backstage().getPatches();
		return patches.stream()
					  .map(this::labelPatch)
					  .toArray(String[]::new);
	}
	
	public String labelPatch(Domain p) {
		return p.getTitle();
	}

	
	@Override
	public Domain getValue() {
		return patch;
	}

	@Override
	public void setValue(Domain v) {
		patch = v;
		sender.setValue(v);
	}

	@Override
	public void update() {
		updatePatchList();
		
		sender.update();
		patchMenu.update();
	}

	@Override
	public void disable(boolean b) {
		sender.disable(b);
		patchMenu.disable(b);
	}

	@Override
	public DisplayContainerIF getDm() {
		return patchMenu.getDm();
	}
	
	@Override
	public void singleThread() {
		sender.singleThread();
	}
}
