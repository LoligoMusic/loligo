package gui;

import java.util.List;

import module.Module;
import module.ModuleContainer;
import patch.LoligoPatch;
import pr.RootClass;

public class Finder {

	public static void show(LoligoPatch patch) {
		
		ListObject li = outline(RootClass.mainPatch());
		
		patch.getDisplayManager().getInput().setRemovableTarget(li);
		
	}
	
	public static ListObject outline(ModuleContainer patch) {
		
		List<? extends Module> list = patch.getContainedModules();
		
		ListObject li = new ListObject();
		for (Module m : list) {
			if(m instanceof ModuleContainer) {
				ListObject sl = outline((ModuleContainer) m);
				li.addCascadeElement(m.getName(), sl);
			}else {
				li.addListElement(m.getName(), () -> select(m));
			}
		}
		
		return li;
	}
	
	public static void select(Module m) {
		//patch.getDisplayManager().getInput().selection.add(m.getGui());
	}
}
