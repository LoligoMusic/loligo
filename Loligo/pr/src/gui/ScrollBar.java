package gui;

import static constants.InputEvent.DRAG_STOP;
import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import util.Colors;

public abstract class ScrollBar extends DisplayObject implements DisplayGuiSender<Float>{
	
	protected final Scrollable target;
	protected final GuiSenderObject<Float> sender;
	
	@Getter@Setter
	private float offset, length, maxLength; 
	
	protected float x, y, lenFac, offFac, w, h, len, off;
	protected boolean dragging;
	
	public ScrollBar(Scrollable target, Consumer<Float> cons, Supplier<Float> sup) {
		
		sender = new GuiSenderObject<Float>(this, cons, sup, 0f);
		sender.singleThread();		
		
		this.target = target;
		setFlag(DisplayFlag.RECT, true);
		setFlag(DisplayFlag.CLICKABLE, true);
		setFlag(DisplayFlag.fixedToParent, true);
		setColor(0xff666666);
	}
	
	
	public void setScrollParams(float offset, float length, float maxLength) {
		this.offset = offset;
		this.length = length;
		this.maxLength = maxLength;
		
	}
	
	@Override
	public void render(PGraphics g) {
		
		x = getX();
		y = getY();
		lenFac = length / (maxLength);
		
		//offFac = offset /  (maxLength);
		
		w = getWidth();
		h = getHeight();
		
		
		g.fill(getColor());
		g.rect(x, y, w, h);
		
		g.fill(Colors.grey(255, 100));
		
		drawBar(g);
		
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == DRAG_STOP)
			dragging = false;
	}
	
	protected abstract void drawBar(PGraphics g);

	@Override
	public Float getValue() {
		return sender.getValue();
	}
	
	@Override
	public void setValue(Float v) {
		sender.setValue(v);
	}
	
	@Override
	public void update() {
		sender.update();
		offFac = sender.getValue();
	}
	
	@Override
	public void disable(boolean b) {
		
	}
	
	
	public static final class Vertical extends ScrollBar {

		public Vertical(Scrollable target) {
			super(target, target::setScrollPosY, target::getScrollPosY);
		}

		@Override
		protected void drawBar(PGraphics g) {
			off = offFac * (h - len);
			len = lenFac * h;
			g.rect(x, y + h - off - len, w, len);
		}
		
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			switch(e) {
			case CLICK -> {
				var in = getDm().getInput();
				float my = in.getMouseY() - y;
				//float p = h - off- len;
				
				if(!(my > off && my <= off + len)) {
					float f = my / (getHeight());
					f = max(0, min(1, f));
					target.setScrollPosY(1-f);
				}
			}
			case DRAG_START -> {
				var in = getDm().getInput();
				float my = getHeight() - (in.getMouseY() - y);
				if(my > off && my <= off + len) {
					dragging = true;
				}
			}
			case DRAG -> {
				if(dragging) {
					
					var in = getDm().getInput();
					float my = in.getMouseY() - y;
					
					float f = my / (getHeight());
					f = max(0, min(1, f));
					//target.setScrollPosY(1-f);
					sender.message(1 - f, false);
				}
			}
			default -> {}
			}
		}
		
		
		@Override
		public void update() {
			super.update();
			lenFac = target.getViewportLengthY();
		}
	}
	
	public static final class Horizontal extends ScrollBar {

		public Horizontal(Scrollable target) {
			super(target, target::setScrollPosX, target::getScrollPosX);
		}

		@Override
		protected void drawBar(PGraphics g) {
			off = offFac * w;
			len = lenFac * w;
			g.rect(x + off, y, len, h);
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			
			switch(e) {
			case CLICK -> {
				var in = getDm().getInput();
				float mx = in.getMouseX() - x;
				if(!(mx > off && mx <= off + len)) {
					float f = mx / getWidth();
					f = max(0, min(1, f));
					target.setScrollPosX(1-f);
				}
			}
			case DRAG_START -> {
				var in = getDm().getInput();
				float mx = in.getMouseX() - x;
				if(mx > off && mx <= off + len) {
					dragging = true;
				}
			}
			case DRAG -> {
				if(dragging) {
					var in = getDm().getInput();
					float mx = in.getMouseX() - x;
					
					float f = mx / (getWidth());
					f = max(0, min(1, f));
					//target.setScrollPosX(1-f);
					
					sender.message(1 - f, false);
				}
			}
			default -> {}
			}
		}
		
		
		@Override
		public void update() {
			super.update();
			lenFac = target.getViewportLengthX();
		}
		
	}
	
}
