package gui;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.sqrt;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pr.DisplayObjectIF;
import pr.Positionable;
import processing.core.PMatrix2D;


public class Position implements Positionable {
	
	public float x = 0;
	public float y = 0;
	
	public Position() {
		
	}
	
	public Position(float x, float y) {
		setPos(x, y);
	}
	
	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public void setPos(float x, float y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public Position getPosObject() {
		return this;
	}

	@Override
	public void setPosObject(Position p) {}
	
	public void setGlobal(float x, float y) {
		setPos(x, y);
	}
	
	@Override
	public String toString() {
		return "(" + getX() + ", " + getY() + ")";
	}
	
	static public class PositionOffset extends Position {
		
		protected final Positionable pos;
		
		public PositionOffset(Positionable pos) {
			this.pos = pos;
		}
		
		public PositionOffset(Positionable pos, float x, float y) {
			super(x, y);
			this.pos = pos;
		}
		
		@Override
		public float getX() {
			return x + pos.getX();
		}
		
		@Override
		public float getY() {
			return y + pos.getY();
		}
		
		@Override
		public void setGlobal(float x, float y) {
			setPos(x - pos.getX(), y - pos.getY());
		}
		
	}
	
	static public class PositionTarget extends Position {
		
		private final Positionable pos;
		private float ox, oy;
		
		public PositionTarget(Positionable p, float x, float y) {
			pos = p;
			ox = x;
			oy = y;
		}
		
		public PositionTarget(Positionable p) {
			this(p, 0, 0);
		}
		
		@Override
		public float getX() {
			return pos.getX() + ox;
		}
		
		@Override
		public float getY() {
			return pos.getY() + oy;
		}
		
		@Override
		public void setPos(float x, float y) {
			pos.setPos(x, y);
		}
		
		@Override
		public void setGlobal(float x, float y) {
			
			pos.getPosObject().setGlobal(x, y);
		}
	}
	
	static public class Bounds extends PositionOffset {
		
		protected int xMin, xMax, yMin, yMax;
		
		public Bounds(Positionable p, int w, int h) {
			super(p);
			setBounds(w, h);
		}
		
		final public void setBounds(int w, int h) {
			this.xMax = w;
			this.yMax = h;
		}
		
		final public void setBounds(int xMin, int xMax, int yMin, int yMax) {
			this.xMin = xMin;
			this.xMax = xMax;
			this.yMin = yMin;
			this.yMax = yMax;
		}
		
		@Override
		public void setPos(float x, float y) {
			x = max(xMin, min(x, xMax));
			y = max(yMin, min(y, yMax));
			super.setPos(x, y);
		}
	}
	
	static public class ParentBounds extends Bounds {
		
		private final DisplayObjectIF parent;
		
		public ParentBounds(DisplayObjectIF parent) {
			super(parent, parent.getWidth(), parent.getHeight());
			this.parent = parent;
		}
		
		@Override
		public void setPos(float x, float y) {
			xMax = parent.getWidth();
			yMax = parent.getHeight();
			super.setPos(x, y);
		}
	}

	public static class NestedPosition implements Positionable {
		
		private Position pos;
		
		public NestedPosition() {
			
			this(new Position());
		}
		
		public NestedPosition(Position pos) {
			
			this.pos = pos;
		}

		public float getX() {
			return pos.getX();
		}

		public float getY() {
			return pos.getY();
		}

		public void setPos(float x, float y) {
			pos.setPos(x, y);
		}

		public Position getPosObject() {
			return pos;
		}

		public void setPosObject(Position p) {
			pos = p;
		}

		public void setGlobal(float x, float y) {
			pos.setGlobal(x, y);
		}
		
		
	}
	
	@RequiredArgsConstructor
	public static class TransformPosition extends Position {
		
		@Getter
		private final Positionable pos;
		private final PMatrix2D matrix;
		
		@Override
		public float getX() {
			return matrix.multX(pos.getX(), pos.getY());
		}
		
		@Override
		public float getY() {
			return matrix.multY(pos.getX(), pos.getY());
		}
	}
	
	
	@AllArgsConstructor
	public static class Interpolate extends Position {
		
		@Getter
		private final Positionable pos1, pos2;
		@Getter@Setter
		private float fac;
		
		@Override
		public float getX() {
			float px = pos1.getX();
			return px + (pos2.getX() - px) * fac;
		}
		
		@Override
		public float getY() {
			float py = pos1.getY();
			return py + (pos2.getY() - py) * fac;
		}

	}
	
	
	public static class Circular extends PositionOffset {
		@Getter@Setter
		private float radius;
		private float xt, yt;
		
		public Circular(Positionable pos, float radius) {
			super(pos);
			this.radius = radius;
			x = radius;
		}
		
		@Override
		public void setPos(float x, float y) {
			float sum = x * x + y * y;
			if(sum > 0) {
				float s = radius / (float) sqrt(sum);
				xt = s * x;
				yt = s * y;
			}
			super.setPos(xt, yt);
		}
	}
	
	
}


