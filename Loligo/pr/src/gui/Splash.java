package gui;

import constants.DisplayFlag;
import patch.LoligoPatch;
import pr.DisplayManagerIF;
import util.Utils;

public class Splash extends ImageObject {

	public static void show(LoligoPatch patch) {
		
		Splash s = new Splash();
		
		DisplayManagerIF dm = patch.getDisplayManager();
		
		s.setPos(
			(dm.getWidth() - s.getWidth()) / 2, 
			(dm.getHeight() - s.getHeight()) / 2
		);
		
		dm.getInput().setRemovableTarget(s);
	}
	
	public Splash() {
		super("/resources/loligo_splash.jpg");
		
		Button link = new Button(() -> Utils.openWebpage("http://vanjacuk.de/loligo"), "");
		
		link.setFlag(DisplayFlag.fixedToParent, true);
		link.setPos(120, 225);
		link.setWH(160, 20);
		link.setButtonColor(0x00ffffff);
		link.setMouseOverButtonColor(0x49ffffff);
		addChild(link);
	}

	
	
}
