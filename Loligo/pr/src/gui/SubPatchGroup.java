package gui;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;
import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import module.Module;
import module.ModuleDeleteMode;
import module.Modules;
import module.inout.Input;
import module.inout.Output;
import patch.DomainType;
import patch.LoligoPatch;
import patch.subpatch.SubPatch;
import pr.Positionable;
import save.IOData;
import save.LinkData;
import save.ModuleData;
import save.PatchData;
import save.SaveDataContext;
import save.SaveMode;
import save.SaveTarget;

public final class SubPatchGroup {
	
	public static void group(LoligoPatch patch) {
		
		var g = new SubPatchGroup(patch);
		
		g.copyModules();
		
		if(g.modules.isEmpty())
			return;
		
		g.selectedToSubPatch();
		
		g.modules.forEach(m -> patch.removeModule(m, ModuleDeleteMode.NODE));
		
		patch.getDisplayManager().getInput().selection.add(g.subpatch.getGui());
		
	}
	
	
	
	private LoligoPatch patch;
	private List<Module> modules;
	
	private PatchData patchData;
	private ModuleData nodeIn, nodeOut;
	private SubPatch subpatch;
	
	
	private SubPatchGroup(LoligoPatch patch) {
		
		this.patch = patch;
	}
	
	private void copyModules() {
		
		modules = CopyPaste.getSelectedModules(patch);
		SaveDataContext sdc = new SaveDataContext(SaveMode.DUPLICATION);
		sdc.setSaveTarget(SaveTarget.MEMENTO);
		sdc.addModules(modules);
		
		patchData = sdc.buildPatchData();
		patchData.setPatchType(DomainType.SUBPATCH);
	}
	
	
	private void selectedToSubPatch() {
		
		var outNameMap1 = createNodeInLinks();
		var  outNameMap2 = createNodeOutLinks();
		
		createSubPatch();
		
		for(Output o : outNameMap1.keySet()) {
			Input in = Modules.getInputByName(subpatch, outNameMap1.get(o));
			patch.getModuleManager().connect(in, o);
		}
		
		for(Input in : outNameMap2.keySet()) {
			Output o = Modules.getOutputByName(subpatch, outNameMap2.get(in));
			patch.getModuleManager().connect(in, o);
		}
		
	}
	
	
	
	private Map<Input, String> createNodeOutLinks() {
		
		Set<Input> ins = modules.stream()
				.map	(Module::getOutputs)
				.filter	(Objects::nonNull)
				.flatMap(Collection::stream)
				.filter	(io -> io.getNumConnections() > 0)
				.map	(Output::getPartners)
				.flatMap(Collection::stream)
				.filter	(io -> !modules.contains(io.getModule()))
				.collect(Collectors.toSet());		
		
		Set<Output> outs = ins.stream()
				.map(Input::getPartner)
				.collect(Collectors.toSet());		
		
		addNodeOutData();
		
		
		Map<Input, String> inNameMap = new IdentityHashMap<>();
		int i = 0;
		for(Output o : outs) {
			i++;
			IOData iod = new IOData(o.getType().toString() + i, o.getType());
			nodeOut.getInputs().add(iod);
			
			ModuleData md = patchData.getNodes().stream()
					.filter(m -> m.getModuleIn() == o.getModule())
					.findAny().get();
			
			LinkData ld = new LinkData(md, o.getName(), nodeOut, iod.name);
			patchData.addLink(ld);
			
			ins.stream()
				.filter(in -> in.getPartner() == o)
				.forEach(in -> inNameMap.put(in, iod.name));
			
		}
		
		return inNameMap;
	}

	private Map<Output, String> createNodeInLinks() {
		
		var ins = modules.stream()
				.map	(Module::getInputs)
				.filter	(Objects::nonNull)
				.flatMap(Collection::stream)
				.filter	(io -> io.getNumConnections() > 0)
				.filter	(io -> !modules.contains(io.getPartner().getModule()))
				.collect(Collectors.toSet());
		
		var outs = ins.stream()
				.map(Input::getPartner)
				.collect(Collectors.toSet());
		
		
		addNodeInData();
		
		var outNameMap = new IdentityHashMap<Output, String>();
		
		int i = 0;
		for(Output o : outs) {
			i++;
			IOData iod = new IOData(o.getType().toString() + i, o.getType());
			nodeIn.getOutputs().add(iod);
			
			outNameMap.put(o, iod.name);
		}
		
		for(Input in : ins) {
			
			var md = patchData.getNodes().stream()
					.filter(m -> m.getModuleIn() == in.getModule())
					.findAny().get();
			
			String on = outNameMap.get(in.getPartner());
			LinkData ld = new LinkData(nodeIn, on, md, in.getName());
			patchData.addLink(ld);
		}
		
		return outNameMap;
	}
	
	
	private void addNodeInData() {
		nodeIn = new ModuleData();
		nodeIn.name = "In";
		nodeIn.setOutputs(new ArrayList<>());
		patchData.addNode(nodeIn);
	}
	
	private void addNodeOutData() {
		nodeOut = new ModuleData();
		nodeOut.name = "Out";
		nodeOut.setInputs(new ArrayList<>());
		patchData.addNode(nodeOut);
	}
	
	
	private void createSubPatch() {
		
		//subpatch = patch.getNodeRegistry().createInstance(SubPatch.class);
		subpatch = new SubPatch();
		subpatch.setName("SubPatch");
		patch.addModule(subpatch);
		
		doPositions();
		
		subpatch.restorePatch(patchData);
	}
	

	private void doPositions() {
		
		int minX = MAX_VALUE, maxX = MIN_VALUE, minY = MAX_VALUE, maxY = MIN_VALUE;
		int minX2 = MAX_VALUE, maxX2 = MIN_VALUE, minY2 = MAX_VALUE, maxY2 = MIN_VALUE; 
		
		for(ModuleData md : patchData.getNodes()) {
			if(md.getXy() != null) {
				minX = Math.min(md.getX(), minX);
				maxX = Math.max(md.getX(), maxX);
				minY = Math.min(md.getY(), minY);
				maxY = Math.max(md.getY(), maxY);
				
				if(md.getModuleIn() instanceof Positionable) {
					minX2 = Math.min(md.getX(), minX2);
					maxX2 = Math.max(md.getX(), maxX2);
					minY2 = Math.min(md.getY(), minY2);
					maxY2 = Math.max(md.getY(), maxY2);
				}
			}
		}
		
		minX -= 80;
		maxX += 80;
		int my = minY + (maxY - minY) / 2;
		nodeIn.setXY(minX, my);
		nodeOut.setXY(maxX, my);
		
		int margin = 100;
		patchData.setWidth (maxX - minX + 2 * margin);
		patchData.setHeight(maxY - minY + 2 * margin);
		
		int kx = margin - minX,
			ky = margin - minY;
		
		for(ModuleData md : patchData.getNodes()) {
			md.setXY(
				md.getX() + kx, 
				md.getY() + ky);
		}
		
		float ox = (minX2 + maxX2) / 2f,
			  oy = (minY2 + maxY2) / 2f;
		
		subpatch.setPos(ox, oy);
		subpatch.setOffsetX(ox + kx);
		subpatch.setOffsetY(oy + ky);
	}
}
