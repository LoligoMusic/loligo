package gui;

public abstract class ShortcutFactory {

	private ShortcutFactory next;
	
	public abstract void generateShortcuts(Shortcuts sr);
	
	public void  append(ShortcutFactory factory) {
		
		if(next == null)
			next = factory;
		else
			next.append(factory);
	}
}
