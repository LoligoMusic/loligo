package gui;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.jogamp.newt.event.KeyEvent;

import constants.InputEvent;
import patch.GuiDomain;
import patch.LoligoPatch;
import pr.EventSubscriber;
import pr.userinput.UserInput;


public class Shortcuts implements EventSubscriber{

	/**
	 * Bitflag constants for modifier keys 
	 */
	static public final int SHIFT = 1, CTRL = 2, ALT = 4; 
	static public final int CONTEXT = 1; // ignore shortcut if SelectionManager.activeElement != null
	
	private final List<Shortcut> shortcuts = new ArrayList<>();
	
	public Shortcuts(GuiDomain domain) {
		defaultShortcuts();
	}
	

	private final void defaultShortcuts() {
		
		new ShortcutFactory() {
			
			@Override
			public void generateShortcuts(Shortcuts sr) {
				
				for(UserAction a : UserAction.values()) {
					add(a.getShortcut());
				}
			}
		
		}.generateShortcuts(this);
	}
	
	
	public Shortcut add(int keyCode, int mods, Consumer<LoligoPatch> action) {
		var sc = new Shortcut(action, keyCode, mods, 0);
		add(sc);
		return sc;
	}
	
	
	public void add(Shortcut sc) {
		
		assert findShortcut(sc.keyCode, sc.mods) != null : "Keystroke already exists: " + sc;
		shortcuts.add(sc);
	}
	
	/*
	public Shortcut addShortcut(char key, int mods, Consumer<LoligoPatch> action) {
		
		return addShortcut(KeyEvent.getExtendedKeyCodeForChar(key), mods, action);
	}
	*/
	
	public Shortcut add(int keyCode, Consumer<LoligoPatch> action) {
		
		return add(keyCode, 0, action);
	}
	
	/*
	public Shortcut addShortcut(char key, Consumer<LoligoPatch> action) {
		
		return addShortcut(key, 0, action);
	}
	*/
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(e == InputEvent.KEY) {
			
			executeShortcut(
					(LoligoPatch) input.display.getDomain(), 
					input.getLastKeyCode(), 
					convertToBitflag(input.ctrl, input.alt, input.shift)
				);
		}
	}
	
	
	public int convertToBitflag(boolean ctrl, boolean alt, boolean shift) {
		
		return (ctrl ? CTRL : 0) | (alt ? ALT : 0) | (shift ? SHIFT : 0);
	}
	
	
	public void executeShortcut(LoligoPatch patch, int keyCode, int mods) {
		
		var s = findShortcut(keyCode, mods);
		
		if(s != null && s.action != null) {
			
			var in = patch.getDisplayManager().getInput();
			var a = in.selection.getActiveElement();
			
			boolean b = a != null && !a.getGuiType().isModule() || in.specialFocusMode();
			if(s.hasFlag(CONTEXT) && b) 
				return;
			
			s.action.accept(patch);
		}
		
	}
	
	Shortcut findShortcut(int keyCode, int mods) {
		
		return shortcuts.stream()
				.filter(s -> s.keyCode == keyCode && s.mods == mods)
				.findFirst()
				.orElse(null);
	}
	
	
	public record Shortcut(Consumer<LoligoPatch> action, int keyCode, int mods, int flags) {
		
		public void run(LoligoPatch p) {
			action.accept(p);
		}
		
		public boolean hasFlag(int flag) {
			return (flags & flag) == flag;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof Shortcut s) {
				if(s.keyCode == keyCode && s.mods == mods && s.flags == flags)
					return true;
			}
			return false;
		}
		
		boolean hasMod(int mod) {
			return (mods & mod) == mod;
		}
		
		@Override
		public String toString() {
		
			String m = "";
			if(hasMod(CTRL))
				m += "Ctrl+";
			if(hasMod(ALT))
				m += "Alt+";
			if(hasMod(SHIFT))
				m += "Shift+";
			
			if(keyCode == KeyEvent.NULL_CHAR)
				return m;
			
			String key = java.awt.event.KeyEvent.getKeyText(keyCode);
			key = key.replace("NumPad-", "F");
			key = keyCode == KeyEvent.VK_F11 ? "F11" : key;
			
			return m + key;
		}
		
	}
}
