package gui.particles;

import lombok.Getter;
import lombok.Setter;
import module.inout.NumericAttribute;


public class ModParam {

	@Getter
	private final String name;
	@Getter
	private final double defaultValue;
	@Getter@Setter
	private double value;
	@Getter
	private final NumericAttribute numType;
	
	public ModParam(String name, double defaultValue) {
		this(name, defaultValue, NumericAttribute.DEZIMAL_FREE);
	}
	
	public ModParam(String name, double defaultValue, NumericAttribute numType) {
		this.name = name;
		this.value = defaultValue;
		this.defaultValue = defaultValue;
		this.numType = numType;
	}
	
}
