package gui.particles;

import pr.DisplayObjectIF;

public interface Multiplicator {

	public float calcFac(Particle p, PositionableParticleModifier m);

	public DisplayObjectIF createGui();
}
