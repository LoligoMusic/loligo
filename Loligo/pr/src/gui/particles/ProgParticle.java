package gui.particles;

import java.util.List;

import constraints.Constrainable;
import gui.Position;
import gui.particles.prog.ParamHolderObject;
import gui.particles.prog.ParticleParam;
import gui.particles.prog.ParticleParamHolder;
import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.inout.IOType;
import patch.SubDomain;


public class ProgParticle extends ParticleImpl implements ParticleParamHolder {

	private SubDomain moduleBlock;
	
	@Getter@Setter
	private int color;
	@Getter@Setter
	private float size;
	@Getter
	private List<ParticleParam> params;
	
	
	public ProgParticle(ParticleSystem pSystem, List<ParticleParam> params) {
		
		super(pSystem);
		
		this.params = params;
	}
	
	@Override
	public void init() {
		
		super.init();
		moduleBlock.getSignalFlow().process();
	}
	
	public void setModuleBlock(SubDomain moduleBlock) {
		
		for (Module m : moduleBlock.getContainedModules()) {
			
			if(m instanceof Constrainable) {
				
				Constrainable p = (Constrainable) m;
				
				if(p.getContainingPosConstraint() == null)
					p.setPosObject(new Position.PositionTarget(this, 0, 0));
			}
		}
		
		this.moduleBlock = moduleBlock;
	}
	
	
	@Override
	public void kill() {
		
		super.kill();
		moduleBlock.removeFromContainer();
		
		/*
		RootClass.mainPatch().getSignalFlow().suspendBuild();
		
		
		for (Module m : moduleBlock.getContainedModules()) 
			
			moduleBlock.removeModule(m);
		
		RootClass.mainPatch().getSignalFlow().resumeBuild();
		*/
	}
	

	@Override
	public ParticleParam getParam(String name, IOType io) {
		
		for(var p : params) {
			if(p.getName().equals(name))
				return p;
		}
		
		return null;
	}
	

	@Override
	public void addParam(ParticleParam param, IOType io) {
		
		//throw new UnsupportedOperationException();
	}
	
	@Override
	public void removeParam(ParticleParam param, IOType io) {
		
		throw new UnsupportedOperationException();
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectIn() {
		return null;
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectOut() {
		return null;
	}
	
	@Override
	public List<ParticleParam> copyParams() {
		return ParamHolderObject.copyParams(params);
	}
	
	@Override
	public Particle getParticle() {
	
		return this;
	}


	


	


	
	
}
