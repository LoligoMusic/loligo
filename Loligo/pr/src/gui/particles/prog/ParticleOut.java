package gui.particles.prog;

import module.inout.DataType;
import module.inout.IOType;
import patch.DomainType;
import patch.IONode;
import save.ModuleData;
import save.SaveDataContext;

public class ParticleOut extends IONode.In{

	
	public ParticleOut(ParticleParamHolder paramHolder) {
		
		super(paramHolder);
		
		setName("ParticleOut");
	}

	@Override
	public ParticleParam createParam(String name, DataType type) {
		
		if(getDomain().domainType() == DomainType.PARTICLE_INSTANCE) {
			
			ParticleParam p = getParam(name, IOType.IN);
			p.createOutput(this);
			return p;
			
		}else {
			
			return super.createParam(name, type);
		}
	}
	
	@Override
	public void processIO() {
		
		paramHolder.getParams().forEach(ParticleParam::set);
	}
	
	@Override
	public void deleteParams() {
		// Don't run super.deleteParams()
		super.deleteParams();
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc);
		
		if(md.outputs != null) {
			md.outputs.forEach(o -> o.value = o.io.getValueObject());
		}
		//sdc.addParticeParams(md.outputs);
		return md;
	}

	/*
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		super.doDeserialize(data, rdc);
		
		getParams().forEach(p -> createParam(p.name, p.type));
		
	}
	*/
	
	
}
