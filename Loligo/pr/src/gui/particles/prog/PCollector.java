package gui.particles.prog;


import static java.lang.Double.MAX_VALUE;
import static java.lang.Double.MIN_VALUE;
import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import pr.ExitFrameListener;
import pr.RootClass;

public class PCollector implements ExitFrameListener{
	
	private static final List<PCollector> allCollectors = new ArrayList<>();
	//private static final ListMultimap<String, Collector> nodeMap = ArrayListMultimap.create();
	private static final Map<String, List<Collector>> nodeMap = new HashMap<>();
	
	
	static PCollector findCollector(String key, PCollector.Operation op) {
 		for (var c : allCollectors) {
			if(c.matches(key, op)) {
				addCollector(c);
				return c;
			}
		}
		var c = new PCollector(op, key);
		allCollectors.add(c);
		addCollector(c);
		return c;
	}
	
	private static void addCollector(PCollector c) {
		if(c.addUser())
			RootClass.mainDm().addExitFrameListener(c);
	}
	
	static void removeCollector(PCollector c) {
		if(c.removeUser()) {
			allCollectors.remove(c);
			RootClass.mainDm().removeExitFrameListener(c);
		}
	}
	
	public static void addNode(Collector c) {
		var li = nodeMap.computeIfAbsent(c.getKey(), k -> new ArrayList<>());
		li.add(c);
	}
	
	public static void removeNode(Collector c) {
		var li = nodeMap.get(c.getKey());
		if(li != null) {
			li.remove(c);
			if(li.isEmpty()) {
				nodeMap.remove(c.getKey());
			}
		}
	}
	
	public static String[] getKeys() {
		return nodeMap.keySet().toArray(String[]::new);
	}
	
	
	enum Operation {
		
		MIN{
			public double calc(List<Collector> li) {
				double v = MAX_VALUE;
				for (var c : li) 
					v = Math.min(v, c.value);
				return v;
		}},
		MAX	{
			public double calc(List<Collector> li) {
				double v = MIN_VALUE;
				for (var c : li) 
					v = Math.max(v, c.value);
				return v;
		}},
		AVERAGE {
			public double calc(List<Collector> li) {
				double v = 0;
				for (var c : li) 
					v += c.value;
				return v / li.size();
		}},
		CENTER {
			public double calc(List<Collector> li) {
				double mi = MAX_VALUE,  mx = MIN_VALUE;
				for (var c : li) {
					mi = min(mi, c.value);
					mx = max(mx, c.value);
				}
				return (mi + mx) / 2d;
		}},
		RANGE {
			public double calc(List<Collector> li) {
				double mi = MAX_VALUE, mx = MIN_VALUE;
				for (var c : li) {
					mi = min(mi, c.value);
					mx = max(mx, c.value);
				}
				return mx - mi;
		}};
		
		public PCollector create(String key) {
			return new PCollector(this, key);
		}
		
		abstract double calc(List<Collector> li);
	}
	
	
	@Getter
	private final Operation mode;
	@Getter
	private final String key;
	private int numUsers;
	private final List<Collector> collectors;
	@Getter
	private double value;
	
	public PCollector(Operation mode, String key) {
		this.mode = mode;
		this.key = key;
		collectors = nodeMap.get(key);
	}
	
	public boolean matches(String key, Operation mode) {
		return key.equals(this.key) && mode == this.mode;
	}
	
	public boolean addUser() {
		numUsers++;
		return numUsers == 1;
	}
	
	public boolean removeUser() {
		numUsers--;
		numUsers = max(0, numUsers);
		return numUsers == 0;
	}
	
	@Override
	public void exitFrame() {
		value = mode.calc(collectors);
	}
	
}
