package gui.particles.prog;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import module.Module;
import module.Modules;
import module.dynio.DynIOModule;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.Input;
import module.inout.Output;

@EqualsAndHashCode
public class ParticleParam {
	
	@JsonProperty@Getter@Setter
	private String name;
	@JsonProperty@Getter@Setter
	private Object value;
	@JsonProperty@Getter@Setter
	private DataType type;
	
	@Getter private Input io1;
	@Getter private Output io2;

	public ParticleParam() {
		
	}
	
	public ParticleParam(String name, DataType type, Object value) {
	
		this.name = name;
		this.value = value;
		this.type = type;
	}
	
	
	public void addInOut1(Input io) {
		
		io1 = io;
		get();
	}
	
	public void addInOut2(Output io) {
		
		io2 = io;
	}
	
	public Input createInput(Module m) {
		
		Input in = Modules.getInputByName(m, name);
		
		if(in == null) {
			in = getType().createInput(name, m);
			in.setOptional();
			m.addIO(in);
			Modules.initIOGUI(m, in);
		}
		addInOut1(in);
		return in;
	}
	
	public Output createOutput(Module m) {
		
		Output out = Modules.getOutputByName(m, name);
		
		if(out == null) {
			out = this.getType().createOutput(name, m);
			out.setOptional();
			m.addIO(out);
			Modules.initIOGUI(m, out);
		}
		addInOut2(out);
		return out;
	}
	
	
	
	public void rename(String name) {
		
		if(!name.equals(this.name)) {
			this.name = name;
			if(io1 != null)
				io1.setName(name);
			if(io2 != null)
				io2.setName(name);
		}
	}
	
	public ParticleParam copy() {
		
		return new ParticleParam(name, type, value);
	}
	
	public void set() {
		
		io2.setValueObject(value);
	}
	
	public void get() {
		
		io1.updateValue();
		value = io1.getValueObject();
	}
	
	public void transfer() {
		
		get();
		set();
	}

	public void forceValue(Object v) {
		
		if(v != null) {
			value = v;
			io1.setValueObject(v);
		}
	}
	
	public void deleteIOs() {
		var m1 = (DynIOModule) io1.getModule();
		m1.deleteDynIO(name, IOType.IN);
		var m2 = (DynIOModule) io2.getModule();
		m2.deleteDynIO(name, IOType.OUT);
	}
	
}
