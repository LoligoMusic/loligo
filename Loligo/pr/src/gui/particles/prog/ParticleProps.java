package gui.particles.prog;

import module.AbstractModule;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import save.SaveMode;


public abstract class ParticleProps extends AbstractModule {

	/*
	protected ProgParticle particle;
	
	protected ProgParticleFactory factory;
	*/
	protected final ParticleParamHolder paramHolder;
	
	
	public ParticleProps(ParticleParamHolder paramHolder) {
		
		this.paramHolder = paramHolder;
	}

	@Override
	public void postInit() {
		paramInit();
		super.postInit();
		
		//paramHolder = getDomain().domainType() == DomainType.MAIN ? particle : factory;
		
		
	}
	
	protected void paramInit() {
		
	}
	
	
	/*
	public ProgParticleFactory getFactory() {
		
		return factory;
	}
	
	
	public void setFactory(ProgParticleFactory factory) {
		
		this.factory = factory;
	}
	
	
	public void setParticle(ProgParticle particle) {
		
		this.particle = particle;
	}
	*/
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc);
		
		return md;
	}
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
	
		super.doDeserialize(data, rdc);
		
		if(rdc.getSaveMode() == SaveMode.DUPLICATION) {
			
			if(rdc instanceof RestoreDataContextParticle) {
				
				//RestoreDataContextParticle rdp = (RestoreDataContextParticle) rdc;
				//paramHolder = particle = rdp.getParticle();
				
				//setFactory();
			}
			/*
			setFactory(((ParticleProps)data.getModuleIn()).factory); // TODO not working after Shooter deserialized bc moduleIn == null
			particle = factory.getCurrentParticle();
			*/
		}
	}

}
