package gui.particles.prog;

import constants.Flow;
import gui.particles.Particle;
import module.inout.NumberInputIF;
import module.inout.TriggerInputIF;
import patch.DomainType;

public class ParticleIn extends ParticleProps {

	private final NumberInputIF in_force_dir, in_force;
	private final TriggerInputIF trigger_kill;
	private Particle particle;
	
	public ParticleIn(ParticleParamHolder paramHolder) {
		
		super(paramHolder);
		
		setName("ParticleIn");
		flowFlags.add(Flow.SINK);
		
		in_force = addInput("Force").setDefaultValue(.5);
		in_force_dir = addInput("Direction");
		trigger_kill = addTrigger();
		particle = paramHolder.getParticle();
	}
	
	
	@Override
	public void processIO() {
		
		if(getDomain().domainType() == DomainType.PARTICLE_INSTANCE) {
		
			double f = in_force.getInput() * 10,
				   d =  (in_force_dir.getInput() * 2 * Math.PI);
			
			particle.setDx((float) (Math.sin(d) * f));
			particle.setDy((float) (Math.cos(d) * f));
			
			if(trigger_kill.triggered()) {
				
				particle.setKillFlag();
				
			}
			
		}
	}

}
