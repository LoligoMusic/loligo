package gui.particles.prog;

import static module.inout.NumericType.toBool;
import static patch.DomainType.PARTICLE_FACTORY;

import module.inout.NumberInputIF;
import module.inout.NumericAttribute;

public class ParticleLife extends ParticleProps{

	private final NumberInputIF in_kill;
	
	public ParticleLife(ParticleParamHolder paramHolder) {
		super(paramHolder);
		
		in_kill = addInput("Kill", NumericAttribute.BANG);
	}

	@Override
	public void processIO() {
		
		if(getDomain().domainType() != PARTICLE_FACTORY && toBool(in_kill.getInput())) {
			paramHolder.getParticle().setKillFlag();
		}
	}

}
