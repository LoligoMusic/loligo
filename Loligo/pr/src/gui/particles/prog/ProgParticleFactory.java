package gui.particles.prog;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

import gui.particles.DefaultParticleFactory;
import gui.particles.Particle;
import gui.particles.ProgParticle;
import lombok.var;
import module.Module;
import module.inout.IOType;
import patch.Domain;
import patch.PatchCloner;
import patch.SubDomain;
import save.PatchData;
import save.SaveJson;


public class ProgParticleFactory extends DefaultParticleFactory 
								 implements ParticleParamHolder
										{

	private PatchCloner patchCloner;
	private ProgParticle currentParticle;
	private ParticleParamHolder paramHolder;
	
	public ProgParticleFactory(ParticleParamHolder paramHolder) {
		
		this.paramHolder = paramHolder;
		patchCloner = new PatchCloner();
		//patchCloner.setAudioManager(getParticleSystem().getParticleAudioManager());
		//createPatchWindow();
		
	}
	
	
	@Override
	protected Particle instance() {
		
		List<ParticleParam> params = getParams();
		PatchData pd = patchCloner.getPatchData();
		
		List<ParticleParam> pcopy = new ArrayList<>(params.size());
		for (ParticleParam p : params) {
			var pp = p.copy();
			pcopy.add(pp);
		}
		
		pd.setParticleParams(pcopy);
		
		currentParticle = new ProgParticle(getParticleSystem(), pcopy);
		
		//initValues(currentParticle);
		
		//currentParticle.setColor(getColor());
		//currentParticle.setSize(getSize());
		
		//checkUpdate();
		
		patchCloner.setRestoreDataContextSupplier(
				(p, d) -> new RestoreDataContextParticle(p, d, currentParticle));
		
		SubDomain sd = patchCloner.createInstance(currentParticle);
		
		currentParticle.setModuleBlock(sd);
		
		
		return currentParticle;
	}

	@Override
	public List<ParticleParam> copyParams() {
		
		return paramHolder.copyParams();
	}
	
	
	public void removeModuleBlock(SubDomain sd) {
		
		patchCloner.removeModuleBlock(sd);
	}
	
	public void createPatchWindow() {
		
		patchCloner.createPatchWindow(this);
	}
	
	public ProgParticle getCurrentParticle() {
		return currentParticle;
	}
	
	
	public PatchData getMasterCopy() {
		
		return patchCloner.getPatchData();
	}
	
	public void setMasterCopy(PatchData patchData) {
		
		patchCloner.setPatchData(patchData);
		
		var ios = patchData.getParticleParams();
		if(ios != null) {
			for (ParticleParam io : ios) {
				var p = getParamHolderObjectIn().addParam(io.getName(), IOType.IN, io.getType());
				p.forceValue(io.getValue());
			}
			
		}
		
	}
	
	public void insertDefaultProgram() {
		
		try {
			
			var is = ProgParticleFactory.class.getResourceAsStream("DefaultPatch.json");
			
			String js = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
			
			PatchData pd = SaveJson.parseJsonToPatch(js);
			setMasterCopy(pd);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}


	public Domain getPatch() {
		
		return patchCloner.getPatch();
	}
	
	
	@Override
	public ParticleParam getParam(String name, IOType io) {
		
		return paramHolder.getParam(name, io);
	}

	@Override
	public List<ParticleParam> getParams() {
	
		return paramHolder.getParams();
	}
	
	@Override
	public void addParam(ParticleParam param, IOType io) {
		
	}
	
	@Override
	public void removeParam(ParticleParam param, IOType io) {
		
	}

	@Override
	public Particle getParticle() {
		
		return null;
	}

	@Override
	public Module module() {
	
		return paramHolder.module();
	}

	@Override
	public ParamHolderObject getParamHolderObjectIn() {
		return paramHolder.getParamHolderObjectIn();
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectOut() {
		return paramHolder.getParamHolderObjectOut();
	}
	
	public void dispose() {
		
		patchCloner.dispose();
	}

	public void setUpdate() {
		patchCloner.setUpdate();
	}
	
	
}
