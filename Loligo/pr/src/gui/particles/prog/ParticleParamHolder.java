package gui.particles.prog;

import gui.particles.Particle;


public interface ParticleParamHolder extends ParamHolder {
	
	Particle getParticle();
}
