package gui.particles.prog;

import constants.Flow;
import constants.Units;
import gui.particles.Particle;
import gui.particles.ParticleSystem;
import module.inout.NumericAttribute;
import module.inout.NumberOutputIF;

public class ParticleInfo extends ParticleProps {

	private final NumberOutputIF out_lifeTime, out_speed, out_dir, out_id, out_x, out_y;
	
	private final Particle particle;
	
	
	public ParticleInfo(ParticleParamHolder paramHolder) {
		
		super(paramHolder);
		
		flowFlags.add(Flow.SOURCE);
		
		out_lifeTime = addOutput("Lifetime");
		out_x = addOutput("X");
		out_y = addOutput("Y");
		out_speed = addOutput("Speed");
		out_dir = addOutput("Dir");
		out_id = addOutput("ID", NumericAttribute.INTEGER_POSITIVE);
		out_id.setValue(ParticleSystem.getCount());
		
		particle = paramHolder.getParticle();
		
		setName("Info");
	}
	

	@Override
	public void processIO() {
		
		if(particle != null) {
			
			out_lifeTime.setValue(
					(double)particle.getLifeTime() / 
					((double)particle.getMaxLifeTime() + 1)
					);
			
			out_x.setValue(particle.getX() / Units.LENGTH.normFactor);
			out_y.setValue(particle.getY() / Units.LENGTH.normFactor);
			
			float dx = particle.getDx(),
				  dy = particle.getDy();
			
			out_speed.setValue(Math.sqrt(dx * dx + dy * dy));
			out_dir.setValue((Math.atan2(dy, dx) + Math.PI) / (Math.PI * 2));
		}
		
	}
	
}
