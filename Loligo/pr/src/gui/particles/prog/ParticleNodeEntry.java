package gui.particles.prog;

import java.util.function.Function;

import module.Module;
import module.loading.AbstractNodeEntry;
import module.loading.NodeCategory;

public class ParticleNodeEntry extends AbstractNodeEntry{

	private final Function<ParticleParamHolder, Module> foo;
	
	public ParticleNodeEntry(Function<ParticleParamHolder, Module> foo, String name, boolean hidden) {
		
		super(name, NodeCategory.PARTICLE, hidden);
		
		this.foo = foo;
	}

	
	public Module createInstance(ParticleParamHolder pm) {
		
		return foo.apply(pm);
	}
	
	
	@Override
	public Module createInstance() {
		
		return foo.apply(null);
	}


}
