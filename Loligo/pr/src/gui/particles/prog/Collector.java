package gui.particles.prog;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import gui.nodeinspector.NodeInspector;
import gui.text.TextInputField;
import lombok.Getter;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.inout.NumberInputIF;
import patch.DomainType;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

public class Collector extends AbstractModule {

	private final NumberInputIF in;
	double value;
	@Getter
	private String key;
	
	public Collector() {
		
		setName("Collect");
		
		in = addInput("In", DEZIMAL_FREE);
		
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		setKey("A");
	}
	
	public void setKey(String key) {
		if(!key.equals(this.key)) {
			PCollector.removeNode(this);
			this.key = key;
			PCollector.addNode(this);	
		}
	}
	
	
	@Override
	public void onAdd() {
		if(getDomain().domainType() != DomainType.PARTICLE_FACTORY)
			PCollector.addNode(this);
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		PCollector.removeNode(this);
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
	
		var n = NodeInspector.newInstance(this, ni -> {
			
			var ti = new TextInputField(k -> setKey(k.trim()), this::getKey);
			ni.addLabeledEntry("Key", ti);
			
		});
		return n;
	}

	
	@Override
	public void processIO() {
		value = in.getWildInput();
	}
	

	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc).addExtras(key);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		setKey(data.nextString());
	}
	
}
