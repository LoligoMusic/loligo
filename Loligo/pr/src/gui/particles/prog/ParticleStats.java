package gui.particles.prog;

import static java.util.Arrays.asList;
import static module.inout.NumericAttribute.DEZIMAL_FREE;

import java.util.function.Consumer;
import java.util.function.Supplier;

import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.inout.NumberOutputIF;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Utils;

public class ParticleStats extends AbstractModule {
	
	private final NumberOutputIF out;
	private PCollector collectOperation;
	
	public ParticleStats() {
		
		out = addOutput("Out", DEZIMAL_FREE);
	}
	
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		setCollector("A", PCollector.Operation.AVERAGE);
	}
	
	
	public void setCollector(String key, PCollector.Operation pc) {
		if(collectOperation != null)
			PCollector.removeCollector(collectOperation);
		collectOperation = PCollector.findCollector(key, pc);
	}

	
	@Override
	public NodeInspector createGUISpecial() {

		var ni = NodeInspector.newInstance(this, n -> {
			
			Consumer<Integer> cons = i -> {
				String s = PCollector.getKeys()[i];
				setCollector(s, collectOperation.getMode());
			};
			Supplier<Integer> su = () -> {
				var ks = asList(PCollector.getKeys());
				return ks.indexOf(collectOperation.getKey());
			};
			var mc = new DropDownMenu(cons, su, PCollector::getKeys);
			
			n.addLabeledEntry("Collector", mc);
			
			
			Consumer<Integer> con = i -> {
				var pc = PCollector.Operation.values()[i];
				setCollector(collectOperation.getKey(), pc);
			};
			Supplier<Integer> sup = () -> collectOperation.getMode().ordinal();
			
			var mds = Utils.formatedEnumString(PCollector.Operation.values());
		
			var mo = new DropDownMenu(con, sup, mds);
			n.addLabeledEntry("Operation", mo);
		});

		return ni;
	}
	
	
	@Override
	public void processIO() {
		
		out.setValue(collectOperation.getValue());
	}

	
	@Override
	public void onAdd() {
		
	}

	
	@Override
	public void onDelete(ModuleDeleteMode m) {
		PCollector.removeCollector(collectOperation);
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
			.addExtras(
				collectOperation.getKey(), 
				collectOperation.getMode().name()
			);
	}


	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		var k = data.nextString();
		var s = data.nextString();
		var o = PCollector.Operation.valueOf(s);
		setCollector(k, o);
	}
	
}
