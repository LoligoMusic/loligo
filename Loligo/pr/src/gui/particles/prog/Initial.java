package gui.particles.prog;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import constants.Units;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import patch.DomainType;

@NodeInfo(type = NodeCategory.PARTICLE)
public class Initial  extends ParticleProps{

	private final NumberInputIF in_x, in_y, in_dir, in_speed;
	
	public Initial(ParticleParamHolder paramHolder) {
		super(paramHolder);
		
		in_x = addInput("X", NumericAttribute.DEZIMAL_FREE);
		in_y = addInput("Y", NumericAttribute.DEZIMAL_FREE);
		in_dir = addInput("Angle", NumericAttribute.DEZIMAL_FREE);
		in_speed = addInput("Speed", NumericAttribute.DEZIMAL_FREE);
	}

	@Override
	public void processIO() {
		
		if(getDomain().domainType() != DomainType.PARTICLE_FACTORY) {
			
			var p = paramHolder.getParticle();
			
			if(p.getLifeTime() == 0) {
				
				float dir2 = (float) (in_dir.getWildInput() * Math.PI);
				float dx = p.getDx();
				float dy = p.getDy();
				float dir = dir2 + (float) atan2(dy, dx);
				float speed = (float) (20f * in_speed.getWildInput() + sqrt(dx * dx + dy * dy));
				float dx2 = (float) sin(dir) * speed;
				float dy2 = (float) cos(dir) * speed;
				
				p.setDx(dx2);
				p.setDy(dy2);
				
				p.setPos(
					p.getX() + Units.LENGTH.normFactorF * (float) in_x.getWildInput(), 
					p.getY() + Units.LENGTH.normFactorF * (float) in_y.getWildInput()
				);
			}
		}
	}

}
