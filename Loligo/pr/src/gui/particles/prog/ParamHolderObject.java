package gui.particles.prog;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import module.dynio.DynIOModules;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;

public class ParamHolderObject {

	@Getter
	private final List<ParticleParam> params = new ArrayList<>();

	@Getter
	@Setter
	private ParamHolder paramHolder;

	
	public ParticleParam getParam(String name) {

		for (ParticleParam p : params) {

			if (p.getName().equals(name))
				return p;
		}

		return null;
	}

	public ParticleParam getParam(InOutInterface io) {
		for (ParticleParam p : params) {
			if (p.getIo1() == io || p.getIo2() == io)
				return p;
		}
		return null;
	}
	
	public ParticleParam addParam(String name, IOType io, DataType type) {
		
		ParticleParam pa = getParam(name);

		boolean b = pa == null;
		if (b) {

			pa = new ParticleParam(name, type, null);

		}

		paramHolder.addParam(pa, io);
		
		if (b)
			params.add(pa);

		return pa;
	}
	

	public List<ParticleParam> copyParams() {
		return copyParams(params);
	}

	public static List<ParticleParam> copyParams(List<ParticleParam> pp1) {
		
		ArrayList<ParticleParam> pp = new ArrayList<>(pp1.size());

		for(ParticleParam p : pp1) {
			pp.add(p.copy());
		}
		return pp;
	}

	public void removeParam(ParticleParam param, IOType ioType) {

		if (params.remove(param)) {

			paramHolder.removeParam(param, ioType);
			
		}
	}

	public ParticleParam removeParam(String name, IOType ioType) {
		var p = getParam(name);
		removeParam(p, ioType);
		return p;
	}

	public void transfer() {
		for (ParticleParam p : params) {
			p.transfer();
		}
	}
	
	public String rename(InOutInterface io, String name) {
		
		String n = DynIOModules.tryRenameIO(io, name);
		var p = getParam(io);
		
		if(p != null) {
			p.rename(n);
		}
		return n;
	}

}
