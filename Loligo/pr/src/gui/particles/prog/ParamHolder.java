package gui.particles.prog;

import java.util.Collection;
import java.util.List;

import module.Module;
import module.inout.IOType;

public interface ParamHolder {


	public static ParticleParam getParam(Collection<ParticleParam> col, String name, IOType io) {
		
		for (ParticleParam p : col) 
			
			if(p.getName().equals(name)) 
				return p;
			
		return null;
	}
	
	List<ParticleParam> getParams();
	
	ParticleParam getParam(String name, IOType io);
	
	void addParam(ParticleParam param, IOType io); 

	List<ParticleParam> copyParams();
	
	default Module module() {return null;}


	void removeParam(ParticleParam param, IOType io);
	
	ParamHolderObject getParamHolderObjectIn();
	
	ParamHolderObject getParamHolderObjectOut();
	
	//void removeParam(String name);
	
	//void removeParam(ParticleParam<?> p);
	
}
