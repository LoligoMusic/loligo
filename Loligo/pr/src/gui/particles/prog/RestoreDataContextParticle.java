package gui.particles.prog;

import gui.particles.ProgParticle;
import lombok.Getter;
import patch.Domain;
import save.PatchData;
import save.RestoreDataContext;

public class RestoreDataContextParticle extends RestoreDataContext{

	@Getter
	private final ProgParticle particle;
	
	public RestoreDataContextParticle(PatchData patch, Domain domain, ProgParticle particle) {
		
		super(patch, domain);
		
		this.particle = particle;
	}
}
