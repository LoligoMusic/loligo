package gui.particles.prog;

import static constants.Units.LENGTH;

import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import gui.particles.Particle;
import lombok.Getter;
import lombok.Setter;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.SwitchInputIF;
import patch.DomainType;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

public class ParticleMove extends ParticleProps {

	private final SwitchInputIF in_switch;
	private final NumberInputIF in_x, in_y;
	@Getter@Setter
	private int mode = 0;
	
	
	public ParticleMove(ParticleParamHolder paramHolder) {
		super(paramHolder);
		
		setName("Move");
		in_switch = addSwitch();
		in_x = addInput("X", NumericAttribute.DEZIMAL_FREE);
		in_y = addInput("Y", NumericAttribute.DEZIMAL_FREE);
	}


	@Override
	public NodeInspector createGUISpecial() {
	
		NodeInspector ni = NodeInspector.newInstance(this, n -> {
				String[] ss = {"Position", "Speed", "Force"};
				DropDownMenu dm = new DropDownMenu(this::setMode, this::getMode, ss);
				n.addLabeledEntry("Mode", dm);
			}
		);
		
		return ni;
	}


	@Override
	public void processIO() {
		
		if(getDomain().domainType() != DomainType.PARTICLE_FACTORY && in_switch.getSwitchState()) {
		
			Particle p = paramHolder.getParticle();
			float x = (float) in_x.getWildInput(), y = (float) in_y.getWildInput();
			
			switch(mode) {
			case 0:
			p.setPos(x * LENGTH.normFactorF, y * LENGTH.normFactorF);
			break;
			case 1:
			p.setDx(x * 20);
			p.setDy(y * 20);
			break;
			case 2:
			p.addDx(x);	
			p.addDy(y);
			break;
			}
		}
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
				.addExtras(mode);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		mode = data.nextInt();
	}

	

}
