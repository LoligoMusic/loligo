package gui.particles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import pr.DisplayManagerIF;
import pr.RootClass;


public class ParticleSystemRegistry {

	public static final String defaultName = "@default";
	
	private static ParticleSystemRegistry instance;
	
	private final List<ParticleSystem> particleSystems = new ArrayList<>();
	
	
	private ParticleSystemRegistry() {
		
		//particleSystems.add(new ParticleSystem(Loligo.PROC.display, defaultName));
	}
	
	
	public static ParticleSystemRegistry getInstance() {
		
		if(instance == null) {
			instance = new ParticleSystemRegistry();
			
		}
		
		return instance;
	}
	
	
	public static List<ParticleSystem> getParticleSystems() {
		
		return Collections.unmodifiableList(getInstance().particleSystems);
	}
	
	
	public static int numParticleSystems() {
		
		return instance == null ? 0 : instance.particleSystems.size();
	}
	
	
	public static ParticleSystem getOrCreateParticleSystem(DisplayManagerIF dm, String name) {
		
		Optional<ParticleSystem> ps =
		
			getInstance().particleSystems.stream()
				.filter(s -> {return s.getName().equals(name);})
				.findAny();
		
		if(ps.isPresent())
			return ps.get();
		
		ParticleSystem p = new ParticleSystem(dm, name);
		
		//p.addModifier(new Drag());
		
		instance.particleSystems.add(p);
		
		return p;
	}
	
	
	public static ParticleSystem defaultParticleSystem() {
		
		return getOrCreateParticleSystem(RootClass.mainDm(), defaultName);
	}

}
