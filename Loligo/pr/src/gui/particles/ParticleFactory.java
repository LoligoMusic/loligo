package gui.particles;

public interface ParticleFactory {

	public Particle createParticle();

}
