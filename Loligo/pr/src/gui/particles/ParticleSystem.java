package gui.particles;

import java.util.ArrayList;
import java.util.List;

import audio.AudioOut;
import lombok.Getter;
import pr.DisplayManagerIF;
import pr.EnterFrameListener;

public class ParticleSystem implements EnterFrameListener {
	
	private final DisplayManagerIF dm;
	
	private final String name;
	
	private final List<Particle> particles = new ArrayList<>();
	private final List<PositionableParticleModifier> modifiers = new ArrayList<>();
	
	private ParticleFactory factory;
	
	@Getter
	private static int count;
	
	@Getter
	private AudioOut particleAudioManager;
	
	public ParticleSystem(DisplayManagerIF dm, String name) {
		
		this.dm = dm;
		this.name = name;
		
		particleAudioManager = new AudioOut();
		particleAudioManager.connect();
		//particleAudioManager.initAudio();
	}
	
	
	public void addParticle() {
		
		addParticle(factory.createParticle());
	}
	
	
	public void addParticle(ParticleFactory pf) {
		
		addParticle(pf.createParticle());
	}
	

	public void addParticle(final Particle p) {
		
		if(particles.isEmpty()) {
			
			dm.addEnterFrameListener(this);
		}
		
		particles.add(p);
		
		dm.addAnimObject(p.getDisplayObject());
		
		count++;
	}
	
	
	public void addModifier(final PositionableParticleModifier pm) {
		
		for (int i = 0; i < modifiers.size(); i++) {
			
			if(pm.priority() >= modifiers.get(i).priority()) {
				
				modifiers.add(i, pm);
				return;
			}
		}
		modifiers.add(pm);		
	}
	
	
	public void removeModifier(final PositionableParticleModifier pm) {
		
		modifiers.remove(pm);
	}
	
	
	public void step() {
		
		for (int i = particles.size() - 1; i >= 0 ; i--) {
			
			Particle p = particles.get(i); 
			
			if(p.hasDied()) {
				killParticle(i);
				continue;
			}
			
			//for (ParticleModifier mod : modifiers) 
			//	mod.modify(p);
			
			p.step();
			
		}
	}
	
	
	private void killParticle(int index) {
		
		Particle p = particles.remove(index);
		
		if(particles.isEmpty())
			dm.removeEnterFrameListener(this);
		
		p.kill();
	}
	
	
	public void killParticle(Particle p) {
		
		killParticle(particles.indexOf(p));
	}
	
	public void clearParticles() {
		
		while(!particles.isEmpty()) {
			
			killParticle(particles.size() - 1);
		}
		
	}
	
	
	@Override
	public void enterFrame() {
		
		step();
	}
	
	
	public List<Particle> getParticles() {
		
		return particles;
	}
	
	
	public ParticleFactory getFactory() {
		return factory;
	}

	public void setFactory(ParticleFactory factory) {
		this.factory = factory;
	}

	public DisplayManagerIF getDm() {
		return dm;
	}

	public List<PositionableParticleModifier> getModifiers() {
		return modifiers;
	}
	
	public int numParticles() {
		
		return particles.size();
	}
	
	public String getName() {
		
		return name;
	}
}
