package gui.particles;

import constants.DisplayFlag;
import pr.DisplayObject;

public class DefaultParticleFactory implements ParticleFactory {
	
	private ParticleSystem pSystem;
	private float initX, initY, dx, dy, size;
	private int color = 0xffffffff;
	private int maxLifeTime = 50;
	
	
	public void initParticle(Particle p) {
		
		DisplayObject d = new DisplayObject();
		initDisplayObject(d);
		p.setDisplayObject(d);
		initValues(p);
		p.init();
	}
	
	public void initDisplayObject(DisplayObject d) {
		
		d.setFlag(DisplayFlag.ROUND, true);
		d.setFlag(DisplayFlag.CLICKABLE, false);
		d.setPos(initX, initY);
		d.setWH((int)size, (int)size);
		d.setColor(color);
	}
	
	public void initValues(Particle p) {
		
		p.setMaxLifeTime(maxLifeTime);
		p.setPos(initX, initY);
		p.setDx(dx);
		p.setDy(dy);
		p.setColor(color);
		p.setSize(size); 
	}
	
	@Override
	public Particle createParticle() {
		
		Particle p = instance();
		
		initParticle(p);
		
		return p;
	}

	protected Particle instance() {
		
		return new ParticleImpl(pSystem);
	}
	
	public float getInitX() {
		return initX;
	}

	public DefaultParticleFactory setParticleSystem(ParticleSystem pSystem) {
		
		this.pSystem = pSystem;
		return this;
	}
	
	public ParticleSystem getParticleSystem() {
		
		return pSystem;
	}
	
	public DefaultParticleFactory setInitX(float initX) {
		
		this.initX = initX;
		return this;
	}

	public float getInitY() {
		return initY;
	}

	public DefaultParticleFactory setInitY(float initY) {
		this.initY = initY;
		return this;
	}

	public float getDx() {
		return dx;
	}

	public DefaultParticleFactory setDx(float dx) {
		this.dx = dx;
		return this;
	}

	public float getDy() {
		return dy;
	}

	public DefaultParticleFactory setDy(float dy) {
		this.dy = dy;
		return this;
	}

	public int getColor() {
		return color;
	}

	public DefaultParticleFactory setColor(int color) {
		this.color = color;
		return this;
	}

	public int getMaxLifeTime() {
		return maxLifeTime;
	}

	public DefaultParticleFactory setMaxLifeTime(int maxLifeTime) {
		this.maxLifeTime = maxLifeTime;
		return this;
	}

	public int getSize() {
		return (int) size;
	}

	public DefaultParticleFactory setSize(float size) {
		this.size = size;
		return this;
	}

}
