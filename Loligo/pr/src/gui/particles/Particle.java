package gui.particles;

import java.util.List;

import gui.particles.prog.ParticleParam;
import pr.DisplayObjectIF;
import pr.Positionable;


public interface Particle extends Positionable{

	void init();

	void setDisplayObject(DisplayObjectIF d);

	DisplayObjectIF getDisplayObject();

	int getMaxLifeTime();

	void setMaxLifeTime(int maxLifeTime);

	int getLifeTime();

	void setLifeTime(int lifeTime);

	float getDx();

	float getDy();
	
	void setDx(float x);
	
	void setDy(float y);
	
	void addDx(float x);
	
	void addDy(float y);
	
	void setColor(int c);
	
	int getColor();

	void step();

	void move();

	void applyModifiers(List<PositionableParticleModifier> mods);

	int getLayerMask();
	
	void setLayerMask(int mask);
	
	boolean hasDied();

	/**
	 * Indicates to the ParticeSystem to kill this Particle
	 */
	void setKillFlag();
	
	/**
	 * Disposes particle. Don't call directly, use {@link #setKillFlag}
	 */
	void kill();
	
	void setSize(float size);

	float getSize();
	
	List<ParticleParam> getParams();

}