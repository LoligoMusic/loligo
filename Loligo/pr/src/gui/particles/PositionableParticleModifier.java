package gui.particles;

import java.util.Arrays;

import constants.Units;
import gui.Position;
import lombok.Getter;
import lombok.Setter;
import module.inout.NumericAttribute;
import pr.DisplayObjectIF;

public abstract class PositionableParticleModifier {

	public static final int defaultPriority = 10;
	
	private Position pos = new Position();
	@Getter@Setter
	private DistanceMask multiplicator = new DistanceMask();
	@Getter
	private final String name;
	@Getter@Setter
	private int layerMask = LayerMask.DEFAULT_LAYERMASK;
	
	private ModParam[] params;
	
	/**
	 * Highest priority Modifier gets applied first
	 */
	protected int priority = defaultPriority;
	
	
	public PositionableParticleModifier() {
		this("Unnamed Modifier");
	}
	
	public PositionableParticleModifier(String name) {
		
		this.name = name;
	}
	
	public float multiplicator(Particle p) {
		
		return multiplicator.calcFac(p, this);
	}
	
	abstract public void modify(Particle p);
	
	
	public void tryModify(Particle p) {
		
		if(LayerMask.sharesLayer(p, this))
			modify(p);
	}
	
	public DisplayObjectIF createGui() {
		
		return multiplicator.createGui();
	}
	
	protected void setParams(ModParam...params) {
		
		params = Arrays.copyOf(params, params.length + 1);
		
		params[params.length - 1] = new ModParam("Distance", 1, NumericAttribute.DEZIMAL_POSITIVE) {
			@Override
			public void setValue(double value) {
				
				super.setValue(value);
				multiplicator.setMaxDistance((float) (value * Units.LENGTH.normFactor)); 
			}
		};
		
		
		this.params = params;
	}
	
	public ModParam[] getParams() {
		return params;
	}
	
	public int priority() {
		return priority;
	}
	
	public float getX() {
		return pos.getX();
	}

	public float getY() {
		return pos.getY();
	}

	public void setPos(float x, float y) {
		pos.setPos(x, y);
	}

	public Position getPosObject() {
		return pos;
	}

	public void setPosObject(Position p) {
		pos = p;
	}

	public void setGlobal(float x, float y) {
		pos.setGlobal(x, y);
	}
	
	
	
}
