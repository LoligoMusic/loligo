package gui.particles;

public final class LayerMask {

	private LayerMask() {}
	
	public static final int 
		BIT_ENABLE = 1 << 31, 
		DEFAULT_LAYERMASK = 0xffffffff, 
		DEFAULT_MODIFIERMASK = 0xffffffff;
	
	
	
	public static boolean isEnabled(PositionableParticleModifier p) {
		return isEnabled(p.getLayerMask());
	}
	
	public static boolean isEnabled(Particle p) {
		return isEnabled(p.getLayerMask());
	}
	
	public static boolean isEnabled(int m) {
		return ((m >> 31) & 1) == 1;
	}
	
	public static boolean sharesLayer(Particle p, PositionableParticleModifier mod) {
		return sharesLayer(mod.getLayerMask(), p.getLayerMask());
	}
	
	public static boolean sharesLayer(int m1, int m2) {
		int m = m1 & m2;
		return (m ^ BIT_ENABLE) > 0 && isEnabled(m);
	}
	
	public static int enableMask(Particle p, boolean b) {
		return enableMask(p.getLayerMask(), b);
	}
	
	public static int enableMask(int m, boolean b) {
		return b ? m | BIT_ENABLE : 
					   m & ~BIT_ENABLE;
	}
}
