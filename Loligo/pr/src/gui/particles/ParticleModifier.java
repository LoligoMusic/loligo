package gui.particles;

import pr.DisplayObjectIF;
import pr.Positionable;

public interface ParticleModifier extends Positionable{

	public static final int defaultPriority = 10;
	
	public String getName();
	
	public ModParam[] getParams();
	
	public void modify(Particle p);
	
	public void tryModify(Particle p);
	
	default public void setMultiplier(float fac) {}
	
	public int priority();
	
	public DisplayObjectIF createGui();
	
	public int getLayerMask();
	
	public void setLayerMask(int layerMask);

	
}
