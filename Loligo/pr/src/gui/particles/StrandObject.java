package gui.particles;

import java.util.List;

import constants.DisplayFlag;
import gui.Position;
import gui.particles.prog.ParticleParam;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;

public class StrandObject implements Particle {

	@Setter@Getter
	private Particle particle;
	private final DefaultParticleFactory pFactory;
	private StrandDrawer strandDrawer;
	private Position pos;
	private float[] points;
	private int[] colors;
	@Getter
	private float strandLength, crookedness;
	@Getter@Setter
	private int layerMask = LayerMask.DEFAULT_LAYERMASK;
	
	public StrandObject(ParticleSystem pSystem, DefaultParticleFactory pFactory) {
		
		this(pSystem, pFactory, new ParticleImpl(pSystem, null, 0, 0, 0));
	}
	
	public StrandObject(ParticleSystem pSystem, DefaultParticleFactory pFactory, Particle particle) {
		
		this.particle = particle;
		this.pFactory = pFactory;
		
		strandDrawer = new StrandDrawer();
	}

	@Override
	public void init() {
		particle.init();
	}
	
	public void setInactive() {
		
		points = new float[2];
		colors = new int[1];
	}
	
	@Override
	public void step() {
		
		reset();
		
		int i, ii;
		float x, y, len = 0, crk = 0, crkt = crk;
		
		ParticleParam pa = null;
		for(var p : particle.getParams()) {
			if(p.getName().equals("color")) {
				pa = p;
				pa.setValue(getColor());
				break;
			}
		}
		
		while(!particle.hasDied()) {
			
			i = getLifeTime();
			
			ii = i * 2;
			
			x = particle.getDx();
			y = particle.getDy();
			len += Math.sqrt(x * x + y * y);
			
			float c = (float) Math.atan2(y, x);
			crk += Math.abs(c - crkt);
			crkt = c;
			
			points[ii] 	   = particle.getX();
			points[ii + 1] = particle.getY();
			colors[i] = (Integer) pa.getValue();
			
			particle.step();
			
			
		}
		
		strandLength = len;
		crookedness = crk;
	}
	
	public void reset() {
		
		pFactory.setInitX(pos.getX())
				.setInitY(pos.getY());
		
		pFactory.initParticle(particle);
		
		setColor(pFactory.getColor());
		
		particle.setLifeTime(0);
		
		resetPoints();
		
	}
	
	private void resetPoints() {
		
		int mt = getMaxLifeTime();
		int t = mt * 2 + 2;
		
		if(points == null ||points.length != t) {
			points = new float[t];
			colors = new int[mt + 1];
		}
	}
	
	@Override
	public float getX() {
		
		return pos.getX();
	}

	@Override
	public float getY() {
		
		return pos.getY();
	}
	
	@Override
	public int getColor() {
		
		return strandDrawer.getColor();
	}

	@Override
	public void setColor(int c) {
	
		strandDrawer.setColor(c);
	}
	
	@Override
	public void setPos(float x, float y) {
		
		pos.setPos(x, y);
	}

	@Override
	public Position getPosObject() {
		
		return pos;
	}

	@Override
	public void setPosObject(Position p) {
		pos = p;
	}

	

	@Override
	public void setDisplayObject(DisplayObjectIF d) {
		
		
	}

	@Override
	public DisplayObjectIF getDisplayObject() {
	
		return strandDrawer;
	}

	@Override
	public int getMaxLifeTime() {
		
		return particle.getMaxLifeTime();
	}

	@Override
	public void setMaxLifeTime(int maxLifeTime) {
		
		particle.setMaxLifeTime(maxLifeTime);
		
		resetPoints();
	}

	@Override
	public int getLifeTime() {
		
		return particle.getLifeTime();
	}

	@Override
	public void setLifeTime(int lifeTime) {
	
		particle.setLifeTime(lifeTime);
	}

	@Override
	public float getDx() {
		
		return 0;
	}

	@Override
	public void setDx(float dx) {
		
	}

	@Override
	public float getDy() {
		
		return 0;
	}

	@Override
	public void setDy(float dy) {
		
	}
	
	@Override
	public void setSize(float size) {
		
		particle.setSize(size);
	}

	@Override
	public float getSize() {
		
		return particle.getSize();
	}
	
	@Override
	public void move() {
	}

	@Override
	public void applyModifiers(List<PositionableParticleModifier> mods) {
		
	}

	@Override
	public boolean hasDied() {
		
		return false;
	}

	@Override
	public void kill() {
		//reset();
	}
	
	public DefaultParticleFactory getParticleFactory() {
		return pFactory;
	}

	

	private class StrandDrawer extends DisplayObject {
		
		public StrandDrawer() {
			
			setFlag(DisplayFlag.CLICKABLE, false);
		}
		
		@Override
		public void render(PGraphics g) {
		
			g.noFill();
			//g.stroke(getColor());
			
			g.strokeWeight(particle.getSize());
			
			g.beginShape();
			
			int ps = points.length / 2;
			for (int i = 0; i < ps; i++) {
				
				g.stroke(colors[i]);
				g.vertex(points[i * 2], points[i * 2 + 1]);
				
			}
			
			g.endShape();
			
			g.noStroke();
		}
	}

	@Override
	public void setKillFlag() {}

	@Override
	public void addDx(float x) {}

	@Override
	public void addDy(float y) {}

	@Override
	public List<ParticleParam> getParams() {
		return null;
	}

	


}
