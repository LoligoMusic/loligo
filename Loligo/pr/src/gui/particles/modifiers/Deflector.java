package gui.particles.modifiers;

import gui.particles.ModParam;
import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;

public class Deflector extends PositionableParticleModifier {

	private final ModParam dir;
	
	public Deflector() {
		super("Deflector");
		
		dir = new ModParam("Angle", 0);
		setParams(dir);
	}

	@Override
	public int priority() {
	
		return defaultPriority - 100;
	}
	
	@Override
	public void modify(Particle p) {
		
		float dx = 0,
			  dy = 550;
			  
		
		float t = intersect(
				getX(), getY(), getX() + dx, getY() + dy,
				p.getX(), p.getY(), p.getX() + p.getDx(), p.getY() + p.getDy());
		
		float s = intersect(
				p.getX(), p.getY(), p.getX() + p.getDx(), p.getY() + p.getDy(),
				getX(), getY(), getX() + dx, getY() + dy);
		
		
		if(t >= 0 && t <= 1 && s >= 0 && s <= 1) {
			
			float cx = p.getX() + p.getDx() * t,
				  cy = p.getY() + p.getDy() * t;
			
			float ax = cx - p.getX(),
				  ay = cy - p.getY();
			
			//abstand p - deflektorschnittpunkt
			float ad = (float) Math.sqrt(ax * ax + ay * ay);
			
			//p speed
			float v = (float) Math.sqrt(p.getDx() * p.getDx() + p.getDy() * p.getDy());
			
			//p rest speed
			float vr = v - ad;
			
			//p winkel nach kollision
			float wp = 2 * (dx * p.getDx() + dy * p.getDy()) / (dx * dx + dy * dy);
			 
			float wx = wp * dx - p.getDx(),
				  wy = wp * dy - p.getDy();
			
			
			float ex = (float)Math.sin(wp) * vr,
				  ey = (float)Math.cos(wp) * vr;
			
			p.setPos(cx + ex, cy + ey);
			p.setDx(wx);
			p.setDy(wy);
		}
	}
	
	/*
	 * returns scalar for line q
	 */
	private float intersect(float px1, float py1, float px2, float py2, float qx1, float qy1, float qx2, float qy2) {
		
		float sx = px1 - px2,
			  sy = py1 - py2,
			  rx = qx1 - qx2,
			  ry = qy1 - qy2;
		
		return ((qx1 - px1) * sy - (qy1 - py1) * sx) / (rx * sy - ry * sx);
	}
	
	

}
