package gui.particles.modifiers;

import constants.Units;
import gui.particles.ModParam;
import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;
import module.inout.NumericAttribute;

public class Attractor extends PositionableParticleModifier{
	
	private float force, power = 2;
	
	private final ModParam _force, _power;
	
	
	public Attractor() {
		
		super("Attractor");
		 
		_force = new ModParam("Force", 1, NumericAttribute.DEZIMAL_FREE);
		_power = new ModParam("Power", 1);
		setParams(_force, _power);
	}
	
	@Override
	public void modify(Particle p) {
		
		power = (float) _power.getValue() * 4;
		force = (float) _force.getValue() * 1f;
		
		float dx = getX() - p.getX(),
			  dy = getY() - p.getY();
		double d = (Math.sqrt(dx * dx + dy * dy) / Units.LENGTH.normFactor);
		float n = (float) d;
		dx /= n;
		dy /= n;
		
		float f = force / (float) Math.pow(1 + d, power);
		f *= multiplicator(p);
		f = Math.min(f, 100);
		
		p.addDx(dx * f);
		p.addDy(dy * f);
	}
	

	public float getForce() {
		return force;
	}

	public void setForce(float force) {
		this.force = force;
	}

	public float getPower() {
		return power;
	}

	public void setPower(float power) {
		this.power = power;
	}
	
}
