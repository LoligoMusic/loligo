package gui.particles.modifiers;

import gui.particles.ModParam;
import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;

public class Wind extends PositionableParticleModifier {
	
	private ModParam direction, strength; //falloff;

	
	public Wind() {
		super("Wind");
		
		direction = new ModParam("Direction", 0);
		strength = new ModParam("strength", 0.2);
		
		setParams(direction, strength);
	}

	@Override
	public void modify(Particle p) {
		
		double d = direction.getValue(),
			   s = strength.getValue() * multiplicator(p),
			   w = d * Math.PI * 2;
		
		p.addDx((float) (Math.sin(w) * s));
		p.addDy((float) (Math.cos(w) * s));

	}
	
	public void setDirection(float dir) {
		
		direction.setValue(dir);
	}
	
	public void setStrength(float s) {
		
		strength.setValue(s);
	}

	public void setFalloff(float falloff) {
		
		//this.falloff = falloff;
	}
}
