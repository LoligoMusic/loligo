package gui.particles.modifiers;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Supplier;

import gui.particles.PositionableParticleModifier;
import lombok.var;
import module.DefaultParticleModifierModule;
import module.Module;
import module.ParamSetter;
import module.loading.InternalNodeEntry;
import module.loading.NodeEntry;
import module.loading.NodeLoader;
import module.loading.NodeCategory;


public enum ParticleModifierEntry implements NodeEntry{

	ATTRACTOR	(Attractor::new),
	DEFLECTOR	(Deflector::new),
	DRAG		(Drag::new),
	//TUMBLE		(Tumble::new),
	WIND		(Wind::new),
	TURBULENCE	(Turbulence::new),
	//PARAMSETTER	(ParamSetter::new),
	LORENZ		(Lorenz::new);
	
	
	private final Supplier<PositionableParticleModifier> supplier;
	
	private ParticleModifierEntry(Supplier<PositionableParticleModifier> supplier) {
		
		this.supplier = supplier;
	}
	
	public static NodeLoader getLoader() {
		
		return new NodeLoader() {
			
			@Override
			public List<NodeEntry> loadEntries() {
				EnumSet<ParticleModifierEntry> en = EnumSet.allOf(ParticleModifierEntry.class);
				
				List<NodeEntry> li = new ArrayList<>(en);
				var ps = new InternalNodeEntry(ParamSetter::new, "ParamSetter", NodeCategory.PARTICLE);
				li.add(ps);
				return li;
			}
		};
	}

	
	
	@Override
	public String getName() {
		
		String s = toString();
		
		return s.substring(0, 1) + s.substring(1).toLowerCase();
	}

	@Override
	public NodeCategory getType() {
		
		return NodeCategory.PARTICLE;
	}

	@Override
	public Module createInstance() {
		
		PositionableParticleModifier pm = supplier.get();
		DefaultParticleModifierModule pmm = new DefaultParticleModifierModule(pm);
		pmm.setName(getName());
		return pmm;
	}

	@Override
	public boolean isHidden() {
		return false;
	}
	
}
