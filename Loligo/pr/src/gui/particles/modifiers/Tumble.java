package gui.particles.modifiers;

import gui.particles.ModParam;
import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;

public class Tumble extends PositionableParticleModifier {

	private ModParam speed, radius;
	
	public Tumble() {
		super("Tumble");
		
		speed = new ModParam("Freq", 0.5);
		radius = new ModParam("Size", 0.5);
		setParams(speed, radius);
	}
	
	@Override
	public void modify(Particle p) {
		
		double ph =  10 * p.getLifeTime() * (2*Math.PI) / (Math.round(speed.getValue() * 100)),
				r = radius.getValue() * multiplicator(p);
		
		p.addDx((float) (Math.sin(ph) * r * 100));
		p.addDy((float) (Math.cos(ph) * r * 100));
				
	}

	public void setSpeed(float s) {
		speed.setValue(s);
	}

	public void setRadius(float r) {
		radius.setValue(r);
	}

	@Override
	public int priority() {
		
		return defaultPriority;
	}
	
	

}
