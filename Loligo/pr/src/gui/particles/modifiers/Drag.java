package gui.particles.modifiers;

import gui.particles.ModParam;
import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;


public class Drag extends PositionableParticleModifier {
	
	private ModParam factor;
	
	public Drag() {
		super("Drag");
		
		priority = defaultPriority + 2;
		factor = new ModParam("Amount", 0.1);
		setParams(factor);
		
	}
	
	
	@Override
	public void modify(Particle p) {
		
		float f = 1 - (float) (factor.getValue() * multiplicator(p));
		
		p.setDx(p.getDx() * f);
		p.setDy(p.getDy() * f);
	}

	public void setFactor(float f) {
		
		factor.setValue(f);
	}
	
	@Override
	public int priority() {
		
		return defaultPriority;
	}
	
	

}
