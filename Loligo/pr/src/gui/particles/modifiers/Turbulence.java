package gui.particles.modifiers;

import gui.particles.ModParam;
import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;
import pr.RootClass;

public class Turbulence extends PositionableParticleModifier {

	private final ModParam strength, freq;
	
	
	public Turbulence() {
		super("Turbulence");
		
		strength = new ModParam("Strength", 0.5);
		freq = new ModParam("Freq", 1);
		setParams(strength, freq);
	}

	@Override
	public void modify(Particle p) {
		
		float fr = (float) freq.getValue(); 
		
		double s = strength.getValue() * multiplicator(p);
		
		double f = RootClass.mainProc().noise(
				(p.getX() - getX()) * fr, 
				(p.getY() - getY()) * fr);
		
		p.addDx((float) (Math.sin(f * 6.28) * s));
		p.addDy((float) (Math.cos(f * 6.28) * s));
		
	}

	public void setStrength(float s) {
		
		strength.setValue(s);
	}

	public void setFrequency(float f) {
		
		freq.setValue(f);
	}
	
}
