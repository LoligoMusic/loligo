package gui.particles.modifiers;

import gui.particles.ModParam;
import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;
import static java.lang.Math.sqrt;

public class Lorenz extends PositionableParticleModifier {

	private final ModParam force;
	
	
	public Lorenz() {
		super("Lorenz");
		
		priority = defaultPriority + 1;
		force = new ModParam("Force", .5);
		setParams(force);
	}
	
	
	@Override
	public void modify(Particle p) {
		
		float f = (float) (force.getValue() * multiplicator(p));
		
		float dx1 = p.getDx(),
			  dy1 = p.getDy(),
			  dx2 = -p.getDy() * f,
			  dy2 =  p.getDx() * f;
		
		float dx = dx1 + dx2, 
			  dy = dy1 + dy2;
		
		float c = (float) 
				(sqrt(dx1 * dx1 + dy1 * dy1)/
					sqrt(dx * dx + dy * dy)  
				);
		
		dx *= c;
		dy *= c;
		
		p.setDx(dx);
		p.setDy(dy);
	}

}
