package gui.particles.modifiers;

import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;
import gui.paths.NullPathObject;
import gui.paths.Path;
import gui.paths.PosConstraint;

public class PathForce extends PositionableParticleModifier {

	PosConstraint pc;
	
	public PathForce(PosConstraint pc) {
	
		this.pc = pc;
	}
	
	
	@Override
	public void modify(Particle p) {
		
		Path path = pc.getPaths().get(0);
		
		double d = path.nearestPoint(p.getX(), p.getY());
		
		NullPathObject po = new NullPathObject();
		path.goToPos(po, d);
		
		double dx = po.getX() - p.getX(),
			   dy = po.getY() - p.getY();
		
		double dist = Math.sqrt(dx * dx + dy * dy) * .003;
		System.out.println(dist);
		p.setDx((float)(p.getDx() + Math.sin(po.getTangent() + 3.14 + 1.57) / dist));
		p.setDy((float)(p.getDy() + Math.cos(po.getTangent() + 3.14 + 1.57) / dist));
		
	}

	
}
