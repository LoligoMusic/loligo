package gui.particles;

import constants.DisplayFlag;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;

public class DistanceMask implements Multiplicator {

	@Getter@Setter
	private float maxDistance = 30, minDistance = 0;
	
	public DistanceMask() {
		
	}

	@Override
	public float calcFac(Particle p, PositionableParticleModifier m) {
		
		float dx = p.getX() - m.getX(),
			  dy = p.getY() - m.getY(),
			  d  = (float)Math.sqrt(dx * dx + dy * dy);
		
		return d < maxDistance && d > minDistance ? 1 : 0;
	}

	@Override
	public DisplayObjectIF createGui() {
		
		DisplayObject d = new DisplayObject() {
			
			@Override
			public void render(PGraphics g) {
				g.stroke(getColor());
				g.strokeWeight(1);
				g.noFill();
				float d = maxDistance * 2;
				g.ellipse(getX(), getY(), d, d);
				g.noStroke();
			}
		};
		d.setColor(0xaa0000ff);
		d.setFlag(DisplayFlag.fixedToParent, true);
		d.setFlag(DisplayFlag.CLICKABLE, false);
		return d;
	}

}
