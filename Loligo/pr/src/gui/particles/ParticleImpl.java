package gui.particles;

import java.util.ArrayList;
import java.util.List;

import gui.DisplayObjects;
import gui.Position;
import gui.particles.prog.ParticleParam;
import lombok.Getter;
import lombok.Setter;
import module.inout.DataType;
import pr.DisplayObjectIF;
import util.Colors;

public class ParticleImpl implements Particle{

	private final ParticleSystem pSystem;
	private DisplayObjectIF displayObj;
	private Position pos;
	@Getter@Setter
	private int layerMask = LayerMask.DEFAULT_LAYERMASK;
	
	private int maxLifeTime = 150, lifeTime = 0;
	private float dx, dy, size;
	
	private boolean isDead = false;
	private final List<ParticleParam> params;
	
	public ParticleImpl(ParticleSystem pSystem) {
	
		this.pSystem = pSystem;
		
		pos = new Position();
		params = null;
	}
	
	public ParticleImpl(ParticleSystem pSystem, DisplayObjectIF displayObj, int maxLifeTime, float dx, float dy) {
		
		this.pSystem = pSystem; 
		this.displayObj = displayObj;
		//this.maxLifeTime = maxLifeTime;
		this.dx = dx;
		this.dy = dy;
		
		pos = new Position();
		
		setDisplayObject(displayObj);
		
		params = new ArrayList<>(2);
		params.add(new ParticleParam("color", DataType.COLOR, Colors.RED));
		params.add(new ParticleParam("size", DataType.Number, 0));
	}
	
	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#init()
	 */
	@Override
	public void init() {
		
		isDead = false;
		lifeTime = 0;
	}
	
	
	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#setDisplayObject(pr.DisplayObjectIF)
	 */
	@Override
	public void setDisplayObject(DisplayObjectIF d) {
		
		displayObj = d;
		if(d != null)
			displayObj.setPosObject(new Position.PositionTarget(this));
	}
	
	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#getDisplayObject()
	 */
	@Override
	public DisplayObjectIF getDisplayObject() {
		
		return displayObj;
	}
	
	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#getMaxLifeTime()
	 */
	@Override
	public int getMaxLifeTime() {
		return maxLifeTime;
	}


	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#setMaxLifeTime(int)
	 */
	@Override
	public void setMaxLifeTime(int maxLifeTime) {
		this.maxLifeTime = maxLifeTime;
	}


	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#getLifeTime()
	 */
	@Override
	public int getLifeTime() {
		return lifeTime;
	}


	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#setLifeTime(int)
	 */
	@Override
	public void setLifeTime(int lifeTime) {
		this.lifeTime = lifeTime;
	}


	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#getDx()
	 */
	@Override
	public float getDx() {
		return dx;
	}

	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#getDy()
	 */
	@Override
	public float getDy() {
		return dy;
	}
	
	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#setDx(float)
	 */
	@Override
	public void setDx(float x) {
		this.dx = x;
	}

	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#setDy(float)
	 */
	@Override
	public void setDy(float y) {
		this.dy = y;
	}

	@Override
	public void addDx(float x) {
	
		dx += x;
	}
	
	@Override
	public void addDy(float y) {
	
		dy += y;
	}

	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#step()
	 */
	@Override
	public void step() {
		
		applyModifiers(pSystem.getModifiers());
		move();
		
		lifeTime++;
		
		if(lifeTime > maxLifeTime)
			setKillFlag();
	}
	
	
	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#move()
	 */
	@Override
	public void move() {
		/*
		dx += dx2;
		dy += dy2;
		dx2 = dy2 = 0f;
		*/
		setPos(
			getX() + dx, 
			getY() + dy);
	}

	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#applyModifiers(java.util.List)
	 */
	@Override
	public void applyModifiers(List<PositionableParticleModifier> mods) { 
		
		for (PositionableParticleModifier pm : mods) 
			
			pm.tryModify(this);
		 
	}
	
	
	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#hasDied()
	 */
	@Override
	public boolean hasDied() {
		
		return isDead;
	}

	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#kill()
	 */
	@Override
	public void kill() {
		
		DisplayObjects.removeFromDisplay(displayObj);
	}

	@Override
	public void setColor(int c) {
		
		if(displayObj != null)
			displayObj.setColor(c);
	}
	
	
	@Override
	public int getColor() {
		
		return displayObj.getColor();
	}
	
	@Override
	public void setSize(float size) {
		
		this.size = size;
		displayObj.setWH((int)size, (int)size);
	}
	
	@Override 
	public float getSize() {
		
		return size;
	}
	
	
	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#getX()
	 */
	@Override
	public float getX() {
		
		return pos.getX();
	}


	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#getY()
	 */
	
	@Override
	public float getY() {
		
		return pos.getY();
	}


	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#setPos(float, float)
	 */
	
	@Override
	public void setPos(float x, float y) {
		
		pos.setPos(x, y);
	}


	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#getPosObject()
	 */
	
	@Override
	public Position getPosObject() {
		
		return pos.getPosObject();
	}


	/* (non-Javadoc)
	 * @see gui.particles.ParticleIF#setPosObject(gui.Position)
	 */
	
	@Override
	public void setPosObject(Position p) {
		
		displayObj.setPosObject(p);
		pos = p;
	}

	@Override
	public void setKillFlag() {
		
		isDead = true;
	}

	@Override
	public List<ParticleParam> getParams() {
		return params;
	}

}
