package gui;

public interface Scrollable {
	
	void setScrollPosX(float pos);
	void setScrollPosY(float pos);
	
	float getScrollPosX();
	float getScrollPosY();
	
	float getViewportLengthX();
	float getViewportLengthY();
	
	float getTotalLengthX();
	float getTotalLengthY();
	
}
