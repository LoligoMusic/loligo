package gui.sampler;

import gui.Button;
import gui.GUIBox;
import gui.GuiReceiver;
import gui.GuiSender;

public class SampleMenu extends GUIBox {

	private final Button newLoop_btn;
	final SampleViewer viewer;
	private final SampleFile file;
	final LoopContainer loopContainer;
	
	public SampleMenu(SampleFile file) {
		this.file = file;
		viewer = new SampleViewer(file);
		newLoop_btn = new Button(file::newLoop, "add Loop");
		loopContainer = new LoopContainer(null);
		addRow(viewer);
		addRow(loopContainer.gui);
		addRow(newLoop_btn);
		
	}
	
}
