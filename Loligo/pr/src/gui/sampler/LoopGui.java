package gui.sampler;

import java.util.ArrayList;

import pr.DisplayObject;
import processing.core.PConstants;
import processing.core.PGraphics;

public class LoopGui extends DisplayObject {
	private static final long serialVersionUID = 1L;
	
	final Loop loop;
	private PGraphics waveImg;
	final ArrayList<LoopGui> clones;
	final boolean isClone;
	
	public LoopGui(Loop loop) {
		this(loop, false);
	}
	
	private LoopGui(Loop loop, boolean isClone) {
		this.loop = loop;
		setWidth(100);
		setHeight(30);
		form = FORM_RECT;
		dragable = isClone;
		waveImg = PROC.createGraphics(getWidth(), getHeight(), PConstants.P2D);
		this.isClone = isClone;
		clones = isClone ? null : new ArrayList<LoopGui>();
	}
	
	@Override
	public void render(PGraphics g) {
		getDm().g.image(waveImg, getX(), getY());
	}

	void update() {
		loop.sampleFile.sampleMenu.viewer.drawSample(waveImg, loop.inPoint, loop.outPoint);
		
		//if(!isClone)
		//	for (LoopGui o : clones) 
		//		o.update();
			
	}
	
	@Override
	public void mouseStartDrag() {
		if(!isClone)
		getDm().getInput().setDragTarget(getClone());
	}
	
	public LoopGui getClone() {
		LoopGui o = new LoopGui(loop, true);
		clones.add(o);
		o.waveImg = waveImg;
		//o.update();
		PROC.display.GUIContainer.addChild(o);
		return o;
	}
	
	
	
	
}
