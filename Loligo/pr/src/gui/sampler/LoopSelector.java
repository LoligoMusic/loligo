package gui.sampler;

import gui.GuiSender;
import gui.GuiSenderObject;
import pr.DisplayObject;
import processing.core.PGraphics;

public class LoopSelector extends DisplayObject implements GuiSender<Float[]> {

	private static final long serialVersionUID = 1L;
	private static int barHeight = 20;
	private final GuiSenderObject<Float[]> sender;
	private final Loop loop;
	private final SampleViewer viewer;
	final Handle in, out;
	
	
	public LoopSelector(Loop loop) {
		this.loop = loop;
		viewer = loop.sampleFile.sampleMenu.viewer;
		
		sender = new GuiSenderObject<Float[]>(this, loop, new Float[]{0f, 1f});
		
		form = FORM_RECT;
		setColor(0x11ff0000);
		setStrokeColor(0x00000000);
		setHeight(viewer.getHeight());
		fixedToParent = dragable = lockY = true;
		
		in = new Handle(true);
		out = new Handle(false);
		in.lockY = out.lockY = true;
		in.fixedToParent = out.fixedToParent = true;
		in.setPos(0, viewer.getWidth());
		
		addChild(in, out);
		viewer.addChild(this);
		//set();
	}
	
	public void send() {
		sender.setValue(new Float[]{in.pos, out.pos});
		sender.message();
	}
	
	@Override
	public Float[] getValue() {
		return sender.getValue();
	}

	@Override
	public void setValue(Float[] v) {
		sender.setValue(v);
		updatePos();
	}
	
	public void updatePos() {
		
		float[] p = new float[2];
		
		for (int i = 0; i < 2; i++) {
			
			Float[] f = sender.getValue();
			p[i] = viewer.globalToPixel(f[i]);
			Handle h = i == 0 ? in : out;
			if(p[i] < 0) {
				
				p[i] = 0;
				h.removeFromDisplay();
			}else if(p[i] > viewer.getWidth()){
				
				p[i] = viewer.getWidth();
				h.removeFromDisplay();
			}else if(h.getParent() == null) 
				addChild(h);
		}
		
		//rx = p[0];
		getPosObject().setPos(p[0], getPosObject().getY());
		
		setWidth((int)(p[1] - p[0]));
		out.getPosObject().setPos(getWidth(), getPosObject().getY());
		//out.rx = width;
	}
	
	@Override
	public void render(PGraphics g) {
		getDm().g.fill(getColor());
		getDm().g.noStroke();
		getDm().g.rect(getX(), getY(), getWidth(), getHeight());
		getDm().g.fill(getColor() + 0x66000000);
		getDm().g.rect(getX(), getY() + getHeight() - barHeight, getWidth(), barHeight);
	}
	
	@Override
	public boolean hitTest(float mx, float my) {
		return mx > getX() && mx < getX() + getWidth() && my > getY() + getHeight() - barHeight && my < getY() + getHeight();
	}
	
	@Override
	public void mouseDragged() {
		in.pos = viewer.pixelToGlobal(in.getX() - viewer.getX()); 
		out.pos = viewer.pixelToGlobal(out.getX() - viewer.getX()); 
		send();
		
		if(rx < 0)
			rx = 0;
		else if(rx + getWidth() > viewer.getWidth())
			rx = viewer.getWidth() - getWidth();
	}
	
	class Handle extends DisplayObject {
		private static final long serialVersionUID = 1L;
		boolean isIn = true;
		float pos;
		
		public Handle(boolean in) {
			form = FORM_RECT;
			setHeight(8);
			setWidth(getHeight() * (in ? 1 : 1));
			setColor(0x90ffffff);
			dragable = true;
		}
		
		@Override public void render(PGraphics g) {
			getDm().g.fill(getColor());
			getDm().g.rect(getX(), getY(), getWidth(), getHeight());
			getDm().g.strokeWeight(1);
			getDm().g.stroke(getColor());
			getDm().g.line(getX(), getY(), getX(), getY() + viewer.getHeight());
			getDm().g.noStroke();
		}
		
		@Override public void mouseDragged() {
			float dx = getX() - viewer.getY();
			if(dx < 0) {
				rx = 0;
				x = viewer.x;
			}else if(dx > viewer.getWidth()){
				rx = viewer.x + viewer.getWidth() - LoopSelector.this.x;
				x = viewer.x + viewer.getWidth();
			}
			
			
			if(this == in) {
				if(in.rx > out.rx) {
					in.rx = out.rx - 1;
					in.x = out.x - 1;
				}
				float tx = LoopSelector.this.rx;
				LoopSelector.this.rx = x - viewer.x;
				LoopSelector.this.x = viewer.x + LoopSelector.this.rx;
				LoopSelector.this.setWidth((int) (out.x - in.x));
				out.rx = LoopSelector.this.getWidth();
			}else if(this == out){
				if(out.rx < in.rx) 
					out.rx = in.rx + 1;
				LoopSelector.this.setWidth((int) (out.rx - in.rx));;
			}
			
			pos = viewer.pixelToGlobal(x - viewer.x);
			send();
		}
		
		@Override public void mouseStopDrag() {
			mouseDragged();
		}
	}

	@Override
	public void disable(boolean b) {
		// TODO Auto-generated method stub
		
	}

}
