package gui.sampler;

import constants.DisplayFlag;
import lombok.var;
import net.beadsproject.beads.data.Sample;
import pr.DisplayObject;
import processing.core.PConstants;
import processing.core.PGraphics;
import util.Utils;

public class SampleViewer extends DisplayObject {
	
	private static final float zoomLimit = .00005f, resolution = 1000; 
	private final SampleFile sampleFile;
	private transient final PGraphics canvas;
	private float zoom = 1, yScale = .9f, start = 0, mouseY_temp;
	private float mousePos;
	
	public SampleViewer(SampleFile sampleFile) {
		this.sampleFile = sampleFile;
		
		canvas = Utils.createGraphics(600, 200, PConstants.P2D);
		setFlag(DisplayFlag.RECT, true);
		setWH(canvas.width, canvas.height);
		
		drawSample(0, 1);
	}
	
	void drawSample(float startSa, float endSa) {
		drawSample(canvas, startSa, endSa);
		updateMarkers();
	}
	
	void drawSample(PGraphics graphics, float startSa, float endSa) {
		startSa = startSa < 0 ? 0 : startSa;
		endSa = endSa > 1 ? 1 : endSa;
		Sample sample = sampleFile.sample;
		float[] frameData = new float[sample.getNumChannels()];
		float numFrames = sample.getNumFrames();
		int cc = (int)(numFrames * startSa);
		float basis = (int)(numFrames * startSa);
		float scala = (int)(numFrames * endSa - basis);
		numFrames -= numFrames * startSa + numFrames * (1 - endSa);
		float bins = numFrames / graphics.width;
		graphics.beginDraw();
		canvas.stroke(255);
		canvas.strokeWeight(1);
		graphics.background(100);
		if(bins >= 1) {
			numFrames = 1 + (float)Math.log(numFrames) * resolution;
			bins = numFrames / graphics.width;
			
			for (int i = 0; i < graphics.width; i++) {
				float binMax = 0, binMin = 0, ii = i * bins, s;
				for (int j = 0; j < bins; j++) {
					sample.getFrame((int)(basis + scala * ((ii + j) / numFrames)), frameData);
					
					s = frameData[0];
					if(s > binMax)
						binMax = s;
					else if(s < binMin)
						binMin = s;
				}
				//frame /= frames;
				float max = graphics.height / 2 + (binMax * graphics.height * yScale) * .5f;//(graphics.height - bin * graphics.height * yScale) / 2;
				float min = graphics.height / 2 + (binMin * graphics.height * yScale) * .5f;
				graphics.line(i, min, i, max);//graphics.line(i, v, i, graphics.height - v);
			}
		}else {
			float f1 = graphics.height / 2, f2;
			//float step = graphics.width / numFrames;
			for (int i = 0; i < numFrames; i++) {
				sample.getFrame(cc, frameData);
				f2 = frameData[0];
				f2 = graphics.height / 2 + f2 * graphics.height * yScale * .5f;
				
				graphics.line(i / bins, f1, (i + 1) / bins, f2);
				f1 = f2;
				cc++;
			}
		}
		graphics.endDraw();
	}
	
	private void updateMarkers() {
		for (Loop o : sampleFile.loops) 
			o.selector.updatePos();
	}
	
	float pixelToGlobal(float p) {
		float f = start + (zoom) * (p / canvas.width);
		f = f < 0 ? 0 : f > 1 ? 1 : f;
		return f;
	}
	
	float globalToPixel(float p) {
		return canvas.width * (p - start) / (zoom);
	}
	
	@Override
	public void render(PGraphics g) {
		g.image(canvas, getX(), getY());
		/*
		float ap = globalToPixel((float)player.getPosition() / sample.getLength());
		if(ap >= 0 && ap <= canvas.width) {
			float p = x + ap;
			dm.g.stroke(200, 0 ,0, 100);
			dm.g.strokeWeight(1);
			dm.g.line(p, y, p, y + height);
		}
		*/
		//System.out.println(player.getPosition());
	}
	
	@Override
	public void mouseStartDrag() {
		var in = getDm().getInput();
		mouseY_temp = in.mouseY;
		mousePos = pixelToGlobal(in.mouseX);
	}
	
	@Override
	public void mouseDragged() {
		var in = getDm().getInput();
		float dy = .008f * (float)Math.pow(zoom, 1.1) * (in.mouseY - mouseY_temp);
		mouseY_temp = in.mouseY;
		zoom += dy;
		zoom = zoom > 1 ? 1 : zoom < zoomLimit ?zoomLimit : zoom;
		float z = zoom;
		
		float ratio = (float)in.mouseX / canvas.width;
		start = mousePos - z * ratio;
		if(start < 0)
			start = 0;
		float end = start + z;
		if(end > 1) {
			start = 1 - z;
			end = 1;
		}
		drawSample(start, end);
	}
}
