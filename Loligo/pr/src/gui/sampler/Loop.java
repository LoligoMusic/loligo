package gui.sampler;

import gui.GuiReceiver;
import gui.GuiSender;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.SamplePlayer;
import patch.Loligo;

public class Loop implements GuiReceiver{
	final SampleFile sampleFile;
	final SamplePlayer player;
	final LoopSelector selector;
	final LoopGui gui;
	float inPoint = 0, outPoint = 1;

	public Loop(SampleFile sampleFile) {
		this.sampleFile = sampleFile;
		player = new SamplePlayer(Loligo.RootClass.audioManager().getAC(), 1);
		player.setSample(sampleFile.sample);
		player.setPosition(new Glide(Loligo.RootClass.audioManager().getAC(), 0, 10000));
		selector = new LoopSelector(this);
		gui = new LoopGui(this);
		
		//sampleFile.sampleMenu.addRow(gui);
	}

	@Override
	public void guiMessage(GuiSender<?> s) {
		if(s == selector) {
			Float[] f = selector.getValue();
			inPoint = f[0];
			outPoint = f[1];
			player.setLoopPointsFraction(inPoint, outPoint);
			gui.update();
		}
	}

	@Override
	public void updateSender(GuiSender<?> s) {
		selector.setValue(new Float[]{inPoint, outPoint});
	}
	
	public SamplePlayer getPlayer() {
		return player;
	}
	
	public float getInPoint() {
		return inPoint;
	}
}
