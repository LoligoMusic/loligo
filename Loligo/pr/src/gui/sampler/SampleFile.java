package gui.sampler;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import net.beadsproject.beads.data.Sample;
import util.Container;

public class SampleFile implements Container<Sample> {
	final static HashMap<Sample, SampleFile> sampleMap = new HashMap<>();
	final transient Sample sample;
	final String path;
	final SampleMenu sampleMenu;
	final ArrayList<Loop> loops;
	
	public static SampleFile createSampleFile(String path) {
		Set<Entry<Sample, SampleFile>> set = sampleMap.entrySet();
		for (Entry<Sample, SampleFile> e : set) 
			if(e.getValue().path.equals(path))
					return e.getValue();
		
		SampleFile sf = new SampleFile(path);
		sampleMap.put(sf.sample, sf);
		return sf;
	}
	
	private SampleFile(String path) {
		this.path = path;
		sample = loadSample(path);
		loops = new ArrayList<>();
		sampleMenu = new SampleMenu(this);
		sampleMenu.x = 20;
		Processing.PROC.display.GUIContainer.addChild(sampleMenu);
	}
	
	private Sample loadSample(String path) {
		try{
			return new Sample(path);
		}catch(final Exception e) {
			System.out.println("SampleFile" + e);
			return null;
		}
	}
	
	public Loop newLoop() {
		Loop o = new Loop(this);
		loops.add(o);
		sampleMenu.loopContainer.addLoop(o.gui);
		return o;
	}
	
	@Override
	public Sample getValue() {
		return sample;
	}

	@Override
	public void setValue(Sample o) {}
	
	private void readObject(ObjectInputStream ois)
			throws ClassNotFoundException, IOException {
		ois.defaultReadObject();
		
		Field f = null;
		
		try {
			f = getClass().getDeclaredField("sample");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		f.setAccessible(true);
		try {
			f.set(this, loadSample(path));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		f.setAccessible(false);	
	}

}
