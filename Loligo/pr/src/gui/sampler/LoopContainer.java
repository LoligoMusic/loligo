package gui.sampler;

import java.util.ArrayList;

import constants.InputEvent;
import constants.MouseEventContext;
import gui.GuiReceiver;
import gui.GuiSender;
import gui.ListObject;
import module.modules.audio.SamplerOld;
import pr.DisplayObjectIF;
import pr.EventSubscriber;
import pr.UserInput;
 
public class LoopContainer implements GuiReceiver, EventSubscriber {
	
	final ArrayList<Loop> loops;
	final SamplerOld sampler;
	public ListObject gui;
	
	public LoopContainer(SamplerOld sampler) {
		this.sampler = sampler;
		gui = new ListObject();
		gui.fixedToParent = true;
		if(sampler != null) {
			gui.setWidth(120);
			gui.setHeight(60);
			gui.dropable = true;
			gui.setPos(0, sampler.getGui().getHeight() + 80);
			sampler.getGui().addChild(gui);
		}
		
		loops = new ArrayList<>();
		MouseEventContext.instance.addListener(this);
	}
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		if(e == InputEvent.DROPPED) {
			DisplayObjectIF d = input.getDropTarget();
			if(d == gui) {
				if(input.getDragTarget() instanceof LoopGui)
					addLoop((LoopGui) input.getDragTarget());
			}else if(gui.contains(d)) {
				
			}
		}
	}

	public Loop getLoop(double d) {
		
		LoopGui g = (LoopGui) gui.get(d);
		return g == null ? null : g.loop;
	}
	
	public void addLoop(LoopGui lg) {
		
		loops.add(lg.loop);
		gui.add(lg);
	}
	
	public void removeLoop(Loop lo) {
		
		loops.remove(lo);
		gui.remove(lo.gui);
	}

	@Override
	public void guiMessage(GuiSender<?> s) {
		
		if(s == gui) {
			
		}
	}

	@Override
	public void updateSender(GuiSender<?> s) {
		// TODO Auto-generated method stub
		
	}

	
}
