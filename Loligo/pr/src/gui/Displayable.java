package gui;

import pr.DisplayObjectIF;

/**
 * Interface for objects that have a direct visual representation in form of a DisplayObjectIF.
 *  
 * @author Vanja
 *
 */
public interface Displayable {

	public DisplayObjectIF createGUI();
	public DisplayObjectIF getGui();
	//public void setGui(DisplayObjectIF gui);
}
 