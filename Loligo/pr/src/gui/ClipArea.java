package gui;

import constants.DisplayFlag;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;

public class ClipArea extends DisplayObject {

	private final DisplayObject clipEnd;
	
	public ClipArea() {
		
		setFlag(DisplayFlag.RECT, true);
		
		clipEnd = new DisplayObject() {
			@Override 
			public void render(PGraphics g) {
				g.noClip();
			}
		};
		clipEnd.setWH(0, 0);
		clipEnd.setFlag(DisplayFlag.CLICKABLE, false);
		addChild(clipEnd);
	}
	
	
	@Override
	public void render(PGraphics g) {
		float x = getX(), y = getY(), w = getWidth(), h = getHeight();
		g.clip(x, y, w, h);
		renderBackground(g, x, y, w, h);
	}
	
	
	public void renderBackground(PGraphics g, float x, float y, float w, float h) {
		g.fill(getColor());
		g.rect(x, y, w, h);
	}
	
	
	@Override
	public void addChild(DisplayObjectIF d) {
		super.addChild(d);
		DisplayObjects.setChildIndex(clipEnd, -1);
	}
	
}
