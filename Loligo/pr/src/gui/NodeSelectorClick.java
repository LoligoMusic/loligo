package gui;

import java.util.function.Consumer;

import constants.DisplayFlag;
import constants.InputEvent;
import constants.PatchEvent;
import constants.PatchEventListener;
import constants.PatchEventManager;
import lombok.Setter;
import module.Module;
import module.ModuleType;
import patch.LoligoPatch;
import pr.EventSubscriber;
import pr.RootClass;
import pr.userinput.SpecialMouseModeOwner;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PImage;
import util.Images;

public class NodeSelectorClick implements PatchEventListener, EventSubscriber, SpecialMouseModeOwner{

	public static final PImage ICON_PICK_DEFAULT = Images.loadPImage("/resources/pick_module_15px.png");
	public static final PImage ICON_PICK_ACTIVE = Images.fillAlphaImage(ICON_PICK_DEFAULT, 0xffaaaaaa);
	public static final PImage ICON_PICK = Images.fillAlphaImage(ICON_PICK_DEFAULT, 0xff444444);
	
	static public Button button(Consumer<Module> cons) {
		var b = new Button(() -> new NodeSelectorClick(cons), "");
		b.setPos(3, 3);
		b.setImage(ICON_PICK, ICON_PICK_ACTIVE);
		b.setMouseOverText("Select node from patch");
		b.setFlag(DisplayFlag.fixedToParent, true);
		return b;
	}
	
	private LoligoPatch patch;
	@Setter
	private Consumer<Module> moduleConsumer = m -> {};
	
	
	public NodeSelectorClick(Consumer<Module> cons) {
		
		var pm = PatchEventManager.instance;
		pm.addListener(this);
		
		var p2 = pm.getFocusedPatch();
		if(p2 != null)
			setPatch(p2);
		
		moduleConsumer = cons;
		
		RootClass.backstage().getAllPatches()
			.forEach(p -> p.getPApplet().cursor(PConstants.CROSS));
		
	}
	
	
	private void close() {
		PatchEventManager.instance.removeListener(this);
		
		if(patch != null) {
			var in = patch.getDisplayManager().getInput();
			in.removeInputEventListener(this);
			in.stopSpecialMouseMode();
		}
		
		RootClass.backstage().getAllPatches()
			.forEach(p -> p.getPApplet().cursor(PConstants.ARROW));
	}
	
	
	private void setPatch(LoligoPatch patch) {
		
		if(this.patch != null) {
			this.patch.getDisplayManager().getInput().removeInputEventListener(this);
		}
		
		this.patch = patch;
		var in = patch.getDisplayManager().getInput();
		in.startSpecialMouseMode(this);
		in.addInputEventListener(this);
		patch.getPApplet().cursor(PConstants.CROSS);
		System.out.println(patch);
	}
	
	
	public void onSelectModule(Module m) {
		moduleConsumer.accept(m);
	}
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		switch(e) {
		case PRESSED -> {
			var d = input.getPressedTarget();
			if(d != null && d.getGuiType().isModule()) {
				
				var m = patch.getModuleManager().getModuleFromGui(d);
				
				if(ModuleType.isPositionable(m)) {
					System.out.println(m);
					onSelectModule(m);
					close();
				}
			}
		}
		case CLICK_RIGHT -> close();
		default -> {}
		}
	}


	@Override
	public void onPatchEvent(PatchEvent e) {
		
		var p = e.patch();
		if(p == null)
			return;
		
		var in = p.getDisplayManager().getInput();
		
		switch (e.type()) {
		case FOCUS_GAINED -> setPatch(p);
		case FOCUS_LOST, KILL_WINDOW -> {
			if(patch == p) {
				in.removeInputEventListener(this);
				patch = null;
			}
			}
		default -> {}
		}
		
	}


	@Override
	public void specialMouseModeStop() {
		close();
	}


}
