package gui.nodeinspector;

import static constants.DisplayFlag.DRAGABLE;
import static util.Colors.grey;
import static util.Images.fillAlphaImage;
import static util.Images.loadPImage;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.Flow;
import constants.InputEvent.MouseButton;
import gui.CopyPaste;
import gui.DisplayGuiSender;
import gui.DisplayObjects;
import gui.ToggleButton;
import gui.text.Fonts;
import gui.text.TextInputField;
import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.Modules;
import module.dynio.DynIOModule;
import module.inout.IOGuiMode;
import module.inout.InOutInterface;
import patch.GuiDomainImpl;
import patch.LoligoPatch;
import pr.DisplayObjectIF;
import processing.core.PGraphics;
import processing.core.PImage;

public class NodeInspector extends InspectorGuiBox {
	
	public final static int ICON_COLOR_ACTIVE = 0xffffffff, ICON_COLOR_IDLE = grey(200);
	
	public final static PImage 
		ICON_IO_VISIBLE = fillAlphaImage(loadPImage("/resources/eye_visible.png"), ICON_COLOR_ACTIVE),
		ICON_IO_HIDDEN	= fillAlphaImage(loadPImage("/resources/eye_hidden.png") , ICON_COLOR_ACTIVE);
	
	private static final Map<Module, NodeInspector> inspectorMap = new IdentityHashMap<>();
	
	public static void updateFor(Module m) {
		runWithNodeInspector(m, NodeInspector::update);
	}
	
	public static void runWithNodeInspector(Module m, Consumer<NodeInspector> cons) {
		var ni = inspectorMap.get(m);
		if(ni != null) 
			cons.accept(ni);
	}
	
	public static NodeInspector getInspectorFor(Module m) {
		return inspectorMap.get(m);
	}
	
	public static NodeInspector openForPatch(LoligoPatch patch) {
		
		var mods = CopyPaste.getSelectedModules(patch);
		
		if(!mods.isEmpty()) {
			var m = mods.get(0);
			return openForPatch(m);
		}
		return null;
	}
	
	public static NodeInspector openForPatch(Module m) {
		
		var n = inspectorMap.get(m);
		if(n != null) {
			n.getDm().getGuiDomain().getPApplet().requestFocus(); //TODO Check if window still there
			return n;
		}
		
		var in = m.createGUISpecial();
		inspectorMap.put(m, in);  //Entry removed again by NodeInspector#removedFromDisplay()
		
		/*
		//p.getDisplayManager().getInput().setRemovableTarget(in);
		var domain = GuiDomainObject.createFor(p, in, t);
		*/
		
		var b = new GuiDomainImpl.Builder();
		b.setTitle(m.getLabel() + " (" + m.getName() + ")");
		b.setInitialWH(DEFAULT_INSPECTORWIDTH, 100);
		b.setParent(m.getDomain());
		b.setGui(in);
		b.build();
		
		return in;
	}
	
	
	public static void removeInspectorFor(Module m) {
		var ni = inspectorMap.get(m);
		if(ni != null) 
			ni.getDm().getGuiDomain().dispose();
	}
	
	
	/**
	 * Creates and initializes new NodeInspector.
	 * 
	 * @param m 
	 * 
	 * @return new initialized Nodeinspector
	 */
	public static NodeInspector newInstance(Module m) {
		
		return newInstance(m, n -> {});
	}
	
	public static NodeInspector newInstance(Module m, Consumer<NodeInspector> special) {
		
		var n = m.checkFlag(Flow.DYNAMIC_IO) ? new DynIONodeInspector((DynIOModule) m ) :
											   new NodeInspector(m);
		n.setSpecial(special);
		n.init();
		return n;
	}
	
	public static NodeInspector newInstance(NodeInspector ni) {
		ni.init();
		return ni;
	}
	 
	@Getter
	private final Module module;
	@Getter
	private final List<DisplayGuiSender<?>> ios = new ArrayList<>();
	
	
	
	@Setter
	private Consumer<NodeInspector> special;
	
	
	public NodeInspector(Module module) {
		this.module = module;
	}

	
	@Override
	public void doBody() {
		doInputs();
		doOutputs();
		doSpecial();
	}
	
	
	@Override
	public void doHeader() {
		
		barHeight = 4;
		
		var na = createLabel(module.getName().toUpperCase());
		na.setFont(Fonts.DEFAULT_DEMI_14);
		na.textSize = 14;
		na.setTextColor(0xff777777);
		
		setTop(1);
		addRow(na);
		setTop(4);
		
		var label = new TextInputField(module::setLabel, module::getLabel);
		label.singleThread();
		addLabeledEntry("Label", label);
	}
	
	public void doInputs() {
		
		doIOs(module.getInputs(), "Inputs");
		
	}
	
	public void doOutputs() {
		
		doIOs(module.getOutputs(), "Outputs");
		
	}
	
	public void doIOs(List<? extends InOutInterface> ios, String sectionName) {
		
		if(ios != null) {
			setTop(4);
			createSection(sectionName);
			ios.forEach(this::addIO);
		}
	}
	
	public void doSpecial() {
		if(special != null) {
			setTop(5);
			special.accept(this);
			setTop(1);
		}
	}
	
	
	@Override
	public void render(PGraphics g) {
	
		super.render(g);
		
		ios.forEach(DisplayGuiSender::update);
	}
	
	
	
	public DisplayGuiSender<?> createInputElement(InOutInterface io) {
		
		var gs = io.createInputElement(IOGuiMode.INSPECTOR);
		ios.add(gs);
		return gs;
	}
	
	
	public DisplayGuiSender<?>[] createIOButtons(InOutInterface io) {
		
		Consumer<Double> c = d -> {
			boolean b = d < .5;
			io.getGUI().setHidden(b);
			
			if(b) {
				io.getModule().getDomain().getModuleManager().disconnect(io);
			}
			
			Modules.positionSlots(io.getModule());
			
		};
		
		Supplier<Double> s = () -> io.getGUI().isHidden() ? 0d : 1d;
		
		var vis = new ToggleButton(c , s);
		vis.singleThread();
		vis.setMouseButton(MouseButton.LMB);
		vis.alphaMaskMode();
		vis.setMouseOverText("Toggle Visibility");
		vis.setImage(ICON_IO_VISIBLE, ICON_IO_HIDDEN);
		vis.setColorOn(0xff999999);
		vis.setColorOff(0xffee1111);
		DisplayObjects.copyDimensions(vis, ICON_IO_VISIBLE);
		//alignWH(vis, dw);
		vis.update();
		return new DisplayGuiSender[] {vis};
	}
	
	/*
	public DisplayGuiSender<?> addIO(InOutInterface io) {
		
		var gs = createInputElement(io);
		
		addLabeledEntry(io.getName(), gs);
		
		return gs;
	}
	*/
	
	
	public DisplayObjectIF createIOLabel(InOutInterface io) {
		return createLabel(io.getName());
	}
	
	
	public DisplayGuiSender<?> addIO(InOutInterface io) {
		
		//if(!io.isOptional()) 
		//	return super.addIO(io);
		
		//var ioTypes = module.getDynIOInfo().getIoTypes();
		
		//if(!ioTypes.contains(io.getIOType()))
			//return super.addIO(io);
		
		var t = createIOLabel(io);
		
		var buttons = createIOButtons(io);
		
		float btnsWidth = 0; 
		btnsWidth += buttons.length * 5; //add margin between Buttons
		
		for (var ds : buttons) {
			btnsWidth += ds.getWidth();
		}
		float cwidth = getContentWidth() - padding;
		btnsWidth /= cwidth;
		
		//float dw = (float) (getLabelHeight()) / getContentWidth();
		alignWH(t, ratio - btnsWidth);
		t.setWH(t.getWidth() - padding, t.getHeight());
		
		//for (var ds : buttons) {
			//alignWH(ds, ds.getWidth() / cwidth);
		//}
		
		var gs = createInputElement(io);
		
		alignWH(gs, 1 - ratio);
		
		var ar = new DisplayObjectIF[buttons.length + 2];
		System.arraycopy(buttons, 0, ar, 1, buttons.length);
		ar[0] = t;
		ar[ar.length - 1] = gs;
		
		addRow(true, ar);
		
		gs.setFlag(DRAGABLE, false); // TODO DynIO InputElements can be dragged around otherwise, why are regular IEs ok?
		
		return gs;
	}
	
	
	@Override
	public void removedFromDisplay() {
		super.removedFromDisplay();
		inspectorMap.remove(module);
	}

	
}
