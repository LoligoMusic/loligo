package gui.nodeinspector;

import static util.Images.fillAlphaImage;
import static util.Images.loadPImage;

import java.util.function.Consumer;

import gui.Button;
import gui.DisplayGuiSender;
import gui.DisplayObjects;
import gui.text.TextInputField;
import gui.text.TextBlock.AlignMode;
import lombok.RequiredArgsConstructor;
import lombok.var;
import module.dynio.DynIOAction;
import module.dynio.DynIOModule;
import module.dynio.DynIOModules;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import pr.DisplayObjectIF;
import processing.core.PImage;
import util.Colors;

public class DynIONodeInspector extends NodeInspector {

	public final static PImage 
		ICON_ADD_NUMBER 		= fillAlphaImage(loadPImage("/resources/add_number.png"			), ICON_COLOR_IDLE	),
		ICON_ADD_NUMBER_OVER 	= fillAlphaImage(ICON_ADD_NUMBER.copy()							 , ICON_COLOR_ACTIVE),
		ICON_ADD_COLOR  		= fillAlphaImage(loadPImage("/resources/add_color.png"			), ICON_COLOR_IDLE	),
		ICON_ADD_COLOR_OVER  	= fillAlphaImage(ICON_ADD_COLOR.copy()							 , ICON_COLOR_ACTIVE),
		ICON_ADD_TRANSFORM 		= fillAlphaImage(loadPImage("/resources/add_transform.png"		), ICON_COLOR_IDLE	),
		ICON_ADD_TRANSFORM_OVER = fillAlphaImage(ICON_ADD_TRANSFORM.copy()						 , ICON_COLOR_ACTIVE),
		ICON_ADD_UNIVERSAL		= fillAlphaImage(loadPImage("/resources/add_universal.png"		), ICON_COLOR_IDLE	),
		ICON_ADD_UNIVERSAL_OVER = fillAlphaImage(ICON_ADD_UNIVERSAL.copy()						 , ICON_COLOR_ACTIVE),
		ICON_ADD_AUDIO			= fillAlphaImage(loadPImage("/resources/add_audio.png"		)	 , ICON_COLOR_IDLE	),
		ICON_ADD_AUDIO_OVER 	= fillAlphaImage(ICON_ADD_AUDIO.copy()						 , ICON_COLOR_ACTIVE),
		ICON_DELETE 			= fillAlphaImage(loadPImage("/resources/delete.png"), 0xffaaaaaa),
		ICON_DELETE_OVER 		= fillAlphaImage(ICON_DELETE.copy(), 0xffcc4444);

	
	private final DynIOModule module;
	
	private InOutInterface lastIoByButton;
	
	public DynIONodeInspector(DynIOModule module) {
		super(module);
		this.module = module;
	}
	
	
	@RequiredArgsConstructor
	class CP implements Runnable {
		final DataType type;
		final IOType ioType;
		@Override public void run() {
			String s = DynIOModules.makeIOName(type);
			var io = createIO(s, type, ioType);
			lastIoByButton = io;
			update();
		}
	}
	
	
	private void addedDynIO(InOutInterface io, TextInputField t) {
		if(lastIoByButton != null && io == lastIoByButton) {
			lastIoByButton = null;
			t.active(true);
		}
	}
	
	
	public InOutInterface createIO(String name, DataType type, IOType iot) {
		var io = module.createDynIO(name, type, iot);
		var a = DynIOAction.getCreate(module, io.getName(), type, iot);
		module.getDomain().getHistory().addAction(a);
		return io; 
	}
	
	
	public void deleteIO(String name, DataType type, IOType iot) {
		var io = module.deleteDynIO(name, iot); 
		if(io != null) {
			var a = DynIOAction.getRemove(module, io);
			module.getDomain().getHistory().addAction(a);
		}
	}
	
	
	public void rename(InOutInterface io, String name) {
		var a = DynIOAction.getRename(io, name);
		module.getDomain().getHistory().addAction(a);
		a.execute();
		//module.rename(io, name);
	}
	
	
	public Button createButton(DataType t, IOType iot) {
		
		String ios = iot == IOType.IN ? "Input" : "Output";
		PImage p1 = null, p2 = null;
		String mo = null;
		
		switch (t) {
		case Number:
			p1 = ICON_ADD_NUMBER;
			p2 = ICON_ADD_NUMBER_OVER;
			mo = "Add Number " + ios;
			break;
		case COLOR:
			p1 = ICON_ADD_COLOR;
			p2 = ICON_ADD_COLOR_OVER;
			mo = "Add Color " + ios;
			break;
		case TRANSFORM:
			p1 = ICON_ADD_TRANSFORM;
			p2 = ICON_ADD_TRANSFORM_OVER;
			mo = "Add Transform " + ios;
			break;
		case UNIVERSAL:
			p1 = ICON_ADD_UNIVERSAL;
			p2 = ICON_ADD_UNIVERSAL_OVER;
			mo = "Add Universal " + ios;
			break;
		case AUDIOCTRL:
			p1 = ICON_ADD_AUDIO;
			p2 = ICON_ADD_AUDIO_OVER;
			mo = "Add Audio " + ios;
			break;
		default:
			break;
		}
		
		Button bn = new Button(new CP(t, iot), "");
		bn.setImage(p1, p2);
		bn.setMouseOverText(mo);
		
		return bn;
	}
	
	@Override
	public void doInputs() {
		super.doInputs();
		if(module.getDynIOInfo().isInput()) {
			addButtons(IOType.IN);
		}
	}
	
	@Override
	public void doOutputs() {
		super.doOutputs();
		if(module.getDynIOInfo().isOutput()) {
			addButtons(IOType.OUT);
		}
	}
	
	
	public Button[] addButtons(IOType iot) {
		
		var dataTypes = module.getDynIOInfo().getDataTypes();
		Button[] bts = new Button[dataTypes.size()];
		int i = 0;
		
		for(DataType t : dataTypes) {
			
			Button b = createButton(t, iot);
			bts[i] = b;
			i++;
		}
		align(RIGHT);
		addRow(bts);
		align(LEFT);
		return bts;
	}
	
	@Override
	public DisplayGuiSender<?> addIO(InOutInterface io) {
		return super.addIO(io);
		//addedDynIO(io, t);
	}

	@Override
	public DisplayGuiSender<?>[] createIOButtons(InOutInterface io) {
		
		Runnable r = () -> {
			deleteIO(io.getName(), io.getType(), io.getIOType());
			clear();
			init();
		};
		
		var del = new Button(r, "");
		del.setMouseOverText("Remove " + io.getName());
		del.setImage(ICON_DELETE, ICON_DELETE_OVER);
		DisplayObjects.copyDimensions(del, ICON_DELETE);
		
		DisplayGuiSender<?>[] bs = super.createIOButtons(io);
		DisplayGuiSender<?>[] out = new DisplayGuiSender<?>[bs.length + 1];
		System.arraycopy(bs, 0, out, 1, bs.length);
		out[0] = del;
		
		return out;
	}


	@Override
	public DisplayObjectIF createIOLabel(InOutInterface io) {
		
		Consumer<String> cs = s -> {
			rename(io, s);
		};
		
		TextInputField t = new TextInputField(cs, io::getName);
		int c = Colors.grey(200);
		t.setUndoable(false);
		t.setText_color_active(c);
		t.setText_color_inactive(c);
		t.setTextColor(c);
		t.singleThread();
		
		t.setAlignMode(AlignMode.LEFT);
		t.bgBox = false;
		return t;
	}
	
}
