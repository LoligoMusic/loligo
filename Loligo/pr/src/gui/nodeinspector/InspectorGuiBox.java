package gui.nodeinspector;

import constants.DisplayFlag;
import gui.DisplayGuiSender;
import gui.GUIBox;
import gui.text.Fonts;
import gui.text.TextBox;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayObjectIF;

public class InspectorGuiBox extends GUIBox{

	public static final int DEFAULT_INSPECTORWIDTH = 300;
	
	@Getter@Setter
	private int contentWidth = DEFAULT_INSPECTORWIDTH;
	@Getter@Setter
	private int labelHeight = 20;
	protected float ratio = .5f;
	
	
	public void init() {
		paddingBottom = 20;
		doHeader();
		doBody();
		postInit();
	}
	
	public void update() {
		clear();
		init();
	}
	
	
	public void doHeader() {
		
	}
	
	public void doBody() {
		
	}
	
	public void postInit() {
		
	}
	
	
	public void addInspectorElement(DisplayObjectIF d) {
		addInspectorElement(d, 1);
	}

	
	public void addInspectorElement(DisplayObjectIF d, int height) {
		
		d.setWH(contentWidth, d.getHeight());
		setTop(height);
		addRow(d);
	}
	
	public void alignWH(DisplayObjectIF d, float ratio) {
		
		d.setWH((int)((contentWidth - padding) * ratio) , labelHeight);
	}
	
	public TextBox createLabel(String text) {
		
		TextBox t = new TextBox(text);
		t.setWH(80, 20);
		return t;
	}
	
	public TextBox createSection(String text) {
		
		TextBox t = createLabel(text);
		t.setFont(Fonts.DEFAULT_DEMI_12);
		t.textSize = 12;
		t.setTextColor(0xffffffff);
		t.setWH(80, 20);
		addRow(t);
		return t;
	}
	
	
	public void addLabeledEntry(String name, DisplayGuiSender<?> d) {
		
		TextBox t = createLabel(name);
		alignWH(t, ratio);
		
		d.setFlag(DisplayFlag.lockedPosition, true);
		alignWH(d, 1 - ratio);
		addRow(t, d);
	}
	
}
