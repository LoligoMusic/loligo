package gui;

import java.util.function.Consumer;
import java.util.function.Supplier;

import lombok.Getter;
import lombok.Setter;

public class Range implements Consumer<Double>, Supplier<Double>{
	
	@Getter@Setter
	private double min, max;
	private final Consumer<Double> consumer;
	private final Supplier<Double> supplier;
	@Getter
	private boolean isLimited = true; 
	/**
	 * Default Range Double.MIN_VALUE to MAX_VALUE 
	 */
	public Range(Consumer<Double> consumer, Supplier<Double> supplier) {
		
		this(consumer, supplier, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
	}
	
	public Range(Consumer<Double> consumer, Supplier<Double> supplier, double min, double max) {
		
		this.consumer = consumer;
		this.supplier = supplier;
		
		setRange(min, max);
	}

	
	public void setRange(double min, double max) {
		this.min = min;
		this.max = max;
		isLimited = min != Double.NEGATIVE_INFINITY && max != Double.POSITIVE_INFINITY;
	}

	public double applyRange(double t) {
		
		double d = Math.min(t, max);
			   d = Math.max(d, min);
		return d;
	}
	
	@Override
	public void accept(Double t) {
		consumer.accept(applyRange(t));
	}

	@Override
	public Double get() {
		return applyRange(supplier.get());
	}
}