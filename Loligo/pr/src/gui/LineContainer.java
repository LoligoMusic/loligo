package gui;

import static constants.InputEvent.OVER;
import static constants.SlotEvent.SLOT_STARTTARGET;
import static constants.SlotEvent.SLOT_STOPTARGET;
import static module.inout.DataType.TRANSFORM;
import static module.inout.IOType.IN;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import constants.DisplayFlag;
import constants.InputEvent;
import constants.SlotEvent;
import constants.SlotEventListener;
import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.Modules;
import module.inout.DataType;
import module.inout.InOutGUI;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.Output;
import patch.Domain;
import patch.LoligoPatch;
import pr.DisplayManager;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import util.Colors;


public class LineContainer extends DisplayObject implements SlotEventListener{
	
	private static final int COL_HIGH   = 0xffffffff, 
							 COL_NORMAL = 0xffaaaaaa;
	
	private static final float STROKE_HIGH = 1, STROKE_NORMAL = .5f;
	
	private ArrayList<Line> lines = new ArrayList<Line>();
	
	private HashMap<Input, Line> lineMap = new HashMap<>();
	//private InOut lastHighlighted; 
	private float mouseStrokeWeight = .5f;
	private int mouseStrokeColor = 0xffaaaaaa;
	private final List<Input> inSlots = new ArrayList<>();
	@Setter
	private boolean showMouseLine = true;
	@Getter
	private InOutInterface lineTarget;
	private Domain domain;
	
	
	public void init(DisplayManager displayManager) {
		this.domain = displayManager.getDomain();
		displayManager.getInput().addInputEventListener(this);
		displayManager.getDomain().getModuleManager().addSlotEventListener(this);
	}
	
	public void setLineTarget(Input d) {
		lineTarget = d;
		domain.getModuleManager().dispatchSlotEvent(SLOT_STARTTARGET, (Input)d, null);
	}
	
	public void setLineTarget(Output d) {
		lineTarget = d;
		domain.getModuleManager().dispatchSlotEvent(SLOT_STARTTARGET, null, (Output)lineTarget);
	}
	
	public void clearLineTarget() {
		
		if(lineTarget != null && domain != null) {
			var man = domain.getModuleManager();
			if(lineTarget instanceof Input) {
				man.dispatchSlotEvent(SLOT_STOPTARGET, (Input) lineTarget, null);
			}else {
				man.dispatchSlotEvent(SLOT_STOPTARGET, null, (Output)lineTarget);
			}
			lineTarget = null;
		}
	}
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		var dm = getDm();
		if(dm == null || input != dm.getInput())
			return;
		
		switch(e) {
		case OVER, OVER_STOP -> {
			
			var d = dm.getInput().getOverTarget();
			if(d instanceof InOutGUI iog) {
				
				var s = iog.getInOut();
				var m = s.getModule();
				
				if(m != null && domain.getContainedModules().contains(m)) {
					highlight(s, e == OVER);
				}
			}
		}
		case DRAG_STOP -> {
			
			if(lineTarget != null && input.getOverTarget() == null && input.ctrl) {
				
				createIOBox(input);
			}
		}
		default -> {}
		}
		
		if(e.isReleaseEvent) 
			removeMouseLine();
	}

	private void createIOBox(UserInput input) {
		
		var n = DataType.getIOEntry((LoligoPatch) domain, lineTarget);
		
		if(n != null) {
			
			Module m = n.createInstance();
			Modules.initializeModuleDefault(m, domain);
			
			m.getGui().setPos(
					input.getMouseX(), 
					input.getMouseY());
			
			InOutInterface io = null;
			
			if(lineTarget.getIOType() == IN) {
				
				io = m.getOutputs().get(0);
				
				if(lineTarget.getType() != TRANSFORM) {
					List<Input> ins = m.getInputs();
					if(ins != null && ins.size() > 0) {
						
						m.getInputs().get(0).setValueObject(lineTarget.getValueObject());
					}
				}
				
			}else {
				io = m.getInputs().get(0);
			}
			
			lineTarget.getValueObject();
		
			domain.getModuleManager().connect(io, lineTarget);
		}
	}
	
	
	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		
		InOutInterface io = in == null ? out : in;
		var d = io.getModule().getDomain();
		
		if(d == null || d.getDisplayManager().getLineContainer() != this)
			return;
		
		switch(e) {
		case SLOT_STARTTARGET -> {
			if(in != null) {
				
				if(domain.getContainedModules().contains(in.getModule()))
					if(in.getPartner() != null) {
						addMouseLine(in.getPartner());
					}else {
						addMouseLine(in);
					}
				
			}else if(out != null && domain.getContainedModules().contains(out.getModule())){
				addMouseLine(out);
			}
		}
		case SLOT_CONNECT -> {
			if(in.getGUI() != null && out.getGUI() != null && domain.getContainedModules().contains(in.getModule())) {
				addLine(in);
			}
			
		}
		case SLOT_DISCONNECT -> {
			if(lineMap.containsKey(in))
				removeLine(in);
		}
		default -> {}
		}
	}
	
	public void updateInSlotList() {
		
		if(domain.getContainedModules() != null) {
			
			inSlots.clear();
			for (Module m : domain.getContainedModules())
				if(m.getInputs() != null)
					for(Input s : m.getInputs())
						inSlots.add(s);
		}
	}
	
	@Override
	public void render(PGraphics g) {
		
		g.strokeWeight(.5f);
		g.noFill();
		
		if(lineTarget != null && showMouseLine){
			
			g.stroke(mouseStrokeColor);
			g.strokeWeight(mouseStrokeWeight);
			g.line(
					lineTarget.getGUI().getX(), 
					lineTarget.getGUI().getY(), 
					getDm().getInput().getMouseX(), 
					getDm().getInput().getMouseY());
		}
		
	}
	
	private void addLine(Input inSlot){
		
		if(inSlot == null || inSlot.getPartner() == null || lineMap.containsKey(inSlot)) 
			return;
		
		Line line = new Line(inSlot);
		lines.add(line);
		lineMap.put(inSlot, line);
		addChild(line);
	}
	
	
	private void removeLine(Input inSlot){
		
		Line line = lineMap.get(inSlot);
		
		if(line != null) {
			
			DisplayObjects.removeFromDisplay(line);
			lines.remove(line);
			lineMap.remove(inSlot);
		}
	}
	
	public void repairLines() {
		
		for (var m : domain.getContainedModules()) 
			if(m.getInputs() != null)
				for (Input s : m.getInputs()) 
					addLine(s);
	
	}
	
	
	public void highlight(InOutInterface slot, boolean high) {
		
		if(slot.getNumConnections() < 1)
			return;
		
		Input[] ins = null;//InputEvent.
		
		if(slot instanceof Input in) {
			ins = new Input[] {in};
		}else 
		if(slot instanceof Output out) {
			var o = out.getPartners();
			ins = o.toArray(Input[]::new);
		}
		
		if(ins != null)
			for (Input i : ins) {
				Line line = lineMap.get(i);
				if(line != null)
					line.setColor(high ? COL_HIGH: COL_NORMAL);
			}
	}
	
	public void addMouseLine(InOutInterface d){
		
		mouseStrokeColor = Colors.grey(200);
		mouseStrokeWeight = .5f;
		
		lineTarget = d;
	}
	
	public void removeMouseLine(){
		lineTarget = null;
	}
	
	
	class Line extends DisplayObject{
	
		final Input inSlot;
		final Output outSlot;
		final Position inPos;
		final Position outPos;
		
		public Line(Input inSlot) {
			
			this.inSlot = inSlot;
			outSlot = inSlot.getPartner();
			setColor(COL_NORMAL);
			
			inPos = new Position.PositionOffset(inSlot.getGUI());
			outPos = new Position.PositionOffset(outSlot.getGUI());
		}
		
		@Override 
		public void render(PGraphics g) {
			
			float dy = Math.abs(inSlot.getGUI().getY() - outSlot.getGUI().getY());
			dy = dy > 60 ? 60 : dy;
			inPos.setPos(-dy, 0);
			outPos.setPos(dy, 0);
			
			if (inSlot.getGUI().checkFlag(DisplayFlag.mouseOver) || outSlot.getGUI().checkFlag(DisplayFlag.mouseOver)) {
				
				g.stroke(COL_HIGH);
				g.strokeWeight(STROKE_HIGH);
				
			}else{
				
				g.stroke(COL_NORMAL);
				g.strokeWeight(STROKE_NORMAL);
				
			}
			
			g.stroke(getColor());
			g.strokeWeight(STROKE_NORMAL);
			g.bezier(inSlot.getGUI().getX(), inSlot.getGUI().getY(), 
						inPos.getX(), inPos.getY(),
						outPos.getX(), outPos.getY(),
						outSlot.getGUI().getX(), outSlot.getGUI().getY());
		}
	}
	
}

