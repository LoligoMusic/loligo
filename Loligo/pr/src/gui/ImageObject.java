package gui;

import constants.DisplayFlag;
import pr.DisplayObject;
import processing.core.PGraphics;
import processing.core.PImage;
import util.Images;

public class ImageObject extends DisplayObject {

	private PImage img;

	public ImageObject(final PImage img) {
		this.img = img;
		setWH(img.width, img.height);
		setFlag(DisplayFlag.RECT, true);
	}
	
	public ImageObject(final String path) {
		this(Images.loadPImage(path));
	}
	
	@Override
	public void render(PGraphics g) {
		g.image(img, getX(), getY());
	}
	
}
