package gui;

import constants.DisplayFlag;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayObject;
import pr.Positionable;
import processing.core.PGraphics;

public class Arrow extends DisplayObject {
	
	@Setter@Getter
	private Positionable pos1, pos2;
	@Setter@Getter
	private boolean isHeadVisible = true;
	@Setter@Getter
	private float headOffset = 0, lineWeight = 2;
	
	public Arrow() {
		setColor(0xffaaaaaa);
		setFlag(DisplayFlag.CLICKABLE, false);
		
	}
	
	public void remove() {
		
	}

	@Override
	public void render(PGraphics g) {
		int c = getColor();
		g.stroke(c);
		g.strokeWeight(lineWeight);
		
		float x1 = pos1.getX(), y1 = pos1.getY(),
			  x2 = pos2.getX(), y2 = pos2.getY();
		g.line(x1, y1, x2, y2);
		g.noStroke();
		
		if(isHeadVisible) {
			g.fill(c);
			g.pushMatrix();
			float r = -1.57f + (float) (Math.atan2(y2 - y1, x2 - x1) );
			g.translate(x2, y2);
			g.rotate(r);
			float o = headOffset;
			g.triangle(0, o, 5, -12+o, -5, -12+o);
			g.popMatrix();
		}
	}
}