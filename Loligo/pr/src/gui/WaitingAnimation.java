package gui;

import patch.LoligoPatch;
import pr.DisplayObject;
import pr.ExitFrameListener;
import processing.core.PApplet;
import processing.core.PGraphics;

public class WaitingAnimation extends DisplayObject implements ExitFrameListener{

	private boolean remove, hideBackground = false;
	
	public WaitingAnimation() {
		
		setWH(60, 60);
		setColor(0xffffffff);
	}
	
	public WaitingAnimation start(LoligoPatch patch) {
		PApplet proc = patch.getPApplet();
		setPos(proc.width / 2, proc.height / 2);
		patch.getDisplayManager().addGuiObject(this);
		return this;
	}
	
	@Override
	public void addedToDisplay() {
	
		getDm().addExitFrameListener(this);
	}
	
	
	@Override
	public void render(PGraphics g) {
	
		if(remove)
			return;
		
		if(hideBackground) {
			g.background(0xff000000);
		}
		
		g.noFill();
		g.strokeWeight(2);
		
		g.stroke(getColor());
		
		float x = getX(),
			  y = getY();
		
		g.circle(x, y, getWidth());
		
		g.noStroke();
		g.fill(getColor());
		
		float w = getWidth() / 2;
		float dt = getDm().getFrameCount() / 7f;
		
		g.circle(
			x + w * (float)Math.sin(dt), 
			y + w * (float)Math.cos(dt), 
			10
		);
	}
	
	/**
	 * Marks this for removal from animation thread
	 */
	public void removeAnimation() {
		
		remove = true;
	}

	public void hideBackground() {
		hideBackground = true;
	}

	@Override
	public void exitFrame() {
		
		if(remove) {
			DisplayObjects.removeFromDisplay(this);
			getDm().removeExitFrameListener(this);
		}
	}
}
