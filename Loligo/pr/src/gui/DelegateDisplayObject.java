package gui;

import java.util.List;
import java.util.function.Consumer;
import constants.DisplayFlag;
import constants.InputEvent;
import gui.selection.GuiType;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayContainerIF;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public class DelegateDisplayObject<T extends DisplayObjectIF> implements DisplayObjectIF{
	
	@Setter@Getter
	private T delegate;
	@Setter@Getter
	private DisplayObjectIF delegateParent;
	
	
	public void reparent() {
		delegate.setParent(delegateParent);
	}

	public void mouseOverRest(boolean on) {
		delegate.mouseOverRest(on);
	}

	public void mouseEvent(InputEvent e, UserInput input) {
		delegate.mouseEvent(e, input);
	}

	public void addedToDisplay() {
		reparent();
		delegate.addedToDisplay();
	}

	public void removedFromDisplay() {
		delegate.removedFromDisplay();
	}

	public boolean hitTest(float mx, float my) {
		return delegate.hitTest(mx, my);
	}

	public void render(PGraphics g) {
		delegate.render(g);
	}

	public void addChild(DisplayObjectIF d) {
		delegate.addChild(d);
	}

	public void addChild(DisplayObjectIF... dArr) {
		delegate.addChild(dArr);
	}

	public void removeChild(DisplayObjectIF d) {
		delegate.removeChild(d);
	}

	public void removeChild(DisplayObjectIF[] dArr) {
		delegate.removeChild(dArr);
	}

	public void setSelectionObject(Consumer<PGraphics> cons) {
		delegate.setSelectionObject(cons);
	}

	public void setSelected(boolean b) {
		delegate.setSelected(b);
	}

	public void fixedToParent(boolean b) {
		delegate.fixedToParent(b);
	}

	public float getX() {
		return delegate.getX();
	}

	public float getY() {
		return delegate.getY();
	}

	public void setPos(float x, float y) {
		delegate.setPos(x, y);
	}

	public void setGlobal(float x, float y) {
		delegate.setGlobal(x, y);
	}

	public Position getPosObject() {
		return delegate.getPosObject();
	}

	public void setPosObject(Position p) {
		delegate.setPosObject(p);
	}

	public DisplayObjectIF getParent() {
		return delegate.getParent();
	}

	public void setParent(DisplayObjectIF parent) {
		delegate.setParent(parent);
		delegateParent = parent;
	}

	public List<DisplayObjectIF> getChildren() {
		return delegate.getChildren();
	}

	public DisplayContainerIF getDm() {
		return delegate.getDm();
	}

	public void setDm(DisplayContainerIF dm) {
		delegate.setDm(dm);
	}

	public boolean checkFlag(DisplayFlag flag) {
		return delegate.checkFlag(flag);
	}

	public void setFlag(DisplayFlag f, boolean b) {
		delegate.setFlag(f, b);
	}

	public int getColor() {
		return delegate.getColor();
	}

	public int setColor(int color) {
		return delegate.setColor(color);
	}

	public int getStrokeColor() {
		return delegate.getStrokeColor();
	}

	public void setStrokeColor(int strokeColor) {
		delegate.setStrokeColor(strokeColor);
	}

	public int getWidth() {
		return delegate.getWidth();
	}

	public void setWH(int width, int height) {
		delegate.setWH(width, height);
	}

	public int getHeight() {
		return delegate.getHeight();
	}

	public GuiType getGuiType() {
		return delegate.getGuiType();
	}

	public void setGuiType(GuiType guiType) {
		delegate.setGuiType(guiType);
	}

	public void mouseEventThis(InputEvent e, UserInput input) {
		delegate.mouseEventThis(e, input);
	}
	
}
