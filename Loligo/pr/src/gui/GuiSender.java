package gui;

import pr.DisplayContainerIF;


public interface GuiSender<T> {
	public T getValue();
	public void setValue(T v);
	public void update();
	public void disable(boolean b);
	public DisplayContainerIF getDm();
	
	default public void singleThread() {}
}
