package gui.selection;

public enum FocusContext {

	PATCH,
	ELEMENT,
	/**
	 * External window like Save/Load prompt
	 */
	WINDOW,  
	NONE;
}
