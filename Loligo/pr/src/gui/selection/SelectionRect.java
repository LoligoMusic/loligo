package gui.selection;

import java.util.function.Function;

import constants.DisplayFlag;
import constants.InputEvent;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PGraphics;

class SelectionRect extends DisplayObject {
	
	float x1, y1, x2, y2;
	final UserInput ui;
	final SelectionManager selection;
	
	public SelectionRect(SelectionManager selection) {
		
		setFlag(DisplayFlag.RECT, true);
		setFlag(DisplayFlag.CLICKABLE, false);
		
		this.selection = selection;
		ui = selection.getInput();
		
		this.x1 = this.x2 = ui.getMouseX();
		this.y1 = this.y2 = ui.getMouseY();
		
		setPos(x1,  y1);
		
	}
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		
	}
	
	
	@Override
	public void render(PGraphics g) {
		
		g.noFill();
		g.stroke(0x77777777);
		g.strokeWeight(1);
		g.rectMode(PConstants.CORNERS);
		
		x2 = ui.getMouseX();
		y2 = ui.getMouseY();
		
		setWH((int) (x2 - x1), (int) (y2 - y1));
		
		if(getWidth() < 0) {
			
			setWH(getWidth() * -1, getHeight());
			setPos(x2, getY());
		}
		if(getHeight() < 0) {
			
			setWH(getWidth(), getHeight() * -1);
			setPos(getX(), y2);
		}
		
		g.rect(x1, y1, x2, y2);
		
		g.noStroke();
		g.rectMode(PConstants.CORNER);
		
		select();
	}
	
	
	public void select() {
		
		
			Function<DisplayObjectIF, Boolean> c = new Function<DisplayObjectIF, Boolean>() {
			
				@Override
				public Boolean apply(DisplayObjectIF d) {
					
					if(!d.getGuiType().isModule() || !d.checkFlag(DisplayFlag.CLICKABLE))
						return false;
					
					DisplayObjectIF m = d;//ModuleManager.getModuleFromGui(d);
				
					float x1 = d.getX(), y1 = d.getY(), x2 = x1 + d.getWidth(), y2 = y1 + d.getHeight();
					
					if( hitTest(x1, y1) || 
					    hitTest(x2, y2) || 
						hitTest(x1, y2) || 
						hitTest(x2, y1)) {
						
							if(!selection.contains(m)) {
								
								selection.add(m);
							}
					
					}else if(!selection.getInput().ctrl){
						
						selection.remove(m);
					}
					return false;
				}
			};
			
			ui.getDisplayObjectAt(0, 0, c);
		
	}
	
}