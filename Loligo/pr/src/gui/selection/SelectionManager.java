package gui.selection;

import static constants.DisplayFlag.DRAGABLE;
import static constants.InputEvent.DRAG_START;
import static constants.InputEvent.FOCUS_LOST;
import static constants.InputEvent.PRESSED;
import static constants.InputEvent.RELEASED;
import static constants.InputEvent.SELECT_REMOVE;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayObjects;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import module.Module;
import module.ModuleContainer;
import module.ModuleManager;
import patch.Domain;
import pr.DisplayObjectIF;
import pr.EventSubscriber;
import pr.userinput.UserInput;


public class SelectionManager implements EventSubscriber {

	@Getter
	private final UserInput input;
	//private final ContainerList<DisplayObjectIF, SelPos> modules = new ContainerList<>();
	private List<DisplayObjectIF> selection;
	@Getter@Setter
	private DisplayObjectIF activeElement;
	@Getter
	private DisplayObjectIF inputElement;
	private SelectionRect rect;
	@Getter
	private GuiType guiType;
	@Getter
	private DisplayObjectIF[] lastAction;
	
	public SelectionManager(final UserInput input) {
		
		this.input = input;
		selection = new ArrayList<>();
		
		input.addInputEventListener(this);
	}
	
	
	public void add(DisplayObjectIF... mm) {
		
		if(mm == null || mm.length <= 0)
			return;
		
		var g = mm[0].getGuiType();
		if(g == GuiType.INPUT) {
			inputElement = mm[0];
			guiType = GuiType.INPUT;
			return;	
		}
		inputElement = null;
		
		for (int i = 0; i < mm.length; i++) {
			
			var d = mm[i];
			if(d.getGuiType().priority < g.priority)
				g = d.getGuiType();
		}
		
		if(guiType == null) {
			guiType = g;
		}else 
		if(guiType.priority > g.priority){
			clear();
			guiType = g;
		}
		
		int count = 0;
		
		for(var m : mm) {
			
			if(m.getGuiType().priority <= guiType.priority && !selection.contains(m)) {
				
				m.setSelected(true);
				selection.add(m);
				count++;
				
				if(m.checkFlag(DisplayFlag.SUBSCRIBE_ON_SELECT))
					input.addInputEventListener(m);
			}
		}
		
		if(count > 0) {
		
			lastAction = mm;
			
			for(var d : mm)
				input.dispatchInputEvent(InputEvent.SELECT_ADD, d);
			
		}
		
	}
	
	
	public boolean remove(DisplayObjectIF m) {
		
		boolean b = rem(m);
		
		if(b) {
			lastAction = new DisplayObjectIF[]{m};
			input.dispatchInputEvent(InputEvent.SELECT_REMOVE, m);
		}
		return b;
	}
	
	public void includeContainedModules() {
		
		var list = new ArrayList<>(selection);
		int i = 0;
		
		while(i < list.size()) {
			
			var p = list.get(i);
			var m = ((Domain)input.getDomain()).getModuleManager().getModuleFromGui(p);
			
			if(m instanceof ModuleContainer mc) {
				
				for(var mm : mc.getContainedModules()) {
					
					var g = mm.getGui();
					if(g != null)
						list.add(g);
				}
			}
			i++;
		}
		
		if(!list.isEmpty()) {
			
			var sel = list.toArray(DisplayObjectIF[]::new);
			add(sel);
		}
	}
	
	
	private boolean rem(DisplayObjectIF m) {
		
		boolean b = selection.remove(m);
		m.setSelected(false);
		
		if(m.checkFlag(DisplayFlag.SUBSCRIBE_ON_SELECT))
			input.removeInputEventListener(m);
		
		return b;
	}
	
	
	public boolean contains(DisplayObjectIF m) {
		
		return selection.contains(m);
	}
	
	
	public void clear() {
		
		lastAction = selection.toArray(DisplayObjectIF[]::new);
		
		while (selection.size() > 0) 
			rem(selection.get(0));
			
		guiType = null;
		inputElement = null;
		
		for(DisplayObjectIF d : lastAction)
		input.dispatchInputEvent(SELECT_REMOVE, d);
	}
	
	
	public List<DisplayObjectIF> getSelection() {
		
		return ImmutableList.copyOf(selection);
	}
	
	
	public int getNumSelected() {
		
		return selection.size();
	}
	
	
	public int getOriginX() {
		
		return getSmallest(m -> (int) m.getX());
	}
	
	
	public int getOriginY() {
		
		return getSmallest(m -> (int) m.getY());
	}
	
	
	private int getSmallest(Function<DisplayObjectIF, Integer> b) {
		
		int x = Integer.MAX_VALUE;
		
		for (var m : selection) {
			
			int px = b.apply(m);
			if(px < x)
				x = px;
		}
		return x;
		
	}
	
	
	/*
	public void setPos(float nx, float ny) {
		
		float dx = getOriginX() - nx,
			  dy = getOriginY() - ny;
		
		for (AbstractModule m : modules) {
			
			m.gui.setPos(m.gui.getX() + dx, m.gui.getY() + dy);
		}
	}*/
	
	
	
	public void apply(Consumer<DisplayObjectIF> consumer) {
		
		selection.forEach(consumer);
	}
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(input != this.input)
			return;
		if(rect != null)
			rect.mouseEvent(e, input);
		
		if(input.isCurrentTarget(inputElement, FOCUS_LOST)) {
			
			inputElement = null;
			
		}else
		if(e.isDragEvent) {
		
			var m = input.getDragTarget();
			if(e == DRAG_START) {
				
				if(m != null && m.checkFlag(DRAGABLE) && !selection.contains(m)) {
					
					if(!selection.isEmpty()) {
						clear();
					}
					add(m);
				}
			}
		}else
		if(e.isClickEvent) {
			
			if(e == PRESSED) {
				
				var m = input.getPressedTarget();
				
				if(m == null) {
					
					if(!input.ctrl)
						clear();
					
					if(rect == null) {
						rect = new SelectionRect(this);
						input.display.addGuiObject(rect);
					}
					
				}else
				if(contains(m)) {
				
					if(input.ctrl)
						remove(m);
				
				}else{
					
					boolean ctrl = input.ctrl,
							single = m.getGuiType().isMultiSelect(),
							inp = m.getGuiType() != GuiType.INPUT;
					
					if(inp && (!ctrl || single) )
						clear();
					
					if(m.getGuiType() != GuiType.NO_SELECT)
						add(m);
				}
				
			}else 
			if(e == RELEASED) {
				
				if(rect != null) {
					removeSelectionRect();
				}
				
			}
		}
		
	}
	
	
	public void setSelection(List<DisplayObjectIF> sel) {
		
		selection.removeIf(d -> {
			
			if(!sel.contains(d)) {
				
				d.setSelected(false);
				return true;
			}
			return false;
		});
		
		for (var d : sel) {
			
			if(!selection.contains(d)) {
				
				selection.add(d);
				d.setSelected(true);
			}
		}
	}

	public void selectModules(List<Module> ms) {
		
		var ds = ms.stream().map(Module::getGui)
							.filter(Objects::nonNull)
							.toArray(DisplayObjectIF[]::new);
		add(ds);
	}
	
	public void removeSelectionRect() {
		DisplayObjects.removeFromDisplay(rect);
		rect = null;
	}
	
	public void selectAll() {
		
		@SuppressWarnings("unchecked")
		var ms = (List<Module>) ((Domain)input.getDomain()).getContainedModules();
		selectModules(ms);
	}

	public void invert() {
		
		var ms = ((Domain)input.getDomain()).getContainedModules();
		var ds = ms.stream()
				   .map(Module::getGui)
				   .filter(Objects::nonNull)
				   .filter(d -> !selection.contains(d))
				   .toArray(DisplayObjectIF[]::new);
		clear();
		add(ds);
	}

	public void removeActiveElement(DisplayObjectIF d) {
		
		activeElement = activeElement == d ? null : activeElement;
	}

	
	public List<Module> getSelectedModules() {
		return selection.stream()
						.map(((Domain)input.getDomain()).getModuleManager()::getModuleFromGui)
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
	}
	
	
	public void selectLinked() {
		
		if(getNumSelected() > 0) {
			
			var mods = getSelectedModules();
			
			var ms = ModuleManager.getConnected(mods);
			selectModules(ms);
		}
	}
	
}
