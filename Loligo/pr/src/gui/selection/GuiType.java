package gui.selection;

import java.util.EnumSet;

public enum GuiType {

	READER(1),
	ANIM(1),
	AUDIO(1),
	SIGNAL(1),
	CONSTRAINT(1),
	
	DRAGABLE_INPUT(11),
	DRAGABLE_FREE(5),
	
	INPUT(10),
	WINDOW(100),
	NO_SELECT(1000);
	
	
	private static final EnumSet<GuiType> modules = EnumSet.range(READER, CONSTRAINT);
	private static final EnumSet<GuiType> multiSelect = EnumSet.range(READER, DRAGABLE_FREE);
	
	public final int priority;
	
	private GuiType(int priority) {
		
		this.priority = priority;
		
	}

	public boolean isModule() {
		return modules.contains(this);
	}
	
	public boolean isMultiSelect() {
		return multiSelect.contains(this);
	}
	
}
