package gui;

import java.util.function.Consumer;
import java.util.function.Supplier;

import history.Action;
import history.SetValueAction;
import lombok.var;
import patch.Domain;
import pr.DisplayContainerIF;


public class GuiSenderObject<T> implements GuiSender<T> {
	
	private T value;
	
	private final GuiSender<T> client;
	private final Consumer<T> action;
	private final Supplier<T> updateAction;
	private Consumer<GuiSenderObject<T>> onUpdate = g -> {};
	private boolean isSingleThread = false;
	
	public GuiSenderObject(final GuiSender<T> client, final Consumer<T> action, final Supplier<T> updateAction, final T value) {
		
		this.action = action;
		this.value = value;
		this.client = client;
		this.updateAction = updateAction;
	}
	
	public void singleThread() {
		isSingleThread = true;
	}
	
	public void message(T v) {
		message(v, true);

	}
	
	public void message(T v, boolean undoable) {
		
		if(action != null) {
			
			SetValueAction<T> a = new SetValueAction<>(client, value, v, action);
			DisplayContainerIF dm = getDm();
			
			if(isSingleThread) {
				
				a.execute();
				if(undoable)
					dm.getDomain().getHistory().addAction(a);
			}else
			if(dm != null) {
				var dom = dm.getDomain();
				var bdom = Domain.getBaseDomain(dom);
				dom.getHistory().executeIn(bdom, a, undoable);
			}
			//client.update(); //{@link action} muss eine funktion aufrufen, die client updatet!
		}
	}
	
	
	@Deprecated
	public String getName(){return null;}
	
	
	public T getValue() {
		return value;
	}
	
	
	public void setValue(T v) {
		value = v;
	}
	
	
	@Override
	public void update() {
		
		if(updateAction != null) {
			
			Action a = () -> {
				T t = updateAction.get();
				client.setValue(t);
				onUpdate.accept(this);
			};
			
			var dm = getDm();
			if(isSingleThread) 
				a.execute();
			else
			if(dm != null)
				dm.getDomain().getHistory().execute(a, false);
		}
	}
	
	public void onUpdate(Consumer<GuiSenderObject<T>> cons) {
		onUpdate = cons;
	}
	
	@Override
	public void disable(boolean b) {
		client.disable(b);
	}


	@Override
	public DisplayContainerIF getDm() {
		
		return client.getDm();
	}
}
