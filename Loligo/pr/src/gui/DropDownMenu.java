package gui;

import static constants.DisplayFlag.CLICKABLE;
import static constants.InputEvent.CLICK;
import static gui.text.TextBlock.AlignMode.CENTER;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.text.TextBox;
import gui.text.TextInputField;
import lombok.Setter;
import lombok.experimental.Delegate;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import util.Colors;

public class DropDownMenu extends DisplayObject implements DisplayGuiSender<Integer>{
	
	private final TextBox textBox = new TextBox();
	public List<String> items;
	public String noSelectionText = "-";
	private final GuiSenderObject<Integer> sender;
	private int mousePos, itemHeight = 16, top = 5, bottom = 5;
	public boolean open = false;
	public int mouseOverItem = -1;
	public DisplayObject menu;
	private boolean enabled = true, autoAdapt = false;
	@Setter
	private Supplier<String[]> itemsListUpdateAction;
	@Delegate(types=MouseOverRestInterface.class)
	private final MouseOverTextHandler mouseOverText = new MouseOverTextHandler(this);
	
	
	public DropDownMenu(Consumer<Integer> action, Supplier<Integer> updateAction, Supplier<String[]> itemsListUpdateAction) {
		this(action, updateAction);
		this.itemsListUpdateAction = itemsListUpdateAction;
	}
	
	
	public DropDownMenu(Consumer<Integer> action, Supplier<Integer> updateAction, String... items ) {
		
		sender = new GuiSenderObject<Integer>(this, action, updateAction, -1);
		
		setFlag(CLICKABLE, true);
		textBox.setFlag(DisplayFlag.fixedToParent, true);
		addChild(textBox);
		textBox.bgBox = true;
		textBox.autoFit = true;
		
		setColor(TextInputField.COLOR_INACTIVE);
		textBox.setTextColor(TextInputField.TEXT_COLOR_INACTIVE);
		
		init(items);
	}
	
	
	private final void init(String... _items) {

		setFlag(DisplayFlag.RECT, true);
		
		if(_items != null)
			items = new ArrayList<String>(Arrays.asList(_items));
	
		menu = new MenuList();
		
		adaptSize();
		setWH(getWidth(), itemHeight);
		textBox.padding = 4;
		textBox.setAlignMode(CENTER);
		
		sender.onUpdate(g -> onUpdate());
	}
	
	public void onUpdate() {
		if(autoAdapt) {
			int w = (int) textBox.textWidth(getStringValue());
			setWH(w + textBox.padding * 2, getHeight());
		}
	}
 	
	public void singleThread() {
		sender.singleThread();
	}
	
	public void openMenu() {
		if(enabled) {
			update();
			menu.add(0, getHeight());
			adaptSize();
			getDm().getInput().setRemovableTarget(menu, this);
			textBox.setWH(menu.getWidth(), textBox.getHeight());
		}
	}
	
	public void closeMenu() {
		if(checkFlag(DisplayFlag.onDisplay))
			getDm().getInput().removeRemovableTarget(menu);
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == CLICK) {
			openMenu();
		}
	}
	
	public void addItem(String s) {
		items.add(s);
		updateHeight();
	}

	public void removeItem(String s) {
		items.remove(s);
		updateHeight();
	}
	
	private void updateHeight() {
		menu.setWH(menu.getWidth(), itemHeight * items.size() + top + bottom);
	}
	
	public void clear() {
		items.clear();
	}
	
	public void enableAutoAdapt() {
		autoAdapt = true;
	}
	
	public void adaptSize() {
		if(items == null)
			return;
		float max = 0;
		
		for (String s : items) 
			if(s != null) {
				float w = textBox.textWidth(s); //RootClass.mainProc().textWidth(s);
				max = w > max ? w : max;
			}
		
		int w = Math.max((int) max + 10, getWidth());
		menu.setWH(w, getHeight());
		
		updateHeight();
	}
	
	
	public void setSelected(int index) {
		
		if(index != sender.getValue()) {
			
			sender.setValue(index);
			String s = getStringValue();
			textBox.setText(s);
			sender.update();
		}
		
	}
	

	public String getStringValue() {
		
		int i = sender.getValue();
		if(i < 0 || i >= items.size()) {
			return noSelectionText;
		}
		return items.get(i);
	}
	
	
	public boolean setStringValue(String s) {
		
		for (int i = 0; i < items.size(); i++) {
			if(items.get(i).equals(s)) {
				setSelected(i);//setValue(i);
				return true;
			}
		}
		return false;
	}
	
	
	@Override
	public Integer getValue() {
		return sender.getValue();
	}
	
	
	@Override
	public void setValue(Integer v) {
		setSelected(v);
	}
	
	
	public void update() {
		
		sender.update();
		
		if(itemsListUpdateAction != null) {
			
			items = Arrays.asList(itemsListUpdateAction.get());
			sender.update();
			int s = sender.getValue();
			if(s >= 0 && s < items.size()) {
				String v = items.get(s);
				int i = items.indexOf(v);
				sender.setValue(i);
			}
		}
		
	}
	

	@Override
	public void disable(boolean b) {
		
		enabled = !b;
		
		if(enabled) {
			setColor(TextInputField.COLOR_INACTIVE);
		}else {
			closeMenu();
			setColor(TextInputField.color_disabled);
		}
	}
	
	@Override
	public int setColor(int color) {
		int c = super.setColor(color);
		textBox.setColor(color);
		return c;
	}
	
	@Override
	public void setWH(int width, int height) {
	
		super.setWH(width, height);
		
		if(menu != null && !autoAdapt)
			menu.setWH(width, menu.getHeight());
		textBox.setWH(width, height);
		textBox.align();
	}
	
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		update();
	}
	
	
	private class MenuList extends DisplayObject {
		
		MenuList() {
			setFlag(DisplayFlag.RECT, true);
			setColor(Colors.grey(60));
		}
		
		@Override 
		public void render(PGraphics g) {
			g.noStroke();
			super.render(g);
			if(items != null)
				for (int i = 0; i < items.size(); i++) {
					
					mousePos = (int) ((getDm().getInput().getMouseY() - getY() - top) / itemHeight);
					
					if(checkFlag(DisplayFlag.mouseOver) && i == mousePos){
						g.fill(0x15ffffff);
						g.rect(getX(), getY() + i * itemHeight + top, getWidth(), itemHeight);
						mouseOverItem = i;
						g.fill(200);
					}else{
						g.fill(i == sender.getValue() ? 160 : 100);
						if(mouseOverItem == i)
							mouseOverItem = -1;
					}
					
					float h = getY() + i * itemHeight + 4 + top;
					g.text(items.get(i), getX() + textBox.padding, h);
				}
		}

		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			if(e == CLICK) {
				if(items != null && mousePos >= 0 && mousePos < items.size()) {
					sender.message(mousePos);
				}
				getDm().getInput().removeRemovableTarget(this);
			}
		}
		
		@Override 
		public void removedFromDisplay() {
			textBox.setWH(DropDownMenu.this.getWidth(), textBox.getHeight());
		}
	};
	
	
}
