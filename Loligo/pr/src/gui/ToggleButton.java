package gui;

import static constants.InputEvent.OVER;
import static constants.InputEvent.OVER_STOP;
import static constants.InputEvent.MouseAction.PRESSED;
import static constants.InputEvent.MouseAction.RELEASED;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import constants.Timer;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Delegate;
import module.inout.NumericType;
import module.inout.NumericType.SubType;
import patch.Domain;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import processing.core.PImage;
import util.Colors;
import util.Images;

public class ToggleButton extends DisplayObject implements DisplayGuiSender.Numeric {

	@Setter
	private int colorOn = 150, colorOff = 50;
	protected final GuiSenderObject<Double> sender;
	@Delegate
	private final MouseOverTextHandler mouseOverText = new MouseOverTextHandler(this);
	protected boolean active = true;
	private boolean alphaMaskMode;
	protected int statusColor = colorOff;
	private Mode mode;
	private PImage img, imgTrue, imgFalse;
	@Setter
	private InputEvent.MouseButton mouseButton = InputEvent.MouseButton.RMB;
	
	@Getter@Setter
	private int mouseOverButtonColor = 0xffffffff, 
				buttonColor			 = 0xffaaaaaa, 
				disabledColor		 = 0xff444444, 
				backgroundColor		 = 0x15ffffff,
				buttonPressedColor	 = 0xff9a9a9a;
	
	
	public ToggleButton(Consumer<Double> action, Supplier<Double> updateAction) {
		this(action, updateAction, NumericType.SubType.TOGGLE);
	}
	
	public ToggleButton(Consumer<Double> action, Supplier<Double> updateAction, NumericType.SubType boolType) {
	
		sender = new GuiSenderObject<Double>(this, action, updateAction, 0d);
		
		setBoolType(boolType);
		
		setColor(Colors.grey(40));
		setWH(30, 30);
		setFlag(DisplayFlag.RECT, true);
		setFlag(DisplayFlag.CLICKABLE, true);
	}

	public void singleThread() {
		sender.singleThread();;
	}
 	
	
	@Override
	public NumericType.SubType getNumericType() {
		return mode.subType;
	}


	@Override
	public void setNumericType(SubType st) {
		setBoolType(st);
	}
	
	public final void setBoolType(NumericType.SubType boolType) {
		mode = createMode(boolType);
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		super.mouseEventThis(e, input);
		if(active) {
			if(e == mouseButton.forAction(PRESSED)) {
				onPress(true);
			}else 
			if(e == mouseButton.forAction(RELEASED)) {
				onPress(false);
			}else
			if(e == OVER) {
				//img = imgOver != null ? imgOver : img;
			}else
			if(e == OVER_STOP) {
				setColor(buttonColor);
				//img = imgIdle;
			}
		}
	}
	
	
	protected void onPress(boolean pressed) {
		mode.onPress(pressed);
	}
	
	
	@Override
	public void render(PGraphics g) {
		
		if(img != null) {
			
			g.tint(statusColor);
			g.image(img, getX(), getY());
			g.tint(Colors.WHITE);
			g.noTint();
			
		}else {
		
			float x = getX(), y = getY(), w = getWidth(), h = getHeight();
			g.fill(getColor());
			g.rect(x, y, w, h);
			g.stroke(60);
			g.fill(statusColor);
			g.rect(x + 5, y + 5, w - 10, h - 10);
			g.noStroke();
		}
	}
	
	public boolean toBoolean() {
		return getValue() >= .5;
	}
	
	public Double getValue() {
		return sender.getValue();
	}

	
	public void setValue(Double v) {
		sender.setValue(v);
		
		if(toBoolean()) {
			statusColor = colorOn;
			img = imgTrue;
		}else {
			statusColor = colorOff;
			img = imgFalse;
		}
	}


	public void update() {
		sender.update();
	}


	public void disable(boolean b) {
		active = !b;
	}
	
	public void setImage(PImage img) {
		this.img = imgTrue = imgFalse = img;
		setWH(img.width, img.height);
		setAlphaMaskColor(getColor());
	}
	
	
	public void setImage(PImage imgTrue, PImage imgFalse) {
		this.imgTrue = imgTrue;
		this.imgFalse = imgFalse;
		this.img = imgTrue;
		
		setWH(img.width, img.height);
		
		mouseOverButtonColor = buttonColor = Colors.WHITE;
	}
	
	private void setAlphaMaskColor(int c) {
		if(img != null && alphaMaskMode)
			Images.fillAlphaImage(img, c);
	}
	
	/**
	 * Use button-image as alpha mask and fill with mouseOver-color
	 */
	public void alphaMaskMode() {
		alphaMaskMode = true;
		updateColor();
	}
	
	public void updateColor() {
		
		int c = !active 						 ? disabledColor :
				checkFlag(DisplayFlag.mouseOver) ? mouseOverButtonColor :
												   buttonColor;
		setColor(c);
	}
	
	
	private final Mode createMode(NumericType.SubType boolType) {
		Mode m;
		switch (boolType) {
		case TOGGLE:
			m = new Toggle();
			break;
		case BANG:
			m = new Bang();
			break;
		case PUSH:
			m = new Push();
			break;
		default:
			m = new Toggle();
		}
		m.subType = boolType;
		return m;
	}
	
	
	@Override
	public void setRange(double min, double max) {}

	@Override
	public double maxValue() {
		return 1;
	}

	@Override
	public double minValue() {
		return 0;
	}
	
	@Override
	public void active(boolean b) {}
	

	private static abstract class Mode {
		NumericType.SubType subType;
		abstract void onPress(boolean pressed);
		//abstract void render(PGraphics g);
	}
	
	
	private class Toggle extends Mode {
		@Override
		public void onPress(boolean pressed) {
			if(pressed) {
				double r = toBoolean() ? 0d : 1d;
				sender.message(r, true);
			}
		}
		
	}
	
	private class Bang extends Mode {
		@Override
		public void onPress(boolean pressed) {
			if(pressed) {
				sender.message(1d, false);
				Timer.createFrameDelay(
					Domain.getBaseDomain(getDm().getDomain()), 
					t -> sender.message(0d, false)
				).start();
			}
		}
		
	}
	
	private class Push extends Toggle {
		@Override
		public void onPress(boolean pressed) {
			double r = pressed ? 1d : 0d;
			sender.message(r, false);
		}
	}
	
	/*
	private class Image implements Renderer {

		private PImage img_;
		
		@Override
		public void render(PGraphics g) {
			
		}
		
	}
	*/

}
