package gui;

import static com.jogamp.newt.event.KeyEvent.NULL_CHAR;
import static com.jogamp.newt.event.KeyEvent.VK_A;
import static com.jogamp.newt.event.KeyEvent.VK_ALT;
import static com.jogamp.newt.event.KeyEvent.VK_BACK_SPACE;
import static com.jogamp.newt.event.KeyEvent.VK_C;
import static com.jogamp.newt.event.KeyEvent.VK_D;
import static com.jogamp.newt.event.KeyEvent.VK_DELETE;
import static com.jogamp.newt.event.KeyEvent.VK_F1;
import static com.jogamp.newt.event.KeyEvent.VK_F10;
import static com.jogamp.newt.event.KeyEvent.VK_F11;
import static com.jogamp.newt.event.KeyEvent.VK_F2;
import static com.jogamp.newt.event.KeyEvent.VK_F3;
import static com.jogamp.newt.event.KeyEvent.VK_F4;
import static com.jogamp.newt.event.KeyEvent.VK_G;
import static com.jogamp.newt.event.KeyEvent.VK_I;
import static com.jogamp.newt.event.KeyEvent.VK_L;
import static com.jogamp.newt.event.KeyEvent.VK_N;
import static com.jogamp.newt.event.KeyEvent.VK_O;
import static com.jogamp.newt.event.KeyEvent.VK_R;
import static com.jogamp.newt.event.KeyEvent.VK_S;
import static com.jogamp.newt.event.KeyEvent.VK_V;
import static com.jogamp.newt.event.KeyEvent.VK_W;
import static com.jogamp.newt.event.KeyEvent.VK_X;
import static com.jogamp.newt.event.KeyEvent.VK_Y;
import static com.jogamp.newt.event.KeyEvent.VK_Z;
import static gui.Shortcuts.ALT;
import static gui.Shortcuts.CONTEXT;
import static gui.Shortcuts.CTRL;
import static gui.Shortcuts.SHIFT;

import java.util.function.Consumer;

import gui.Shortcuts.Shortcut;
import gui.nodeinspector.NodeInspector;
import gui.selection.SelectionManager;
import lombok.Getter;
import module.Console;
import module.ModuleManager;
import module.modules.anim.Dupli;
import module.modules.anim.Stamp;
import patch.LoligoPatch;
import pr.DisplayManagerIF;
import pr.RootClass;
import pr.userinput.UserInput;
import save.SaveJson;
import util.HelpPatch;

public enum UserAction {

	SELECT_INVERT	("Invert Selection"	, VK_I, CTRL			, p -> selection(p).invert()						),
	SELECT_LINKED	("Select Linked"	, VK_L, CTRL			, p -> selection(p).selectLinked()					),
	TOGGLE_GRID		("Toggle Grid"		, VK_G, CTRL | ALT		, p -> input(p).toggleGrid()						),
	PATCH_NEW		("New"				, VK_N, CTRL			, p -> RootClass.backstage().createPatch()			),
	PATCH_AUTOSAVE	("Save..."			, VK_S, CTRL			, SaveJson::autoSavePatch							),
	PATCH_SAVEAS	("Save As"			, VK_S, CTRL | SHIFT	, SaveJson::runPatchSavePrompt						),
	PATCH_OPEN		("Open..."			, VK_O, CTRL			, SaveJson::runPatchLoadPrompt						),
	PATCH_CLOSE		("Close"			, VK_W, CTRL			, RootClass.backstage()::closePatch					),
	//SHOW_FINDER		("Finder"			, VK_F, CTRL			, Finder::show										),
	
	SHOW_SPLASH		("About"			, NULL_CHAR				, Splash::show										),
	SHOW_SETTINGS	("Settings"			, NULL_CHAR				, SettingsInspector::create							),
	
	HELPPATCH		("Node Help"		, VK_F1					, HelpPatch::openHelpPatch							),
	
	//NEXT_TAB		("Next Tab"			, VK_TAB				, p -> RootClass.backstage().nextPatch()			),
	//NEW_TAB			(VK_T, CTRL | ALT	, p -> RootClass.backstage().createPatch()		),
	
	REDO			("Redo"				, VK_Z, CTRL			, p -> p.getHistory().undo()						),
	UNDO			("Undo"				, VK_Y, CTRL			, p -> p.getHistory().redo()						),
	SELECT_ALL		("Select All"		, VK_A, CTRL			, p -> selection(p).selectAll()						),
	
	LINKEDCOPY		("Make Dupli"		, VK_D, CTRL | SHIFT, CONTEXT, Dupli::create								),
	STAMP			("Make Stamp"		, VK_D, CTRL | SHIFT | ALT, CONTEXT, Stamp::create							),
	COPY			("Copy"				, VK_C, CTRL, CONTEXT	, CopyPaste::copy									),
	PASTE			("Paste"			, VK_V, CTRL, CONTEXT	, CopyPaste::paste									),
	CUT				("Cut"				, VK_X, CTRL, CONTEXT	, CopyPaste::cut									),
	DUPLICATE		("Duplicate"		, VK_D, CTRL, CONTEXT	, CopyPaste::duplicateSelection								),
	GROUP			("Group"			, VK_G, CTRL, CONTEXT	, SubPatchGroup::group								),
	
	CLOSE_WINDOW	("Close Tab"		, VK_W, CTRL			, UserAction::kill									),
	CLOSE_PROGRAM	("Exit"				, VK_F4, ALT			, p -> p.getPApplet().tryExit()						),
	
	SHOW_CONSOLE	("Console"			, VK_F3					, p -> Console.showConsole()						),
	INSPECT			("Inspect"			, VK_F2					, NodeInspector::openForPatch						),
	PAUSE			("Pause"			, VK_F10				, DisplayManagerIF::togglePause						),	
	TOGGLE_GUI		("Toggle GUI"		, VK_F11				, DisplayManagerIF::toggleGui						),
	
	DELETE			("Delete"			, VK_BACK_SPACE, 0, CONTEXT, ModuleManager::deleteSelected					),
	DELETE2			("Delete"			, VK_DELETE, 0, CONTEXT, ModuleManager::deleteSelected						),
	RESET			("Reset"			, VK_R, CTRL, CONTEXT, ModuleManager::resetSelected							), 
	TOGGLE_BAR		("Toggle Bar"		, VK_ALT, ALT, CONTEXT, p -> dm(p).showMenuBar()							); 
	
	
	public static void kill(LoligoPatch p) {
		RootClass.mainPatch().getHistory().execute(p::killWindow);
	}
	
	public static DisplayManagerIF 	dm(LoligoPatch p)		 {return p.getDisplayManager();}
	public static UserInput 		input(LoligoPatch p) 	 {return p.getDisplayManager().getInput();}
	public static SelectionManager 	selection(LoligoPatch p) {return p.getDisplayManager().getInput().selection;}
	
	
	@Getter
	private final Shortcuts.Shortcut shortcut;
	@Getter
	private final String description;
	
	UserAction(String description, int keyCode, Consumer<LoligoPatch> action) {
		this(description, keyCode, 0, action);
	}
	
	UserAction(String description, int keyCode, int mods, Consumer<LoligoPatch> action) {
		this(description, keyCode, mods, 0, action);
	}
	
	UserAction(String description, int keyCode, int mods, int flags, Consumer<LoligoPatch> action) {
		this.description = description;
		shortcut = new Shortcut(action, keyCode, mods, flags);
	}
}
