package gui.text;

import static gui.text.TextBlock.AlignMode.CENTER;
import static gui.text.TextBlock.AlignMode.LEFT;
import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;

import java.awt.AWTException;
import java.awt.Point;
import java.awt.Robot;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayGuiSender;
import gui.GuiSenderObject;
import gui.Position;
import gui.Range;
import gui.selection.GuiType;
import lombok.Getter;
import lombok.Setter;
import module.inout.NumericType;
import patch.Loligo;
import pr.DisplayContainerIF;
import pr.DisplayObjectIF;
import pr.RootClass;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PGraphics;


public class NumberInputField implements DisplayGuiSender.Numeric {

	{
		try {
			robot = new Robot();
		} catch (AWTException e1) {
			e1.printStackTrace();
		}
	}
	
	private static Robot robot;
	
	@Getter
	private final TextInputField textField;
	private GuiSenderObject<Double> sender;
	private Range range;
	
	@Getter@Setter
	private int precision = 3;
	private Point posStart;
	protected double scroll;
	@Setter@Getter
	private double scrollSpeed = 1;
	
	
	public NumberInputField(Consumer<Double> action, Supplier<Double> updateAction) {
		
		textField = new TextInputField(
						this::setStringValue, 
						this::getStringValue
					);
		//textField.singleThread();
		textField.setSelf(this);
		textField.setOffsetY(1);
		textField.autoFit = true;
		textField.setAlignMode(CENTER, LEFT);
		
		range = new Range(action, updateAction);
		sender = new GuiSenderObject<>(this, range, range, 0d);
	}
	
	
	private void setStringValue(String t) {
		double d = parseStringValue(t);
		sender.message(d);
	}
	
	public double parseStringValue(String t) {
		
		try{
			double d = 0;
			d = Double.valueOf(t);
			return d;
		}catch(NumberFormatException e) {}
		return 0;
	}
	
	
	protected String getStringValue() {
		
		String s = String.format("%."+ precision + "f", getValue());
		return s;
	}
	
	
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		textField.mouseEventThis(e, input);
		
		if(!textField.disabled && !textField.active) {
			
			//Main PApplet cursor() works best, even on secondary Papplets
			Loligo pr = RootClass.mainProc();	//input.getDomain().getPApplet(); 
			
			switch(e) {
			case PRESSED_RIGHT -> {
				scroll = getValue();
				pr.noCursor();
				posStart = java.awt.MouseInfo.getPointerInfo().getLocation();
			}
			case RELEASED_RIGHT, FOCUS_LOST, DRAG_STOP_RIGHT -> {
				pr.cursor(PConstants.ARROW);
			}
			case DRAG_START_RIGHT -> {
				pr.noCursor();
			}
			case DRAG_RIGHT -> {
				var p = java.awt.MouseInfo.getPointerInfo().getLocation();
				int dy = (int) (p.getY() - posStart.y);
				
				robot.mouseMove(posStart.x, posStart.y);
				
				int scale = 1 + 
						(textField.getCharProfile() == CharProfile.INT ? 0 : 1) + 
						(input.shift ? 1 : 0) + 
						(input.ctrl  ? 1 : 0);
				
				double r = scrollFunction(dy, scale);
				
				sender.message(r);
				textField.update();
			}
			default -> {}
			}
		}
	}
	
	public double scrollFunction(int dy, int scale) {
		
		double d = Math.pow(10, scale);
		
		double ss = scrollSpeed * dy / d;
		scroll -= ss;
		scroll = range != null ? range.applyRange(scroll) : scroll;
		
		int round = textField.getCharProfile() == CharProfile.INT ? 0 : scale;
		scroll = scroll == POSITIVE_INFINITY ?  9999 :
			     scroll == NEGATIVE_INFINITY ? -9999 : scroll;
		
		return new BigDecimal(scroll).setScale(round, RoundingMode.HALF_UP).doubleValue();
	}
 	
	public NumberInputField setCharProfile(CharProfile charProfile) {
		textField.setCharProfile(charProfile);
		return this;
	}
	
	@Override
	public void setRange(double min, double max) {
		range.setRange(min, max);
		//return this;
	}
	
	@Override
	public double minValue() {
		return range.getMin();
	}
	
	@Override
	public double maxValue() {
		 return range.getMax();
	}
	
	
	@Override
	public Double getValue() {
		
		return sender.getValue();
	}

	
	@Override
	public void setValue(Double v) {
		
		if(sender.getValue() != v) {
			sender.setValue(v);
			textField.update();
		}
	}
	

	@Override
	public void update() {
		textField.update();
		sender.update();
	}

	
	@Override
	public void disable(boolean b) {
		
		textField.disable(b);
	}

	@Override
	public void active(boolean a) {
		textField.active(a);
	}
	
	//---------DisplayObject delegation methods---------------
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		textField.mouseEvent(e, input);
	}
	
	@Override
	public DisplayContainerIF getDm() {
		return textField.getDm();
	}


	@Override
	public void addedToDisplay() {
		update();
		textField.addedToDisplay();
	}

	@Override
	public void removedFromDisplay() {
		textField.removedFromDisplay();
	}

	@Override
	public boolean hitTest(float mx, float my) {
		return textField.hitTest(mx, my);
	}

	@Override
	public void render(PGraphics g) {
		textField.render(g);
		
		if(range.isLimited()) {
			g.fill(0x15000000);
			float w = getWidth() * (float) (getValue() / (range.getMax() - range.getMin()));
			g.rect(getX(), getY(), w, getHeight());
		}
	}

	@Override
	public void addChild(DisplayObjectIF d) {
		textField.addChild(d);
	}

	@Override
	public void addChild(DisplayObjectIF... dArr) {
		textField.addChild(dArr);
	}

	@Override
	public void removeChild(DisplayObjectIF d) {
		textField.removeChild(d);
	}

	@Override
	public void removeChild(DisplayObjectIF[] dArr) {
		textField.removeChild(dArr);
	}

	@Override
	public void setSelectionObject(Consumer<PGraphics> cons) {
		textField.setSelectionObject(cons);
	}

	@Override
	public void setSelected(boolean b) {
		textField.setSelected(b);
	}

	@Override
	public void fixedToParent(boolean b) {
		textField.fixedToParent(b);
	}

	@Override
	public float getX() {
		return textField.getX();
	}

	@Override
	public float getY() {
		return textField.getY();
	}

	@Override
	public void setPos(float x, float y) {
		textField.setPos(x, y);
	}

	@Override
	public Position getPosObject() {
		return textField.getPosObject();
	}

	@Override
	public void setPosObject(Position p) {
		textField.setPosObject(p);
	}

	@Override
	public DisplayObjectIF getParent() {
		
		return textField.getParent();
	}

	@Override
	public void setParent(DisplayObjectIF parent) {
		textField.setParent(parent);
	}

	@Override
	public List<DisplayObjectIF> getChildren() {
		return textField.getChildren();
	}


	@Override
	public void setDm(DisplayContainerIF dm) {
		textField.setDm(dm);
	}

	@Override
	public boolean checkFlag(DisplayFlag flag) {
		return textField.checkFlag(flag);
	}

	@Override
	public void setFlag(DisplayFlag f, boolean b) {
		textField.setFlag(f, b);
	}

	@Override
	public int getColor() {
		return textField.getColor();
	}

	@Override
	public int setColor(int color) {
		return textField.setColor(color);
	}

	@Override
	public int getStrokeColor() {
		return textField.getStrokeColor();
	}

	@Override
	public void setStrokeColor(int strokeColor) {
		textField.setStrokeColor(strokeColor);
	}

	@Override
	public int getWidth() {
		return textField.getWidth();
	}

	@Override
	public void setWH(int width, int height) {
		textField.setWH(width, height);
	}

	@Override
	public int getHeight() {
		return textField.getHeight();
	}

	@Override
	public GuiType getGuiType() {
		
		return textField.getGuiType();
	}

	@Override
	public void setGuiType(GuiType guiType) {
		textField.setGuiType(guiType);
		
	}

	@Override
	public void setGlobal(float x, float y) {
		textField.setGlobal(x, y);
	}

	@Override
	public void mouseOverRest(boolean on) {
		textField.mouseOverRest(on);
	}


	@Override
	public NumericType.SubType getNumericType() {
		return textField.getCharProfile() == CharProfile.INT ? 
				NumericType.SubType.INTEGER : 
				NumericType.SubType.DECIMAL;
	}


	@Override
	public void setNumericType(NumericType.SubType st) {
		var p = st == NumericType.SubType.INTEGER ? 
					CharProfile.INT : 
					CharProfile.DEZIMAL;
		textField.setCharProfile(p);
	}
	
}
