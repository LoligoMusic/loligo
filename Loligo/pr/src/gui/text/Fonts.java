package gui.text;

import java.io.IOException;
import java.io.InputStream;

import processing.core.PFont;

public class Fonts {

	public static final float LEADING_FACTOR = 1.275f;
	
	static final public PFont DEFAULT_10 = loadFont("LucidaSans-10.vlw"),
							  DEFAULT_12 = loadFont("LucidaSans-12.vlw"),
							  DEFAULT_14 = loadFont("LucidaSans-14.vlw"),
							  DEFAULT_DEMI_12 = loadFont("LucidaSans-Demi-12.vlw"),
							  DEFAULT_DEMI_14 = loadFont("LucidaSans-Demi-14.vlw");
	
	public final static PFont[] FONTS_REGULAR = {
		DEFAULT_10,
		DEFAULT_12,
		DEFAULT_14,
		loadFont("LucidaSans-16.vlw"),
		loadFont("LucidaSans-18.vlw"),
		loadFont("LucidaSans-20.vlw"),
		loadFont("LucidaSans-22.vlw"),
		loadFont("LucidaSans-24.vlw"),
	};
	
	public final static PFont[] FONTS_DEMI = {
		DEFAULT_DEMI_12,
		DEFAULT_DEMI_14
	};
	
	
	public static PFont loadFont(String s) {
		try {
			InputStream in = TextBox.class.getResourceAsStream("/resources/" + s);
			return new PFont(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static PFont getFont(int size, PFont[] fonts) {
		for (PFont f : fonts) {
			if(f.getSize() >= size) {
				return f;
			}
		}
		return fonts[0];
	}
	
	
	public static float lineHeight(PFont font, float textSize) {
		return (font.ascent() + font.descent()) * textSize * LEADING_FACTOR; 
	}
	
	
	public static float leading(PFont font, float textSize) {
		return (font.ascent() + font.descent()) * textSize * (LEADING_FACTOR - 1); 
	}
	
	
	public static float textWidth(PFont font, float textSize, char[] chars, int start, int length) {
		float e = 0;
		int end = start + length;
		for (int i = start; i < end; i++) 
			e += font.width(chars[i]);
		return e * textSize;
	}
	
	
	public static float textWidth(PFont font, float textSize, String s) {
		float e = 0;
		for (int i = 0; i < s.length(); i++) 
			e += font.width(s.charAt(i));
		return e * textSize; 
	}
	
	
	public static float textWidth(PFont font, float textSize, char c) {
		 return font.width(c) * textSize;
	}
	
	
	
	
}
