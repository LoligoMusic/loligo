package gui.text;

import static constants.DisplayFlag.CLICKABLE;

import constants.DisplayFlag;
import gui.text.TextBlock.AlignMode;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayObject;
import pr.RootClass;
import processing.core.PConstants;
import processing.core.PFont;
import processing.core.PGraphics;
import util.Colors;
import util.Utils;

public class TextBox extends DisplayObject implements TextObject {

	@Setter
	public boolean autoFit = false;
	@Setter@Getter
	private int textColor = Colors.grey(200);
	public int textSize = 12;
	private int centerY;
	@Getter@Setter
	private int offsetX = 0, offsetY = 0;
	@Getter
	public int offX = 0;
	protected int textHeight, textWidth;
	public boolean bgBox = false;
	private int maxWidth = 150;
	public int padding = 0;
	protected String text, displayText;
	@Getter
	private boolean overflow;
	protected boolean enableAlignOverflow = true;
	private AlignMode align = AlignMode.LEFT, alignOverflow = AlignMode.LEFT;
	protected int displayStart, displayEnd, displayStartOffset = Integer.MAX_VALUE;
	@Getter
	private PFont font = Fonts.DEFAULT_12;
	
	
	public TextBox() {
		this("", false);
	}
	
	public TextBox(final String text) {
		this(text, false);
	}
	
	public TextBox(final String text, boolean autoFit) {
		
		setFlag(CLICKABLE, false);
		this.text = text != null ? text : "-";
		displayText = this.text;
		displayStart = 0;
		displayEnd = this.text.length();
		this.autoFit = autoFit;
		setFlag(DisplayFlag.RECT, true);
		
		setFont(font);
		
		setWH(autoFit ? textWidth + 16 : getWidth(), textHeight + 10);
		
		setText(text);
	}
	
	@Override
	public void render(PGraphics g) {
		
		if(bgBox)
			super.render(g);
		
		g.fill(textColor);
		//g.textSize(textSize);
		g.textFont(font);
		
		g.textAlign(PConstants.LEFT, PConstants.TOP);
		
		float x = getX(), y = getY(), w = getWidth(), h = getHeight();
		
		if(autoFit) {
			//g.clip(x, y, w, h);
			g.text(displayText, x + offX /*padding*/, y + centerY + offsetY, w - padding * 2, h);
			//g.noClip();
		}else
			g.text(displayText, x + offX, y + centerY + offsetY);
		
		/*
		if(true) {
			float y2 = y + h;
			int wo = 20;
			float xi = x + w - wo - padding;
			int c = getColor();
			g.strokeWeight(1);
			for (int i = 1; i < wo; i++) {
				float xp = xi + i;
				g.stroke(Colors.setAlpha(c, (float)i / wo));
				g.line(xp, y, xp, y2);
			}
			g.noStroke();
		}
		*///2 vor 12 min,,,,,,   10Uhr
		
		
	}
	
	
	public float textWidth(int end) {
		PGraphics g = getDm().getGraphics();
		g.textSize(textSize);
		float w = textWidth(text.substring(0, end));
		return offX + w;
	}
	
	
	public float textWidth(String s) {
		return Utils.textWidth(s, textSize, font);
	}
	
	
	public void setText(String t) {
		
		text = t == null ? "null" : t;
	
		align();
		
	}
	
	public final String getText() {
		
		return text;
	}
	
	final public void setFont(PFont font) {
		
		this.font = font;
		
		textHeight = (int) (font.ascent() * textSize);//(int) (font.ascent() + font.descent()) * textSize;
		
		int tw = (int) RootClass.mainProc().textWidth(this.text); //Utils.textWidth(text, textSize, font);//
		textWidth = tw > maxWidth ? maxWidth : tw;
	}
	
	@Override
	final public void setAlignMode(AlignMode mode) {
		setAlignMode(mode, AlignMode.LEFT);
	}
	
	final public void setAlignMode(AlignMode mode, AlignMode modeOverflow) {
		
		align = mode;
		alignOverflow = modeOverflow;
		align();
	}
	
	final public void align() {
		
		int w = (int) Utils.textWidth(text, textSize, font);
		int maxWidth = getWidth() - padding * 2;
		
		if(autoFit && w > maxWidth - padding * 2) {
			
			overflow = true;
			String sur = "..";
			int d = (int) Utils.textWidth(sur, textSize, font);
			int ds = 0, de = text.length();
			
			for (int i = 0; i < text.length(); i++) {
				
				displayText = text.substring(ds, de);
				float f = Utils.textWidth(displayText, textSize, font) + d;
				
				if(f < maxWidth) {
					displayText += sur;
					w = (int)f;
					break;
				}
				
				boolean b = ds < displayStartOffset;
				if(enableAlignOverflow) 
					b &= alignOverflow != AlignMode.LEFT;
				
				if(b) {
					ds++;
				}else {
					de--;
				}
			}
			displayStart = ds;
			displayEnd = de;
			
		}else {
			overflow = false;
			displayText = text;
			displayStart = 0;
			displayEnd = text.length();
		}
		
		offX = switch (align) {
		case LEFT  	-> padding;
		case RIGHT 	-> getWidth() - w - padding;
		case CENTER -> (getWidth() - w) / 2;
		};
		
		offX += offsetX;
		centerY = (getHeight() - textHeight) / 2 ;
	}
	
	public final void setPadding(int padding) {
		this.padding = padding;
	}

}
