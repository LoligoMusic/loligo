package gui.text;

import static com.jogamp.newt.event.KeyEvent.VK_A;
import static com.jogamp.newt.event.KeyEvent.VK_BACK_SPACE;
import static com.jogamp.newt.event.KeyEvent.VK_C;
import static com.jogamp.newt.event.KeyEvent.VK_CONTROL;
import static com.jogamp.newt.event.KeyEvent.VK_DELETE;
import static com.jogamp.newt.event.KeyEvent.VK_END;
import static com.jogamp.newt.event.KeyEvent.VK_ENTER;
import static com.jogamp.newt.event.KeyEvent.VK_HOME;
import static com.jogamp.newt.event.KeyEvent.VK_V;
import static com.jogamp.newt.event.KeyEvent.VK_X;
import static constants.DisplayFlag.CLICKABLE;
import static constants.DisplayFlag.DRAGABLE;
import static constants.DisplayFlag.fixedToParent;
import static java.lang.Character.isLetterOrDigit;
import static java.lang.Character.isWhitespace;
import static java.lang.Math.ceil;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static processing.core.PConstants.ENTER;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayGuiSender;
import gui.DisplayObjects;
import gui.GuiSenderObject;
import gui.MouseOverTextHandler;
import gui.Position;
import history.Action;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Delegate;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.Positionable;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PGraphics;
import util.Colors;
import util.Utils;


public class MultiLineTextInputField extends TextBlock implements DisplayGuiSender<String>{
	
	//public static final int FILENAME = 0, PLAINTEXT = 1, INT = 2, DEZIMAL = 3;
	public static final int TEXT_COLOR_ACTIVE = 0xff222222, TEXT_COLOR_INACTIVE = Colors.grey(50);
	
	@Setter
	private DisplayGuiSender<?> self; // self reference for NumberInputField delegation
	@Delegate
	private final MouseOverTextHandler mouseOverText = new MouseOverTextHandler(this);
	
	public boolean active = false;
	
	protected boolean disabled = false;
	//private boolean liveupdate;
	@Setter
	private boolean undoable;
	@Getter@Setter
	private int /*charProfile,*/ charLimit = Integer.MAX_VALUE;
	private final GuiSenderObject<String> sender;
	private final TCursor textCursor;
	private final List<DisplayObject> selRect = new ArrayList<>();
	private int cursor = 0, sel, sel1, sel2;
	private boolean isDragable;
	private final StringBuffer sb;
	
	@Getter@Setter private int color_active 		= COLOR_ACTIVE;
	@Getter        private int color_inactive 		= COLOR_INACTIVE;
	@Getter@Setter private int text_color_active 	= TEXT_COLOR_ACTIVE;
	@Getter 	   private int text_color_inactive 	= TEXT_COLOR_INACTIVE;
	
	
	public MultiLineTextInputField(Consumer<String> action, Supplier<String> updateAction) {
		
		this(action, updateAction, CharProfile.PLAINTEXT);
	}
	
	
	public MultiLineTextInputField(Consumer<String> action, Supplier<String> updateAction, CharProfile charProfile) {
		
		self = this;
		
		sb = new StringBuffer();
		
		this.charProfile = charProfile;
		
		Supplier<String> ua = null;
		if(updateAction != null) {
			ua = () -> {
				String s = updateAction.get();
				setCursor(cursor);
				return s;
			};
		}
		
		sender = new GuiSenderObject<String>(this, action, ua, "");
		//bgBox = true;
		
		//this.target = target;
		setWH(50, 20);
		
		setFlag(CLICKABLE, true);
		//setFlag(DisplayFlag.Subscriber, true);
		
		textCursor = new TCursor();
		
		//active(false);
		setColor(Colors.grey(color_inactive));
		setTextColor(text_color_inactive);
		
		//setAlignMode(CENTER);
		//setAutoFit(true);
		
		
	}
	
	public void singleThread() {
		sender.singleThread();
	}
	
	@Override
	public void addedToDisplay() {
		DisplayObjects.addSubscriber(self);
		self.update();
	}
	
	@Override
	public void removedFromDisplay() {
		DisplayObjects.removeSubscriber(self);
	}
	
	
	@Override
	public void setValue(String text) {
		
		if(!active) {
			//delete(0, sb.length(), false);
			sb.setLength(0);
			resetCursor(0);
			if(text != null)
				insert(text);
		}
		
		sender.setValue(text);
		
	}
	
	@Override
	public void update() {
		sender.update();
	}
	
	@Override
	public String getValue() {
		return sender.getValue();
	}
	
	public void setColorInactive(int c) {
		color_inactive = c;
		setColor(c);
	}
	
	public void setTextColorInactive(int c) {
		text_color_inactive = c;
		setTextColor(c);
	}
	
	
	public void active(boolean a) {
		
		if(disabled || a == active)
			return;
		
		active = a;
		//enableAlignOverflow = !active;
		resetCursor(sb.length());
		
		var proc = self.getDm().getGuiDomain().getPApplet();
		var input = self.getDm().getInput();
		
		if(active) {
			
			if(input.getOverTarget() == self)
				proc.cursor(PConstants.TEXT);
			
			self.setColor(color_active);
			setTextColor(text_color_active);
			
			self.addChild(textCursor);
			
			selectAll();
			
			isDragable = self.checkFlag(DRAGABLE);
			self.setFlag(DRAGABLE, false);
			input.setFocusTarget(self);
			input.setSpecialFocusTarget(self);
			input.addInputEventListener(self);
			
		} else{
			
			proc.cursor(PConstants.ARROW);
			self.setColor(color_inactive);
			setTextColor(text_color_inactive);
			
			self.removeChild(textCursor);
			//DisplayObjects.removeFromDisplay(selRect);
			
			sendMessage();
			
			self.setFlag(DRAGABLE, isDragable);
			
			input.selection.removeActiveElement(self);
			input.removeSpecialFocusTarget(self);
			input.removeInputEventListener(this);
		}
	}

	
	private void sendMessage() {
		sendMessage(sb.toString());
	}

	
	protected void sendMessage(String s) {
		
		String v = sender.getValue();
		
		if(v == null || !v.equals(s)) {
		
			sender.message(s, undoable);
			sender.update();
		}
	}
	
	
	@Override
	public void disable(boolean b) {
		
		if(b) {
			
			active(false);
			disabled = true;
			
			self.setColor(color_disabled);
			setTextColor(Colors.grey(40));
			
		} else{
			
			disabled = false;
			self.setColor(color_inactive);
			active(false);
		}
		//setFlag(DisplayFlag.clickable, !b);
		
	}
	
	
	private void resetCursor(int c) {
	
		setCursor(c);
		deselect();
	}
	
	
	private void setCursor(int c) {
		
		cursor = c;
		textCursor.resetBlink();
		getTextPos(c, textCursor);
	}
	
	
	private void setSelection(int c1, int c2) {
		
		if(c1 == c2) 
			deselect();
		else{
			sel = c1;
			setSelection(c2);
		}
	}
	
	
	private void setSelection(int c) {
		
		setCursor(c);
		clearSelRect();
		
		if(cursor == sel) 
			return;
		
		if(cursor > sel) {
			sel1 = sel;
			sel2 = cursor;
		}else{
			sel1 = cursor;
			sel2 = sel;
		}
		
		int l1 = getCurrentLine(sel1);
		int l2 = getCurrentLine(sel2);
		var ps = new Position();
		float x1 = getTextPos(sel1, ps).getX();
		float x2 = getTextPos(sel2, ps).getX();
		
		float p = getPaddingLeft();
		
		for (int i = l1; i <= l2; i++) {
			var li = lines.get(i);
			float o = p + li.offsetX;
			float c1 = i == l1 ? x1 : o;
			float c2 = i == l2 ? x2 : o + Math.max(4, li.width);
			float y = linePosY(i);
			addSelRect(c1, c2, y);
		}
	}
	
	
	public void selectAll() {
		setSelection(0, sb.length());
	}
	
	private void deselect() {
		
		clearSelRect();
		sel = sel1 = sel2 = cursor;
	}
	
	
	private int getCurrentLine(int cursor) {
		
		int r = 0;
		int len = 0;
		int spaces = 0;
		
		for (int i = 0; i < lines.size(); i++) {
			int le = len + lines.get(i).text.length();
			r = i;
			spaces += i == 0 ? 0 : lines.get(i - 1).spaces;
			if(le >= cursor - spaces)
				break;
			len = le;
		}
		return r;
	}
	
	
	private float linePosY(int lineIdx) {
		return getPaddingY() + lineHeight() * lineIdx - leading() / 2;
	}
	
	
	private Positionable getTextPos(int cursor, Positionable p) {
		
		int c = 0;
		int r = 0;
		int len = 0;
		int space = 0;
		
		for (int i = 0; i < lines.size(); i++) {
			int le = len + lines.get(i).text.length();
			r = i;
			space += i == 0 ? 0 : lines.get(i - 1).spaces;
			int u = cursor - space;
			if(le >= u) { 
				c = u - len;
				break;
			}
			len = le;
		}
		
		float x = getPaddingLeft();
		if(r < lines.size()) {
			var li = lines.get(r);
			String s = li.text.substring(0, c);
			x += Utils.textWidth(s, getTextSize(), getFont()) + li.offsetX;
		}
		float y = linePosY(r);
		
		p.getPosObject().x = x;
		p.getPosObject().y = y;
		return p;
	}
	
	
	private int getCharIndexFromPixelPos(float x, float y) {
		
		if(lines.isEmpty())
			return 0;
		
		float p = y - getPaddingY();
		int lx = (int)(p / lineHeight());
		lx = max(lx, 0);
		lx = min(lx, lines.size( )- 1);
		
		return charIndexFromPixelPosLine(lx, x);
	}
	
	private int charIndexFromPixelPosLine(int lx, float x) {
		Line line = lines.get(lx);
		char[] cc = line.text.toCharArray();
		float min = Float.MAX_VALUE;
		int pos = -1;
		
		for (int i = 0; i < cc.length + 1; i++) {
			
			float w = i == 0 ? 0 : Utils.textWidth(cc, 0, i, getTextSize(), getFont());
			float d = Math.abs(w - x + getPaddingLeft() + line.offsetX);
			if(d < min) {
				min = d;
				pos = i;
			}
		}
		
		int len = 0;
		int spaces = 0;
		
		for (int i = 0; i < lx; i++) {
			Line li = lines.get(i);
			len += li.text.length();
			spaces += li.spaces;
		}
		
		return spaces + len + pos;
	}
	
	
	private void delete(int s1, int s2, boolean send) {
		
		s1 = s1 < 0 		  ? 0 :
			 s1 > sb.length() ? sb.length() :
				 				s1;
		s2 = s2 < 0 		  ? 0 :
			 s2 > sb.length() ? sb.length() :
				 				s2;
		
		if(s1 < s2) {
		
			sb.delete(s1, s2);
			setText(sb.toString());
			resetCursor(s1);
			
			if(checkTextFlag(TextFlag.LIVEUPDATE) && send)
				sendMessage();
		}
	}
	
	
	private void clearSelection() {
		
		delete(sel1, sel2, true);
	}
	
	
	private int nextSpace(int dir) {
		
		int len = sb.length();
		int i = max(0, min(cursor + dir, len - 1));
		
		if(isWhitespace(sb.charAt(i)))
			i--;
		
		while(i >= 0 && i < len) {
			
			char c1 = sb.charAt(i);
			int i1 = i + 1;
			if(i1 >= len) {
				if(dir > 0) 
					return i1;
			}else {
				char c2 = sb.charAt(i1);
				boolean w = isWhitespace(c1) && !isWhitespace(c2);
				if(w) {
					return i1; 
				}
			}
			i += dir;
		}
		
		return dir > 0 ? len - 1 : 0;
	}
	
	
	private void toClipboard(int s1, int s2) {
		var cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		var sl = new StringSelection(sb.substring(s1, s2));
		cb.setContents(sl, sl);
	}
	
	
	private void command(final int key, final boolean strg, final boolean shift) {
		
		final int LEFT = 37, RIGHT = 39, UP = 38, DOWN = 40;
		
		switch(key) {
		
		case VK_CONTROL -> {return;}
		case LEFT, RIGHT, UP, DOWN -> { //KeyEvent.VK_LEFT
			
			int cur = 0;
			
			switch(key) {
			case LEFT, RIGHT -> {
				int dir = key == LEFT ? -1 : 1;
				cur = strg ? nextSpace(dir) : cursor + dir;
			}
			case UP, DOWN -> {
				int cl = getCurrentLine(cursor) + (key == UP ? -1 : 1);
				cl = min(cl, lines.size() - 1);
				cl = max(cl, 0);
				cur = charIndexFromPixelPosLine(cl, textCursor.getPosObject().x);
			}}
			cur = max(0, cur);
			cur = min(sb.length(), cur);
			
			if(shift){
				setSelection(cur);
			}else{
				int c = sel1 == sel2 ? cur  :
						key  == LEFT ? sel1 : sel2;
				resetCursor(c);
				deselect();
			}
		}
		case VK_HOME, VK_END -> { 
			int p = key == VK_HOME ? 0 : sb.length();//sb.length();
			if(shift)
				setSelection(p);
			else
				resetCursor(p);
		}
		case VK_BACK_SPACE -> {
			if(sel1 == sel2) {
				delete(cursor - 1, cursor, true);
			}else{
				clearSelection();
			}
		}
		case VK_DELETE -> clearSelection();
		case ENTER, VK_ENTER -> onEnterConfirm();
		default -> {
			if(strg) 
				strgCommand(key);
		}
		}
	}
	
	private void strgCommand(int key) {
		
		switch (key) {
		case VK_A -> selectAll();
		case VK_X -> {
			toClipboard(sel1, sel2);
			delete(sel1, sel2, true);
		}
		case VK_C -> toClipboard(sel1, sel2);
		case VK_V -> {
			try {
				var clip = Toolkit.getDefaultToolkit().getSystemClipboard();
				insert((String) clip.getData(DataFlavor.stringFlavor));
			} catch (UnsupportedFlavorException e1) {
				System.err.println("Not a String in Clipboard.");
				e1.printStackTrace();
			} catch (IOException e1) {
				System.err.println("Clipboard unavailable.");
				e1.printStackTrace();
			}
		}
		default -> {}
		}
	}
	
	/**
	 * Called when user closes this Inputfield with Enter/Return
	 */
	public void onEnterConfirm() {
		active(false);
	}


	public void type(final char c) {
		
		//if(isValid(c)) {
			
			clearSelection();
			
			if(sb.length() < charLimit) {
				
				int i = cursor <= sb.length() ? cursor : sb.length();
				sb.insert(i, c);
				
				setText(sb.toString());
				resetCursor(cursor + 1);
				
				if(checkTextFlag(TextFlag.LIVEUPDATE))
					sendMessage();
				
				//typeAction(true, Character.toString(c), i);
			}
		//}
	}
	
	
	public void insert(String str) {
		
		if(charProfile.matches(str)) {
			
			if(charProfile == CharProfile.INT) {
				int i = str.indexOf('.');
				if(i >= 0)
					str = str.substring(0, i);
			}
			
			clearSelection();
			
			if(sb.length() + str.length() > charLimit) {
				int s = Math.max(0, charLimit - sb.length());
				str = str.substring(0, s);
			}
			sel1 = min(sel1, sb.length());
			sb.insert(sel1, str);
			setText(sb.toString());
			
			resetCursor(sel1 + str.length());
		}
	}
	
	
	private void wordSelect(float mx) {
		
		int len = sb.length();
		
		if(len <= 0)
			return;
		
		int p = Math.min(cursor, len - 1);
		p -= mx < textCursor.getPosObject().x ? 1 : 0;
		int s1 = p, s2 = p;
		
		for (int i = p; i >= 0; i--) {
			if(isLetterOrDigit(sb.charAt(i))) 
				s1 = i;
			else 
				break;
		}
		
		for (int i = p; i < len; i++) {
			s2 = i;
			if(!isLetterOrDigit(sb.charAt(i)))
				break;
			else 
			if(i == len - 1)
				s2 = len;
		}
		
		setSelection(s1, s2);
	}
	
	
	public void mouseEvent(InputEvent e, UserInput input) {
		
		super.mouseEvent(e, input);
		
		if(active) {
			switch(e) {
			case KEY, KEY_AUTOREPEAT -> {
				
				if(charProfile.matches(input.getLastChar())) {
					type(input.getLastChar());
				}else {
					command(input.getLastKeyCode(), input.ctrl, input.shift);
				}
			}
			default -> {}
			}
		}
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		super.mouseEventThis(e, input);
		
		if(disabled)
			return;
		
		switch(e) {
		
		case DOUBLECLICK -> {
			if(active)
				wordSelect(input.getMouseX() - getX());
			else{
				active(true);
				setCursor(sb.length());
				getDm().getGuiDomain().getPApplet().cursor(PConstants.TEXT);
				//PROC.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR)); TODO Cursor Procressing 3
			}
		}
		case PRESSED -> {
			if(active) {
				float x = input.getMouseX() - getX(),
					  y = input.getMouseY() - getY();
				setCursor(getCharIndexFromPixelPos(x, y));
				deselect();
			}
		}
		case FOCUS_LOST -> active(false);
		case DRAG -> {
			if(active) {
				float x = input.getMouseX() - getX(),
					  y = input.getMouseY() - getY();
				setSelection(getCharIndexFromPixelPos(x, y));
			}
		}
		case OVER -> {
			if(active && input.isCurrentTarget(self))
				self.getDm().getDomain().getPApplet().cursor(PConstants.TEXT);
		}
		case OVER_STOP -> {
			if(input.isCurrentTarget(self) && active) 
				self.getDm().getDomain().getPApplet().cursor(PConstants.ARROW);
		}	
		default -> {}
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj == self;
	}
	
	@Override
	public int hashCode() {
		return self.hashCode();
	}

	
	private DisplayObjectIF addSelRect(float x1, float x2, float y) {
		
		var d = new DisplayObject() {
			@Override
			public void render(PGraphics g) {
				g.blendMode(PConstants.EXCLUSION);
				g.fill(getColor());
				g.rect(getX(), getY(), getWidth(), lineHeight());
				g.blendMode(PConstants.BLEND);
			}
		};
		d.setFlag(fixedToParent, true);
		d.setWH(getWidth(), (int)(lineHeight()));
		d.setFlag(DisplayFlag.RECT, true);
		d.setColor(0xffffffff);
		d.setFlag(CLICKABLE, false);
		
		d.getPosObject().x = x1;
		d.getPosObject().y = y;
		d.setWH((int)(x2 - x1), (int) ceil(lineHeight()));
		
		selRect.add(d);
		addChild(d);
		return d;
	}
	
	private void clearSelRect() {
		for (int i = selRect.size() - 1; i >= 0; i--) {
			var r = selRect.remove(i);
			DisplayObjects.removeFromDisplay(r);
		}
	}
	
	

	class TCursor extends DisplayObject {
		
		int c = 0;
		boolean v = true;
		final float w = 2;
		
		public TCursor() {
			setFlag(CLICKABLE, false);
			setFlag(fixedToParent, true);
		}
		
		void resetBlink() {
			c = 0;
			v = true;
		}
		
		@Override public void render(PGraphics g) {
			
			c++;
			if(c > 20) {
				c = 0;
				v = !v;
			}
			if(v) {
				g.blendMode(PConstants.EXCLUSION);
				g.fill(Colors.WHITE);
				g.rect(getX() - w / 2, getY(), w, lineHeight());
				
				g.blendMode(PConstants.BLEND);
			}
			
			
		}
	}
	
	/*
	private void typeAction(boolean t, String s, int pos) {
		var a = new TextAction(t, s, pos);
		getDm().getGuiDomain().getHistory().addAction(a);
	}
	*/
	
	@RequiredArgsConstructor
	class TextAction implements Action {
		final boolean type;
		final String text;
		final int start;
		
		private void write() {
			setCursor(start);
			insert(text);
			//setSelection(start, start + text.length());
		}
		
		private void del() {
			setCursor(start);
			delete(start, start + text.length(), false);
		}
		
		@Override
		public void execute() {
			if(type) write();
			else	 del();
		}
		
		@Override
		public void undo() {
			if(type) del();
			else	 write();
		}
		
	}
	
	
}
