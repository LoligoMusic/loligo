package gui.text;

import static com.google.common.base.CharMatcher.anyOf;
import static com.google.common.base.CharMatcher.digit;
import static com.google.common.base.CharMatcher.inRange;
import static com.google.common.base.CharMatcher.javaIsoControl;

import com.google.common.base.CharMatcher;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum CharProfile { 
	
	FILENAME, PLAINTEXT, INT, DEZIMAL, HEX;

	@Getter
	private CharMatcher matcher;
	
	public boolean matchesSingleLine(char c) {
		return matcher.and(SINGLE).matches(c);
	}
	
	public boolean matchesSingleLine(String s) {
		return matcher.and(SINGLE).matchesAllOf(s);
	}
	
	public boolean matches(char c) {
		return matcher.or(MULTI).matches(c);
	}
	
	public boolean matches(String s) {
		return matcher.or(MULTI).matchesAllOf(s);
	}
	
	//Unicode < 32 not printable. \u007F = BACKSPACE, 65535 = invalid char
	public static final CharMatcher 
		BASE   = javaIsoControl().or(CharMatcher.is((char) 65535)).negate(),
		PLAIN  = BASE,
		NUM    = BASE.and(inRange('0', '9').or(anyOf("-."))),
		HX	   = BASE.and(digit()).or(inRange('a', 'f')).or(inRange('A', 'F')).or(anyOf("Xx#")),
		SINGLE = CharMatcher.noneOf("\n\r"),
		MULTI  = CharMatcher.anyOf("\n\r\t");
	
	static {
		FILENAME .matcher = 
		PLAINTEXT.matcher = PLAIN;
		INT		 .matcher = 
		DEZIMAL	 .matcher = NUM;
		HEX		 .matcher = HX;
	}
	
}