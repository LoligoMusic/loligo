package gui.text;

import static util.Notes.toFrequency;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.jboss.forge.roaster.ParserException;

import util.Notes;

public class NoteInputField extends NumberInputField {
	
	private static float A0 = toFrequency(Notes.A0), A4 = Notes.toFrequency(Notes.C8);
	
	public NoteInputField(Consumer<Double> action, Supplier<Double> updateAction) {
		super(action, updateAction);
		
		//setScrollSpeed(100);
		//setCharProfile(TextInputField.INT);
		setRange(A0, A4);
	}

	@Override
	public double scrollFunction(int dy, int scale) {
		//double d = super.scrollFunction(dy, scale);
		
		scale -= 2;
		
		double d = Math.pow(10, scale);
		
		double n = Notes.freqToNote(scroll);
		n -= getScrollSpeed() * dy / d;
		n = new BigDecimal(n).setScale(scale, RoundingMode.HALF_UP).floatValue();
		
		scroll = Notes.toFrequency(n);
		
		return scroll;
	}
	
	@Override
	public double parseStringValue(String t) {
		
		try {
			double n = Notes.valueOf(t);
			return Notes.toFrequency(n);
		} catch(ParserException e) {
			return super.parseStringValue(t);
		}
	}
	
	@Override
	protected String getStringValue() {
		double n = Notes.freqToNote(getValue());
		return Notes.toNoteName(n);
	}
	
}
