package gui.text;

import static gui.text.TextBlock.AlignMode.CENTER;
import static gui.text.TextBlock.AlignMode.LEFT;
import static gui.text.TextBlock.AlignMode.RIGHT;
import static gui.text.TextBlock.TextFlag.MULTILINE;
import static java.lang.Character.isWhitespace;
import static java.lang.Math.ceil;
import static java.lang.Math.max;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import constants.DisplayFlag;
import gui.ResizeBox;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pr.DisplayObject;
import processing.core.PFont;
import processing.core.PGraphics;
import util.Utils;


public class TextBlock extends DisplayObject implements TextObject {

	//private static final int MAX_WIDTH = 500;
	public static enum AlignMode {LEFT, RIGHT, CENTER};
	public static enum TextFlag{RESIZEABLE, MULTILINE, LIVEUPDATE, AUTOFIT};
	//public static enum CharProfile{FILENAME, PLAINTEXT, INT, DEZIMAL};
	
	@Getter
	private String text = "";
	@Getter@Setter
	private float paddingLeft = 3, paddingRight = 3, paddingY = 3;
	@Getter
	private int textSize = 14;
	@Getter@Setter
	private PFont font = Fonts.DEFAULT_14;
	@Setter
	private boolean background = false, fixedWidth;
	@Getter@Setter
	private int textColor = 0xff111111;
	@Getter
	private AlignMode alignMode = LEFT;
	protected final EnumSet<TextFlag> textFlags = EnumSet.allOf(TextFlag.class);
	protected CharProfile charProfile = CharProfile.PLAINTEXT;
	//private final WidthHandle widthHandle;
	private final ResizeBox resizeBox;
	protected final List<Line> lines = new ArrayList<>();
	
	
	public TextBlock() {
		
		setFlag(DisplayFlag.RECT, true);
		setWH(150, 200);
		//widthHandle = new WidthHandle();
		setColor(TextInputField.TEXT_COLOR_INACTIVE);
		addLine("", 0);
		
		resizeBox = new ResizeBox(this);
		resizeBox.setVerticalResize(false);
		addChild(resizeBox);
		resizeBox.setOnResize((x, y) -> adaptSize());
	}
	
	
	public void setTextFlag(TextFlag textFlag) {
		textFlags.add(textFlag);
	}
	
	public boolean checkTextFlag(TextFlag textFlag) {
		return textFlags.contains(textFlag);
	}
	
	public void setPadding(float p) {
		paddingLeft = paddingRight = paddingY = p;
	}
	
	public void setTextSize(int size) {
		font = Fonts.getFont(size, Fonts.FONTS_REGULAR);
		textSize = size;
		
		adaptSize();
	}
	
	/*
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(e == InputEvent.OVER) {
			addChild(widthHandle);
		}else
		if(e == InputEvent.OVER_STOP) {
			removeChild(widthHandle);
		}
	}
	*/
	
	
	@Override
	public void render(PGraphics g) {
	
		if(background) 
			super.render(g);
		
		g.fill(textColor);
		g.textFont(font, textSize);
		
		float h = lineHeight();
		for (int i = 0; i < lines.size(); i++) {
			var li = lines.get(i);
			String t = li.text;
			g.text(t, getX() + paddingLeft + li.offsetX, getY() + paddingY + i * h);
		}
	}
	
	
	public void setText(String txt) {
		text = txt;
		adaptSize();
	}
	
	public void setAlignMode(AlignMode a) {
		alignMode = a;
		adaptSize();
	}
	
	
	public void lineBreak() {
		
		lines.clear();
		
		if (!checkTextFlag(MULTILINE)) {
			String s = text.replace('\n', ' ');
			lines.add(new Line(s, 0, 0, 100));
			return;
		}
		
		float width = textWidth();
		
		final int len = text.length();
		int beginW = 0, beginL = 0;
		float lineWidth = 0, wordWidth = 0;
		
		for (int i = 0; i < len; i++) {
			
			char c = text.charAt(i);
			float ww = Utils.textWidth(c, textSize, font);
			lineWidth += ww;
			wordWidth += ww;
			
			if(c == '\t' || c == '?') {
				
			}else
			if(lineWidth >= width) {
				
				if(lineWidth == wordWidth) {
					String line = text.substring(beginL, i);
					addLine(line, 0);
					lineWidth =
					wordWidth = ww;
					beginW =
					beginL = i;
				}else
				if(lineWidth > width)
				{
					String line = text.substring(beginL, beginW);
					addLine(line, 1);
					lineWidth = wordWidth;
					beginL = beginW + 1;
				}
			}else
			if(c == '\n' || c == '\r') {
				String line = text.substring(beginL, i);
				addLine(line, 1);
				lineWidth = 0;
				beginL = i + 1;
				if(i == len - 1) 
					addLine("", 1);
				
			}
			
			if(isWhitespace(c)) {
				wordWidth = 0;
				beginW = i;
			}
			
		}
		
		if(beginL < len) {
			String line = text.substring(beginL);
			addLine(line, 1);
		}
		
	}
	
	
	public void adaptSize() {
		
		if(getDm() == null)
			return;
		
		lineBreak();
		
		var pr = getDm().getGuiDomain().getPApplet();
		
		pr.textSize(textSize);
		
		String[] ss = text.split("\\r?\\n");
		
		if(fixedWidth) {
			float c = (float) ceil(max(1, lines.size()));
			int h = (int)(paddingY * 2 - leading()  + c * lineHeight());
			setWH(getWidth(), h);
			
		}else{
			
			int w = 0;
			for(String s : ss) 
				w = Math.max(w, (int) Utils.textWidth(s, textSize, font));
			
			float th = (pr.textAscent() + pr.textDescent()) * (ss.length > 1 ? 1.2f : 1);
			float h = th * ss.length;
			float p = paddingY * 2 + 3; 
			
			w += p;
			h += p;
			
			setWH((int) w, (int) h);
		}
	}
	
	public float lineHeight() {
		return (font.ascent() + font.descent()) * textSize * Fonts.LEADING_FACTOR; 
	}
	
	public float leading() {
		return (font.ascent() + font.descent()) * textSize * (Fonts.LEADING_FACTOR - 1); //g.textAscent() - g.textDescent();
	}
	
	public float textWidth() {
		return getWidth() - paddingLeft - paddingRight;
	}
	
	@Override
	public void addedToDisplay() {
		adaptSize();
	}
	
	/*
	private class WidthHandle extends DisplayObject {
		final int handleWidth = 5;
		
		public WidthHandle() {
			setFlag(DisplayFlag.clickable, true);
			setFlag(DisplayFlag.dragable, true);
			setFlag(DisplayFlag.RECT, true);
			setGuiType(GuiType.DRAGABLE_INPUT);
			setPosObject(new Position.Bounds(TextBlock.this, MAX_WIDTH, 0));
		}
		
		@Override
		public void addedToDisplay() {
			setWH(handleWidth, TextBlock.this.getHeight());
			setPos(TextBlock.this.getWidth() - getWidth(), 0);
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			if(e == InputEvent.DRAG) {
				int w = (int) (getX() - TextBlock.this.getX());
				int m = Math.max(w + getWidth(), handleWidth);
				TextBlock.this.setWH(m, 0);
				adaptSize();
				setWH(handleWidth, TextBlock.this.getHeight());
			}else
			if(e == InputEvent.OVER) {
				TextBlock.this.addChild(this);
			}else
			if(e == InputEvent.OVER_STOP && input.getDragTarget() != this) {
				TextBlock.this.removeChild(this);
			}
		}
		
	};
	*/
	
	private Line addLine(String text, int spaces) {
		
		float w = Utils.textWidth(text, textSize, font);
		float e = textWidth() - w;
		
		float o = alignMode == LEFT   ? 0 :
				  alignMode == RIGHT  ? e :
				  alignMode == CENTER ? e / 2 : 
					  					0;
		
		var v = new Line(text, spaces, o, w);
		lines.add(v);
		return v;
	}
	
	@RequiredArgsConstructor 
	protected class Line {
		final String text;
		final int spaces;
		final float offsetX, width;
		
		@Override
		public String toString() {
			return text;
		}
	}

	
}
