package gui.text;

import static constants.InputEvent.OVER;
import static constants.InputEvent.OVER_STOP;
import static gui.text.TextBlock.AlignMode.LEFT;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;
import static util.Images.loadPImage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.Button;
import gui.DisplayGuiSender;
import gui.GuiSenderObject;
import gui.Position;
import gui.selection.GuiType;
import gui.text.TextBlock.AlignMode;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayContainerIF;
import pr.DisplayManagerIF;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import processing.core.PImage;
import util.AsyncFileDialog;
import util.Colors;
import util.Utils;

public class FileInputField implements DisplayGuiSender<URL>{
	
	private static final PImage folder_icon   = loadPImage("/resources/folder_16px.png", Colors.WHITE),
								refresh_icon  = loadPImage("/resources/refresh14_2.png", Colors.WHITE);
	private static final String PROTOCOL_FILE = "file:/", PROTOCOL_HTTP = "http://";
	private static final Pattern REG_PROTOCOL = compile("^\\w+:\\/\\/\\w+", CASE_INSENSITIVE);
	
	private final TextInputField textField;
	private final Button fileButton, refreshButton;
	private final GuiSenderObject<URL> sender;
	private String errorText = "";
	@Getter
	private boolean isFileChooserOpen, isURLValid, isFileValid = true; //URLValid: URL format correct, FileValid: set from outside
	@Setter
	private Consumer<String> onInvalidPath;
	@Getter@Setter
	private AsyncFileDialog fileDialog = new AsyncFileDialog(null, null);
	
	
	public FileInputField(Consumer<URL> action, Supplier<URL> updateAction) {
	
		 textField = new TextInputField(this::setStringValue, this::getStringValue) {
				TextBlock t;
				@Override 
				public void mouseEventThis(InputEvent e, UserInput input) {
					
					super.mouseEventThis(e, input);
					
					if(e == OVER) {
						
						if(errorText.length() > 0 && !(isURLValid && isFileValid)) {
							 t = new TextBlock();
							 t.setFlag(DisplayFlag.CLICKABLE, false);
							 t.fixedToParent(true);
							 t.setBackground(true);
							 t.setText(errorText);
							 this.addChild(t);
						 }
					}else
					if(e == OVER_STOP) {
						this.removeChild(t);
					}
				}
			 };
		 textField.setSelf(this);
		 textField.setAutoFit(true);
		 textField.setOffsetX(43);
		 textField.setAlignMode(LEFT);
			 
		 sender = new GuiSenderObject<URL>(this, action, updateAction, null);
		 
		 fileButton    = addButton(folder_icon , this::openFileChooser,  3, 2, "Open File");
		 refreshButton = addButton(refresh_icon, this::reload         , 22, 3, "Reload"   );
		 
	}
	
	
	private Button addButton(PImage img, Runnable action, int x, int y, String mouseText) {
		 var b = new Button(action, "");
		 b.setImage(img);
		 b.setWH(20, 20);
		 b.setPos(x, y);
		 b.setFlag(DisplayFlag.fixedToParent, true);
		 b.setMouseOverText(mouseText);
		 b.setButtonColor(Colors.grey(80));
		 b.setMouseOverButtonColor(Colors.grey(200));
		 b.setButtonPressedColor(Colors.grey(60));
		 addChild(b);
		 return b;
	}

	
	public void openFileChooser() {
		
		if(isFileChooserOpen)
			return;
		
		isFileChooserOpen = true;
		
		Consumer<File[]> c = ff -> {
			isFileChooserOpen = false;
			
			if(ff != null && ff.length > 0) {
				
				File f = ff[0];
				setFile(f);
			}
		};
		
		fileDialog.setDm((DisplayManagerIF) getDm()); 
		fileDialog.setOnReturn(c);
		fileDialog.runAsync();
	}
	
	
	public void reload() {
		setFile(sender.getValue());
	}
	
	
	public void setFile(File f) {
		
		try {
			URL u = f.toURI().toURL();
			setFile(u);
			isURLValid(true);
		} catch (MalformedURLException e) {
			isURLValid(false);
			e.printStackTrace();
		}
	}
	
	public void setFile(URL u) {
		if(u == null)
			return;
		isURLValid(true);
		sender.message(u);
		update();
	}
	
	private String checkProtocol(String s) {
		
		s = s.trim();
		
		if(s == null || s.length() <= 0) {
			return "";
		}
		
		try {
			new URL(s);
			return s;
		} catch (MalformedURLException e) {
			
			if(REG_PROTOCOL.matcher(s).matches()) {
				System.err.println(e);
				return s;
			}
			if(s.startsWith("www.")) {
				return PROTOCOL_HTTP + s;
			}
			return PROTOCOL_FILE + s;
		}
	}
	
	private void setStringValue(String t) {
		
		String[] ee = fileDialog.getEndings();
		
		if(fileDialog.isSave() && ee.length > 0 && !t.isEmpty())
			t = Utils.addFileEnding(t, ee[0]);
		
		t = checkProtocol(t);
		
		try {
			URL f = new URL(t);
			//isURLValid(true);
			//sender.message(f);
			setFile(f);
		} catch (MalformedURLException e) {
			isURLValid(false);
			e.printStackTrace();
			return;
		}
	}
	
	private void isURLValid(boolean b) {
		isURLValid = b;
	}
	
	public void isFileValid(boolean b, String message) {
		isFileValid = b;
	}
	
	/*
	public void validate(boolean valid, String msg) {

		isFileValid = true;
		
		if(f == null || f.getName().isEmpty()) {
			onInvalidPath("No File specified.");
			isFileValid = false;
		}else {
		
			try {
				Paths.get(f.getPath());
				f = f.getCanonicalFile();
			}catch(InvalidPathException | IOException e) {
				
				isFileValid = false;
				onInvalidPath(e.getMessage());
			} 
		}
		
		textField.setColor(isFileValid ? textField.getColor_inactive() : 0xffaa5555);
	}
	*/
	
	public void setFile(String path) {
		setStringValue(path);
	}
	
	public void onInvalidPath(String msg) {
		
		System.out.println(msg);
		
		errorText = msg;
		
		if(onInvalidPath != null)
			onInvalidPath.accept(msg);
	}
	
	private String getStringValue() {
		URL f = sender.getValue();
		
		//validate(f);
		if(f == null)
			return null;
		
		String s = f.toExternalForm();
		
		if(s.startsWith(PROTOCOL_FILE)) {
			s = s.substring(PROTOCOL_FILE.length());
		}
		return s;
	}

	
	@Override
	public URL getValue() {
		return sender.getValue();
	}

	@Override
	public void setValue(URL v) {
		
		sender.setValue(v);
		textField.update();
	}

	@Override
	public void update() {
		textField.update();
		sender.update();
		boolean v = sender.getValue() != null;
		//refreshButton.disable(v);
		isURLValid(v);
	}

	@Override
	public void disable(boolean b) {
		fileButton.disable(b);
		textField.disable(b);
		refreshButton.disable(b);
	}
	
	
	
	
	//---------DisplayObject delegation methods---------------

	@Override
	public int hashCode() {
		return textField.hashCode();
	}


	@Override
	public void render(PGraphics g) {
		textField.render(g);
	}


	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		textField.mouseEvent(e, input);
	}


	public final String getText() {
		return textField.getText();
	}


	public final void setAlignMode(AlignMode mode) {
		textField.setAlignMode(mode);
	}


	public final void align() {
		textField.align();
	}


	@Override
	public boolean hitTest(float mx, float my) {
		return textField.hitTest(mx, my);
	}


	@Override
	public void addedToDisplay() {
		textField.addedToDisplay();
		update();
	}


	@Override
	public void removedFromDisplay() {
		textField.removedFromDisplay();
	}


	public void setValue(String text) {
		textField.setValue(text);
	}


	public void active(boolean a) {
		textField.active(a);
	}


	public final void setPadding(int padding) {
		textField.setPadding(padding);
	}


	@Override
	public void addChild(DisplayObjectIF d) {
		textField.addChild(d);
	}


	@Override
	public boolean equals(Object obj) {
		return textField.equals(obj);
	}


	@Override
	public void addChild(DisplayObjectIF... dArr) {
		textField.addChild(dArr);
	}


	@Override
	public void removeChild(DisplayObjectIF d) {
		textField.removeChild(d);
	}


	public void setText(String t) {
		textField.setText(t);
	}


	public void liveupdate(boolean b) {
		textField.liveupdate(b);
	}

	@Override
	public void removeChild(DisplayObjectIF[] dArr) {
		textField.removeChild(dArr);
	}


	@Override
	public void setSelectionObject(Consumer<PGraphics> cons) {
		textField.setSelectionObject(cons);
	}


	@Override
	public void setSelected(boolean b) {
		textField.setSelected(b);
	}


	@Override
	public void fixedToParent(boolean b) {
		textField.fixedToParent(b);
	}


	@Override
	public float getX() {
		return textField.getX();
	}


	@Override
	public float getY() {
		return textField.getY();
	}


	@Override
	public void setPos(float x, float y) {
		textField.setPos(x, y);
	}


	@Override
	public void setGlobal(float x, float y) {
		textField.setGlobal(x, y);
	}


	@Override
	public Position getPosObject() {
		return textField.getPosObject();
	}


	@Override
	public void setPosObject(Position p) {
		textField.setPosObject(p);
	}


	@Override
	public DisplayObjectIF getParent() {
		return textField.getParent();
	}


	@Override
	public void setParent(DisplayObjectIF parent) {
		textField.setParent(parent);
	}


	@Override
	public List<DisplayObjectIF> getChildren() {
		return textField.getChildren();
	}


	@Override
	public DisplayContainerIF getDm() {
		return textField.getDm();
	}


	@Override
	public void setDm(DisplayContainerIF dm) {
		textField.setDm(dm);
	}


	@Override
	public boolean checkFlag(DisplayFlag flag) {
		return textField.checkFlag(flag);
	}


	@Override
	public void setFlag(DisplayFlag f, boolean b) {
		textField.setFlag(f, b);
	}


	@Override
	public int getColor() {
		return textField.getColor();
	}


	@Override
	public int setColor(int color) {
		return textField.setColor(color);
	}


	@Override
	public int getStrokeColor() {
		return textField.getStrokeColor();
	}


	@Override
	public void setStrokeColor(int strokeColor) {
		textField.setStrokeColor(strokeColor);
	}


	@Override
	public int getWidth() {
		return textField.getWidth();
	}


	@Override
	public void setWH(int width, int height) {
		textField.setWH(width, height);
	}


	@Override
	public int getHeight() {
		return textField.getHeight();
	}


	@Override
	public GuiType getGuiType() {
		return textField.getGuiType();
	}


	@Override
	public void setGuiType(GuiType guiType) {
		textField.setGuiType(guiType);
	}


	public void onEnterConfirm() {
		textField.onEnterConfirm();
	}


	@Override
	public String toString() {
		return textField.toString();
	}


	public void type(char c) {
		textField.type(c);
	}


	public void insert(String str) {
		textField.insert(str);
	}


	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		textField.mouseEventThis(e, input);
	}

	@Override
	public void mouseOverRest(boolean on) {
		textField.mouseOverRest(on);
	}

}
	
	
	

