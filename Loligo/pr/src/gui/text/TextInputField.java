package gui.text;

import static com.jogamp.newt.event.KeyEvent.VK_A;
import static com.jogamp.newt.event.KeyEvent.VK_BACK_SPACE;
import static com.jogamp.newt.event.KeyEvent.VK_C;
import static com.jogamp.newt.event.KeyEvent.VK_CONTROL;
import static com.jogamp.newt.event.KeyEvent.VK_DELETE;
import static com.jogamp.newt.event.KeyEvent.VK_END;
import static com.jogamp.newt.event.KeyEvent.VK_ENTER;
import static com.jogamp.newt.event.KeyEvent.VK_HOME;
import static com.jogamp.newt.event.KeyEvent.VK_V;
import static constants.DisplayFlag.CLICKABLE;
import static constants.DisplayFlag.DRAGABLE;
import static constants.DisplayFlag.RECT;
import static constants.DisplayFlag.fixedToParent;
import static gui.text.CharProfile.INT;
import static gui.text.TextBlock.AlignMode.CENTER;
import static java.lang.Math.max;
import static java.lang.Math.min;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.InputEvent;
import gui.DisplayGuiSender;
import gui.DisplayObjects;
import gui.GuiSenderObject;
import gui.MouseOverTextHandler;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Delegate;
import pr.DisplayObject;
import pr.RootClass;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PGraphics;
import util.Colors;


public class TextInputField extends TextBox implements DisplayGuiSender<String>{
	
	static public final int TEXT_COLOR_ACTIVE = 0xff222222, TEXT_COLOR_INACTIVE = Colors.grey(50);
	
	@Setter
	private DisplayGuiSender<?> self; // self reference for NumberInputField delegation
	@Delegate@Getter
	private final MouseOverTextHandler mouseOverTextHandler = new MouseOverTextHandler(this);
	
	public boolean active = false;
	
	protected boolean disabled = false;
	private boolean liveupdate;
	@Setter
	private boolean undoable;
	@Getter@Setter
	private CharProfile charProfile;
	@Getter@Setter
	private int charLimit = Integer.MAX_VALUE;
	private final GuiSenderObject<String> sender;
	private final TCursor textCursor;
	private final DisplayObject selRect;
	private int cursor = 0, sel, sel1, sel2;
	private boolean isDragable;
	private final StringBuffer sb;
	
	@Getter@Setter private int color_active 		= COLOR_ACTIVE;
	@Getter@Setter private int color_inactive 		= COLOR_INACTIVE;
	@Getter@Setter private int text_color_active 	= TEXT_COLOR_ACTIVE;
	@Getter@Setter private int text_color_inactive 	= TEXT_COLOR_INACTIVE;
	
	
	public TextInputField(Consumer<String> action, Supplier<String> updateAction) {
		
		this(action, updateAction, CharProfile.PLAINTEXT);
	}
	
	
	public TextInputField(Consumer<String> action, Supplier<String> updateAction, CharProfile charProfile) {
		
		super("");
		
		self = this;
		
		sb = new StringBuffer();
		
		this.charProfile = charProfile;
		
		Supplier<String> ua = null;
		if(updateAction != null) {
			ua = () -> {
				String s = updateAction.get();
				setCursor(cursor);
				return s;
			};
		}
		
		sender = new GuiSenderObject<String>(this, action, ua, "");
		bgBox = true;
		
		//this.target = target;
		setWH(50, 20);
		
		setFlag(CLICKABLE, true);
		//setFlag(DisplayFlag.Subscriber, true);
		
		textCursor = new TCursor();
		
		selRect = new DisplayObject() {
			@Override
			public void render(PGraphics g) {
				g.blendMode(PConstants.EXCLUSION);
				super.render(g);
				g.blendMode(PConstants.BLEND);
			}
		};
		selRect.setFlag(fixedToParent, true);
		selRect.setWH(getWidth(), (int)(textHeight * 1.4f));
		selRect.setFlag(RECT, true);
		selRect.setColor(0xffffffff);
		selRect.setFlag(CLICKABLE, false);
		selRect.getPosObject().y = getOffsetY();
		
		//active(false);
		setColor(Colors.grey(color_inactive));
		setTextColor(text_color_inactive);
		
		setAlignMode(CENTER);
		setAutoFit(true);
	}
	
	public void singleThread() {
		sender.singleThread();
	}
	
	@Override
	public void addedToDisplay() {
		DisplayObjects.addSubscriber(self);
		self.update();
	}
	
	@Override
	public void removedFromDisplay() {
		DisplayObjects.removeSubscriber(self);
	}
	
	
	@Override
	public void setValue(String text) {
		
		if(!active) {
			//delete(0, sb.length(), false);
			sb.setLength(0);
			resetCursor(0);
			if(text != null)
				insert(text);
		}
		
		sender.setValue(text);
	}
	
	@Override
	public void update() {
		sender.update();
	}
	
	@Override
	public String getValue() {
		return sender.getValue();
	}
	
	
	public void active(boolean a) {
		
		if(disabled || a == active)
			return;
		
		active = a;
		enableAlignOverflow = !active;
		resetCursor(sb.length());
		
		var dm = self.getDm();
		var proc = dm.getGuiDomain().getPApplet();
		var input = dm.getInput();
		
		if(active) {
			
			if(input.getOverTarget() == self)
				proc.cursor(PConstants.TEXT);
			
			self.setColor(color_active);
			setTextColor(text_color_active);
			
			self.addChild(textCursor);
			
			selectAll();
			
			isDragable = self.checkFlag(DRAGABLE);
			self.setFlag(DRAGABLE, false);
			input.setFocusTarget(self);	
			input.setSpecialFocusTarget(self);
			input.addInputEventListener(self);
			
		} else{
			
			proc.cursor(PConstants.ARROW);
			self.setColor(color_inactive);
			setTextColor(text_color_inactive);
			
			self.removeChild(textCursor);
			DisplayObjects.removeFromDisplay(selRect);
			
			sendMessage();
			
			self.setFlag(DRAGABLE, isDragable);
			
			input.selection.removeActiveElement(self);
			input.removeSpecialFocusTarget(self);
			input.removeInputEventListener(this);
		}
	}

	
	private void sendMessage() {
		sendMessage(sb.toString());
	}

	
	protected void sendMessage(String s) {
		
		String v = sender.getValue();
		
		if(v == null || !v.equals(s)) {
		
			sender.message(s, undoable);
			sender.update();
		}
	}
	
	
	@Override
	public void setText(String t) {
		
		super.setText(t);
	}
	
	
	public void liveupdate(boolean b) {
		
		liveupdate = b;
	}
	
	
	@Override
	public void disable(boolean b) {
		
		if(b) {
			
			active(false);
			disabled = true;
			
			self.setColor(color_disabled);
			setTextColor(Colors.grey(40));
			
		} else{
			
			disabled = false;
			self.setColor(color_inactive);
			active(false);
		}
		//setFlag(DisplayFlag.clickable, !b);
		
	}
	
	
	private void resetCursor(int c) {
	
		setCursor(c);
		deselect();
	}
	
	
	private void setCursor(int c) {
		
		cursor = c;
		
		if(c <= displayStart)
			displayStartOffset = c - 4;
		else
		if(c >= displayEnd)
			displayStartOffset = c - displayText.length() + 4;
		
		align();
		
		textCursor.resetBlink();
		textCursor.getPosObject().x = getTextPos(cursor);
		textCursor.getPosObject().y = selRect.getPosObject().y;
		textCursor.setWH(1, selRect.getHeight());
	}
	
	
	private void setSelection(int c1, int c2) {
		
		if(c1 == c2) 
			deselect();
		else{
			sel = c1;
			setSelection(c2);
		}
	}
	
	
	private void setSelection(int c) {
		
		setCursor(c);
		
		if(cursor > sel) {
			sel1 = sel;
			sel2 = cursor;
		}else{
			sel1 = cursor;
			sel2 = sel;
		}
		
		if(cursor != sel) 
			self.addChild(selRect);
			
		int x1 = (int) getTextPos(sel1),
			x2 = (int) getTextPos(sel2);
		
		//x2 = Math.min(x2, displayEnd);
		
		selRect.getPosObject().x = x1;
		selRect.getPosObject().y = getOffsetY();
		selRect.setWH(x2 - x1, getHeight() - getOffsetY() * 2 );
	}
	
	public void selectAll() {
		setSelection(0, sb.length());
	}
	
	private void deselect() {
		
		DisplayObjects.removeFromDisplay(selRect);
		sel = sel1 = sel2 = cursor;
	}
	
	
	private float getTextPos(int x) {
		x = max(x, displayStart);
		x = min(x, min(displayEnd, sb.length()));
		
		String str = sb.substring(displayStart, x);
		float f = RootClass.mainDm().getGraphics().textWidth(str);
		return f + offX;
	}
	
	
	private int getCharIndexFromPixelPos(float x) {
		
		char[] cc = displayText.toCharArray();
		float min = Float.MAX_VALUE;
		int pos = -1;
		
		for (int i = 0; i < cc.length + 1; i++) {
			
			float w = i == 0 ? 0 : self.getDm().getGraphics().textWidth(cc, 0, i);
			float d = Math.abs(w - x + offX);
			
			if(d < min) {
				min = d;
				pos = i;
			}
		}
		
		return pos + displayStart;
	}
	
	
	private void delete(int s1, int s2, boolean send) {
		
		s1 = s1 < 0 		  ? 0 :
			 s1 > sb.length() ? sb.length() :
				 				s1;
		s2 = s2 < 0 		  ? 0 :
			 s2 > sb.length() ? sb.length() :
				 				s2;
		
		if(s1 < s2) {
		
			sb.delete(s1, s2);
			setText(sb.toString());
			resetCursor(s1);
			
			if(liveupdate && send)
				sendMessage();
		}
	}
	
	
	private void clearSelection() {
		
		delete(sel1, sel2, true);
	}
	
	
	private void command(final int key, final boolean strg, final boolean shift) {
		
		if(key == VK_CONTROL)
			return;
		
		final int LEFT = 37, RIGHT = 39;//KeyEvent.VK_LEFT
		
		switch(key) {
		case LEFT, RIGHT -> { 
			
			int cur = cursor + (key == LEFT ? -1 : 1);
			cur = Math.max(0, cur);
			cur = Math.min(sb.length(), cur);
			
			if(shift && strg){
				
			}else
			if(shift){
				setSelection(cur);
			}
			else
			if(strg){
				
				deselect();
			}else{
				int c = sel1 == sel2 ? cur  :
						key  == LEFT ? sel1 : sel2;
				resetCursor(c);
				deselect();
			}
		} 
		case VK_HOME, VK_END -> { 
			
			int p = key == VK_HOME ? 0 : sb.length();//sb.length();
			
			if(shift)
				setSelection(p);
			else
				resetCursor(p);
			
		}
		case VK_BACK_SPACE -> {
			
			if(sel1 == sel2) {
				
				delete(cursor - 1, cursor, true);
				
			}else{
				clearSelection();
			}
			
		} 
		case VK_DELETE -> clearSelection();
		case PConstants.ENTER, VK_ENTER -> onEnterConfirm();
		default -> {
			
			if(strg) {
				
				switch (key) {
				case VK_A -> selectAll();
				case VK_C -> { 
					var clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					var sl = new StringSelection(sb.substring(sel1, sel2));
					clipboard.setContents(sl, sl);
				}
				case VK_V -> { 
					try {
						var clip = Toolkit.getDefaultToolkit().getSystemClipboard();
						insert((String) clip.getData(DataFlavor.stringFlavor));
					} catch (UnsupportedFlavorException e1) {
						System.err.println("Not a String in Clipboard.");
						e1.printStackTrace();
					} catch (IOException e1) {
						System.err.println("Clipboard unavailable.");
						e1.printStackTrace();
					}
				}
				default -> {}
				}
			}
		
		}}
		
	}
	
	/**
	 * Called when user closes this Inputfield with Enter/Return
	 */
	public void onEnterConfirm() {
		active(false);
	}


	public void type(final char c) {
		
		//if(isValid(c)) {
			
			clearSelection();
			
			if(sb.length() < charLimit) {
				
				int i = cursor <= sb.length() ? cursor : sb.length();
				sb.insert(i, c);
				
				setText(sb.toString());
				resetCursor(cursor + 1);
				
				if(liveupdate)
					sendMessage();
			}
		//}
	}
	
	
	public void insert(String str) {
		
		if(charProfile.matchesSingleLine(str)) {
			
			if(charProfile == INT) {
				int i = str.indexOf('.');
				if(i >= 0)
					str = str.substring(0, i);
			}
			
			clearSelection();
			
			if(sb.length() + str.length() > charLimit) {
				int s = Math.max(0, charLimit - sb.length());
				str = str.substring(0, s);
			}
			sel1 = min(sel1, sb.length());
			sb.insert(sel1, str);
			setText(sb.toString());
			
			resetCursor(sel1 + str.length());
		}
	}
	
	
	private void wordSelect() {
		
		if(sb.length() <= 0)
			return;
		
		int p = cursor;
		
		int pi = cursor >= sb.length() ? sb.length() - 1 : cursor;
		
		char c = sb.charAt(pi);
		boolean b = true;
		
		if(!Character.isLetterOrDigit(c)) {
			
			b = false;
			p++;
			if(p < sb.length()) {
				
				c = sb.charAt(p);
				if(Character.isLetterOrDigit(c)) {
					b = true;
				}
			}
		}
		if(b) {
			
			int s1 = p, s2 = p + 1;
			
			for (int i = p - 1; i >= 0; i--) 
				if (Character.isLetterOrDigit(sb.charAt(i))) 
					s1 = i;
				else 
					break;
			
			for (int i = p + 1; i < sb.length(); i++) {
				s2 = i;
				if(!Character.isLetterOrDigit(sb.charAt(i)))
					break;
				else if(i == sb.length() - 1)
					s2 = sb.length();
			}
			setSelection(s1, s2);
			
		}
	}
	
	
	public void mouseEvent(InputEvent e, UserInput input) {
		super.mouseEvent(e, input);
		
		if(active) {
			switch(e) {
			case KEY:
			case KEY_AUTOREPEAT:
				
				if(active) {
					char c = input.getLastChar();
					if(charProfile.matchesSingleLine(c)) {
						type(c);
					}else {
						command(input.getLastKeyCode(), input.ctrl, input.shift);
					}
				}
				break;
			default:
			}
			
		}
	}
	
	@SuppressWarnings("incomplete-switch")
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(disabled)
			return;
		
		switch(e) {
			case DOUBLECLICK -> {
				if(active)
					wordSelect();
				else{
					active(true);
					setCursor(sb.length());
					getDm().getGuiDomain().getPApplet().cursor(PConstants.TEXT);
					//PROC.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR)); TODO Cursor Procressing 3
				}
			}
			case PRESSED -> {
				if(active) {
					setCursor(getCharIndexFromPixelPos(input.getMouseX() - getX()));
					deselect();
				}
			}		
			case FOCUS_LOST -> active(false);
			case DRAG -> {
				if(active) 
					setSelection(getCharIndexFromPixelPos(input.getMouseX() - getX()));
			}
			case OVER -> {
				if(active && input.isCurrentTarget(self))
					self.getDm().getGuiDomain().getPApplet().cursor(PConstants.TEXT);
			}
			case OVER_STOP -> {
				if(input.isCurrentTarget(self) && active) 
					self.getDm().getGuiDomain().getPApplet().cursor(PConstants.ARROW);
			}
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj == self;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}


	class TCursor extends DisplayObject {
		
		int c = 0;
		boolean v = true;
		final float w = 1;
		
		public TCursor() {
			
			setFlag(CLICKABLE, false);
			setFlag(fixedToParent, true);
		}
		
		void resetBlink() {
			c = 0;
			v = true;
		}
		
		@Override public void render(PGraphics g) {
			
			c++;
			if(c > 20) {
				c = 0;
				v = !v;
			}
			if(v) {
				g.strokeWeight(w);
				g.stroke(getTextColor());
				g.line(getX(), getY() + 1, getX(), getY() + TextInputField.this.getHeight() - 2);
				g.noStroke();
			}
		}
	}
	
	
}
