package gui.text;

import gui.text.TextBlock.AlignMode;
import pr.DisplayObjectIF;
import processing.core.PFont;

public interface TextObject extends DisplayObjectIF {
	
	public void setText(String s);
	
	public String getText();
	
	public void setAlignMode(AlignMode m);
	
	public void setTextColor(int c);
	
	public void setFont(PFont f);
	
}
