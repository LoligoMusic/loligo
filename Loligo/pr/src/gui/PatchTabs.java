package gui;

import static java.lang.Math.min;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.IdentityHashMap;
import java.util.Map;
import constants.PatchEvent;
import constants.PatchEventListener;
import constants.PatchEventManager;
import gui.ListObject.ListElement;
import lombok.Getter;
import lombok.val;
import lombok.var;
import patch.DomainType;
import patch.LoligoPatch;
import pr.RootClass;

public class PatchTabs implements PatchEventListener {
	
	private static PatchTabs patchTabs;
	
	public static PatchTabs getPatchTabs() {
		if(patchTabs == null) {
			patchTabs = new PatchTabs();
			patchTabs.init();
		}
		return patchTabs;
	}
	
	@Getter
	private final ListObject list = new ListObject() {
		
		public void render(processing.core.PGraphics g) {};
		
		protected void arrangeHorizontal() {
			int w = getDm().getWidth() - (int) getX();
			int s = size();
			w -= (s + 1) * itemMargin;
			int wi = s > 0 ? min(w / s, 100) : 0;
			items.stream().forEach(it -> it.setWH(wi, it.getHeight()));
			
			super.arrangeHorizontal();
		};
		
		/*
		public ListObject createOverFlowList() {
			val overFlowList = new ListObject();
			overFlowList.setFlag(DisplayFlag.fixedToParent, true);
			overFlowList.setPos(getWidth(), 0);
			overFlowList.setColor(getColor());
			overFlowList.setWH(getWidth(), 0);
			overFlowList.itemMargin = itemMargin;
			overFlowList.padding = padding;
			overFlowList.top = top;
			overFlowList.bottom = bottom;
			
			addChild(overFlowList);
			
			return overFlowList;
		};
		*/
	
	};
	
	private final Map<LoligoPatch, ListElement> patchMap = new IdentityHashMap<>();
	
	public PatchTabs() {
		
		
		//list.setOverFlowSize(3);
	}
	
	/*
	public ListObject getList() {
		list = new ListObject() {
			@Override
			public void removedFromDisplay() {
				PatchEventManager.instance.removeListener(PatchTabs.this);
			}
		};
		list.setVertical(false);
		return list;
	}
	*/
	
	public void init() {
		list.setVertical(false);
		list.top = 0;
		list.bottom = 0;
		list.padding = 0;
		PatchEventManager.instance.addListener(this);
		reload();
	}
	
	public void reload() {
		list.clear();
		patchMap.clear();
		
		var b = RootClass.backstage();
		
		b.getPatches().stream()
			.map(d -> (LoligoPatch) d)	
			.forEachOrdered(this::addTab);
		
		openTab(b.getActivePatch());
	}
	
	public void addTab(LoligoPatch p) {
		/*
		Runnable r = () -> {
			var b = RootClass.backstage();
			Timer ti = Timer.createFrameDelay(p, tt -> b.loadPatch(p));
			ti.start();
		};
		*/
		var e = list.addListElement(p.getTitle(), () -> RootClass.backstage().loadPatch(p));
		e.closeOnClick = false;
		renameTab(p, e);
		patchMap.put(p, e);
		openTab(RootClass.backstage().getActivePatch());
	}
	
	public void renameTab(LoligoPatch p) {
		val e = patchMap.get(p);
		if(e != null) {
			renameTab(p, e);
		}
	}
	
	public void renameTab(LoligoPatch p, ListElement e) {
		val url = p.getLocation();
		if(url == null) {
			e.setMouseOverText(p.getTitle());
			e.getTextBox().setText(p.getTitle());
		}else {
			e.setMouseOverText(url.toExternalForm());
			String s = "";
			try {
				Path path = Paths.get(url.toURI());
				s = path.getFileName().toString();
			} catch (URISyntaxException e1) {
				e1.printStackTrace();
			}
			//String f = url.getFile();
			//String s = f.substring(f.lastIndexOf('/') + 1);
			e.getTextBox().setText(s);
		}
		
	}
	
	public void removeTab(LoligoPatch p) {
		
		var o = patchMap.remove(p);
		if(o == null)
			return;
		
		list.remove(o);
		
	}
	
	public void openTab(LoligoPatch p) {
		var op = patchMap.get(p);
		for(var o : list.items) {
			Button b = (Button) o;
			if(o == op) {
				b.setButtonColor(0xff404040);
				b.setMouseOverButtonColor(0xff4f4f4f);
			}else {
				b.setButtonColor(0xff111111);
				b.setMouseOverButtonColor(0xff222222);
			}
			b.updateColor();
		}
		
	}


	@Override
	public void onPatchEvent(PatchEvent e) {
		
		var p = e.patch();
		
		if(p.domainType() != DomainType.MAIN)
			return;
		
		switch(e.type()) {
		case PATCH_CREATED:
			addTab(p);
			break;
		case PATCH_REMOVED:
			removeTab(p);
			break;
		case FOCUS_GAINED:
			openTab(p);
			break;
		case PATCH_RENAMED:
			renameTab(p);
			break;
		default:
			break;
		}
		
		val dm = p.getDisplayManager();
		if(dm != null)
			dm.showMenuBar().update();
		
	}
	
	
	
	
}
