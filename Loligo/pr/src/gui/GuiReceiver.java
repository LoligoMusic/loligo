package gui;

public interface GuiReceiver {
	public void guiMessage(GuiSender<?> s);
	public void updateSender(GuiSender<?> s);
} 
