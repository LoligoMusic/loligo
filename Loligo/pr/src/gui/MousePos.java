package gui;

import pr.Positionable;
import pr.userinput.UserInput;

public record MousePos(UserInput input) implements Positionable {

	@Override
	public float getX() {
		return input.getMouseX();
	}

	@Override
	public float getY() {
		return input.getMouseY();
	}

	@Override
	public void setPos(float x, float y) {}

	@Override
	public Position getPosObject() {
		return new Position.PositionTarget(this);
	}

	@Override
	public void setPosObject(Position p) {
		
	}

}
