package gui;

import pr.DisplayContainerIF;

public class DoubleToStringAdapter implements GuiSender<Double> {

	private final GuiSender<String> stringSender;
	
	public DoubleToStringAdapter(GuiSender<String> stringSender) {
	
		this.stringSender = stringSender;
	}

	public Double getValue() {
		
		try{
			return Double.valueOf(stringSender.getValue());
		}catch(NumberFormatException e) {}
		
		return 0d;
	}

	@Override
	public void setValue(Double v) {
		stringSender.setValue(String.valueOf(v));
	}

	@Override
	public void update() {
		stringSender.update();
	}

	@Override
	public void disable(boolean b) {
		stringSender.disable(b);
	}
	@
	Override
	public DisplayContainerIF getDm() {
		return stringSender.getDm();
	}
	
	

}
 