package gui.modulmenu;

import static gui.text.TextBlock.AlignMode.LEFT;

import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayObjects;
import gui.ListObject;
import gui.ListObject.ListElement;
import gui.text.TextInputField;
import module.loading.NodeCategory;
import module.loading.NodeEntry;
import patch.Domain;
import pr.DisplayObject;
import pr.EventSubscriber;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import processing.core.PImage;
import util.Images;


public class NodeMenu implements EventSubscriber{
	
	ListObject nodeMenu;
	
	final Domain domain;
	
	
	private Collection<? extends NodeEntry> entries;
	
	
	public NodeMenu(final Domain domain) {

		this.domain = domain;
		
		domain.getDisplayManager().getInput().addInputEventListener(this);
		
	}
	
	
	public ListObject getListObject() {
		
		if(nodeMenu == null)
			nodeMenu = createNodeMenu();
		
		return nodeMenu;
	}
	
	
	private ListObject createNodeMenu() {
		
		EnumSet<NodeCategory> types = EnumSet.allOf(NodeCategory.class);
		
		ListObject mainList = setupListObject();
		
		createSearchbar(mainList);
		
		loadEntries();
		
		for (NodeCategory t : types) {
			
			List<NodeEntryTextBox> net = entries.stream()
			
				.filter (
					e 		 -> e.getType() == t && !e.isHidden()
				)
				.sorted (
					(e1, e2) -> e1.getName().compareTo(e2.getName())
				)
				.map (
					e 		 -> new NodeEntryTextBox(this, e)
				)
				.collect(Collectors.toList());
			
			if(net.size() > 0) {
				
				ListObject subList = setupListObject();
				
				for (NodeEntryTextBox ne : net) {
					
					addEntry(subList, ne);
				}
				
				mainList.addCascadeElement(formatTypeName(t.name()), subList);
			}
		}
		
		return mainList;
	}
	
	public Collection<? extends NodeEntry> loadEntries() {
		
		entries = entries == null ? domain.getNodeRegistry().loadAllEntries() : entries;
		return entries;
	}
	
	private String formatTypeName(String s) {
		
		return s.substring(0, 1) + s.substring(1).toLowerCase();
	}
	
	
	private ListObject setupListObject() {
		
		ListObject li = new ListObject();
		
		li.adjustSize(true);
		li.itemMargin = 0;
		li.setColor(0xff111111);
		li.top = 5;
		li.bottom = 2;
		li.setOverFlowSize(15);
		li.enableScrolling(true);
		
		return li;
	}
	
	private void createSearchbar(ListObject list) {
	
		SearchBar ti = new SearchBar();
		list.addCascadeElement(0, ti, ti.resultList);
		ti.setPos(-list.padding, 0);
		ti.setWH(list.getWidth() , ti.getHeight());
		
		//Spacer
		DisplayObject d = new DisplayObject();
		d.setColor(0x00ffffff);
		
		list.add(d);
		
		d.setFlag(DisplayFlag.RECT, true);
		d.setFlag(DisplayFlag.CLICKABLE, false);
		d.setWH(80, 5);
		
	}
	
	
	private class SearchBar extends TextInputField {
		
		final ListObject resultList;
		final PImage img;
		
		public SearchBar() {
			super(s -> {}, null);
			
			img = Images.loadPImage("/resources/search_13px.png");
			
			singleThread();
			resultList = setupListObject();
			bgBox = true;
			liveupdate(true);
			setAlignMode(LEFT);
			setOffsetX(img.width + 6);
			
			align();
			update();
		}
		
		@Override
		public void render(PGraphics g) {
		
			super.render(g);
			g.image(img, getX() + 3, getY() + 3);
		}
		
		@Override
		public void setValue(String text) {
			super.setValue(text);
			
			resultList.clear();
			String t = text.trim();
			 
			if(t.trim().length() > 0) {
				
				List<NodeEntryTextBox> li = matchname(t);
				if(li.isEmpty()) {
					
					DisplayObjects.removeFromDisplay(resultList);
					
				}else{
					
					for(NodeEntryTextBox ne : li)
						addEntry(resultList, ne);
					
				}
				
			}else {
				
				DisplayObjects.removeFromDisplay(resultList);
			}
		}
		
		@Override
		public void onEnterConfirm() {
		
			//super.onEnterConfirm();
			
			if(resultList.size() > 0) {
				resultList.get(0).mouseEventThis(InputEvent.CLICK, domain.getDisplayManager().getInput());
				
				ListElement t = (ListElement) resultList.get(0);
				t.action();
				
				DisplayObjects.removeFromDisplay(NodeMenu.this.nodeMenu);
			}
		}
		
		@Override
		public void type(final char c) {
			super.type(c);
			getParent().mouseEventThis(InputEvent.OVER, domain.getDisplayManager().getInput());
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			super.mouseEventThis(e, input);
			
			if(e == InputEvent.OVER || e == InputEvent.OVER_STOP)
				getParent().mouseEventThis(e, input);
			
		}
		
		
		@Override
		public void addedToDisplay() {
			active(true);
			getDm().getDomain().getDisplayManager().getInput().setFocusTarget(this);
		}
	};


	private void addEntry(ListObject list, NodeEntryTextBox ne) {
		
		Runnable r = () -> {
			ne.createInstance();
			DisplayObjects.removeFromDisplay(nodeMenu);
		};
		
		list.addListElement(ne.getText(), r);
	}
	
	public List<NodeEntryTextBox> matchname(String str) {
		
		final String s = str.toLowerCase();
		
		Predicate<NodeEntry> filt = e -> {
			String n = e.getName().toLowerCase();
			return !e.isHidden() && 
					(n.startsWith(s) || (s.length() >= 3 && n.contains(s)));
		};
		
		Comparator<NodeEntry> comp = (e1, e2) -> {
			
			String s1 = e1.getName(),
				   s2 = e2.getName();
			boolean b1 = s1.toLowerCase().startsWith(s),
					b2 = s2.toLowerCase().startsWith(s);
			
			int c = b1 && !b2 ? -1  :
					b2 && !b1 ? 1 :
								s1.compareTo(s2);
			return c;
		};
		
		return
		
			loadEntries().stream()
						 .filter(filt)
						 .sorted(comp)
						 .map(e -> new NodeEntryTextBox(this, e))
						 .collect(Collectors.toList());
	}
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(e == InputEvent.CLICK_RIGHT && 
				input.getOverTarget() == null && 
				domain.getDisplayManager().getInput() == input) {
			
			DisplayObject d = getListObject();
			d.setPos(input.getMouseX(), input.getMouseY());
			
			input.setRemovableTarget(d);
			DisplayObjects.holdInScreen(d);
		}
	}
	
}
