package gui.modulmenu;

import constants.InputEvent;
import gui.DisplayObjects;
import gui.text.TextBox;
import lombok.Getter;
import lombok.var;
import module.Module;
import module.Modules;
import module.loading.NodeEntry;
import pr.userinput.UserInput;

class NodeEntryTextBox extends TextBox {
	
	private final NodeMenu nodeMenu;
	@Getter
	private final NodeEntry entry;
	
	
	public NodeEntryTextBox(NodeMenu nodeMenu, NodeEntry entry) {
		
		super(entry.getName());
		
		
		//adaptSize();
		this.nodeMenu = nodeMenu;
		this.entry = entry;
		//bgBox = true;
	}
	
	
	public Module createInstance() {
		
		Module m = entry.createInstance();
		
		var dom = nodeMenu.domain;
		
		Modules.initializeModuleDefault(m, dom);
		
		m.processIO();
		
		UserInput in = dom.getDisplayManager().getInput();
		in.selection.clear();
		in.selection.add(m.getGui());
		m.getGui().setPos(in.getMouseX(), in.getMouseY());
		
		return m;
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(e == InputEvent.CLICK) {
			createInstance();
			DisplayObjects.removeFromDisplay(this.nodeMenu.nodeMenu);
		}else
		if(e == InputEvent.DRAG_START) {
			Module m = createInstance();
			UserInput ui = this.nodeMenu.domain.getDisplayManager().getInput();
			ui.injectDragTarget(m.getGui());
		}
	}
	
	
	@Override
	public String toString() {
	
		return "NodeEntry(" + entry + ")";
	}
}