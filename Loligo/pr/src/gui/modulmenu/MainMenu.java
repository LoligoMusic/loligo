package gui.modulmenu;

import java.util.ArrayList;
import java.util.List;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.Button;
import gui.ImageObject;
import gui.ListObject;
import patch.LoligoPatch;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import save.SaveJson;

public class MainMenu extends DisplayObject {
	
	public static void show(LoligoPatch patch) {
		
		MainMenu main = new MainMenu(patch);
		patch.getDisplayManager().addGuiObject(main);
	}
	
	
	//final private LoligoPatch patch;
	//final private UserInput input;
	
	final private DisplayObject loadButton, modulesButton;
	final private ListObject fileMenu;
	final private Button loadBtn, saveBtn, deleteBtn;//, examplesBtn, aboutBtn;
	
	
	public MainMenu(LoligoPatch patch) {
		
		//this.patch = patch;
		//this.input = patch.getDisplayManager().getInput();
		
		//super(false);
		setFlag(DisplayFlag.CLICKABLE, false);
		setColor(0xff2c2c2c);
		setStrokeColor(0xaaffffff);
		setFlag(DisplayFlag.RECT, true);
		setWH(120, 48);
		
		setPos(0, 0);
		
		//DisplayObject.setDimensions(30, 13);
		//DisplayObject.fill(PROC.color(90));
		
		fileMenu = new ListObject();
		fileMenu.adjustSize(true);
		
		loadButton = new DisplayObject() {
			@Override
			public void mouseEventThis(InputEvent e, UserInput input) {
				if(e == InputEvent.CLICK) {
					fileMenu.setPos(getX(), getY() + getHeight());
					patch.getDisplayManager().getInput().setRemovableTarget(fileMenu);
				}
			}
		};
		
		loadBtn = new Button(() -> SaveJson.runPatchLoadPrompt(patch), "Open");
		saveBtn = new Button(() -> SaveJson.runPatchSavePrompt(patch), "Save");
		deleteBtn = new Button(patch::reset, "New");
		
		/*
		examplesBtn = new TextBox("Examples", true) {
			@Override public void mouseClicked() {
				Save.loadExampleMenu();
			}};
		aboutBtn = new TextBox("About", true) {
			@Override public void mouseClicked() {
				UserInput.instance.setRemovableTarget(null);
				gui.SplashScreen.show();
			}};
		*/
		DisplayObject exitBtn = new Button(patch::killWindow, "Exit");
		
		List<DisplayObject> list = new ArrayList<>();
		list.add(deleteBtn);
		list.add(loadBtn);
		list.add(saveBtn);
		//list.add(aboutBtn);
		list.add(exitBtn);
		
		
		for(DisplayObjectIF d : list) {
			d.setFlag(DisplayFlag.CLICKABLE, true);
			((Button)d).setButtonColor(0x00ffffff);
			d.setWH(80, d.getHeight());
			d.setColor(0xFF333333);
		}
		
		
		fileMenu.setColor(0xFF333333);
		fileMenu.add(list);
		
		/*
		ListObject lo = new ListObject();
		lo.adjustSize(true);
		lo.setColor(0xFF333333);
		lo.add(Save.loadExampleMenu());
		fileMenu.addCascadeElement(3, "Examples", lo);
		*/
		
		modulesButton = new DisplayObject() {
			//@Override public void mouseClicked() {
				//PROC.menu.show(0, 0);
			//}
		};
		
		
		
		DisplayObject imgLoad = new ImageObject("/resources/file.png"), 
					  imgModules = new ImageObject("/resources/node.png");
		
		imgLoad.setFlag(DisplayFlag.fixedToParent, true);
		imgModules.setFlag(DisplayFlag.fixedToParent, true);
		imgLoad.setPos(1, 1);
		imgModules.setPos(1, 1);
		loadButton.addChild(imgLoad);
		modulesButton.addChild(imgModules);
		
		loadButton.setColor(modulesButton.setColor(0x00FFFFFF));
		loadButton.setFlag(DisplayFlag.RECT, true);
		modulesButton.setFlag(DisplayFlag.RECT, true);
		loadButton.setFlag(DisplayFlag.CLICKABLE, true);
		modulesButton.setFlag(DisplayFlag.CLICKABLE, true);
		//modulesButton.bgBox = loadButton.bgBox = true;
		
		loadButton.setWH(40, 40);
		modulesButton.setWH(40, 40);
		
		modulesButton.setFlag(DisplayFlag.fixedToParent, true);
		loadButton.setFlag(DisplayFlag.fixedToParent, true);
		loadButton.setPos(20, 4);
		modulesButton.setPos(loadButton.getX() + loadButton.getWidth() + 10, 4);
		addChild(loadButton, modulesButton);
	
		//PROC.display.modulContainer.addChild(this);
	}
	
	
}
