package gui;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public class SliderArray extends DisplayObject implements GuiSender<Double[]> {

	//private Float[] array;
	//private final GuiReceiver target;
	private final GuiSenderObject<Double[]> sender;
	private float top = 1 , bottom = 0;
	
	public SliderArray(final int length, final Consumer<Double[]> action, final Supplier<Double[]> updateAction) {
		
		setFlag(DisplayFlag.RECT, true);
		
		Double[] array = new Double[length];
		
		for (int i = 0; i < array.length; i++) 
			array[i] = 0d;
		
		sender = new GuiSenderObject<Double[]>(this, action, updateAction, array);
		
		setWH(200, 70);
		
		setColor(0xff111111);
		
	}
	
	public final void setLength(int length) {
		
		setValue(Arrays.copyOf(getValue(), length));
	}
	
	public int getLength() {
		
		return getValue().length;
	}
	
	public void setRange(final float top, final float bottom) {
		
		this.top = top;
		this.bottom = bottom;
	}
	
	@Override
	public void render(PGraphics g) {
		
		super.render(g);
		
		Double[] array = getValue();
		
		float w = (float) getWidth() / array.length; 
				
		g.fill(0xaaffffff);
		
		float r = top - bottom,
			  base = (top > 0 && bottom < 0) ||
				     (top < 0 && bottom > 0) ? top / r :
				      top > 0	? 			   1 : 0;
		
		float x = getX();
				      
		for (int i = 0; i < array.length; i++) {
			
			g.rect(
				x + w * i, 
				getY() + getHeight() * (base), 
				w - 1, 
				-(array[i].floatValue() - bottom) / r  * getHeight() + getHeight() * (1-base));
			
		}
		//dm.g.stroke(0xffff0000);
		//dm.g.line(getX(), getY() + height - height * base, getX() + width, getY() + height - height * base);
		//dm.g.noStroke();
		
	}
	
	private void mouseOp() {
		
		Double[] array = Arrays.copyOf(getValue(), getValue().length);
		
		int i = (int) ((array.length) * ((getDm().getInput().getMouseX() - getX()) / getWidth()));
		
		if(i >= array.length) {
			
			i = array.length - 1;
			
		}else if(i < 0) {
			
			i = 0;
		}
		
		double v = 1f - ((getDm().getInput().getMouseY() - getY()) / getHeight());
		v = Math.max(v, 0);
		v = Math.min(v, 1);
		
		v = v * (top - bottom) + bottom;
		
		array[i] = v;
		
		sender.message(array);
	}
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		update();
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == InputEvent.DRAG || e == InputEvent.PRESSED)
			mouseOp();
	}
	
	@Override
	public Double[] getValue() {
		
		return sender.getValue();
	}

	@Override
	public void setValue(Double[] v) {
		
		sender.setValue(v);
		//array = v;
	}
	
	@Override
	public void update() {
		
		sender.update();
	}
	
	/*
	public void copyValue(Float[] v) {
		
		for (int i = 0; i < v.length; i++) 
			if(i < array.length)
				array[i] = v[i];
	}
	*/
	
	@Override
	public void disable(boolean b) {
		
	}

	

}
