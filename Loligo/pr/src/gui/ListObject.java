package gui;

import static constants.DisplayFlag.mouseOver;
import static constants.DisplayFlag.onDisplay;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import constants.Timer;
import gui.selection.GuiType;
import lombok.Getter;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public class ListObject extends DisplayObject implements GuiSender<List<DisplayObjectIF>> {

	private static final float TEXT_MARGIN = 5;
	
	final private GuiSenderObject<List<DisplayObjectIF>> sender;
	//final private GuiReceiver target;
	protected List<DisplayObjectIF> items;
	protected List<CascadeElement> cascadeEls = new ArrayList<>();
	
	private boolean adjustSize = false, vertical = true;
	public int itemMargin = 3, padding = 5, top = 8, bottom = 8;
	private DisplayObjectIF lastAdded;
	private Timer timer;
	private CascadeElement cascadeElement;
	
	@Getter
	private ListObject overFlowList;
	
	private int startIndex, overflowSize = 30;
	private boolean enableScrolling;
	protected final ScrollElement scrollUp, scrollDown;
	
	
	public ListObject(final Consumer<List<DisplayObjectIF>> action, final Supplier<List<DisplayObjectIF>> updateAction, List<DisplayObjectIF> items) {
		
		this.items = items;
		setFlag(DisplayFlag.RECT, true);
		setFlag(DisplayFlag.CLICKABLE, false);
		
		
		//target = receiver;
		sender = new GuiSenderObject<List<DisplayObjectIF>>(this, action, updateAction, items);
		
		if(!items.isEmpty())
			lastAdded = items.get(items.size() - 1);
		
		scrollUp = new ScrollElement(true);
		scrollDown = new ScrollElement(false);
		
		arrangeItems();
		
	}
	
	
	public ListObject(final Consumer<List<DisplayObjectIF>> action, final Supplier<List<DisplayObjectIF>> updateAction) {
		this(action, updateAction, new ArrayList<DisplayObjectIF>());
	}
	
	public ListObject() {
		this(null, null);
	}
	
	public void setVertical(boolean v) {
		vertical = v;
		
	}
	
	public void endList() {
		arrangeItems();
	}
	
	public void enableScrolling(boolean enableScrolling) {
		
		this.enableScrolling = enableScrolling;
	} 


	private void arrangeItems() {
		if(vertical) 
			arrangeVertical();
		else
			arrangeHorizontal();
	}
	
	protected void arrangeVertical() {
		
		int h = top, maxW = 0;
		
		items.stream().forEach(d -> removeChild(d));
		
		int end = startIndex + overflowSize;
		
		if(end > items.size()) {
			
			end = items.size();
		}
			
		for (int i = startIndex; i < end; i++) {
			
			DisplayObjectIF d = items.get(i);
			addChild(d);
			d.setPos(padding, h);
			//d.rx = padding;
			//d.ry = h;
			h += d.getHeight() + itemMargin;
			
			if(d.getWidth() > maxW)
				maxW = d.getWidth();
		}
		//if(adjustSize) {
			
		setWH(maxW + 2 * padding, h + bottom);
		//}
		if(enableScrolling) {
			
			arrangeScrollButtons();
		}
	}
	
	private void arrangeScrollButtons() {
		
		if(startIndex > 0) {
			addChild(scrollUp);
		}else{
			removeChild(scrollUp);
		}
		
		if(startIndex + overflowSize < items.size()){
			addChild(scrollDown.setPos());
		}else{
			removeChild(scrollDown);
		}
	}
	
	protected void arrangeHorizontal() {
		
		int w = top, maxH = 0;
		
		items.stream().forEach(d -> removeChild(d));
		
		int end = startIndex + overflowSize;
		
		if(end > items.size()) {
			
			end = items.size();
		}
			
		for (int i = startIndex; i < end; i++) {
			
			DisplayObjectIF d = items.get(i);
			addChild(d);
			d.setPos(w, padding);
			w += d.getWidth() + itemMargin;
			
			if(d.getHeight() > maxH)
				maxH = d.getHeight();
		}
		//if(adjustSize) {
		setWH(w + 2 * padding, maxH + bottom);
		//}
	}
	
	public void scroll(int count) {
		startIndex += count;
		startIndex = Math.min(startIndex, items.size() - overflowSize);
		startIndex = Math.max(startIndex, 0);
		arrangeItems();
	}
	
	public DisplayObjectIF get(int i) {
		return items.get(i);
	}
	
	public DisplayObjectIF get(double d) {
		if(d < 0)
			d -= d * 2;
		if(d > 1)
			d -= (int)d;
		return items.size() > 0 ? items.get((int)((items.size() - 1) * d)) : null;
	}
	
	public int size() {
		return items.size();
	}
	
	public ListElement createListElement(String name, Runnable r) {
		return new ListElement(this, r::run, name);
	}
	
	public ListElement addListElement(String name, Runnable r) {
		var e = createListElement(name, r);
		add(e);
		return e;
	}
	
	public void add(DisplayObjectIF item) {
		
		if(size() < overflowSize) {
			
			add(size(), item);
		}else {
			
			if(overFlowList == null) {
				
				overFlowList = createOverFlowList();
			}
			
			overFlowList.add(item);
		}
	}
	
	public ListObject createOverFlowList() {
		overFlowList = new ListObject();
		overFlowList.setFlag(DisplayFlag.fixedToParent, true);
		overFlowList.setPos(getWidth(), 0);
		overFlowList.overflowSize = overflowSize;
		overFlowList.setColor(getColor());
		overFlowList.setWH(getWidth(), 0);
		overFlowList.itemMargin = itemMargin;
		overFlowList.padding = padding;
		overFlowList.top = top;
		overFlowList.bottom = bottom;
		
		addChild(overFlowList);
		
		return overFlowList;
	}
	

	public void add(int i, DisplayObjectIF item) {
		
		if(item == null)
			return;
		
		if(i >= items.size())
			items.add(item);
		else if(i >= 0)
			items.add(i, item);
		else
			items.add(0, item);
		lastAdded = item;
		
		
		item.setFlag(DisplayFlag.fixedToParent, true);
		//item.setFlag(DisplayFlag.clickable, true);
		
		if(adjustSize) {
			item.setWH(100, 20);
			//item.setColor(getColor());
		}
		
		//addChild(item);
		arrangeItems();
	}
	
	public void add(List<? extends DisplayObjectIF> newItems) {
		if(newItems != null) 
			for (DisplayObjectIF t : newItems) 
				add(t);
	}

	
	public boolean remove(DisplayObjectIF item) {
		
		boolean r = items.remove(item);
		cascadeEls.remove(item);
		
		if(r) {
			item.setFlag(DisplayFlag.fixedToParent, false);
			removeChild(item);
			arrangeItems();
		}else 
		if(overFlowList != null) {
			return overFlowList.remove(item);
		}
		return r;
	}
	
	public void adjustSize(boolean b) {
		adjustSize = b;
	}
	
	public List<DisplayObjectIF> list() {
		
		return items;
	}
	
	public boolean contains(DisplayObjectIF item) {
		
		return items.contains(item);
	}
	
	public void clear() {
		
		for (int i = items.size() - 1; i >= 0; i--) 
			remove(items.get(0));
	}

	
	@Override
	public List<DisplayObjectIF> getValue() {
		var l = new ArrayList<DisplayObjectIF>(items.size());
		l.addAll(items);
		if(overFlowList != null)
			l.addAll(overFlowList.getValue());
		return l;
	}

	@Override
	public void setValue(List<DisplayObjectIF> v) {
		clear();
		add(v);
	}
	
	
	@Override
	public void removedFromDisplay() {
		
		if(cascadeElement != null)
			cascadeElement.onListRemoved();
		
		for (CascadeElement ce : cascadeEls)
			DisplayObjects.removeFromDisplay(ce.listObject);
		
	}
	
	@Override
	public void update() {
		
		sender.update();
		arrangeItems();
	}
	
	@Override
	public void disable(boolean b) {
		
	}
	
	public DisplayObjectIF getLastAdded() {
		return lastAdded;
	}
	
	public void setOverFlowSize(int s) {
		
		overflowSize = s;
	}
	
	public int numOverFlowLists() {
		
		if(overFlowList != null) {
			return 1 + overFlowList.numOverFlowLists();
		}
		
		return 1;
	}
	
	public int totalWidth() {
		
		if(overFlowList != null) {
			return getWidth() + overFlowList.totalWidth();
		}
		
		return getWidth();
	}
	
	public void addCascadeElement(int i, String name, ListObject listObject) {
		/*
		TextBox t = new TextBox(name);
		//t.setWH(c.getWidth(), c.getHeight());
		t.bgBox = false;
		*/
		CascadeElement c = createCascadeElement(name, listObject);
		//addCascadeElement(i, c, listObject);
		cascadeEls.add(c);
		add(i, c);
	}
	
	public void addCascadeElement(int i, String name, Consumer<ListObject> onUpdateList) {
		var c = createCascadeElement(name, onUpdateList);
		cascadeEls.add(c);
		add(i, c);
	}
	
	public void addCascadeElement(int i, DisplayObject item, ListObject listObject) {
		
		var c = createCascadeElement("", listObject);
		
		item.setFlag(DisplayFlag.fixedToParent, true);
		c.addChild(item);
		item.setPos(TEXT_MARGIN, 0);
		
		cascadeEls.add(c);
		add(i, c);
	}
	
	public void addCascadeElement(String name, ListObject listObject) {
		
		addCascadeElement(items.size(), name, listObject);
	}
	
	public CascadeElement createCascadeElement(String name, ListObject listObject) {
		return new CascadeElement(name, listObject);
	}
	
	public CascadeElement createCascadeElement(String name, Consumer<ListObject> cons) {
		var ce = new CascadeElement(name);
		ce.onUpdateListObject(cons);
		return ce;
	}
	
	public DisplayObjectIF addSeparator() {
		var d = createSeparator();
		add(d);
		return d;
	}
	
	public DisplayObjectIF addSeparator(int idx) {
		var d = createSeparator();
		add(idx, d);
		return d;
	}
	
	public DisplayObjectIF createSeparator() {
		return new Separator();
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
	
		if(e == InputEvent.MOUSEWHEEL /*&& input == getDm().getInput()*/) { //TODO: getDm() fixen
			
			var d = input.getOverTarget();
			
			if(d == this || DisplayObjects.isChildOf(this, d)) {
				
				scroll(input.getWheelCount());
				
			}
		}
	}
	
	protected class CascadeElement extends ListElement {
		
		private Consumer<ListObject> updateListObject;
		
		public CascadeElement(String name) {
			this(name, new ListObject());
		}
		
		public CascadeElement(String name, ListObject listObject) {
			super(listObject, ()->{}, name);
			listObject.setColor(ListObject.this.getColor());
			setGuiType(GuiType.INPUT);
			closeOnClick = false;
		}

		@Override
		public void render(PGraphics g) {
			
			if(listObject.checkFlag(onDisplay)) {
				setColor(0x20ffffff);
				drawSelection(g);
			}else
			if(checkFlag(mouseOver)) {
				setColor(COLOR_MOUSEOVER);
				drawSelection(g);
			}
				
			if(vertical) {
				int x = (int)getX() + getWidth() - 7, 
					y = (int)getY() + getHeight() / 2;
				
				g.fill(0xffaaaaaa);
				g.triangle(x, y + 3, x, y - 3, x + 3, y);
			}
		}
		
		public void onUpdateListObject(Consumer<ListObject> cons) {
			updateListObject = cons;
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			if(e == InputEvent.OVER)
				addList();
		}
			
		
		public void addList() {
			
			if(updateListObject != null) {
				listObject.clear();
				updateListObject.accept(listObject);
			}
			
			if(listObject.size() > 0){
				
				int w = listObject.totalWidth();
				
				if(vertical) {
					listObject.setPos(
							ListObject.this.getX() + ListObject.this.getWidth() + 1, 
							getY() - 8);
					
					if(listObject.getX() + w + 1 > getDm().getGraphics().width) {
						
						listObject.setPos(
							ListObject.this.getX() - w - 1, 
							listObject.getY());
					}
					
				}else {
					listObject.setPos(
						getX(),
						ListObject.this.getY() + ListObject.this.getHeight() + 1
					);
				}
				
				showList();
				
				DisplayObjects.holdInScreen(listObject); // for vertical bounds
				
			}
			
			for (CascadeElement ce : cascadeEls) {
				if(ce != this && ce.listObject.checkFlag(DisplayFlag.onDisplay))
					DisplayObjects.removeFromDisplay(ce.listObject);
			}
			
			if(timer != null)
				timer.reset();
			
			//if(!listObject.checkFlag(DisplayFlag.onDisplay))
			//	PROC.display.input.setRemovableTarget(listObject, ListObject.this);

		}
		
		public void showList() {
			
			ListObject.this.addChild(listObject);
		}
		
		public void overStop() {
			
			 if(timer != null) {
				 
				 timer.reset();
			 }else {
				 timer = new Timer(getDm().getGuiDomain(), 400) {
					
					@Override 
					public void timerEvent() {
						
						UserInput in = getDm().getInput();
						
						if(in.getOverTarget() == null || DisplayObjects.getParentDepth(in.getOverTarget(), listObject) < 0)
							ListObject.this.removeChild(listObject);
							//PROC.display.input.removeRemovableTarget(listObject);
					}
				};
				
				timer.start();
			}
		}
		
		public void onListRemoved() {
			
		}

	}
	
	public class ListElement extends Button {
		
		final int COLOR_MOUSEOVER = 0x11ffffff;
		boolean closeOnClick = true;
		private BooleanSupplier enableCondition; 
		protected final ListObject listObject;
		
		public ListElement(ListObject listObject, Runnable action, String name) {
			super(action, name);
			this.listObject = listObject;
			//listObject.setColor(ListObject.this.getColor());
			setFlag(DisplayFlag.CLICKABLE, true);
			setFlag(DisplayFlag.DRAGABLE, false);
			setGuiType(GuiType.INPUT);
			setFlag(DisplayFlag.RECT, true);
			setWH(80, 20);
			setColor(COLOR_MOUSEOVER);
			if(getTextBox() != null) {
				getTextBox().setOffsetX(5);
				getTextBox().autoFit = true;
			}
			setButtonColor(0x00ffffff);
			setMouseOverButtonColor(COLOR_MOUSEOVER);
			setDisabledColor(0x00ffffff);
			
		}
		
		@Override
		public void action() {
			super.action();
			if(closeOnClick) {
				DisplayObjects.removeFromDisplay(listObject);
			}
		}
		
		@Override
		public void render(PGraphics g) {
			super.render(g);
			if(checkFlag(DisplayFlag.mouseOver)) {
				drawSelection(g);
			}
		}

		protected void drawSelection(PGraphics g) {
			g.fill(getColor());
			g.rect(getX(), getY(), getWidth(), getHeight());
		}
		
		public ListElement enableIf(BooleanSupplier sup) {
			enableCondition = sup;
			return this;
		}
		
		@Override
		public void addedToDisplay() {
			super.addedToDisplay();
			
			if(enableCondition != null) {
				disable(!enableCondition.getAsBoolean());
			}
		}
	}

	
	private class ScrollElement extends DisplayObject{
		
		boolean up;
		
		public ScrollElement(boolean up) {
			
			this.up = up;
			setFlag(DisplayFlag.fixedToParent, true);
			setFlag(DisplayFlag.CLICKABLE, true);
			setFlag(DisplayFlag.RECT, true);
			setWH(80, 20);
		}
		
		@Override
		public void render(PGraphics g) {
		
			super.render(g);
			
			int x = (int)getX() + getWidth() / 2, y = (int)getY() + 10 + (up ? 3 : -3);
			g.fill(0xffaaaaaa);
			g.triangle(x - 3, y, x + 3, y, x, y + (up ? -3 : 3));
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			if(e == InputEvent.PRESSED)
				scroll(up ? -1 : 1);
		}
		
		
		ScrollElement setPos() {
			
			setPos(0, ListObject.this.getHeight() + 10);
			return this;
		}
	}
	
	public class Separator extends DisplayObject {
	
		private int separatorWidth = 0;
		
		public Separator() {
			setFlag(DisplayFlag.CLICKABLE, false);
			setWH(1, 1);
		}	
	
		public Separator setSeparatorWidth(int w) {
			separatorWidth = w;
			if(ListObject.this.vertical) {
				setWH(1, w);
			}else {
				setWH(w, 1);
			}
			return this;
		}
		
		@Override
		public void render(PGraphics g) {
			g.pushStyle();
			g.noFill();
			g.stroke(0xff444444);
			g.strokeWeight(1);
			float x1 = getX(),
				  y1 = getY();
			float x2, y2;
			
			int w = separatorWidth / 2;
			
			if(vertical) {
				
				x2 = x1 + ListObject.this.getWidth() - padding * 2;
				y2 = y1 = y1 + w;
			}else{
				x2 = x1 = x1 + w;
				y2 = y1 + ListObject.this.getHeight() - padding * 2;
			}
			
			g.line(x1, y1, x2, y2);
			g.popStyle();
		}
	}
	
	
}
