package gui;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static util.Notes.pitchNamesSharp;
import static util.Notes.toNoteName;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.text.Fonts;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import util.Notes;

public class NoteSequencer {
	
	@RequiredArgsConstructor
	private class Note{
		final int pos, value;
	}
	
	private final List<Note> notes = new ArrayList<>();
	
	
	public void addNote(int pos, int value) {
		var n = new Note(pos, value);
		int i = posIndex(pos);
		notes.add(i, n);
	}
	
	public void removeNote(int pos) {
		var n = noteAt(pos);
		if(n != null) {
			notes.remove(n);
		}
	}
	
	public Note noteAt(int pos) {
		int s = notes.size();
		for (int i = 0; i < s; i++) {
			var n = notes.get(i); 
			if(n.pos == pos)
				return n;
		}
		return null;
	}
	
	private int posIndex(int pos) {
		int s = notes.size();
		for (int i = 0; i < s; i++) 
			if(notes.get(i).pos <= pos)
				return i;
		return s;
	}
	
	public void forEachInRange(int p1, int p2, Consumer<Note> cons) {
		
		for (Note n : notes) {
			int p = n.pos;
			if(p >= p1 && p <= p2) {
				cons.accept(n);
			}
		}
	}
	
	
	public static class NodeSequencerGui extends DisplayObject implements Scrollable{
		
		private final NoteSequencer sequencer;
		
		@Setter@Getter
		private int xCellsMax = 200, yCellsMax = 88;
		private int xOffset, yOffset, xOffsetCell, yOffsetCell;
		private int numCellsX, numCellsY, cellWidth = 20, cellHeight = 15, marginLeft = 30, marginRight = 4, marginBottom = 4;
		private int viewWidth, viewHeight, xMax, yMax;
		
		private final ScrollBar.Horizontal scrollH;
		private final ScrollBar.Vertical scrollV;
		
		
		public NodeSequencerGui(NoteSequencer sequencer) {
			
			this.sequencer = sequencer;
			setFlag(DisplayFlag.RECT, true);
			
			scrollH = new ScrollBar.Horizontal(this);
			scrollV = new ScrollBar.Vertical(this);
			addChild(scrollH);
			addChild(scrollV);
			
			setWH(300, 150);
		}
		
		@Override
		public void setWH(int width, int height) {
			super.setWH(width, height);
			
			update();
			
			scrollH.setPos(marginLeft, height - marginBottom);
			scrollV.setPos(width - marginRight, 0);
			
			scrollH.setWH(width - marginLeft - marginRight, marginBottom);
			scrollV.setWH(marginRight, height - marginBottom);
		}
		
		public void update() {
			int w = getWidth(), h = getHeight();
			viewWidth = w - marginLeft - marginRight;
			viewHeight = h - marginBottom;
			numCellsX = viewWidth / cellWidth;
			numCellsY = viewHeight / cellHeight;
			xMax = xCellsMax * cellWidth; 
			yMax = yCellsMax * cellHeight;
			setXOffset(xOffset);
			setYOffset(yOffset);
			scrollH.update();
			scrollV.update();
		}
		
		public void incrementXOffset(int xp) {
			int xo = xOffset + xp;
			setXOffset(xo);
		}
		
		public void setXOffset(int xo) {
			xOffset = max(cellWidth * (numCellsX - xCellsMax), min(0, xo));
			xOffsetCell = -xOffset / cellWidth;
			scrollH.update();
		}
		
		public void incrementYOffset(int yp) {
			int yo = yOffset + yp;
			setYOffset(yo);
			
		}
		
		public void setYOffset(int yo) {
			yOffset = max(0, min((yCellsMax) * cellHeight - marginBottom, yo));
			yOffsetCell = -yOffset / cellHeight;
			scrollV.update();
		}
		
		public void incrementZoom(int z) {
			
			int ch = cellHeight;
			cellHeight += z; 
			cellHeight = max(4, cellHeight);
			int v2 = viewHeight - (int) (getDm().getInput().getMouseY() - getY() - marginBottom); 
			yOffset = (int) (((float) cellHeight / ch) * (yOffset + v2)) - v2;
			
			int cw = cellWidth;
			cellWidth += z; 
			cellWidth = max(4, cellWidth);
			int w2 = (int) (getDm().getInput().getMouseX() - getX() - marginLeft); 
			xOffset = (int) (((float) cellWidth / cw) * (xOffset - w2)) + w2;
			
			update();
		}
		
		
		@Override
		public void render(PGraphics g) {
			
			g.fill(0x6059aaff);
			g.rect(getX(), getY(), getWidth(), getHeight());
			
			g.pushStyle();
			g.clip(getX(), getY(), getWidth(), getHeight());
			
			renderTable(g);
			renderNoteBar(g);
			
			g.fill(0x88ffffff);
			
			g.noClip();
			g.popStyle();
		
		}

		private void renderTable(PGraphics g) {
			
			int x = (int) getX() + marginLeft;
			int w = getWidth() - marginLeft, y1 = (int) getY();
			int y2 = y1 + yOffset + viewHeight ;
			
			int octHeight = 12 * cellHeight, octHeight2 = octHeight * 2;
			int numOct = 1 + viewHeight / octHeight;
			
			g.fill(0x20000000);
			
			for (int i = 0; i <= numOct; i ++) {
				int ym = y1 + viewHeight + 8 * cellHeight - (-yOffset % octHeight2) - (i+1) * octHeight2;
				g.rect(x, ym, w, octHeight);
			}
			
			g.stroke(0xaf000000);
			int beat = 4, beatWidth = beat * cellWidth, numBeats = w / beatWidth;
		
			for (int i = 0; i <= numBeats; i++) {
				int xm = x + beatWidth + (xOffset % (beatWidth)) + (i * beatWidth);
				g.line(xm, y1, xm, y1 + viewHeight);
			}
			
			g.stroke(0x49000000);
			g.strokeWeight(1);
			
			//g.fill(0x12000000);
			//g.noStroke();
			
			int from2 = yOffsetCell - numCellsY - 1; 
			int to2 = from2 + numCellsY + 1;
			
			for (int i = from2; i < to2; i ++) {
				int ym = y2 + cellHeight * i;
				//g.rect(x, ym, w, cellHeight);
				g.line(x, ym, x + w, ym);
			}
			
			int from = xOffsetCell, 
				to = from + numCellsX + 1;
			
			for (int i = from; i < to; i ++) {
				int xm = x + xOffset + cellWidth * i;
				//g.rect(xm, y1, cellWidth, h);
				g.line(xm, y1 + viewHeight, xm, y1);
			}
			
			
			
			g.fill(0xffffffff);
			Consumer<Note> cons = n -> {
				float xa = x + xOffset + (n.pos) * cellWidth,
					  ya = y2 - (n.value + 1) * cellHeight;
					
				g.rect(xa, ya, cellWidth, cellHeight);
			};
			
			sequencer.forEachInRange(xOffsetCell, xOffsetCell + numCellsX + 1, cons);
			
			g.rect(x, y1 + viewHeight, w, marginBottom);
			
			scrollH.setScrollParams(-xOffset, viewWidth, xMax);
			scrollV.setScrollParams(yOffset , viewHeight, yMax + viewHeight);
		}
		

		private void renderNoteBar(PGraphics g) {
			
			int x1 = (int) getX(), x2 = x1 + marginLeft, y = (int) getY(), h = getHeight();
			int yo = y + yOffset + viewHeight - marginBottom;
			
			g.textFont(Fonts.DEFAULT_10);
			g.fill(0xff525252);
			g.noStroke();
			g.rect(x1, y, marginLeft, h);
			
			g.fill(0xdfffffff);
			
			int from2 = yOffsetCell - numCellsY - 2; 
			int to2 = from2 + numCellsY + 3;
			
			for (int i = from2; i < to2; i ++) {
				int yi = yo + i * cellHeight;
				int ni = Notes.A0 - (i + 1);
				//ni = max(Notes.A0, ni);
				String p = toNoteName(ni, pitchNamesSharp);
				g.text(p, getX() + 2, yi + cellHeight / 2);
			}
			
			g.strokeWeight(1);
			g.stroke(0x4f000000);
			g.line(x2, y, x2, y + h);
			
			g.noStroke();
			g.fill(0xff29292f);
			g.rect(x1, y + h - marginBottom, marginLeft, marginBottom);
		}
		
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			
			switch(e) {
			case CLICK -> {
				var in = getDm().getInput();
				int mx = in.getMouseX();
				if(mx - getX() > marginLeft) {
					
					int x = cellAtX(in.getMouseX());
					int y = cellAtY(in.getMouseY());
					
					sequencer.addNote(x, y);
				}
			}
			case CLICK_RIGHT -> {
				int x = cellAtX(input.getMouseX());
				//int y = cellAtY(input.getMouseY());
				sequencer.removeNote(x);
			}
			case MOUSEWHEEL -> {
				incrementZoom(input.getWheelCount());
			}
			case DRAG -> {
				var in = getDm().getInput();
				incrementYOffset(in.getMouseDeltaY());
				incrementXOffset(in.getMouseDeltaX());
			}
			default -> {}
			}
		}
		
		
		private int cellAtX(int xa) {
			int x = xa  - ((int) getX() + marginLeft + xOffset);
			return x / cellWidth;
		}
		
		private int cellAtY(int ya) {
			int y = getHeight() - (ya - (int) getY()) + yOffset - marginBottom;
			return  y / cellHeight;
		}

		
		@Override
		public void setScrollPosX(float pos) {
			int xo = (int) ((pos - 1) * xMax);
			setXOffset(xo);
		}

		@Override
		public void setScrollPosY(float pos) {
			int yo = (int) (pos * yMax);
			setYOffset(yo);
		}

		@Override
		public float getScrollPosX() {
			return (float) -xOffset / xMax;
		}

		@Override
		public float getScrollPosY() {
			return (float) yOffset / yMax;
		}

		@Override
		public float getViewportLengthX() {
			return (float) viewWidth / xMax;
		}

		@Override
		public float getViewportLengthY() {
			return (float) viewHeight / yMax;
		}

		@Override
		public float getTotalLengthX() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public float getTotalLengthY() {
			// TODO Auto-generated method stub
			return 0;
		}
		
	}
}
