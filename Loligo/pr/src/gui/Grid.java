package gui;

import constants.InputEvent;
import gui.SnapBehaviour.SnapToGrid;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayManagerIF;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.EventSubscriber;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PShape;

public class Grid implements EventSubscriber {
	
	final private DisplayManagerIF dm;
	@Getter@Setter
	private int gridWidth = 50;
	private int screenWidth, screenHeight;
	private Gui gui;
	
	public Grid(DisplayManagerIF dm) {
	
		this.dm = dm;
		screenWidth = dm.getWidth();
		screenHeight = dm.getHeight();
		
		dm.getInput().addInputEventListener(this);
		gui = new Gui();
		dm.addGuiObject(gui);
	}
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		DisplayObjectIF dt = input.getDragTarget();
		if(e == InputEvent.DRAG_START && input == dm.getInput() && dt.getGuiType().isModule()) {
			SnapToGrid snap = new SnapToGrid(dt);
			snap.setSnapDistance(15);
			snap.setAutoRemove(true);
			input.addInputEventListener(snap); 
		}
	}
	
	public void removeGrid() {
		DisplayObjects.removeFromDisplay(gui);
		dm.getInput().removeInputEventListener(this);
	}
	
	private class Gui extends DisplayObject {
		
		PShape shape;
		
		public Gui() {
			
			setColor(0x15ffffff);
			
			lineGrid();
		}
		
		
		@SuppressWarnings("unused")
		void crossGrid() {
			
			shape = dm.getGraphics().createShape();
			
			shape.beginShape(PConstants.LINES);
			shape.noFill();
			shape.strokeWeight(1f);
			shape.stroke(getColor());
			
			int h = screenHeight / gridWidth,
				w = screenWidth / gridWidth;
			float start = 0f, end = 3f;
			
			for (int i = 0; i <= w; i++) {
				float x = i* gridWidth, y;
				for (int j = 0; j <= h; j++) {
					y = j * gridWidth;
					shape.vertex(x + start, y);
					shape.vertex(x + end  , y);
					shape.vertex(x - start, y);
					shape.vertex(x - end  , y);
					shape.vertex(x, y + start);
					shape.vertex(x, y + end);
					shape.vertex(x, y - start);
					shape.vertex(x, y - end);
				}
			}
			
			shape.endShape();
		}
		
		void lineGrid() {
			
			shape = dm.getGraphics().createShape();
			
			shape.beginShape(PConstants.LINES);
			shape.noFill();
			shape.strokeWeight(1f);
			shape.stroke(getColor());
			
			int h = screenHeight / gridWidth;
			
			for (int i = 1; i < h; i++) {
				int wi = i * gridWidth;
				shape.vertex(0, wi);
				shape.vertex(screenWidth, wi);
			}
			int w = screenWidth / gridWidth;
			for (int i = 1; i < w; i++) {
				int wi = i * gridWidth;
				shape.vertex(wi, 0);
				shape.vertex(wi, screenHeight);
			}
			
			shape.endShape();
		}
		
		
		@Override
		public void render(PGraphics g) {
			g.shape(shape);
		}
		
	}

}
