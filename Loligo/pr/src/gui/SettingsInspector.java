package gui;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer.Info;

import gui.nodeinspector.InspectorGuiBox;
import gui.text.Fonts;
import gui.text.NumberInputField;
import gui.text.TextBox;
import gui.text.TextInputField;
import lombok.val;
import lombok.var;
import module.inout.NumericType.SubType;
import net.beadsproject.beads.core.io.JavaSoundAudioIO;
import patch.LoligoPatch;
import pr.RootClass;
import save.Settings;

public class SettingsInspector extends InspectorGuiBox {

	public static SettingsInspector create(LoligoPatch p) {
		val ins = new SettingsInspector();
		ins.init();
		p.getDisplayManager().getInput().setRemovableTarget(ins);
		return ins;
	}
	
	@Override
	public void doHeader() {
		
		barHeight = 4;
		
		TextBox na = createLabel("SETTINGS");
		na.setFont(Fonts.DEFAULT_DEMI_14);
		na.textSize = 14;
		na.setTextColor(0xff777777);
		
		setTop(1);
		addRow(na);
		setTop(4);
	}
	
	@Override
	public void doBody() {
		
		createSection("Graphics");
		
		val proc = RootClass.mainProc();
		val fps = new NumberInputField(
			f -> proc.setFrameRate(f.intValue()), 
			() -> (double) proc.getFps()
		);
		fps.setRange(0, 240);
		fps.setNumericType(SubType.INTEGER);
		addLabeledEntry("Frame rate", fps);
		
		createSection("Audio");
		
		val st = Arrays.stream(AudioSystem.getMixerInfo())
			.map(Info::getName)
			.toArray(String[]::new);
		
		var dma = new DropDownMenu[1];
		
		Consumer<Integer> cons = i -> {
			val ios = new JavaSoundAudioIO();
			ios.selectMixer(i);
			RootClass.audioManager().initAudio(ios);
			//RootClass.backstage().reloadSession();
			Settings.retain();
			checkAudioDevice(dma[0]);
		};
		
		Supplier<Integer> sup = () -> {
			val m = RootClass.audioManager().getMixer().getMixerInfo();
			val n = m.getName();
			return Arrays.asList(st).indexOf(n);
		};
		
		var dm = new DropDownMenu(cons, sup, st);
		dma[0] = dm;
		checkAudioDevice(dm);
		
		addLabeledEntry("Audiodevice", dm);
	}
	
	private void checkAudioDevice(DropDownMenu dm) {
		boolean b = RootClass.audioManager().isHasValidSoundDevice();
		dm.setColor(b ? TextInputField.COLOR_ACTIVE : 0xffff6666);
	}
}
