package gui.path;

import gui.Position;
import pr.Positionable;

public class PositionableObject implements Positionable {
	
	
	protected Positionable pos;
	
	public PositionableObject() {
		this(new Position());
	}
	
	public PositionableObject(Positionable p) {
		pos = p;
	}
	
	@Override
	public float getX() {
		return pos.getX();
	}

	@Override
	public float getY() {
		return pos.getY();
	}

	@Override
	public void setPos(float x, float y) {
		pos.setPos(x, y);
	}

	@Override
	public Position getPosObject() {
		if(pos instanceof Position)
			return (Position) pos;
		else
			return new Position(getX(), getY()) {
				@Override public float getX() {
					return PositionableObject.this.getX();
				}
				
				@Override public float getY() {
					return PositionableObject.this.getY();
				}
				
				@Override public void setPos(float x, float y) {
					PositionableObject.this.setPos(x, y);
				}
		};
	}

	@Override
	public void setPosObject(Position p) {
		pos = p;
	}

}
