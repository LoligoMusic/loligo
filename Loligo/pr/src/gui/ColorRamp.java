package gui;

import static constants.InputEvent.DOUBLECLICK;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

import org.python.google.common.primitives.Doubles;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.selection.GuiType;
import lombok.AllArgsConstructor;
import pr.DisplayContainerIF;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.RootClass;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PImage;
import save.ColorJson;
import util.Colors;

public class ColorRamp implements GuiSender<Integer> {
	
	private final ArrayList<Handle> handles = new ArrayList<>();
	private boolean changed = false;
	private double position;
	private int colorValue;
	private DisplayObjectIF gui;
	private ColorRampGUI crg;
	

	public void reset() {
		handles.clear();
		addHandle(0).colorValue = Colors.BLACK;
		addHandle(1).colorValue = Colors.WHITE;
	}
	
	
	public DisplayObject createGUI() {
		
		crg = new ColorRampGUI();
		crg.gradient();
		return crg;
	}
	
	
	public Handle addHandle(final double p) {
		
		Handle handle = null;
		
		for (int i = 0; i < handles.size(); i++) {
			var h = handles.get(i);
			if(p < h.pos) {
				handle = new Handle(p);
				handles.add(i, handle);
				break;
			}
		}
		if(handle == null) {
			handle = new Handle(p);
			handles.add(handle);
		}
		
		if(crg != null) {
			
			handle.createGUI(crg);
			crg.gradient();
		}
		
		return handle;
	}
	
	public void removeHandle(Handle h) {
		if(handles.size() > 1) {
			handles.remove(h);
			if(crg != null)
				crg.removeChild(h.gui);
			crg.gradient();
		}
	}
	
	public int getColorAt(double p) {
		position = p;
		colorValue = getColorAt2(p);
		return colorValue;
	}
	
	
	private int getColorAt2(double p) {
		p = Doubles.constrainToRange(p, 0, 1);
		
		Handle h2 = null;
		
		for (int i = 0; i < handles.size(); i++) {
			h2 = handles.get(i);
			if(p <= h2.pos) {
				if(i == 0)
					return h2.colorValue;
				var h1 = handles.get(i - 1);
				p = (p - h1.pos) / (h2.pos - h1.pos);
				return Colors.lerp(h1.colorValue, h2.colorValue, (float) p);
			}
		}
		
		return h2 == null ? Colors.BLACK : h2.colorValue;
 	}
	
	
	public boolean changed() {
		if(changed) {
			changed = false;
			return true;
		}else
			return false;
	}
	
	@Override
	public DisplayContainerIF getDm() {
		return gui.getDm();
	}
	
	@Override
	public Integer getValue() {
		return colorValue;
	}
	
	@Override
	public void setValue(Integer v) {}
	
	@Override
	public void update() {
		
		if(crg != null)
			for (Handle h : handles) 
				h.gui.update();
		
	}
	
	@Override
	public void disable(boolean b) {}
	
	
	public Map.Entry<double[], int[]> toMap() {
		var positions = new double[handles.size()];
		var colors = new int[handles.size()];
		for (int i = 0; i < colors.length; i++) {
			var h = handles.get(i);
			positions[i] = h.pos;
			colors[i] = h.colorValue;
		}
		var e = new AbstractMap.SimpleEntry<>(positions, colors);
		return e;
	}
	
	
	public void fromMap(double[] positions, int[] colors) {
		
		for (int i = 0; i < colors.length; i++) {
			addHandle(positions[i]).colorValue = colors[i];
		}
		
		update();
	}
	
	
	@AllArgsConstructor
	public class Point {
		public double pos;
		@JsonSerialize(using = ColorJson.Serializer.class)
		@JsonDeserialize(using = ColorJson.Deserializer.class)
		public int color;
	}
	
	
	
	private class ColorRampGUI extends DisplayObject {
		
		private PImage img;
		
		public ColorRampGUI() {
			
			
			setFlag(DisplayFlag.RECT, true);
			
			img = createImage();
			
			//setFlag(DisplayFlag.Subscriber, true);
			
			for (Handle h : handles) {
				//if(h.gui == null) {
					h.createGUI(this);
				//}
				//addChild(h.gui);
			}
			
			gradient();
			
		}
		
		private PImage createImage() {
			return RootClass.mainProc().createImage(getWidth(), getHeight(), PConstants.ARGB);
		}
		
		final void gradient() {
			
			for (int i = 0; i < img.width; i++) {
			
				int c = getColorAt2((double) i / img.width);
				
				for (int j = 0; j < img.height; j++) 
					img.pixels[i + getWidth() * j] = c;
			}
			img.updatePixels();
			
			colorValue = getColorAt2(position);
			
			changed = true;
		}
		
		@Override
		public void setWH(int width, int height) {
		
			super.setWH(width, height);
			
			img = createImage();
			gradient();
			
			for(Handle h : handles) {
				
				h.gui.update();
				
			}
		}
		
		@Override
		public void render(PGraphics g) {
			
			g.image(img, getX(), getY());
			g.noStroke();
			g.fill(colorValue);
			g.ellipse(getX() + (float)position * getWidth(), getY() + getHeight(), 6, 6);
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			if(e == DOUBLECLICK) {
				double p = (input.getMouseX() - getX()) / getWidth();
				int c = getColorAt2(p);
				var h= addHandle(p);
				h.colorValue = c;
				gradient();
				input.injectDragTarget(h.gui);
			}
		}
		
	}
	
	
	
	class Handle {
		
		int colorValue = 0xFF;
		double pos;
		transient HandleGUI gui;
		
		public Handle(double p) {
			pos = p;
		}
		
		public void setPoint(Point p) {
			colorValue = p.color;
			pos = p.pos;
		}
		
		public void createGUI(ColorRampGUI crg) {
			gui = new HandleGUI(crg, this);
		}
		
		public void move(double pos) {
			
			int index = handles.indexOf(this);
			
			if(pos < 0)
				pos = 0;
			else if(pos > 1)
				pos = 1;
			
			this.pos = pos;
			
			if(index > 0) 
				if(pos < handles.get(index - 1).pos) {
					handles.remove(index);
					handles.add(index - 1, this);
				}
			
			if(index < handles.size() - 1) 
				if(pos > handles.get(index + 1).pos) {
					handles.remove(index);//rx = h.rx;
					handles.add(index + 1, this);
				}
			
			if(crg != null) {
				gui.update();
				crg.gradient();
			}
			
		}
		
		public void setColor(int i) {
			
			colorValue = i;
			if(crg != null)
				crg.gradient();
		}
		
		public int getColor() {
			return colorValue;
		}
		
	}
	
	
	class HandleGUI extends DisplayObject {
		
		private ColorPicker picker;
		Handle h;
		final ColorRampGUI crg;
		
		public HandleGUI(ColorRampGUI crg, Handle h) {
			this.h = h;
			this.crg = crg;
			setWH(8, 8);
			
			setFlag(DisplayFlag.DRAGABLE, true);
			setGuiType(GuiType.DRAGABLE_INPUT);
			setColor(Colors.grey(50));
			setFlag(DisplayFlag.fixedToParent, true);
			//setPos(crg.width * (float)h.pos, 0);
			crg.addChild(this);
			setPosObject(new Position.Bounds(getPosObject(), crg.getWidth(), 0));
			update();
		}
		
		
		@Override public void render(PGraphics g) {
			g.strokeWeight(2);
			g.stroke(getColor());
			g.line(getX(), getY(), getX(), getY() + crg.getHeight());
			g.strokeWeight(1);
			g.fill(h.colorValue);
			g.ellipse(getX(), getY(), getWidth(), getHeight());
		}
		
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
		
			switch(e) {
			case CLICK_RIGHT -> removeHandle(h);
			case CLICK -> {
				if(picker == null)
					picker = new ColorPicker(h::setColor, h::getColor, h.getColor());
				picker.update();
				picker.setPos(this).add(0, crg.getHeight() + 5);
				getDm().getInput().setRemovableTarget(picker, crg);
			}
			case DRAG -> h.move(getPosObject().x / crg.getWidth());
			default -> {}
			}
		}
		
		@Override
		public void addedToDisplay() {
			super.addedToDisplay();
			
		}

		public void update() {
			
			Position.Bounds b = (Position.Bounds) getPosObject();
			b.setBounds(crg.getWidth(), 0);
			setPos(crg.getWidth() * (float) h.pos, 0);
		}
	}


	

	
}
