package gui;

import static java.util.Arrays.stream;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import gui.nodeinspector.NodeInspector;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import module.Module;
import module.ModuleType;
import patch.Domain;
import pr.DisplayContainerIF;
import pr.RootClass;

public class NodeSelector implements GuiSender<Module> {

	private final GuiSenderObject<Module> sender;
	
	@Getter
	private final DropDownMenu patchMenu, nodeMenu;
	@Setter
	private EnumSet<ModuleType> types = EnumSet.allOf(ModuleType.class);
	private Module[] exclusions = new Module[0];
	
	private List<Domain> patches = Collections.emptyList();
	private List<Module> modules = Collections.emptyList();
	
	private Domain patch;
	private Module module;
	
	
	public NodeSelector(Consumer<Module> action, Supplier<Module> updateAction) {
		
		patchMenu = new DropDownMenu(this::selectPatch, this::getPatchIndex, this::updatePatchList);
		patchMenu.singleThread();
		
		Supplier<Module> uaw = () -> {
 			module = updateAction.get();
			setValue(module);
			if(module != null) {
				patch = module.getDomain();
				patchMenu.update();
			}
			return module;
		};
		
		Supplier<Integer> msup = () -> {
			uaw.get();
			return getModuleIndex();
		};
		
		nodeMenu = new DropDownMenu(this::selectModule, msup, this::updateModuleList);
		nodeMenu.singleThread();
		
		sender = new GuiSenderObject<Module>(this, action, uaw, null);
	}
	

	private void selectPatch(int i) {
		var p = patches.get(i);
		selectPatch(p);
	}
	
	
	public void selectPatch(Domain p) {
		if(patch != p) {
			patch = p;
			patchMenu.setValue(patches.indexOf(patch));
			updateModuleList();
			
			selectModule(0);
		}
	}
	
	
	public int getPatchIndex() {
		var m = module;
		if(m != null)
			patch = m.getDomain();
		return patches.indexOf(patch);
	}
	
	
	public void selectModule(int i) {
		if(i < 0) 
			selectModule(null);
		else
		if(i < modules.size())
			selectModule(modules.get(i));
	}
	
	
	public void selectModule(Module m) {
		module = m;
		message(m);
		nodeMenu.update();
	}
	
	
	private int getModuleIndex() {
		return modules.indexOf(module);
	}
	
	
	private String[] updatePatchList() {
		patches = RootClass.backstage().getAllPatches();
		return patches.stream()
					  .map(this::labelPatch)
					  .toArray(String[]::new);
	}
	
	
	private String[] updateModuleList() {
		if(patch != null) {
			var mods = patch.getContainedModules();
			modules = mods.stream()
						  .filter(this::isCompatible)
						  .collect(Collectors.toList());
		}else {
			modules = Collections.emptyList();
		}
		return modules.stream()
				  	  .map(this::labelModule)
				  	  .toArray(String[]::new);
	}
	
	public String labelPatch(Domain p) {
		return p.getTitle();
	}
	
	public String labelModule(Module m) {
		String s = m.getLabel();
		s = s == null ? m.getName() + "(Null)" : s; 
		return s;
	}
	
	public void exclude(Module...mods) {
		exclusions = mods;
	}
	
	public boolean isCompatible(Module m) {
		
		if(stream(exclusions).anyMatch(me -> me == m))
			return false;
		
		for (ModuleType t : types) {
			if(m.checkType(t))
				return true;
		}
		return false;
	}
	
	
	@Override
	public void singleThread() {
		sender.singleThread();
	}
	
	  
	public void message(Module v) {
		sender.message(v);
	}
	
	
	public void message(Module v, boolean undoable) {
		sender.message(v, undoable);
	}
	

	@Override
	public Module getValue() {
		return sender.getValue();
	}
	
	
	@Override
	public void setValue(Module v) {
		module = v;
		sender.setValue(v);
	}
	
	
	@Override
	public void update() {
		sender.update();
	}
	
	
	@Override
	public void disable(boolean b) {
		sender.disable(b);
		nodeMenu.disable(b);
		patchMenu.disable(b);
	}
	
	
	@Override
	public DisplayContainerIF getDm() {
		return nodeMenu.getDm();
	}

	public void toInspector(NodeInspector n) {
		toInspector(n, "Node");
	}
	
	public void toInspector(NodeInspector n, String nodeLabel) {
		n.addLabeledEntry("Patch", getPatchMenu());
		n.addLabeledEntry(nodeLabel, getNodeMenu());
	}
}
