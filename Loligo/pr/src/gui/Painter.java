package gui;

import java.util.function.DoubleUnaryOperator;
import lombok.Getter;
import lombok.Setter;
import processing.core.PConstants;
import processing.core.PGraphics;

@SuppressWarnings("unused")
public class Painter {

	static public class Curve {
		
		private Vec a, b, ta, tb;
		
		
		public Curve() {
			a = new Vec();
			b = new Vec();
			ta = new Vec();
			tb = new Vec();
		}
		
		public void start(float x, float y) {
			b.set(x, y);
		}
		
		public void paintTo(float x, float y) {
			swap();
			b.set(x, y);
			tb.set(b)
			  .subtract(a);
			  //.mult(2)
			  //.flip();
		}
		
		public void draw(PGraphics g) {
			
			g.beginDraw();
			g.stroke(0x55ffffff);
			g.strokeCap(PConstants.SQUARE);
			g.strokeWeight(20);
			//g.curveTightness(1);
			g.curve(
				ta.getXf(), ta.getYf(),
				a.getXf(), a.getYf(),
				b.getXf(), b.getYf(),
				b.getXf() + tb.getXf(), b.getYf() + tb.getYf()
			);
			g.endDraw();
			/*
			System.out.println();
			System.out.println(ta.getXf() + " " + ta.getYf());
			System.out.println(a.getXf() + " " + a.getYf());
			System.out.println(b.getXf() + " " + b.getYf());
			*/
		}
		
		public void swap() {
			ta.set(a);
			a.set(b);
			
		}
	}
	
	
	
	
	private Vec[] vec1, vec2;
	private Vec a1, a2, am, b1, b2, bm, dir, corner, t;
	
	@Getter@Setter
	private double weight = 20;
	@Getter@Setter
	private int color = 0xffffffff;
	
	
	public Painter() {
		vec1 = new Vec[] {
			a1 = new Vec(),
			a2 = new Vec(),
			am = new Vec()
		};
		vec1 = new Vec[] {
			b1 = new Vec(),
			b2 = new Vec(),
			bm = new Vec()
		};
		
		dir = new Vec();
		corner = new Vec();
		t = new Vec();
		//for (int i = 0; i < base.length; i++) 
		//	base[i] = new Vec();
	}
	
	public void start(float x, float y) {
		b1.set(x, y);
		b2.set(x, y);
		bm.set(x, y);
	}
	
	public void paintTo(float x, float y) {
		
		swap();
		bm.set(x, y);
		
		double dl = dir.set(am)
				   	   .subtract(bm)
				   	   .length();
		if(dl > 0) {
		
			dir.divide(dl)
			   .mult(weight)
			   .normal();
			   
			b1.set(bm).add(dir);
			a1.add(dir);
			dir.flip();
			b2.set(bm).add(dir);
			a2.add(dir);
		}
	}
	
	public void draw(PGraphics g) {
		g.fill(color);
		g.beginShape();
		g.vertex(a1.getXf(), a1.getYf());
		g.vertex(a2.getXf(), a2.getYf());
		g.vertex(b2.getXf(), b2.getYf());
		g.vertex(b1.getXf(), b1.getYf());
		g.endShape();
	}
	
	private void swap() {
		a1.set(bm);
		a2.set(bm);
		am.set(bm);
	}
	
	private void setBase(double x, double y) {
		
	}
	
	
	private static class Vec {
		double x = 0, y = 0;
		
		float getXf() {
			return (float) x;
		}
		
		float getYf() {
			return (float) y;
		}
		
		Vec mult(double d) {
			x *= d;
			y *= d;
			return this;
		}
		
		Vec divide(double d) {
			x /= d;
			y /= d;
			return this;
		}

		Vec add(Vec v) {
			x += v.x;
			y += v.y;
			return this;
		}
		
		Vec add(double d) {
			x += d;
			y += d;
			return this;
		}
		
		Vec subtract(Vec v) {
			x -= v.x;
			y -= v.y;
			return this;
		}
		
		Vec subtract(double x, double y) {
			this.x -= x;
			this.y -= y;
			return this;
		}
		
		Vec set(double x, double y) {
			this.x = x;
			this.y = y;
			return this;
		}
		
		Vec set(Vec v) {
			this.x = v.x;
			this.y = v.y;
			return this;
		}
		
		double length() {
			return Math.sqrt(x * x + y * y);
		}
		
		Vec normal() {
			double t = x;
			x = y;
			y = -t;
			return this;
		}
		
		Vec flip() {
			x = -x;
			y = -y;
			return this;
		}
		
		
		Vec apply(DoubleUnaryOperator func) {
			x = func.applyAsDouble(x);
			y = func.applyAsDouble(y);
			return this;
		}
		
		Vec copy() {
			return new Vec().set(this);
		}
		
		boolean isNaN() {
			return Double.isNaN(x) || Double.isNaN(y);
		}
	}
}
