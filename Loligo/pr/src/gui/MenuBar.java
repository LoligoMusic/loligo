package gui;

import static gui.UserAction.CLOSE_PROGRAM;
import static gui.UserAction.COPY;
import static gui.UserAction.CUT;
import static gui.UserAction.DELETE;
import static gui.UserAction.DUPLICATE;
import static gui.UserAction.GROUP;
import static gui.UserAction.HELPPATCH;
import static gui.UserAction.INSPECT;
import static gui.UserAction.LINKEDCOPY;
import static gui.UserAction.PASTE;
import static gui.UserAction.PATCH_AUTOSAVE;
import static gui.UserAction.PATCH_CLOSE;
import static gui.UserAction.PATCH_NEW;
import static gui.UserAction.PATCH_OPEN;
import static gui.UserAction.PATCH_SAVEAS;
import static gui.UserAction.REDO;
import static gui.UserAction.RESET;
import static gui.UserAction.SELECT_ALL;
import static gui.UserAction.SELECT_INVERT;
import static gui.UserAction.SELECT_LINKED;
import static gui.UserAction.SHOW_CONSOLE;
import static gui.UserAction.SHOW_SETTINGS;
import static gui.UserAction.SHOW_SPLASH;
import static gui.UserAction.TOGGLE_GRID;
import static gui.UserAction.TOGGLE_GUI;
import static gui.UserAction.UNDO;
import static module.ModuleType.ANIM;

import java.util.function.BooleanSupplier;
import java.util.function.Function;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.selection.SelectionManager;
import gui.text.TextBox;
import lombok.Getter;
import lombok.Setter;
import patch.DomainType;
import patch.LoligoPatch;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import util.Colors;

public class MenuBar extends ListObject{

	private static final int DROPDOWN_WIDTH = 200;
	
	private static final Function<LoligoPatch, SelectionManager> s = p -> p.getDisplayManager().getInput().selection;
	private static final Function<LoligoPatch, Boolean> 
		hasSelection = p -> s.apply(p).getNumSelected() > 0,
		hasNodes 	 = p -> !p.getContainedModules().isEmpty(),
		hasAnim 	 = p -> s.apply(p).getSelectedModules().stream().anyMatch(m -> m.checkType(ANIM));
	
	
	@Getter
	private static MenuBar bar;
	
	public static MenuBar getMenuBar(LoligoPatch p) {
		if(bar == null)
			createMenuBar(p);
		return bar;
	}
	
	public static MenuBar createMenuBar(LoligoPatch patch) {
		
		var isMain = patch.domainType() == DomainType.MAIN;
		
		bar = new MenuBar();
		bar.setColor(0xff111111);
		bar.setVertical(false);
		
		var file = bar.new MBListObject();
		
		if(isMain)
			file.add(PATCH_NEW	);
		file.add(PATCH_OPEN		);
		file.add(PATCH_AUTOSAVE	);
		file.add(PATCH_SAVEAS	);
		file.add(PATCH_CLOSE	);
		if(isMain) { 
			file.add(SHOW_SETTINGS);
			file.addSeparator();
			file.add(CLOSE_PROGRAM);
		}
		file.endList();
		bar.addCascadeElement("File", file);
		
		var edit = bar.new MBListObject();
		edit.add(UNDO			).enableIfPatch(p -> p.getHistory().hasNextUndo());
		edit.add(REDO			).enableIfPatch(p -> p.getHistory().hasNextRedo());
		edit.addSeparator();
		edit.add(CUT			).enableIfPatch(hasSelection);
		edit.add(COPY			).enableIfPatch(hasSelection);
		edit.add(PASTE			).enableIf(() -> true);
		edit.add(DUPLICATE		).enableIfPatch(hasSelection);
		edit.add(LINKEDCOPY		).enableIfPatch(hasAnim	    );
		edit.add(DELETE			).enableIfPatch(hasSelection);
		edit.add(RESET			).enableIfPatch(hasSelection);
		edit.addSeparator();
		edit.add(INSPECT		).enableIfPatch(hasSelection);
		edit.add(GROUP			).enableIfPatch(hasSelection);
		edit.endList();
		
		bar.addCascadeElement("Edit", edit);
		
		var select = bar.new MBListObject();
		select.add(SELECT_ALL		).enableIfPatch(hasNodes	);
		select.add(SELECT_INVERT	).enableIfPatch(hasSelection);
		select.add(SELECT_LINKED	).enableIfPatch(hasSelection);
		select.endList();
		bar.addCascadeElement("Select", select);
		
		var view = bar.new MBListObject();
		view.add(SHOW_CONSOLE);
		view.addSeparator();
		view.add(TOGGLE_GRID);
		view.add(TOGGLE_GUI);
		view.endList();
		bar.addCascadeElement("View", view);
		
		var help = bar.new MBListObject();
		help.add(HELPPATCH		).enableIfPatch(hasSelection); 
		help.add(SHOW_SPLASH); 
		help.endList();
		bar.addCascadeElement("Help", help);
		
		
		if(isMain) {
			bar.addSeparator();
			var t = PatchTabs.getPatchTabs().getList();
			bar.add(t);
		}	
		
		return bar;
	}
	
	
	@Getter@Setter
	private LoligoPatch patch;
	@Getter@Setter
	private boolean enabled = true;
	
	public MenuBar() {
		setVertical(false);
		setColor(0xaaffffff);
		
	}
	
	@Override
	public CascadeElement createCascadeElement(String name, ListObject listObject) {
		return new MBCascadeElement(name, listObject);
	}
	
	
	private class MBListObject extends ListObject{
		
		@Override
		public gui.ListObject.ListElement createListElement(String name, Runnable r) {
			return new MBListElement(this, r, name);
		}
		
		MBListElement add(UserAction action) {
			var sc = action.getShortcut();
			var n = action.getDescription();
			Runnable r = () -> sc.run(patch);
			var e = (MBListElement) addListElement(n, r);
			e.setWH(DROPDOWN_WIDTH, e.getHeight());
			
			String keyString = sc.toString();
			if(keyString.length() > 0) {
				var t = new TextBox(keyString, true);
				t.setTextColor(Colors.grey(100));
				t.setPos(e.getWidth() - t.getWidth() + padding, 0);
				t.setFlag(DisplayFlag.fixedToParent, true);
				e.addChild(t);
			}
			return e;
		}
		
	}
	

	private class MBListElement extends ListElement {

		public MBListElement(ListObject listObj, Runnable action, String name) {
			super(listObj, action, name);
		}
		
		public MBListElement enableIfPatch(Function<LoligoPatch, Boolean> foo) {
			BooleanSupplier s = () -> foo.apply(patch);
			super.enableIf(s);
			return this;
		}
		
	}
	
	
	private class MBCascadeElement extends CascadeElement {

		public MBCascadeElement(String name, ListObject listObject) {
			super(name, listObject);
		}
		
		public void showList() {
			MenuBar.this.getDm().getInput().setRemovableTarget(listObject, MenuBar.this);
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			if(e == InputEvent.CLICK) {
				addList();
			}else
			if(e == InputEvent.OVER) {
				DisplayObjectIF s = getDm().getInput().selection.getInputElement();
				if(items.contains(s))
					addList();
			}
		}
		
	}
	
}
