package gui;

import static constants.InputEvent.DRAG;
import static constants.InputEvent.PRESSED;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.text.TextBox;
import lombok.experimental.Delegate;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;


public class Slider extends DisplayObject implements DisplayGuiSender<Double>{
	
	protected final GuiSenderObject<Double> sender;
	@Delegate
	private final MouseOverTextHandler mouseOverText = new MouseOverTextHandler(this);
	
	protected boolean active = true;
	public boolean vertical = true;
	public int colorInactive = 0xff664444;
	protected final TextBox textBox;
	
	
	
	public Slider(final String name, final Consumer<Double> action, final Supplier<Double> updateAction, double value) {
		
		sender = new GuiSenderObject<Double>(this, action, updateAction, value);
		
		if(name != null) {
			textBox = new TextBox(name);
			textBox.setFlag(DisplayFlag.fixedToParent, true);
			addChild(textBox);
			textBox.setPos(0, -21);
		}else
			textBox = null;
		//type = name;
		setFlag(DisplayFlag.RECT, true);
		//type = "";
		
		setColor(0xff666666);
	}
	
	
	public Slider(final Consumer<Double> action, final Supplier<Double> updateAction, final String name) {
		this(name, action, updateAction, .5);
	}
	
	
	public Slider() {
		
		sender = new GuiSenderObject<Double>(this, null, null, 0d);
		setFlag(DisplayFlag.RECT, true);
		//type = "";
		textBox = null;
	}
	
	
	@Override
	public void render(PGraphics g) {
		
		g.noStroke();
		g.fill(active ? getColor() : colorInactive );
		g.rect(getX(), getY(), getWidth(), getHeight());
		//dm.g.text(type, getX(), getY() - 2);
		g.fill(255, 100);
		g.strokeWeight(2);
		float f;
		
		if(vertical) {
			f = (float)((1 - sender.getValue()) * getHeight());
			g.rect(getX(), getY(), getWidth(), f);
			if(active) {
				g.stroke(250);
				g.line(getX() - 1, getY() + f, getX() + getWidth() + 1, getY() + f);
			}
			
		}else{
			f = (float)(getWidth() * sender.getValue());
			g.rect(getX(), getY(), f, getHeight());
			if(active) {
				g.stroke(250);
				g.line(getX() + f, getY() - 1, getX() + f, getY() + getHeight() + 1);
			}
		}
		
		g.noStroke();
	}
	
	private double calculateOutput() {
		var in = getDm().getInput();
		double v = (double) (vertical ? 1 - (in.getMouseY() - getY()) / getHeight() :
     									    (in.getMouseX() - getX()) / getWidth());
		if(v > 1)      v = 1d;
		else if(v < 0) v = 0d;
		
		return v;
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(e == DRAG || e == PRESSED) {
			
			if(active) 
				sender.message(calculateOutput());
			
		}
	}
	
	
	@Override
	public void disable(boolean b) {
		active = !b;
	}

	@Override
	public Double getValue() {
		return sender.getValue();
	}

	@Override
	public void setValue(Double v) {
		sender.setValue(v);
	}
	
	@Override
	public void update() {
		
		sender.update();
	}
	
	
}
