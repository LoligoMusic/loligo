package gui;

import static gui.text.TextBlock.TextFlag.MULTILINE;

import java.util.function.Consumer;

import gui.text.TextBlock;
import gui.text.TextBlock.AlignMode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor

public class MouseOverTextHandler implements MouseOverRestInterface{

	private final DisplayGuiSender<?> target;
	@Getter@Setter
	private String mouseOverText;
	private TextBlock textBlock;
	@Setter
	private Consumer<TextBlock> textBlockConsumer = t -> {};
	
	
	public void initTextBlock(TextBlock textBlock) {
		//textBlock.setWH(mouseOverTextWidth, textBlock.getHeight());
		textBlock.setTextFlag(MULTILINE);
		textBlock.setFixedWidth(true);
		textBlock.setBackground(true);
		textBlock.setColor(0xff111111);
		textBlock.setTextColor(0xffaaaaaa);
		textBlock.setText(mouseOverText);
		textBlock.setAlignMode(AlignMode.CENTER);
		textBlock.setPos(target.getX(), target.getY() - 15);
		textBlockConsumer.accept(textBlock);
	}
	
	@Override
	public void mouseOverRest(boolean on) {
		 
		if(mouseOverText == null) 
			return;
		
		if(on) {
			textBlock = new TextBlock();
			initTextBlock(textBlock);
			//target.addChild(textBlock);
			target.getDm().getInput().setRemovableTarget(textBlock, target);
		}else {
			//target.removeChild(textBlock);
			target.getDm().getInput().removeRemovableTarget(textBlock);
		}
	}
}
