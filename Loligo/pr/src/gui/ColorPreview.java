package gui;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.selection.GuiType;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import processing.core.PImage;
import util.Images;


public class ColorPreview extends DisplayObject implements DisplayGuiSender<Integer>{

	private static final PImage ALPHA_BG = Images.loadPImage("/resources/checker.png");
	
	private final DisplayGuiSender<Integer> editor;
	protected boolean active = true;
	private final Supplier<Integer> getFoo;
	
	public ColorPreview(Consumer<Integer> setFoo, Supplier<Integer> getFoo) {
		
		editor = createColorPicker(setFoo, getFoo);
		this.getFoo = getFoo;
		
		setWH(20, 20);
		setFlag(DisplayFlag.RECT, true); 
		setFlag(DisplayFlag.CLICKABLE, true);
		setGuiType(GuiType.SIGNAL);
		//setFlag(DisplayFlag.Subscriber, true);
	}
	
	@Override
	public void render(PGraphics g) {

		float w = getWidth() / 2,
			  h = getHeight(),
			  x = getX(),
			  x2 = x + w,
			  y = getY();
		
		int c = getColor();
		
		g.fill(c | 0xff000000);
		g.rect(x, y, w, h);
		
		g.clip(x2, y, w, h);
		g.image(ALPHA_BG, x2, y);
		
		g.fill(c);
		g.rect(x2, y, w, h);
		g.noClip();
	}
	
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(active && e == InputEvent.DOUBLECLICK) {
			
			input.setRemovableTarget(editor, this);
			editor.setPos(getX(), getY() + getHeight() + 4); 
			DisplayObjects.holdInScreen(editor);
		}
	}
	
	
	public DisplayGuiSender<Integer> createColorPicker(Consumer<Integer> setFoo, Supplier<Integer> getFoo) {
		
		ColorPicker cp = new ColorPicker(setFoo, getFoo);
		
		return cp;
	}


	@Override
	public Integer getValue() {
		
		return editor.getValue();
	}


	@Override
	public void setValue(Integer v) {
		
		editor.setValue(v);
		
		setColor(v);
	}

 
	@Override
	public void update() {
		if(editor.checkFlag(DisplayFlag.onDisplay)) {
			editor.update();
			setColor(editor.getValue());
		}else {
			setColor(getFoo.get());
		}
	}


	@Override
	public void disable(boolean b) {
		editor.disable(b);
		active = !b;
	}
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		update();
	}
	
}
