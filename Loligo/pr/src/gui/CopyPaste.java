package gui;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import module.Module;
import module.ModuleDeleteMode;
import module.Modules;
import module.modules.anim.Image;
import module.modules.anim.SVG;
import module.modules.audio.Sampler;
import patch.LoligoPatch;
import save.AssetContainer;
import save.AssetRegistry;
import save.ImageData;
import save.MediaType;
import save.ModuleData;
import save.PatchData;
import save.RestoreDataContext;
import save.SaveDataContext;
import save.SaveJson;
import save.SaveMode;
import save.SaveTarget;
import util.Naming;
import util.StringFormat;

public class CopyPaste {

	public static final Pattern IMAGE_URL_PATTERN = Pattern.compile(
			"(href|src)\\s*=\\s*\"([^\\s]+\\/\\/[^\\/]+.\\/[^\\s]+\\.(jpg|jpeg|png|gif|bmp))");
	
	static public void copy(LoligoPatch patch) {
	
		List<Module> nodes = getSelectedModules(patch);
		
		PatchData pd = nodesToPatchData(nodes, patch);
		
		String json = SaveJson.patchToJson(pd);
		
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
				new StringSelection(json), null);
	}

	
	static public void paste(LoligoPatch patch) {
		
		String s = getClipBoardString();
		
		if(s == null)
			return;
		
		switch(StringFormat.getFormat(s)) {
		case HTML_CLIPBOARD -> pasteHtmlClipboard(patch, s);
		case LOLIGOFILE 	-> restoreLoligoFile(patch, s);
		case URL 			-> handleURL(patch, s);
		case UNKNOWN 		-> System.err.println("String Pasted: Unknown Format");
		default 			-> {}
		}
		
		
		//System.err.println("Invalid clipboard data!\n" + json);
	}

	public static boolean hasClipBoardContentLoligoPatch() {
		return hasClipBoardContent(StringFormat.LOLIGOFILE);
	}
	
	public static boolean hasClipBoardContent(StringFormat f) {
		String s = getClipBoardString();
		return s != null && StringFormat.getFormat(s) == f;
	}
	
	public static void handleURL(LoligoPatch patch, String url) {
		try {
			URL u = Paths.get(url).toUri().toURL();
			handleURL(patch, u);
		} catch (MalformedURLException | InvalidPathException e) {
			e.printStackTrace();
		}
	}

	public static void handleURL(LoligoPatch patch, URL url) {
		
		var m = MediaType.find(url);
		
		if(m == null)
			return;
		
		loadToNode(patch, url, m);
		
		/*
		switch(m) {
		case AUDIO:
			loadToSamplerNode(patch, url);
			break;
		case IMAGE:
			loadToImageNode(patch, url);
			break;
		case LOLIGOPATCH:
			SaveJson.loadPatchFromStream(url);
			break;
		case PYSCRIPT:
			loadScriptNode(patch, url);
			break;
		case SVG:
			loadToSVGNode(patch, url);
			break;
		}
		*/
	}
	
	
	public static void loadScriptNode(LoligoPatch patch, URL url) {
		
	}
	
	
	public static void loadToSamplerNode(LoligoPatch patch, URL url) {
		
	}
	
	
	public static void loadToNode(LoligoPatch patch, URL url, MediaType mt) {
		
		var cls = switch(mt) {
		case AUDIO -> Sampler.class;
		case IMAGE -> Image.class;
		case SVG   -> SVG.class; 
		default	   -> null;
		};
			
		if(cls != null) {
			var m = patch.getNodeRegistry().createInstance(cls);
			Modules.initializeModuleDefault(m, patch);
			var ui = patch.getDisplayManager().getInput();
			m.getModuleGUI().setPos(ui.getMouseX(), ui.getMouseY());
			((AssetContainer) m).loadAssetFromURL(url);
		}else 
		if(mt == MediaType.LOLIGOPATCH) {
			SaveJson.loadPatchFromStream(url);
		}
		
	}
	
	
	public static void loadToSVGNode(LoligoPatch patch, URL url) {
		
		
	}
	
	
	public static void loadToImageNode(LoligoPatch patch, URL url) {
		
		var ui = patch.getDisplayManager().getInput();
		float x = ui.getMouseX(), 
			  y = ui.getMouseY();
		
		final ImageData id = AssetRegistry.createImageDataAsync(url, d -> {});
		
		Runnable r = () -> {
			var imgs = id.getImgs();
			if(imgs != null && imgs.length > 0) {
				Image node = patch.getNodeRegistry().createInstance(Image.class);
				patch.addModule(node);
				node.defaultSettings();
				node.setImages(id);
				node.setPos(x, y);
			}
		};
		
		SaveJson.showWaitingAnimation(patch, () -> id.getStatus().finished(), r);
	}


	public static void cut(LoligoPatch patch) {
		copy(patch);
		getSelectedModules(patch).forEach(m -> patch.removeModule(m, ModuleDeleteMode.NODE));
	}
	
	
	public static void duplicateSelection(LoligoPatch patch) {
		var ns = getSelectedModules(patch);
		duplicate(ns, patch);
	}
	
	public static void duplicate(Module m, LoligoPatch patch) {
		duplicate(Collections.singletonList(m), patch);
	}
	
	public static void duplicate(List<Module> mods, LoligoPatch patch) {
		var pd = nodesToPatchData(mods, patch);
		restorePatchData(patch, pd);
	}
	
	
	public static List<Module> getSelectedModules(LoligoPatch patch) {
		var sm = patch.getDisplayManager().getInput().selection;
		sm.includeContainedModules();
		var sel = sm.getSelection();
		
		return sel.stream()
				  .map(patch.getModuleManager()::getModuleFromGui)
				  .filter(Objects::nonNull)
				  .toList();
	}
	
	
	public static PatchData nodesToPatchData(List<Module> nodes, LoligoPatch patch) {
		var sdc = new SaveDataContext(SaveMode.DUPLICATION);
		sdc.addModules(nodes);
		sdc.setSaveTarget(SaveTarget.CLIPBOARD);
		
		PatchData pd = sdc.buildPatchData();
		pd.setId(patch.getDomainID());
		return pd;
	}
	
	
	public static void restoreLoligoFile(LoligoPatch patch, String s) {
		try {
			var pd = SaveJson.parseJsonToPatch(s);
			restorePatchData(patch, pd);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void restorePatchData(LoligoPatch patch, PatchData pd) {
		
		new RestoreDataContext(pd, patch)
			.resetNodeIDs()
			.restorePatch();
		
		var in = patch.getDisplayManager().getInput();
		
		in.selection.clear();
		
		pd.getNodes()
		  .stream()
		  .map(ModuleData::getModuleOut).forEach(
			m -> {
				String n = Naming.markAsCopy(m.getLabel()); 
				String la = Naming.findFreeModuleName(n, patch);
				m.setLabel(la);
				in.selection.add(m.getGui());
			}
		);
		
		in.mouseDrag.setOrigin(in.selection.getSelection(), in.getMouseX(), in.getMouseY(), true);
	}
	
	
	public static void pasteHtmlClipboard(LoligoPatch patch, String s) {
		
		Matcher m = IMAGE_URL_PATTERN.matcher(s);
		
		if(m.find()) {
			
			String us = m.group(0);
			us = us.substring(us.indexOf('"') + 1);
			try {
				
				URL url = new URL(us);
				loadToImageNode(patch, url);
				
			} catch (MalformedURLException e) {
			}
		}
	}
	
	static public String getClipBoardString() {
		
		Transferable tr = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
		
		if(tr == null)
			return null;
		

		try {
			
			for (DataFlavor f : tr.getTransferDataFlavors()) {
				
				/*
				if(f.getRepresentationClass() == String.class) {
					System.out.println(f.getMimeType());
					System.out.println();
					String s = (String) tr.getTransferData(f);
					System.out.println(s);
					System.out.println();
					System.out.println(StringFormat.getFormat(s));
					System.out.println("----");
				}
				*/
				
				if(f.getRepresentationClass() == String.class) {
					String s = (String) tr.getTransferData(f);
					return s;
				}else
				if(f.isFlavorJavaFileListType()) {
					List<?> files = (List<?>) tr.getTransferData(f);
					if(!files.isEmpty()) {
						File fi = (File) files.get(0);
						return fi.getPath();
					}
				}else {
					
				}
				
			}
			
		} catch (UnsupportedFlavorException | IOException e) {
			System.err.println(e);
		}
		
		return null;
	}
	
	
	
	
	private CopyPaste() {}
	
	

}
