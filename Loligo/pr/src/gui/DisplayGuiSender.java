package gui;

import static util.Colors.grey;

import module.inout.NumericType;
import pr.DisplayObjectIF;

public interface DisplayGuiSender<T> extends GuiSender<T>, DisplayObjectIF {

	static public final int COLOR_ACTIVE = grey(200), COLOR_INACTIVE = grey(160), color_disabled = grey(120);


	public static interface Numeric extends DisplayGuiSender<Double> {
		
		void active(boolean b);
		void setRange(double min, double max);
		double maxValue();
		double minValue();
		NumericType.SubType getNumericType();
		
		void setNumericType(NumericType.SubType st);
		
		default void setMinValue(double min) {
			setRange(min, maxValue());
		}
		
		default void setMaxValue(double max) {
			setRange(minValue(), max);
		}
	}

}
