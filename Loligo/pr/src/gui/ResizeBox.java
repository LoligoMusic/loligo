package gui;

import static util.Images.loadPImage;

import java.util.function.BiConsumer;

import constants.DisplayFlag;
import constants.InputEvent;
import lombok.Setter;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PImage;


public class ResizeBox extends DisplayObject{

	private static final PImage NS   = loadPImage("/resources/cursor_ns.png"  ),
								EW   = loadPImage("/resources/cursor_ew.png"  ),
								NWSE = loadPImage("/resources/cursor_nwse.png"),
								NESW = loadPImage("/resources/cursor_nesw.png");
	
	private final DisplayObjectIF self;
	@Setter
	private BiConsumer<Integer, Integer> onResize = (a, b) -> {};
	@Setter
	private int minWidth = 20, minHeight = 20;
	private int xRight, yDown;
	private float border = 3;
	private boolean left, right, up, down, drag;
	@Setter
	private boolean horizontalResize = true, verticalResize = true;
	
	
	public ResizeBox(DisplayObjectIF self) {
		this.self = self;
		setFlag(DisplayFlag.RECT, true);
		setFlag(DisplayFlag.fixedToParent, true);
	}
	
	
	@Override
	public void render(PGraphics g) {
		
	}
	
	
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		switch(e) {
		case OVER -> {
			//input.getDomain().getPApplet().cursor(PConstants.CROSS);
		}
		case OVER_STOP -> {
			if(!drag)
				input.getDomain().getPApplet().cursor(PConstants.ARROW);
		}
		case DRAG -> {
			resize(input.mouseX, input.mouseY);
		}
		case DRAG_START -> {
			xRight = (int) getX() + getWidth();
			yDown  = (int) getY() + getHeight();
			drag = true;
		}
		case DRAG_STOP -> {
			drag = false;
			input.getDomain().getPApplet().cursor(PConstants.ARROW);
		}
		default -> {}
		}
	}
	
	
	private void resize(int mx, int my) {
		
		int x = (int) getX(), 
			y = (int) getY(), 
			w = getWidth(),
			h = getHeight();
		
		if(left) {
			x = mx;
			w = xRight - mx;
		}else
		if(right) {
			w = mx - (int) self.getX();
		}
		
		if(up) {
			y = my;
			h = yDown - my;
		}else
		if(down) {
			h = my - (int) self.getY();
		}
		
		if(w >= minWidth && h >= minHeight) {
			self.setPos(x, y);
			setWH(w, h);
			onResize.accept(w, h);
		}
	}
	
	
	@Override
	public boolean hitTest(float mx, float my) {
		return self.hitTest(mx, my) && overBorder((int) mx, (int) my);
	}
	

	private boolean overBorder(int mx, int my) {
		
		if(drag)
			return true;
		
		int x = mx - (int) self.getX();
		int y = my - (int) self.getY();
		int w = self.getWidth();
		int h = self.getHeight();
		
		left  = horizontalResize && x <= border;
		right = horizontalResize && x >= w - border;
		up    = verticalResize   && y <= border;
		down  = verticalResize   && y >= h - border;
		
		boolean b = left || right || up || down;
		
		if(b) {
			
			PImage c = (left && up) || (right && down) ? NWSE :
					   (left && down) || (right && up) ? NESW :
						left || right 				   ? EW   :
						up || down   				   ? NS   : NS;
			
			getDm().getGuiDomain().getPApplet().cursor(c);
			
		}else
		if(drag){
			getDm().getGuiDomain().getPApplet().cursor(PConstants.ARROW);
		}
		
		return b;
	}
	

	@Override
	public int getWidth() {
		return self.getWidth();
	}
	
	@Override
	public int getHeight() {
		return self.getHeight();
	}
	
	@Override
	public void setWH(int width, int height) {
		super.setWH(width, height);
		self.setWH(width, height);
	}
	
}
