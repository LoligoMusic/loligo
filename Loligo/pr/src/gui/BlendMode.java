package gui;

import lombok.RequiredArgsConstructor;
import processing.core.PConstants;

@RequiredArgsConstructor
public enum BlendMode {
	
	DEFAULT(PConstants.BLEND),
	BLEND(PConstants.BLEND),
	ADD(PConstants.ADD),
	SUBTRACT(PConstants.SUBTRACT),
	DARKEST(PConstants.DARKEST),
	LIGHTEST(PConstants.LIGHTEST),
	//DIFFERENCE(PConstants.DIFFERENCE), Not supported by OpenGl Renderer
	EXCLUSION(PConstants.EXCLUSION),
	MULTIPLY(PConstants.MULTIPLY),
	SCREEN(PConstants.SCREEN),
	REPLACE(PConstants.REPLACE);
	
	public static final BlendMode[] blendValues = BlendMode.values();
	
	public final int value;
	
}
