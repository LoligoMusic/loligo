package gui;

import java.util.function.Consumer;

import constants.DisplayFlag;
import pr.DisplayObject;
import processing.core.PGraphics;



public class SelectionDeko extends DisplayObject {

	private int stroke = 0xaaffffff;
	private int weight = 1;
	private Consumer<PGraphics> cons;
	
	
	public SelectionDeko(DisplayObject obj) {
		
		this(obj, null);
	}
	
	public SelectionDeko(final DisplayObject obj, final Consumer<PGraphics> cons) {
		
		setWH(obj.getWidth(), obj.getHeight());
		
		this.cons = cons != null 		   			 ? cons :
					obj.checkFlag(DisplayFlag.ROUND) ? g -> g.ellipse(obj.getX(), obj.getY(), obj.getWidth(), obj.getWidth()) :
					obj.checkFlag(DisplayFlag.RECT)  ? g -> g.rect(obj.getX(), obj.getY(), obj.getWidth(), obj.getHeight()) :
											 		   null;
	}
	
	@Override
	public void render(PGraphics g) {
		
		g.noFill();
		g.stroke(stroke);
		g.strokeWeight(weight);
		
		cons.accept(g);
		
		g.noStroke();
		
	}
	
}
