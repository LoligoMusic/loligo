package gui;

import static constants.DisplayFlag.CLICKABLE;
import static constants.InputEvent.PRESSED;
import static gui.text.TextBlock.AlignMode.CENTER;
import static java.util.Arrays.stream;

import java.util.EnumSet;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.InputEvent;
import gui.text.TextBox;
import gui.text.TextInputField;
import lombok.Setter;
import lombok.var;
import module.Module;
import module.ModuleType;
import patch.Domain;
import pr.RootClass;
import pr.userinput.UserInput;
import util.Colors;


public class NodeSelector2 extends TextBox implements DisplayGuiSender<Module> {

	private final GuiSenderObject<Module> sender;
	
	private ListObject list;
	
	@Setter
	private EnumSet<ModuleType> types = EnumSet.allOf(ModuleType.class);
	private Module[] exclusions = new Module[0];
	
	
	public NodeSelector2(Consumer<Module> action, Supplier<Module> updateAction) {
		sender = new GuiSenderObject<Module>(this, action, updateAction, null);
		list = new ListObject();
		list.bottom = 0;
		list.top = 4;
		
		setFlag(CLICKABLE, true);
		bgBox = true;
		padding = 5;
		setColor(TextInputField.COLOR_INACTIVE);
		setTextColor(TextInputField.TEXT_COLOR_INACTIVE);
		setAlignMode(CENTER);
		
		var b = NodeSelectorClick.button(this::selectModule);
		b.setWH(getHeight(), getHeight());
		addChild(b);
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		super.mouseEventThis(e, input);
		
		if(e == PRESSED) {
			makePatchList();
			list.setPos(getX(), getY() + getHeight());
			getDm().getInput().setRemovableTarget(list, this);
		}
	}
	
	private void makePatchList() {
		list.clear();
		list.adjustSize(true);
		list.setColor(Colors.grey(60));
		for (var p : RootClass.backstage().getAllPatches()) {
			String s = labelPatch(p);
			list.addCascadeElement(list.size(), s, li -> makeNodeList(p, li));
		}
		
	}
	
	
	private void makeNodeList(Domain d, ListObject li) {
		li.bottom = 0;
		li.top = 4;
		li.adjustSize(true);
		li.setColor(Colors.grey(60));
		for (Module m : d.getContainedModules()) {
			if(isCompatible(m)) {
				String s = labelModule(m);
				li.addListElement(s, () -> selectModule(m));
			}
		}
	}
	
	
	public void selectModule(Module m) {
		setValue(m);
		message(m);
	}
	
	
	public String labelPatch(Domain p) {
		return p.getTitle();
	}
	
	public String labelModule(Module m) {
		String s = m.getLabel();
		s = s == null ? m.getName() + "(Null)" : s; 
		return s;
	}
	
	public void exclude(Module...mods) {
		exclusions = mods;
	}
	
	public boolean isCompatible(Module m) {
		
		if(stream(exclusions).anyMatch(me -> me == m))
			return false;
		
		for (ModuleType t : types) {
			if(m.checkType(t))
				return true;
		}
		return false;
	}
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		update();
	}
	
	@Override
	public void singleThread() {
		sender.singleThread();
	}
	
	  
	public void message(Module v) {
		sender.message(v);
	}
	
	
	public void message(Module v, boolean undoable) {
		sender.message(v, undoable);
	}
	

	@Override
	public Module getValue() {
		return sender.getValue();
	}
	
	
	@Override
	public void setValue(Module v) {
		sender.setValue(v);
		setText(v == null ? "" : v.getLabel());
	}
	
	
	@Override
	public void update() {
		sender.update();
	}
	
	
	@Override
	public void disable(boolean b) {
		sender.disable(b);
	}
	
}