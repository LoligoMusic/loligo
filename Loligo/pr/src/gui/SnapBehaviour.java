package gui;

import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import constants.InputEvent;
import constants.Timer;
import gui.paths.NullPathObject;
import gui.paths.Path;
import gui.paths.PathObject;
import lombok.Getter;
import lombok.Setter;
import pr.EventSubscriber;
import pr.Positionable;
import pr.userinput.UserInput;

public abstract class SnapBehaviour<T extends Positionable> implements EventSubscriber{

	private SnapBehaviour<?> nextSnapBehaviour;
	protected final Positionable target;

	protected int snapDistance = 15;
	
	private Consumer<T> onSnap, onUnsnap, onDropSnap; 
	protected BiConsumer<Float, Float> onEmptyDrop;
	
	protected boolean isSnapping;
	protected T lastSnap;
	
	
	private boolean autoRemove;
	
	
	public SnapBehaviour(Positionable target) {
		
		this(target, p -> {});
	}
	
	public SnapBehaviour(Positionable target, Consumer<T> onSnap) {
		
		this.target = target;
		this.onSnap = onSnap;
		
	}
	
	public SnapBehaviour<?> addSnapBehaviour(SnapBehaviour<?> sb) {
		
		if(nextSnapBehaviour == null) {
			nextSnapBehaviour = sb;
		}else{
			nextSnapBehaviour.addSnapBehaviour(sb);
		}
		return this;
	}
	
	public SnapBehaviour<?> addOnSnapConsumer(Consumer<T> onSnap) {
		
		this.onSnap = onSnap;
		return this;
	}
	
	public SnapBehaviour<?> addOnUnsnapConsumer(Consumer<T> onUnsnap) {
		
		this.onUnsnap = onUnsnap;
		return this;
	}
	
	public SnapBehaviour<?> addDropConsumer(Consumer<T> onDropSnap) {
		
		this.onDropSnap = onDropSnap;
		return this;
	}
	
	/**
	 * Executed if target dropped without snapping on something. Only works on the last SnapBehaviour in the chain.
	 * @param onEmptyDrop
	 * @return
	 */
	public SnapBehaviour<?> addEmptyDropConsumer(BiConsumer<Float, Float> onEmptyDrop) {
		
		this.onEmptyDrop = onEmptyDrop;
		return this;
	}
	
	public void setAutoRemove(boolean autoRemove) {
		this.autoRemove = autoRemove;
	}
	
	public abstract boolean applySnap(UserInput in);
	
	public void multiApplySnap(UserInput in) { 
		
		if(applySnap(in)) {
			
			
		}else
		if(nextSnapBehaviour != null) {
			
			nextSnapBehaviour.multiApplySnap(in);
		}
	}
	
	public void setSnapDistance(int snapDistance) {
		this.snapDistance = snapDistance;
	}
	
	public int getSnapDistance() {
		return snapDistance;
	}
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(e == InputEvent.DRAG && input.getDragTarget() == target) {
			
			multiApplySnap(input);
		}else
		if(e == InputEvent.DRAG_STOP && input.getDragTarget() == target) {
			
			//if(isSnapping) {
				
				dropped(input);
			//}
			
		}else
		if(e == InputEvent.DROPPED && input.getDragTarget() == target) {
			
			dropped(input);
		}
		
	}

	private void dropped(UserInput input) {
		
		if(lastSnap != null) {
				
			onDrop(lastSnap);
		}else
		if(nextSnapBehaviour != null) {
			
			nextSnapBehaviour.dropped(input);
		}else{
			
			onEmptyDrop();
		}
		
		lastSnap = null;
		
		if(autoRemove) {	// TODO this is shit
			Timer.createFrameDelay(
					input.getDomain(), 
					t -> input.removeInputEventListener(this)
			).start();
		}
	}
	
	public void setPosition(float x, float y) {
		target.setGlobal(x, y);
	}
	
	public void onDrop(T t){
		if(onDropSnap != null)
			onDropSnap.accept(t);
	}
	
	public void onEmptyDrop() {
		if(onEmptyDrop != null)
			onEmptyDrop.accept(target.getX(), target.getY());
	}
	
	public void onSnap(T t) {
		if(t != lastSnap) {
			onUnsnap();
		}
		
		if(lastSnap != t) {
			onSnap.accept(t);
			lastSnap = t;
		}
	}
	
	public void onUnsnap() {
		if(lastSnap != null && onUnsnap != null) {
			onUnsnap.accept(lastSnap);
		}
		lastSnap = null;
	}

	public boolean isSnapping() {
		return isSnapping;
	}

	
	
	
	
	public static class SnapToObjects<C extends Positionable> extends SnapBehaviour<C> {
		
		protected List<C> snapPoints;
		
		
		public SnapToObjects(Positionable target) {
			
			this(target, Collections.emptyList());
		}
		
		public SnapToObjects(Positionable target, List<C> snapPoints) {
			
			super(target);
			
			this.snapPoints = snapPoints;
		}

		public void setSnapPoints(List<C> snapPoints) {
			
			this.snapPoints = snapPoints;
		}
		
		public boolean applySnap(UserInput in) {
			
			if(snapPoints.isEmpty())
				return false;
			
			C pp = snapFunction(in);
			
			if(isSnapping = pp != null) {
				
				setPosition(pp.getX(), pp.getY());
				
				onSnap(pp);
			}else {
				onUnsnap();
			}
				
			
			return isSnapping;
		}
		
		public C snapFunction(UserInput in) {
			C pp = null;
			float minDist = Float.MAX_VALUE;
			for (C p : snapPoints) {
				
				if(Positionables.withinRange(p, in.mousePos, snapDistance)) {
					
					float d = Positionables.distance(p, in.mousePos);
					if(d < minDist) {
						pp = p;
						minDist = d;
					}
				}
			}
			return pp;
		}
	}
	
	
	public static class SnapToRing extends SnapBehaviour<Positionable> {
		
		@Getter@Setter
		private float radius;
		@Setter
		private Positionable ring;
		private float lastX, lastY;
		
		public SnapToRing(Positionable target) {
			this(target, 20);
		}
		
		public SnapToRing(Positionable target, float radius) {
			super(target);
			this.radius = radius;
		}

		
		public boolean applySnap(UserInput in) {
			float dx = in.getMouseX() - ring.getX();
			float dy = in.getMouseY() - ring.getY();
			float q = dx * dx + dy * dy;
			boolean z = q > 0;
			float d = (float) Math.sqrt(q);
			isSnapping = d < radius + snapDistance || !z;
			
			if(isSnapping) {
				if(z) {
					float s = radius / d;
					lastX = ring.getX() + dx * s;
					lastY = ring.getY() + dy * s;
				}
				setPosition(lastX, lastY);
				onSnap(ring);
			}else {
				onUnsnap();
			}
			
			return isSnapping;
		}
	}
	
	public static class SnapToGrid extends SnapBehaviour<Positionable> {

		private int gridWidth = 50;
		
		public SnapToGrid(Positionable target) {
			super(target);
			
		}
		
		public void setGridWidth(int gridWidth) {
			this.gridWidth = gridWidth;
		}
		
		@Override
		public boolean applySnap(UserInput in) {
			
			int mx = Math.round((float)in.mouseX / gridWidth) * gridWidth,
				my = Math.round((float)in.mouseY / gridWidth) * gridWidth;
			
			float dx = mx - in.mouseX,
				  dy = my - in.mouseY;
			
			if(Math.sqrt(dx * dx + dy * dy) < snapDistance) {
			
				setPosition(mx, my);
				
				onSnap(new Position(mx, my));
				
				return true;
			}
			onUnsnap();
			return false;
		}
	}
	
	public static class SnapToPath extends SnapBehaviour<PathObject> {
		
		final gui.paths.PosConstraint constraint;
		NullPathObject no = new NullPathObject();
		
		public SnapToPath(Positionable target, gui.paths.PosConstraint constraint) {
			super(target);
			this.constraint = constraint;
		}
		
		@Override
		public boolean applySnap(UserInput in) {
			
			var paths = constraint.getPaths(); 
			
			Path pp = null;
			float x = 0, y = 0, pathPos = 0;
			double minDist = Double.MAX_VALUE;
			
			for (var p : paths) {
				
				double pos = p.nearestPoint(in.mouseX, in.mouseY);
				
				if(pos >= 0 && pos <= 1) {
					
					p.goToPos(no, pos);
					no.setPos(constraint.getX() + no.getX(), constraint.getY() + no.getY());
					
					double d = Positionables.distance(no, in.mousePos);
					
					if(d <= snapDistance && d < minDist) {
						
						minDist = d;
						pp = p;
						x = no.getX();
						y = no.getY();
						pathPos = (float) pos;
					}
				}
			}
			
			if(isSnapping = pp != null) {
				
				var n = new NullPathObject();
				n.setPos(x, y);
				n.setPath(pp);
				n.goToPathPosition(pathPos);
				
				setPosition(x, y);
				
				onSnap(n);
			}else{
				onUnsnap();
			}
			
			return isSnapping;
		}
	}
	
	
}
