package gui;

import static constants.DisplayFlag.CLICKABLE;
import static constants.DisplayFlag.DRAGABLE;
import static constants.DisplayFlag.DRAWABLE;
import static constants.DisplayFlag.DROPABLE;
import static constants.DisplayFlag.fixedToParent;
import static constants.DisplayFlag.onDisplay;
import static gui.selection.GuiType.NO_SELECT;

import java.util.ArrayList;
import java.util.List;

import lombok.val;
import lombok.var;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.Positionable;
import processing.core.PImage;

public final class DisplayObjects {

	private DisplayObjects() {}
	
	
	static public void addChildren(DisplayObjectIF parent, DisplayObjectIF... children) {
		for (int i = 0; i < children.length; i++) {
			parent.addChild(children[i]);
		}
	}
	
	static public void removeChildren(DisplayObjectIF parent) {
		var ch = parent.getChildren();
		var ar = ch.toArray(new DisplayObjectIF[ch.size()]);
		parent.removeChild(ar);
	}
	
	static public int getParentDepth(DisplayObjectIF child, DisplayObjectIF parent) {
		int i = 0;
		var d = child;
		while(d != null) {
			if(d == parent) 
				return i;
			d = d.getParent();
			i++;
		}
		return -1;
	}
	
	static public int getParentDepth(DisplayObjectIF child) {
		return getParentDepth(child, child.getDm().getContainer());
	}

	static public boolean isChildOf(DisplayObjectIF parent, DisplayObjectIF child) {
		
		return getParentDepth(child, parent) > 0;
	}
	
	static public boolean haveChildParentRelationship(DisplayObjectIF d1, DisplayObjectIF d2) {
		
		return isChildOf(d1, d2) || isChildOf(d2, d1);
	}

	public static void setChildIndex(DisplayObjectIF d, int i) {
		
		if(d.getParent() == null)
			return;
		
		final var dd = d.getParent().getChildren();
		
		if(i < 0)
			i = dd.size() + i + 1;
		
		int ni = dd.indexOf(d);
		
		if(ni != i && !(ni < i && ni == dd.size() - 1)) {
			
			if(dd.contains(d) && i >= 0) {
				
				dd.remove(d);
				
				if(i < dd.size())
					dd.add(i, d);
				else
					dd.add(d);
				if(d.getDm() != null)
					d.getDm().listChanged();
			}
		}
		
	}
	
	public static List<DisplayObjectIF> getOffspring(DisplayObjectIF par){
		
		var off = new ArrayList<DisplayObjectIF>();
		
		off.add(par);
		
		int i = 0;
		DisplayObjectIF d;
		
		while(i < off.size()){
			
			d = off.get(i);
			off.addAll(i + 1, d.getChildren());
			i++;
		}
		
		return off;
	}
	
	public static void removeFromDisplay(DisplayObjectIF d) {
		
		var par = d.getParent();
		if(par != null)
			par.removeChild(d);
	}
	
	public static void addSubscriber(DisplayObjectIF d) {
		d.getDm().getInput().addInputEventListener(d);
	}

	public static void removeSubscriber(DisplayObjectIF d) {
		val dm = d.getDm();
		if(dm != null) {
			var in = dm.getInput();
			in.selection.remove(d);
			in.removeInputEventListener(d);
		}
	}
	
	
	public static void fitToChildren(DisplayObjectIF parent) {
		
		int w = 0, h = 0;
		for (var c : getOffspring(parent)) {
			w = Math.max(w, c.getWidth());
			h = Math.max(h, c.getHeight());
		}
		
		parent.setWH(w, h);
	}

	public static DisplayObject createContainer() {
		var d = new DisplayObject();
		d.setFlag(DRAWABLE, false);
		d.setFlag(CLICKABLE, false);
		d.setFlag(DRAGABLE, false);
		d.setFlag(DROPABLE, false);
		d.setFlag(fixedToParent, true);
		d.setGuiType(NO_SELECT);
		return d;
	}


	static public void holdInScreen(DisplayObjectIF d) {
		
		int margin = 10;
		int width = d.getDm().getWidth() - margin;
		int height = d.getDm().getHeight() - margin;
		
		if(d.getX() + d.getWidth() >= width)
			d.setGlobal(width - d.getWidth(), d.getY());
		
		if(d.getX() <= margin)
			d.setGlobal(margin, d.getY());
		
		if(d.getY() + d.getHeight() >= height)
			d.setGlobal(d.getX(), height - d.getHeight());
		
		if(d.getY() <= margin)
			d.setGlobal(d.getX(), margin);
	}


	static public void holdInScreen(Positionable d, int sWidth, int sHeight) {
		
		int margin = 10;
		int width = sWidth - margin;
		int height = sHeight - margin;
		
		if(d.getX() >= width)
			d.setGlobal(width , d.getY());
		
		if(d.getX() <= margin)
			d.setGlobal(margin, d.getY());
		
		if(d.getY() >= height)
			d.setGlobal(d.getX(), height);
		
		if(d.getY() <= margin)
			d.setGlobal(d.getX(), margin);
	}
	
	static public void copyDimensions(DisplayObjectIF target, PImage source) {
		target.setWH(source.width, source.height);
	}
	
	static public void copyDimensions(DisplayObjectIF target, DisplayObjectIF source) {
		target.setWH(source.getWidth(), source.getHeight());
	}


	public static void removedFromDisplayNotifyChildren(DisplayObjectIF d) {
		for(var c : getOffspring(d)) {
			c.setFlag(onDisplay, false);
			c.removedFromDisplay();
		}
	}
}
