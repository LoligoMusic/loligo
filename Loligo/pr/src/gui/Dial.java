package gui;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import pr.RootClass;
import pr.userinput.UserInput;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PGraphics;

public class Dial extends Slider {
	 
	static final private float size = 16;
	
	private float rotX, rotY, sy = 0, pointerSize, gap = 1.5f;
	private double valueTemp;
	private final int mouseRange = 200;
	
	
	public Dial(final Consumer<Double> action, final Supplier<Double> updateAction, String name) {
		
		super(action, updateAction, name);
		
		setFlag(DisplayFlag.ROUND, true);
		
		setWH((int) size, (int) (size * 1.5f + 5));
		
		pointerSize = size / 4;
		
		
		textBox.setPos(-RootClass.mainProc().textWidth(name) / 2, size / 2 + 5);
		
		sender.singleThread();
	}
	
	@Override
	public void render(PGraphics g) {
		
		float x = getX(),
			  y = getY();
		
		g.fill(0xffaaaaaa);
		float p = gap / 2;
		g.arc(x, y, size, size, PConstants.PI - p, PConstants.TWO_PI + PConstants.PI - gap - p);
		
		g.fill(0xff666666);
		g.ellipse(x - 1, y, size - pointerSize, size - pointerSize);
		
		g.fill(0xff333333);
		g.ellipse(x + rotX, y + rotY, pointerSize, pointerSize);
		
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == InputEvent.DRAG_START) {
			sy = getDm().getInput().getMouseY();
			valueTemp = sender.getValue();
		}else
		if(e == InputEvent.DRAG) {
			double v = valueTemp + ((sy - getDm().getInput().getMouseY()) / mouseRange);
			v = v < 0 ? 0 : v > 1 ? 1 : v;
			sender.message(v);
			//send();
		}
	}
	
	
	@Override
	public void setValue(Double v) {
		super.setValue(v);
		updateDial();
	}
	
	
	@Override
	public void update() {
		super.update();
		updateDial();
	}
	
	
	private void updateDial() {
		float w = gap / 2  + (PConstants.TWO_PI - gap) * (1 - getValue().floatValue());
		float fac = size * .5f - pointerSize / 1.8f;
		rotX = PApplet.sin(w) * fac;
		rotY = PApplet.cos(w) * fac;
	}
}
