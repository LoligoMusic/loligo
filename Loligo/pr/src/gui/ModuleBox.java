package gui;

import static gui.text.TextBlock.AlignMode.CENTER;

import constants.DisplayFlag;
import errors.LoligoException;
import gui.selection.GuiType;
import gui.text.TextBlock;
import gui.text.TextBox;
import lombok.Getter;
import module.Module;
import module.Modules;
import pr.ModuleGUI;

public class ModuleBox extends TextBox implements ModuleGUI {

	private TextBlock errorMessage;
	@Getter
	private Module module;
	
	//private DisplayObject selectObj;
	
	
	public ModuleBox(Module m) {
		//super(m);
		
		super(m.getName(), true);
		
		module = m;
		
		bgBox = true;
		autoFit = true;
		setGuiType(GuiType.SIGNAL);
		setFlag(DisplayFlag.CLICKABLE, true);
		
		setAlignMode(CENTER);
		setColor(COLOR_DEFAULT);
	}

	@Override
	public void markError(LoligoException e) {
		
		setColor(COLOR_ERROR);
		
		/*
		errorMessage = new TextBlock();
		errorMessage.setWH(getWidth(), 100);
		errorMessage.setText(e.getMessage());
		*/
	}

	@Override
	public void unmarkError() {
		
		setColor(COLOR_DEFAULT);
		
		if(errorMessage != null) {
			
			
		}
			
	}

	@Override
	public void positionSlots() {
		
		Modules.positionSlotsRect(getModule());
	}

	@Override
	public void setGUIName(String name) {
		setText(name);
	}

	@Override
	public String getGUIName() {
		
		return getText();
	}
	
	/*
	protected DisplayObject createSelection() {
		
		return new SelectionDeko(this);
	}
	

	@Override
	public void setSelected(boolean b) {
		
		if(b) {
			
			if(selectObj == null) {
				selectObj = createSelection();
				selectObj.form = FORM_RECT;
				selectObj.clickable = false;
				selectObj.color = 0x44ffffff;
				selectObj.fixedToParent = true;
			}
			
			addChild(selectObj);
			selectObj.setChildIndex(1);
			selectObj.width = width;
			selectObj.height = height;
			
		}else{
			if(selectObj != null)
				selectObj.removeFromDisplay();
		}
	}
	*/
	
	
	
	
}
