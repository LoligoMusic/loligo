package gui;

import static constants.InputEvent.DRAG;
import static constants.InputEvent.PRESSED;
import static util.Colors.blue;
import static util.Colors.green;
import static util.Colors.red;

import java.awt.Color;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.text.CharProfile;
import gui.text.TextInputField;
import pr.DisplayObject;
import pr.RootClass;
import pr.userinput.UserInput;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PImage;
import util.Colors;

public class ColorPicker extends GUIBox implements DisplayGuiSender<Integer> {
	
	private final HueSlider hueSlider;
	private final Slider alphaSlider;
	private final ColorField colorField;
	private final TextInputField hexField;
	private final GuiSenderObject<Integer> sender;
	
	private int alpha;
	private boolean external = true;
	
	
	public ColorPicker(final Consumer<Integer> action, final Supplier<Integer> updateAction) {
		this(action, updateAction, 0xFFFFFFFF);
	}
	
	
	public ColorPicker(final Consumer<Integer> action, final Supplier<Integer> updateAction, int c) {
		//this.target = target;
		sender = new GuiSenderObject<>(this, action, updateAction, c);
		
		colorField = new ColorField(100, 100);
		
		Consumer<Double> cons = d -> {
			colorField.setHue(d.floatValue());
			sendUserAction();
		};
		
		hueSlider = new HueSlider(cons, ()->(double)colorField.hue, 12, 100);
		
		
		Consumer<Double> ca = d -> {
			alpha = (int)(d * 255);
			sendUserAction();
		};
		
		alphaSlider = new Slider(ca, ()-> alpha / 255d, "");
		alphaSlider.singleThread();
		alphaSlider.setColor(Colors.grey(200));
		alphaSlider.setWH(10, 100);
		
		setTop(1);
		margin = 4;
		barHeight = margin;
		addRow(colorField, hueSlider, alphaSlider);
		
		hexField = new TextInputField(this::setHex, this::getHex, CharProfile.HEX);
		hexField.singleThread();
		hexField.setWH(
			colorField.getWidth() + hueSlider.getWidth() + alphaSlider.getWidth() + margin * 2, 
			hexField.getHeight());
		
		addRow(hexField);
				
		setValue(c);
		sender.update();
	}
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		update();
	}
	
	@Override
	public Integer getValue() {
		return sender.getValue();
	}
	

	@Override
	public void setValue(Integer v) {
		sender.setValue(v);
		
		if(external) {
			alpha = Colors.alpha(v);
			colorField.setRGB(v);
			hueSlider.update();
			alphaSlider.update();
			hexField.update();
		}
		
		external = true;
	}
	
	
	private void setHex(String hex) {
		try {
			int v = Colors.hex(hex);
			setValue(v);
			sendUserAction();
		} catch(NumberFormatException e) {
			System.err.println(e);
			hexField.update();
		}
	}
	
	
	private String getHex() {
		return Colors.toHexString(getValue());
	}
	
	
	@Override
	public void disable(boolean b) {
		
		hueSlider.disable(b);
		alphaSlider.disable(b);
		colorField.active = !b;
		hexField.disable(b);
	}
	
	
	@Override
	public void update() {
		sender.update();
	}

	
	private int getPickedColor() {
		int c = Color.HSBtoRGB(colorField.hue, colorField.saturation, colorField.brightness);
		return Colors.setAlpha(c, alpha / 255f);
	}
	
	
	private void sendUserAction() {
 		external = false;
		sender.message(getPickedColor());
	}
	
	
	class HueSlider extends Slider {
		
		private PImage img;
		
		public HueSlider(Consumer<Double> action, Supplier<Double> updateAction, int w, int h) {
		
			super(action, updateAction, "");
			
			vertical = true;
			setColor(Colors.grey(0, 0));
			setWH(w, h);
			singleThread();
		}
		
		@Override public void render(PGraphics g) {
			
			g.image(img, getX(), getY());
			float h = getY() + (1 - sender.getValue().floatValue()) * getHeight();
			g.stroke(active ? 0xFFFFFFFF : 0xFF555555);
			g.strokeWeight(2);
			g.line(getX() - 1, h, getX() + getWidth(), h);
			g.noStroke();
		}
		
		@Override
		public void addedToDisplay() {
			
			drawGradient();
		}
		
		private void drawGradient() {
			
			img = getDm().getGuiDomain().getPApplet().createImage(getWidth(), getHeight(), PConstants.RGB);
			
			for (int i = 0; i < getWidth(); i++) 
				for (int j = 0; j < getHeight(); j++) {
					
					int c = Color.HSBtoRGB(1 - (float)j / getHeight(), 1, 1);
					img.pixels[i + j * getWidth()] = c;
				}
			
			img.updatePixels();
		}
		
	}
	
	
	
	class ColorField extends DisplayObject {
		
		private float hue = 0, brightness = .5f, saturation = .5f;
		private boolean active = true;
		private PImage img;
		
		public ColorField(int w, int h) {
			
			setFlag(DisplayFlag.RECT, true);
			setWH(w, h);
			
		}
		
		@Override
		public void setWH(int width, int height) {
			
			super.setWH(width, height);
			img = RootClass.mainProc().createImage(getWidth(), getHeight(), PConstants.RGB);
		}
		
		@Override public void render(PGraphics g) {
			
			g.image(img, getX(), getY());
			g.fill(active ? 200 : 100);
			g.stroke(30);
			g.strokeWeight(1);
			g.ellipse(getX() + saturation * getWidth(), getY() + brightness * getHeight(), 5, 5);
			g.noStroke();
		}
		
		public void setRGB(int c) {
			float[] hsb = Color.RGBtoHSB(red(c), green(c), blue(c), null);
			setHSB(hsb[0], hsb[1], hsb[2]);
		}
		
		public void setHue(float hue) {
			
			this.hue = hue;
			drawField();
		}
		
		public void setHSB(float h, float s, float b) {
			
			hue = b > 0 && s > 0 ? h : hue;
			brightness = b;
			saturation = b > 0 ? s : saturation;
			drawField();
		}
		
		private void drawField() {
			
			final int[] pixels = img.pixels;
			int i, j;
			
			for (int k = 0; k < pixels.length; k++) { 
				i = k % img.width;
				j = k / img.width;
				pixels[k] = Color.HSBtoRGB(hue, (float)i / img.width, (float)j / img.height);
			}
			img.updatePixels();
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			
			if(e == PRESSED || e == DRAG) {
				
				if(active) {
					saturation = (input.getMouseX() - getX()) / getWidth();
					brightness = (input.getMouseY() - getY()) / getHeight();
					saturation = saturation > 1 ? 1 : saturation < 0 ? 0 : saturation;
					brightness = brightness > 1 ? 1 : brightness < 0 ? 0 : brightness;
					//colorValue = Color.HSBtoRGB(hueSlider.getValue().floatValue(), saturation, brightness);
					
					sendUserAction();
					
					//guiMessage(null);
				}
			}
		}
		
		
		
	}

	

}
