package audio;

import java.util.EnumSet;

public enum AudioType{ 
	
	SOURCE, 
	EFFECT, 
	STACK, 
	DUMMY, 
	CONTROL;
	
	private final static EnumSet<AudioType> stackables = EnumSet.of(SOURCE, EFFECT, STACK);

	public boolean isStackable()
	{
		return stackables.contains(this);
	}

}

