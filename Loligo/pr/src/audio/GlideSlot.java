package audio;

import module.inout.NumberInputIF;
import net.beadsproject.beads.ugens.Glide;
import util.Notes;

public class GlideSlot {
	
	final NumberInputIF slot;
	final Glide glide;
	final float offset, fac;
	boolean isUpdateImmediately, isPitch;
	
	public GlideSlot(NumberInputIF slot, Glide glide, float offset, float fac) {
		this.glide = glide;
		this.slot = slot;
		this.offset = offset;
		this.fac = fac;
	
		//glide.setValueImmediately(value());
	}
	
	void init() {
		//if(isUpdateImmediately)
			updateImmediately();
	}
	
	public GlideSlot setUpdateImmediately() {
		isUpdateImmediately = true;
		return this;
	}
	
	public GlideSlot setIsPitch(){
		isPitch = true;
		return this;
	}
	
	public float value() {
		
		if(isPitch) {
			float f = Notes.toFrequency(slot.getWildInput());
			return f;
		}
		
		float f = offset + (float) slot.getInput() * fac;
		return f;
	}
	
	public void update() {
		
		glide.setValue(value());
	}
	
	public void updateImmediately() {
		
		glide.setValueImmediately(value());
	}
}