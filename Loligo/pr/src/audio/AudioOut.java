package audio;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.Plug;
import pr.RootClass;

public class AudioOut {

	private final Plug plug;
	
	public AudioOut() {
	
		plug = new Plug(RootClass.audioManager().getAC(), 1);
	}


	public UGen getMasterBead() {
		return plug;
	}

	
	public void addInput(UGen ugen) {
		plug.addInput(ugen);
	}

	
	public void removeInput(UGen ugen) {
		plug.removeAllConnections(ugen);
	}

	public void connect() {
		RootClass.audioManager().addInput(plug);
		
	}
	
	public void disconnect() {
		RootClass.audioManager().removeInput(plug);
	}


	public AudioContext getAC() {
		
		return RootClass.audioManager().getAC();
	}
	

}
