package audio;

import lombok.Getter;
import module.inout.NumberInputIF;
import net.beadsproject.beads.ugens.Glide;

public class GlideInput {
	
	private final NumberInputIF input;
	@Getter
	private final Glide glide;
	private final float offset, fac;
	
	
	public GlideInput(NumberInputIF input, Glide glide) {
		this(input, glide, 0, 1);
	}
	
	public GlideInput(NumberInputIF input, Glide glide, float offset, float fac) {
	
		this.input = input;
		this.glide = glide;
		this.offset = offset;
		this.fac = fac;
	}

	public void setValue(double value) {
		input.setValue(value);
	}

	public double getWildInput() {
		
		double d = input.getWildInput();
		glide.setValue((float) d * fac + offset);
		
		return d;
	}

	public double getInput() {
		
		double d = input.getInput();
		glide.setValue((float) d * fac + offset);
		
		return d;
	}

}
