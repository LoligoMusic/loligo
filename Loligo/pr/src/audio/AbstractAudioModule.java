package audio;

import static audio.AudioType.SOURCE;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import module.AbstractModule;
import module.inout.AudioInput;
import module.inout.AudioOutput;
import module.inout.NumericAttribute;
import net.beadsproject.beads.core.AudioContext;

public abstract class AbstractAudioModule extends AbstractModule {

	protected List<AudioInput> audioInputs = new ArrayList<>();
	@Getter@Setter
	protected AudioType audioType = SOURCE;
	protected AudioContext audioContext;
	
	
	@Override
	public void postInit() {
		
		audioContext = getDomain().getAudioManager().getAC();
		beadConstruct();
		super.postInit();
	}
	
	
	public abstract void beadConstruct();
	
	
	@Override
	public void processIO() {
		for(var a : audioInputs) {
			a.updateAudio();
		}
	}
	
	
	protected AudioInput addControlInput(String name, NumericAttribute attr) {
		val in = new AudioInput(name, this, attr);
		addIO(in);
		return in;
	}
	
	protected AudioOutput addControlOutput(String name) {
		val o = new AudioOutput(name, this);
		addIO(o);
		return o;
	}
	
	public void addAudioInput(AudioInput in) {
		audioInputs.add(in);
	}
	
	public void removeAudioInput(AudioInput in) {
		audioInputs.remove(in);
	}

}
