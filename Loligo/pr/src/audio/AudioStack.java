package audio;

import static audio.AudioType.EFFECT;
import static audio.AudioType.STACK;
import static constants.AudioStackContext.Type.*;
import java.util.ArrayList;
import java.util.List;

import constants.AudioStackContext;
import constants.DisplayFlag;
import gui.Position;
import module.Module;
import module.ModuleContainer;
import module.ModuleDeleteMode;
import module.Modules;
import module.inout.AudioInput;
import module.inout.NumericAttribute;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Throughput;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import save.ModuleData;
import save.SaveDataContext;
import save.StackData;
import util.Colors;

public class AudioStack extends AudioModule implements ModuleContainer {
	
	final static public String audioStackName = "AudioStack";
	final static private int STACKCOLOR = 0xff282828;
	final static public ArrayList<AudioStack> audioStacks = new ArrayList<AudioStack>();
	
	public List<AudioModule> list = new ArrayList<AudioModule>();
	
	transient Gain outGain;
	protected int gainHeight = 20, barHeight = 2;
	protected boolean killOnEmpty = true;
	AudioModule first, last;
	//protected AudioModule m1, m2;
	private AudioInput in_gain;
	private int lastEventStackIndex = -1;
	
	
	public static void createStack(AudioModule m1, AudioModule m2) {
		
		AudioStack s = new AudioStack();
		Modules.initializeModuleDefault(s, m1.getDomain());
		
		float x = 0, y = 0;
		
		if(m1.getGui() != null) {
			x = m1.getGui().getX();
			y = m1.getGui().getY();
		}
		
		s.addModule(m1);
		s.addModule(m2);
		
		if(s.getGui() != null) {
			
			s.getGui().setPos(x, y);
		}
	
	}
	
	
	public AudioStack() {
		
		in_gain = addControlInput("Gain", NumericAttribute.DEZIMAL_POSITIVE).setDefaultValueAudio(1);
		//in_gain.setValue(1);
		setAudioType(STACK);
		setName(audioStackName);
	}
	
	
	@Override
	public ModuleGUI createGUI() {
		
		ModuleGUI d = super.createGUI();
		
		d.setColor(STACKCOLOR);
		/*
		for (AudioModule m: list) {
			if(m.gui != null) {
				m.createGUI();
				
				m.gui.fixedToParent = true;
				gui.addChild(m.gui);
			}
		}*/
		
		return d;
	}
	
	@Override
	public void postInit() {
		super.postInit();
	}
	
	@Override
	public void beadConstruct() {
		if(!audioStacks.contains(this))
			audioStacks.add(this);

		outGain = new Gain(audioContext, 1, AudioManager.DUMMY_UGEN);
		in_gain.onChangeAudio(outGain::setGain);
		
		beadIn = new Throughput(audioContext);
		beadOut = outGain;
	}
	
	public void addModule(AudioModule after, AudioModule m) {
		addModule(m, list.indexOf(after) + 1);
	}
	
	public void addModule(AudioModule m) {
		addModule(m, -1);
	}
	
	public void addModule(AudioModule m, int index) {
		
		if(m == null) 
			return;
		
		resetConnections();

		if(m.getStack() != null)
			if(m.getStack() == this || list.contains(m))
				removeModule(m);
			else 
				m.getStack().removeModule(m);
		
		m.setStack(this);
		
		if(index >= 0 && index < list.size())
			list.add(index, m);
		else
			list.add(m);
		
		
		plug();
		
		DisplayObjectIF g = m.getGui();
		if(g != null) {
			g.setFlag(DisplayFlag.lockedPosition, true);
			g.setFlag(DisplayFlag.fixedToParent, true);
			this.getGui().addChild(m.getGui());
		
		}
		
		m.addedToStack();
		
		int li = list.indexOf(m);
		AudioModule lm = li > 0 ? list.get(li - 1) : null;
		
		AudioStackContext.instance.dispatchAudioStackEvent(AUDIOSTACK_ADD, m, lm, this, li);
	}
	
	public void removeModule(AudioModule m) {
		
		int ind = list.indexOf(m);
		
		if(ind == -1) {
			return;
		}
		
		AudioModule lm = ind > 0 ? list.get(ind - 1) : null;
			
		resetConnections();
		
		list.remove(m);
		m.setStack(null);
		
		if(m.beadIn != null)
			m.beadIn.clearInputConnections();
		
		plug();
		
		DisplayObjectIF g = m.getGui();
		if(g != null) {
			g.setFlag(DisplayFlag.lockedPosition, false);
			g.setFlag(DisplayFlag.fixedToParent, false);
			g.setPosObject(new Position(m.getGui().getX(), m.getGui().getY()));
			
			if(getDomain().getContainedModules().contains(m))
			this.getGui().getDm().addModuleDisplay(m);
		}
		
		m.removedFromStack();
		AudioStackContext.instance.dispatchAudioStackEvent(AUDIOSTACK_REMOVE, m, lm, this, ind);
		
		if(killOnEmpty && list.size() < 2)
			delete();
	}
	
	public void plug() {
		if(list.size() == 0) {
			outGain.addInput(beadIn);
		}else{
			for(int j = 0; j < list.size() - 1; j++) {
				AudioModule m1 = list.get(j);
				for (int i = j + 1; i < list.size(); i++) {
					AudioModule m2 = list.get(i);
					if(m2.getAudioType() == EFFECT) {
						m2.beadIn.addInput(m1.beadOut);
						break;
					}
					if(i == list.size() - 1) 
						outGain.addInput(m1.beadOut);
				}
			}
			first = list.get(0);
			last = list.get(list.size() - 1);
			first.beadIn.addInput(beadIn);//TODO quellen überspringen
			outGain.addInput(last.beadOut);
		}
		if(getGui() != null)
			updateGUI();
		
	}

	private void resetConnections() {
		for (int i = 1; i < list.size(); i++) {
			AudioModule m = list.get(i);
			if(m.beadIn != null)
				m.beadIn.clearInputConnections();
		}
		outGain.clearInputConnections();
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		
		super.onDelete(mode);
		
		resetConnections();
		for(int i = 0; i < list.size(); i++) {
			AudioModule m = list.get(0);
			removeModule(m);
		}
		outGain.kill();
		audioStacks.remove(this);
	}
	
	@Override
	public void addedToStack() {
		//audioContext.out.removeConnection(0, outGain, 0);
		//audioContext.out.removeConnection(1, outGain, 0);
		getDomain().getAudioManager().removeInput(outGain);
		
		if(gui != null)
			updateGUI();
	}
	
	@Override
	public void removedFromStack() {
		if(gui != null)
			getGui().setColor(STACKCOLOR);
		getDomain().getAudioManager().addInput(outGain);
	}
	
	public int getLastEventStackIndex() {
		
		return lastEventStackIndex;
	}
	
	protected void updateGUI() {
		
		int h = barHeight, w = 0;
		
		for(AudioModule m : list) {
			m.getGui().setPos(0, h);
			h += m.getGui().getHeight();
			if(m.getGui().getWidth() > w)
				w = m.getGui().getWidth();
		}
		
		getGui().setWH(w + 4, h + gainHeight);
		
		positionSlots();
		
		if(getStack() != null) {
			getStack().updateGUI();
			getGui().setColor(Colors.grey(Colors.brightness(getStack().getGui().getColor()) + 20));
		}
	}
	
	/*
	@Override
	public AbstractModule duplicate() {
		AudioStack s = (AudioStack) super.duplicate();
		for (AudioModule m : list) 
			s.addModule((AudioModule) m.duplicate());
		s.killOnEmpty = killOnEmpty;
		return s;
	}
	*/
	
	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc);
		
		//ModuleContainerData mcd = new ModuleContainerData(md);
		StackData sd = new StackData();
		sd.stack = md;
		
		list.stream().forEachOrdered(
			m -> {
				ModuleData d = sdc.findData(m);
				//mcd.addModuleData(d);
				sd.nodes.add(d);
			}
		);
		
		//sdc.putData(mcd);
		
		sdc.addStack(sd);
		
		return md;
	}

	@Override
	public List<AudioModule> getContainedModules() {
		return list;
	}

	@Override
	public Module createInstance() {
		return new AudioStack();
	}

	@Override
	public boolean addModule(Module m) {
		
		if(m instanceof AudioModule s) {
			addModule(s);
			return true;
		}else
			return false;
	}

	@Override
	public boolean removeModule(Module m, ModuleDeleteMode mode) {
		
		if(m instanceof AudioModule s) {
			removeModule(s);
			return true;
		}else
			return false;
	}
	
	/*
	@Override
	public boolean isSubModule() {
		return true;
	}*/
}
