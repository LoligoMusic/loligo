package audio;

import java.io.File;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.RecordToFile;

public class Recorder {

	private RecordToFile rec;
	private boolean isRecording = false;
	private final UGen audioIn;
	private final AudioContext ac;
	
	public Recorder(AudioContext ac, UGen audioIn) {
		this.ac = ac;
		this.audioIn = audioIn;
	}
	
	
	public void startRecord(File file) {
		
		if(isRecording)
			return;
		
		//File f = new File(Utils.getJarPath(), "record_xy.wav");
		isRecording = true;
		
		//AudioFormat af = new AudioFormat(44100.0f, 16, 1, true, true);
		//Sample outputSample = new Sample(5000, 1);  //TODO  API changes .. no idea if this still works
		try {
			rec = new RecordToFile(ac, 1, file); 
			rec.addInput(audioIn);
			audioIn.addDependent(rec);
			
			//rec.pause(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void stopRecord() {
		
		if(!isRecording)
			return;
		
		isRecording = false;
		
		rec.pause(true);
		//rec.clip();
		
		/*
		
		if(s != null) {
		
			new Thread(() -> {
				
					if (s != null) {
						
						Sample samp = rec.getSample();
						
						try {
							samp.write(s.getAbsolutePath(), AudioFileType.WAV); //TODO  API changes ..
							
						} catch (Exception e) {
							
							e.printStackTrace();
						}finally {
							samp.clear();
						}
						
					}
				
				rec = null;
				
			}).start();
			
		}
		*/
		
		rec.removeAllConnections(audioIn);
		audioIn.removeDependent(rec);
		rec.kill();
	}
	
	/*
	public void cancelRecord() {
		isRecording = false;
		//rec.getSample().clear();
		rec.removeAllConnections(audioIn);
		audioIn.removeDependent(rec);
		rec.kill();
		
	}
	*/
	
	public boolean isRecording() {
		
		return isRecording;
	}
}
