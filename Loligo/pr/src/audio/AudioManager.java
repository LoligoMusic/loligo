package audio;

import java.util.Arrays;
import java.util.stream.Collectors;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.Mixer.Info;

import lombok.Getter;
import lombok.val;
import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.core.io.JavaSoundAudioIO;
import net.beadsproject.beads.ugens.Plug;
import net.beadsproject.beads.ugens.Static;
import save.Settings;
import util.ExceptionHandler;


public final class AudioManager {
	
	public static Static DUMMY_UGEN;
	
	private Plug masterPlug;
	private float vol;
	
	private AudioContext ac;
	@Getter
	private boolean hasValidSoundDevice = false;
	
	
	public AudioManager() {
		
	}
	
	final private void plugIn() {
		ac.out.addInput(1, masterPlug, 0);
		ac.out.addInput(0, masterPlug, 0);
	}
	
	
	final private void plugOut() {
		ac.out.removeAllConnections(masterPlug);
	}
	
	
	public void initAudio() {
		val s = new JavaSoundAudioIO();
		String dev = Settings.getSoundDevice();
		int mi = dev == null ? 0 : mixerIndex(dev);
		s.selectMixer(mi);
		initAudio(s);
	}
	
	public int mixerIndex(String name) {
		val mxs = Arrays.stream(AudioSystem.getMixerInfo())
						.map(Info::getName)
						.collect(Collectors.toList());
		return mxs.indexOf(name);
	}
			
	
	public void initAudio(JavaSoundAudioIO ios) {
		
		stop(); 
		
		ExceptionHandler.addExceptionListener(e -> {
			val m = e.getMessage();
			if(m != null && m.startsWith("Line unsupported")) {
				System.out.println("Audio Device unsupported!");
				hasValidSoundDevice = false;
				return true;
			}
			return false;
		});
		
		hasValidSoundDevice = true;
		
		ac = new AudioContext(ios);
		masterPlug = new Plug(ac);
		ac.start();
		
		plugIn();
		vol = ac.out.getGain();
		
		DUMMY_UGEN = new Static(ac, 0);
	}
	
	public void stop() {
		
		if(ac != null) {
			plugOut();
			ac.stop();
		}
	}
	
	
	public AudioContext getAC() {
		return ac;
	}
	
	public Mixer getMixer() {
		val ios = (JavaSoundAudioIO) getAC().getAudioIO();
		return ios.getMixer();
	}
	
	
	public UGen getMasterBead() {
		return masterPlug;
	}
	
	
	public void addInput(UGen ugen) {
		
		masterPlug.addInput(ugen);
	}
	
	public void removeInput(UGen ugen) {
		
		masterPlug.removeAllConnections(ugen);
	}
	
	public void mute() {
		
		ac.out.setGain(0);
	}
	
	public void setVolume(float v) {
		
		vol = v;
		ac.out.setGain(v);
	}
	
	public float getVolume() {
		return ac.out.getGain();
	}
	
	public void unmute() {
		
		ac.out.setGain(vol);
	}
	
	public void toggleMute() {
		
		if(isActive())
			mute();
		else
			unmute();
	}
	
	public boolean isActive() {
		return getVolume() > 0;
	}
	
	
}
