package audio;

import static audio.AudioType.SOURCE;
import static audio.AudioType.STACK;

import java.util.ArrayList;
import java.util.List;

import constants.DisplayFlag;
import constants.Flow;
import constants.InputEvent;
import constants.Timer;
import gui.DisplayObjects;
import gui.selection.GuiType;
import gui.text.TextBox;
import module.ModuleDeleteMode;
import module.ModuleType;
import module.inout.NumberInputIF;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.Plug;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import pr.ModuleGUIWrapper;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;


public abstract class AudioModule extends AbstractAudioModule {
    
	public UGen beadOut, beadIn;
	public AudioModule inPartner, outPartner;
	protected List<GlideSlot> glideSlots;
	private AudioStack stack;
	private DisplayObject highlight;
	
	
	public AudioModule() {
		flowFlags.add(Flow.SINK);
	}
	
	
	protected GlideSlot addGlideInput(NumberInputIF slot, Glide glide) {
		
		return addGlideInput(slot, glide, 0, 1);
	}
	
	protected GlideSlot addGlideInput(NumberInputIF slot, Glide glide, float offset, float fac) {
		if(glideSlots == null)
			glideSlots = new ArrayList<>();
		
		GlideSlot s = new GlideSlot(slot, glide, offset, fac);
		glideSlots.add(s);
		
		return s;
	}
	
	@Override
	public ModuleGUI createGUI() {
		
		final float rad = 6;
		
		TextBox t = ModuleGUI.createModuleTextBox(this);
		t.bgBox = false;
		t.setGuiType(GuiType.AUDIO);
		
		ModuleGUIWrapper box = new ModuleGUIWrapper(this, t) {
			
			@Override public void render(PGraphics g) {
				g.fill(getColor());
				g.noStroke();
				g.rect(getX(),  getY(), getWidth(), getHeight(), rad, rad, rad, rad);
				
				super.render(g);
			}
			
			@SuppressWarnings("incomplete-switch")
			@Override public void mouseEventThis(InputEvent e, UserInput input) {
				
				super.mouseEventThis(e, input);
				
				if(e.isDropEvent) {
					
					DisplayObjectIF drop = input.getDropTarget();
					DisplayObjectIF drag = input.getDragTarget();
					
					switch(e) {
					case DROPPED -> {
						var am = tryGetAudioModule(drag);
						if(am != null) {
							AudioModule.this.appendAudioModule(am);
						}
						DisplayObjects.removeFromDisplay(highlight);
					}
					case DROP_OVER -> {
						if(drop != null && tryGetAudioModule(drag) != null) 
							drop.addChild(highlight);
					}
					case DROP_STOP -> DisplayObjects.removeFromDisplay(highlight);
					}
				}else
				if(e == InputEvent.DRAG_START) {
					
					if(getStack() != null && getAudioType().isStackable() && input.selection.getNumSelected() == 1) 
						getStack().removeModule(AudioModule.this);
				}
			}
			
			AudioModule tryGetAudioModule (DisplayObjectIF d) {
				var m = d.getDm().getDomain().getModuleManager().getModuleFromGui(d);
				return m instanceof AudioModule a && a.getAudioType().isStackable() ? a : null;
			}
 		};
		
		
		
		
		box.setWH(80, 25);
		box.setSelectionObject(g -> g.rect(box.getX(),  box.getY(), box.getWidth(), box.getHeight(), rad, rad, rad, rad));
		
		box.setGuiType(GuiType.AUDIO);
		
		//box.width = 70;
		//box.children.get(0).rx = g.width - 10;
		//box.height = 30;
		box.setFlag(DisplayFlag.RECT, true);
		box.setFlag(DisplayFlag.DROPABLE, true);
		//box.setFlag(DisplayFlag.Subscriber, true);
		//getDomain().getDisplayManager().getInput().addInputEventListener(box);
		
		highlight = new DisplayObject() {
			
			@Override public void render(PGraphics g) {
				if(getParent() == null)
					return;
				g.stroke(200);
				g.strokeWeight(1);
				float vy = getParent().getY() + getParent().getHeight();
				g.line(getParent().getX(), vy, getParent().getX() + getParent().getWidth(), vy);
				g.line(getParent().getX(), vy - 5, getParent().getX() , vy);
			}
		};
		
		return box;
	}
	
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		
		super.onDelete(mode);
		
		//getDomain().getDisplayManager().getInput().removeInputEventListener(box);
		
		removeFromStack();
		disconnectFromMasterOut();
		
	}
	
	
	
	@Override
	public void processIO() { 
		
		super.processIO();
		
		if(glideSlots != null) 
			
			for (GlideSlot g : glideSlots) 
				
				if(g.slot.changed()) {
					
					g.update();
				}
	}
	
	
	public void connectToMasterOut() {
		
		if(beadOut != null && getStack() == null && (getAudioType() == SOURCE || getAudioType() == STACK)) {
			getDomain().getAudioManager().addInput(beadOut);
		
		}
	}
	
	public void removeFromStack() {
		if(getStack() != null)
			getStack().removeModule(this);
	}
	
	public void appendAudioModule(AudioModule m) {
		
		if(getStack() == null) {
			AudioStack.createStack(this, m);
			//ModuleManager.newModule(new AudioStack(this, m));
			
		}else if(getStack() != this) {
			getStack().addModule(this, m);
			
			
		}
	}
	
	@Override
	public void onAdd() {
		
		super.onAdd();
		
		Timer.createFrameDelay(getDomain(), this::onExitInitialFrame).start();
	}
	
	
	public void disconnectFromMasterOut() {
		
		getDomain().getAudioManager().removeInput(beadOut);
	}
	
	
	public void addedToStack(){
		
		disconnectFromMasterOut();
	}
	
	
	public void removedFromStack(){
		
		connectToMasterOut();
	}
	
	
	protected void build(UGen... ugens) {
		
		build(null, ugens);
	}
	
	protected void build(NumberInputIF in, UGen... ugens) {
		
		if(ugens.length > 1)
			
			for (int i = 1; i < ugens.length; i++) {
				ugens[i].addInput(ugens[i - 1]);
			}
		
		if(in != null) {
			
			beadIn  = new Plug(audioContext, 1);
			beadOut = new Plug(audioContext, 1);
			
			Gain dry = new Gain(audioContext, 1),
				 wet = new Gain(audioContext, 1);
			
			wet.addInput(ugens[ugens.length - 1]);
			dry.addInput(beadIn);
			ugens[0].addInput(beadIn);
			
			beadOut.addInput(dry);
			beadOut.addInput(wet);
			
			MixGlide mg = new MixGlide(audioContext);
			wet.setGain(mg);
			dry.setGain(mg.getMixPartner());
			
			addGlideInput(in, mg);
			
		}else{
			
			beadIn = ugens[0];
			beadOut = ugens[ugens.length - 1];
		}
	}
	
	@Override
	public boolean checkType(ModuleType t) {
		return t == ModuleType.AUDIO;
	}
	
	public AudioStack getStack() {
		return stack;
	}

	public void setStack(AudioStack stack) {
		this.stack = stack;
	}
	
	public void insertFreqUgen(UGen ugen) {
		
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		super.doDeserialize(data, rdc);
	}

	
	private void onExitInitialFrame(Timer t) {
		
		onExitInitialFrame();
	}
	
	
	protected void onExitInitialFrame() {
		
		if(glideSlots != null)
			glideSlots.forEach(GlideSlot::init);
		
		connectToMasterOut();
		
	}
	
	
	protected void crossFader(UGen effect, NumberInputIF in_fade) {
		
		beadIn = new Plug(audioContext, 1);
		beadOut = new Plug(audioContext, 1);
		
		MixGlide wetGlide = new MixGlide(audioContext);
		addGlideInput(in_fade, wetGlide);
		
		Gain gain_dry = new Gain(audioContext, 1, wetGlide.getMixPartner()),
			 gain_wet = new Gain(audioContext, 1, wetGlide);
		
		effect.addInput(beadIn);
		gain_dry.addInput(beadIn);
		gain_wet.addInput(effect);
		beadOut.addInput(gain_wet);
		beadOut.addInput(gain_dry);
	}

	
	
}


