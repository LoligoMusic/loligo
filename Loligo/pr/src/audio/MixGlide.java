package audio;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.ugens.Glide;

public class MixGlide extends Glide {
	final private Glide mixPartner;
	
	public MixGlide(AudioContext ac) {
		super(ac, .5f, 30);
		mixPartner = new Glide(ac, .5f, 30);
	}
	
	public MixGlide(AudioContext ac, Glide mixPartner, float currentValue, float glideTimeMS) {
		super(ac, currentValue, glideTimeMS);
		this.mixPartner = mixPartner;
		mixPartner.setGlideTime(glideTimeMS);
		mixPartner.setValueImmediately(1 - currentValue);
	}
	
	public Glide getMixPartner() {
		return mixPartner;
	}
	 
	@Override
	public void setGlideTime(float msTime) {
		super.setGlideTime(msTime);
		if(mixPartner != null)
			mixPartner.setGlideTime(msTime);
	}
    
	@Override
	public void setValue(float targetValue) {
		super.setValue(targetValue);
		mixPartner.setValue(1 - targetValue);
	}
    
	@Override
	public void setValueImmediately(float targetValue) {
		super.setValueImmediately(targetValue);
		mixPartner.setValueImmediately(1 - targetValue);
	}
}