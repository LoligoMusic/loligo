package timeLine;

import java.util.List;

import pr.Positionable;


public class TimeLine {

	private List<? extends Positionable> vertices;
	
	
	public TimeLine(List<? extends Positionable> vertices) {
	
		this.vertices = vertices;
		
	}
	
	
	public void setVertices(List<? extends Positionable> vertices) {
		this.vertices = vertices;
	}
	
	
	public List<? extends Positionable> getVertices() {
		return vertices;
	}
	
	
	
	
	
}
