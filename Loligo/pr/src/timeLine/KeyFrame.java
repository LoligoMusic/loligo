package timeLine;

public class KeyFrame <T>{

	private double pos;
	private T value;
	
	public KeyFrame(final T value, final double pos) {
		
		this.pos = pos;
		this.value = value;
	}

	public double getPos() {
		return pos;
	}

	public void setPos(double pos) {
		this.pos = pos;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
	
	
	
	
}
