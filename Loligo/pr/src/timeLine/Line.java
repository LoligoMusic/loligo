package timeLine;

import java.util.ArrayList;
import java.util.List;

public abstract class Line <T>{

	List<KeyFrame<T>> keyFrames;
	
	public Line(List<KeyFrame<T>> keyFrames) {
		
		this.keyFrames = keyFrames;
	}
	
	public Line() {
		
		keyFrames = new ArrayList<>();
	}
	
	
	public void addKeyFrame(final T value, final double pos) {
		
		 KeyFrame<T> k = new KeyFrame<T>(value, pos);
		 
		 keyFrames.add(posToIndex(pos), k);
	}
	
	void updateIndex(final KeyFrame<T> key) {
		
		int oldIndex = keyFrames.indexOf(key);
		
		if(oldIndex == -1)
			return;
		
		int index = posToIndex(key.getPos());
		
		if(oldIndex != index) {
			
			keyFrames.remove(oldIndex);
			keyFrames.add(index, key);
		}
		
	}
	
	private int posToIndex(final double pos) {
		
		int i = 0;
		
		for (; i < keyFrames.size(); i++) 
			if(pos < keyFrames.get(i).getPos()) 
				break;
		
		return i;
		
	}
	
	
	public T calculateValue(final double pos) {
		
		int size = keyFrames.size();
		
		if(size == 0)
			return getDefaultValue();
		
		int index = posToIndex(pos);
			
		if(index >= size)
			return keyFrames.get(size - 1).getValue();
		if(size == 1 || index == 0)
			return keyFrames.get(0).getValue();
		
		
		KeyFrame<T> k1 = keyFrames.get(index - 1);
		KeyFrame<T> k2 = keyFrames.get(index);
		
		double fac = (pos - k1.getPos()) / (k2.getPos() - k1.getPos());
		
		return interpolate(k1.getValue(), k2.getValue(), fac);
		
	}
	
	protected abstract T interpolate(T v1, T v2, double fac);
	
	protected abstract T getDefaultValue();
}
