package timeLine;


public class FloatLine extends Line<Float> {

	
	@Override
	protected Float interpolate(Float v1, Float v2, double fac) {
		
		return v1 + (v2 - v1) * (float)fac;
	}

	@Override
	protected Float getDefaultValue() {
		
		return 0f;
	}

}
