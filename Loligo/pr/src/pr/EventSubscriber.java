package pr;

import constants.InputEvent;
import pr.userinput.UserInput;

public interface EventSubscriber {
	public void mouseEvent(InputEvent e, UserInput input);
}
