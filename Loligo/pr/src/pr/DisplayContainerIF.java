package pr;

import java.util.List;

import module.Module;
import patch.Domain;
import patch.GuiDomain;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public interface DisplayContainerIF extends DisplayObjectIF{

	void addGuiObject(DisplayObjectIF d);

	void draw();

	void updateDisplayList();

	void addModuleDisplay(Module m);

	void addedToDisplay();

	void removedFromDisplay();

	void listChanged();

	void resetListChanged();

	boolean getListChanged();

	Domain getDomain();
	
	GuiDomain getGuiDomain();

	UserInput getInput();

	void setInput(UserInput input);

	DisplayObjectIF getContainer();

	PGraphics getGraphics();

	List<DisplayObjectIF> getGuiList();

	void initGraphics(PGraphics g);
	
	int getFrameCount();

	void dispatchEnterFrameEvent();

	void dispatchExitFrameEvent();
	
	Transform getTransform();

	void addEnterFrameListener(EnterFrameListener f);

	boolean removeEnterFrameListener(EnterFrameListener f);

	void addExitFrameListener(ExitFrameListener f);

	boolean removeExitFrameListener(ExitFrameListener f);
	
}