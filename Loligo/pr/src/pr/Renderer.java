package pr;

import processing.core.PGraphics;

public interface Renderer {
	void render(PGraphics g);
}
