package pr;

import gui.LineContainer;
import gui.MenuBar;
import lombok.var;
import module.Canvas;
import patch.LoligoPatch;
import processing.core.PGraphics;

public interface DisplayManagerIF extends DisplayContainerIF{

	public static void togglePause(LoligoPatch p) {
		var dm = p.getDisplayManager();
		boolean b = dm.isProcessActive();
		dm.pauseProcessing(b);
	}
	
	public static void toggleGui(LoligoPatch p) {
		var dm = p.getDisplayManager();
		boolean b = !dm.isGuiActive();
		dm.setGuiActive(b);
	}
	
	
	void addEnterFrameListener(EnterFrameListener f);

	boolean removeEnterFrameListener(EnterFrameListener f);

	void addExitFrameListener(ExitFrameListener f);

	boolean removeExitFrameListener(ExitFrameListener f);

	void pauseProcessing(boolean pause);

	boolean isProcessActive();

	void setCanvas(Canvas canvas);

	Canvas canvas();

	void removeCanvas();

	void addAnimObject(DisplayObjectIF d);

	LineContainer getLineContainer();

	void initGraphics(PGraphics g);
	
	void renderGuiLayer(PGraphics g);
	
	void renderAnimLayer(PGraphics g);
	
	boolean isGuiActive();
	
	void setGuiActive(boolean b);
	
	default MenuBar showMenuBar() {
		throw new UnsupportedOperationException();
	}

}