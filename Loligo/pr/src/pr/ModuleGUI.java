package pr;

import static constants.DisplayFlag.CLICKABLE;
import static gui.selection.GuiType.SIGNAL;
import static gui.text.TextBlock.AlignMode.CENTER;

import constants.Flow;
import errors.LoligoException;
import gui.nodeinspector.NodeInspector;
import gui.text.TextBox;
import lombok.var;
import module.Module;
import module.dynio.DynIOGUIWrapper;
import module.dynio.DynIOModule;
import module.dynio.DynIOModuleGUI;

public interface ModuleGUI extends DisplayObjectIF {
	
	public static ModuleGUI createModuleBox(Module m) {
		var t = createModuleTextBox(m);
		return createModuleBox(m, t);
	}
	
	public static ModuleGUI createModuleBox(Module m, DisplayObjectIF d) {
		
		//TextBox t = m.checkFlag(Flow.DYNAMIC_IO) ? createDynModuleTextBox((DynIOModule) m) :
		//										   createModuleTextBox(m);
		
		return m.checkFlag(Flow.DYNAMIC_IO) ? 	new DynIOGUIWrapper((DynIOModule) m, d) :
												new ModuleGUIWrapper(m, d);
	}

	
	public static TextBox createDynModuleTextBox(DynIOModule m) {
		var t = new DynIOModuleGUI(m);
		initTextBox(t);
		return t;
	}
	
	public static TextBox createModuleTextBox(Module m) {
		var t = new TextBox(m.getName(), true);
		initTextBox(t);
		return t;
	}
	
	public static void initTextBox(TextBox t) {
		t.bgBox = true;
		t.setGuiType(SIGNAL);
		t.setFlag(CLICKABLE, true);
		
		t.setAlignMode(CENTER);
		t.setColor(COLOR_DEFAULT);
	}
	
	
	
	
	
	public static final int COLOR_DEFAULT = 0xff1E1E1E, COLOR_ERROR = 0xffbb1E1E, COLOR_SELECTED = 0xff303030;
	public static final int WIDTH_DEFAULT = 20, HEIGHT_DEFAULT = 20;
	
	
	
	public void setGUIName(String name);
	
	public String getGUIName();
	
	public void unmarkError();

	void markError(LoligoException e);
	
	Module getModule();
	
	public void positionSlots();
	
	public default void onModuleDeleted() {
		NodeInspector.removeInspectorFor(getModule());
	}
	
	public default void enterEditMode() {}
	
	public default void exitEditMode() {}
}
