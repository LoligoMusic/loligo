package pr;

import java.util.List;

import constants.DisplayFlag;
import gui.LineContainer;
import module.Anim;
import module.Canvas;
import module.Module;
import patch.Domain;
import patch.GuiDomain;
import patch.SubDomain;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public class PassiveDisplayManager extends DisplayObject implements DisplayManagerIF {

	//private DisplayObjectIF anim;
	private final SubDomain domain;
	private final Transform transform = new Transform();
	
	
	public PassiveDisplayManager(SubDomain domain) {
		
		this.domain = domain;
		setDm(this);
		setFlag(DisplayFlag.DRAWABLE, false);
		
	}
	
	@Override
	public void render(PGraphics g) {
		
	}
	
	@Override
	public void draw() {
		
		
	}

	@Override
	public void updateDisplayList() {
		
	}

	@Override
	public void addModuleDisplay(Module m) {
		
		if(m instanceof Anim a) {
			
			addAnimObject(a.getAnim());
		}
		
		if(m.getGui() != null)
			addGuiObject(m.getGui());
	}

	@Override
	public int getFrameCount() {
		
		return domain.getParentDomain().getDisplayManager().getFrameCount();
	}

	@Override
	public void addEnterFrameListener(EnterFrameListener f) {
		
		domain.getParentDomain().getDisplayManager().addEnterFrameListener(f);
	}

	@Override
	public boolean removeEnterFrameListener(EnterFrameListener f) {
		
		return domain.getParentDomain().getDisplayManager().removeEnterFrameListener(f);
	}

	@Override
	public void addExitFrameListener(ExitFrameListener f) {
		
		domain.getParentDomain().getDisplayManager().addExitFrameListener(f);
	}

	@Override
	public boolean removeExitFrameListener(ExitFrameListener f) {
		
		return domain.getParentDomain().getDisplayManager().removeExitFrameListener(f);
	}

	@Override
	public void pauseProcessing(boolean pause) {
		
		domain.getParentDomain().getDisplayManager().pauseProcessing(pause);
	}

	@Override
	public boolean isProcessActive() {
		
		return domain.getParentDomain().isProcessActive();
	}

	@Override
	public void setCanvas(Canvas canvas) {
		
		
	}

	@Override
	public Canvas canvas() {
		
		return domain.getParentDomain().getDisplayManager().canvas();
	}

	@Override
	public void removeCanvas() {
		
		
	}

	@Override
	public void addAnimObject(DisplayObjectIF d) {
		
		addChild(d);
	}

	@Override
	public void addGuiObject(DisplayObjectIF d) {
		
		domain.getParentDomain().getDisplayManager().addGuiObject(d);
	}

	@Override
	public LineContainer getLineContainer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addedToDisplay() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removedFromDisplay() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listChanged() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resetListChanged() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getListChanged() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Domain getDomain() {
		
		return domain;
	}
	
	@Override
	public GuiDomain getGuiDomain() {
		return domain;
	}

	@Override
	public UserInput getInput() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setInput(UserInput input) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DisplayObjectIF getContainer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PGraphics getGraphics() {
		
		return null;
	}

	@Override
	public List<DisplayObjectIF> getGuiList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initGraphics(PGraphics g) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void dispatchEnterFrameEvent() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void dispatchExitFrameEvent() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void renderGuiLayer(PGraphics g) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void renderAnimLayer(PGraphics g) {
		
	}

	@Override
	public Transform getTransform() {
		return transform ;
	}

	@Override
	public boolean isGuiActive() {
		return false;
	}

	@Override
	public void setGuiActive(boolean b) {}


	
	/*
	@Override
	public PMatrix2D getMatrix() {
		
		return domain.getDisplayManager().getMatrix();
	}

	@Override
	public PMatrix2D getBackMatrix() {
		return domain.getDisplayManager().getBackMatrix();
	}
	
	@Override
	public void flipMatrices() {
		//domain.getDisplayManager().flipMatrices();
	}
	
	@Override
	public float getRotation() {
		
		return domain.getDisplayManager().getRotation();
	}

	@Override
	public void setRotation(float rotation) {
		
	}
	*/
}
