package pr;

import java.util.Collections;
import java.util.List;

import gui.DisplayObjects;
import patch.SubDomain;
import processing.core.PGraphics;

public class SubPatchPassiveDisplayManager extends PassiveDisplayManager {

	private List<DisplayObjectIF> displayList = Collections.emptyList();
	private final Transform transform = new Transform();
	
	public SubPatchPassiveDisplayManager(SubDomain domain) {
		super(domain);
		
	}

	@Override
	public void updateDisplayList() {
	
		displayList = DisplayObjects.getOffspring(this);
	}
	
	@Override
	public void render(PGraphics g) {
		renderAnimLayer(g);
	}
	
	@Override
	public void renderAnimLayer(PGraphics g) {
		
		for(int i = 1; i < displayList.size(); i++) {
			displayList.get(i).render(g);
		}
	}
	
	@Override
	public Transform getTransform() {
		return transform;
	}
}
