package pr;

import static module.ModuleType.POSITIONABLES;
import static util.Colors.grey;

import java.util.List;
import java.util.function.Consumer;

import constants.DisplayFlag;
import constants.InputEvent;
import errors.LoligoException;
import gui.DisplayGuiSender;
import gui.DisplayObjects;
import gui.Position;
import gui.nodeinspector.NodeInspector;
import gui.selection.GuiType;
import gui.text.TextBlock.AlignMode;
import gui.text.TextBox;
import gui.text.TextObject;
import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.ModuleType;
import module.Modules;
import pr.userinput.UserInput;
import processing.core.PGraphics;


public class ModuleGUIWrapper implements ModuleGUI {
	
	@Getter @Setter
	protected DisplayObjectIF displayObject;
	@Getter
	private final Module module;
	private String guiName;
	private TextBox mouseOverTextBox;
	
	public ModuleGUIWrapper(Module module, DisplayObjectIF displayObject) {
		this.displayObject = displayObject;
		this.module = module;
		guiName = module.getName();
	}

	
	@Override
	public void setGUIName(String name) {
		guiName = name;
		if(displayObject instanceof TextObject t) {
			t.setText(name);
		}
	}


	@Override
	public String getGUIName() {
		return guiName;
	}
	
	/*
	public void isSelected(boolean selected) {
		
		displayObject.setColor(selected ? COLOR_SELECTED : COLOR_DEFAULT);
	}
	*/
	
	
	@Override
	public void markError(LoligoException e) {
		
		setColor(COLOR_ERROR);
		
		/*
		errorMessage = new TextBlock();
		errorMessage.setWH(getWidth(), 100);
		errorMessage.setText(e.getMessage());
		*/
	}

	@Override
	public void unmarkError() {
		setColor(COLOR_DEFAULT);
	}


	@Override
	public void positionSlots() {
		Modules.positionSlots(module);
	}
	
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		displayObject.mouseEventThis(e, input);
		switch(e) {
		case CLICK_RIGHT -> NodeInspector.openForPatch(module);
		case DOUBLECLICK -> enterEditMode();
		case DRAG_START  -> {}
		/*
		case DRAG_STOP   -> {
			var dragStart = input.mouseDrag.getDragStart();
			int dx = input.getMouseX() - (int) dragStart.getX();
			int dy = input.getMouseY() - (int) dragStart.getY();
			var a = new ModuleAction.Move(module.getDomain(), module.getModuleID(), dx, dy);
			module.getDomain().getHistory().addAction(a);
		}
		*/
		default -> {}
		}
	}
	
	@Override
	public void mouseOverRest(boolean on) {
		displayObject.mouseOverRest(on);
		
		if(!ModuleType.hasAnyType(module, POSITIONABLES)) {
			return;
		}
		
		if(on) {
			String t = "# " + module.getLabel();
			mouseOverTextBox = new TextBox(t, true);
			mouseOverTextBox.setColor(grey(10));
			mouseOverTextBox.setTextColor(grey(140));
			mouseOverTextBox.setAlignMode(AlignMode.CENTER);
			mouseOverTextBox.bgBox = true;
			float d = 7;
			mouseOverTextBox.setPos(getX() + d, getY() - mouseOverTextBox.getHeight() - d);
			((DisplayManagerIF)getDm()).addGuiObject(mouseOverTextBox); 
		}else {
			DisplayObjects.removeFromDisplay(mouseOverTextBox);
		}
	}
	
	
	
	//---------------------DisplayObject delegation----------------------------------
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		displayObject.mouseEvent(e, input);
	}

	public void addedToDisplay() {
		displayObject.addedToDisplay();
	}

	public void removedFromDisplay() {
		displayObject.removedFromDisplay();
	}

	public boolean hitTest(float mx, float my) {
		return displayObject.hitTest(mx, my);
	}

	public void render(PGraphics g) {
		displayObject.render(g);
	}

	public void addChild(DisplayObjectIF d) {
		displayObject.addChild(d);
	}

	public void addChild(DisplayObjectIF... dArr) {
		displayObject.addChild(dArr);
	}

	public void removeChild(DisplayObjectIF d) {
		displayObject.removeChild(d);
	}

	public void removeChild(DisplayObjectIF[] dArr) {
		displayObject.removeChild(dArr);
	}

	public void setSelectionObject(Consumer<PGraphics> cons) {
		displayObject.setSelectionObject(cons);
	}

	public void setSelected(boolean b) {
		displayObject.setSelected(b);
	}

	public void fixedToParent(boolean b) {
		displayObject.fixedToParent(b);
	}

	public float getX() {
		return displayObject.getX();
	}

	public float getY() {
		return displayObject.getY();
	}

	public void setPos(float x, float y) {
		displayObject.setPos(x, y);
	}

	public void setGlobal(float x, float y) {
		displayObject.setGlobal(x, y);
	}

	public Position getPosObject() {
		return displayObject.getPosObject();
	}

	public void setPosObject(Position p) {
		displayObject.setPosObject(p);
	}

	public DisplayObjectIF getParent() {
		return displayObject.getParent();
	}

	public void setParent(DisplayObjectIF parent) {
		displayObject.setParent(parent);
	}

	public List<DisplayObjectIF> getChildren() {
		return displayObject.getChildren();
	}

	public DisplayContainerIF getDm() {
		return displayObject.getDm();
	}

	public void setDm(DisplayContainer dm) {
		displayObject.setDm(dm);
	}

	public boolean checkFlag(DisplayFlag flag) {
		return displayObject.checkFlag(flag);
	}

	public void setFlag(DisplayFlag f, boolean b) {
		displayObject.setFlag(f, b);
	}

	public int getColor() {
		return displayObject.getColor();
	}

	public int setColor(int color) {
		return displayObject.setColor(color);
	}

	public int getStrokeColor() {
		return displayObject.getStrokeColor();
	}

	public void setStrokeColor(int strokeColor) {
		displayObject.setStrokeColor(strokeColor);
	}

	public int getWidth() {
		return displayObject.getWidth();
	}

	public void setWH(int width, int height) {
		displayObject.setWH(width, height);
	}

	public int getHeight() {
		return displayObject.getHeight();
	}

	public GuiType getGuiType() {
		return displayObject.getGuiType();
	}

	public void setGuiType(GuiType guiType) {
		displayObject.setGuiType(guiType);
	}

	@Override
	public void setDm(DisplayContainerIF dm) {
		displayObject.setDm(dm);
	}
	
	
	
	public static class Sender<T> extends ModuleGUIWrapper implements DisplayGuiSender<T> {

		private final DisplayGuiSender<T> sender;
		
		public Sender(Module module, DisplayGuiSender<T> sender) {
			super(module, sender);
			this.sender = sender;
		}

		@Override
		public T getValue() {
			return sender.getValue();
		}

		@Override
		public void setValue(T v) {
			sender.setValue(v);
		}

		@Override
		public void update() {
			sender.update();
		}

		@Override
		public void disable(boolean b) {
			sender.disable(b);
		}
		
	}
	
}
