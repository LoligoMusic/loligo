package pr.userinput;

import static constants.DisplayFlag.DRAGABLE;
import static constants.DisplayFlag.lockedPosition;
import static constants.InputEvent.DRAG;
import static constants.InputEvent.DRAG_START;
import static constants.InputEvent.DRAG_STOP;
import static constants.InputEvent.KEY;
import static constants.InputEvent.KEY_AUTOREPEAT;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.Math.min;

import java.util.List;

import constants.InputEvent;
import gui.Position;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pr.DisplayObjectIF;
import pr.EventSubscriber;
import processing.core.PConstants;

@RequiredArgsConstructor
public class MouseDrag implements EventSubscriber{
	
	private final UserInput input;
	private DragInfo[] drags;
	@Getter
	private Position dragStart = new Position();
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		//if(e == InputEvent.SELECT_ADD || e == InputEvent.SELECT_REMOVE) {
		//	dragStart();
		//}else
		
		
		if((e == KEY || e == KEY_AUTOREPEAT) && !input.specialFocusMode()) {
			
			var t = input.selection.getGuiType();
			if(t != null && t.isModule())
				moveWithArrowKeys(input);
		}
	}


	public void moveWithArrowKeys(UserInput input) {
		int a = 1 + (input.shift ? 5 : 0) + (input.ctrl ? 15 : 0);
		int k = input.getLastKeyCode();
		
		int x = k == PConstants.LEFT  ? -a :
				k == PConstants.RIGHT ?  a : 0;
		int y = k == PConstants.UP    ? -a :
				k == PConstants.DOWN  ?  a : 0;
		
		if(x != 0 || y != 0)
			setOriginRelative(input.selection.getSelection(), x, y, true);
	}
	
	
	public void dragStart() {
		dragStart.setPos(input.mousePos);
		initDrag(
			input.selection.getSelection(), 
			(int) dragStart.getX(), 
			(int) dragStart.getY()
		);
	}

	
	private void initDrag(List<DisplayObjectIF> targets, int x, int y) {
		
		drags = targets.stream().map(d -> {
			int ox = (int) d.getX() - x;
			int oy = (int) d.getY() - y;
			return new DragInfo(d, ox, oy);
		}).toArray(DragInfo[]::new);
	}

	
	public void drag(int x, int y, boolean force) {
		
		if(!input.specialFocusMode())  {
			//if(force || input.selection.getSelection().contains(input.getDragTarget())) 
				for (var m : drags) {
					m.drag(input, x, y);
				}
					
		}
		//input.dispatchInputEvent(DRAG, input.getDragTarget());
	}
	
	
	public void dragStop() {
		drags = null;
	}
	
	
	public void setOrigin(List<DisplayObjectIF> targets, int x, int y, boolean force) {
			
		if(targets.isEmpty())
			return;
		
		int ox = MAX_VALUE, oy = ox;
		for(var t : targets) {
			ox = min(ox, (int) t.getX());
			oy = min(oy, (int) t.getY());
		}
		
		var target = targets.get(0);
		
		
		input.selection.setSelection(targets);
		
		initDrag(input.selection.getSelection(), ox, oy);
		input.dispatchInputEvent(DRAG_START, target);
		
		drag(x, y, force);
		input.dispatchInputEvent(DRAG, target);
		
		dragStop();
		input.dispatchInputEvent(DRAG_STOP, target);
	}
	
	
	public void setOriginRelative(List<DisplayObjectIF> targets, int dx, int dy, boolean force) {
		
		int ox = MAX_VALUE, oy = ox;
		for(var t : targets) {
			ox = min(ox, (int) t.getX());
			oy = min(oy, (int) t.getY());
		}
		setOrigin(targets, ox + dx, oy + dy, force);
	}
	
	
	private record DragInfo(DisplayObjectIF dragTarget, int xOff, int yOff) {
		
		public void drag(UserInput input, int mx, int my) {
			
			if(!dragTarget.checkFlag(DRAGABLE) || dragTarget.checkFlag(lockedPosition))
				return;
			dragTarget.setGlobal(mx + xOff, my + yOff);
		}
	}


	
}
