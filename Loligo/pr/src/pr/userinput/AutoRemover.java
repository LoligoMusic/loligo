package pr.userinput;

import static gui.DisplayObjects.getParentDepth;

import gui.DisplayObjects;
import lombok.RequiredArgsConstructor;
import pr.DisplayContainerIF;
import pr.DisplayObjectIF;


@RequiredArgsConstructor
public class AutoRemover {

	private final DisplayContainerIF display;
	private Removable removable;

	
	public void setRemovableTarget(DisplayObjectIF d) {
		setRemovableTarget(d, null);
	}
	
	
	public void setRemovableTarget(DisplayObjectIF d, DisplayObjectIF parent) {
		
		if(d == null) {
			
			if(removable != null)
				removable.delAll();
			
			return;
		}
		
		//if(removable != null && (parent == null || isChildOf(parent, removable.d))) 
		//	removable.delAll();
		checkRemovable(d, parent);
		
		new Removable(d);
	}
	
	
	public void removeRemovableTarget(DisplayObjectIF d) {
		if(removable != null)
			removable.del(d);
	}
	
	public void checkRemovable(DisplayObjectIF d) {
		var p = d == null ? null : d.getParent();
		checkRemovable(d, p);
	}
	
	public void checkRemovable(DisplayObjectIF d, DisplayObjectIF parent) {
		
		if(removable != null)
			removable.check(d, parent);
	}
	
	
	
	private class Removable {
			
			final DisplayObjectIF d;
			Removable next;
			
			Removable(DisplayObjectIF d) {
				this.d = d;
				this.next = removable;
				removable = this;
				display.addGuiObject(d);
				DisplayObjects.holdInScreen(d);
			}
			
			void check(DisplayObjectIF pressed, DisplayObjectIF parent) {
				
				boolean b = pressed == null || 
							(getParentDepth(pressed, d) < 0 &&  getParentDepth(parent, d) < 0);
				
				if(b) {
					
					del();
					if(next != null)
						next.check(pressed, parent);
				}
			}
			
			void delAll() {
				del();
				if(next != null)
					next.delAll();
			}
			
			void del() {
				DisplayObjects.removeFromDisplay(d);
				removable = next;
			}
			
			void del(DisplayObjectIF re) {
				if(contains(re))
					del2(re);
			}
			
			private void del2(DisplayObjectIF re) {
				if(re == d)
					del();
				else if(next != null)
					next.del2(re);
			}
			
			boolean contains(DisplayObjectIF di) {
				if(d == di)
					return true;
				else if(next != null)
					return next.contains(di);
				else
					return false;
			}
		}
}
