package pr.userinput;

import static constants.DisplayFlag.CLICKABLE;
import static constants.DisplayFlag.DROPABLE;
import static constants.InputEvent.CLICK;
import static constants.InputEvent.CLICK_RIGHT;
import static constants.InputEvent.DOUBLECLICK;
import static constants.InputEvent.DRAG;
import static constants.InputEvent.DRAG_DROPPED;
import static constants.InputEvent.DRAG_OVER;
import static constants.InputEvent.DRAG_OVER_STOP;
import static constants.InputEvent.DRAG_RIGHT;
import static constants.InputEvent.DRAG_START;
import static constants.InputEvent.DRAG_START_RIGHT;
import static constants.InputEvent.DRAG_STOP;
import static constants.InputEvent.DRAG_STOP_RIGHT;
import static constants.InputEvent.DROPPED;
import static constants.InputEvent.DROP_OVER;
import static constants.InputEvent.DROP_STOP;
import static constants.InputEvent.FOCUS_GAINED;
import static constants.InputEvent.FOCUS_LOST;
import static constants.InputEvent.KEY;
import static constants.InputEvent.KEY_AUTOREPEAT;
import static constants.InputEvent.KEY_RELEASED;
import static constants.InputEvent.MOUSEWHEEL;
import static constants.InputEvent.MOVE;
import static constants.InputEvent.OVER_STOP;
import static constants.InputEvent.PRESSED;
import static constants.InputEvent.PRESSED_RIGHT;
import static constants.InputEvent.RELEASED;
import static constants.InputEvent.RELEASED_CENTER;
import static constants.InputEvent.RELEASED_RIGHT;
import static gui.DisplayObjects.getParentDepth;
import static processing.core.PConstants.CENTER;
import static processing.core.PConstants.LEFT;
import static processing.core.PConstants.RIGHT;

import java.util.function.Function;

import constants.DisplayFlag;
import constants.InputEvent;
import constants.MouseEventContext;
import constants.Timer;
import gui.DisplayObjects;
import gui.Grid;
import gui.MousePos;
import gui.Shortcuts;
import gui.selection.GuiType;
import gui.selection.SelectionManager;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Delegate;
import patch.GuiDomain;
import pr.DisplayContainer;
import pr.DisplayContainerIF;
import pr.DisplayManagerIF;
import pr.DisplayObjectIF;
import pr.EventSubscriber;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.event.KeyEvent;
import processing.event.MouseEvent;


public class UserInput {
	
	private PApplet proc;
	
	@Getter
	private GuiDomain domain;
	public DisplayContainerIF display;
	
	private final MouseEventContext mouseEventContext;
	public SelectionManager selection;
	private Shortcuts shortcuts;
	@Delegate
	private AutoRemover autoRemover;
	
	public final MousePos mousePos = new MousePos(this);
	public MouseDrag mouseDrag;
	
	private Grid grid;
	
	public boolean rightDown, leftDown, centerDown, keyDown, ctrl, alt, shift;
	
	@Getter
	private char lastChar;
	private boolean waitingForDrag = true, mouseMoved, expectingDoubleClick = true;
	public boolean directMouse = true;
	@Getter
	private DisplayObjectIF clickTarget, pressedTarget, dragTarget;
	@Getter@Setter(value = AccessLevel.PACKAGE)
	private DisplayObjectIF overTarget, dropTarget;
	private DisplayObjectIF lastPressedTarget;
	@Getter
	private DisplayObjectIF currentTarget, focusTarget, specialFocusTarget;
	
	private int clickInterval = 0;
	public float targetOffX = 0, targetOffY = 0;
	public int mouseX, mouseY;
	private Timer autoRepeatDelayTimer, autoRepeatTimer;
	@Getter
	private int mouseDeltaX, mouseDeltaY, lastKeyCode;
	private int tmpMouseX, tmpMouseY, lastClickX, lastClickY;
	@Getter
	private SpecialMouseModeOwner specialMouseModeOwner;
	private MouseOverHandler mouseOverHandler;
	@Getter
	private int wheelCount;
	
	
	public UserInput(){
		mouseEventContext = new MouseEventContext();
	}
	
	public void init(GuiDomain p) {
		
		shortcuts = new Shortcuts(domain);
		init(p, (DisplayContainer) p.getDisplayManager(), p.getPApplet(), true);
	}
	
	public void init(GuiDomain domain, DisplayContainerIF display, PApplet proc, boolean enableSelection) {
		this.domain = domain;
		this.display = display;
		this.proc = proc;
		mouseDrag = new MouseDrag(this);
		
		if(enableSelection)
			selection = new SelectionManager(this);
		
		autoRepeatDelayTimer = new Timer(domain, 500, t -> autoRepeatTimer.start());
		autoRepeatTimer = new Timer(domain, 70, -1, this::autoRepeatKey);
		
		autoRemover = new AutoRemover(display);
		
		mouseOverHandler = new MouseOverHandler(this);
	}
	
	public void addInputEventListener(EventSubscriber lis) {
		
		mouseEventContext.addListener(lis);
	}
	
	public void removeInputEventListener(EventSubscriber lis) {
		
		mouseEventContext.removeListener(lis);
	}
	
	public void addChildInputEventListener(EventSubscriber lis) {
		
		mouseEventContext.addListener(lis);
	}
	
	public void removeChildInputEventListener(EventSubscriber lis) {
		
		mouseEventContext.removeListener(lis);
	}
	
	
	public void dispatchInputEvent(InputEvent e) {
		dispatchInputEvent(e, null);
	}
	
	
	public void dispatchInputEvent(InputEvent e, DisplayObjectIF target) {
	
		currentTarget = target;
		
		
		if(target != null) {
			if(e.isDragEvent && target.getGuiType().isMultiSelect()) {
				var sel = selection.getSelection();
				sel.forEach(d -> d.mouseEventThis(e, this));
				if(target != null && !sel.contains(target)) {
					target.mouseEventThis(e, this);
				}
			}else {
			
				target.mouseEventThis(e, this);
			}
		}
		
		mouseDrag.mouseEvent(e, this);
		if(shortcuts != null)
			shortcuts.mouseEvent(e, this);
		mouseEventContext.eventOn(e, this);
		
		dispatchEventToParent(e, target, this);
		
		cursor(e, target);
		
		currentTarget = null;
	}

	
	private void cursor(InputEvent e, DisplayObjectIF target) {
		if(domain != null && !specialFocusMode()) {
			var p = getDomain().getPApplet();
			
			if(p != null && target != null && target.getGuiType().isModule()) {
				if(e == DRAG_START) {
					p.cursor(PConstants.MOVE);
				}else
				if(e == DRAG_STOP) {
					p.cursor(PConstants.ARROW);
				}
			}
			if(e == DROPPED) {
				p.cursor(PConstants.ARROW);
			}
			
		}
	}
	
	
	public void dispatchChildInputEvent(InputEvent e, DisplayObjectIF target, UserInput input) {
		
		mouseEventContext.eventOn(e, input);
		dispatchEventToParent(e, target, input);
	}
	
	private void dispatchEventToParent(InputEvent e, DisplayObjectIF target, UserInput input) {
		
		if(domain != null) {
			var d = domain.getParentDomain();
			if(d != null) {
				var in = d.getDisplayManager().getInput();
				in.dispatchChildInputEvent(e, target, input);
			}
		}
	}
	
	
	public DisplayObjectIF getDisplayObjectAt(int x, int y, DisplayFlag flag) {
		
		return getDisplayObjectAt(x, y, d -> d.checkFlag(flag));
	}
	
	
	public DisplayObjectIF getDisplayObjectAt(int x, int y, Function<DisplayObjectIF, Boolean> foo) {
		
		var rList = display.getGuiList();
		
		for(int i = rList.size() - 1; i >= 0; i--) {
			var d = rList.get(i);
			if(foo.apply(d) && d.hitTest(x, y)) {
				return d;
			}
		}
		return null;
	}
	
	public void mouseCheck(){
		
		mouseEventContext.updateRemoved();
		
		var d = getDisplayObjectAt(getMouseX(), getMouseY(), CLICKABLE);
		
		if(d == null) {
			if(overTarget != null) {
				overStop();
				setMouseOverTarget(null);
			}
		}
		else
		if(overTarget != d) {
			
			if(overTarget != null || d == null) {
				overStop();
			}
			if(d != null) {
				
				setMouseOverTarget(d);
				d.setFlag(DisplayFlag.mouseOver, true);
				currentTarget = overTarget;
				dispatchInputEvent(InputEvent.OVER, overTarget);
			}
		}
		
		checkMouseStopped();
		
	}
	
	private void overStop() {
		overTarget.setFlag(DisplayFlag.mouseOver, false);
		currentTarget = overTarget;
		mouseOverHandler.overStop();
		dispatchInputEvent(OVER_STOP, overTarget);
	}
	
	private void setMouseOverTarget(DisplayObjectIF d) {
		overTarget = d;
	}
	
	public void mouseEvent(MouseEvent event) {
		
		ctrl = event.isControlDown();
		alt = event.isAltDown();
		shift = event.isShiftDown();
		
		mouseX = event.getX();
		mouseY = event.getY();
		
		switch(event.getAction()) {
		case MouseEvent.PRESS 	-> mousePressed();
		case MouseEvent.RELEASE -> mouseReleased();
		case MouseEvent.MOVE 	-> mouseMoved();
		case MouseEvent.DRAG 	-> mouseDragged();
		case MouseEvent.ENTER 	-> mouseEnter();
		case MouseEvent.EXIT 	-> mouseExit();
		case MouseEvent.WHEEL 	-> mouseWheel(event.getCount());
		}
	}
	
	
	private void mouseEnter() {
		//if(domain != null)
		//	domain.getPApplet().cursor(PConstants.ARROW);
	}
	
	private void mouseExit() {
		
	}

	
	public void mousePressed() {
		
		boolean leftPressed = false, rightPressed = false;
		
		switch(proc.mouseButton) {
		case RIGHT:
			rightPressed = !rightDown;
			rightDown = true;
			break;
		case LEFT:
			leftPressed = !leftDown;
			leftDown = true;
			break;
		case CENTER:
			dispatchInputEvent(InputEvent.PRESSED_CENTER);
			break;
		}
		
		checkRemovable(overTarget);
		
		
		if(leftPressed) {
			
			if(rightDown && dragTarget != null)
				doDragStopRight();
			
			if(overTarget != null) {
				
				pressedTarget = overTarget;
				DisplayObjects.setChildIndex(pressedTarget, -1);
				
				targetOffX = pressedTarget.getX() - getMouseX();
				targetOffY = pressedTarget.getY() - getMouseY();
			}
			
			int t = proc.millis();
			int dif = t - clickInterval;
			
			clickInterval = t;
			
			int delt = Math.abs(lastClickX - mouseX) + Math.abs(lastClickY - mouseY);
			lastClickX = mouseX;
			lastClickY = mouseY;
			
			if(expectingDoubleClick && lastPressedTarget == pressedTarget && dif < 300 && delt <= 2){
				
				dispatchInputEvent(DOUBLECLICK, pressedTarget);
				expectingDoubleClick = false;
			}else {
				dispatchInputEvent(PRESSED, pressedTarget);
				expectingDoubleClick = true;
				lastPressedTarget = pressedTarget;
			}
			
		}
		
		if(rightPressed) {
			
			if(specialMouseModeOwner == null) {
				
				if(pressedTarget != null) {
					doDragStop();
				}
				pressedTarget = overTarget;
				dispatchInputEvent(PRESSED_RIGHT, overTarget);
			}
		}
		
		setFocusTarget(overTarget);
		mouseOverHandler.cancelMouseOverRest();
	}
	
	
	public void setFocusTarget(DisplayObjectIF t) {
		
		if(t != focusTarget) {
			dispatchInputEvent(FOCUS_LOST, focusTarget);
			focusTarget = t;
			if(selection != null)
				selection.setActiveElement(t);
			dispatchInputEvent(FOCUS_GAINED, focusTarget);
		}
	}
	
	
	public void setSpecialFocusTarget(DisplayObjectIF t) {
		specialFocusTarget = t;
	}
	
	
	public void removeSpecialFocusTarget(DisplayObjectIF t) {
		if(t == specialFocusTarget)
			specialFocusTarget = null;
	}
	
	
	public boolean specialFocusMode() {
		return specialFocusTarget != null;
	}
	
	
	public void mouseReleased() {
		
		if(leftDown) {
			
			if(overTarget != null) {
				
				if(overTarget == pressedTarget && dragTarget == null) {
					
					clickTarget = overTarget;
					dispatchInputEvent(CLICK, clickTarget);
					clickTarget = null;
				}else {
					dispatchInputEvent(RELEASED, overTarget);
				}
			}
			
			boolean dropped = false, dragStop = false;
			
			if (dragTarget != null) {
				
				waitingForDrag = true;
				dragStop = true;
				dropped = dropTarget != null;
			}
			
			if(dropped) {
				dispatchInputEvent(DROPPED, dropTarget);
				dispatchInputEvent(DRAG_DROPPED, dragTarget);
			}else 
			if(dragStop) {
				doDragStop();
			}else{
				dispatchInputEvent(RELEASED, overTarget);
			}
			pressedTarget = 
			dragTarget = 
			dropTarget = null;
			//clearLineTarget();
			
		}else if(rightDown) {
			
			if(dragTarget != null) {
				doDragStopRight();
			}else	
			if(specialMouseModeOwner != null) {
				stopSpecialMouseMode();	
			}else{
				dispatchInputEvent(CLICK_RIGHT, overTarget);
			}
			dispatchInputEvent(RELEASED_RIGHT, overTarget);
			
			currentTarget = dragTarget = pressedTarget = null;
		}
		else if(centerDown) {
			
			dispatchInputEvent(RELEASED_CENTER);
		}
		
		rightDown = leftDown = centerDown = false;
	}
	
	private void doDragStop() {
		dispatchInputEvent(DRAG_STOP, dragTarget);
		dragTarget = dropTarget = null;
	}
	
	private void doDragStopRight() {
		dispatchInputEvent(DRAG_STOP_RIGHT, dragTarget);
		waitingForDrag = true;
	}
	
	
	private void mouseDelta() {
		
		mouseDeltaX = proc.mouseX - tmpMouseX;
		mouseDeltaY = proc.mouseY - tmpMouseY;
		
		tmpMouseX = proc.mouseX;
		tmpMouseY = proc.mouseY;
	}
	
	
	public void mouseMoved() {
		
		//mouseDelta(); mouseDelta called in checkMouseStopped
		
		if(!mouseMoved) {
			mouseMoved = true;
			mouseMoveStart();
		}
		dispatchInputEvent(MOVE);
	}
	
	public void checkMouseStopped() {
		mouseDelta();
		
		if(mouseMoved) {
			
			if(mouseDeltaX == 0 && mouseDeltaY == 0) {
				mouseStopped();
				mouseMoved = false;
			}
			
		}
	}
	
	public void mouseMoveStart() {
		mouseOverHandler.cancelMouseOverRest();
	}
	
	
	public void mouseStopped() {
		mouseOverHandler.start();
	}
	
	
	public void mouseDragged() {
		
		mouseDelta();
		
		if(pressedTarget != null) {
			dragTarget = pressedTarget;
			if(waitingForDrag){
				
				waitingForDrag = false;
				
				if(leftDown) {
					
					dragStart();
				}else
				if(rightDown) {
					
					dispatchInputEvent(DRAG_START_RIGHT, dragTarget);
				}
			}else {
				if(leftDown) {
					checkDrop(dragTarget);
					currentTarget = dragTarget;
					mouseDrag.drag(getMouseX(), getMouseY(), false);
					dispatchInputEvent(DRAG, dragTarget);
				}else
				if(rightDown) {
					dispatchInputEvent(DRAG_RIGHT, dragTarget);
				}
			}
			
		}
	}
	
	private void dragStart() {
		dispatchInputEvent(DRAG_START, dragTarget);
		mouseDrag.dragStart();
		checkDrop(dragTarget);
		mouseDrag.drag(getMouseX(), getMouseY(), false);
	}
 	

	public void checkDrop(DisplayObjectIF target) {
		
		var d = getDisplayObjectAt((int)target.getX() , (int)target.getY(), CLICKABLE);
		
		if(d == null) {
			
			if(dropTarget != null) {
				
				dispatchInputEvent(DROP_STOP, dropTarget);
				dispatchInputEvent(DRAG_OVER_STOP, dragTarget);
				dropTarget = null;
			}
			
		}else if(d.checkFlag(DROPABLE) && d != target && getParentDepth(d, target) == -1) 
			
			if(dropTarget != d) {
				
				dispatchInputEvent(DROP_STOP, dropTarget);
				dispatchInputEvent(DRAG_OVER_STOP, dragTarget);
				dropTarget = d;
				dispatchInputEvent(DROP_OVER, dropTarget);
				dispatchInputEvent(DRAG_OVER, dragTarget);
			}
	}
	
	
	public void keyEvent(KeyEvent e) {
		
		ctrl = e.isControlDown();
		alt = e.isAltDown();
		shift = e.isShiftDown();
		
		switch(e.getAction()) {
		case KeyEvent.PRESS   -> keyPressed(e);
		case KeyEvent.RELEASE -> keyReleased(e);
		}
	}
	
	
	public void keyPressed(KeyEvent event) {
		lastChar = event.getKey();
		lastKeyCode = event.getKeyCode();//ctrl ? proc.keyCode : java.awt.event.KeyEvent.getExtendedKeyCodeForChar(proc.key);
		dispatchInputEvent(KEY, focusTarget);
		autoRepeatTimer.reset();
		autoRepeatDelayTimer.reset();
		autoRepeatDelayTimer.start();
	}
	
	
	public void keyReleased(KeyEvent event) {
		lastChar = event.getKey();
		lastKeyCode = event.getKeyCode();
		dispatchInputEvent(KEY_RELEASED, focusTarget);
		lastKeyCode = -1;
		lastChar = 0;
		autoRepeatTimer.reset();
		autoRepeatDelayTimer.reset();
	}

	
	public void autoRepeatKey(Timer t) {
		dispatchInputEvent(KEY_AUTOREPEAT, focusTarget);
	}
	
	
	public void mouseWheel(int count) {
		wheelCount = count;
		dispatchInputEvent(MOUSEWHEEL, focusTarget);
	}
	
	
	public void setDragTarget(DisplayObjectIF d) {
		dragTarget = d;
		d.setPos(getMouseX() + targetOffX, getMouseY() + targetOffY);
		DisplayObjects.setChildIndex(d, -1);
	}
	
	public void injectDragTarget(DisplayObjectIF d) {
		pressedTarget = d;
		dragTarget = d;
		//dispatchInputEvent(DRAG_START, d);
		if(selection != null) {
			selection.clear();
			selection.add(d);
		}
		dragStart();
		
	}
	
	
	

	public void setPressedTarget(DisplayObjectIF d) {
		
		targetOffX = 0;
		targetOffY = 0;
		pressedTarget = d;
	}
	
	
	/**
	 * Checks if d is the target of the current MouseEvent. Should only be called inside the mouseEvent method.
	 * @param d
	 * @return
	 */
	public boolean isCurrentTarget(DisplayObjectIF d) { 
		
		return currentTarget != null && currentTarget.equals(d);
	}
	
	public boolean isCurrentTarget(DisplayObjectIF d, InputEvent e) { 
		
		return isCurrentTarget(d) && e == mouseEventContext.getCurrentEvent();
	}

	public int getMouseX() {
		
		return mouseX = proc.mouseX;
	}
	
	public int getMouseY() {
		
		return mouseY = proc.mouseY;
	}

	public void toggleGrid() {
		
		if(grid == null) {
			grid = new Grid((DisplayManagerIF) display);
		}else {
			grid.removeGrid();
			grid = null;
		}
	}

	public void startSpecialMouseMode(SpecialMouseModeOwner specialMouseModeOwner) {
		if(this.specialMouseModeOwner != null)
			stopSpecialMouseMode();
		this.specialMouseModeOwner = specialMouseModeOwner;
	}
	
	public void stopSpecialMouseMode() {
		var s = this.specialMouseModeOwner;
		if(s != null) {
			this.specialMouseModeOwner = null;
			s.specialMouseModeStop();
		}
	}

	
	/**
	 * Resets all Mouse and Keyboard states
	 */
	public void reset() {
		
		rightDown = leftDown = centerDown = keyDown = ctrl = alt = shift = mouseMoved = false;
		waitingForDrag = expectingDoubleClick = directMouse = true;
		setMouseOverTarget(null);
		clickTarget = pressedTarget = dragTarget = dropTarget =
		lastPressedTarget = currentTarget = focusTarget = specialFocusTarget = null;
		
		mouseOverHandler.resetState();
	}
}

