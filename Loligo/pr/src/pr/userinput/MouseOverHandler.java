package pr.userinput;

import constants.Timer;
import lombok.Getter;
import pr.DisplayObjectIF;

class MouseOverHandler {

	private static final int TIMEOUT_MOUSEOVER = 500;
	
	private final UserInput input;
	private final Timer timer;
	@Getter
	private DisplayObjectIF mouseOverTarget, mouseOverRestTarget;
	private boolean locked;
	
	MouseOverHandler(UserInput input) {
		this.input = input;
		timer = new Timer(input.getDomain(), TIMEOUT_MOUSEOVER, this::mouseOverRest);
	}
	
	
	private void mouseOverRest(Timer t) {
		var over = input.getOverTarget();
		if(over != null) {
			timer.reset();
			
			mouseOverRestTarget = over;
			over.mouseOverRest(true);
			locked = true;
		}
	}
	
	
	void start() {
		var over = input.getOverTarget();
		if(over != null && !locked) {
			timer.start();
		}
	}
	
	
	void cancelMouseOverRest() {
		timer.reset();
		if(mouseOverRestTarget != null) {
			mouseOverRestTarget.mouseOverRest(false);
			mouseOverRestTarget = null;
		}
	}

	
	void resetState() {
		timer.reset();
		mouseOverRestTarget = null;
		locked = false;
	}

	
	void overStop() {
		locked = false;
	}

	/*
	void checkMouse(DisplayObjectIF d) {
		if(d == null) {
			
			if(mouseOverTarget != null) {
				
				mouseOverTarget.setFlag(DisplayFlag.mouseOver, false);
				input.setCurrentTarget(mouseOverTarget);
				input.dispatchInputEvent(InputEvent.OVER_STOP, mouseOverTarget);
				setMouseOverTarget(null);
			}
		}
		else
		if(mouseOverTarget != d) {
			
			if(mouseOverTarget != null || d == null) {
				
				mouseOverTarget.setFlag(DisplayFlag.mouseOver, false);
				input.setCurrentTarget(mouseOverTarget);
				input.dispatchInputEvent(InputEvent.OVER_STOP, mouseOverTarget);
				
			}
			if(d != null) {
				
				setMouseOverTarget(d);
				d.setFlag(DisplayFlag.mouseOver, true);
				input.setCurrentTarget(mouseOverTarget);
				input.dispatchInputEvent(InputEvent.OVER, mouseOverTarget);
			}
		}
	}
	*/
}
