package pr;

import java.util.List;
import java.util.function.Consumer;
import constants.DisplayFlag;
import constants.InputEvent;
import gui.MouseOverRestInterface;
import gui.Position;
import gui.selection.GuiType;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public interface DisplayObjectIF extends Renderer, Positionable, EventSubscriber, MouseOverRestInterface{

	void mouseEvent(InputEvent e, UserInput input);
	
	void mouseEventThis(InputEvent e, UserInput input);

	void addedToDisplay();

	void removedFromDisplay();

	boolean hitTest(float mx, float my);

	void render(PGraphics g);

	void addChild(DisplayObjectIF d);

	void addChild(DisplayObjectIF... dArr);

	void removeChild(DisplayObjectIF d);

	void removeChild(DisplayObjectIF[] dArr);

	void setSelectionObject(Consumer<PGraphics> cons);

	void setSelected(boolean b);

	void fixedToParent(boolean b);

	float getX();

	float getY();

	void setPos(float x, float y);

	void setGlobal(float x, float y);

	Position getPosObject();

	void setPosObject(Position p);

	DisplayObjectIF getParent();

	void setParent(DisplayObjectIF parent);

	List<DisplayObjectIF> getChildren();

	DisplayContainerIF getDm();

	void setDm(DisplayContainerIF dm);

	boolean checkFlag(DisplayFlag flag);

	void setFlag(DisplayFlag f, boolean b);

	int getColor();

	int setColor(int color);

	int getStrokeColor();

	void setStrokeColor(int strokeColor);

	int getWidth();

	void setWH(int width, int height);

	int getHeight();

	GuiType getGuiType();

	void setGuiType(GuiType guiType);

}