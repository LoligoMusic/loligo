package pr;

import java.util.List;

import constants.DisplayFlag;
import constants.EnterFrameContext;
import constants.ExitFrameContext;
import gui.DisplayObjects;
import lombok.Getter;
import module.Anim;
import module.Module;
import patch.Domain;
import patch.GuiDomain;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import util.Colors;

public class DisplayContainer extends DisplayObject implements EnterFrameListener, DisplayContainerIF{

	private final GuiDomain domain;
	private UserInput input;
	protected List<DisplayObjectIF> renderList;
	public transient PGraphics g;
	private boolean listChanged = true;
	protected final DisplayObjectIF container;
	
	
	@Getter
	private final Transform transform = new Transform();
	
	@Getter
	protected int frameCount = 0;
	
	protected EnterFrameContext enterFrameContext = new EnterFrameContext();
	protected ExitFrameContext exitFrameContext = new ExitFrameContext();
	

	public DisplayContainer(GuiDomain domain) {
		
		this.domain = domain;
		//this.dm = this;
		
		container = new DisplayObject();
		//drawable = false;
		container.setFlag(DisplayFlag.DRAWABLE, false);
		container.setDm(this);
		//addChild(container);
	}

	
	
	public void initGraphics(PGraphics g) {
		
		this.g = g;
		g.tint(Colors.WHITE);
		//addedToDisplay();
		//g.beginDraw();
		//g.smooth();
		//g.endDraw();
	}
	
	@Override
	public void render(PGraphics g) {
		
		if(listChanged) {
			listChanged = false;
			updateDisplayList();
			draw();
		}
		//g.image(g, getX(), getY());
	}
	
	@Override
	public void draw() {
		
		if(listChanged)
			updateDisplayList();
		
		enterFrameContext.dispatchEnterFrameEvent();
		
		g.beginDraw();
		
		g.background(getColor());
		
		for (var d: renderList) {
			d.render(g);
		}
		
		g.endDraw();
		
		getInput().mouseCheck();
		
		exitFrameContext.dispatchExitFrameEvent();
	}

	@Override
	public void updateDisplayList() {
		listChanged = false;
		renderList = DisplayObjects.getOffspring(container);
	}
	
	@Override
	public void addModuleDisplay(Module m) {
		
		if(m instanceof Anim a) 
			renderList.add(a.getAnim());
		if(m.getGui() != null)
			renderList.add(m.getGui());
		
	}
	
	
	@Override
	public void addedToDisplay() {
		
		var cc = DisplayObjects.getOffspring(container);
		
		for (var d : cc) {
			d.setFlag(DisplayFlag.onDisplay, true);
			if(d != this)
				d.addedToDisplay();
		}
	}
	
	
	@Override
	public void removedFromDisplay() {
		DisplayObjects.removedFromDisplayNotifyChildren(container);
		/*
		for (var d : DisplayObjects.getOffspring(container)) {
			d.setFlag(DisplayFlag.onDisplay, false);
			if(d != container)
				d.removedFromDisplay();
		}
		*/
	}
	
	@Override
	public void addGuiObject(DisplayObjectIF d) {
		
		container.addChild(d);
	}
	
	@Override
	public List<DisplayObjectIF> getGuiList() {
		
		return renderList;
	}
	
	@Override
	public void addEnterFrameListener(EnterFrameListener f) {
		
		enterFrameContext.addListener(f);
	}
	
	@Override
	public boolean removeEnterFrameListener(EnterFrameListener f) {
		
		enterFrameContext.removeListener(f);
		return true;
	}
	
	@Override
	public void addExitFrameListener(ExitFrameListener f) {
		
		exitFrameContext.addListener(f);
	}
	
	@Override
	public boolean removeExitFrameListener(ExitFrameListener f) {
		
		exitFrameContext.removeListener(f);
		return true;
	}
	
	/*
	boolean drop = false;
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input2) {
		
		if(input2 != getDm().input)
			return;
		
		if(input2.getOverTarget() == this) {
			
			if(e == InputEvent.OVER) {
				
				PROC.display.addEnterFrameListener(this);
				
			}else if(e == InputEvent.OVER_STOP){
				
				PROC.display.removeEnterFrameListener(this);
				
			}else if(e == InputEvent.PRESSED) {
				
				input.mousePressed();
				
			}else if(e.isReleaseEvent){// == InputEvent.RELEASED){
				
				input.mouseReleased();
				
			}else if(e == InputEvent.DRAG) {
				
				input.mouseDragged();
				
			}else if(e == InputEvent.CLICK_RIGHT) {
				
				input.rightDown = true;
				input.mousePressed();
			}else if(e == InputEvent.RELEASED_RIGHT) {
				
				input.rightDown = true;
				input.mouseReleased();
			}else if(e == InputEvent.KEY) {
				
				input.keyPressed();
			}else if(e == InputEvent.KEY_RELEASED) {
				
				input.keyReleased();
			}
		}
		else if(e.isDropEvent && input2.getDropTarget() == this) {
			
			if(e == InputEvent.DROP_OVER) {
				
				//input.setPressedTarget(input2.getPressedTarget());
				PROC.display.addEnterFrameListener(this);
				drop = true;
				
			}else if(e == InputEvent.DROP_STOP || e == InputEvent.DROPPED) {
				
				if(e == InputEvent.DROPPED) {
					
					input.setDragTarget(input2.getDragTarget());
					//input.setDropTarget(input2.getDropTarget());
					MouseEventContext.instance.eventOn(InputEvent.DROPPED, input);
					
					input.setDragTarget(null);
					input.setDropTarget(null);
					
				}
				PROC.display.removeEnterFrameListener(this);
				drop = false;
			}
		}
		if(drop && input2.getDragTarget() != this) {
			
			if(e == InputEvent.DRAG) {
				
				input.setDragTarget(input2.getDragTarget());
				input.checkDrop(input2.getDragTarget());//input.mouseDragged();
				
			}else if(e == InputEvent.RELEASED){
				
				//System.out.println(input.getDropTarget() + " " + input.getDragTarget());
				//input.mouseReleased();
				
				
			}
			
		}
		//if(e == InputEvent.DROPPED  )
		/*
		else if(input2.getDropTarget() == this) {
			if(e == InputEvent.DROP_OVER) {
				//input.setPressedTarget(input2.getDragTarget());
				System.out.println(e);
				PROC.display.addFrameListener(this);
				
			}else if(e == InputEvent.DROP_STOP) {
				System.out.println(e);
				PROC.display.removeFrameListener(this);
			}else if(e == InputEvent.DROPPED){
				System.out.println(e);
			}
				
		}
		
	}
	*/
	
	@Override
	public void enterFrame() {
		draw();
		
	}
	
	@Override
	public void listChanged() {
		
		listChanged = true;
	}
	
	@Override
	public void resetListChanged() {
		
		listChanged = false;
	}
	
	@Override
	public boolean getListChanged() {
		
		return listChanged;
	}

	@Override
	public Domain getDomain() {
		return domain.getParentDomain();
	}
	
	@Override
	public GuiDomain getGuiDomain() {
		return domain;
	}
 
	@Override
	public UserInput getInput() {
		return input;
	}

	@Override
	public void setInput(UserInput input) {
		this.input = input;
	}

	@Override
	public DisplayObjectIF getContainer() {
		return container;
	}

	@Override
	public PGraphics getGraphics() {
		
		return g;
	}
	
	@Override
	public void dispatchEnterFrameEvent() {
		enterFrameContext.dispatchEnterFrameEvent();
	}
	
	@Override
	public void dispatchExitFrameEvent() {
		exitFrameContext.dispatchExitFrameEvent();
	}
	
	@Override
	public void addChild(DisplayObjectIF d) {
		container.addChild(d);
	}
	
}