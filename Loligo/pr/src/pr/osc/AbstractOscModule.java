package pr.osc;

import constants.Flow;
import lombok.Getter;
import lombok.Setter;
import module.AbstractModule;
import module.dynio.DynIOModule;

public abstract class AbstractOscModule extends AbstractModule implements DynIOModule {

	@Getter@Setter
	protected String oscPath = "";
	@Getter@Setter
	protected int port = 4444;
	protected double[] values;
	
	
	public AbstractOscModule() {
		flowFlags.add(Flow.DYNAMIC_IO);
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		setOscPath("/path");;
	}
	
	public void setNumValues(int len) {
		values = new double[len];
	}
	
	
}
