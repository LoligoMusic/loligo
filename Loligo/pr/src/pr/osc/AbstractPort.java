package pr.osc;

import java.io.IOException;

import com.illposed.osc.transport.udp.OSCPort;

public abstract class AbstractPort {

	protected final OscManager oscManager;
	protected final int port;
	
	private int numUsers;
	
	
	public AbstractPort(OscManager oscManager, int port) {
		
		this.oscManager = oscManager;
		this.port = port;
	}
	
	
	public abstract OSCPort getOSCPort();
	
	
	public void addUser() {
		numUsers++;
	}
	
	
	public void removeUser() {
		numUsers--;
		
		if(numUsers <= 0) {
			
			numUsers = 0;
			close();
		}
	}
	
	
	protected void close() {
		
		try {
			getOSCPort().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
