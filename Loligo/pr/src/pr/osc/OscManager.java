package pr.osc;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import com.illposed.osc.transport.udp.OSCPortIn;
import com.illposed.osc.transport.udp.OSCPortOut;

import errors.LoligoException;


public class OscManager {

	private final List<POut> ps = new ArrayList<>();
	private final List<PIn> oscIn = new ArrayList<>();
	
	
	
	public POut findOut(InetAddress address, int port) throws LoligoException{
		
		for(POut p : ps) {
			
			if(p.equals(address, port)) {
				return p;
			}
		}
		
		OSCPortOut osc = null;
		
		try {
			osc = new OSCPortOut(address, port);
		} catch (IOException e) {
			 
			throw new LoligoException(e, "Address already in use.");
		}
		
		
		return new POut(this, osc, address, port);
	}
	
	
	public PIn findIn(int port) throws LoligoException {
		
		for(PIn p : oscIn) {
			
			if(p.equals(port)) {
				return p;
			}
		}
		
		OSCPortIn osc = null;
		
		try {
			osc = new OSCPortIn(port);
			osc.startListening();
			
		} catch (IOException e) {

			throw new LoligoException(e, "Port already in use.");
		}
		
		PIn p = new PIn(this, osc, port);
		oscIn.add(p);
		
		return p;
	}


	public void removeIn(PIn pIn) {
		
		
		oscIn.remove(pIn);
	}
	
	public void removeOut(POut pout) {
		
		ps.remove(pout);
	}
	
}
