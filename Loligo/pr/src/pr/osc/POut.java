package pr.osc;

import java.io.IOException;
import java.net.InetAddress;
import java.util.List;

import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCSerializeException;
import com.illposed.osc.transport.udp.OSCPort;
import com.illposed.osc.transport.udp.OSCPortOut;

import errors.LoligoException;

public class POut extends AbstractPort{
	
	private final OSCPortOut osc;
	private final InetAddress address;
	
	
	public POut(OscManager oscManager, OSCPortOut osc, InetAddress address, int port) {
		
		super(oscManager, port);
		
		this.address = address;
		
		this.osc = osc;
	}
	
	public boolean equals(InetAddress address, int port) {
		
		return this.address.equals(address) && this.port == port; 
	}

	public void send(List<Object> values, String path) throws LoligoException {
		
		OSCMessage p = new OSCMessage(path, values);
		
		try {
			osc.send(p);
			
		} catch (IOException | OSCSerializeException e) {
			
			throw new LoligoException(e, "Couldn't send OSC message.");
		} 
	}
	
	public void close() {
		
		super.close();
		
		oscManager.removeOut(this);
	}

	@Override
	public OSCPort getOSCPort() {
		
		return osc;
	}
}