package pr.osc;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.illposed.osc.MessageSelector;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCMessageEvent;
import com.illposed.osc.OSCMessageListener;
import com.illposed.osc.messageselector.OSCPatternAddressMessageSelector;
import com.illposed.osc.transport.udp.OSCPort;
import com.illposed.osc.transport.udp.OSCPortIn;


public class PIn extends AbstractPort{
	
	private OSCPortIn osc;
	private final Map<String, C> values = new HashMap<>();
	
	
	public PIn(OscManager oscManager, OSCPortIn osc, int port) {
		
		super(oscManager, port);
		
		this.osc = osc;
	}
	
	public void addPath(String path) {
		
		if(path.length() == 0 || values.containsKey(path))
			return;
		
		C c = new C(path);
		c.init();
		values.put(path, c);
	}
	
	public void getValues(String path, double[] doubles) {
		C c = values.get(path);
		if(c != null) {
			c.getValues(doubles);
		}
	}
	
	public boolean equals(int port) {
		
		return this.port == port;
	}
	
	@Override
	protected void close() {
		super.close();
		values.values().forEach(c -> c.remove());
		osc.stopListening();
		this.oscManager.removeIn(this);
	}

	@Override
	public OSCPort getOSCPort() {
		
		return osc;
	}
	
	
	private class C implements OSCMessageListener{
		
		final MessageSelector selector;
		List<Object> values = Collections.emptyList();
		
		C(String path) {
			selector = new OSCPatternAddressMessageSelector(path);
		}

		void init() {
			osc.getDispatcher().addListener(selector, this);
		}
		
		void remove() {
			osc.getDispatcher().removeListener(selector, this);
		}
		
		@Override
		public void acceptMessage(OSCMessageEvent e) {
			OSCMessage m = e.getMessage();
			values = m.getArguments();
		}
		
		void getValues(double[] doubles) {
			for(int i = 0; i < values.size() && i < doubles.length; i++) {
				if(values.get(i) instanceof Number v) {
					doubles[i] = v.doubleValue();
				}
			}
		}
	}
}