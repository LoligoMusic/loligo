package pr;

import lombok.Getter;
import lombok.Setter;
import patch.Domain;
import processing.core.PMatrix2D;

public class Transform {

	@Getter
	private final PMatrix2D matrix = new PMatrix2D(), multipliedMatrix = new PMatrix2D();
	
	@Getter@Setter
	private float rotation, multipliedRotation;
	
	
	public void applyParent(Transform parent) {
		
		multipliedMatrix.set(parent.multipliedMatrix);
		multipliedMatrix.apply(matrix);
		
		multipliedRotation = this.rotation + parent.multipliedRotation;
	}
	
	
	
	public static PMatrix2D applyTransformStack(Domain domain, PMatrix2D m) {
		Domain.iteratePatchParents(domain, 
				d -> m.preApply(d.getDisplayManager().getTransform().getMatrix()));
		return m;
	}
	
	
	public static float sumRotations(Domain dom) {
		
		float r = 0;
		while(true){
			r += dom.getDisplayManager().getTransform().rotation;
			Domain d = dom.getParentDomain();
			if(d == null || d == dom) 
				return r;
			dom = d;
		} 
	}
	
	
}
