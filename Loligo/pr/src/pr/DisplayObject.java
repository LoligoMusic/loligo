package pr;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Consumer;
import static constants.DisplayFlag.*;
import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayObjects;
import gui.Position;
import gui.SelectionDeko;
import gui.selection.GuiType;
import pr.userinput.UserInput;
import processing.core.PGraphics;



public class DisplayObject implements DisplayObjectIF {
	
	protected static int defaultColor = 0xFF888888, defaultHeight = 20, defaultWidth = 20;
	
	public final static int FORM_NONE = -1, FORM_ROUND = 0, FORM_RECT = 1, FORM_LINE = 2, FORM_BEZIER = 3, FORM_CUSTOM = 4, FORM_IMG = 5;
	
	private Position pos = new Position();
	private DisplayContainerIF dm;
	private DisplayObjectIF parent;
	private final List<DisplayObjectIF> children = new ArrayList<>();
	
	private DisplayObjectIF selectDeko;
	
	private int color = 0xFF505050;

	private int strokeColor = 0xFFF0F0F0;
	
	private int width, height;
	private GuiType guiType = GuiType.INPUT;
	
	private final EnumSet<DisplayFlag> displayFlags;
	
	
	public DisplayObject() {
		
		displayFlags = EnumSet.of(ROUND, DRAWABLE, CLICKABLE, DRAGABLE);
		
		
		//drawable = _drawable;
		//clickable = _drawable;
		
		//if(!_drawable)
		//	form = FORM_NONE;
		//setWH(defaultWidth, defaultHeight);
		width = defaultWidth;
		height = defaultHeight;
		color = defaultColor;
	}

	
	@Override
	public void mouseEvent(InputEvent e, UserInput input){}
	@Override
	public void mouseEventThis(InputEvent e, UserInput input){}
	@Override
	public void mouseOverRest(boolean on) {}
	
	@Override
	public void addedToDisplay() {}
	
	@Override
	public void removedFromDisplay() {
		DisplayObjects.removeSubscriber(this);
	}

	
	@Override
	public boolean hitTest(float mx, float my) {
		
		if(checkFlag(DisplayFlag.ROUND)) {
			
			float dx = mx - pos.getX();
			float dy = my - pos.getY();
			float r = getWidth() * .5f;
			return dx * dx + dy * dy < r * r;
			
		}else
		if(checkFlag(DisplayFlag.RECT)) {
			
			return mx > pos.getX() && mx < pos.getX() + width && my > pos.getY() && my < pos.getY() + height;
		}
			
		return false;
	}
	
	
	@Override
	public void render(PGraphics g){
		
		//if(checkFlag(DisplayFlag.mouseOver))	dm.g.stroke(strokeColor);
		//else									dm.g.noStroke();
		
		g.fill(color);
		
		
		if(checkFlag(DisplayFlag.RECT))
			
			g.rect(pos.getX(), pos.getY(), width, height);
		else 
		if(checkFlag(DisplayFlag.ROUND))
			
			g.ellipse(pos.getX(), pos.getY(), getWidth(), getHeight());
		
		/*
		else if(form == FORM_IMG) {
			
			g.image(img, pos.getX(), pos.getY());
		}
		*/
		g.noStroke();
	}
	
	@Override
	public void addChild(DisplayObjectIF d){
		
		if(d.getParent() != this) {
			if(d.getParent() != null && d.getParent() != this && d.getParent().getChildren() != null) 
				d.getParent().getChildren().remove(d);
			if(!getChildren().contains(d))
				getChildren().add(d);
			
			d.setParent(this);
			
			if(d.checkFlag(DisplayFlag.fixedToParent)) {
				var po = d.getPosObject();
				d.setPosObject(new Position.PositionOffset(this, po.getX(), po.getY()));
			}	
			
			for(var c : DisplayObjects.getOffspring(d)) {
				c.setDm(dm);
				if(checkFlag(onDisplay)) {
					c.setFlag(onDisplay, true);
					c.addedToDisplay();
				}
			}
			
			if(dm != null)
				dm.listChanged();
		}
	}
	
	@Override
	public void addChild(DisplayObjectIF... dArr){
		if(dArr != null)
			for(DisplayObjectIF d : dArr)
				addChild(d);
	}
	
	@Override
	public void removeChild(DisplayObjectIF d){
		if(d != null && getChildren().remove(d)) {
			d.setParent(null);
			if(d.checkFlag(fixedToParent)) {
				var po = d.getPosObject();
				d.setPosObject(new Position(po.x, po.y));
			}
				
			if(checkFlag(onDisplay)) {
				DisplayObjects.removedFromDisplayNotifyChildren(d);
				if(dm != null)
					dm.listChanged();
			}
		}
	}
	
	@Override
	public void removeChild(DisplayObjectIF[] dArr){
		if(dArr != null)
			for(var d : dArr)
				removeChild(d);
	}
	/*
	@Override
	public void removeFromDisplay(){
		DisplayObjects.removeFromDisplay(this);
	}
	*/
	static public void setDimensions(int w, int h) {
		defaultWidth = w;
		defaultHeight = h;
	}
	
	static public void setDimensions(int quad) {
		defaultHeight = defaultWidth = quad;
	}
	
	static public void fill(int c) {
		defaultColor = c;
	}
	
	@Override
	public void setSelectionObject(Consumer<PGraphics> cons) {
		
		selectDeko = new SelectionDeko(this, cons);
	}
	
	@Override
	public void setSelected(boolean b) {
		
		defaultSetSelected(b);
	}
	
	protected void defaultSetSelected(boolean b) {
		
		setFlag(selected, b);
		
		if(b) {
			
			if(selectDeko == null) 
				selectDeko = new SelectionDeko(this);
			
			addChild(selectDeko);
			DisplayObjects.setChildIndex(selectDeko, 1);
		}else{
			
			if(selectDeko != null)
				DisplayObjects.removeFromDisplay(selectDeko);
		}
	}

	@Override
	public void fixedToParent(boolean b) {
		
		setFlag(fixedToParent, b);
		
		if(parent != null) 
			if(b)
				pos = new Position.PositionOffset(parent);
			else
				pos = new Position(pos.getX(), pos.getY());
		//dm.updateDisplayList();
		
	}

	
	@Override
	public float getX() {
		return pos.getX();
	}

	
	@Override
	public float getY() {
		return pos.getY();
	}

	
	@Override
	public void setPos(float x, float y) {
		pos.setPos(x, y);
	}
	
	
	@Override
	public void setGlobal(float x, float y) {
		
		pos.setGlobal(x, y);
	}

	
	@Override
	public Position getPosObject() {
		return pos;
	}

	
	@Override
	public void setPosObject(Position p) {
		pos = p;
	}

	@Override
	public DisplayObjectIF getParent() {
		return parent;
	}

	@Override
	public void setParent(DisplayObjectIF parent) {
		this.parent = parent;
	}

	@Override
	public List<DisplayObjectIF> getChildren() {
		return children;
	}

	@Override
	public DisplayContainerIF getDm() {
		return dm;
	}

	@Override
	public void setDm(DisplayContainerIF dm) {
		this.dm = dm;
	}

	@Override
	public boolean checkFlag(DisplayFlag flag) {
		
		return displayFlags.contains(flag);
	}
	
	
	
	@Override
	public void setFlag(DisplayFlag f, boolean b) {
		
		if(b) {
			displayFlags.add(f);
		
			if(f == DisplayFlag.RECT)
				displayFlags.remove(DisplayFlag.ROUND);
		}else
			displayFlags.remove(f);
	}
	
	/*
	public void removeFlag(DisplayFlag... flags) {
		
		for (DisplayFlag f : flags) 
			displayFlags.remove(f);
	}*/
	/*
	private void writeObject(ObjectOutputStream oos) throws IOException {
		if(parent == PROC.display) {
			parent = null;
	    	oos.defaultWriteObject();
	    	parent = PROC.display;
		}else
			oos.defaultWriteObject();
	}
	*/

	@Override
	public int getColor() {
		return color;
	}

	@Override
	public int setColor(int color) {
		this.color = color;
		return color;
	}

	@Override
	public int getStrokeColor() {
		return strokeColor;
	}

	@Override
	public void setStrokeColor(int strokeColor) {
		this.strokeColor = strokeColor;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public void setWH(int width, int height) {
		
		this.width = width;
		this.height = height;
	}

	@Override
	public int getHeight() {
		return height;
	}

	/*
	public int setHeight(int height) {
		this.height = height;
		return height;
	}
	*/
	
	@Override
	public GuiType getGuiType() {
		return guiType;
	}

	@Override
	public void setGuiType(GuiType guiType) {
		this.guiType = guiType;
	}

	
}
