package pr;

import gui.Position;
import gui.Positionables;
import processing.core.PMatrix2D;

public interface Positionable {
	
	public float getX();
	public float getY();
	public void setPos(float x, float y);
	public Position getPosObject();
	public void setPosObject(Position p);
	
	default public void setGlobal(float x, float y) {
		
		setPos(x, y);
	}
	
	
	/*
	 * Default Vector methods
	 */
	
	public default Positionable copyPos() {
		return Positionables.copyPosition(this);
	}
	
	public default Positionable setPos(Positionable pos) {
		setPos(pos.getX(), pos.getY());
		return this;
	}
	
	public default float distance(float x, float y) {
		return Positionables.distance(this, x, y);
	}
	
	public default float distance(Positionable p) {
		return Positionables.distance(this, p);
	}
	
	public default boolean withinRange(Positionable p, float range) {
		return Positionables.withinRange(this, p, range);
	}
	
	public default Positionable subtract(Positionable p) {
		Positionables.subtract(this, p);
		return this;
	}
	
	public default Positionable subtract(float x, float y) {
		Positionables.subtract(this, x, y);
		return this;
	}
	
	public default Positionable add(Positionable p) {
		Positionables.add(this, p);
		return this;
	}
	
	public default Positionable add(float x, float y) {
		Positionables.add(this, x, y);
		return this;
	}
	
	public default Positionable multiply(float f) {
		Positionables.multiply(this, f);
		return this;
	}
	
	public default Positionable divide(float f) {
		Positionables.divide(this, f);
		return this;
	}
	
	public default Positionable apply(PMatrix2D m) {
		Positionables.apply(this, m);
		return this;
	}
}
