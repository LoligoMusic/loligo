package pr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Supplier;

import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.modules.reader.MidiIn;
import module.modules.reader.MidiOut;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import themidibus.MidiBus;
import util.Utils;


public abstract class AbstractMidiObj extends AbstractModule {
	
	protected static final int ALL = 16;
	
	protected static MidiBus midiBus;
	private static boolean open = false;
	final protected static ArrayList<MidiIn> ins  = new ArrayList<MidiIn>();
	final protected static ArrayList<MidiOut> outs = new ArrayList<MidiOut>();
	
	protected String device;
	
	protected int controller = 0, channel = 0;
	protected int pitch = 0, velocity = 100, tempPitch, tempVelocity, tempController;
	protected int controllerID, channelID;
	 
	
	public AbstractMidiObj() {
		
		if(midiBus == null || !open) {
			
			midiBus = new MidiBus();//(RootClass.mainProc(), -1, -1);
			open = true;
		}
	}
	
	
	public abstract String[] availableDevices();
	
	public abstract void setDevice(final int num);
	
	
	public void setDevice(final String in) {
		
		String[] devs = availableDevices();
		for (int i = 0; i < devs.length; i++) {
			if(devs[i].equals(in)) {
				setDevice(i);
				break;
			}
		}
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector n = NodeInspector.newInstance(this, ni -> {
		
			Supplier<Integer> up = ()->Arrays.asList(availableDevices()).indexOf(device);
			
			DropDownMenu menu_dev = new DropDownMenu(this::setDevice, up, availableDevices());
			
			menu_dev.setItemsListUpdateAction(() -> {
				MidiBus.findMidiDevices();
				return availableDevices();
			});
			
			ni.addLabeledEntry("Device", menu_dev);
			
			ArrayList<String> list = new ArrayList<String>(Arrays.asList(Utils.incrementingNumbers(16, 1)));
			list.add("All");
			
			DropDownMenu menu_chan = new DropDownMenu(this::setChannel, ()->channel, list.toArray(new String[list.size()]));
			
			ni.addLabeledEntry("Channel", menu_chan);
			
			/*
			ni.addLabeledEntry(
					"Controller",
					new ValuePicker(this::setController, ()->(float)(controller),"setController", 128)
				);
			*/	
			
			menu_chan.update();
			menu_dev.update();
			
			});
			
		
		return n;
	}
	
	
	protected void removeDevice(ArrayList<? extends AbstractMidiObj> list) {
		
		int b = 0;
		
		for(AbstractMidiObj m : list)
			if(device == m.device) 
				b++;
		
		if(b <= 1) {
			if(list == ins) {
				midiBus.removeInput(device);
			}else {
				midiBus.removeOutput(device);
			}
		}
	}
	
	
	public void setController(final int c) {
		controller = c;
	}
	
	
	public void setChannel(final int c) {
		channel = c;
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		
		if(outs.size() == 0 && ins.size() == 0) {
			if(midiBus != null)
				midiBus.close();
			open = false;
		}
		
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
				.addExtras(device, channelID, controllerID);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		
		setDevice(data.nextString());
		channelID = data.nextInt();
		controllerID = data.nextInt();
	}

	
}
