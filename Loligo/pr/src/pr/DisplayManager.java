package pr;

import java.util.ArrayList;
import java.util.List;

import constants.DisplayFlag;
import gui.DisplayObjects;
import gui.LineContainer;
import gui.MenuBar;
import lombok.Getter;
import lombok.Setter;
import module.Anim;
import module.Canvas;
import module.Module;
import patch.Domain;
import patch.LoligoPatch;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public class DisplayManager extends DisplayContainer implements DisplayManagerIF{
	
	@Getter
	private final Domain domain;
	private boolean processActive = true;
	@Setter@Getter
	private boolean guiActive = true;
	private DisplayObjectIF animContainer;
	private DisplayObject guiContainer;
	@Getter
	private LineContainer lineContainer;
	@Getter
	private MenuBar menuBar;
	
	private Canvas canvas;
	
	@Setter
	private boolean allowCanvas = true;
	private int breakPoint = 0;
	
	public DisplayManager(Domain domain){
		
		super(domain);
		
		this.domain = domain;
		
		setInput(new UserInput());
		
		setDm(this);
		setFlag(DisplayFlag.RECT, true);
		
		setColor(0xff000000);
		
		renderList = new ArrayList<>();
		
		animContainer = new DisplayObject();
		guiContainer = new DisplayObject();
		
		animContainer.setFlag(DisplayFlag.DRAWABLE, false);
		guiContainer.setFlag(DisplayFlag.DRAWABLE, false);
		
		lineContainer = new LineContainer();
		
		menuBar = MenuBar.createMenuBar((LoligoPatch)domain);
	}
	
	@Override
	public void initGraphics(PGraphics g) {
	
		super.initGraphics(g);
		
		setWH(g.width, g.height);
		
		getInput().init(getDomain());
		
		guiContainer.addChild(lineContainer);
		getContainer().addChild(guiContainer, animContainer);
		
		lineContainer.init(this);
		
	}
	
	
	@Override
	public void draw() {
		
		if(getListChanged())
			updateDisplayList();
		
		frameCount++;
		
		enterFrameContext.dispatchEnterFrameEvent();
		
		g.beginDraw();
		
		g.background(getColor());
		
		renderAnimLayer(g);
		
		if(processActive)
			processReaders();
		
		if(guiActive)
			renderGuiLayer(g);
		
		g.endDraw();
		
		getInput().mouseCheck();
		
		if(processActive) {
			
			processPatch();
		}	
		
		exitFrameContext.dispatchExitFrameEvent();
		
	
	}

	protected void processPatch() {
		RootClass.signalFlowManager().processPatch();
	}

	protected void processReaders() {
		
		RootClass.signalFlowManager().processReaders();
		
		if(getDomain().getSignalFlow() != null)
			getDomain().getSignalFlow().processReaders();
	}
	
	@Override
	public void renderGuiLayer(PGraphics g) {
		DisplayObjectIF d;
		for (int i = breakPoint; i < renderList.size(); i++) {
			d = renderList.get(i);
			if(d.checkFlag(DisplayFlag.DRAWABLE))
				d.render(g);
		}
	}

	@Override
	public void renderAnimLayer(PGraphics g) {
		
		if(canvas != null)
			canvas.render(g);
		
		DisplayObjectIF d;
		for (int i = 0; i < breakPoint; i++) {
			d = renderList.get(i);
			if(d.checkFlag(DisplayFlag.DRAWABLE))
				d.render(g);
		}
	}
	
	@Override
	public List<DisplayObjectIF> getGuiList() {
		
		return renderList.subList(breakPoint, renderList.size());
	}
	
	@Override
	public void updateDisplayList() {
		
		resetListChanged();
		renderList = DisplayObjects.getOffspring(animContainer);
		breakPoint = renderList.size();
		renderList.addAll(DisplayObjects.getOffspring(guiContainer));
		
	}
	
	@Override
	public void addModuleDisplay(Module m) {
		
		if(m.getGui() != null) {
			guiContainer.addChild(m.getGui());
			lineContainer.updateInSlotList();
		}
		
		if(m instanceof Anim a) 
			addAnimObject(a.getAnim());
	}
	
	
	@Override
	public void pauseProcessing(boolean pause) {
		
		processActive = !pause;
		if(pause){
			//RootClass.audioManager().unmute();
		}else{
			//RootClass.audioManager().mute();
		}
	}

	@Override
	public boolean isProcessActive() {
		
		return processActive;
	}
	
	@Override
	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
		canvas.addedToDisplay();
		getContainer().addChild(canvas);
	}
	
	@Override
	public Canvas canvas() {
		
		if(canvas == null && allowCanvas)
			setCanvas(new Canvas());
		
		return canvas;
	}
	
	@Override
	public void removeCanvas() {
		
		canvas = null;
		canvas.removedFromDisplay();
	}

	
	@Override
	public MenuBar showMenuBar() {
		menuBar.setPatch((LoligoPatch) getDomain());
		addGuiObject(menuBar);
		return menuBar;
	}
	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		if(menuBar.isEnabled()) {
			showMenuBar();
		}
	}
	
	@Override
	public void removedFromDisplay() {
		DisplayObjects.removedFromDisplayNotifyChildren(animContainer);
		DisplayObjects.removedFromDisplayNotifyChildren(guiContainer);
		DisplayObjects.removedFromDisplayNotifyChildren(lineContainer);
		DisplayObjects.removedFromDisplayNotifyChildren(menuBar);
		if(canvas != null)
			DisplayObjects.removedFromDisplayNotifyChildren(canvas);
	}
	
	@Override
	public void addAnimObject(DisplayObjectIF d) {
		
		animContainer.addChild(d);
	}
	
	@Override
	public void addGuiObject(DisplayObjectIF d) {
		
		guiContainer.addChild(d);
	}
	
	@Override
	public void addChild(DisplayObjectIF d) {
		addGuiObject(d);
	}
	
	
	
}
