package pr;

import java.util.Locale;

import audio.AudioManager;
import lombok.SneakyThrows;
import module.Console;
import module.signalFlow.SignalFlowManager;
import patch.Backstage;
import patch.Loligo;
import patch.LoligoPatch;
import save.Settings;
import util.CustomURLStreamHandler;
import util.ExceptionHandler;


public class RootClass {

	private static Loligo PROC;
	
	private static Backstage STAGE;
	
	private static SignalFlowManager signalFlowManager;
	
	private static AudioManager AUDIO;
	
	
	public static void init() {
		
		System.setProperty("sun.java2d.dpiaware", "false");
		
		ExceptionHandler.set();
		
		Settings.loadSettings();
		
		//shortcuts = new Shortcuts();
		CustomURLStreamHandler.init();
		Console.init();
		
		signalFlowManager = new SignalFlowManager();
		
		//initAudioManager();
		AUDIO = new AudioManager();
		
		initMainPatch();
		
		
		
		Locale.setDefault(Locale.ENGLISH);
		
		
		//ClassPreloader.preload();
	}
	
	@SneakyThrows
	public static void initMainPatch() {
		
		PROC = new Loligo();
		STAGE = new Backstage(PROC);
		
		PROC.setFrameRate(Settings.DATA.fps);
		PROC.startSketch();
		
		ExceptionHandler.set(); // Called again, bc startSketch sets its own DefaultExceptionHandler
		
		PROC.setMainWindow();
		
		AUDIO.initAudio();
		STAGE.init();
		
	}
	
	public static void initAudioManager() {
		AUDIO = new AudioManager();
		AUDIO.initAudio();
	}
	
	public static AudioManager audioManager() {
		
		return AUDIO;
	}
	
	public static LoligoPatch mainPatch() {
		
		return STAGE.getActivePatch();
	}

	public static SignalFlowManager signalFlowManager() {
		
		return signalFlowManager;
	}
	
	public static Backstage backstage() {
		
		return STAGE;
	}
	
	public static Loligo mainProc() {
		
		return PROC;
	}

	public static DisplayManagerIF mainDm() {
		
		return (DisplayManager) STAGE.getActivePatch().getDisplayManager();
	}

}
