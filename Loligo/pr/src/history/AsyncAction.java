package history;

import patch.GuiDomain;

/**
 * 
 * Delegation Class for Actions that register in the history of one patch, but execute in the draw loop of another patch.
 *
 */

public class AsyncAction implements Action{

	private final Action action;
	private final GuiHistory history;
	
	public AsyncAction(GuiDomain domain, Action action) {
		this(domain.getHistory(), action);
	}
	
	public AsyncAction(GuiHistory history, Action action) {
		
		this.action = action;
		this.history = history;
	}

	public void execute() {
		
		history.execute(action::execute);
	}

	public void undo() {
		
		history.execute(action::undo);
	}

	public void redo() {
		
		history.execute(action::redo);
	}

	@Override
		public String toString() {
		
			return "Async[" +super.toString() + "]";
		}
	
}
