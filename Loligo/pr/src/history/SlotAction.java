package history;

import static constants.SlotEvent.SLOT_CONNECT;
import static constants.SlotEvent.SLOT_DISCONNECT;

import constants.SlotEvent;
import module.inout.Input;
import module.inout.Output;

record SlotAction(Input in, Output out, SlotEvent type) implements Action {

	@Override
	public void undo() {
		
		var m = in.getModule().getDomain().getModuleManager(); 
		
		if(type == SLOT_CONNECT) m.disconnect(in);
		else 					 m.connect(in, out);
	}

	@Override
	public void execute() {
		
		var m = in.getModule().getDomain().getModuleManager(); 
		
		if(type == SLOT_DISCONNECT) m.disconnect(in);
		else 						m.connect(in, out);
		
	}
	
	@Override
	public String toString() {
		return "Slot" + (type == SLOT_CONNECT ? "+" : "-");
	}

}
