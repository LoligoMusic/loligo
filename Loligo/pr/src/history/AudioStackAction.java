package history;

import static constants.AudioStackContext.Type.AUDIOSTACK_ADD;
import static constants.AudioStackContext.Type.AUDIOSTACK_REMOVE;

import constants.AudioStackContext.AudioStackEvent;

public record AudioStackAction(AudioStackEvent event) implements Action {

	@Override
	public void execute() {
		var s = event.stack();
		var m = event.module();
		if(event.event() == AUDIOSTACK_ADD) s.addModule(m, event.index());
		else								s.removeModule(m);
	}
	

	@Override
	public void undo() {
		var s = event.stack();
		var m = event.module();
		if(event.event() == AUDIOSTACK_REMOVE) s.addModule(m, event.index());
		else 								   s.removeModule(m);
	}		
	
	@Override
	public String toString() {
		return "Stack" + (event.event() == AUDIOSTACK_ADD ? "+" : "-");
	}

}
