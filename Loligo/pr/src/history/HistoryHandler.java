package history;

import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import patch.Domain;
import patch.GuiDomain;


public class HistoryHandler {

	protected final static int mergeLimit = 10;
	
	private final GuiDomain domain;
	protected final Stack<List<Action>> actions, redos;
	
	protected int maxDepth = 10, lastFrame = -1;
	@Setter@Getter
	protected boolean active = true;
	protected boolean groupActions = false;
	@Setter
	private Consumer<Action> onRunAction = a -> {};
	
	
	public HistoryHandler(GuiDomain domain) { 
		this.domain = domain;
		actions = new Stack<>();
		redos = new Stack<>();
	}
	

	public void init() {
		
	}
	
	
	public void undo() {
		
		if (!actions.empty()) {

			active = false;

			var acs = actions.pop();
			redos.push(acs);
			
			var re = Lists.reverse(acs); 
			re.forEach(Action::undo);
			
			active = true;
		}
	}

	public void redo() {
		
		if (!redos.empty()) {

			active = false;

			var acs = redos.pop();
			actions.push(acs);
			
			acs.forEach(Action::redo);
			active = true;
		}
	}
	
	
	public Action addAction(Runnable exe, Runnable undo) {
		return addAction(new DynamicAction(exe, undo));
	}
	
	
	public Action addAction(Action a) {
		if(active) {
			doAddAction(a);
		}
		return a;
	}
	
	
	protected void doAddAction(Action a) {
		
		redos.clear();

		int curFrame = frameCount();
		
		if(!actions.empty() && (curFrame - lastFrame < mergeLimit || groupActions)) {
			var acs = actions.peek();
			acs.add(a);
		} else {
			actions.push(Lists.newArrayList(a));
		}

		lastFrame = curFrame;
		
		if (actions.size() > maxDepth)
			actions.remove(0);

		onRunAction.accept(a);
	}

	
	public boolean hasNextUndo() {
		return !actions.isEmpty(); 
	}
	
	
	public boolean hasNextRedo() {
		return !redos.isEmpty();
	}
	
	
	private int frameCount() {
		return Domain.getBaseDomain(domain).getDisplayManager().getFrameCount();
	}
	
	
	public void clear() {

		actions.clear();
		redos.clear();
	}

	
	private void beginGroup() {
		
		if (lastFrame != frameCount())
			doAddAction(DynamicAction.empty());
		
		groupActions = true;
	}

	
	private void endGroup() {

		groupActions = false;
		lastFrame = frameCount();
	}

}
