package history;

import java.util.function.Consumer;

import gui.GuiSender;

public record SetValueAction<T>(GuiSender<T> client, T f0, T f1, Consumer<T> consumer) implements Action {
	
	
	@Override
	public void execute() {
		if(consumer != null)
			consumer.accept(f1);
		client.setValue(f1);
	}
	
	
	@Override
	public void undo() {
		if(consumer != null)
			consumer.accept(f0);
		client.setValue(f0);
	}
	
	@Override
	public String toString() {
		return "setValue from " + f0 + " to " + f1;
	}
	
}
