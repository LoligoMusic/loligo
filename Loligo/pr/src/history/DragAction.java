package history;

import java.util.List;

import pr.DisplayObjectIF;
import pr.userinput.UserInput;

record DragAction(List<DisplayObjectIF> items, UserInput input, int dx, int dy) implements Action {
	
	@Override
	public void execute() {
		move(dx, dy);
	}
	
	@Override
	public void undo() {
		move(-dx, -dy);
	}
	
	private void move(int x, int y) {
		input.mouseDrag.setOriginRelative(items, x, y, true);
	}

	@Override
	public String toString() {
		return "Drag";
	}

}