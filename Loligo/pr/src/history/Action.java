package history;

public interface Action extends Runnable{
	
	public void execute();
	
	public default void run() {
		execute();
	}
	
	public default void undo() {}
	
	public default void redo() {
		execute();
	}
	
}
