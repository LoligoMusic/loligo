package history;

import static constants.DisplayFlag.DRAGABLE;
import static constants.InputEvent.DRAG;

import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

import com.google.common.collect.Lists;

import constants.InputEvent;
import gui.Position;
import lombok.Getter;
import lombok.Setter;
import patch.Domain;
import patch.DomainType;
import patch.GuiDomain;
import pr.EnterFrameListener;
import pr.EventSubscriber;
import pr.ExitFrameListener;
import pr.userinput.UserInput;


public class GuiHistory implements EventSubscriber, EnterFrameListener, ExitFrameListener {

	protected final static int mergeLimit = 10;
	
	private final GuiDomain domain;
	protected final Stack<List<Action>> actions, redos;
	protected final Queue<Action> executionQueue = new ConcurrentLinkedQueue<>();

	protected int maxDepth = 10, lastFrame = -1;
	@Setter@Getter
	protected boolean active = true;
	protected boolean groupActions = false;
	@Setter
	private Consumer<Action> onRunAction = a -> {};
	
	
	public GuiHistory(GuiDomain domain) { 
		this.domain = domain;
		actions = new Stack<>();
		redos = new Stack<>();
	}
	

	public void init() {
		
		var dm = domain.getDisplayManager();
		dm.getInput().addInputEventListener(this);
		
		if(domain.domainType() != DomainType.SUBPATCH) {
			dm.addExitFrameListener(this);
			dm.addEnterFrameListener(this);
		}
	}
	
	public void undo() {
		
		if (!actions.empty()) {

			active = false;

			var acs = actions.pop();
			redos.push(acs);
			
			var re = Lists.reverse(acs); 
			re.forEach(Action::undo);
			
			//System.out.println("UNDO: " + re);
			
			active = true;

			// System.out.println(actions + " p: " + pos);
		}
	}

	public void redo() {
		
		if (!redos.empty()) {

			active = false;

			var acs = redos.pop();
			actions.push(acs);
			
			acs.forEach(Action::redo);
			
			//System.out.println("REDO: " + acs);
			
			active = true;
		}
	}

	Action firstGuiAction = null, lastGuiAction = null;
	
	public void execute(final Runnable r) {
		var a = new DynamicAction(r);
		execute(a, false);
	}
	
	public void execute(final Action action, boolean undoable) {

		executionQueue.add(action);
		
		if(undoable) {
		
			if(firstGuiAction == null)
				firstGuiAction = action;
			
			lastGuiAction = action;
			addAction(action);
		}
	}
	
	public void executeInMain(Runnable a) {
		executeIn(Domain.getBaseDomain(domain), a);
	}
	
	public void executeIn(GuiDomain d, Runnable a) {
		executeIn(d, new DynamicAction(a), false);
	}
	
	public void executeIn(GuiDomain d, Action a, boolean undoable) {
		var aa = new AsyncAction(d, a);
		execute(aa, undoable);
	}

	public void runExecutionQueue() {
		Action a;
		while ((a = executionQueue.poll()) != null) {
			a.execute();
		}
	}
	
	public Action addAction(Runnable exe, Runnable undo) {
		
		return addAction(new DynamicAction(exe, undo));
	}
	
	public Action addAction(Action a) {
		
		if(active) {
			doAddAction(a);
		}
		return a;
	}
	
	protected void doAddAction(Action a) {
		
		redos.clear();

		int curFrame = frameCount();
		
		
		if(!actions.empty() && (curFrame - lastFrame < mergeLimit || groupActions)) {
			
			var acs = actions.peek();
			acs.add(a);
		} else {

			actions.push(Lists.newArrayList(a));
		}

		lastFrame = curFrame;
		
		if (actions.size() > maxDepth)
			actions.remove(0);

		onRunAction.accept(a);
		//((LoligoPatch) domain).setPatchSaved(false);
	}

	public boolean hasNextUndo() {
		return !actions.isEmpty(); 
	}
	
	public boolean hasNextRedo() {
		return !redos.isEmpty();
	}
	
	private int frameCount() {
		return Domain.getBaseDomain(domain).getDisplayManager().getFrameCount();
	}
	
	public void clear() {
		actions.clear();
		redos.clear();
	}

	private void beginGroup() {
		
		if (lastFrame != frameCount())
			doAddAction(DynamicAction.empty());
		
		groupActions = true;
		
	}

	private void endGroup() {

		groupActions = false;
		lastFrame = frameCount();

	}

	private Position lastDrag;
	private boolean killDrag = false;

	@Override
	public void mouseEvent(InputEvent e, UserInput input) {

		if (active && (e.isDragEvent || e.isReleaseEvent) && e != DRAG) {
			
			switch(e) {
			case DRAG_START -> {
				
				var mods = input.selection.getSelection();
				boolean b = true;
				for (var d : mods) {  // TODO Hack um slot drag zu vermeiden
					if(!d.checkFlag(DRAGABLE)) {
						b = false;
						break;
					}
				}
				if(b) {
					lastDrag = new Position(input.mouseX, input.mouseY);
					beginGroup();
				}
			} 
			case DRAG_STOP -> {
				
				if(lastDrag != null) { 
					int dx = input.getMouseX() - (int) lastDrag.getX(),
						dy = input.getMouseY() - (int) lastDrag.getY();
					var da = createDragAction(input, dx, dy);
					doAddAction(da);
				}
				endGroup();
				killDrag = true;
			}
			default -> {}
			}
		}
	}
	
	protected Action createDragAction(UserInput input, int dx, int dy) {
		return new DragAction(input.selection.getSelection(), input, dx, dy);
	}
	
	@Override
	public void exitFrame() {
		/*
		if(killDrag) {
			lastDrag = null;
			killDrag = false;
			
			if(firstGuiAction != null && lastGuiAction != null) {
				
				final Action a0 = firstGuiAction, a1 = lastGuiAction;
				
				Action a = new DynamicAction(a1::execute, a0::undo) {
					@Override 
					public String toString() {
						return "GUI" + super.toString();
					}
				};
				doAddAction(a);
				lastGuiAction = firstGuiAction = null;
			}
		}
		*/
	}


	@Override
	public void enterFrame() {
		runExecutionQueue(); 
	}

	

}
