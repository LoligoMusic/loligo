package history;

import constants.AudioStackContext;
import constants.AudioStackContext.AudioStackEvent;
import constants.AudioStackListener;
import constants.ConstraintEventListener;
import constants.InputEvent;
import constants.ModuleEvent;
import constants.ModuleEventListener;
import constants.SlotEvent;
import constants.SlotEventListener;
import constraints.FollowerObject;
import constraints.LoligoPosConstraint;
import module.Module;
import module.inout.Input;
import module.inout.Output;
import patch.Domain;
import patch.DomainType;


public class History extends GuiHistory implements AudioStackListener, ConstraintEventListener, SlotEventListener, ModuleEventListener{

	private Domain domain;
	
	public History(Domain domain) { 
		super(domain);
		this.domain = domain;
	}
	

	public void init() {
		super.init();
		var m = domain.getModuleManager();
		m.addModuleEventListener(this);
		m.addSlotEventListener(this);
		AudioStackContext.instance.addListener(this); // TODO do something with AUdioStackCOntext
	}


	@Override
	public void audioStackEvent(AudioStackEvent e) {

		if (isHistoryEvent(e.stack())) {

			var ca = new AudioStackAction(e);
			
			/*
			if(lastDrag != null && lastDrag.length == 1) {
				
				
				if(e.event == InputEvent.AUDIOSTACK_ADD) {
					lastDrag[0].appendAction(ca);
					doAddAction(lastDrag[0]);
					lastDrag = null;
				}else
					doAddAction(ca);
				
			}else
			*/
				doAddAction(ca);
		}
		
		
	}
	
	
	@Override
	public void constraintEvent(InputEvent e, LoligoPosConstraint constraint, FollowerObject follower) {
		/*
		if (active) {

			boolean type = e == InputEvent.CONSTRAINT_ON ? ConstraintAction.ADD :
														   ConstraintAction.REMOVE;
			
			ConstraintAction ca = new ConstraintAction(constraint, follower.getTarget(), follower.getPathPosition() - constraint.getOffset(), type);
			
			if(lastDrag != null && lastDrag.length == 1) {
				
				lastDrag[0].setEndPoint(follower.getTarget().getX(), follower.getTarget().getY());
				
				if(type == ConstraintAction.ADD) {
					lastDrag[0].appendAction(ca);
					addAction(lastDrag[0]);
					lastDrag = null;
				}else
					addAction(ca);
				
			}else
			
				addAction(ca);
		}
		*/
	}
	

	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		if (e.isConnectDisconnect && isHistoryEvent(in.getModule())) {
			doAddAction(new SlotAction(in, out, e));
		}
	}
	

	@Override
	public void moduleEvent(ModuleEvent e, Module m) {
		if (isHistoryEvent(m)) {
			var ma = new ModuleAction(m, e);
			doAddAction(ma);
		}
	}
	
	private boolean isHistoryEvent(Module m) {
		return active && !m.isSubModule() && m.getDomain().domainType() != DomainType.PARTICLE_INSTANCE;
	}
	

}
