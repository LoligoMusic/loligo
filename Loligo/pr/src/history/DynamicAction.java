package history;

public class DynamicAction implements Action {

	public static Action empty() {
		return new DynamicAction(() -> {}, () -> {}, "Empty");
	}
	
	final String name;
	final Runnable exeFoo, undoFoo;
	
	public DynamicAction(Runnable exeFoo, Runnable undoFoo, String name) {
		this.exeFoo = exeFoo;
		this.undoFoo = undoFoo;
		this.name = name;
	}
	
	public DynamicAction(Runnable exeFoo, Runnable undoFoo) {
		this(exeFoo, undoFoo, "Dynamic");
	}
	
	public DynamicAction(Runnable exeFoo) {
		this(exeFoo, () -> {});
	}
	
	@Override
	public void execute() {
		exeFoo.run();
	}

	@Override
	public void undo() {
		undoFoo.run();
	}
	
	@Override
	public String toString() {
		return "dynamic ";
	}

	public DynamicAction reverse() {
		return new DynamicAction(undoFoo, exeFoo);
	}
}
