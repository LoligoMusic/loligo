package history;

import constants.ModuleEvent;
import module.Module;
import module.ModuleDeleteMode;

record ModuleAction(Module module, ModuleEvent operation) implements Action {
	
	@Override
	public void undo() {
		switch(operation) {
		case MODULE_ADD    -> delete();
		case MODULE_REMOVE -> add();
		}
	}

	@Override
	public void execute() {
		switch(operation) {
		case MODULE_ADD    -> add();
		case MODULE_REMOVE -> delete();
		}
	}
	
	private void delete() {
		module.getDomain().removeModule(module, ModuleDeleteMode.NODE);
	}
	
	private void add() {
		
		module.getDomain().addModule(module);
		
		/*
		var sdc = new SaveDataContext(SaveMode.DUPLICATION);
		sdc.setSaveTarget(SaveTarget.MEMENTO);
		sdc.addModule(module);
		var pd = sdc.buildPatchData();
		
		new RestoreDataContext(pd, (LoligoPatch) module.getDomain()).restorePatch();
		var md = pd.getNodes().get(0);
		this.module = md.getModuleOut();
		*/
		
	}
	
	@Override
	public String toString() {
		return operation.toString();
	}

	

}
