package errors;


import constants.AbstractEventManager;
import module.Module;


public class ErrorHandler extends AbstractEventManager<LoligoErrorListener>{
	
	
	public void dispatchErrorEvent(Module target, LoligoException ex) {
		
		ErrorEvent e = new ErrorEvent(target, ex);
		
		listeners.forEach(s -> s.onLoligoError(e));
	}
	
}
