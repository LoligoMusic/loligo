package errors;

import lombok.Getter;
import module.Module;

public class ErrorEvent {
	
	@Getter
	private final Module target;
	@Getter
	private final LoligoException exception;
	
	
	public ErrorEvent(Module target, LoligoException exception) {
		
		this.target = target;
		this.exception = exception;
	}

	
}
