package errors;

import lombok.Getter;

public class LoligoException extends Exception {

	private static final long serialVersionUID = 1L;
	
	@Getter
	private final Exception exception;
	@Getter
	private final String message;
	
	public LoligoException(Exception e, String message) {
		
		this.exception = e;
		this.message = message;
	}
}
