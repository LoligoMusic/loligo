package util;

import java.util.Arrays;
import java.util.function.Predicate;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.var;
import module.Module;
import patch.Domain;

@NoArgsConstructor(access = AccessLevel.PRIVATE)

public final class Naming {
	
	public static final String COPY_SUFFIX = "-Copy", DEFAULT_ORDINAL = "01";
	
	
	public static String defaultModuleName(Module m) {
		return m.getName() + '.' + DEFAULT_ORDINAL;
	}
	
	
	public static String findFreeModuleName(Module mod, Domain dom) {
		String s = defaultModuleName(mod);
		return findFreeModuleName(s, dom);
	}
	
	
	public static String findFreeModuleName(String n, Domain dom) {
		
		Predicate<String> p = s -> {
			return dom.getContainedModules().stream().anyMatch(m -> s.equals(m.getLabel()));
		};
		return findFreeName(n, p);
	}
	
	
	public static String findFreeName(String name, Predicate<String> exists) {
		
		if(!exists.test(name))
			return name;
		
		int dLen = digitSuffixLength(name);
		String base;
		int i;
		if(dLen == 0) {
			base = name;
			dLen = 2;
			i = 0;
		}else {
			base = name.substring(0, name.length() - dLen);
			String s = name.substring(name.length() - dLen);
			i = Integer.parseInt(s);
		}
		
		if(base.charAt(base.length() - 1) != '.') {
			base += '.';
		}
		
		String n;
		do {
			i++;
			String num = toZeroPaddedString(i, dLen);
			n = base + num;
		}while(exists.test(n));
		
		return n;
	}
	
	
	static int digitSuffixLength(String n) {
		
		int len = n.length();
		for (int i = 0; i < len; i++) {
			char c = n.charAt(len - i - 1);
			if(!Character.isDigit(c)) {
				return i;
			}
		}
		return len;
	}
	
	
	public static String toZeroPaddedString(int i, int len) {
		String n = String.valueOf(i);
		
		int nlen = len - n.length();
		if(nlen > 0) {
			var z = new char[nlen];
			Arrays.fill(z, '0');
			String zs = new String(z);
			n = zs + n;
		}
		
		return n;
	}

	
	public static String markAsCopy(String label) {
		
		if(!label.contains(COPY_SUFFIX)) 
			return label + COPY_SUFFIX;
		
		return label;
	}
}
