package util;

public interface Container<Ob> {
	public Ob getValue();
	public void setValue(Ob o);
}
