package util;

import java.util.AbstractList;

public class ArrayWrapperList<T> extends AbstractList<T> {

	private final T[] data;
	
	public ArrayWrapperList(T[] data) {
	
		this.data = data;
	}
	
	@Override
	public T get(int index) {
		
		return data[index];
	}

	@Override
	public int size() {
		
		return data.length;
	}

}
