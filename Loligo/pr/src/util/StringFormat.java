package util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonNodeReader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import lombok.var;
import save.SaveJson;


public enum StringFormat {
	
	PATH			(StringFormat::isPath			),
	URL				(StringFormat::isURL			),
	LOLIGOFILE		(StringFormat::isLoligoJson		),
	JSON			(StringFormat::isJson			),
	HTML_CLIPBOARD	(StringFormat::isHtmlClipboard	),
	XML				(StringFormat::isXML			),
	UNKNOWN			(StringFormat::isUnknown		);
	
	
	private final Predicate<String> tester;
	
	private StringFormat(Predicate<String> tester) {
		this(tester, "plain");
	}
	
	private StringFormat(Predicate<String> tester, String type) {
		this.tester = tester;
	}
	
	
	public boolean test(String s) {
		return tester.test(s);
	}
	
	
	
	
	public static boolean isPath(String s) {
		try {
			Paths.get(s);
			return true;
		}catch(InvalidPathException e) {
			return false;
		}
	}
	
	
	public static boolean isURL(String s) {
		try {
			new URL(s);
			return true;
		} catch (MalformedURLException e) {
			return false;
		}
	}
	
	
	public static StringFormat getFormat(String s) {
		
		return EnumSet.complementOf(EnumSet.of(UNKNOWN)).stream()
				.filter(e -> e.test(s))
				.findFirst()
				.orElse(UNKNOWN);
	}
	
	
	public static boolean isJson(String s) {
		
		ObjectMapper om = new ObjectMapper();
		om.clearProblemHandlers();
		try {
			om.readTree(s);
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	
	public static boolean isLoligoJson(String s) {
		try {
			
			JsonNode content = SaveJson.defaultObjectMapper().readTree(s);
			
			var nr = new JsonNodeReader();
			var is = StringFormat.class.getResourceAsStream("/save/loligo_json-schema.json");
			JsonNode n = nr.fromInputStream(is);
			JsonSchema sh = JsonSchemaFactory.byDefault().getJsonSchema(n);
			
			ProcessingReport r = sh.validate(content);
			return r.isSuccess();
			
		} catch (IOException e) {
			//e.printStackTrace();
			return false;
		} catch (ProcessingException e) {
			return false;
		}
	}
	
	
	public static boolean isXML(String s) {
		
		InputStream is = new ByteArrayInputStream(s.getBytes(Charset.forName("UTF-8")));
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
			//Silent Errorhandler
			ErrorHandler e = new ErrorHandler() {
				@Override public void warning(SAXParseException e) throws SAXException { throw e; }
				@Override public void error(SAXParseException e) throws SAXException { throw e; }
				@Override public void fatalError(SAXParseException e) throws SAXException { throw e; }
			};
			builder.setErrorHandler(e);
		} catch (ParserConfigurationException e1) {}
		
		try {
			builder.parse(is);
			return true;
		} catch (SAXException | IOException e) {
			return false;
		}
	}
	
	
	public static boolean isHtmlClipboard(String s) {
		
		return match(s, ".*Version:\\d+\\.\\d+\\sStartHTML:\\d+\\sEndHTML:\\d+\\sStartFragment:\\d+\\sEndFragment:\\d+\\s*");
	}
	
	
	public static boolean isUnknown(String s) {
		return getFormat(s) == UNKNOWN; 
	}
	
	
	public static boolean match(String s, String regex) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(s);
		return m.find();
	}
}
