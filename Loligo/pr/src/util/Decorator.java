package util;

public abstract class Decorator<D, R>{
	D decorated;
	
	public abstract R calculate();
}
