package util;

import static java.lang.Math.floor;
import static java.lang.Math.floorMod;
import static net.beadsproject.beads.data.Pitch.pitchNames;

import org.jboss.forge.roaster.ParserException;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import net.beadsproject.beads.data.Pitch;

@NoArgsConstructor(access = AccessLevel.PRIVATE)

public final class Notes {
	
	@RequiredArgsConstructor
	static public enum Scale {
		MAJOR(Pitch.major),
		MINOR(Pitch.minor),
		DORIAN(Pitch.dorian),
		PENTATONIC(Pitch.pentatonic),
		CIRCLE_OF_FIFTHS(Pitch.circleOfFifths);
		public final int[] scale;
		public double degree(int i) {
			return scale[i % scale.length];
		}
	}


	public static final String[] pitchNamesFlat = new String[]{"C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"};
	public static final String[] pitchNamesSharp = new String[]{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
	public static final int A4 = 69, A0 = 21, C8 = 108;
	
	
	static public double valueOf(String s) throws ParserException {
		
		String t = s.strip().toUpperCase();
		
		for (int i = 0; i < pitchNames.length; i++) {
			String nf = pitchNamesFlat[i],
				   ns = pitchNamesSharp[i];
			if(t.startsWith(nf) || t.startsWith(ns)) {
				
				int ix = nf.length();
				int o = 0;
				double cts = 0;
				
				if(ix < t.length()) {
					char cn = t.charAt(ix);
					if(Character.isDigit(cn)) {
						o = cn - '0' + 1; 
					}
					cts = parseCents(t);
				}
				return i + o * 12 + cts;
			}
		}
		throw new ParserException("Error parsing \'" + t + "\' to Note.");
	}
	
	
	public static double parseCents(String t) {
		
		int ic = t.indexOf('+');
		ic = ic < 0 ? t.indexOf('-') : ic;
		
		if(ic < 0) 
			return 0;
		
		int cu = t.indexOf('c', ic);
		if(cu < 0) 
			cu = t.length();
		cu = Math.min(cu, ic + 3);
		
		String cs = t.substring(ic, cu);
		int cents =  Integer.parseInt(cs);
		
		return cents / 100d;
	}

	
	public static String toNoteName(double d) {
		return toNoteName(d, pitchNamesSharp);
	}
	
	
	public static String toNoteName(double d, String[] pitchNames) {
		
		double c = (d % 1) * 100;
		int cts = (int)(Math.round(c * 100) / 100d);
		char sign = '+';
		if(cts > 50) {
			cts = 100 - cts;
			sign = '-';
			d++;
		}
		
		int len = pitchNames.length;
		int i = (int) (d % len);
		int o = (int) (d / len) - 1;
		String s = pitchNames[i] + o;
		
		if(cts != 0) {
			s = (s + " " + sign) + cts + "c";
		}
		
		return s;
	}


	public static float toFrequency(double d) {
		return toFrequency((float) d);
	}
	
	
	public static float toFrequency(float d) {
		return Pitch.mtof(d);
	}
	
	public static float freqToNote(double frequency) {
		return Pitch.ftom((float) frequency);
	}
	
	public static float degreeInScale(int n, int[] scale) {
		
		int len = scale.length;
		
		int i = floorMod(n, len);
		int t = scale[i];
		int oct =  (int) (floor((float) n / len) * 12);
		
		return oct + t; 
	}
	
	
}
