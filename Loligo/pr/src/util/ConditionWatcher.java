package util;

import java.util.function.BooleanSupplier;
import pr.DisplayManagerIF;
import pr.EnterFrameListener;


public class ConditionWatcher implements EnterFrameListener{

	private final DisplayManagerIF dm;
	private final BooleanSupplier condition;
	private final Runnable action;
	
	
	public ConditionWatcher(DisplayManagerIF dm, BooleanSupplier condition, Runnable action) {
		
		this.dm = dm;
		this.condition = condition;
		this.action = action;
	}
	
	
	public void run() {
		
		dm.addEnterFrameListener(this);
	}
	
	
	@Override
	public void enterFrame() {
		
		if(condition.getAsBoolean()) {
			dm.removeEnterFrameListener(this);
			
			action.run();
		}
	}
}
