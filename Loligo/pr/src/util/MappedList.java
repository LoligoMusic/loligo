package util;

import java.util.AbstractList;
import java.util.List;
import java.util.function.Function;

public class MappedList<K, V> extends AbstractList<V> {

	private final List<K> keys;
	private final Function<K, V> mapper;
	
	public MappedList(List<K> keys, Function<K, V> mapper) {
		
		this.mapper = mapper;
		this.keys = keys;
	}
	
	
	public List<K> getKeys() {
		
		return keys;
	}
	

	@Override
	public V get(int index) {
		
		return mapper.apply(keys.get(index));
	}

	@Override
	public int size() {
		
		return keys.size();
	}

}
