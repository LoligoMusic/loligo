package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ContainerList<Ob, Con extends Container<Ob>> implements List<Ob> {
	
	private final ArrayList<Con> containers = new ArrayList<Con>();
	
	public ContainerList() {
		
	}
	
	protected Con newContainer(Ob o) {
		throw new UnsupportedOperationException("newContainer() must be overridden.");
	}
	
	public Con getContainer(int i) {
		return containers.get(i);
	}
	
	public Con getContainer(Ob o) {
		for (Con c : containers) 
			if(c.getValue() == o)
				return c;
		return null;
	}
	
	public int indexOfContainer(Con c) {
		return containers.indexOf(c);
	}
	
	public List<Con> getContainers() {
		
		return containers;
	}
	
	public void removeValue(Ob o) {
		for (Con c : containers) 
			if(c.getValue() == o)
				containers.remove(c);
	}
	
	public boolean add(Ob o) {
		Con c = newContainer(o);
		if(c.getValue() == null)
			c.setValue(o);
		return containers.add(c);
	}
	
	public boolean add(Con e) {
		if(e.getValue() == null)
			throw new java.lang.IllegalArgumentException("Empty Container");
		return containers.add(e);
	}

	public void add(int index, Ob o) {
		Con c = newContainer(o);
		c.setValue(o);
		containers.add(index, c);
	}
	
	public void add(int index, Con element) {
		if(element.getValue() != null)
			containers.add(index, element);
	}

	@Deprecated
	public boolean addAll(Collection<? extends Ob> c) {
		throw new UnsupportedOperationException();
	}

	@Deprecated
	public boolean addAll(int index, Collection<? extends Ob> c) {
		throw new UnsupportedOperationException();
	}

	public void clear() {
		containers.clear();
	}

	@Deprecated
	public Object clone() {
		throw new UnsupportedOperationException();
	}

	public boolean contains(Object o) {
		return indexOf(o) >= 0;
	}

	
	public boolean containsAll(Collection<?> c) {
		for (Object o : c) 
			if(!contains(o))
				return false;
		return true;
	}

	public void ensureCapacity(int minCapacity) {
		containers.ensureCapacity(minCapacity);
	}

	public boolean equals(Object o) {
		return containers.equals(o);
	}

	public Ob get(int index) {
		return containers.get(index).getValue();
	}

	public int hashCode() {
		return containers.hashCode();
	}

	public int indexOf(Object o) {
		for (int i = 0; i < containers.size(); i++) {
			if(containers.get(i).getValue() == o)
				return i;
		}
		return -1;
	}

	public boolean isEmpty() {
		return containers.isEmpty();
	}

	public Iterator<Ob> iterator() {
		Iterator<Ob> iter = new Iterator<Ob>() {
			private int index = 0;
			
			@Override
			public boolean hasNext() {
				return index < containers.size() && containers.get(index) != null;
			}

			@Override
			public Ob next() {
				return containers.get(index++).getValue();
			}

			@Override
			public void remove() {}
		};
		return iter;
	}

	public int lastIndexOf(Object o) {
		for (int i = containers.size() - 1; i >= 0; i++) 
			if(containers.get(i).getValue() == o)
				return i;
		return -1;
	}
	
	@Deprecated
	public ListIterator<Ob> listIterator() {
		throw new UnsupportedOperationException("listIterator() not implemented");
	}

	@Deprecated
	public ListIterator<Ob> listIterator(int index) {
		throw new UnsupportedOperationException("listIterator(int) not implemented");
	}

	public Ob remove(int index) {
		return containers.remove(index).getValue();
	}

	public boolean remove(Object o) {
		int i = indexOf(o);
		if(i >= 0)
			return remove(i) == null ? false :true;
		else
			return false;
	}
	
	public boolean removeContainer(Con c) {
		return containers.remove(c);
	}

	public boolean removeAll(Collection<?> c) {
		boolean b = true;
		for (Object o : c) 
			if(!remove(o))
				b = false;
		return b;
	}
	
	public boolean retainAll(Collection<?> c) {
		for (Object o : c) 
			if(!contains(o))
				remove(o);
		return true;
	}
	
	public Ob set(int index, Ob element) {
		return containers.set(index, getContainer(element)).getValue();
	}

	public int size() {
		return containers.size();
	}

	public List<Ob> subList(int fromIndex, int toIndex) {
		List<Ob> obList = new ArrayList<Ob>();
		for (int i = fromIndex; i <= toIndex; i++) 
			obList.add(get(i));
		return obList; 
	}

	public Object[] toArray() {
		Object[] o = new Object[containers.size()];
		for (int i = 0; i < o.length; i++) 
			o[i] = get(i);
		return o;
	}
	
	@Deprecated
	public <T> T[] toArray(T[] a) {
		//return containers.toArray(a);
		throw new UnsupportedOperationException("toArray(T[] a) not implemented");
	}

	public String toString() {
		return containers.toString();
	}

	public void trimToSize() {
		containers.trimToSize();
	}

	
}
