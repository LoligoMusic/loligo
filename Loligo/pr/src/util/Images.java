package util;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pr.RootClass;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PImage;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Images {
	
	
	static public PGraphics createGraphics(int w, int h) {
		return Images.createGraphics(w, h, "SansSerif");
	}
	

	static public PGraphics createGraphics(int w, int h, String font) {
		
		PGraphics pg = RootClass.mainProc().createGraphics(w, h, PConstants.JAVA2D);
		pg.smooth();
		pg.beginDraw();
		pg.textFont(RootClass.mainProc().createFont(font, 12));
		pg.endDraw();
		return pg;
	}
	
	
	static public BufferedImage loadBufferedImage(String path) {
		BufferedImage img = null;
		try {
			InputStream is = Utils.class.getResourceAsStream(path);
			
			img = ImageIO.read(is);
		} catch (IOException e) {
			e.printStackTrace();
			img = new BufferedImage(0, 0, BufferedImage.TYPE_INT_ARGB);
		}
		return img;
	}
	
	
	static public PImage loadPImage(String path, int fill) {
		var p = loadPImage(path);
		return fillAlphaImage(p, fill);
	}
	
	
	static public PImage loadPImage(String path) {
		BufferedImage img = loadBufferedImage(path);
		return bufferedImageToPImage(img);
	}
	
	
	static public PImage bufferedImageToPImage(BufferedImage img) {
		
		return bufferedImageToPImage(RootClass.mainProc(), img);
	}
	
	
	static public PImage bufferedImageToPImage(PApplet proc, BufferedImage img) {
		
		PImage pimg = new PImage(img.getWidth(), img.getHeight(), PConstants.ARGB);
		pimg.parent = proc;
		
		pimg.pixels = img.getRGB(0, 0, pimg.width, pimg.height, pimg.pixels, 0, pimg.width);//getRaster().getPixels(0, 0, 300, 200, pimg.pixels);
		pimg.updatePixels();
		return pimg;
		
	}
	
	
	static public BufferedImage PImageToBufferedImage(PImage img) {
		img.loadPixels();
		return pixelsToBufferedImage(img.width, img.height, img.pixels);
	}
	
	
	static public BufferedImage pixelsToBufferedImage(int width, int height, int[] pixels) {
		
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		int[] newpixels = new int[pixels.length * 4];
		for (int i = 0; i < pixels.length; i++) {
			int i4 = i * 4;
			int c = pixels[i];
			newpixels[i4 + 3] = (c >> 24) & 0xff;
			newpixels[i4 + 0] = (c >> 16) & 0xff;
			newpixels[i4 + 1] = (c >> 8)  & 0xff;
			newpixels[i4 + 2] =  c 		  & 0xff;
		}
		img.getRaster().setPixels(0, 0, width, height, newpixels);
		return img;
	}
	
	
	public static PImage fillAlphaImage(PImage img, int color) {
		
		img.loadPixels();
		int[] pxs = img.pixels;
		for (int i = 0; i < pxs.length; i++) {
			pxs[i] = (pxs[i] & 0xff000000) | (color & 0x00ffffff);
		}
		img.updatePixels();
		return img;
	}
		
	
}
