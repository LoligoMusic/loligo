package util;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.Function;
import javax.swing.JOptionPane;
import pr.RootClass;
import save.SaveJson;


public class ExceptionHandler implements UncaughtExceptionHandler {

	public static final ExceptionHandler handler = new ExceptionHandler();
	
	private static final List<Function<Throwable, Boolean>> listeners = new ArrayList<>();
	
	
	public static void addExceptionListener(Function<Throwable, Boolean> s) {
		listeners.add(s);
	}
	
	public static void set() {
		Thread.setDefaultUncaughtExceptionHandler(handler);
	}
	
	public static boolean onException(Throwable e) {
		return listeners.stream()
						.filter(f -> f.apply(e))
						.findAny().isPresent();
	}
	
	
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		
		if(onException(e)) {
			
		}else {
		
			PrintWriter writer, jsonWriter;
			try {
				String json = SaveJson.savePatch(RootClass.mainPatch());
				String date = new SimpleDateFormat("YY-MM-dd-HH-mm-ss").format(Calendar.getInstance().getTime());
				jsonWriter = new PrintWriter("loligo_error_backup_" + date, "UTF-8");
				jsonWriter.write(json);
				jsonWriter.close();
				
				writer = new PrintWriter("loligo_error_" + date + ".log", "UTF-8");
				
				e.printStackTrace(writer);
				writer.close();
				
				e.printStackTrace();
				
			} catch (FileNotFoundException e1) {
				
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			JOptionPane.showConfirmDialog(
							null, 
							"Loligo crashed and will close now. A backup file has been written to the root directory.", 
							"Loligo crashed", 
							JOptionPane.PLAIN_MESSAGE);
					
			System.exit(1);
		}
	}

}
