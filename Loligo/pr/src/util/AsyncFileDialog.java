package util;

import java.io.File;
import java.util.function.Consumer;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import lombok.Getter;
import lombok.Setter;
import pr.DisplayManagerIF;


public class AsyncFileDialog extends AsyncOperation<String, File[]> {

	@Setter
	private String fileName;
	private String dialogTitle, description;
	@Setter
	private File directory;
	@Getter
	private String[] endings;
	@Getter
	private boolean folderOnly, multiSelect, save;
	
	
	public AsyncFileDialog(DisplayManagerIF dm, Consumer<File[]> onReturn) {
		
		super(dm, onReturn);
	}
	
	public void useSaveDialog() {
		
		save = true;
	}
	
	public void setDialogTitle(String title) {
		
		dialogTitle = title;
	}
	
	public void setFileEndings(String... endings) {
		
		this.endings = endings;
	}
	
	public void setFileDescription(String description) {
		
		this.description = description;
	}
	
	public void allowImageFiles() {
		
		setFileEndings(".png", ".jpeg", ".jpg", ".bmp", ".gif");
		setFileDescription("Image files");
	}
	
	public void setMultipleSelection(boolean b) {
		
		multiSelect = b;
	}
	
	private FileFilter buildFilter() {
		
		FileFilter f = endings == null ? null :
			
			new FileFilter() {
			
			@Override
			public String getDescription() {
				
				return description;
			}
			
			@Override
			public boolean accept(File f) {
				
				if(f.isDirectory())
					return true;
				else if(folderOnly)
					return false;
				
				for (String s : endings) 
					if(f.getName().endsWith(s)) 
						return true;
					
				return false;
			}
		};
		
		return f;
	}
	
	
	@Override
	protected File[] function(String arg) {
		
		JFileChooser c = new JFileChooser();
		
		if(dialogTitle != null)
			c.setDialogTitle(dialogTitle);
		
		if(fileName != null) {
			
		}
		
		if(directory != null) {
			c.setSelectedFile(directory);
		}
		
		FileFilter f = buildFilter();
		if(f != null)
			c.setFileFilter(f);
		
		JDialog jf = Utils.newJFrameBasic(dm.getGuiDomain().getPApplet().getWindow());
		
		int i = save ? c.showSaveDialog(jf) : c.showOpenDialog(jf);
		
		jf.dispose();
		
		if(i == JFileChooser.APPROVE_OPTION) {
			
			if(multiSelect)
				return c.getSelectedFiles();
			
			return new File[] {c.getSelectedFile()};
		}
		
		return new File[0];
	}

}
