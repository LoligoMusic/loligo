package util;

import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.function.Function;

import lombok.Setter;
import pr.DisplayManagerIF;

public class AsyncOperation<T, R> {

	final private Runnable timer;
	final private Function<T, R> foo;
	@Setter
	private Consumer<R> onReturn;
	private R result = null;
	boolean finished;
	@Setter
	protected DisplayManagerIF dm;
	
	public AsyncOperation(DisplayManagerIF dm, Consumer<R> onReturn)  {
		
		this(dm, null, onReturn);
	}
	
	public AsyncOperation(DisplayManagerIF dm, Function<T, R> foo, Consumer<R> onReturn) {
		
		this.dm = dm;
		this.foo = foo;
		this.onReturn = onReturn;
		
		if(dm == null) {
			
			timer = new IndependentTimer();
		}else {
			
			ConditionWatcher cw = new ConditionWatcher(dm, AsyncOperation.this::checkFinished, () -> {});
			timer = cw::run;
		}
		
	}
	
	
	public void runAsync() {
		
		runAsync(null);
	}
	
	
	public void runAsync(final T arg) {
		
		new Thread( () -> doRun(arg)).start();
		
		timer.run();
	}
	
	public void run() {
		
		run(null);
	}
	
	private void run(final T arg) {
		
		doRun(arg);
		checkFinished();
	}
	
	private void doRun(final T arg) {
		try {
			result = function(arg);
		} finally {
			finished = true;
		}
	}
	
	protected R function(T arg) {
		
		if(foo == null)
			throw new Error("Provide a java.lang.Function in the constructor or override method function.");
		
		return foo.apply(arg);
	}
	
	public boolean checkFinished() {
		
		if(finished) 
			onReturn.accept(result);
		
		return finished;
	}
	
	
	class IndependentTimer implements Runnable{
		
		private Timer timer;
		private long delay = 10, period = 50;
		
		@Override
		public void run() {
			
			timer = new Timer();
			
			timer.schedule(
				
				new TimerTask() {
					
					@Override
					public void run() {
						
						if(checkFinished())
							timer.cancel();
					}
				}
				
				, delay, period);
		}

	}
	
	
}
