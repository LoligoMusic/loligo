package util;

import static java.lang.Integer.parseInt;
import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static org.python.google.common.primitives.Ints.constrainToRange;

import java.util.function.IntUnaryOperator;

public final class Colors {
	
	private Colors() {}

	static public final int 
		WHITE 		= 0xffffffff, 
		BLACK 		= 0xff000000, 
		TRANSPARENT = 0x00ffffff, 
		RED 		= 0xffff0000, 
		BLUE 		= 0xff0000ff, 
		GREEN 		= 0xff00ff00;
	
	
	static public int rgbaNorm(float r, float g, float b, float a) {
		return rgba((int)(r * 255), (int)(g * 255), (int)(b * 255), (int)(a * 255));
	}
	
	static public int rgba(int r, int g, int b, int a) {
		return a << 24 | r << 16 | g << 8 | b;
	}
	
	
	static public int grey(int grey, int alpha) {
		return rgba(grey, grey, grey, alpha);
	}
	
	
	static public int grey(int grey) {
		
		return rgba(grey, grey, grey, 255);
	}
	
	static public int grey(float grey) {
		int g = (int) (grey * 255);
		return grey(g);
	}
	
	static public float brightness(int rgb) {
		return (
			0.2126f * red(rgb) + 
			0.7152f * green(rgb) + 
			0.0722f * blue(rgb)
		) / 255f;
	}
	
	public static int rgb(int r, int g, int b) {
		
		return rgba(r, g ,b, 255);
	}
	
	public static int setAlpha(int rgb, float alpha) {
		int a = (int)(255f * alpha);
		return (rgb & 0x00ffffff) | (a << 24);
	}

	
	public static int hex(String hexString) throws NumberFormatException{
		hexString = hexString.trim();
		int s = hexString.startsWith("#") ? 1 : 
				hexString.startsWith("0x")? 2 :
				hexString.startsWith("0X")? 2 : 0;
		
		String h = hexString.substring(s);
		int len = h.length();
		
		int i = switch(len) {
		case 1, 2 -> grey(parseInt(h, 16));
		case 6 	  -> rgb (ff(h, 0), ff(h, 2), ff(h, 4));
		case 8 	  -> rgba(ff(h, 2), ff(h, 4), ff(h, 6), ff(h, 0));
		default   -> throw new NumberFormatException(hexString);
		};
		
		return i;
	}
	
	private static int ff(String hex, int idx) {
		String ff = hex.substring(idx, idx + 2);
		return Integer.parseInt(ff, 16);
	}

	
	public static int multiply(int c1, int c2) {
		int a = ( alpha(c1) * alpha(c2) ) / 255;
		int r = ( red  (c1) * red  (c2) ) / 255;
		int g = ( green(c1) * green(c2) ) / 255;
		int b = ( blue (c1) * blue (c2) ) / 255;
		return rgba(r, g, b, a);
	}
	
	public static int difference(int c1, int c2) {
		int a = abs( alpha(c1) - alpha(c2) );
		int r = abs( red  (c1) - red  (c2) );
		int g = abs( green(c1) - green(c2) );
		int b = abs( blue (c1) - blue (c2) );
		return rgba(r, g, b, a);
	}
	

	static public int complementary(int c) {
		int a = alpha(c), r = red(c), g = green(c), b = blue(c);
		int minRGB = min(r,min(g,b));
		int maxRGB = max(r,max(g,b));
		int p = minRGB + maxRGB;
		return rgba(p - r, p - g, p - b, a);
	}
	
	
	static public int lerp(int c1, int c2, float f) {
		int a1 = alpha(c1), r1 = red(c1), g1 = green(c1), b1 = blue(c1);
		int a2 = alpha(c2), r2 = red(c2), g2 = green(c2), b2 = blue(c2);
		a1 += (a2 - a1) * f;
		r1 += (r2 - r1) * f;
		g1 += (g2 - g1) * f; 
		b1 += (b2 - b1) * f;
		return rgba(r1, g1, b1, a1);
	}
	
	
	static public int scale(int c, float f) {
		IntUnaryOperator foo = e -> (int)(e * f);
		return functionRgb(c, foo);
	}
	
	
	static public int add(int c, int s) {
		IntUnaryOperator foo = e -> (int)(e + s);
		return functionRgb(c, foo);
	}
	
	
	static public int functionRgb(int c, IntUnaryOperator foo) {
		int r = foo.applyAsInt(red  (c));
		int g = foo.applyAsInt(green(c));
		int b = foo.applyAsInt(blue (c));
		r = constrainToRange(r, 0, 255);
		g = constrainToRange(g, 0, 255);
		b = constrainToRange(b, 0, 255);
		return rgba(r, g, b, alpha(c));
	}
	
	
	static public int functionRgba(int c, IntUnaryOperator foo) {
		int r = foo.applyAsInt(red  (c));
		int g = foo.applyAsInt(green(c));
		int b = foo.applyAsInt(blue (c));
		int a = foo.applyAsInt(alpha(c));
		r = constrainToRange(r, 0, 255);
		g = constrainToRange(g, 0, 255);
		b = constrainToRange(b, 0, 255);
		a = constrainToRange(a, 0, 255);
		return rgba(r, g, b, a);
	}
	
	
	static public int alpha(int c) {
		return shiftBits(c, 24);
	}
	
	static public int red(int c) {
		return shiftBits(c, 16);
	}
	
	static public int green(int c) {
		return shiftBits(c, 8);
	}
	
	static public int blue(int c) {
		return shiftBits(c, 0);
	}
	
	static public float alphaf(int c) {
		return alpha(c) / 255f;
	}
	
	static public float redf(int c) {
		return red(c) / 255f;
	}
	
	static public float greenf(int c) {
		return green(c) / 255f;
	}
	
	static public float bluef(int c) {
		return blue(c) / 255f;
	}

	private static int shiftBits(int c, int pos) {
		return ((c >> pos) & 0xff);
	}
	
	public static String toHexString(int c) {
		return "0x" + String.format("%1$08X", c);
	}
	
}
