package util.statemachine;

public interface StateMachineListener {

	public void onStateMachineEvent(StateMachineEvent e, StateMachine.State state, String action);
}
