package util.statemachine;

import static constants.DisplayFlag.RECT;
import static constants.InputEvent.DOUBLECLICK;
import static constants.InputEvent.DRAG_STOP;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import constants.InputEvent;
import gui.DisplayObjects;
import lombok.Getter;
import pr.DisplayObject;
import pr.userinput.UserInput;
import util.statemachine.StateMachine.Action;
import util.statemachine.StateMachine.State;


public class StateMachineG extends DisplayObject {
	
	@Getter
	final StateMachineAgent agent;
	@Getter
	final List<StateG> stateGuis = new ArrayList<>();
	@Getter
	final List<ActionG> actionGuis = new ArrayList<>();
	final ActionPicker picker = new ActionPicker(this);
	private final EntryG entry;
	private final DisplayObject stateContainer;
	private Consumer<StateMachineG> onEdit = sg -> {};
	
	
	public StateMachineG(StateMachineAgent agent) {
		this.agent = agent;
		setFlag(RECT, true);
		//setFlag(DisplayFlag.DROPABLE, true);
		setColor(0xff000000);
		
		stateContainer = DisplayObjects.createContainer();
		addChild(stateContainer);
		
		entry = new EntryG(this);
	}
	
	
	public void reset() {
		stateGuis.forEach(DisplayObjects::removeFromDisplay);
		actionGuis.forEach(DisplayObjects::removeFromDisplay);
		stateGuis.clear();
		actionGuis.clear();
		DisplayObjects.removeFromDisplay(entry);
	}
	
	
	public StateG addStateG(State s) {
		return stateGuis.stream()
						.filter(sg -> sg.state == s)
						.findAny()
						.orElseGet(() -> newStateG(s));
	}
	
	
	private StateG newStateG(State s) {
		var sg = new StateG(this, s);
		stateGuis.add(sg);
		stateContainer.addChild(sg);
		if(s == agent.getStateMachine().getEntryState()) 
			entry.setEntryState(sg);
		return sg;
	}
	
	
	public ActionG addActionG(State s, Action a) {
		return actionGuis.stream()
						 .filter(ag -> ag.action == a)
						 .findAny()
						 .orElseGet(() -> newActionG(s, a));
	}
	
	
	private ActionG newActionG(State s, Action a) {
		var ag = new ActionG(this, s, a);
		actionGuis.add(ag);
		addChild(ag);
		edited();
		validateActions(s);
		return ag;
	}
	
	
	public void init() {
		reset();
		var ss = agent.getStateMachine().getStates();
		ss.stream().forEach(this::addStateG);
		for(State s : ss) 
			for(Action a : s.getActions()) 
				addActionG(s, a);
	}
	
	
	public StateG createState(float x, float y) {
		var s = new State("State");
		agent.getStateMachine().addState(s);
		var sg = addStateG(s);
		sg.setPos(x, y);
		edited();
		return sg;
	}
	
	
	void edited() {
		onEdit.accept(this);
	}
	
	
	public void onEdit(Consumer<StateMachineG> onEdit) {
		this.onEdit = onEdit;
	}

	
	@Override
	public void addedToDisplay() {
		DisplayObjects.addSubscriber(this);
	}
	
	
	@Override
	public void removedFromDisplay() {
		DisplayObjects.removeSubscriber(this);
	}
	
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		if(e == DRAG_STOP)
			edited();
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == DOUBLECLICK) {
			float x = input.getMouseX() - getX();
			float y = input.getMouseY() - getY();
			createState(x, y);
		}
	}

	
	public StateG getStateG(State s) {
		return stateGuis.stream()
					 	.filter(sg -> sg.state == s)
					 	.findAny()
					 	.orElse(null);
	}
	
	
	public ActionG getActionG(Action a) {
		return actionGuis.stream()
					 	 .filter(ag -> ag.action == a)
					 	 .findAny()
					 	 .orElse(null);
	}
	
	public void resize(int w, int h) {
		
	}
	
	
	public void validate() {
		agent.getStateMachine().getStates()
			 .forEach(this::validateActions);
	}
	
	public void validateActions(State s) {
		var actions = s.getActions();
		for(int i = 0; i < actions.size(); i++) {
			var a1 = actions.get(i);
			for (int j = i + 1; j < actions.size(); j++) {
				var a2 = actions.get(j);
				var ag = getActionG(a2);
				if(a1.getName().equals(a2.getName())) {
					ag.setValid(false, null);
				}else {
					ag.setValid(true, null);
				}
			}
		}
	}
}

