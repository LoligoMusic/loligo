package util.statemachine;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.Position;
import gui.SnapBehaviour;
import gui.selection.GuiType;
import lombok.var;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;


public class EntryG extends DisplayObject{

	private final StateMachineG stateMachineG;
	private StateG stateG;
	
	
	public EntryG(StateMachineG stateMachineG) {
		
		this.stateMachineG = stateMachineG;
		setFlag(DisplayFlag.DRAGABLE, true);
		setGuiType(GuiType.DRAGABLE_FREE);
		
		setFlag(DisplayFlag.ROUND, true);
		setWH(20, 20);
		
		setSelectionObject(g -> {});
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		
		if(e == InputEvent.DRAG_START) {
			var s = new SnapBehaviour.SnapToObjects<StateG>(this, stateMachineG.stateGuis) {
				@Override
				public void setPosition(float x, float y) {
					target.setGlobal(x, y - 40);
				}
			};
			s.setAutoRemove(true);
			s.addDropConsumer(this::changeEntryState);
			s.addEmptyDropConsumer((x, y) -> setEntryState(stateG));
			s.setSnapDistance(stateG.getWidth() / 2);
			//s.addOnSnapConsumer(sg -> this.setGlobal(sg.getX(), sg.getY()-40));
			input.addInputEventListener(s);
		}
		
		
		/*else
		if(e == InputEvent.DRAG_DROPPED) {
			
			if(target != null && target == input.getDropTarget()) {
				changeEntryState(target);
			}else {
				setEntryState(stateG);
			}
			target = null;
		}else
		if(e == InputEvent.DRAG_OVER) {
			
			val d = input.getDropTarget();
			stateMachineG.getStateGuis().stream()
				.filter(g -> g == d)
				.findAny()
				.ifPresent(sg -> {
					target = sg;
					snapState(target);
				});
			
		}else
		if(e == InputEvent.DRAG_OVER_STOP) {
			target = null;
			
		}else
		if(e == InputEvent.DRAG_STOP) {
			target = null;
			setEntryState(stateG);
		}
		*/
	}
	
	
	@Override
	public void render(PGraphics g) {
		float x = getX(),
			  y = getY() + 10;
		int c = checkFlag(DisplayFlag.mouseOver) ? 0xffffffff : 0xff999999;
		g.fill(c);
		g.text("Start", x - 13, y - 15 - 12);
		g.triangle(x, y, x - 8, y - 15, x + 8, y - 15);
	}
	
	
	private void changeEntryState(StateG sg) {
		stateMachineG.agent.getStateMachine().setEntryState(sg.state);
		setEntryState(sg);
	}
	
	private void snapState(StateG sg) {
		setPosObject(new Position.PositionOffset(stateG, 0, -10-sg.getHeight() / 2));
	}
	
	public void setEntryState(StateG sg) {
		stateG = sg;
		if(sg != null) {
			snapState(sg);
			stateMachineG.addChild(this);
		}
	}
	
}
