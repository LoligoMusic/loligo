package util.statemachine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.val;

public class StateMachine {
	
	@Getter
	private final List<State> states = new ArrayList<>();
	private final List<String> actionNames = new ArrayList<>();
	@Getter
	private State entryState;
	
	private final List<StateMachineListener> listeners = new ArrayList<>();
	
	
	public void addListener(StateMachineListener n) {
		listeners.add(n);
	}
	
	public void removeListener(StateMachineListener n) {
		listeners.remove(n);
	}
	
	public void dispatchEvent(StateMachineEvent e, State state, String action) {
		for (val n : listeners) {
			n.onStateMachineEvent(e, state, action);
		}
	}
	
	public void addState(State s) {
		
		if(states.isEmpty()) {
			setEntryState(s);
		}
		
		states.add(s);
		dispatchEvent(StateMachineEvent.STATE_ADDED, s, null);
	}
	
	public void removeState(State s) {
		states.remove(s);
		
		boolean e = !states.isEmpty();
		
		if(s == entryState && e) {
			setEntryState(states.get(0));
		}
		dispatchEvent(StateMachineEvent.STATE_REMOVED, s, null);
	}
	
	
	public void addActionName(Action a) {
		String name = a.getName();
		if(!actionNames.contains(name)) {
			actionNames.add(name);
			dispatchEvent(StateMachineEvent.ACTION_ADDED, null, a.getName());
		}
	}
	
	public void removeAction(State s, Action a) {
		s.removeAction(a);
		removeActionName(a.getName());
	}
	
	public void removeActionName(String n) {
		
		boolean b = getActions().stream()
								.map(Action::getName)
								.noneMatch(n::equals);
		if(b) {
			actionNames.remove(n);
			dispatchEvent(StateMachineEvent.ACTION_REMOVED, null, n);
		}
	}
	
	
	public void setEntryState(State s) {
		entryState = s;
	}
	
	
	public List<Action> getActions() {
		return 
			states.stream().flatMap(s -> s.actions.stream()) 
				   		   .collect(Collectors.toList());
	}
	
	
	@RequiredArgsConstructor
	public static class State {
		
		@Getter@Setter
		private String name;
		@Getter
		private final List<Action> actions = new ArrayList<>();
		
		public State(String name) {
			this.name = name;
		}
		
		public void addAction(Action a) {
			actions.add(a);
		}
		
		public void removeAction(Action a) {
			actions.remove(a);
		}
		
		public boolean contains(Action a) {
			return actions.contains(a);
		}

		@Override
		public String toString() {
			return "State(" + name + ")";
		}
	}
	
	
	@AllArgsConstructor
	public static class Action {
		@Getter
		private final State state;
		@Getter@Setter
		private String name;
		
		@Override
		public String toString() {
			return "Action(" + name + ")";
		}
	}
	
	
	
	
	
	
}
