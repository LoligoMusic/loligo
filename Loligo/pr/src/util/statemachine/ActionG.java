package util.statemachine;

import gui.Arrow;
import gui.DisplayObjects;
import gui.Position;
import gui.text.TextInputField;
import lombok.Getter;
import lombok.val;
import lombok.var;
import pr.DisplayObject;
import util.Colors;
import util.statemachine.StateMachine.Action;
import util.statemachine.StateMachine.State;

public class ActionG extends Arrow {
	/**
	 * 
	 */
	
	private static final int COLOR_DEFAULT = Colors.WHITE, COLOR_INVALID = Colors.grey(100);
	
	private final StateMachineG stateMachineG;
	@Getter
	private StateG state1, state2;
	@Getter
	final DisplayObject handle1, handle2;
	@Getter
	final Action action;
	final TextInputField text;
	
	
	public ActionG(StateMachineG stateMachineG, State s1, Action action) {
		this.stateMachineG = stateMachineG;
		this.action = action;
		this.state1 = this.stateMachineG.getStateG(s1);
		this.state2 = this.stateMachineG.getStateG(action.getState());
		setHeadOffset(-5);
		
		setPos1(handle1 = state1.createActionHandle(this));
		setPos2(handle2 = state2.createActionHandle(this));

		addChild(handle1);
		addChild(handle2);
		
		text = new TextInputField(this::rename, action::getName);
		Position p = new Position.Interpolate(handle1, handle2, .5f);
		Position p2 = new Position.PositionOffset(p, -text.getWidth() / 2, -text.getHeight() / 2);
		text.setPosObject(p2);
		text.update();
		addChild(text);
		setColor(COLOR_DEFAULT);
	}
	
	
	private void rename(String n) {
		var an = action.getName();
		if(n.equals(an))
			return;
		
		action.setName(n);
		val sm = this.stateMachineG.agent.getStateMachine();
		sm.removeActionName(an);
		sm.addActionName(action);
		stateMachineG.edited();
		stateMachineG.validateActions(state1.state);
	}
	
	
	public void remove() {
		this.stateMachineG.agent.getStateMachine().removeAction(state1.state, action);
		DisplayObjects.removeFromDisplay(this);
		DisplayObjects.removeFromDisplay(handle1);
		DisplayObjects.removeFromDisplay(handle2);
		stateMachineG.validateActions(state1.state);
	}
	
	public void setValid(boolean valid, String message) {
		setColor(valid ? COLOR_DEFAULT : COLOR_INVALID);
		
	}
	
	
}