package util.statemachine;

import static com.jogamp.newt.event.KeyEvent.VK_BACK_SPACE;
import static com.jogamp.newt.event.KeyEvent.VK_DELETE;
import static constants.DisplayFlag.CLICKABLE;
import static constants.DisplayFlag.DRAGABLE;
import static constants.DisplayFlag.DROPABLE;
import static constants.InputEvent.CLICK_RIGHT;
import static constants.InputEvent.DOUBLECLICK;
import static constants.InputEvent.DRAG_START;
import static constants.InputEvent.KEY;
import static gui.selection.GuiType.DRAGABLE_FREE;
import static gui.text.TextBlock.AlignMode.CENTER;
import static java.util.stream.Collectors.toList;

import java.util.function.Consumer;
import java.util.stream.Stream;

import com.google.common.collect.Streams;

import constants.DisplayFlag;
import constants.InputEvent;
import constants.SingleTask;
import gui.Arrow;
import gui.DisplayObjects;
import gui.Position;
import gui.text.TextInputField;
import lombok.Getter;
import lombok.var;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import util.statemachine.StateMachine.State;

public class StateG extends DisplayObject {
	/**
	 * 
	 */
	public final static int WIDTH = 60;
	
	private final StateMachineG stateMachineG;
	@Getter
	final State state;
	private final TextInputField text;
	private final Ring ring;
	
	
	public StateG(StateMachineG stateMachineG, State state) {
		
		this.stateMachineG = stateMachineG;
		this.state = state;
		var p = new Position.ParentBounds(this.stateMachineG);
		setPosObject(p);
		setGuiType(DRAGABLE_FREE);
		setFlag(DROPABLE, true);
		
		setWH(WIDTH, WIDTH);
		
		Consumer<String> sn = n -> {
			state.setName(n);
			stateMachineG.edited();
		};
		
		text = new TextInputField(sn, state::getName) {
			@Override
			public void mouseEventThis(InputEvent e, UserInput input) {
				super.mouseEventThis(e, input);
				if(e == DRAG_START && !active) {
					input.injectDragTarget(StateG.this);
				}
			}
			
			@Override
			public void active(boolean a) {
				super.active(a);
				setFlag(CLICKABLE, a);
				setFlag(DRAGABLE, a);
			}
		};
		text.bgBox = false;
		text.setPos(-text.getWidth() / 2, -text.getHeight() / 2);
		text.setFlag(DisplayFlag.fixedToParent, true);
		text.setFlag(CLICKABLE, false);
		text.setFlag(DRAGABLE, false);
		text.setAlignMode(CENTER);
		text.update();
		addChild(text);
		
		ring = new Ring();
		addChild(ring);
	}
	
	DisplayObject createActionHandle(Arrow ag) {
		
		var d = new DisplayObject() {
			@Override 
			public void mouseEventThis(InputEvent e, UserInput input) {
				int c = input.getLastKeyCode();
				boolean k = e == KEY && (c == VK_BACK_SPACE || c == VK_DELETE);
				boolean m = e == CLICK_RIGHT;
				if(k || m) {
					ag.remove();
				}
			}
		};
		d.setColor(0xff777777);
		d.setWH(9, 9);
		d.setFlag(DRAGABLE, true);
		d.setGuiType(DRAGABLE_FREE);
		float w = (getWidth() - d.getWidth()) * .5f;
		var p = new Position.Circular(this, w);
		d.setPosObject(p);
		return d;
	}
	
	
	@Override
	public void render(PGraphics g) {
		int c = this.stateMachineG.agent.getCurrentState() == state ? 0xff990505 : 0xff999999;
		g.fill(c);
		g.circle(getX(), getY(), getWidth());
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(e == DOUBLECLICK) {
			SingleTask.run(input.getDomain(), () -> text.active(true));
		}else
		if(e == KEY && input.getLastKeyCode() == VK_DELETE) 
			removeState();
		
	}
	
	public void removeState() {
		Stream<ActionG> 
			s1 = state.getActions().stream().map(this.stateMachineG::getActionG),
			s2 = this.stateMachineG.actionGuis.stream().filter(ag -> ag.action.getState() == state);		
		Streams.concat(s1, s2).collect(toList()).forEach(ActionG::remove);
		
		this.stateMachineG.agent.getStateMachine().removeState(state);
		this.stateMachineG.stateGuis.remove(this);
		DisplayObjects.removeFromDisplay(this);
	}
	
	void hasPicker(boolean b) {
		ring.hasPicker(b);
	}
	
	public void connectionMode(boolean connecting) {
		ring.setFlag(CLICKABLE, !connecting);
		//text.setFlag(CLICKABLE, !connecting);
	}
	
	
	
	private class Ring extends DisplayObject {
		
		private static final int COLOR_RING = 0x77666666;
		private static final int COLOR_MOUSEOVER = 0x77dddddd;
		private static final int COLOR_PICKER = 0x77ffffff;
		private static final float ringWidth = 9;
		
		boolean hasPicker;
		
		public Ring() {
			setFlag(DRAGABLE, true);
			setFlag(CLICKABLE, true);
			setGuiType(DRAGABLE_FREE);
			
		}
		
		void hasPicker(boolean b) {
			hasPicker = b;
		}
		
		@Override
		public void render(PGraphics g) {
			g.noFill();
			g.strokeWeight(ringWidth);
			int c = hasPicker 						 ? COLOR_PICKER : 
					checkFlag(DisplayFlag.mouseOver) ? COLOR_MOUSEOVER : 
													   COLOR_RING ;
			g.stroke(c);
			g.circle(StateG.this.getX(), StateG.this.getY(), StateG.this.getWidth() - ringWidth);
			g.noStroke();
		}
		
		@Override
		public boolean hitTest(float mx, float my) {
			float dx = StateG.this.getX() - mx, dy = StateG.this.getY() - my;
			float d = (float) Math.sqrt(dx * dx + dy * dy);
			float r = StateG.this.getWidth() / 2;
			return d < r && d >= r - ringWidth;
		}
		
		@Override
		public void mouseEventThis(InputEvent e, UserInput input) {
			if(e == DRAG_START) 
				stateMachineG.picker.start(StateG.this, input);
		}
	}


}