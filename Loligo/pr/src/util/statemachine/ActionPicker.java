package util.statemachine;

import static gui.Positionables.position;
import static gui.selection.GuiType.DRAGABLE_FREE;

import constants.InputEvent;
import gui.Arrow;
import gui.Positionables;
import gui.SnapBehaviour;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import util.statemachine.StateMachine.Action;

class ActionPicker extends Arrow {
	
	/**
	 * 
	 */
	private final StateMachineG stateMachineG;
	StateG stateG, stateG2;
	DisplayObject handle1;
	final SnapBehaviour.SnapToRing snapper;
	
	public ActionPicker(StateMachineG stateMachineG) {
		this.stateMachineG = stateMachineG;
		setGuiType(DRAGABLE_FREE);
		setSelectionObject(g -> {});
		setPos2(this);
		snapper = new SnapBehaviour.SnapToRing(this);
		snapper.setAutoRemove(true);
		snapper.addDropConsumer(p -> connect((StateG) p));
		snapper.addEmptyDropConsumer(this::createState);
		snapper.addOnSnapConsumer(p -> snap((StateG) p));
		snapper.addOnUnsnapConsumer(p -> unsnap((StateG) p));
	}
	
	@Override
	public void render(PGraphics g) {
		boolean b = Positionables.withinRange(getPos1(), getPos2(), 20);
		setHeadVisible(!b);
		super.render(g);
	}
	
	public void start(StateG s, UserInput input) {
		this.stateG = s;
		setPos1(handle1 = stateG.createActionHandle(this));
		
		var m = stateG.getDm().getInput().mousePos;
		var p = m.copyPos().subtract(stateG);
		handle1.setPos(p);
		addChild(handle1);
		
		this.stateMachineG.picker.setPos(m);
		this.stateMachineG.addChild(this.stateMachineG.picker);
		input.injectDragTarget(this.stateMachineG.picker);
		
		stateMachineG.stateGuis.stream()
							   .filter(g -> g != stateG)
							   .forEach(g -> g.connectionMode(true));
	}
	
	void connect(StateG s) {
		remove();
		
		var a = new Action(s.getState(), "");
		var s1 = stateG.getState();
		s1.addAction(a);
		
		var ag = this.stateMachineG.addActionG(s1, a);
		
		var p1 = handle1.copyPos().subtract(stateG);
		ag.handle1.setPos(p1);
		ag.text.active(true);
		
		var m = stateG.getDm().getInput().mousePos.copyPos();
		
		ag.handle2.setPos(m.subtract(s));//setPos(m).subtract(s);
		
		this.stateMachineG.agent.getStateMachine().addActionName(a);
	}
	
	void snap(StateG s) {
		stateG2 = s;
		s.hasPicker(true);
	}
	
	void unsnap(StateG s) {
		stateG2 = null;
		if(s != null)
			s.hasPicker(false);
	}
	
	@Override
	public void remove() {
		this.stateMachineG.removeChild(this);
		removeChild(handle1);
		unsnap(stateG2);
		
		stateMachineG.stateGuis.forEach(g -> g.connectionMode(false));
	}
	
	private void createState(float mx, float my) {
		
		var p = position(mx, my)
				.subtract(stateG)
				.divide(distance(mx, my))
				.multiply(StateG.WIDTH / 2);
		
		var k = position(mx, my)
				.subtract(stateMachineG)
				.add(p);
		
		var s = stateMachineG.createState(k.getX(), k.getY());
		connect(s);
		remove();
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		var drop = input.getDropTarget();
		
		switch(e) {
		case DRAG_STOP, DRAG_DROPPED -> remove();
		case DRAG_OVER -> {
			if(drop != stateG && drop != stateG2 && stateMachineG.stateGuis.contains(drop)) {
				snapper.setRing((StateG) drop);
				snapper.setRadius(drop.getWidth() / 2);
				input.addInputEventListener(snapper);
			}
		}
		default -> {}
		}
	}
}