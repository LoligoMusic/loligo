package util.statemachine;

public enum StateMachineEvent {
	
	STATE_ADDED,
	STATE_REMOVED,
	ACTION_ADDED,
	ACTION_REMOVED;
}
