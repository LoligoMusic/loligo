package util.statemachine;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import util.statemachine.StateMachine.Action;
import util.statemachine.StateMachine.State;


public class StateMachineAgent implements StateMachineListener{

	@Getter
	private final StateMachine stateMachine;
	@Getter@Setter
	private StateMachine.State currentState;
	
	
	public StateMachineAgent(StateMachine stateMachine) {
		this.stateMachine = stateMachine;
		toEntryState();
	}
	
	
	public State doAction(String n) {
		for (Action a : currentState.getActions()) {
			if(a.getName().equals(n)) {
				var s = a.getState();
				setCurrentState(s);
				return s;
			}
		}
		return currentState;
	}
	
	public State doAction(Action a) {
		if(currentState.contains(a)) {
			setCurrentState(a.getState());
		}
		return currentState;
	}
	
	public void toEntryState() {
		setCurrentState(stateMachine.getEntryState());
	}

	@Override
	public void onStateMachineEvent(StateMachineEvent e, State state, String action) {
		
		boolean b = (e == StateMachineEvent.STATE_REMOVED && state == currentState) ||
				 	(e == StateMachineEvent.STATE_ADDED && currentState == null);
		
		if(b) 
			toEntryState();
		
	}
	
}
