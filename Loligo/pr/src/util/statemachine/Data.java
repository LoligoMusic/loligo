package util.statemachine;

import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.NoArgsConstructor;
import lombok.ToString;
import util.statemachine.StateMachine.Action;
import util.statemachine.StateMachine.State;


public class Data {

	public static Data buildAll(StateMachine st) {
		Data dat = new Data();
		dat.build(st);
		return dat;
	}
	
	public static Data buildAll(StateMachine st, StateMachineG gui) {
		Data dat = buildAll(st);
		dat.buildGui(gui);
		return dat;
	}
	
	public transient StateMachine stateMachine;

	public StateData[] states;
	public StateData entryPoint;
	public ActionData[] actions;
	
	
	
	
	public void build(StateMachine stateMachine) {
		
		var ss = stateMachine.getStates();
		states = ss.stream().map(StateData::new)
			  		   		.toArray(StateData[]::new);
		
		entryPoint = findStateData(stateMachine.getEntryState());
		
		var sds = new ArrayList<ActionData>();
		
		for(StateData sd1 : states) {
			var acs = sd1.state.getActions();
			var ads = new ActionData[acs.size()];
			for(int i = 0; i < acs.size(); i++) {
				Action a = acs.get(i);
				StateData sd2 = findStateData(a.getState());
				var ad = new ActionData(sd1, sd2, a);
				ads[i] = ad;
				//sd1.actions = ads;
				sds.add(ad);
			}
		}
		actions = sds.toArray(new ActionData[sds.size()]);
	}
	
	
	public void buildGui(StateMachineG gui) {
		
		for (StateData sd : states) {
			StateG sg = gui.getStateG(sd.state);
			sd.pos = new float[] {
				sg.getX() - gui.getX(),
				sg.getY() - gui.getY()
			};
		}
		
		for (ActionData ad : actions) {
			ActionG ag = gui.getActionG(ad.action);
			var h1 = ag.getHandle1();
			var h2 = ag.getHandle2();
			var s1 = ag.getState1();
			var s2 = ag.getState2();
			ad.handles = new float[] {
				h1.getX() - s1.getX(),
				h1.getY() - s1.getY(),
				h2.getX() - s2.getX(),
				h2.getY() - s2.getY(),
			};
		}
	}
	
	public void restore(StateMachine stateMachine) {
		
		for (StateData sd : states) {
			State s = new State(sd.name);
			sd.state = s;
			stateMachine.addState(s);
		}
		
		stateMachine.setEntryState(entryPoint.state);
		
		for (ActionData ad : actions) {
			Action a = new Action(ad.to.state, ad.name);
			ad.action = a;
			ad.from.state.addAction(a);
			stateMachine.addActionName(a);
		}
	}
	
	public void restoreGui(StateMachineG gui) {
		
		for (StateData sd : states) {
			StateG sg = gui.addStateG(sd.state);
			float[] p = sd.pos;
			sg.setPos(p[0], p[1]);
		}
		
		for (ActionData ad : actions) {
			ActionG a = gui.addActionG(ad.from.state, ad.action);
			float[] p = ad.handles;
			a.getHandle1().setPos(p[0], p[1]);
			a.getHandle2().setPos(p[2], p[3]);
		}
	}
	
	
	private StateData findStateData(State s) {
		for (StateData sd : states)
			if(sd.state == s) 
				return sd;
		return null;
	}
	
	
	@ToString(of = "name")
	@NoArgsConstructor
	@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
	public static class StateData {
		transient State state;
		public String name;
		//public ActionData[] actions;
		public float[] pos;
		
		public StateData(State s) {
			state = s;
			this.name = s.getName();
		}
	}
	

	@ToString(of = "name")
	@NoArgsConstructor
	public static class ActionData {
		public transient Action action;
		public String name;
		public StateData from, to;
		public float[] handles;
		
		public ActionData(StateData from, StateData to, Action a) {
			this.action = a;
			this.name = a.getName();
			this.from = from;
			this.to = to;
		}
		
	}
}
