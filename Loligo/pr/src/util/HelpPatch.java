package util;

import java.io.InputStream;

import lombok.var;
import module.Module;
import module.loading.NodeInfo;
import patch.LoligoPatch;
import save.SaveJson;

public final class HelpPatch {
	
	private HelpPatch() {}
	
	
	public static void openHelpPatch(LoligoPatch p) {
		var mods = p.getDisplayManager().getInput().selection.getSelectedModules();
		if(!mods.isEmpty()) {
			openHelpPatch(mods.get(0));
		}
	}
	
	public static void openHelpPatch(Module module) {
		
		String n = getHelpPatchName(module);
		if(n == null) {
			System.err.println("No help-patch for \"" + module.getName() + "\"");
			return;
		}
		
		InputStream is = HelpPatch.class.getResourceAsStream(n);
		
		if(is == null) {
			System.err.println("No help-patch for \"" + n + "\"");
		}else {
			SaveJson.loadPatchFromStream(is, null, pa -> pa.setTitle(n));
		}
	}
	
	public static String getHelpPatchName(Module module) {
		
		Class<?> c = module.getClass();
		
		NodeInfo[] ins = c.getAnnotationsByType(NodeInfo.class);
		if(ins.length == 0)
			return null;
		NodeInfo in = ins[0];
		
		String h = in.helpPatch();
		String n = h.length() > 0 ? h : module.getName();
		
		return "/help/" + in.type() + "-" + n + "(Help).lol"; 
	}
	

}
