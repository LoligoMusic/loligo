package util;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import lombok.RequiredArgsConstructor;
import save.AssetRegistry;

public class CustomURLStreamHandler extends URLStreamHandler {
	
	private static final Map<URL, Supplier<InputStream>> urlMap = new HashMap<>();
	
	
	public static void init() {
		
		URL.setURLStreamHandlerFactory(new CustomURLStreamHandler.Factory("about"));
		
		addURL("about://image/missing", () -> AssetRegistry.getImageInputStream("No Image"));
		
	}
	
	public static void addURL(String url, Supplier<InputStream> sup) {
		try {
			URL u = new URL(url);
			addURL(u, sup);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public static void addURL(URL u, Supplier<InputStream> sup) {
		urlMap.put(u, sup);
	}
	
	
	@Override
	protected URLConnection openConnection(URL u) throws IOException {
		
		return new Connection(u);
	}
	
	
	@RequiredArgsConstructor
	public static class Factory implements URLStreamHandlerFactory {

		private final String protocol;
		
		@Override
		public URLStreamHandler createURLStreamHandler(String protocol) {
			if(this.protocol.equals(protocol))
				return new CustomURLStreamHandler();
			return null;
		}
	}
	
	
	public static class Connection extends URLConnection {

		protected Connection(URL url) {
			super(url);
		}

		@Override
		public void connect() throws IOException {
			
		}
		
		@Override
		public InputStream getInputStream() throws IOException {
			
			if(!urlMap.containsKey(url))
				throw new IOException();
			
			return urlMap.get(url).get();
		}
	}

}
