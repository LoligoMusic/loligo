package util;


import java.awt.Desktop;
import java.awt.Window;
import java.awt.Window.Type;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.swing.JDialog;

import patch.ProcWindow;
import processing.core.PConstants;
import processing.core.PFont;


public class Utils {
	
	
	static public float textWidth(char[] chars, int start, int length, int textSize, PFont font) {
		float e = 0;
		int end = start + length;
		for (int i = start; i < end; i++) 
			e += font.width(chars[i]);
		return e * textSize;
	}
	
	
	static public float textWidth(String s, int textSize, PFont font) {
		float e = 0;
		for (int i = 0; i < s.length(); i++) 
			e += font.width(s.charAt(i));
		return e * textSize; 
	}
	
	
	static public float textWidth(char c, int textSize, PFont font) {
		 return font.width(c) * textSize;
	}
	
	
	static public String[] formatedEnumString(Enum<?>[] e) {
		final String[] ss = new String[e.length];
		for (int i = 0; i < ss.length; i++) {
			String s = e[i].toString();
			s = s.replace("_", " ");
			ss[i] = s.substring(0, 1) + s.substring(1).toLowerCase();
		}
		return ss;
	}
	
	static public String[] incrementingNumbers(int length, int start) {
		String[] ss = new String[length];
		for (int i = 0; i < ss.length; i++) 
			ss[i] = String.valueOf(start + i);
		return ss;
	}
	
	public static int ArrayIndexOf(Object[] arr, Object o) {
		for (int i = 0; i < arr.length; i++) 
			if(arr[i] == o)
				return i;
		return -1;
	}
	
	static public Map<String, Object> newVersionMap(int version) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("VERSION", version);
		return map;
	}
	
	static public Path relativePath(String path) {
		
		return relativePath(Paths.get(path));
	}
	
	static public Path relativePath(Path path) {
		
		try {
			File base = Utils.getJarPath();
			
			
			
			if(isSubDirectory(base, path.toFile())) {
				
				Path lo = base.toPath();//java.nio.file.Paths.get(base.toString());
				Path p = lo.relativize(path);
				
				return p;
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return path;
	}
	
	static public Path resolveRelativePath(String relative) {
		
		Path p = getJarPath().toPath();
		return p.resolve(relative);
	}
	
	public static File getJarPath() {
		
		String bs = Utils.	class.getProtectionDomain().getCodeSource().getLocation().getPath();
		try {
			bs = URLDecoder.decode(bs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File f = new File(bs);
		if(!f.isDirectory()) 
			f = f.getParentFile();
		
		return f;
		
	}

	static public boolean isSubDirectory(File base, File child)
		    throws IOException {
		
		    base = base.getCanonicalFile();
		    child = child.getCanonicalFile();

		    File parentFile = child;
		    while (parentFile != null) {
		        if (base.equals(parentFile)) {
		            return true;
		        }
		        parentFile = parentFile.getParentFile();
		    }
		    return false;
		}
	
	public static String addFileEnding(String filename, String ending) {
		
		String e = "." + ending;
		return filename.endsWith(e) ? filename : filename + e; 
	}
	
	public static void iterateFolder(String path, Consumer<File> fileCons) {
		
		List<File> files = new ArrayList<>();
		files.add(new File(path));
		
		int i = 0;
		
		while(i < files.size()) {
			
			File f = files.get(i);
			
			fileCons.accept(f);
			
			if(f.isDirectory()) 
				
				files.addAll(Arrays.asList(f.listFiles()));
			
			i++;
			
			
		}
		
		
		
	}
	
	
	static public void copyStream(InputStream in, OutputStream out) throws IOException {
		
		byte[] buffer = new byte[4096];
		int n;
		while ((n = in.read(buffer)) > 0) {
		    out.write(buffer, 0, n);
		}
		
	}
	
	static public JDialog newJFrameBasic(ProcWindow w) {
		
		JDialog jf = new JDialog((Window) null);
		jf.setLocation(w.getX() + w.getWidth() / 2 - 50, w.getY() + w.getHeight() / 2);
		jf.setVisible(false);
	    //jf.setLocation(100, 100);
	    jf.setAlwaysOnTop(true);
	    jf.setType(Type.UTILITY);
	    jf.setUndecorated(true);
	    return jf;
	}
	
	public static void openWebpage(String urlString) {
	    try {
	        Desktop.getDesktop().browse(new URL(urlString).toURI());
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	
	public static Object getStaticFieldValue(Class<?> clazz, String fieldName) {
		
		Field fi = Arrays.stream(PConstants.class.getDeclaredFields())
						 .filter(f -> Modifier.isStatic(f.getModifiers()))
						 .filter(f -> f.getName().equals(fieldName))
						 .findAny()
						 .get();

		try {
			return fi.get(null);
		} catch (IllegalAccessException | IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		}
	}
}
