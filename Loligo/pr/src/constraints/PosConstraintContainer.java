package constraints;

import java.util.List;

import save.ModuleData;
import save.PathData;
import save.RestoreDataContext;

public interface PosConstraintContainer {
	
	public LoligoPosConstraint getPosConstraint();
	public List<Constrainable> getConstrainables();
	
	public PathData doSerializePath(ModuleData md);
	public void doDeserializePath(PathData pd, RestoreDataContext rdc);
}
