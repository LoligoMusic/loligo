package constraints;

import gui.path.PositionableObject;
import gui.paths.PathEvent;
import pr.DisplayObjectIF;
import save.ConstrainableData;
import save.PathData;
import save.SaveDataContext;

public class ConstrainableObject extends PositionableObject implements Constrainable {

	private FollowerObject followerObject;
	

	@Override
	public FollowerObject getFollowerObject() {
		
		return followerObject;
	}

	@Override
	public void setFollowerObject(FollowerObject followerObject) {
		
		this.followerObject = followerObject;
	}

	@Override
	public void onPathEvent(PathEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updatePosition() {
		
		
	}

	@Override
	public DisplayObjectIF createGUI() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public DisplayObjectIF getGui() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ConstrainableData toConstrainableMemento(SaveDataContext sdc, PathData pd) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void restoreMemento(ConstrainableData cd) {
		// TODO Auto-generated method stub
		
	}

}
