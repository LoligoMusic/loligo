package constraints;

import java.util.ArrayList;
import java.util.List;

import gui.Position;
import gui.Position.PositionOffset;
import gui.paths.Node;
import gui.paths.NullPathObject;
import gui.paths.Path;
import gui.paths.PathEvent;
import gui.paths.PathEventEnum;
import gui.paths.PathEventManager;
import gui.paths.PathMementoFactory;
import gui.paths.PathObject;
import gui.paths.PathTree;
import gui.paths.PathTreeRunner;
import gui.paths.SegmentType;
import gui.paths.PathMementoFactory.ConstraintMemento;
import module.ModuleManager;
import module.PositionableModule;
import module.modules.graphs.NodeSwitchModule;
import pr.EnterFrameListener;
import pr.RootClass;

public class PathTreeConstraint extends LoligoPosConstraint {

	//private final PathTree pathTree;
	private final PathObject entryPoint = new NullPathObject();
	
	private final List<NodeSwitch> switches = new ArrayList<>();
	
	public PathTreeConstraint(PositionableModule m) {
		
		super(new PathTree(), m);
		
	}

	
	public void entryPoint(Path p, double pos) {
		
		entryPoint.setPath(p);
		entryPoint.goToPathPosition(pos);
	}
	
	
	public PathTree getPathTree() {
		
		return (PathTree) getPosConstraint();
	}
	
	
	@Override
	public FollowerObject addFollower(Constrainable target) {
		
		FollowerObject fo = new FollowerObject(this, target, new Runner());
		
		fo.setPath(entryPoint.getPath());
		fo.goToPathPosition(entryPoint.getPathPosition());
		
		addFollower(fo);
		
		return fo;
	}
	
	public NodeSwitch findSwitch(Node n) {
		
		NodeSwitch sw = switches.stream().filter(s -> s.node == n).findAny().orElse(null);
		
		if(sw == null) {
			sw = new NodeSwitch(n);
			switches.add(sw);
		}
		
		return sw;
	}

	
	@Override
	public void onPathEvent(PathEvent e) {
	
		if(e.type == PathEventEnum.PATH_CREATE) {
			
			PathEvent.PathAddRemove ne = (PathEvent.PathAddRemove) e;
			
			if(ne.path.getNodeIn().numLinks() > 2)
				findSwitch(ne.path.getNodeIn());
			if(ne.path.getNodeIn().numLinks() > 2)
				findSwitch(ne.path.getNodeOut());
		}else
		if(e.type == PathEventEnum.PATH_DELETE) {
			
			PathEvent.PathAddRemove ne = (PathEvent.PathAddRemove) e;
			
			switches.forEach(s -> {
				if(s.node == ne.path.getNodeIn() || s.node == ne.path.getNodeOut()) 
					s.update();
			});
			
		}else
		if(e.type == PathEventEnum.NODE_REMOVE) {
			
			PathEvent.NodeAddRemove ne = (PathEvent.NodeAddRemove) e;
			
			removeNodeSwitch(ne.node);
			
		}
		
		super.onPathEvent(e);
	}
	
	
	private void removeNodeSwitch(Node n) {
		
		NodeSwitch ns = findSwitch(n);
		if(ns != null) {
			
			switches.remove(ns);
			ns.delete();
		}
	}
	
	
	private class Runner extends PathTreeRunner implements EnterFrameListener{

		public Runner() {
			setSpeed(8);
			RootClass.backstage().getActivePatch().getDisplayManager().addEnterFrameListener(this);
			setPosObject(new Position.PositionOffset(constraint));
		}
		
		@Override
		public Path chooseNextSegment(Node n) {
			
			if(n.numLinks() == 1) {
				
				return n.getLink(0).rootSegment();
			}else
			if(n.numLinks() == 2) {
				
				return n.getLink(getPath() == n.getLink(0).rootSegment() ? 1 : 0).rootSegment(); 
				
			}else{
			
				return findSwitch(n).getPath();
			}
		}

		@Override
		public void enterFrame() {
			doStep();
			
		}
		
		@Override
		public void onRemoveFromConstraint() {
			
			super.onRemoveFromConstraint();
			
			RootClass.backstage().getActivePatch().getDisplayManager().removeEnterFrameListener(this);
		}
		
	}
	
	public class NodeSwitch {
		
		final Node node;
		private Path path;
		int pathIndex;
		final NodeSwitchModule module;
		
		public NodeSwitch(Node node) {
			
			this.node = node;
			module = new NodeSwitchModule(PathTreeConstraint.this, this);
			getDomain().addModule(module);
		}
		
		public Node getNode() {
			return node;
		}
		
		public Path getPath() {
			update();
			return path;
		}
		
		public int numPaths() {
			
			return node.numLinks();
		}
		
		public void setPath(int i) {
			pathIndex = i;
			update();
		}
		
		public void update() {
			
			if(numPaths() > 0) {
				pathIndex = Math.max(0, Math.min(numPaths() - 1, pathIndex));
				path = (Path) node.getLink(pathIndex).rootSegment();
			}
		}
		
		public void delete() {
			
			getDomain().removeModule(module, null);
		}
		
	}

	
}
