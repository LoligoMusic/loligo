package constraints;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import constants.ModuleEvent;
import constants.ModuleEventListener;
import gui.paths.PathEvent;
import gui.paths.PathEventManager;
import gui.paths.PathListener;
import lombok.Getter;
import module.Module;
import patch.Domain;
import patch.DomainObject;
import pr.DisplayObjectIF;

public class ConstraintContext implements ModuleEventListener{
	
	public static ConstraintContext create(Domain domain) {
		
		ConstraintContext cc = new ConstraintContext();
		domain.getModuleManager().addModuleEventListener(cc);
		return cc;
	}
	
	public static ConstraintContext get(DomainObject d) {
		
		return d.getDomain().getConstraintContext();
	}
	
	
	@Getter
	private final List<PosConstraintContainer> constraints = new ArrayList<>();
	
	private final Map<DisplayObjectIF, Constrainable> constrainableGUIMap = new IdentityHashMap<>();
	private final PathEventManager pathEventManager = new PathEventManager();
	//private final PathSnapManager pathSnap = new PathSnapManager(this);
	
	@Override
	public void moduleEvent(ModuleEvent e, Module m) {
		
		if(m instanceof PosConstraintContainer p) {

			switch(e) {
			case MODULE_ADD    -> constraints.add(p);
			case MODULE_REMOVE -> constraints.remove(p);
			}
		}
	}
	
	
	public void assignConstraintGui(Constrainable c) {
		
		constrainableGUIMap.put(c.getGui(), c);
	}
	
	public void removeConstraintGui(DisplayObjectIF gui) {
		
		constrainableGUIMap.remove(gui);
	}
	
	public Constrainable getConstraintFromGui(DisplayObjectIF d) {
		
		return constrainableGUIMap.get(d);
	}
	
	public void addListener(PathListener lis) {
		
		pathEventManager.addListener(lis);
	}
	
	public void removeListener(PathListener lis) {
		
		pathEventManager.removeListener(lis);
	}
	
	public void dispatchPathEvent(PathEvent pe) {
		
		pathEventManager.onPathEvent(pe);
	}


	

	
}
