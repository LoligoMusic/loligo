package constraints;

import gui.Displayable;
import gui.paths.PathEvent;
import gui.paths.PathListener;
import lombok.val;
import pr.Positionable;
import save.ConstrainableData;
import save.PathData;
import save.SaveDataContext;

/**
 * Interface for Objects that can run on LoligoPosConstraint motionpaths. 
 * Instances have to register through their domain's ConstraintManager.assignConstrainableGui
 * and unregister on deletion through removeConstrainableGui.
 */

public interface Constrainable extends Positionable, Displayable, PathListener {

	public FollowerObject getFollowerObject();
	public void setFollowerObject(FollowerObject followerObject);
	public void updatePosition();
	//public DisplayObjectIF getConstraintGui();
	
	public default LoligoPosConstraint getContainingPosConstraint() {
		
		val f = getFollowerObject();
		return f == null ? null : f.getPosConstraint();
	}
	
	public ConstrainableData toConstrainableMemento(SaveDataContext sdc, PathData pd);
	public void restoreMemento(ConstrainableData cd);
	
	public default void onAddToConstraint() {}
	public default void onRemoveFromConstraint() {}
	
	@Override
	default void onPathEvent(PathEvent e) {}
	
	
}
