package constraints;

import constants.DisplayFlag;
import gui.DisplayObjects;
import gui.Position;
import gui.paths.Path;
import gui.paths.PathEvent;
import gui.paths.PathObject;
import lombok.Getter;
import pr.DisplayObject;
import util.Container;


public class FollowerObject implements PathObject, Container<Constrainable> {
	
	@Getter
	private final PathObject pathObject;
	
	private final LoligoPosConstraint posConstraint;
	private final Constrainable target;
	//private double offset;
	
	//private Position pos = new Position.PositionOffset(this.posConstraint);
	
	private DisplayObject gui;
	
	/*
	public Follower(final Positionable _target) {
		
		this(_target, 0);
		offset = givePhase(this);
	}
	*/
	
	FollowerObject(LoligoPosConstraint posConstraint, final Constrainable _target, PathObject pathObject) {
		
		this.pathObject = pathObject;
		this.posConstraint = posConstraint;
		target = _target;
		target.setFollowerObject(this);
		
		target.setPosObject(new Position.PositionTarget(pathObject));
		
		if(this.posConstraint.gui != null)
			createGUI();
	}
	
	public void createGUI() {
		
		if(gui != null)
			return;
		
		gui = new DisplayObject();
		
		gui.setPosObject(new Position.PositionTarget(pathObject));
		gui.setFlag(DisplayFlag.CLICKABLE, false);
		gui.setFlag(DisplayFlag.DRAGABLE, false);
		gui.setFlag(DisplayFlag.ROUND, true);
		gui.setWH(4, 4);
		gui.setColor(posConstraint.gui.getColor());
		
		//target.getGui().addChild(this.gui);
		
		//this.posConstraint.gui
	}

	public LoligoPosConstraint getPosConstraint() {
		return posConstraint;
	}
	
	@Override
	public Constrainable getValue() {
		return target;
	}
	

	@Override
	public void setValue(Constrainable o) {
		//target = o;
	}

	
	@Override
	public float getX() {
		return pathObject.getX();
	}

	
	@Override
	public float getY() {
		return pathObject.getY();
	}
	

	@Override
	public void setPos(float x, float y) {
		
		pathObject.setPos(x, y);
		//target.setPos(getX(), getY());
		
		//if(gui != null)
		//	gui.setPos(x, y);
	}
	

	@Override
	public Position getPosObject() {
		
		return pathObject.getPosObject();
	}

	
	@Override
	public void setPosObject(Position p) {
		
		pathObject.setPosObject(p);
	}

	@Override
	public void onAddToConstraint() {
	
		pathObject.onAddToConstraint();
		target.onAddToConstraint();
	}
	
	@Override
	public void onRemoveFromConstraint() {
		
		if(gui != null)
			DisplayObjects.removeFromDisplay(gui);
		
		pathObject.onRemoveFromConstraint();
		target.onRemoveFromConstraint();
		target.setFollowerObject(null);
	}
	
	/*
	public double getOffset() {
		return offset;
	}

	public void setOffset(double offset) {
		this.offset = offset;
	}
	*/
	
	public Constrainable getTarget() {
		
		return target;
	}

	public DisplayObject getGui() {
		
		return gui;
	}

	public void setGui(DisplayObject gui) {
		
		this.gui = gui;
	}

	
	
	@Override
	public Path getSegment() {
		
		return pathObject.getSegment();
	}

	@Override
	public void setSegment(Path path) {
		
		pathObject.setSegment(path);
	}

	@Override
	public void setPath(Path path) {
		
		pathObject.setPath(path);
	}

	@Override
	public Path getPath() {
		
		return pathObject.getPath();
	}

	@Override
	public double getPathPosition() {
		
		return pathObject.getPathPosition();
	}

	@Override
	public void setGlobal(float x, float y) {
		
		pathObject.setGlobal(x, y);
	}

	@Override
	public void goToPathPosition(double d) {
		
		pathObject.goToPathPosition(d);
	}

	@Override
	public void updatePosition() {
		
		pathObject.updatePosition();
	}

	@Override
	public double getTangent() {
		
		return pathObject.getTangent();
	}

	@Override
	public void setTanget(double tangent) {
		
		pathObject.setTanget(tangent);
	}

	@Override
	public void onPathEvent(PathEvent e) {
		
		pathObject.onPathEvent(e);
		target.onPathEvent(e);
		/*
		
		if(e.type == PathEventEnum.PATH_DELETE) {
			
			PathEvent.PathAddRemove pr = (PathEvent.PathAddRemove) e;
			
			if(pr.path == getPath()) {
				
				posConstraint.removeFollower(this);
			}
		}
		*/
	}
	
	@Override
	public void copyPositionTo(PathObject po) {
		
		pathObject.copyPositionTo(po);
	}
	
}