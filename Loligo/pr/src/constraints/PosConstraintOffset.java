package constraints;

import gui.paths.OffsetPathObject;
import gui.paths.Path;
import gui.paths.PathConstraint;
import gui.paths.PathEvent;
import gui.paths.PathMementoFactory.ConstraintMemento;
import gui.paths.SinglePathConstraint;
import gui.paths.gui.PosConstraintGUI;
import gui.paths.gui.PosConstraintGUIFactory;
import lombok.Getter;
import module.PositionableModule;
import pr.DisplayObjectIF;
import save.ModuleData;
import save.PathData;
import save.SaveDataContext;

public class PosConstraintOffset extends LoligoPosConstraint implements PathConstraint{

	private double phase;
	//private Path path;
	@Getter
	private SinglePathConstraint constraint;
	
	public PosConstraintOffset(PositionableModule m) {
		
		super(new SinglePathConstraint(), m);
		
		setSegmentFactory(new LoligoSegmentFactory());
		
		this.constraint = ((SinglePathConstraint) super.constraint);
		
	}
	
	
	@Override
	public DisplayObjectIF createGUI() {
		
		if(gui == null) {
			PosConstraintGUI pg = new PosConstraintGUI(this, PosConstraintGUIFactory.singlePath());
			gui = pg;
		}
		return gui;
	}
	
	
	public FollowerObject addFollower(Constrainable target, double offset) {
		
		FollowerObject f = new FollowerObject(this, target, new OffsetPathObject(this, offset));
		
		super.addFollower(f);
		
		ConstraintContext.get(this).dispatchPathEvent(new PathEvent.PathObjectAddRemove(PathEvent.ADD, f, constraint.getPath()));
		//PathEventManager.instance().onPathEvent(new PathEvent.PathObjectAddRemove(PathEvent.ADD, f, constraint.getPath()));
		getDomain().getHistory().addAction(
				() -> addFollower(target, offset), 
				() -> removeFollower(f)
			);
		
		updatePosConstraint();
		
		return f;
	}
	
	
	@Override
	public FollowerObject addFollower(Constrainable target, Path p, double pathPos) {
		
		return addFollower(target, pathPos);
	}
	
	
	@Override
	public FollowerObject addFollowerAutoPos(Constrainable target, Path p, double pathPos) {
		
		return addFollower(target, p, pathPos - phase);
	}
	
	
	@Override
	public FollowerObject addFollowerAutoPos(Constrainable target) {
		
		double off = constraint.getPath().nearestPoint(target.getX(), target.getY()) - phase;
		
		//double snapDistance = 3 / path.getLength();
		
		return addFollower(target, off);
	}
	
	@Override
	public void removeFollower(FollowerObject f) {
		
		//super.removeFollower(f);
		
		getDomain().getHistory().addAction(
				() -> {
					super.removeFollower(f);
					getDomain().getConstraintContext().dispatchPathEvent(
							new PathEvent.PathObjectAddRemove(PathEvent.REMOVE, f, constraint.getPath()));
				},
				() -> addFollower(f.getTarget(), f.getPathPosition())
			).execute();
		
		
	}
	
	
	@Override
	public void setOffset(double offset) {
		
		phase = offset;
	}

	@Override
	public double getOffset() {
		
		return phase;
	}

	@Override
	public Path getPath() {
	
		return constraint.getPath();
	}

	public PathData toSaveForm(SaveDataContext sdc, ModuleData pathModule) {
		
		
		PathData pd = new PathData();
		/*
		pd.path = pathModule;
		
		for (FollowerObject f : getFollowers()) {
			
			pd.addAnim(sdc.findData(f.getTarget()), f.getPathPosition() - getOffset());
		}
		
		sdc.addPath(pd);
		*/
		return pd;
	}
	
	public void duplicateAppended(PosConstraintOffset p) {
		/*
		for (int i = 0; i < followers.size(); i++) {
			FollowerObject f1 = followers.getContainer(i);
			PositionableModule m = (PositionableModule) f1.getTarget().duplicate();//ModuleManager.getModuleFromGui(f1.target).duplicate();
			p.addFollower(m, f1.getPathPosition());
			
		}
		p.updatePosConstraint();
		*/
	}
	
	@Override
	public void toConstraintMemento(ConstraintMemento cm) {
	
		super.toConstraintMemento(cm);
	}
	
	@Override
	public void restoreConstraintMemento(ConstraintMemento cm) {
		
		super.restoreConstraintMemento(cm);
	}
	
	
}
