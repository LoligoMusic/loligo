package constraints;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Lists;
import constants.DisplayFlag;
import gui.DisplayObjects;
import gui.Position;
import gui.paths.Node;
import gui.paths.Path;
import gui.paths.PathEvent;
import gui.paths.PathEventEnum;
import gui.paths.PathEventManager;
import gui.paths.PathListener;
import gui.paths.PathMementoFactory.ConstraintMemento;
import gui.paths.PosConstraint;
import gui.paths.Segment;
import gui.paths.gui.PosConstraintGUI;
import lombok.val;
import gui.paths.SegmentFactory;
import gui.paths.SegmentType;
import module.Module;
import module.PositionableModule;
import patch.Domain;
import patch.DomainObject;
import pr.DisplayObjectIF;
import pr.Positionable;
import save.Saveable;

public class LoligoPosConstraint implements PosConstraint, Positionable, PathListener, Saveable, DomainObject {
	
	protected final gui.paths.PosConstraint constraint;
	
	protected final List<FollowerObject> followers = new ArrayList<>();
	protected final List<Constrainable> constrainables = Lists.transform(followers, FollowerObject::getTarget);
	
	private final PositionableModule module;
	
	public PosConstraintGUI gui;
	//private Position pos = new Position();

	
	public LoligoPosConstraint(gui.paths.PosConstraint constraint, PositionableModule m) {
		
		module = m;
		this.constraint = constraint;
		
		//constraint.setPosObject(new Position.PositionTarget(m));
		
		PathEventManager.instance().addListener(this);
		//ConstraintContext.get(m).addListener(this);
	}
	
	
	public DisplayObjectIF createGUI() {
		
		if(gui == null)
			gui = new PosConstraintGUI(this);
		
		return gui;
	}
	
	
	public Module getModule() {
		
		return module;
	}
	
	public List<Constrainable> getConstrainables() {
		return constrainables;
	}
	
	public List<FollowerObject> getFollowers() {
		
		return followers;
	}
	
	/*
	@Override
	public List<? extends Module> getContainedModules() {
		
		return new ArrayList<>(followers);
	}
	*/
	
	public FollowerObject addFollower(Constrainable target) {
		
		return null;
	}
	
	public FollowerObject addFollower(FollowerObject fo) {
		
		followers.add(fo);
		fo.onAddToConstraint();
		
		DisplayObjectIF d = fo.getTarget().getGui();
		if(d != null)
			d.setFlag(DisplayFlag.lockedPosition, true);
		
		return fo;
	}
	
	public FollowerObject addFollower(Constrainable target, Path p, double pathPos) {
		
		return null;
	}
	
	public FollowerObject addFollowerAutoPos(Constrainable target, Path p, double pathPos) {
		
		return null;
	}
	
	public FollowerObject addFollowerAutoPos(Constrainable target) {
		
		return null;
	}
	
	/*
	@Override
	public boolean addModule(Module m) {
		
		if(m instanceof PositionableModule)
			addFollower( (PositionableModule) m);
		return true;
	}
	*/
	
	@Override
	public void updatePosConstraint() {
		
		constraint.updatePosConstraint();
		
		followers.forEach(FollowerObject::updatePosition);
	}
	
	public void removeFollower(Constrainable p) {
		
		int i = constrainables.indexOf(p);
		if(i >= 0) 
			removeFollower(i);
		
	}
	
	/*
	@Override
	public boolean removeModule(Module m) {
		
		if(m instanceof PositionableModule)
			removeFollower((PositionableModule) m);
		return true;
	}
	*/
	
	public void removeFollower(int idx) {
		
		removeFollower(followers.get(idx));
	}
	
	public void removeFollower(FollowerObject f) {
		
		DisplayObjectIF d = f.getTarget().getGui();
		if(d != null)
			d.setFlag(DisplayFlag.lockedPosition, false);
		
		f.getTarget().setPosObject(new Position(f.getX(), f.getY()));
		
		val dm = d.getDm();
		DisplayObjects.holdInScreen(f.getTarget(), dm.getWidth(), dm.getHeight());
		
		followers.remove(f);
		
		f.onRemoveFromConstraint(); 
		dispatchEvent(new PathEvent.PathObjectAddRemove(PathEvent.REMOVE, f, f.getPath()));
		
		
	}
	
	
	public void removeAllFollowers() {
		
		int s = followers.size();
		
		for (int i = 0; i < s; i++) {
			
			removeFollower(followers.get(0));
		}
	}
	
	
	@Override
	public float getX() {
		
		return constraint.getX();
	}

	@Override
	public float getY() {
		
		return constraint.getY();
	}

	@Override
	public void setPos(float x, float y) {
		
		constraint.setPos(x, y);
	}	
	
	@Override
	public Position getPosObject() {
		
		return constraint.getPosObject();
	}

	@Override
	public void setPosObject(Position p) {
		
		constraint.setPosObject(p);
	}
	
	@Override
	public void onPathEvent(PathEvent e) {
	
		if(e.type == PathEventEnum.PATH_DELETE) {
			
			PathEvent.PathAddRemove pe = (PathEvent.PathAddRemove) e;
			
			for (int i = followers.size() - 1; i >= 0; i--) {
				
				FollowerObject fo = followers.get(i);
				if(fo.getPath() == pe.path) {
					removeFollower(i);
				}
			}
			
		}
		
		followers.forEach(p -> p.onPathEvent(e));
 		getNodes().forEach(n -> n.onPathEvent(e));
	}
	
	@Override
	public void setPathListener(PathListener pathListener) {
		
		constraint.setPathListener(pathListener);
	}
	
	
	public gui.paths.PosConstraint getPosConstraint() {
		return constraint;
	}
	
	@Override
	public List<Path> getPaths() {
		
		return constraint.getPaths();
	}
	
	@Override
	public List<Node> getNodes() {
	
		return constraint.getNodes();
	}
	
	@Override
	public void removeNode(Node n) {
		
		constraint.removeNode(n);
	}

	@Override
	public void removePath(Path p) {
		
		constraint.removePath(p);
	}

	@Override
	public Path addPath(Node n1, Node n2, SegmentType type) {
		
		return constraint.addPath(n1, n2, type);
	}
	
	@Override
	public Path addPath(Path path) {
		
		return constraint.addPath(path);
	}

	@Override
	public Node newNode() {
		
		return constraint.newNode();
	}

	@Override
	public void addNode(Node n) {
		
		constraint.addNode(n);
	}

	@Override
	public Domain getDomain() {
		
		return module.getDomain();
	}
	
	@Override
	public void restoreMemento(Object saveform) {
		
		//new PathMementoFactory(this).restoreConstraint((ConstraintMemento) saveform);
	}

	@Override
	public Object toMemento() {
		
		return null;
	}


	@Override
	public void dispatchEvent(PathEvent e) {
		
		constraint.dispatchEvent(e);
	}


	@Override
	public void toConstraintMemento(ConstraintMemento cm) {
		constraint.toConstraintMemento(cm);
	}


	@Override
	public void restoreConstraintMemento(ConstraintMemento cm) {
		constraint.restoreConstraintMemento(cm);
	}
	
	@Override
	public void setSegmentFactory(SegmentFactory sf) {
		
		constraint.setSegmentFactory(sf);
	}

	@Override
	public SegmentFactory getSegmentFactory() {
		
		return constraint.getSegmentFactory();
	}


	@Override
	public Segment createSegment(Node n1, Node n2, SegmentType type) {
		
		return constraint.createSegment(n1, n2, type);
	}
	
}
