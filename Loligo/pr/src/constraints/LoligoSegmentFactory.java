package constraints;

import gui.paths.BezierSegment;
import gui.paths.Node;
import gui.paths.PosConstraint;
import gui.paths.Segment;
import gui.paths.SegmentFactory;
import gui.paths.SegmentType;
import module.modules.anim.Path;
import pr.Positionable;

public class LoligoSegmentFactory implements SegmentFactory {

	@Override
	public Segment createSegmentByType(PosConstraint posConstraint, Node n1, Node n2, SegmentType type) {
		
		Segment s = null;
		
		if(type == SegmentType.BEZIER) {
			
			s = new BezierSegment(posConstraint, null, n1, n2) {
				
				@Override public Positionable newControlPoint(Node n) {
					
					return new Path.CP(n);
				}
			};
			
		}else {
			
			s = defaultFactory.createSegmentByType(posConstraint, n1, n2, type);
		}
		
		s.init();
		
		return s;
	}

}
