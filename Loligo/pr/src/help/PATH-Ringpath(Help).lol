{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010218475,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.anim.RingPath",
    "name" : "RingPath",
    "label" : "RingPath.01",
    "gui" : true,
    "xy" : [ 436, 380 ],
    "inputs" : [ {
      "name" : "Radius",
      "type" : "Number",
      "value" : 2.34,
      "color" : "0"
    }, {
      "name" : "Position",
      "type" : "Number",
      "value" : 81.42441860465117,
      "color" : "0"
    } ],
    "y" : 380,
    "x" : 436
  }, {
    "id" : 2,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 475, 490 ],
    "map" : {
      "shape" : "TRIANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF1461C8"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 490,
    "x" : 475
  }, {
    "id" : 3,
    "node" : "module.modules.anim.Segment",
    "name" : "Segment",
    "label" : "Segment.01",
    "gui" : true,
    "xy" : [ 344, 453 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Resolution",
      "type" : "Number",
      "value" : 6.0,
      "color" : "0"
    }, {
      "name" : "Sector",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Inner radius",
      "type" : "Number",
      "value" : 0.56,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF8EC814"
    } ],
    "y" : 453,
    "x" : 344
  }, {
    "id" : 4,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.02",
    "gui" : true,
    "xy" : [ 670, 398 ],
    "map" : {
      "shape" : "ELLIPSE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFAF14C8"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 398,
    "x" : 670
  }, {
    "id" : 5,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 200, 404 ],
    "map" : {
      "iteration" : 81.0,
      "position" : 0.43023255813953487
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.25,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 404,
    "x" : 200
  }, {
    "id" : 6,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 232, 122 ],
    "map" : {
      "width" : 342,
      "comment" : "RingPath\n-----------------------------------------------\nCircular motion path. Add objects with drag&drop",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 122,
    "x" : 232
  } ],
  "links" : [ {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "Position"
  } ],
  "paths" : [ {
    "@id" : 1,
    "node" : 0,
    "anims" : [ {
      "type" : "NODE",
      "node" : 2,
      "pos" : 0.4794125258922577
    }, {
      "type" : "NODE",
      "node" : 3,
      "pos" : 0.2823869287967682
    } ]
  } ]
}