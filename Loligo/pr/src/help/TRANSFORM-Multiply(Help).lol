{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1007778943,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Multiply",
    "name" : "Multiply",
    "label" : "Multiply.01",
    "gui" : true,
    "xy" : [ 422, 346 ],
    "inputs" : [ {
      "name" : "Transform1",
      "type" : "TRANSFORM",
      "value" : [ 0.6374239921569824, -0.7705132365226746, 0.0, 0.7705132365226746, 0.6374239921569824, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Transform2",
      "type" : "TRANSFORM",
      "value" : [ 0.6100000143051147, 0.0, 0.0, 0.0, 0.6100000143051147, 0.0 ],
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 346,
    "x" : 422
  }, {
    "id" : 1,
    "node" : "module.modules.math.Rotate",
    "name" : "Rotate",
    "label" : "Rotate.01",
    "gui" : true,
    "xy" : [ 342, 308 ],
    "inputs" : [ {
      "name" : "Rotate",
      "type" : "Number",
      "value" : 0.14,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 308,
    "x" : 342
  }, {
    "id" : 2,
    "node" : "module.modules.math.Scale",
    "name" : "Scale",
    "label" : "Scale.01",
    "gui" : true,
    "xy" : [ 340, 380 ],
    "inputs" : [ {
      "name" : "Scale",
      "type" : "Number",
      "value" : 0.61,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 380,
    "x" : 340
  }, {
    "id" : 3,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 556, 325 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.3888286352157593, -0.4700130820274353, 0.0, 0.4700130820274353, 0.3888286352157593, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF631AD9"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 325,
    "x" : 556
  }, {
    "id" : 4,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 254, 205 ],
    "map" : {
      "width" : 393,
      "comment" : "Multiply\n------------------------------------------------------\nCombines two transforms into one",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 205,
    "x" : 254
  }, {
    "id" : 5,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 255, 428 ],
    "map" : {
      "width" : 389,
      "comment" : "\n------------------------------------------------------\n",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 428,
    "x" : 255
  }, {
    "id" : 6,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 261, 308 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.14,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 308,
    "x" : 261
  }, {
    "id" : 7,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02",
    "gui" : true,
    "xy" : [ 260, 380 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.61,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 380,
    "x" : 260
  } ],
  "links" : [ {
    "nodeOut" : 0,
    "output" : "Transform",
    "nodeIn" : 3,
    "input" : "Transform"
  }, {
    "nodeOut" : 1,
    "output" : "Transform",
    "nodeIn" : 0,
    "input" : "Transform1"
  }, {
    "nodeOut" : 2,
    "output" : "Transform",
    "nodeIn" : 0,
    "input" : "Transform2"
  }, {
    "nodeOut" : 6,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Rotate"
  }, {
    "nodeOut" : 7,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "Scale"
  } ]
}