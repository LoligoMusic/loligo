{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010371202,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Alpha",
    "name" : "Alpha",
    "label" : "Alpha.01",
    "gui" : true,
    "xy" : [ 357, 457 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFC81414"
    }, {
      "name" : "Alpha",
      "type" : "Number",
      "value" : 0.3,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 457,
    "x" : 357
  }, {
    "id" : 1,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 251, 213 ],
    "map" : {
      "width" : 323,
      "comment" : "Alpha\n---------------------------------------------\nReplace a color's alpha-value (transparency)",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 213,
    "x" : 251
  }, {
    "id" : 2,
    "node" : "module.modules.math.Color",
    "name" : "Color",
    "label" : "Color.01",
    "gui" : true,
    "xy" : [ 282, 360 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFC81414"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 360,
    "x" : 282
  }, {
    "id" : 3,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 257, 459 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.3,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 459,
    "x" : 257
  }, {
    "id" : 4,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 520, 369 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFC81414"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 369,
    "x" : 520
  }, {
    "id" : 5,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy",
    "gui" : true,
    "xy" : [ 520, 469 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "4CC81414"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 469,
    "x" : 520
  } ],
  "links" : [ {
    "nodeOut" : 0,
    "output" : "Color",
    "nodeIn" : 5,
    "input" : "Color"
  }, {
    "nodeOut" : 2,
    "output" : "Color",
    "nodeIn" : 0,
    "input" : "Color"
  }, {
    "nodeOut" : 3,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "Alpha"
  }, {
    "nodeOut" : 2,
    "output" : "Color",
    "nodeIn" : 4,
    "input" : "Color"
  } ]
}