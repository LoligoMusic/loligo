{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1011788460,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.01",
    "gui" : true,
    "xy" : [ 477, 251 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 251,
    "x" : 477
  }, {
    "id" : 1,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Bang.01-Copy.01",
    "gui" : true,
    "xy" : [ 278, 454 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "BANG"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 454,
    "x" : 278
  }, {
    "id" : 2,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.02",
    "gui" : true,
    "xy" : [ 476, 457 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 457,
    "x" : 476
  }, {
    "id" : 3,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 257, 163 ],
    "map" : {
      "width" : 366,
      "comment" : "TimerFlop\n------------------------------------------------\nSwitches to one if the input has been 1 for a certain time",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 163,
    "x" : 257
  }, {
    "id" : 4,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 253, 376 ],
    "map" : {
      "width" : 366,
      "comment" : "MonoFlop\n-----------------------------------------------\nSets the output to 1 for a predefined time.",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 376,
    "x" : 253
  }, {
    "id" : 5,
    "node" : "module.modules.math.TimerFlop",
    "name" : "TimerFlop",
    "label" : "TimerFlop.01",
    "gui" : true,
    "xy" : [ 372, 272 ],
    "inputs" : [ {
      "name" : "Set",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Time",
      "type" : "Number",
      "value" : 60.0,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Progress",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 272,
    "x" : 372
  }, {
    "id" : 6,
    "node" : "module.modules.math.MonoFlop",
    "name" : "MonoFlop",
    "label" : "MonoFlop.01",
    "gui" : true,
    "xy" : [ 381, 455 ],
    "inputs" : [ {
      "name" : "Set",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Time",
      "type" : "Number",
      "value" : 60.0,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 455,
    "x" : 381
  }, {
    "id" : 7,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03",
    "gui" : true,
    "xy" : [ 267, 265 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 265,
    "x" : 267
  }, {
    "id" : 8,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 476, 280 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 280,
    "x" : 476
  } ],
  "links" : [ {
    "nodeOut" : 7,
    "output" : "Out",
    "nodeIn" : 5,
    "input" : "Set"
  }, {
    "nodeOut" : 6,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "In"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "In"
  }, {
    "nodeOut" : 1,
    "output" : "Out",
    "nodeIn" : 6,
    "input" : "Set"
  }, {
    "nodeOut" : 5,
    "output" : "Progress",
    "nodeIn" : 8,
    "input" : "In"
  } ]
}