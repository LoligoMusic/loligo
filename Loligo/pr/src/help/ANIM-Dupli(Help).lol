{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1008664584,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 300, 167 ],
    "map" : {
      "width" : 302,
      "comment" : "Dupli\n------------------------------------------\nDuplicates another Anim-node",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 167,
    "x" : 300
  }, {
    "id" : 1,
    "node" : "module.modules.anim.Dupli",
    "name" : "Dupli",
    "label" : "Dupli.01",
    "gui" : true,
    "xy" : [ 611, 307 ],
    "map" : {
      "target" : {
        "@class" : "save.Reference$Node",
        "node" : 2
      }
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFFFFFFF"
    } ],
    "y" : 307,
    "x" : 611
  }, {
    "id" : 2,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 379, 406 ],
    "map" : {
      "shape" : "TRIANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ -0.801665723323822, 0.5977725982666016, 0.0, -0.5977725982666016, -0.801665723323822, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF1E61C9"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 406,
    "x" : 379
  }, {
    "id" : 3,
    "node" : "module.modules.math.Rotate",
    "name" : "Rotate",
    "label" : "Rotate.01",
    "gui" : true,
    "xy" : [ 234, 376 ],
    "inputs" : [ {
      "name" : "Rotate",
      "type" : "Number",
      "value" : 0.6019736842105263,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 376,
    "x" : 234
  }, {
    "id" : 4,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 170, 394 ],
    "map" : {
      "iteration" : 70.0,
      "position" : 0.6052631578947368
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 394,
    "x" : 170
  }, {
    "id" : 5,
    "node" : "module.modules.anim.Dupli",
    "name" : "Dupli",
    "label" : "Dupli.01-Copy",
    "gui" : true,
    "xy" : [ 600, 415 ],
    "map" : {
      "target" : {
        "@class" : "save.Reference$Node",
        "node" : 2
      }
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.6100000143051147, 0.0, 0.0, 0.0, 0.6100000143051147, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFFFFFFF"
    } ],
    "y" : 415,
    "x" : 600
  }, {
    "id" : 6,
    "node" : "module.modules.anim.Dupli",
    "name" : "Dupli",
    "label" : "Dupli.01-Copy.01",
    "gui" : true,
    "xy" : [ 600, 520 ],
    "map" : {
      "target" : {
        "@class" : "save.Reference$Node",
        "node" : 2
      }
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF6D19DF"
    } ],
    "y" : 520,
    "x" : 600
  }, {
    "id" : 7,
    "node" : "module.modules.math.HSB",
    "name" : "HSB",
    "label" : "HSB.01",
    "gui" : true,
    "xy" : [ 267, 433 ],
    "inputs" : [ {
      "name" : "Hue",
      "type" : "Number",
      "value" : 0.6019736842105263,
      "color" : "0"
    }, {
      "name" : "Saturation",
      "type" : "Number",
      "value" : 0.85,
      "color" : "0"
    }, {
      "name" : "Brightness",
      "type" : "Number",
      "value" : 0.79,
      "color" : "0"
    }, {
      "name" : "Alpha",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 433,
    "x" : 267
  }, {
    "id" : 8,
    "node" : "module.modules.math.Scale",
    "name" : "Scale",
    "label" : "Scale.01",
    "gui" : true,
    "xy" : [ 496, 402 ],
    "inputs" : [ {
      "name" : "Scale",
      "type" : "Number",
      "value" : 0.61,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 402,
    "x" : 496
  }, {
    "id" : 9,
    "node" : "module.modules.math.Color",
    "name" : "Color",
    "label" : "Color.01",
    "gui" : true,
    "xy" : [ 516, 516 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF6D19DF"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 516,
    "x" : 516
  } ],
  "links" : [ {
    "nodeOut" : 9,
    "output" : "Color",
    "nodeIn" : 6,
    "input" : "Color"
  }, {
    "nodeOut" : 4,
    "output" : "Phase",
    "nodeIn" : 3,
    "input" : "Rotate"
  }, {
    "nodeOut" : 8,
    "output" : "Transform",
    "nodeIn" : 5,
    "input" : "Transform"
  }, {
    "nodeOut" : 3,
    "output" : "Transform",
    "nodeIn" : 2,
    "input" : "Transform"
  }, {
    "nodeOut" : 7,
    "output" : "Color",
    "nodeIn" : 2,
    "input" : "Color"
  }, {
    "nodeOut" : 4,
    "output" : "Phase",
    "nodeIn" : 7,
    "input" : "Hue"
  } ]
}