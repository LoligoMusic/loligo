{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1007778943,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Rotate",
    "name" : "Rotate",
    "label" : "Rotate.01",
    "gui" : true,
    "xy" : [ 335, 423 ],
    "inputs" : [ {
      "name" : "Rotate",
      "type" : "Number",
      "value" : 0.9210526315789473,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 423,
    "x" : 335
  }, {
    "id" : 1,
    "node" : "module.modules.math.Scale",
    "name" : "Scale",
    "label" : "Scale.01",
    "gui" : true,
    "xy" : [ 336, 329 ],
    "inputs" : [ {
      "name" : "Scale",
      "type" : "Number",
      "value" : 0.9210526315789473,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 329,
    "x" : 336
  }, {
    "id" : 2,
    "node" : "module.modules.math.ScaleXY",
    "name" : "ScaleXY",
    "label" : "ScaleXY.01",
    "gui" : true,
    "xy" : [ 334, 246 ],
    "inputs" : [ {
      "name" : "X",
      "type" : "Number",
      "value" : 0.9210526315789473,
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "value" : 0.26,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 246,
    "x" : 334
  }, {
    "id" : 3,
    "node" : "module.modules.math.Translate",
    "name" : "Translate",
    "label" : "Translate.01",
    "gui" : true,
    "xy" : [ 335, 170 ],
    "inputs" : [ {
      "name" : "X",
      "type" : "Number",
      "value" : 0.9210526315789473,
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "value" : 0.14,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 170,
    "x" : 335
  }, {
    "id" : 4,
    "node" : "module.modules.math.Transform",
    "name" : "Transform",
    "label" : "Transform.01",
    "gui" : true,
    "xy" : [ 333, 524 ],
    "inputs" : [ {
      "name" : "X",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "value" : 0.07,
      "color" : "0"
    }, {
      "name" : "Scale X",
      "type" : "Number",
      "value" : 0.9210526315789473,
      "color" : "0"
    }, {
      "name" : "Scale Y",
      "type" : "Number",
      "value" : 0.9210526315789473,
      "color" : "0"
    }, {
      "name" : "Rotate",
      "type" : "Number",
      "value" : 0.9210526315789473,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 524,
    "x" : 333
  }, {
    "id" : 5,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 141, 73 ],
    "map" : {
      "width" : 232,
      "comment" : "Transform Nodes\n-------------------------------\n",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 73,
    "x" : 141
  }, {
    "id" : 6,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 508, 544 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.8100417256355286, 0.4383723735809326, 3.331629991531372, -0.4383723735809326, 0.8100417256355286, 6.156317234039307 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF154C5E"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 544,
    "x" : 508
  }, {
    "id" : 7,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02",
    "gui" : true,
    "xy" : [ 140, 523 ],
    "map" : {
      "width" : 131,
      "comment" : "Translate, rotate & \nscale in one",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 523,
    "x" : 140
  }, {
    "id" : 8,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy",
    "gui" : true,
    "xy" : [ 629, 189 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 92.10526275634766, 0.0, 1.0, 14.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF154C5E"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 189,
    "x" : 629
  }, {
    "id" : 9,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy.01",
    "gui" : true,
    "xy" : [ 540, 262 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.9210526347160339, 0.0, 0.0, 0.0, 0.25999999046325684, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF154C5E"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 262,
    "x" : 540
  }, {
    "id" : 10,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy",
    "gui" : true,
    "xy" : [ 142, 244 ],
    "map" : {
      "width" : 129,
      "comment" : "Scale horizontal & vertical \naxis independently",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 244,
    "x" : 142
  }, {
    "id" : 11,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy.02",
    "gui" : true,
    "xy" : [ 531, 344 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.9210526347160339, 0.0, 0.0, 0.0, 0.9210526347160339, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF154C5E"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 344,
    "x" : 531
  }, {
    "id" : 12,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.01",
    "gui" : true,
    "xy" : [ 143, 333 ],
    "map" : {
      "width" : 127,
      "comment" : "Scale proportionally",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 333,
    "x" : 143
  }, {
    "id" : 13,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy.03",
    "gui" : true,
    "xy" : [ 627, 438 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.879473865032196, 0.47594714164733887, 0.0, -0.47594714164733887, 0.879473865032196, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF154C5E"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 438,
    "x" : 627
  }, {
    "id" : 14,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.02",
    "gui" : true,
    "xy" : [ 144, 168 ],
    "map" : {
      "width" : 124,
      "comment" : "Set positional offset",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 168,
    "x" : 144
  }, {
    "id" : 15,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.03",
    "gui" : true,
    "xy" : [ 144, 420 ],
    "map" : {
      "width" : 127,
      "comment" : "Spin around",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 420,
    "x" : 144
  }, {
    "id" : 16,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 242, 127 ],
    "map" : {
      "iteration" : 51.0,
      "position" : 0.9243421052631579
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 127,
    "x" : 242
  }, {
    "id" : 17,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 297, 487 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.9210526315789473,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 487,
    "x" : 297
  } ],
  "links" : [ {
    "nodeOut" : 16,
    "output" : "Phase",
    "nodeIn" : 17,
    "input" : "In"
  }, {
    "nodeOut" : 16,
    "output" : "Phase",
    "nodeIn" : 1,
    "input" : "Scale"
  }, {
    "nodeOut" : 16,
    "output" : "Phase",
    "nodeIn" : 0,
    "input" : "Rotate"
  }, {
    "nodeOut" : 3,
    "output" : "Transform",
    "nodeIn" : 8,
    "input" : "Transform"
  }, {
    "nodeOut" : 4,
    "output" : "Transform",
    "nodeIn" : 6,
    "input" : "Transform"
  }, {
    "nodeOut" : 16,
    "output" : "Phase",
    "nodeIn" : 3,
    "input" : "X"
  }, {
    "nodeOut" : 2,
    "output" : "Transform",
    "nodeIn" : 9,
    "input" : "Transform"
  }, {
    "nodeOut" : 0,
    "output" : "Transform",
    "nodeIn" : 13,
    "input" : "Transform"
  }, {
    "nodeOut" : 16,
    "output" : "Phase",
    "nodeIn" : 2,
    "input" : "X"
  }, {
    "nodeOut" : 17,
    "output" : "Out",
    "nodeIn" : 4,
    "input" : "Scale X"
  }, {
    "nodeOut" : 17,
    "output" : "Out",
    "nodeIn" : 4,
    "input" : "Scale Y"
  }, {
    "nodeOut" : 17,
    "output" : "Out",
    "nodeIn" : 4,
    "input" : "Rotate"
  }, {
    "nodeOut" : 1,
    "output" : "Transform",
    "nodeIn" : 11,
    "input" : "Transform"
  } ]
}