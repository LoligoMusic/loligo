{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1006902660,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 352, 365 ],
    "map" : {
      "shape" : "ELLIPSE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF145CC8"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 365,
    "x" : 352
  }, {
    "id" : 1,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 305, 225 ],
    "map" : {
      "width" : 316,
      "comment" : "Shape\n-------------------------------------------\nDraws a shape",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 225,
    "x" : 305
  }, {
    "id" : 2,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy",
    "gui" : true,
    "xy" : [ 460, 370 ],
    "map" : {
      "shape" : "TRIANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFC83414"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 370,
    "x" : 460
  }, {
    "id" : 3,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy.01",
    "gui" : true,
    "xy" : [ 569, 366 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF14C89D"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 366,
    "x" : 569
  } ]
}