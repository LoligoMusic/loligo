{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010417016,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 348, 245 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 3.14,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 245,
    "x" : 348
  }, {
    "id" : 1,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 217, 173 ],
    "map" : {
      "width" : 487,
      "comment" : "Value-Boxes\n--------------------------------------------------------------------",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 173,
    "x" : 217
  }, {
    "id" : 2,
    "node" : "module.modules.math.Color",
    "name" : "Color",
    "label" : "Color.01",
    "gui" : true,
    "xy" : [ 347, 460 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF14C831"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 460,
    "x" : 347
  }, {
    "id" : 3,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Integer.01",
    "gui" : true,
    "xy" : [ 348, 271 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "INTEGER"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 3.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 271,
    "x" : 348
  }, {
    "id" : 4,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.01",
    "gui" : true,
    "xy" : [ 348, 297 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 297,
    "x" : 348
  }, {
    "id" : 5,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02",
    "gui" : true,
    "xy" : [ 221, 247 ],
    "map" : {
      "width" : 66,
      "comment" : "Numeric",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 247,
    "x" : 221
  }, {
    "id" : 6,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy",
    "gui" : true,
    "xy" : [ 222, 462 ],
    "map" : {
      "width" : 66,
      "comment" : "Color",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 462,
    "x" : 222
  }, {
    "id" : 7,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.01",
    "gui" : true,
    "xy" : [ 452, 245 ],
    "map" : {
      "width" : 164,
      "comment" : "Decimal - point numbers",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 245,
    "x" : 452
  }, {
    "id" : 8,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.02",
    "gui" : true,
    "xy" : [ 452, 271 ],
    "map" : {
      "width" : 164,
      "comment" : "Integer - whole numbers",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 271,
    "x" : 452
  }, {
    "id" : 9,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.03",
    "gui" : true,
    "xy" : [ 453, 298 ],
    "map" : {
      "width" : 163,
      "comment" : "Boolean - true/false or 1/0",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 298,
    "x" : 453
  }, {
    "id" : 10,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.04",
    "gui" : true,
    "xy" : [ 453, 464 ],
    "map" : {
      "width" : 225,
      "comment" : "Color with alpha value (transparency)",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 464,
    "x" : 453
  }, {
    "id" : 11,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Bang.01",
    "gui" : true,
    "xy" : [ 348, 324 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "BANG"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 324,
    "x" : 348
  }, {
    "id" : 12,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.05",
    "gui" : true,
    "xy" : [ 453, 326 ],
    "map" : {
      "width" : 228,
      "comment" : "Bang - Like Boolean, but automatically goes back to Zero exactly one frame after being triggered.",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 326,
    "x" : 453
  }, {
    "id" : 13,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Bang.01-Copy",
    "gui" : true,
    "xy" : [ 349, 384 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "PUSH"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 384,
    "x" : 349
  }, {
    "id" : 14,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.06",
    "gui" : true,
    "xy" : [ 455, 387 ],
    "map" : {
      "width" : 228,
      "comment" : "Push - Stays at 1 only as long as the button is held down.",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 387,
    "x" : 455
  } ]
}