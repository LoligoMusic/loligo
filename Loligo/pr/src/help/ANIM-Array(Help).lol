{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1007778943,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.anim.Array",
    "name" : "Array",
    "label" : "Array.01",
    "gui" : true,
    "xy" : [ 562, 405 ],
    "map" : {
      "target" : {
        "@class" : "save.Reference$Node",
        "node" : 1
      }
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF61FFA0"
    }, {
      "name" : "Offset",
      "type" : "TRANSFORM",
      "value" : [ 0.9685831665992737, 0.24868981540203094, 32.931827545166016, -0.24868981540203094, 0.9685831665992737, -8.455453872680664 ],
      "color" : "0"
    }, {
      "name" : "Length",
      "type" : "Number",
      "value" : 32.0,
      "color" : "0"
    } ],
    "y" : 405,
    "x" : 562
  }, {
    "id" : 1,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 633, 604 ],
    "map" : {
      "shape" : "ELLIPSE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ -0.02556140162050724, 0.19842293858528137, 0.0, -0.20233917236328125, -0.025066664442420006, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF00B2FF"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 604,
    "x" : 633
  }, {
    "id" : 2,
    "node" : "module.modules.math.Transform",
    "name" : "Transform",
    "label" : "Transform.01",
    "gui" : true,
    "xy" : [ 205, 309 ],
    "inputs" : [ {
      "name" : "X",
      "type" : "Number",
      "value" : 0.34,
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Scale X",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Scale Y",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Rotate",
      "type" : "Number",
      "value" : 0.96,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 309,
    "x" : 205
  }, {
    "id" : 3,
    "node" : "module.modules.math.Transform",
    "name" : "Transform",
    "label" : "Transform.02",
    "gui" : true,
    "xy" : [ 501, 598 ],
    "inputs" : [ {
      "name" : "X",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Scale X",
      "type" : "Number",
      "value" : 0.20394736842105263,
      "color" : "0"
    }, {
      "name" : "Scale Y",
      "type" : "Number",
      "value" : 0.2,
      "color" : "0"
    }, {
      "name" : "Rotate",
      "type" : "Number",
      "value" : -0.27,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 598,
    "x" : 501
  }, {
    "id" : 4,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.02",
    "gui" : true,
    "xy" : [ 437, 610 ],
    "map" : {
      "iteration" : 353.0,
      "position" : 0.20723684210526316
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 610,
    "x" : 437
  }, {
    "id" : 5,
    "node" : "module.modules.math.Color",
    "name" : "Color",
    "label" : "Color.01",
    "gui" : true,
    "xy" : [ 204, 245 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF61FFA0"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 245,
    "x" : 204
  }, {
    "id" : 6,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Integer.01",
    "gui" : true,
    "xy" : [ 206, 389 ],
    "map" : {
      "range" : [ "[D", [ -99999, 99999 ] ],
      "type" : "INTEGER"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 32.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 389,
    "x" : 206
  }, {
    "id" : 7,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 598, 536 ],
    "map" : {
      "width" : 55,
      "comment" : "Target",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 536,
    "x" : 598
  }, {
    "id" : 8,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy.01",
    "gui" : true,
    "xy" : [ 73, 309 ],
    "map" : {
      "width" : 116,
      "comment" : "Transform offset \nadded to each \nelement",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 309,
    "x" : 73
  }, {
    "id" : 9,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy.02",
    "gui" : true,
    "xy" : [ 76, 392 ],
    "map" : {
      "width" : 112,
      "comment" : "Number of copies",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 392,
    "x" : 76
  }, {
    "id" : 10,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy.03",
    "gui" : true,
    "xy" : [ 74, 240 ],
    "map" : {
      "width" : 118,
      "comment" : "Color tint added to \neach element",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 240,
    "x" : 74
  }, {
    "id" : 11,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy.04",
    "gui" : true,
    "xy" : [ 72, 157 ],
    "map" : {
      "width" : 239,
      "comment" : "Array\n--------------------------------\nMakes multiple copies of a target node ",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 157,
    "x" : 72
  } ],
  "links" : [ {
    "nodeOut" : 5,
    "output" : "Color",
    "nodeIn" : 0,
    "input" : "Color"
  }, {
    "nodeOut" : 2,
    "output" : "Transform",
    "nodeIn" : 0,
    "input" : "Offset"
  }, {
    "nodeOut" : 6,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "Length"
  }, {
    "nodeOut" : 4,
    "output" : "Phase",
    "nodeIn" : 3,
    "input" : "Scale X"
  }, {
    "nodeOut" : 3,
    "output" : "Transform",
    "nodeIn" : 1,
    "input" : "Transform"
  } ]
}