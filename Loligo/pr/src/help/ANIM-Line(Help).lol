{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010226407,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.anim.Line",
    "name" : "Line",
    "label" : "Line.01",
    "gui" : true,
    "xy" : [ 300, 380 ],
    "map" : {
      "target" : {
        "@class" : "save.Reference$Node",
        "node" : 1
      }
    },
    "inputs" : [ {
      "name" : "Weight",
      "type" : "Number",
      "value" : 0.19,
      "color" : "0"
    }, {
      "name" : "Section",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFC81414"
    } ],
    "y" : 380,
    "x" : 300
  }, {
    "id" : 1,
    "node" : "module.modules.anim.NullObject",
    "name" : "NullObject",
    "label" : "Line.01-target",
    "gui" : true,
    "xy" : [ 443, 291 ],
    "y" : 291,
    "x" : 443
  }, {
    "id" : 2,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 214, 159 ],
    "map" : {
      "width" : 376,
      "comment" : "Line\n--------------------------------------------------\n",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 159,
    "x" : 214
  }, {
    "id" : 3,
    "node" : "module.modules.anim.Path",
    "name" : "Path",
    "label" : "Path.01",
    "gui" : true,
    "xy" : [ 584, 280 ],
    "map" : {
      "mapping" : "MIRROR"
    },
    "inputs" : [ {
      "name" : "Position",
      "type" : "Number",
      "value" : 130.1875,
      "color" : "0"
    }, {
      "name" : "Tightness",
      "type" : "Number",
      "value" : -1.32,
      "color" : "0"
    } ],
    "y" : 280,
    "x" : 584
  }, {
    "id" : 4,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 497, 234 ],
    "map" : {
      "iteration" : 130.0,
      "position" : 0.203125
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.55,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 234,
    "x" : 497
  } ],
  "links" : [ {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 3,
    "input" : "Position"
  } ],
  "paths" : [ {
    "@id" : 1,
    "node" : 3,
    "constraint" : {
      "vertices" : [ {
        "@id" : 2,
        "x" : 21.0,
        "y" : 20.0
      }, {
        "@id" : 3,
        "x" : -55.0,
        "y" : 222.0
      }, {
        "@id" : 4,
        "x" : -197.0,
        "y" : 53.0
      } ],
      "paths" : [ {
        "subSegments" : [ {
          "segmentType" : "CURVE",
          "vertex1" : 2,
          "vertex2" : 4
        }, {
          "segmentType" : "CURVE",
          "vertex1" : 4,
          "vertex2" : 3
        } ],
        "segmentType" : "PATH",
        "vertex1" : 2,
        "vertex2" : 3
      } ]
    },
    "anims" : [ {
      "type" : "NODE",
      "node" : 1,
      "pos" : 0.1547776609659195
    } ]
  } ]
}