{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010417016,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 297, 103 ],
    "map" : {
      "width" : 266,
      "comment" : "Map\n-----------------------------------\nMaps a value from one range to another",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 103,
    "x" : 297
  }, {
    "id" : 1,
    "node" : "module.modules.math.MapValue",
    "name" : "Map",
    "label" : "Map.01",
    "gui" : true,
    "xy" : [ 461, 336 ],
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.36046511627906974,
      "color" : "0"
    }, {
      "name" : "Minimum In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Maximum In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Minimum Out",
      "type" : "Number",
      "value" : -10.0,
      "color" : "0"
    }, {
      "name" : "Maximum Out",
      "type" : "Number",
      "value" : 10.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 336,
    "x" : 461
  }, {
    "id" : 2,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 327, 266 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.36046511627906974,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 266,
    "x" : 327
  }, {
    "id" : 3,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02",
    "gui" : true,
    "xy" : [ 548, 274 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : -2.790697674418605,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 274,
    "x" : 548
  }, {
    "id" : 4,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.03",
    "gui" : true,
    "xy" : [ 327, 326 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 326,
    "x" : 327
  }, {
    "id" : 5,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.04",
    "gui" : true,
    "xy" : [ 327, 347 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 347,
    "x" : 327
  }, {
    "id" : 6,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.05",
    "gui" : true,
    "xy" : [ 327, 377 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : -10.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 377,
    "x" : 327
  }, {
    "id" : 7,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.06",
    "gui" : true,
    "xy" : [ 327, 398 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 10.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 398,
    "x" : 327
  }, {
    "id" : 8,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02",
    "gui" : true,
    "xy" : [ 234, 325 ],
    "map" : {
      "width" : 79,
      "comment" : "Input range",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 325,
    "x" : 234
  }, {
    "id" : 9,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy",
    "gui" : true,
    "xy" : [ 232, 378 ],
    "map" : {
      "width" : 83,
      "comment" : "Output range",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 378,
    "x" : 232
  }, {
    "id" : 10,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 255, 265 ],
    "map" : {
      "iteration" : 49.0,
      "position" : 0.36627906976744184
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.25,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 265,
    "x" : 255
  } ],
  "links" : [ {
    "nodeOut" : 1,
    "output" : "Out",
    "nodeIn" : 3,
    "input" : "In"
  }, {
    "nodeOut" : 10,
    "output" : "Phase",
    "nodeIn" : 2,
    "input" : "In"
  }, {
    "nodeOut" : 2,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "In"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Minimum In"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Maximum In"
  }, {
    "nodeOut" : 6,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Minimum Out"
  }, {
    "nodeOut" : 7,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Maximum Out"
  } ]
}