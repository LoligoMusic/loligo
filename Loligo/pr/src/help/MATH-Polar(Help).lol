{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010417016,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Polar",
    "name" : "Polar",
    "label" : "Polar.01",
    "gui" : true,
    "xy" : [ 315, 320 ],
    "inputs" : [ {
      "name" : "Length",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Angle",
      "type" : "Number",
      "value" : 0.24705882352941178,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "X",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 320,
    "x" : 315
  }, {
    "id" : 1,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 209, 302 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 302,
    "x" : 209
  }, {
    "id" : 2,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02",
    "gui" : true,
    "xy" : [ 209, 334 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.24705882352941178,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 334,
    "x" : 209
  }, {
    "id" : 3,
    "node" : "module.modules.math.Translate",
    "name" : "Translate",
    "label" : "Translate.01",
    "gui" : true,
    "xy" : [ 376, 321 ],
    "inputs" : [ {
      "name" : "X",
      "type" : "Number",
      "value" : 0.9998292504580527,
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "value" : 0.018478904959129915,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 321,
    "x" : 376
  }, {
    "id" : 4,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 629, 335 ],
    "map" : {
      "shape" : "ELLIPSE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 99.98292541503906, 0.0, 1.0, 1.8478904962539673 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFC81414"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 335,
    "x" : 629
  }, {
    "id" : 5,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 211, 156 ],
    "map" : {
      "width" : 255,
      "comment" : "Polar\n-----------------------------------\nConverts polar coordinates (length/angle) to cartesian (horizontal/vertical)",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 156,
    "x" : 211
  }, {
    "id" : 6,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 144, 349 ],
    "map" : {
      "iteration" : 51.0,
      "position" : 0.25882352941176473
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.48,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 349,
    "x" : 144
  } ],
  "links" : [ {
    "nodeOut" : 1,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "Length"
  }, {
    "nodeOut" : 2,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "Angle"
  }, {
    "nodeOut" : 0,
    "output" : "X",
    "nodeIn" : 3,
    "input" : "X"
  }, {
    "nodeOut" : 0,
    "output" : "Y",
    "nodeIn" : 3,
    "input" : "Y"
  }, {
    "nodeOut" : 3,
    "output" : "Transform",
    "nodeIn" : 4,
    "input" : "Transform"
  }, {
    "nodeOut" : 6,
    "output" : "Phase",
    "nodeIn" : 2,
    "input" : "In"
  } ]
}