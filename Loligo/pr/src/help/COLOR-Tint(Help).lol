{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010371202,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 2,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 255, 162 ],
    "map" : {
      "width" : 323,
      "comment" : "Tint\n---------------------------------------------\nMultiplies two colors",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 162,
    "x" : 255
  }, {
    "id" : 3,
    "node" : "module.modules.math.Color",
    "name" : "Color",
    "label" : "Color.01",
    "gui" : true,
    "xy" : [ 280, 296 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFD7E815"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 296,
    "x" : 280
  }, {
    "id" : 6,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 518, 305 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFD7E815"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 305,
    "x" : 518
  }, {
    "id" : 7,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy",
    "gui" : true,
    "xy" : [ 518, 405 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF661210"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 405,
    "x" : 518
  }, {
    "id" : 8,
    "node" : "module.modules.math.Tint",
    "name" : "Tint",
    "label" : "Tint.01",
    "gui" : true,
    "xy" : [ 357, 395 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFD7E815"
    }, {
      "name" : "Tint",
      "type" : "COLOR",
      "color" : "FF7914C8"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 395,
    "x" : 357
  }, {
    "id" : 9,
    "node" : "module.modules.math.Color",
    "name" : "Color",
    "label" : "Color.01-Copy",
    "gui" : true,
    "xy" : [ 281, 494 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF7914C8"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 494,
    "x" : 281
  }, {
    "id" : 10,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy.01",
    "gui" : true,
    "xy" : [ 518, 505 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF7914C8"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 505,
    "x" : 518
  } ],
  "links" : [ {
    "nodeOut" : 3,
    "output" : "Color",
    "nodeIn" : 6,
    "input" : "Color"
  }, {
    "nodeOut" : 3,
    "output" : "Color",
    "nodeIn" : 8,
    "input" : "Color"
  }, {
    "nodeOut" : 9,
    "output" : "Color",
    "nodeIn" : 8,
    "input" : "Tint"
  }, {
    "nodeOut" : 8,
    "output" : "Color",
    "nodeIn" : 7,
    "input" : "Color"
  }, {
    "nodeOut" : 9,
    "output" : "Color",
    "nodeIn" : 10,
    "input" : "Color"
  } ]
}