{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1007727141,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Maths",
    "name" : "+",
    "label" : "+.01",
    "gui" : true,
    "xy" : [ 430, 143 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 143,
    "x" : 430
  }, {
    "id" : 1,
    "node" : "module.modules.math.Maths",
    "name" : "*",
    "label" : "*.01",
    "gui" : true,
    "xy" : [ 431, 188 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 188,
    "x" : 431
  }, {
    "id" : 2,
    "node" : "module.modules.math.Maths",
    "name" : "-",
    "label" : "-.01",
    "gui" : true,
    "xy" : [ 430, 166 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 166,
    "x" : 430
  }, {
    "id" : 3,
    "node" : "module.modules.math.Maths",
    "name" : "/",
    "label" : "/.01",
    "gui" : true,
    "xy" : [ 431, 211 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 211,
    "x" : 431
  }, {
    "id" : 4,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 288, 203 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 203,
    "x" : 288
  }, {
    "id" : 5,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02",
    "gui" : true,
    "xy" : [ 288, 227 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 227,
    "x" : 288
  }, {
    "id" : 6,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.03",
    "gui" : true,
    "xy" : [ 501, 143 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 6.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 143,
    "x" : 501
  }, {
    "id" : 7,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.04",
    "gui" : true,
    "xy" : [ 501, 189 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 8.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 189,
    "x" : 501
  }, {
    "id" : 8,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.05",
    "gui" : true,
    "xy" : [ 501, 166 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 166,
    "x" : 501
  }, {
    "id" : 9,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.06",
    "gui" : true,
    "xy" : [ 501, 212 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 212,
    "x" : 501
  }, {
    "id" : 10,
    "node" : "module.modules.math.Maths",
    "name" : "AND",
    "label" : "AND.01",
    "gui" : true,
    "xy" : [ 436, 364 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 364,
    "x" : 436
  }, {
    "id" : 11,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.01",
    "gui" : true,
    "xy" : [ 292, 387 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 387,
    "x" : 292
  }, {
    "id" : 12,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.02",
    "gui" : true,
    "xy" : [ 292, 411 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 411,
    "x" : 292
  }, {
    "id" : 13,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03",
    "gui" : true,
    "xy" : [ 500, 365 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 365,
    "x" : 500
  }, {
    "id" : 14,
    "node" : "module.modules.math.Maths",
    "name" : "OR",
    "label" : "OR.01",
    "gui" : true,
    "xy" : [ 436, 387 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 387,
    "x" : 436
  }, {
    "id" : 15,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03-Copy",
    "gui" : true,
    "xy" : [ 500, 388 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 388,
    "x" : 500
  }, {
    "id" : 16,
    "node" : "module.modules.math.Maths",
    "name" : "NOT",
    "label" : "NOT.01",
    "gui" : true,
    "xy" : [ 436, 433 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 433,
    "x" : 436
  }, {
    "id" : 17,
    "node" : "module.modules.math.Maths",
    "name" : "XOR",
    "label" : "XOR.01",
    "gui" : true,
    "xy" : [ 436, 410 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 410,
    "x" : 436
  }, {
    "id" : 18,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03-Copy.01",
    "gui" : true,
    "xy" : [ 500, 411 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 411,
    "x" : 500
  }, {
    "id" : 19,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.05",
    "gui" : true,
    "xy" : [ 500, 434 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 434,
    "x" : 500
  }, {
    "id" : 20,
    "node" : "module.modules.math.Maths",
    "name" : "POW",
    "label" : "POW.01",
    "gui" : true,
    "xy" : [ 431, 234 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 234,
    "x" : 431
  }, {
    "id" : 21,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.07",
    "gui" : true,
    "xy" : [ 501, 235 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 16.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 235,
    "x" : 501
  }, {
    "id" : 22,
    "node" : "module.modules.math.Maths",
    "name" : "Abs",
    "label" : "Abs.01",
    "gui" : true,
    "xy" : [ 432, 257 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 257,
    "x" : 432
  }, {
    "id" : 23,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.08",
    "gui" : true,
    "xy" : [ 501, 258 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 258,
    "x" : 501
  }, {
    "id" : 24,
    "node" : "module.modules.math.Maths",
    "name" : "Modulo",
    "label" : "Modulo.01",
    "gui" : true,
    "xy" : [ 432, 280 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 280,
    "x" : 432
  }, {
    "id" : 25,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.09",
    "gui" : true,
    "xy" : [ 501, 281 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 281,
    "x" : 501
  }, {
    "id" : 26,
    "node" : "module.modules.math.Maths",
    "name" : ">",
    "label" : ">.01",
    "gui" : true,
    "xy" : [ 437, 470 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 3.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 470,
    "x" : 437
  }, {
    "id" : 27,
    "node" : "module.modules.math.Maths",
    "name" : ">=",
    "label" : ">=.01",
    "gui" : true,
    "xy" : [ 437, 493 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 3.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 493,
    "x" : 437
  }, {
    "id" : 28,
    "node" : "module.modules.math.Maths",
    "name" : "<",
    "label" : "<.01",
    "gui" : true,
    "xy" : [ 437, 516 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 3.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 516,
    "x" : 437
  }, {
    "id" : 29,
    "node" : "module.modules.math.Maths",
    "name" : "<=",
    "label" : "<=.01",
    "gui" : true,
    "xy" : [ 437, 539 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 3.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 539,
    "x" : 437
  }, {
    "id" : 30,
    "node" : "module.modules.math.Maths",
    "name" : "=",
    "label" : "=.01",
    "gui" : true,
    "xy" : [ 437, 562 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 3.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 562,
    "x" : 437
  }, {
    "id" : 31,
    "node" : "module.modules.math.Maths",
    "name" : "Max",
    "label" : "Max.01",
    "gui" : true,
    "xy" : [ 432, 303 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 303,
    "x" : 432
  }, {
    "id" : 32,
    "node" : "module.modules.math.Maths",
    "name" : "Min",
    "label" : "Min.01",
    "gui" : true,
    "xy" : [ 432, 326 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 326,
    "x" : 432
  }, {
    "id" : 33,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03-Copy.02",
    "gui" : true,
    "xy" : [ 500, 470 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 470,
    "x" : 500
  }, {
    "id" : 34,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03-Copy.03",
    "gui" : true,
    "xy" : [ 500, 493 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 493,
    "x" : 500
  }, {
    "id" : 35,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03-Copy.04",
    "gui" : true,
    "xy" : [ 500, 516 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 516,
    "x" : 500
  }, {
    "id" : 36,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03-Copy.05",
    "gui" : true,
    "xy" : [ 500, 539 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 539,
    "x" : 500
  }, {
    "id" : 37,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03-Copy.06",
    "gui" : true,
    "xy" : [ 500, 562 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 562,
    "x" : 500
  }, {
    "id" : 38,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01-Copy",
    "gui" : true,
    "xy" : [ 293, 507 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 3.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 507,
    "x" : 293
  }, {
    "id" : 39,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02-Copy",
    "gui" : true,
    "xy" : [ 293, 531 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 531,
    "x" : 293
  }, {
    "id" : 40,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.09-Copy",
    "gui" : true,
    "xy" : [ 501, 304 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 304,
    "x" : 501
  }, {
    "id" : 41,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.09-Copy.01",
    "gui" : true,
    "xy" : [ 501, 327 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 2.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 327,
    "x" : 501
  }, {
    "id" : 42,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 271, 79 ],
    "map" : {
      "width" : 331,
      "comment" : "Math Stuff\n----------------------------------------------",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 79,
    "x" : 271
  } ],
  "links" : [ {
    "nodeOut" : 1,
    "output" : "Out",
    "nodeIn" : 7,
    "input" : "In"
  }, {
    "nodeOut" : 27,
    "output" : "Out",
    "nodeIn" : 34,
    "input" : "In"
  }, {
    "nodeOut" : 17,
    "output" : "Out",
    "nodeIn" : 18,
    "input" : "In"
  }, {
    "nodeOut" : 3,
    "output" : "Out",
    "nodeIn" : 9,
    "input" : "In"
  }, {
    "nodeOut" : 22,
    "output" : "Out",
    "nodeIn" : 23,
    "input" : "In"
  }, {
    "nodeOut" : 11,
    "output" : "Out",
    "nodeIn" : 17,
    "input" : "In1"
  }, {
    "nodeOut" : 12,
    "output" : "Out",
    "nodeIn" : 17,
    "input" : "In2"
  }, {
    "nodeOut" : 32,
    "output" : "Out",
    "nodeIn" : 41,
    "input" : "In"
  }, {
    "nodeOut" : 20,
    "output" : "Out",
    "nodeIn" : 21,
    "input" : "In"
  }, {
    "nodeOut" : 28,
    "output" : "Out",
    "nodeIn" : 35,
    "input" : "In"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "In1"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "In2"
  }, {
    "nodeOut" : 11,
    "output" : "Out",
    "nodeIn" : 10,
    "input" : "In1"
  }, {
    "nodeOut" : 12,
    "output" : "Out",
    "nodeIn" : 10,
    "input" : "In2"
  }, {
    "nodeOut" : 38,
    "output" : "Out",
    "nodeIn" : 30,
    "input" : "In1"
  }, {
    "nodeOut" : 39,
    "output" : "Out",
    "nodeIn" : 30,
    "input" : "In2"
  }, {
    "nodeOut" : 38,
    "output" : "Out",
    "nodeIn" : 29,
    "input" : "In1"
  }, {
    "nodeOut" : 39,
    "output" : "Out",
    "nodeIn" : 29,
    "input" : "In2"
  }, {
    "nodeOut" : 38,
    "output" : "Out",
    "nodeIn" : 26,
    "input" : "In1"
  }, {
    "nodeOut" : 39,
    "output" : "Out",
    "nodeIn" : 26,
    "input" : "In2"
  }, {
    "nodeOut" : 12,
    "output" : "Out",
    "nodeIn" : 16,
    "input" : "In1"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 31,
    "input" : "In1"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 31,
    "input" : "In2"
  }, {
    "nodeOut" : 38,
    "output" : "Out",
    "nodeIn" : 27,
    "input" : "In1"
  }, {
    "nodeOut" : 39,
    "output" : "Out",
    "nodeIn" : 27,
    "input" : "In2"
  }, {
    "nodeOut" : 10,
    "output" : "Out",
    "nodeIn" : 13,
    "input" : "In"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 20,
    "input" : "In1"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 20,
    "input" : "In2"
  }, {
    "nodeOut" : 11,
    "output" : "Out",
    "nodeIn" : 14,
    "input" : "In1"
  }, {
    "nodeOut" : 12,
    "output" : "Out",
    "nodeIn" : 14,
    "input" : "In2"
  }, {
    "nodeOut" : 38,
    "output" : "Out",
    "nodeIn" : 28,
    "input" : "In1"
  }, {
    "nodeOut" : 39,
    "output" : "Out",
    "nodeIn" : 28,
    "input" : "In2"
  }, {
    "nodeOut" : 16,
    "output" : "Out",
    "nodeIn" : 19,
    "input" : "In"
  }, {
    "nodeOut" : 14,
    "output" : "Out",
    "nodeIn" : 15,
    "input" : "In"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 32,
    "input" : "In1"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 32,
    "input" : "In2"
  }, {
    "nodeOut" : 26,
    "output" : "Out",
    "nodeIn" : 33,
    "input" : "In"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 3,
    "input" : "In1"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 3,
    "input" : "In2"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "In1"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "In2"
  }, {
    "nodeOut" : 2,
    "output" : "Out",
    "nodeIn" : 8,
    "input" : "In"
  }, {
    "nodeOut" : 30,
    "output" : "Out",
    "nodeIn" : 37,
    "input" : "In"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 22,
    "input" : "In1"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "In1"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "In2"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 24,
    "input" : "In1"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 24,
    "input" : "In2"
  }, {
    "nodeOut" : 24,
    "output" : "Out",
    "nodeIn" : 25,
    "input" : "In"
  }, {
    "nodeOut" : 31,
    "output" : "Out",
    "nodeIn" : 40,
    "input" : "In"
  }, {
    "nodeOut" : 0,
    "output" : "Out",
    "nodeIn" : 6,
    "input" : "In"
  }, {
    "nodeOut" : 29,
    "output" : "Out",
    "nodeIn" : 36,
    "input" : "In"
  } ]
}