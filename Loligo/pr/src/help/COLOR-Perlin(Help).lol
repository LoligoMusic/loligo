{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010226407,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 223, 170 ],
    "map" : {
      "width" : 408,
      "comment" : "Perlin\n--------------------------------------------------\n2D Perlin-noise. Generates random values based on X/Y input.",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 170,
    "x" : 223
  }, {
    "id" : 1,
    "node" : "module.modules.math.Perlin",
    "name" : "Perlin",
    "label" : "Perlin.01",
    "gui" : true,
    "xy" : [ 304, 385 ],
    "inputs" : [ {
      "name" : "X",
      "type" : "Number",
      "value" : 48.82539682539682,
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "value" : 37.270718232044196,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 385,
    "x" : 304
  }, {
    "id" : 2,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 597, 370 ],
    "map" : {
      "shape" : "ELLIPSE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "F9BF1F8C"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 370,
    "x" : 597
  }, {
    "id" : 3,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 363, 385 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : -0.11304210479882827,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 385,
    "x" : 363
  }, {
    "id" : 4,
    "node" : "module.modules.math.HSB",
    "name" : "HSB",
    "label" : "HSB.01",
    "gui" : true,
    "xy" : [ 471, 383 ],
    "inputs" : [ {
      "name" : "Hue",
      "type" : "Number",
      "value" : -0.11304210479882827,
      "color" : "0"
    }, {
      "name" : "Saturation",
      "type" : "Number",
      "value" : 0.84,
      "color" : "0"
    }, {
      "name" : "Brightness",
      "type" : "Number",
      "value" : 0.75,
      "color" : "0"
    }, {
      "name" : "Alpha",
      "type" : "Number",
      "value" : 0.98,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 383,
    "x" : 471
  }, {
    "id" : 5,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 218, 369 ],
    "map" : {
      "iteration" : 48.0,
      "position" : 0.8333333333333334
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.36,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 369,
    "x" : 218
  }, {
    "id" : 6,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01-Copy",
    "gui" : true,
    "xy" : [ 217, 404 ],
    "map" : {
      "iteration" : 37.0,
      "position" : 0.27624309392265195
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.23,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 404,
    "x" : 217
  } ],
  "links" : [ {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "X"
  }, {
    "nodeOut" : 6,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Y"
  }, {
    "nodeOut" : 4,
    "output" : "Color",
    "nodeIn" : 2,
    "input" : "Color"
  }, {
    "nodeOut" : 3,
    "output" : "Out",
    "nodeIn" : 4,
    "input" : "Hue"
  }, {
    "nodeOut" : 1,
    "output" : "Out",
    "nodeIn" : 3,
    "input" : "In"
  } ]
}