{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1011287563,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Diff",
    "name" : "Diff",
    "label" : "Diff.01",
    "gui" : true,
    "xy" : [ 470, 325 ],
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.9682102673963412,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Diff",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 325,
    "x" : 470
  }, {
    "id" : 1,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02",
    "gui" : true,
    "xy" : [ 521, 325 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.00462087373977782,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 325,
    "x" : 521
  }, {
    "id" : 2,
    "node" : "module.modules.math.Maths",
    "name" : "Sin",
    "label" : "Sin.01",
    "gui" : true,
    "xy" : [ 323, 326 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 258.92857142857144,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 326,
    "x" : 323
  }, {
    "id" : 3,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.03",
    "gui" : true,
    "xy" : [ 376, 325 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.9682102673963412,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 325,
    "x" : 376
  }, {
    "id" : 4,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 249, 325 ],
    "map" : {
      "iteration" : 258.0,
      "position" : 0.9464285714285714
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.58,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 325,
    "x" : 249
  }, {
    "id" : 5,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 257, 216 ],
    "map" : {
      "width" : 323,
      "comment" : "Diff\n--------------------------------------------\nCalculates the difference of the current frame to the previous one.",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 216,
    "x" : 257
  } ],
  "links" : [ {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "In1"
  }, {
    "nodeOut" : 3,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "In"
  }, {
    "nodeOut" : 0,
    "output" : "Diff",
    "nodeIn" : 1,
    "input" : "In"
  }, {
    "nodeOut" : 2,
    "output" : "Out",
    "nodeIn" : 3,
    "input" : "In"
  } ]
}