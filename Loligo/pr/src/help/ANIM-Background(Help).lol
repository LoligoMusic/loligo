{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1007800052,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.anim.Background",
    "name" : "Background",
    "label" : "Background.01",
    "gui" : true,
    "xy" : [ 475, 359 ],
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF290D09"
    } ],
    "y" : 359,
    "x" : 475
  }, {
    "id" : 1,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 291, 275 ],
    "map" : {
      "width" : 307,
      "comment" : "Background\n------------------------------------------\nChanges the background color",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 275,
    "x" : 291
  }, {
    "id" : 2,
    "node" : "module.modules.math.HSB",
    "name" : "HSB",
    "label" : "HSB.01",
    "gui" : true,
    "xy" : [ 405, 359 ],
    "inputs" : [ {
      "name" : "Hue",
      "type" : "Number",
      "value" : 0.023026315789473683,
      "color" : "0"
    }, {
      "name" : "Saturation",
      "type" : "Number",
      "value" : 0.78,
      "color" : "0"
    }, {
      "name" : "Brightness",
      "type" : "Number",
      "value" : 0.16,
      "color" : "0"
    }, {
      "name" : "Alpha",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 359,
    "x" : 405
  }, {
    "id" : 3,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 325, 357 ],
    "map" : {
      "iteration" : 58.0,
      "position" : 0.02631578947368421
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 357,
    "x" : 325
  }, {
    "id" : 4,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 289, 419 ],
    "map" : {
      "width" : 308,
      "comment" : "------------------------------------------\n",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 419,
    "x" : 289
  } ],
  "links" : [ {
    "nodeOut" : 2,
    "output" : "Color",
    "nodeIn" : 0,
    "input" : "Color"
  }, {
    "nodeOut" : 3,
    "output" : "Phase",
    "nodeIn" : 2,
    "input" : "Hue"
  } ]
}