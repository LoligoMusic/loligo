{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010226407,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 210, 194 ],
    "map" : {
      "width" : 444,
      "comment" : "HSB / HSB-Split\n--------------------------------------------------------------\nJoins/splits a color-value from/to hue, brightness & saturation",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 194,
    "x" : 210
  }, {
    "id" : 1,
    "node" : "module.modules.math.HSB",
    "name" : "HSB",
    "label" : "HSB.01",
    "gui" : true,
    "xy" : [ 318, 293 ],
    "inputs" : [ {
      "name" : "Hue",
      "type" : "Number",
      "value" : 0.24,
      "color" : "0"
    }, {
      "name" : "Saturation",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Brightness",
      "type" : "Number",
      "value" : 0.94,
      "color" : "0"
    }, {
      "name" : "Alpha",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 293,
    "x" : 318
  }, {
    "id" : 2,
    "node" : "module.modules.math.RGB",
    "name" : "RGB",
    "label" : "RGB.01",
    "gui" : true,
    "xy" : [ 311, 515 ],
    "inputs" : [ {
      "name" : "Red",
      "type" : "Number",
      "value" : 0.5,
      "color" : "0"
    }, {
      "name" : "Greed",
      "type" : "Number",
      "value" : 0.97,
      "color" : "0"
    }, {
      "name" : "Blue",
      "type" : "Number",
      "value" : 0.1,
      "color" : "0"
    }, {
      "name" : "Alpha",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 515,
    "x" : 311
  }, {
    "id" : 3,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy.01",
    "gui" : true,
    "xy" : [ 210, 421 ],
    "map" : {
      "width" : 443,
      "comment" : "RGB / RGB-Split\n--------------------------------------------------------------\nJoins/splits a color-value from/to red, green & blue",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 421,
    "x" : 210
  }, {
    "id" : 4,
    "node" : "module.modules.math.Color",
    "name" : "Color",
    "label" : "Color.01",
    "gui" : true,
    "xy" : [ 405, 295 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF86F000"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 295,
    "x" : 405
  }, {
    "id" : 5,
    "node" : "module.modules.math.Color",
    "name" : "Color",
    "label" : "Color.01-Copy",
    "gui" : true,
    "xy" : [ 398, 517 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF7FF719"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 517,
    "x" : 398
  }, {
    "id" : 6,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 210, 491 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.5,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 491,
    "x" : 210
  }, {
    "id" : 7,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02",
    "gui" : true,
    "xy" : [ 210, 514 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.97,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 514,
    "x" : 210
  }, {
    "id" : 8,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.03",
    "gui" : true,
    "xy" : [ 210, 537 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.1,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 537,
    "x" : 210
  }, {
    "id" : 9,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01-Copy",
    "gui" : true,
    "xy" : [ 210, 268 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.24,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 268,
    "x" : 210
  }, {
    "id" : 10,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02-Copy",
    "gui" : true,
    "xy" : [ 210, 291 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 291,
    "x" : 210
  }, {
    "id" : 11,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.03-Copy",
    "gui" : true,
    "xy" : [ 210, 314 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.94,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 314,
    "x" : 210
  }, {
    "id" : 12,
    "node" : "module.modules.math.HSBSplit",
    "name" : "HSB-Split",
    "label" : "HSB-Split.01",
    "gui" : true,
    "xy" : [ 466, 295 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF86F000"
    } ],
    "outputs" : [ {
      "name" : "Hue",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Saturation",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Brightness",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 295,
    "x" : 466
  }, {
    "id" : 13,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01-Copy.01",
    "gui" : true,
    "xy" : [ 574, 273 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.24027776718139648,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 273,
    "x" : 574
  }, {
    "id" : 14,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02-Copy.01",
    "gui" : true,
    "xy" : [ 574, 296 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 296,
    "x" : 574
  }, {
    "id" : 15,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.03-Copy.01",
    "gui" : true,
    "xy" : [ 574, 319 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.9411764740943909,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 319,
    "x" : 574
  }, {
    "id" : 16,
    "node" : "module.modules.math.RGBSplit",
    "name" : "RGB-Split",
    "label" : "RGB-Split.01",
    "gui" : true,
    "xy" : [ 456, 518 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF7FF719"
    } ],
    "outputs" : [ {
      "name" : "Red",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Green",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Blue",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 518,
    "x" : 456
  }, {
    "id" : 17,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01-Copy.02",
    "gui" : true,
    "xy" : [ 569, 496 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.4980392156862745,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 496,
    "x" : 569
  }, {
    "id" : 18,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02-Copy.02",
    "gui" : true,
    "xy" : [ 569, 519 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.9686274509803922,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 519,
    "x" : 569
  }, {
    "id" : 19,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.03-Copy.02",
    "gui" : true,
    "xy" : [ 569, 542 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.09803921568627451,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 542,
    "x" : 569
  }, {
    "id" : 20,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 212, 117 ],
    "map" : {
      "width" : 442,
      "comment" : "Color-conversions\n--------------------------------------------------------------",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 117,
    "x" : 212
  } ],
  "links" : [ {
    "nodeOut" : 12,
    "output" : "Saturation",
    "nodeIn" : 14,
    "input" : "In"
  }, {
    "nodeOut" : 9,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Hue"
  }, {
    "nodeOut" : 10,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Saturation"
  }, {
    "nodeOut" : 11,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Brightness"
  }, {
    "nodeOut" : 12,
    "output" : "Brightness",
    "nodeIn" : 15,
    "input" : "In"
  }, {
    "nodeOut" : 4,
    "output" : "Color",
    "nodeIn" : 12,
    "input" : "Color"
  }, {
    "nodeOut" : 6,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "Red"
  }, {
    "nodeOut" : 7,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "Greed"
  }, {
    "nodeOut" : 8,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "Blue"
  }, {
    "nodeOut" : 16,
    "output" : "Red",
    "nodeIn" : 17,
    "input" : "In"
  }, {
    "nodeOut" : 1,
    "output" : "Color",
    "nodeIn" : 4,
    "input" : "Color"
  }, {
    "nodeOut" : 12,
    "output" : "Hue",
    "nodeIn" : 13,
    "input" : "In"
  }, {
    "nodeOut" : 5,
    "output" : "Color",
    "nodeIn" : 16,
    "input" : "Color"
  }, {
    "nodeOut" : 16,
    "output" : "Blue",
    "nodeIn" : 19,
    "input" : "In"
  }, {
    "nodeOut" : 2,
    "output" : "Color",
    "nodeIn" : 5,
    "input" : "Color"
  }, {
    "nodeOut" : 16,
    "output" : "Green",
    "nodeIn" : 18,
    "input" : "In"
  } ]
}