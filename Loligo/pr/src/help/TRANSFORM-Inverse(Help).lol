{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1007778943,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 556, 441 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.5873016119003296, -0.0, 0.0, -0.0, 0.9803921580314636, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF631AD9"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 441,
    "x" : 556
  }, {
    "id" : 1,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 257, 177 ],
    "map" : {
      "width" : 388,
      "comment" : "Inverse\n------------------------------------------------------\nInverts a transformation",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 177,
    "x" : 257
  }, {
    "id" : 2,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 256, 514 ],
    "map" : {
      "width" : 389,
      "comment" : "\n------------------------------------------------------\n",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 514,
    "x" : 256
  }, {
    "id" : 3,
    "node" : "module.modules.math.Inverse",
    "name" : "Inverse",
    "label" : "Inverse.01",
    "gui" : true,
    "xy" : [ 406, 426 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.6299999952316284, 0.0, 0.0, 0.0, 1.0199999809265137, 0.0 ],
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 426,
    "x" : 406
  }, {
    "id" : 4,
    "node" : "module.modules.math.Transform",
    "name" : "Transform",
    "label" : "Transform.01",
    "gui" : true,
    "xy" : [ 291, 290 ],
    "inputs" : [ {
      "name" : "X",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Y",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Scale X",
      "type" : "Number",
      "value" : 0.63,
      "color" : "0"
    }, {
      "name" : "Scale Y",
      "type" : "Number",
      "value" : 1.02,
      "color" : "0"
    }, {
      "name" : "Rotate",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 290,
    "x" : 291
  }, {
    "id" : 5,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01-Copy",
    "gui" : true,
    "xy" : [ 556, 308 ],
    "map" : {
      "shape" : "RECTANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.6299999952316284, 0.0, 0.0, 0.0, 1.0199999809265137, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF1AB7D9"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 308,
    "x" : 556
  } ],
  "links" : [ {
    "nodeOut" : 4,
    "output" : "Transform",
    "nodeIn" : 5,
    "input" : "Transform"
  }, {
    "nodeOut" : 3,
    "output" : "Transform",
    "nodeIn" : 0,
    "input" : "Transform"
  }, {
    "nodeOut" : 4,
    "output" : "Transform",
    "nodeIn" : 3,
    "input" : "Transform"
  } ]
}