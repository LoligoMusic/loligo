{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010218475,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 4,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.02",
    "gui" : true,
    "xy" : [ 670, 398 ],
    "map" : {
      "shape" : "ELLIPSE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFAF14C8"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 398,
    "x" : 670
  }, {
    "id" : 5,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 164, 266 ],
    "map" : {
      "iteration" : 125.0,
      "position" : 0.7616279069767442
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.25,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 266,
    "x" : 164
  }, {
    "id" : 6,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 232, 122 ],
    "map" : {
      "width" : 342,
      "comment" : "Path\n-----------------------------------------------\nCircular motion path. Add objects with drag&drop",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 122,
    "x" : 232
  }, {
    "id" : 7,
    "node" : "module.modules.anim.Path",
    "name" : "Path",
    "label" : "Path.01",
    "gui" : true,
    "xy" : [ 306, 290 ],
    "map" : {
      "mapping" : "WRAP"
    },
    "inputs" : [ {
      "name" : "Position",
      "type" : "Number",
      "value" : 125.75581395348837,
      "color" : "0"
    }, {
      "name" : "Tightness",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 290,
    "x" : 306
  }, {
    "id" : 8,
    "node" : "module.modules.anim.Segment",
    "name" : "Segment",
    "label" : "Segment.01",
    "gui" : true,
    "xy" : [ 370, 540 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Resolution",
      "type" : "Number",
      "value" : 6.0,
      "color" : "0"
    }, {
      "name" : "Sector",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Inner radius",
      "type" : "Number",
      "value" : 0.56,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF8EC814"
    } ],
    "y" : 540,
    "x" : 370
  }, {
    "id" : 11,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 490, 360 ],
    "map" : {
      "shape" : "TRIANGLE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF1461C8"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 360,
    "x" : 490
  } ],
  "links" : [ {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 7,
    "input" : "Position"
  } ],
  "paths" : [ {
    "@id" : 1,
    "node" : 7,
    "constraint" : {
      "vertices" : [ {
        "@id" : 2,
        "x" : 10.0,
        "y" : 10.0
      }, {
        "@id" : 3,
        "x" : 12.0,
        "y" : 257.0
      }, {
        "@id" : 4,
        "x" : 182.0,
        "y" : 67.0
      }, {
        "@id" : 5,
        "x" : 163.0,
        "y" : 219.0
      } ],
      "paths" : [ {
        "subSegments" : [ {
          "segmentType" : "CURVE",
          "vertex1" : 2,
          "vertex2" : 4
        }, {
          "segmentType" : "CURVE",
          "vertex1" : 4,
          "vertex2" : 5
        }, {
          "segmentType" : "CURVE",
          "vertex1" : 5,
          "vertex2" : 3
        } ],
        "segmentType" : "PATH",
        "vertex1" : 2,
        "vertex2" : 3
      } ]
    },
    "anims" : [ {
      "type" : "NODE",
      "node" : 8,
      "pos" : 0.13902169466018677
    }, {
      "type" : "NODE",
      "node" : 11,
      "pos" : 0.6173433065414429
    } ]
  } ]
}