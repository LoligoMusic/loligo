{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1011788460,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.01",
    "gui" : true,
    "xy" : [ 501, 169 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 169,
    "x" : 501
  }, {
    "id" : 1,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.02",
    "gui" : true,
    "xy" : [ 492, 381 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 381,
    "x" : 492
  }, {
    "id" : 2,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 275, 95 ],
    "map" : {
      "width" : 366,
      "comment" : "FlipFlop\n------------------------------------------------\nInput 'Set' turns output to One, 'Reset' turns it back to Zero.",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 95,
    "x" : 275
  }, {
    "id" : 3,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 277, 290 ],
    "map" : {
      "width" : 366,
      "comment" : "Toggle\n-----------------------------------------------\nInput 'Set' turns output to One if it's Zero and Zero if it's One. 'Reset' turns it back to Zero.",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 290,
    "x" : 277
  }, {
    "id" : 4,
    "node" : "module.modules.math.Toggle",
    "name" : "Toggle",
    "label" : "Toggle.01",
    "gui" : true,
    "xy" : [ 409, 382 ],
    "inputs" : [ {
      "name" : "Set",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 382,
    "x" : 409
  }, {
    "id" : 5,
    "node" : "module.modules.math.FlipFlop",
    "name" : "FlipFlop",
    "label" : "FlipFlop.01",
    "gui" : true,
    "xy" : [ 403, 169 ],
    "inputs" : [ {
      "name" : "Set",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 169,
    "x" : 403
  }, {
    "id" : 6,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Bang.01",
    "gui" : true,
    "xy" : [ 301, 156 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "BANG"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 156,
    "x" : 301
  }, {
    "id" : 7,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Bang.01-Copy",
    "gui" : true,
    "xy" : [ 301, 183 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "BANG"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 183,
    "x" : 301
  }, {
    "id" : 8,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Bang.01-Copy.02",
    "gui" : true,
    "xy" : [ 301, 364 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "BANG"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 364,
    "x" : 301
  }, {
    "id" : 9,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Bang.01-Copy.01",
    "gui" : true,
    "xy" : [ 301, 390 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "BANG"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 390,
    "x" : 301
  }, {
    "id" : 10,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02",
    "gui" : true,
    "xy" : [ 187, 172 ],
    "map" : {
      "width" : 93,
      "comment" : "Right-Click ->",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 172,
    "x" : 187
  }, {
    "id" : 11,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy",
    "gui" : true,
    "xy" : [ 192, 365 ],
    "map" : {
      "width" : 93,
      "comment" : "Right-Click ->",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 365,
    "x" : 192
  }, {
    "id" : 12,
    "node" : "module.modules.math.TogEdge",
    "name" : "TogEdge",
    "label" : "TogEdge.01",
    "gui" : true,
    "xy" : [ 395, 571 ],
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Up",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Down",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 571,
    "x" : 395
  }, {
    "id" : 13,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.03",
    "gui" : true,
    "xy" : [ 511, 558 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 558,
    "x" : 511
  }, {
    "id" : 14,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.04",
    "gui" : true,
    "xy" : [ 511, 584 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 584,
    "x" : 511
  }, {
    "id" : 15,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.05",
    "gui" : true,
    "xy" : [ 301, 569 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 569,
    "x" : 301
  }, {
    "id" : 16,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy.01",
    "gui" : true,
    "xy" : [ 280, 496 ],
    "map" : {
      "width" : 366,
      "comment" : "TogEdge\n-----------------------------------------------\nTriggers if the input turns from One to Zero or Zero to One ",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 496,
    "x" : 280
  }, {
    "id" : 17,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.02-Copy.01",
    "gui" : true,
    "xy" : [ 199, 570 ],
    "map" : {
      "width" : 93,
      "comment" : "Right-Click ->",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 570,
    "x" : 199
  } ],
  "links" : [ {
    "nodeOut" : 8,
    "output" : "Out",
    "nodeIn" : 4,
    "input" : "Set"
  }, {
    "nodeOut" : 9,
    "output" : "Out",
    "nodeIn" : 4,
    "input" : "Reset"
  }, {
    "nodeOut" : 15,
    "output" : "Out",
    "nodeIn" : 12,
    "input" : "In"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "In"
  }, {
    "nodeOut" : 12,
    "output" : "Up",
    "nodeIn" : 13,
    "input" : "In"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 0,
    "input" : "In"
  }, {
    "nodeOut" : 6,
    "output" : "Out",
    "nodeIn" : 5,
    "input" : "Set"
  }, {
    "nodeOut" : 7,
    "output" : "Out",
    "nodeIn" : 5,
    "input" : "Reset"
  }, {
    "nodeOut" : 12,
    "output" : "Down",
    "nodeIn" : 14,
    "input" : "In"
  } ]
}