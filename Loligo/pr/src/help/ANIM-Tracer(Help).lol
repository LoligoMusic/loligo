{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1008664584,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.anim.Tracer",
    "name" : "Tracer",
    "label" : "Tracer.01",
    "gui" : true,
    "xy" : [ 384, 345 ],
    "inputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF14C8C7"
    }, {
      "name" : "Weight",
      "type" : "Number",
      "value" : 0.38,
      "color" : "0"
    }, {
      "name" : "Length",
      "type" : "Number",
      "value" : 0.94,
      "color" : "0"
    } ],
    "y" : 345,
    "x" : 384
  }, {
    "id" : 1,
    "node" : "module.modules.anim.RingPath",
    "name" : "RingPath",
    "label" : "RingPath.01",
    "gui" : true,
    "xy" : [ 526, 392 ],
    "inputs" : [ {
      "name" : "Radius",
      "type" : "Number",
      "value" : 2.29,
      "color" : "0"
    }, {
      "name" : "Position",
      "type" : "Number",
      "value" : 56.73101694915255,
      "color" : "0"
    } ],
    "y" : 392,
    "x" : 526
  }, {
    "id" : 2,
    "node" : "module.modules.anim.RingPath",
    "name" : "RingPath",
    "label" : "RingPath.01-Copy",
    "gui" : true,
    "xy" : [ 441, 315 ],
    "inputs" : [ {
      "name" : "Radius",
      "type" : "Number",
      "value" : 1.3,
      "color" : "0"
    }, {
      "name" : "Position",
      "type" : "Number",
      "value" : 333.7118644067797,
      "color" : "0"
    } ],
    "y" : 315,
    "x" : 441
  }, {
    "id" : 3,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 210, 342 ],
    "map" : {
      "iteration" : 333.0,
      "position" : 0.7288135593220338
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.57,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 342,
    "x" : 210
  }, {
    "id" : 4,
    "node" : "module.modules.math.Maths",
    "name" : "*",
    "label" : "*.01",
    "gui" : true,
    "xy" : [ 291, 389 ],
    "inputs" : [ {
      "name" : "In1",
      "type" : "Number",
      "value" : 333.7118644067797,
      "color" : "0"
    }, {
      "name" : "In2",
      "type" : "Number",
      "value" : 0.17,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 389,
    "x" : 291
  }, {
    "id" : 5,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.01",
    "gui" : true,
    "xy" : [ 119, 377 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 377,
    "x" : 119
  }, {
    "id" : 6,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 289, 137 ],
    "map" : {
      "width" : 324,
      "comment" : "Tracer\n------------------------------------------\nDraws a line along its path",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 137,
    "x" : 289
  } ],
  "links" : [ {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 3,
    "input" : "Switch"
  }, {
    "nodeOut" : 3,
    "output" : "Out",
    "nodeIn" : 2,
    "input" : "Position"
  }, {
    "nodeOut" : 3,
    "output" : "Out",
    "nodeIn" : 4,
    "input" : "In1"
  }, {
    "nodeOut" : 4,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Position"
  } ],
  "paths" : [ {
    "@id" : 1,
    "node" : 1,
    "anims" : [ {
      "type" : "NODE",
      "node" : 2,
      "pos" : 0.3636510670185089
    } ]
  }, {
    "@id" : 2,
    "node" : 2,
    "anims" : [ {
      "type" : "NODE",
      "node" : 0,
      "pos" : 0.5408728122711182
    } ]
  } ]
}