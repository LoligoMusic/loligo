{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1007831076,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.anim.Segment",
    "name" : "Segment",
    "label" : "Segment.01",
    "gui" : true,
    "xy" : [ 315, 296 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Resolution",
      "type" : "Number",
      "value" : 6.0,
      "color" : "0"
    }, {
      "name" : "Sector",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Inner radius",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF278C76"
    } ],
    "y" : 296,
    "x" : 315
  }, {
    "id" : 1,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 246, 162 ],
    "map" : {
      "width" : 342,
      "comment" : "Segment\n-----------------------------------------------\nDraws an n-gon",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 162,
    "x" : 246
  }, {
    "id" : 2,
    "node" : "module.modules.anim.Segment",
    "name" : "Segment",
    "label" : "Segment.01-Copy",
    "gui" : true,
    "xy" : [ 381, 404 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Resolution",
      "type" : "Number",
      "value" : 6.0,
      "color" : "0"
    }, {
      "name" : "Sector",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Inner radius",
      "type" : "Number",
      "value" : 0.66,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF8C276E"
    } ],
    "y" : 404,
    "x" : 381
  }, {
    "id" : 3,
    "node" : "module.modules.anim.Segment",
    "name" : "Segment",
    "label" : "Segment.01-Copy.01",
    "gui" : true,
    "xy" : [ 484, 291 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Resolution",
      "type" : "Number",
      "value" : 3.0,
      "color" : "0"
    }, {
      "name" : "Sector",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Inner radius",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF8C2727"
    } ],
    "y" : 291,
    "x" : 484
  }, {
    "id" : 4,
    "node" : "module.modules.anim.Segment",
    "name" : "Segment",
    "label" : "Segment.01-Copy.02",
    "gui" : true,
    "xy" : [ 544, 399 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Resolution",
      "type" : "Number",
      "value" : 10.0,
      "color" : "0"
    }, {
      "name" : "Sector",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Inner radius",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF91421A"
    } ],
    "y" : 399,
    "x" : 544
  }, {
    "id" : 5,
    "node" : "module.modules.anim.Segment",
    "name" : "Segment",
    "label" : "Segment.01-Copy.03",
    "gui" : true,
    "xy" : [ 502, 496 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Resolution",
      "type" : "Number",
      "value" : 10.0,
      "color" : "0"
    }, {
      "name" : "Sector",
      "type" : "Number",
      "value" : 0.25,
      "color" : "0"
    }, {
      "name" : "Inner radius",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF319D10"
    } ],
    "y" : 496,
    "x" : 502
  }, {
    "id" : 6,
    "node" : "module.modules.anim.Segment",
    "name" : "Segment",
    "label" : "Segment.01-Copy.04",
    "gui" : true,
    "xy" : [ 311, 507 ],
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Resolution",
      "type" : "Number",
      "value" : 4.0,
      "color" : "0"
    }, {
      "name" : "Sector",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Inner radius",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF101C9D"
    } ],
    "y" : 507,
    "x" : 311
  }, {
    "id" : 7,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 245, 588 ],
    "map" : {
      "width" : 342,
      "comment" : "-----------------------------------------------\n",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 588,
    "x" : 245
  } ]
}