{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010226407,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 245, 173 ],
    "map" : {
      "width" : 408,
      "comment" : "Send & Receive\n--------------------------------------------------\nTransmit values wherever you "want" :\n- Create Send\n- In the inspector, set a name for it\n- Create Receive somewhere else (different patch also works) \n- In the inspector, pick the name of your Send Node",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 173,
    "x" : 245
  }, {
    "id" : 1,
    "node" : "module.modules.math.SendReceive$Send",
    "name" : "Send",
    "label" : "Send.01",
    "gui" : true,
    "xy" : [ 450, 349 ],
    "map" : {
      "sendername" : "myThing"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "UNIVERSAL",
      "dynType" : "Number",
      "value" : 7.0,
      "color" : "0"
    } ],
    "y" : 349,
    "x" : 450
  }, {
    "id" : 2,
    "node" : "module.modules.math.SendReceive$Receive",
    "name" : "Receive",
    "label" : "Receive.01",
    "gui" : true,
    "xy" : [ 452, 387 ],
    "map" : {
      "sendername" : "myThing"
    },
    "outputs" : [ {
      "name" : "Value",
      "type" : "UNIVERSAL",
      "color" : "0"
    } ],
    "y" : 387,
    "x" : 452
  }, {
    "id" : 3,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.01",
    "gui" : true,
    "xy" : [ 339, 348 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 7.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 348,
    "x" : 339
  }, {
    "id" : 4,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Number.02",
    "gui" : true,
    "xy" : [ 551, 387 ],
    "map" : {
      "range" : [ "[D", [ "-Infinity", "Infinity" ] ],
      "type" : "DECIMAL"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 7.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 387,
    "x" : 551
  }, {
    "id" : 5,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 267, 335 ],
    "map" : {
      "iteration" : 7.0,
      "position" : 0.8333333333333334
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.67,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 335,
    "x" : 267
  } ],
  "links" : [ {
    "nodeOut" : 3,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "In"
  }, {
    "nodeOut" : 5,
    "output" : "Count",
    "nodeIn" : 3,
    "input" : "In"
  }, {
    "nodeOut" : 2,
    "output" : "Value",
    "nodeIn" : 4,
    "input" : "In"
  } ]
}