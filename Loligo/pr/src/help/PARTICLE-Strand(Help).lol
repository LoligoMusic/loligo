{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1010226407,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 218, 116 ],
    "map" : {
      "width" : 366,
      "comment" : "Strand\n--------------------------------------------------\nA strand is a particle trajectory drawn all at once. Thus, it's subject to particle-modifiers like Drag and Attract.",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 116,
    "x" : 218
  }, {
    "id" : 1,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 180, 388 ],
    "map" : {
      "iteration" : 474.0,
      "position" : 0.6507936507936508
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.36,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 388,
    "x" : 180
  }, {
    "id" : 2,
    "node" : "module.modules.particles.Strand",
    "name" : "Strand",
    "label" : "Strand.01",
    "gui" : true,
    "xy" : [ 399, 544 ],
    "inputs" : [ {
      "name" : "Angle",
      "type" : "Number",
      "value" : 0.56,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.32,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFB2C814"
    }, {
      "name" : "Weight",
      "type" : "Number",
      "value" : 0.26,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Length",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Crook",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 544,
    "x" : 399
  }, {
    "id" : 3,
    "node" : "module.modules.anim.RingPath",
    "name" : "RingPath",
    "label" : "RingPath.01",
    "gui" : true,
    "xy" : [ 415, 394 ],
    "inputs" : [ {
      "name" : "Radius",
      "type" : "Number",
      "value" : 1.17,
      "color" : "0"
    }, {
      "name" : "Position",
      "type" : "Number",
      "value" : 0.6428571428571429,
      "color" : "0"
    } ],
    "y" : 394,
    "x" : 415
  }, {
    "id" : 4,
    "node" : "module.DefaultParticleModifierModule",
    "name" : "Attractor",
    "label" : "Attractor.01",
    "gui" : true,
    "xy" : [ 418, 452 ],
    "inputs" : [ {
      "name" : "Force",
      "type" : "Number",
      "value" : 1.95,
      "color" : "0"
    }, {
      "name" : "Power",
      "type" : "Number",
      "value" : 0.82,
      "color" : "0"
    }, {
      "name" : "Distance",
      "type" : "Number",
      "value" : 1.53,
      "color" : "0"
    } ],
    "y" : 452,
    "x" : 418
  }, {
    "id" : 5,
    "node" : "module.modules.math.Value",
    "name" : "Number",
    "label" : "Boolean.01",
    "gui" : true,
    "xy" : [ 56, 384 ],
    "map" : {
      "range" : [ "[D", [ 0.0, 1.0 ] ],
      "type" : "TOGGLE"
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 384,
    "x" : 56
  }, {
    "id" : 6,
    "node" : "module.modules.particles.Strand",
    "name" : "Strand",
    "label" : "Strand.02",
    "gui" : true,
    "xy" : [ 554, 346 ],
    "inputs" : [ {
      "name" : "Angle",
      "type" : "Number",
      "value" : 0.8,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.3,
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FF14C8C5"
    }, {
      "name" : "Weight",
      "type" : "Number",
      "value" : 0.18,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Length",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Crook",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 346,
    "x" : 554
  }, {
    "id" : 7,
    "node" : "module.DefaultParticleModifierModule",
    "name" : "Drag",
    "label" : "Drag.01",
    "gui" : true,
    "xy" : [ 423, 336 ],
    "inputs" : [ {
      "name" : "Amount",
      "type" : "Number",
      "value" : 0.1,
      "color" : "0"
    }, {
      "name" : "Distance",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    } ],
    "y" : 336,
    "x" : 423
  } ],
  "links" : [ {
    "nodeOut" : 1,
    "output" : "Phase",
    "nodeIn" : 3,
    "input" : "Position"
  }, {
    "nodeOut" : 5,
    "output" : "Out",
    "nodeIn" : 1,
    "input" : "Switch"
  } ],
  "paths" : [ {
    "@id" : 1,
    "node" : 3,
    "anims" : [ {
      "type" : "NODE",
      "node" : 4,
      "pos" : 0.6513365388862671
    }, {
      "type" : "NODE",
      "node" : 7,
      "pos" : 1.118542856640286
    } ]
  } ]
}