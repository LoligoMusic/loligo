{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1012118956,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.Graph",
    "name" : "Graph",
    "label" : "Graph.01",
    "gui" : true,
    "xy" : [ 349, 318 ],
    "map" : {
      "vertices" : [ "[Ljava.lang.Object;", [ 0.0, 0.0, 0.13, 0.74, 0.23666666666666666, 0.20999999999999996, 0.4066666666666667, 0.37, 0.43666666666666665, 0.77, 0.48, 0.39, 0.72, 0.27, 0.8233333333333334, 1.0, 1.0, 0.0 ] ]
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.8443579766536965,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 318,
    "x" : 349
  }, {
    "id" : 1,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 286, 318 ],
    "map" : {
      "iteration" : 108.0,
      "position" : 0.8482490272373541
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.08,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 318,
    "x" : 286
  }, {
    "id" : 3,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 517, 378 ],
    "map" : {
      "shape" : "ELLIPSE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 0.880992591381073, 0.0, 0.0, 0.0, 0.880992591381073, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFC81414"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 378,
    "x" : 517
  }, {
    "id" : 5,
    "node" : "module.modules.math.Scale",
    "name" : "Scale",
    "label" : "Scale.01",
    "gui" : true,
    "xy" : [ 424, 317 ],
    "inputs" : [ {
      "name" : "Scale",
      "type" : "Number",
      "value" : 0.8809925849790766,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "color" : "0"
    } ],
    "y" : 317,
    "x" : 424
  }, {
    "id" : 6,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 265, 205 ],
    "map" : {
      "width" : 318,
      "comment" : "Graph\n-------------------------------------------\nMaps input to a graph",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 205,
    "x" : 265
  } ],
  "links" : [ {
    "nodeOut" : 5,
    "output" : "Transform",
    "nodeIn" : 3,
    "input" : "Transform"
  }, {
    "nodeOut" : 0,
    "output" : "Out",
    "nodeIn" : 5,
    "input" : "Scale"
  }, {
    "nodeOut" : 1,
    "output" : "Phase",
    "nodeIn" : 0,
    "input" : "In"
  } ]
}