{
  "version" : 1,
  "saveTarget" : "FILE",
  "id" : 1007831076,
  "width" : 900,
  "height" : 700,
  "nodes" : [ {
    "id" : 0,
    "node" : "module.modules.math.ColorRamp",
    "name" : "ColorRamp",
    "label" : "ColorRamp.01",
    "gui" : true,
    "xy" : [ 361, 345 ],
    "map" : {
      "gradient" : [ "[[Ljava.lang.Object;", [ [ 0.0, "ff167ba6" ], [ 0.3199999928474426, "fff2f70f" ], [ 0.8333333134651184, "ffff0a0a" ], [ 1.0, "ff167ba6" ] ] ]
    },
    "inputs" : [ {
      "name" : "In",
      "type" : "Number",
      "value" : 0.7920792079207921,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "0"
    } ],
    "y" : 345,
    "x" : 361
  }, {
    "id" : 1,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01",
    "gui" : true,
    "xy" : [ 268, 212 ],
    "map" : {
      "width" : 351,
      "comment" : "ColorRamp\n------------------------------------------------\nPicks a color from a gradient",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 212,
    "x" : 268
  }, {
    "id" : 2,
    "node" : "module.modules.anim.Shape",
    "name" : "Shape",
    "label" : "Shape.01",
    "gui" : true,
    "xy" : [ 541, 356 ],
    "map" : {
      "shape" : "ELLIPSE",
      "blendMode" : "DEFAULT"
    },
    "inputs" : [ {
      "name" : "Transform",
      "type" : "TRANSFORM",
      "value" : [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0 ],
      "color" : "0"
    }, {
      "name" : "Color",
      "type" : "COLOR",
      "color" : "FFFD1E0B"
    }, {
      "name" : "Blend",
      "type" : "ENUM",
      "value" : 0.0,
      "color" : "0"
    } ],
    "y" : 356,
    "x" : 541
  }, {
    "id" : 3,
    "node" : "module.modules.math.Clock",
    "name" : "Clock",
    "label" : "Clock.01",
    "gui" : true,
    "xy" : [ 295, 344 ],
    "map" : {
      "iteration" : 76.0,
      "position" : 0.801980198019802
    },
    "inputs" : [ {
      "name" : "Switch",
      "type" : "Number",
      "value" : 1.0,
      "color" : "0"
    }, {
      "name" : "Speed",
      "type" : "Number",
      "value" : 0.43,
      "color" : "0"
    }, {
      "name" : "Reset",
      "type" : "Number",
      "value" : 0.0,
      "color" : "0"
    } ],
    "outputs" : [ {
      "name" : "Phase",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Count",
      "type" : "Number",
      "color" : "0"
    }, {
      "name" : "Out",
      "type" : "Number",
      "color" : "0"
    } ],
    "y" : 344,
    "x" : 295
  }, {
    "id" : 4,
    "node" : "module.modules.math.Comment",
    "name" : "Comment",
    "label" : "Comment.01-Copy",
    "gui" : true,
    "xy" : [ 267, 440 ],
    "map" : {
      "width" : 354,
      "comment" : "------------------------------------------------",
      "fontSize" : 12,
      "fontColor" : -1
    },
    "y" : 440,
    "x" : 267
  } ],
  "links" : [ {
    "nodeOut" : 3,
    "output" : "Phase",
    "nodeIn" : 0,
    "input" : "In"
  }, {
    "nodeOut" : 0,
    "output" : "Color",
    "nodeIn" : 2,
    "input" : "Color"
  } ]
}