package patch;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.System.currentTimeMillis;

import java.net.URL;
import java.util.function.Consumer;

import audio.AudioOut;
import constraints.ConstraintContext;
import module.ModuleContainer;
import module.ModuleManager;
import module.loading.AbstractNodeRegistry;
import module.loading.NodeEntry;
import module.signalFlow.SignalFlowIF;
import save.PatchData;
import save.SaveDataContext;


public interface Domain extends ModuleContainer, GuiDomain {
	
	public SignalFlowIF getSignalFlow();
	public AudioOut getAudioManager();
	public ModuleManager getModuleManager();
	public ConstraintContext getConstraintContext();
	
	public AbstractNodeRegistry<? extends NodeEntry> getNodeRegistry();
	boolean isProcessActive();
	public int getDomainID();
	public void setDomainID(int id);
	public int getNewModuleID();
	public void setCurrentModuleID(int id);
	public int getCurrentModuleID();
	public void doDeserialize(PatchData data, SaveDataContext sdc);
	
	default URL getLocation() {return null;}
	default void setLocation(URL f) {}
	
	
	// -------------- STATIC -------------- 
	
	public static Domain iteratePatchParents(Domain dom, Consumer<Domain> cons) {
		
		while(true){
			cons.accept(dom);
			var d = dom.getParentDomain();
			if(d == null || d == dom) 
				return dom;
			dom = d;
		} 
	}
	
	public static GuiDomain getBaseDomain(GuiDomain dom) {
		while(true){
			var d = dom.getParentDomain();
			if(d == null || d == dom) 
				return dom;
			dom = d;
		} 
	}
	
	public static int createDomainID() {
		
		return (int) ((currentTimeMillis() / 100l) % (long) MAX_VALUE);
	}
	
}
