package patch;

public enum DomainType {

	MAIN,
	PARTICLE_FACTORY,
	PARTICLE_INSTANCE,
	SUBPATCH,
	GUI;
}
