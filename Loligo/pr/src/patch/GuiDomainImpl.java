package patch;

import static constants.DisplayFlag.DRAGABLE;
import constants.GuiDomainEvent;
import constants.GuiDomainEventManager;
import errors.ErrorHandler;
import gui.selection.GuiType;
import history.GuiHistory;
import lombok.Data;
import lombok.Getter;
import lombok.var;
import pr.DisplayManagerIF;
import pr.DisplayObjectIF;
import pr.MinimalDisplayManager;
import pr.userinput.UserInput;

public class GuiDomainImpl implements GuiDomain {

	@Data
	public static class Builder {
		
		Domain parent;
		DisplayObjectIF gui;
		int initialWidth, initialHeight;
		String title;
		
		public void setInitialWH(int w, int h) {
			initialWidth = w;
			initialHeight = h;
		}
		
		public GuiDomainImpl build() {
			
			var domain = new GuiDomainImpl(parent);
			domain.init(initialWidth, initialHeight);
			
			var proc = domain.getPApplet();
			
			proc.setTitle(title);
			
			proc.setWindowSize(gui.getWidth(), gui.getHeight());
			//proc.getWindow().setUndecorated(true);
			
			var dm = domain.getDisplayManager();
			
			gui.setFlag(DRAGABLE, false);
			gui.setGuiType(GuiType.NO_SELECT);
			
			dm.addGuiObject(gui);
			dm.updateDisplayList();
			dm.addedToDisplay();
			
			/*
			EventSubscriber es = (e, in) -> {
				if(in.isCurrentTarget(gui, InputEvent.DRAG)) {
					var p = java.awt.MouseInfo.getPointerInfo().getLocation();
					System.out.println(p.x + " " + p.y);
					//var pp = proc.getWindow().convertToWindowUnits(new int[] {p.x, p.y});
					//proc.getWindow().setPosition(pp[0], pp[1]);
					var w = proc.getWindow();
					w.setPosition(w.getX() + in.getMouseDeltaX(), w.getY() + in.getMouseDeltaY());
					
				}
			};
			dm.getInput().addInputEventListener(es);
			*/
			
			return domain;
		}
 		
	}
	
	
	public static GuiDomainImpl createFor(Domain parent, DisplayObjectIF gui, String title) {
		
		var domain = new GuiDomainImpl(parent);
		domain.init(50, 50);
		
		var pr = domain.getPApplet();
		pr.setTitle(title);
		pr.setWindowSize(gui.getWidth(), gui.getHeight());
		
		var dm = domain.getDisplayManager();
		
		gui.setFlag(DRAGABLE, false);
		
		dm.addGuiObject(gui);
		dm.updateDisplayList();
		dm.addedToDisplay();
		
		return domain;
	}
	
	
	
	private final Domain parent;
	@Getter
	private final Loligo pApplet;
	@Getter
	private final DisplayManagerIF displayManager;
	@Getter
	private final GuiHistory history;
	
	
	public GuiDomainImpl(Domain parent) {
		
		this.parent = parent;
		this.displayManager = new MinimalDisplayManager(this);
		this.history = new GuiHistory(this);
		
		UserInput ui = new UserInput();
		displayManager.setInput(ui);
		
		this.pApplet = new Loligo();
		//pApplet.setRenderer(PConstants.JAVA2D);
		ui.init(this, displayManager, pApplet, true);
	}
	 
	public void init(int w, int h) {
		
		pApplet.initialWH(w, h);
		
		pApplet.startSketch();
		pApplet.setWindowDestroyNotification(this::onDispose);
		pApplet.setWindowDestroyListener(this::onDestroy);
		pApplet.setWindowFocusListener(this::onFocus);
		
		pApplet.setTitle("Window");
		displayManager.initGraphics(pApplet.g);
		pApplet.registerDisplay(displayManager);
		
		history.init();
		
		GuiDomainEventManager.instance.dispatchGuiDomainEvent(this, GuiDomainEvent.Type.CREATED);
		
		/*
		var ml = new MouseAdapter() {
		
			@Override
			public void mouseDragged(MouseEvent e) {
				var pp = pApplet.getWindow().convertToWindowUnits(new int[] {e.getX(), e.getY()});
				pApplet.getWindow().setPosition(pp[0], pp[1]);
			}
			
		};
		pApplet.getWindow().addMouseListener(ml);
		*/
	}

	@Override
	public DomainType domainType() {
		return DomainType.GUI;
	}

	@Override
	public ErrorHandler getErrorHandler() {
		return null;
	}

	@Override
	public void reset() {
		
	}
	
	private void onFocus(boolean focus) {
		var t = focus ? GuiDomainEvent.Type.FOCUS_GAINED : GuiDomainEvent.Type.FOCUS_LOST;
		GuiDomainEventManager.instance.dispatchGuiDomainEvent(this, t);
	}

	/**
	 * Called when window is actually being destroyed
	 */
	private void onDestroy() {
		displayManager.removedFromDisplay();
		GuiDomainEventManager.instance.dispatchGuiDomainEvent(this, GuiDomainEvent.Type.KILL_WINDOW);
	}
	
	/**
	 * Called on user request to close window
	 */
	private void onDispose() {
		dispose();
	}
	
	@Override
	public void dispose() {
		pApplet.getWindow().destroy();
	}

	@Override
	public Domain getParentDomain() {
		return parent;
	}
	
}
