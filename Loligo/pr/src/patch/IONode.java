package patch;

import java.util.List;
import java.util.function.BiPredicate;

import constants.Flow;
import gui.particles.prog.ParamHolder;
import gui.particles.prog.ParamHolderObject;
import gui.particles.prog.ParticleParam;
import lombok.var;
import module.AbstractModule;
import module.Module;
import module.ModuleDeleteMode;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.dynio.DynIOModules;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import save.ModuleData;
import save.RestoreDataContext;

public abstract class IONode extends AbstractModule implements ParamHolder, DynIOModule {

	//protected final List<ParticleParam> params = new ArrayList<>();
	protected final ParamHolder paramHolder;
	private final IOType ioType;
	protected DynIOInfo dynInfo;
	
	private IONode(IOType ioType, ParamHolder paramHolder) {
		
		this.ioType = ioType;
		this.paramHolder = paramHolder;
		flowFlags.add(Flow.DYNAMIC_IO);
	}

	
	@Override
	public void addParam(ParticleParam param, IOType io) {
		
		// Doesn't get called 
	}
	
	@Override
	public void removeParam(ParticleParam param, IOType io) {
	
		// Doesn't get called 
	}
	
	
	abstract public ParticleParam createParam(String name, DataType type);
	
	abstract public ParticleParam deleteParam(String name);
	
	@Override
	public ParticleParam getParam(String name, IOType io) {
	
		//return ParamHolder.getParam(params, name, io);
		return paramHolder.getParam(name, io);
	}
	
	@Override
	public List<ParticleParam> getParams() {
	
		return paramHolder.getParams();
	}
	
	@Override
	public List<ParticleParam> copyParams() {
		return paramHolder.copyParams();
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectIn() {
		return paramHolder.getParamHolderObjectIn();
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectOut() {
		return paramHolder.getParamHolderObjectOut();
	}
	
	public abstract ParamHolderObject getParamHolderObject();
	
	@Override
	public Module module() {
		return this;
	}
	
	
	@Override
	public void processIO() {
	
		//params.forEach(ParticleParam::);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
	
		super.onDelete(mode);
		
		if(mode == ModuleDeleteMode.NODE && getDomain().domainType() != DomainType.PARTICLE_INSTANCE)
			deleteParams();
	}

	public void deleteParams() {
		
		List<ParticleParam> pp = getParamHolderObject().copyParams();
		for(ParticleParam p : pp) {
			deleteParam(p.getName());
		}
		
	}
	
	@Override
	public DynIOInfo getDynIOInfo() {
		return dynInfo;
	}
	
	@Override
	public String rename(InOutInterface io, String name) {
		
		var m = paramHolder.module();
		List<? extends InOutInterface> ios = ioType == IOType.IN ? m.getInputs() : m.getOutputs();
		
		BiPredicate<InOutInterface, String> exists = (iof, n) -> {
			return ios.stream().anyMatch(mio -> mio.getName().equals(n));
		};
		
		name = DynIOModules.tryRenameIO(io, name, exists);
		
		String n = name;// = DynIOModule.tryRenameIO(io, name);
		n = getParamHolderObject().rename(io, n);
		return n;
	}

	
	public static class In extends IONode {
		
		public In(ParamHolder paramHolder) {
			super(IOType.IN, paramHolder);
			flowFlags.add(Flow.SOURCE);
			dynInfo = DynIOInfo.ALLTYPES_OUT;
		}
		
		@Override
		public ParticleParam createParam(String name, DataType type) {
		
			ParticleParam p = getParamHolderObjectIn().addParam(name, IOType.IN, type);
			p.createOutput(this);
			return p;
		}
		
		@Override
		public ParticleParam deleteParam(String name) {
			var p = getParamHolderObjectIn().removeParam(name, IOType.IN);
			removeIO(p.getIo2());
			return p;
		}
		
		
		@Override
		public void addParam(ParticleParam param, IOType io) {
			super.addParam(param, io);
			param.createOutput(this);
		}
		
		@Override 
		public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
			var p = createParam("", type);
			var io = p.getIo2();
			rename(io, name);
			return p.getIo2();
		}
		
		@Override 
		public InOutInterface deleteDynIO(String name, IOType ioType) {
			var p = deleteParam(name);
			return p.getIo2();
		}
		
		public ParamHolderObject getParamHolderObject() {
			return getParamHolderObjectIn();
		}
		/*
		@Override
		public void removeParam(ParticleParam param) {
			super.removeParam(param);
			
			removeIO(param.getIo2());
		}
		*/
		 
		@Override
		public void postInit() {
		
			super.postInit();
			
			//addXXX("Size", DataType.Number);
			//addXXX("Color", DataType.COLOR);
		}
		
		@Override
		public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
			
			super.doDeserialize(data, rdc);
			
			if(data.outputs != null)
				data.outputs.forEach(o -> createParam(o.name, o.type));
		}

	}
	
	
	public static class Out extends IONode {
		
		public Out(ParamHolder paramHolder) {
			super(IOType.OUT, paramHolder);
			flowFlags.add(Flow.SINK);
			dynInfo = DynIOInfo.ALLTYPES_IN;
		}
		
		@Override
		public void postInit() {
			super.postInit();
			setName("Out");
			//addDynamicIO("Num", DataType.Number);
			//addDynamicIO("Color", DataType.COLOR);
		}
		
		@Override
		public ParticleParam createParam(String name, DataType type) {
		
			ParticleParam p = getParamHolderObjectOut().addParam(name, IOType.OUT, type);
			p.createInput(this);
			return p;
		}
		
		@Override
		public void addParam(ParticleParam param, IOType io) {
			super.addParam(param, io);
			param.createInput(this);
		}
		
		@Override
		public ParticleParam deleteParam(String name) {
			var p = getParamHolderObjectOut().removeParam(name, IOType.OUT);
			removeIO(p.getIo1());
			return p;
		}
		
		@Override 
		public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
			var p = createParam("", type);
			var io = p.getIo1();
			rename(io, name);
			return io;
		}
		
		@Override 
		public InOutInterface deleteDynIO(String name, IOType ioType) {
			var p = deleteParam(name);
			return p.getIo1();
		}
		
		public ParamHolderObject getParamHolderObject() {
			return getParamHolderObjectOut();
		}
		
		@Override
		public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
			
			super.doDeserialize(data, rdc);
			
			if(data.inputs != null)
				data.inputs.forEach(o -> createParam(o.name, o.type));
		}

	}

}
