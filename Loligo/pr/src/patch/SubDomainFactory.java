package patch;

public interface SubDomainFactory {

	SubDomain createSubDomain();
	
}
