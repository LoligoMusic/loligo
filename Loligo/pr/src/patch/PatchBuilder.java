package patch;

import java.util.function.Supplier;

import constants.PatchEventManager;
import lombok.Getter;
import module.loading.AbstractNodeRegistry;
import module.loading.NodeEntry;
import pr.RootClass;

public class PatchBuilder {

	private AbstractNodeRegistry<NodeEntry> nodeRegistry;
	@Getter
	private Loligo proc;
	private DomainType domainType;
	private Runnable windowDestroyListener;
	private Supplier<? extends LoligoPatch> patchSupplier = LoligoPatch::new;
	private Supplier<Integer> idSupplier = Domain::createDomainID;
	private String title;
	
	
	public LoligoPatch createPatch() {
		
		LoligoPatch patch = patchSupplier.get();
		
		registerPatch(patch);
		
		assignParams(patch);
		
		return patch;
	}


	public void assignParams(LoligoPatch patch) {
		
		if(title != null)
			patch.setTitle(title);
		
		patch.setDomainType(domainType);
		
		if(nodeRegistry != null)
			patch.setNodeRegistry(nodeRegistry);
		patch.init();
		
		patch.setProc(proc);
		
		proc.setWindowFocusListener(f -> PatchEventManager.instance.dispatchPatchFocusEvent(patch, f));
		
		proc.setWindowResizeListener((w, h) -> {
			patch.getDisplayManager().setWH(w, h);
			PatchEventManager.instance.dispatchPatchResizeEvent(patch);
		});
		
		if(windowDestroyListener != null)
			proc.setWindowDestroyListener(windowDestroyListener);
		
	}
	
	
	public void registerPatch(LoligoPatch patch) {
		int id = idSupplier.get();
		patch.setDomainID(id);
		RootClass.backstage().putPatchId(id, patch);
	}
	
	
	public PatchBuilder setNodeRegistry(AbstractNodeRegistry<NodeEntry> nodeRegistry) {
		this.nodeRegistry = nodeRegistry;
		return this;
	}

	public PatchBuilder setProc(Loligo proc) {
		this.proc = proc;
		return this;
	}
	
	public PatchBuilder setDomainType(DomainType domainType) {
		this.domainType = domainType;
		return this;
	}
	
	public PatchBuilder setPatchSupplier(Supplier<LoligoPatch> patchSupplier) {
		this.patchSupplier = patchSupplier;
		return this;
	}
	
	public PatchBuilder setDomainIdSupplier(Supplier<Integer> idSup) {
		idSupplier = idSup;
		return this;
	}
	
	public PatchBuilder setWindowDestroyListener(Runnable windowDestroyListener) {
		
		this.windowDestroyListener = windowDestroyListener;
		return this;
		
	}


	public PatchBuilder setTitle(String title) {
		this.title = title;
		return this;
	}
	
}
