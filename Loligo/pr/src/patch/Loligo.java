package patch;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.jogamp.newt.opengl.GLWindow;

import lombok.Getter;
import lombok.Setter;
import pr.DisplayContainerIF;
import pr.RootClass;
import pr.userinput.UserInput;
import processing.awt.PSurfaceAWT.SmoothCanvas;
import processing.core.PApplet;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import processing.opengl.PJOGL;


public class Loligo extends PApplet {
	
	
	@Setter
	private String renderer = P2D;
	@Getter@Setter
	private int fps = 60;
	private int initialWidth = 900, initialHeight = 800;
	private DisplayContainerIF dm;
	private UserInput input;
	@Getter
	private ProcWindow window;
	
	@Override
	public void draw() {
		if(dm != null)
			dm.draw();
	}

	public void mouseEvent(MouseEvent event) {
		if(input != null)
			input.mouseEvent(event);
	}
	
	public void keyEvent(KeyEvent event) {
		if(input != null)
			input.keyEvent(event);
	}
	
	public void registerDisplay(DisplayContainerIF dm) {
		
		this.dm = dm;
		this.input = dm.getInput();
		
		//registerMethod("draw", dm);
		/*
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		*/
		//registerMethod("mouseEvent", dm.getInput());
		//registerMethod("keyEvent", dm.getInput());
		
		//unregisterMethod("draw", this);
		//unregisterMethod("mouseEvent", this);
		//unregisterMethod("keyEvent", this);
		
	}
	

	public void startSketch() {
		
		//String spath = "--sketch-path=" + proc.sketchPath();
		//String loc = "--location=" + 5 + "," + 5;
		//String name = proc.;
		String[] args = {getClass().getName()};
		
		PApplet.runSketch(args, this);
		
		initWindow();
		
		registerMethod("mouseEvent", this);
		registerMethod("keyEvent", this);
	}
	
	
	public void tryExit() {
		boolean b = RootClass.backstage().endSession();
		if(b)
			exit();
	}
	
	
	public void setMainWindow() {
		window.setWindowDestroyNotification(w -> tryExit());
	}
	
	/**
	 * Runs onDestroy, but doesn't destroy Window. 
	 * It's the caller's choice if and when to call Loligo.destroy().
	 * 
	 * @param onDestroy
	 */
	//private void setWindowDestroyNotification(Consumer<ProcWindow> onDestroy) {
	//	window.setWindowDestroyNotification(onDestroy);
	//}
	/*
	@Setter
	private Runnable onDestroy;
	*/
	/**
	 * Runs onDestroy, then destroys Window.
	 * 
	 * @param onDestroy
	 */
	public void setWindowDestroyListener(Runnable onDestroy) {
		window.setWindowDestroyListener(w -> onDestroy.run());
	}
	
	public void setWindowDestroyNotification(Runnable onDestroyNotification) {
		window.setWindowDestroyNotification(w -> onDestroyNotification.run());
	}
	
	public void setWindowFocusListener(Consumer<Boolean> onFocus) {
		window.setFocusListener(onFocus);
	}
	
	public void setWindowResizeListener(BiConsumer<Integer, Integer> onResize) {
		window.setResizeListener(onResize);
	}
	
	
	public void settings() {
		
		size(initialWidth, initialHeight, renderer);
		smooth(8);
		
		String p = "/pr/src/resources/";
		PJOGL.setIcon(p + "icon16.png", p + "icon32.png", p + "icon48.png");
	}
	
	
	public void setup() {
		
		background(0);
		noStroke();
		frameRate(fps);
		fill(0);
		
		setTitle(null);
		surface.setResizable(true);
	}

	
	public void initWindow() {
		
		var nat = surface.getNative();
		
		window = switch(renderer) {
			case P2D	-> new ProcWindowGL((GLWindow) nat);
			case JAVA2D -> new ProcWindowJava2D((SmoothCanvas) nat);
			default  	-> null;
		};
		
		window.initWindowListener();
	}
	
	
	public void setWindowSize(int w, int h) {
		window.setSize(w, h);
	}
	
	public void setFrameRate(int fps) {
		if(getSurface() != null)
			frameRate(fps);
		this.fps = fps;
	}
	
	public float getFrameRateTarget() {
		
		return frameRate;
	}
	
	
	@Override
	public void mouseWheel(MouseEvent event) {
		
		//display.getInput().mouseWheel(event.getCount());
	}
	
	
	public void setTitle(String name) {
		
		name = name == null ? "New File" : name; 
		
		getSurface().setTitle(name + " - Loligo");
	}

	public void requestFocus() {
		window.requestFocus();
	}
	
	public void initialWH(int w, int h) {
		initialWidth = w;
		initialHeight = h;
	}
}


