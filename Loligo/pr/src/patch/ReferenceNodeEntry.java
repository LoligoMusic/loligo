package patch;

import java.util.function.Function;
import module.Module;
import module.loading.AbstractNodeEntry;
import module.loading.NodeCategory;

/**
 * NodeEntry that initializes Nodes that take a parameter in their constructor. 
 * Takes a Function<Parameter, Module> -> use createInstance(Parameter),
 * OR a Parameter instance -> use createInstance()
 *
 * @param <R>
 */
public class ReferenceNodeEntry<R> extends AbstractNodeEntry implements ReferenceNodeEntryIF<R>{

	private final Function<R, Module> foo;
	
	public ReferenceNodeEntry(Function<R, Module> foo, String name, NodeCategory type, boolean hidden) {
		
		super(name, type, hidden);
		
		this.foo = foo;
	}

	@Override
	public Module createInstance(R pm) {
		
		return foo.apply(pm);
	}
	
	
	@Override
	public Module createInstance() {
		
		return foo.apply(null);
	}

	
	
	public static class Wrapper<R> implements ReferenceNodeEntryIF<R> {

		private final ReferenceNodeRegistry<R> reg;
		private final ReferenceNodeEntryIF<R> entry;
		
		
		public Wrapper(ReferenceNodeRegistry<R> reg, ReferenceNodeEntryIF<R> entry) {
			
			this.reg = reg;
			this.entry = entry;
		}

		@Override
		public String getName() {
			return entry.getName();
		}

		@Override
		public NodeCategory getType() {
			return entry.getType();
		}

		@Override
		public boolean isHidden() {
			return entry.isHidden();
		}

		@Override
		public Module createInstance() {
			return reg.createInstance(entry);
		}

		@Override
		public Module createInstance(R pm) {
			
			return entry.createInstance(pm);
		}

	}
}
