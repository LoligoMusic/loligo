package patch;

import gui.particles.prog.ParticleParam;
import lombok.var;
import module.AbstractModule;
import module.inout.Output;
import save.ModuleData;
import save.RestoreDataContext;

public class PassiveNodeIn extends AbstractModule {

	public PassiveNodeIn() {
		
	}

	@Override
	public void processIO() {
		
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		//super.doDeserialize(data, rdc);
		
		var ios = rdc.getPatch().getParticleParams();
		
		if(ios != null)
			for (ParticleParam p : ios) {
				Output out = p.getType().createOutput(p.getName(), this);
				addIO(out);
				out.setValueObject(p.getValue());
			}
	}

}
