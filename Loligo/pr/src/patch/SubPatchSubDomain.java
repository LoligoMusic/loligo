package patch;

import java.util.List;

import audio.AudioOut;
import gui.particles.prog.ParamHolderObject;
import gui.particles.prog.ParticleParam;
import module.inout.IOType;
import module.signalFlow.SignalFlowContainer;
import patch.subpatch.SubPatch;

public class SubPatchSubDomain extends SubDomain implements ParamHolderDomain {

	private final SubPatch subPatch;
	
	/*
	public SubPatchSubDomain(Domain parent, SignalFlowContainer container) {
		super(parent, container);
		// TODO Auto-generated constructor stub
	}
	*/

	public SubPatchSubDomain(Domain parent, SignalFlowContainer container, AudioOut audioManager,
			DomainType domainType, SubPatch subPatch) {
		super(parent, container, audioManager, domainType);
		
		this.subPatch = subPatch; 
	}

	@Override
	public void setDomainID(int domainID) {
		super.setDomainID(domainID);
	}
	
	@Override
	public SubPatch getSubPatchModule() {
		return subPatch;
	}
	
	@Override
	public List<ParticleParam> getParams() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ParticleParam getParam(String name, IOType io) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParam(ParticleParam param, IOType io) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<ParticleParam> copyParams() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeParam(ParticleParam param, IOType io) {
		// TODO Auto-generated method stub

	}

	@Override
	public ParamHolderObject getParamHolderObjectIn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ParamHolderObject getParamHolderObjectOut() {
		// TODO Auto-generated method stub
		return null;
	}

}
