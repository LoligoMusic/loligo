package patch;

import module.Module;
import module.loading.NodeEntry;

public interface ReferenceNodeEntryIF<R> extends NodeEntry {

	public Module createInstance(R pm);
}
