package patch;

import errors.ErrorHandler;
import history.GuiHistory;
import pr.DisplayManagerIF;

public interface GuiDomain {
	
	public DisplayManagerIF getDisplayManager();
	public Loligo getPApplet();
	public DomainType domainType();
	public ErrorHandler getErrorHandler();
	public void reset();
	public void dispose();
	public GuiHistory getHistory();
	
	default String getTitle() {return null;}
	default Domain getParentDomain() {return null;}
	
}
