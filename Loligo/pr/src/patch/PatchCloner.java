package patch;

import static save.SaveTarget.MEMENTO;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import audio.AudioOut;
import constants.Flow;
import constants.InputEvent;
import constants.ModuleEvent;
import constants.ModuleEventListener;
import constants.SlotEvent;
import constants.SlotEventListener;
import gui.particles.ParticleSystemRegistry;
import gui.particles.prog.ParticleParamHolder;
import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.ModuleDeleteMode;
import module.Readable;
import module.inout.Input;
import module.inout.Output;
import module.loading.NodeRegistry;
import module.signalFlow.ImmutableSignalFlow;
import module.signalFlow.SignalFlowContainer;
import pr.EventSubscriber;
import pr.ExitFrameListener;
import pr.PassiveDisplayManager;
import pr.RootClass;
import pr.userinput.UserInput;
import save.PatchData;
import save.RestoreDataContext;
import save.SaveDataContext;
import save.SaveMode;


public class PatchCloner implements PatchContainer, 
									ModuleEventListener, 
									SlotEventListener, 
									EventSubscriber,
									ExitFrameListener
									{
	@Getter@Setter
	private volatile PatchData patchData;
	//private Domain domain;
	@Getter
	private LoligoPatch patch;
	private SignalFlowContainer container = new SignalFlowContainer();
	@Getter@Setter
	private BiFunction<PatchData, Domain, RestoreDataContext> restoreDataContextSupplier;
	@Getter@Setter
	private BiFunction<PatchData, Domain, RestoreDataContext> domainSupplier;
	
	//private AudioOut audioManager;
	private final AudioOut particleAudio;
	private boolean allowUpdate = true, needsUpdate = true;
	
	
	
	public PatchCloner() {
		
		container = new SignalFlowContainer();
		RootClass.mainPatch().addModule(container); // TODO eliminate static access
		particleAudio = ParticleSystemRegistry.defaultParticleSystem().getParticleAudioManager();
		
		restoreDataContextSupplier = RestoreDataContext::new;
	}
	
	
	public void createPatchWindow(ParticleParamHolder paramHolder) {
		
		if(patch != null && !patch.isDisposed()) {
			patch.getPApplet().requestFocus();
			return;
		}
		
		PatchBuilder pb = new PatchBuilder() {
			public LoligoPatch createPatch() {
				
				var patch = super.createPatch();
				
				getProc().setWindowDestroyListener(patch::onDispose);
				
				patch.getDisplayManager().addedToDisplay(); // TODO this should be somewhere central for all 
				patch.setTitle("new ShooterPatch");
				patch.setDomainType(DomainType.PARTICLE_FACTORY);
				patch.getAudioManager().disconnect();
				patch.setNodeRegistry(NodeRegistry.createParticleFactoryRegistry(paramHolder));
				patch.getModuleManager().addModuleEventListener(PatchCloner.this);
				patch.getModuleManager().addSlotEventListener(PatchCloner.this);
				patch.getDisplayManager().getInput().addInputEventListener(PatchCloner.this);
				patch.getDisplayManager().addExitFrameListener(PatchCloner.this);
				
				if(getPatchData() != null) {
					
					allowUpdate = false;
					new RestoreDataContext(getPatchData(), patch).restorePatch();
					allowUpdate = true;
				}
				
				PatchCloner.this.patch = patch;
				allowUpdate = true;
				
				return patch;
			}
		};
		pb.setDomainType(DomainType.PARTICLE_FACTORY);
		
		allowUpdate = false;
		LoligoPatch.createSecondaryWindow(pb);
			
	}
	

	public SubDomain createInstance(ParticleParamHolder paramHolder) {
		
		//TODO Move SubDomain creation to Factory, only return cloned Module[]
		SubDomain sd = new SubDomain(RootClass.mainPatch(), container, particleAudio, DomainType.PARTICLE_INSTANCE);
		sd.setDisplayManager(new PassiveDisplayManager(sd));
		container.addBlock(sd);
		
		sd.setNodeRegistry(NodeRegistry.createParticleInstanceRegistry(paramHolder));  //TODO supply SubDomain Builder _with SignalFLowContainer
		
		ImmutableSignalFlow isf = new ImmutableSignalFlow();
		sd.setSignalFlow(isf);
		
		Module[] mds = doClone(sd);
		
		List<Readable> rs = new ArrayList<>(mds.length);
		for (int i = 0; i < mds.length; i++) { 
			Module m = mds[i];
			if(m.checkFlag(Flow.READER)) {
				rs.add((Readable) m);
			}
		}
		Readable[] readers = rs.toArray(new Readable[rs.size()]);
		
		isf.setModules(mds);
		isf.setReaders(readers);
		
		return sd;
	}
	
	
	public void build(List<? extends Module> modules) {
		
		var sdc = new SaveDataContext(SaveMode.DUPLICATION);
		sdc.setSaveTarget(MEMENTO);
		sdc.addPatch(patch, modules);
		
		patchData = sdc.buildPatchData();
	}
	
	
	public Module[] doClone(Domain domain) {
		
		PatchData pd = patchData; // copy reference to pd, because patchData can be overwritten by another thread anytime
		
		restoreDataContextSupplier
			.apply(pd, domain)
			.disableGui()
			.setSaveMode(SaveMode.DUPLICATION)
			.restorePatch();
			
		var mds = pd.getNodes();
		
		var mods = new Module[mds.size()];
		
		for (int i = 0; i < mods.length; i++) {
			
			mods[i] = mds.get(i).getModuleOut();
		}
		
		return mods;
	}
	
	
	public void setUpdate() {
		
		needsUpdate = true;
	}
	
	public void doUpdate() {
		
		patch.getSignalFlow().tryBuild();
		
		patch.getSignalFlow().process();
		
		build(patch.getSignalFlow().getFlowList());
			
		needsUpdate = false;
	}
	
	@Override
	public void exitFrame() {
		
		if(needsUpdate && allowUpdate) {
			
			doUpdate();
		}
	}
	
	@Override
	public void moduleEvent(ModuleEvent e, Module m) {
		
		if(m.getDomain() == patch) {
		
			setUpdate();
		}
	}
	
	
	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		
		if(e.isConnectDisconnect && 
				patch != null && in.getModule().getDomain() == patch) {
			
			setUpdate();
		}
	}


	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(patch != null /*&& input.display.getDomain() == patch*/) {
			
			switch(e) {
				case MOVE:
				case OVER:
				case OVER_STOP:
					break;
				default:
					setUpdate();
			}
			
		}
	}
	
	
	public void removeModuleBlock(SubDomain sd) {
		
		container.removeBlock(sd);
	}
	
	public void dispose() {
		
		RootClass.mainPatch().removeModule(container, ModuleDeleteMode.NODE);
		
		if(patch != null && !patch.isDisposed()) {
			
			patch.killWindow();
		}
	}

}
