package patch;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface ProcWindow {
	
	public int getX();
	public int getY();
	public int getWidth();
	public int getHeight();
	public void setPosition(int x, int y);
	void setSize(int w, int h);
	
	public void initWindowListener();
	
	public void setWindowDestroyNotification(Consumer<ProcWindow> onDestroy);
	public void setWindowDestroyListener(Consumer<ProcWindow> onDestroy);
	public void setFocusListener(Consumer<Boolean> onFocus);
	public void setResizeListener(BiConsumer<Integer, Integer> onResize);
	
	public void requestFocus();
	public void destroy();
}
