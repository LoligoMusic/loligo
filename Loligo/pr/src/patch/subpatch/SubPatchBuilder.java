package patch.subpatch;

import lombok.var;
import patch.DomainType;
import patch.Loligo;
import patch.LoligoPatch;
import patch.PatchBuilder;
import pr.RootClass;
import save.ModuleData;
import save.PatchData;
import save.RestoreDataContext;
import save.SaveDataContext;
import save.SaveMode;
import save.SaveTarget;


public class SubPatchBuilder extends PatchBuilder {

	private final SubPatch subPatch;
	
	public SubPatchBuilder(SubPatch subPatch) {
		this.subPatch = subPatch;
		setDomainType(DomainType.SUBPATCH);
	}
	
	@Override 
	public LoligoPatch createPatch() {
		
		setPatchSupplier(() -> new LoligoSubPatch((LoligoPatch) subPatch.getDomain(), subPatch));
		
		setDomainIdSupplier(subPatch.getPatch()::getDomainID);
		
		var p = (LoligoSubPatch) super.createPatch();
		initSubPatch(p);
		
		return p;
	}
	
	void initSubPatch(LoligoSubPatch p) {
		
		Loligo proc = getProc();
		
		proc.setWindowDestroyListener(p::killWindow);
		//proc.registerDisplay(p.getDisplayManager());
		
		RootClass.mainDm().addEnterFrameListener(p.getHistory()); // Runs executionQueue on main loop
		
		saveData(p);
		
		proc.loop();
		
		var d = new SubpatchOrigin(subPatch);
		p.getDisplayManager().addGuiObject(d);
	}
	
	/*
	@Override
	public void registerPatch(LoligoPatch patch) {
		
		RootClass.backstage().putPatchId(patch.getDomainID(), patch);
	}
	*/
	
	void saveData(LoligoSubPatch p) {
		
		var sdc = new SaveDataContext(SaveMode.DUPLICATION);
		sdc.addPatch(subPatch.getPatch());
		sdc.setSaveTarget(SaveTarget.MEMENTO);
		PatchData pd = sdc.buildPatchData();
		pd.moduleInToOut();
		
		var rdc = new RestoreDataContext(pd, p);
		
		rdc.addModules(); 
		//rdc.restorePaths(); 
		rdc.restoreStacks();
		rdc.restoreLinks();
		
		var patchData = subPatch.getPatchData();
		
		if(patchData != null) {
			
			rdc.restorePatchProperties(patchData.getWidth(), patchData.getHeight());
			
			for(ModuleData md : pd.getNodes()) {
				
				patchData.getNodes().stream()
					.filter(m -> m.getModuleOut() == md.getModuleOut())
					.findFirst()
					.ifPresent(m -> md.getModuleOut().getGui().setPos(m.xy[0], m.xy[1]));
			}
			patchData = null;
		}
		
		subPatch.setPatch(p);
	}

}
