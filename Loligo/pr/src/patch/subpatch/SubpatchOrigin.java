package patch.subpatch;

import static util.Colors.WHITE;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.selection.GuiType;
import pr.DisplayObject;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public class SubpatchOrigin extends DisplayObject {
		
	private final SubPatch subpatch;
	
	public SubpatchOrigin(SubPatch subpatch) {
		
		this.subpatch = subpatch;
		
		setGuiType(GuiType.DRAGABLE_FREE);
		setFlag(DisplayFlag.CLICKABLE, true);
		setFlag(DisplayFlag.DRAGABLE, true);
		setFlag(DisplayFlag.ROUND, true);
		setWH(7, 7);
		setColor(0xffbbbbbb);
		setPos(subpatch.getOffsetX(), subpatch.getOffsetY());
	}
	
	@Override
	public void render(PGraphics g) {
		
		g.strokeWeight(1);
		g.stroke(checkFlag(DisplayFlag.mouseOver) ? WHITE : getColor());
		g.noFill();
		
		float x = getX(), y = getY();
		float w = 10;//getWidth() / 2;
		float r = getWidth();
		float r2 = getWidth() / 2;
		g.line(x - r2, y,	 x - w, y	 );
		g.line(x + r2, y,	 x + w, y	 );
		g.line(x	, y - r2, x	  , y - w);
		g.line(x	, y + r2, x	  , y + w);
		
		g.ellipse(x + .5f, y + .5f, r , r);
		
		g.noStroke();
	}
	
	@Override
	public void setPos(float x, float y) {
		super.setPos(x, y);
		subpatch.setOffsetX(x); 
		subpatch.setOffsetY(y); 
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		if(e == InputEvent.DRAG) {
			subpatch.setOffsetX(getX()); 
			subpatch.setOffsetY(getY());
		}
	}
	
	/*
	private class Pos implements Positionable {
		
	}
	*/
}
