package patch.subpatch;

import java.util.List;

import audio.AudioOut;
import gui.particles.prog.ParamHolderObject;
import gui.particles.prog.ParticleParam;
import module.inout.IOType;
import module.loading.NodeRegistry;
import patch.Domain;
import patch.DomainType;
import patch.LoligoPatch;
import patch.ParamHolderDomain;
import pr.DisplayManager;
import pr.DisplayManagerIF;

public class LoligoSubPatch extends LoligoPatch implements ParamHolderDomain {

	private final SubPatch subpatch;
	
	public LoligoSubPatch(LoligoPatch parent, SubPatch subpatch) {
		
		this.setParentPatch(parent);
		this.subpatch = subpatch;
		domainType = DomainType.SUBPATCH;
		setNodeRegistry(NodeRegistry.createSubPatchRegistry(subpatch));
		
	}
	
	
	@Override
	public DisplayManagerIF initDisplayManager() {
	
		DisplayManager dm = new DisplayManager(this) {
			@Override
			public boolean isProcessActive() {
				
				return LoligoSubPatch.this.getParentPatch().getDisplayManager().isProcessActive();
			}
			/*
			@Override
			public void pauseProcessing(boolean pause) {
			
				//LoligoSubPatch.this.getParentPatch().getDisplayManager().pauseProcessing(pause);
			}
			*/
		};
		
		dm.setDm(dm);
		dm.addedToDisplay();
		
		return dm;
	}
	
	
	@Override
	public AudioOut initAudioManager() {
	
		AudioOut ao = new AudioOut() {
			@Override
			public void connect() {
				LoligoSubPatch.this
					.getParentPatch()
					.getAudioManager()
					.getMasterBead()
					.addInput(getMasterBead());
			}
			
			@Override
			public void disconnect() {
				LoligoSubPatch.this
					.getParentPatch()
					.getAudioManager()
					.getMasterBead()
					.removeAllConnections(getMasterBead());
			}
		};
		
		ao.connect();
		return ao;
	}
	
	@Override
	public boolean isProcessActive() {
		return getParentPatch().isProcessActive();
	}
	
	@Override
	public List<ParticleParam> getParams() {
		
		return subpatch.getParams();
	}
	
	@Override
	public void killWindow() {
		
		subpatch.onKillWindow();
		super.killWindow();
	}

	@Override
	public ParticleParam getParam(String name, IOType io) {
		
		return subpatch.getParam(name, io);
	}

	@Override
	public void addParam(ParticleParam param, IOType io) {
		
	}

	@Override
	public void removeParam(ParticleParam param, IOType io) {
		
	}
	
	@Override
	public List<ParticleParam> copyParams() {
		
		return subpatch.copyParams();
	}

	@Override
	public ParamHolderObject getParamHolderObjectIn() {
		
		return subpatch.getParamHolderObjectIn();
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectOut() {
		
		return subpatch.getParamHolderObjectOut();
	}
	
	@Override
	public Domain getParentDomain() {
	
		return getParentPatch();
	}
	
	@Override
	public SubPatch getSubPatchModule() {
		return subpatch;
	}
}
