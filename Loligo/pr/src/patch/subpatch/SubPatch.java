package patch.subpatch;

import static constants.DisplayFlag.CLICKABLE;
import static constants.InputEvent.CLICK;
import static patch.DomainType.SUBPATCH;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.google.common.io.CharStreams;

import constants.Flow;
import gui.DragHandle;
import gui.particles.prog.ParamHolder;
import gui.particles.prog.ParamHolderObject;
import gui.particles.prog.ParticleParam;
import gui.selection.GuiType;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.var;
import module.AnimObject;
import module.Module;
import module.ModuleDeleteMode;
import module.Modules;
import module.Readable;
import module.dynio.DynIOGUIWrapper;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.dynio.DynIOModules;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import module.loading.NodeRegistry;
import patch.Domain;
import patch.IONode;
import patch.LoligoPatch;
import patch.ParamHolderDomain;
import patch.SubDomain;
import patch.SubPatchSubDomain;
import pr.DisplayManagerIF;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.EnterFrameListener;
import pr.ExitFrameListener;
import pr.ModuleGUI;
import pr.RootClass;
import pr.SubPatchPassiveDisplayManager;
import processing.core.PGraphics;
import processing.core.PMatrix2D;
import save.ModuleData;
import save.PatchData;
import save.RestoreDataContext;
import save.SaveDataContext;
import save.SaveJson;
import save.SaveMode;
import util.Colors;

@NodeInfo(type = NodeCategory.TOOLS)
public class SubPatch extends AnimObject implements ParamHolder, 
													EnterFrameListener, 
													ExitFrameListener,
													Readable,
													DynIOModule
													{
	
	private static final String DEFAULT_JSON = "subpatch_default.json";
	
	@Getter@Setter
	private ParamHolderDomain patch;
	@Getter@Setter
	private boolean hasWindow;
	@Getter@Setter
	private PatchData patchData;
	private final ParamHolderObject paramsIn, paramsOut;
	@Getter@Setter private float rotate;
	@Getter private PMatrix2D matrix = new PMatrix2D();
	@Getter@Setter private int tint = 0xffffffff;
	@Getter@Setter private float offsetX = 150, offsetY = 150;
	
	
	 
	public SubPatch() {
		
		flowFlags.add(Flow.DYNAMIC_IO);
		flowFlags.add(Flow.PERMA);
		flowFlags.add(Flow.READER);
		
		paramsIn = new ParamHolderObject(); 
		paramsOut = new ParamHolderObject();
		
		paramsIn.setParamHolder(this);
		paramsOut.setParamHolder(this);
	}
	
	@Override
	public void defaultSettings() {
	
		super.defaultSettings();
		
		try {
			InputStream is = SubPatch.class.getResourceAsStream(DEFAULT_JSON);
			String js = CharStreams.toString(new InputStreamReader(is, StandardCharsets.UTF_8));
			PatchData pd = SaveJson.parseJsonToPatch(js);
			//Reseting dom-id to not duplicate id of default patch
			pd.setId(patch.getDomainID());
			restorePatch(pd);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void postInit() {
	
		super.postInit();
		
		if(patch != null)
			return;
		
		Domain parent = getDomain();
		
		SubPatchSubDomain sd = new SubPatchSubDomain(parent, null, parent.getAudioManager(), SUBPATCH, this);
		
		int id = Domain.createDomainID();
		sd.setDomainID(id);
		RootClass.backstage().putPatchId(id, sd);
		
		val sdm = new SubPatchPassiveDisplayManager(sd);
		sd.setDisplayManager(sdm);
		SubDomain.create(
				sd,
				new Module[0],
				NodeRegistry.createSubPatchRegistry(SubPatch.this)
			);
		
		DisplayManagerIF dm = parent.getDisplayManager();
		dm.addEnterFrameListener(this);
		dm.addExitFrameListener(this);
		patch = sd;
	}
	
	@Override
	public ModuleGUI createGUI() {
		
		DragHandle h = new DragHandle(this, null) {
			@Override
			public void mouseEventThis(constants.InputEvent e, pr.userinput.UserInput input) {
				if(e == CLICK && hasWindow) {
					patch.getPApplet().requestFocus();
				}
			}
		};
		
		h.setWH(15, 15);
		h.setGuiType(GuiType.ANIM);
		var w = new DynIOGUIWrapper(this, h) {
			@Override
			public void enterEditMode() {
				Runnable r = () -> {if(!hasWindow) createPatchWindow();};
				new Thread(r).start();
			}
		};
		return w;
	}
	
	
	@Override
	public void processIO() {
		
		paramsIn.transfer();
		
		var s = patch.getSignalFlow();
		RootClass.signalFlowManager().insertBlock(s);
		
		s.onProcessComplete(paramsOut::transfer);
	}
	
	
	@Override
	public ParticleParam getParam(String name, IOType io) {
		
		return (io == IOType.IN ? paramsIn : paramsOut).getParam(name);
	}

	@Override
	public void addParam(ParticleParam param, IOType io) { // needs IOType arg to decide on paramsIn or paramsOut
		 
		if(io == IOType.IN)
			param.createInput(this);
		else
			param.createOutput(this);
	}

	@Override
	public void removeParam(ParticleParam param, IOType ioType) {
		
		if(ioType == IOType.IN)
			removeIO(param.getIo1());
		else
			removeIO(param.getIo2());
	}
	 
	@Override
	public Module module() {
		return this;
	}
	
	@Override
	public List<ParticleParam> copyParams() {
		
		var p1 = paramsIn.getParams(),
			p2 = paramsOut.getParams();
		
		var pp = new ArrayList<ParticleParam>(p1.size() + p2.size());
		pp.addAll(p1);
		pp.addAll(p2);
		
		return pp;
	}
	
	@Override
	public List<ParticleParam> getParams() {
		
		val ar = new ArrayList<>(paramsIn.getParams());
		ar.addAll(paramsOut.getParams());
		return ar;
	}


	@Override
	public ParamHolderObject getParamHolderObjectIn() {
		
		return paramsIn;
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectOut() {
		
		return paramsOut;
	}
	
	
	@Override
	public int pipette(int x, int y) {
		return 0;
	}

	@Override
	public DisplayObjectIF createAnim() {
		
		val d = new DisplayObject() {
			@Override
			public void render(PGraphics g) {
				
				final DisplayManagerIF dm = patch.getDisplayManager();
				
				g.push();
				g.tint(Colors.multiply(tint, g.tintColor));
				g.applyMatrix(dm.getTransform().getMatrix());
				dm.renderAnimLayer(g);
				g.tint(Colors.WHITE);
				g.pop();
			}
		};
	
		d.setFlag(CLICKABLE, false);
		
		return d;
	}

	public void onKillWindow() {
		
		patchData = createSubPatchData(SaveMode.SHALLOW);
		hasWindow = false;
		RootClass.mainDm().removeEnterFrameListener(patch.getHistory());
	}
	
	public PatchData createSubPatchData(SaveMode saveMode) {
		
		var sd = new SaveDataContext(saveMode);
		sd.addPatch(patch);
		var pd = sd.buildPatchData();
		return pd;
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.addExtras(offsetX, offsetY);
		
		SaveMode sm = sdc.getSaveMode();
		if(sm != SaveMode.SHALLOW) {
			md.subpatch = hasWindow ? createSubPatchData(sm) : 
									  patchData;
		}
		return md;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		offsetX = data.nextFloat();
		offsetY = data.nextFloat();
		
		restorePatch(data.subpatch);
		
		super.doDeserialize(data, rdc);
	}

	public void restorePatch(PatchData subpatch) {
		
		patchData = subpatch;
		
		val sd = (SubDomain) patch;
		
		var reg = NodeRegistry.createSubPatchRegistry(SubPatch.this);
		sd.setNodeRegistry(reg);
		
		var rd = new RestoreDataContext(subpatch, sd);
		rd.disableGui();
		rd.restorePatch();
		
		List<ModuleData> mds = subpatch.getNodes();
		Module[] mods = new Module[mds.size()];
		for (int i = 0; i < mods.length; i++) {
			mods[i] = mds.get(i).getModuleOut();
		}
		SubDomain.create(sd, mods, reg); 
		
		sd.getDisplayManager().updateDisplayList();
		
	}

	public void setMatrix(PMatrix2D m) {
		matrix.set(m);
	}
	
	private void applyMatrix() {
		var t = patch.getDisplayManager().getTransform();
		var m = t.getMatrix();
		
		m.reset();
		m.translate(getX(), getY());
		m.apply(matrix);
		m.translate(-offsetX, -offsetY);
		
		var pt = getDomain().getDisplayManager().getTransform();
		t.applyParent(pt);
	}
	
	@Override
	public void enterFrame() {
		
		applyMatrix();
		
		patch.getDisplayManager().dispatchEnterFrameEvent();
	}


	@Override
	public void exitFrame() {
		patch.getDisplayManager().dispatchExitFrameEvent();
	}

	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		patch.dispose();
		RootClass.backstage().removePatchId(patch.getDomainID());
	}

	public void createPatchWindow() {
		
		var pb = new SubPatchBuilder(this);
		LoligoPatch.createSecondaryWindow(pb);
		
		hasWindow = true;
	}

	@Override
	public void read() {
		patch.getSignalFlow().processReaders();
	}

	
	@Override
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		
		InOutInterface iof = null;
		IONode m = null;  
		
		var t = ioType == IOType.IN;
		var ms = Modules.findModuleByName(getPatch(), t ? "In" : "Out");
		
		if(ms.isEmpty()) {
			// TODO Create In/Out Node if missing
		}else {
			m = (IONode) ms.get(0);
		}
			
		InOutInterface io = m.createDynIO(name, type, null);
		var p = m.getParamHolderObject().getParam(io);
		
		iof = t ? p.getIo1() : p.getIo2();
		
		rename(iof, name);
		return iof;
	}

	
	@Override
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		
		var t = ioType == IOType.IN;
		var po = t ? paramsIn : paramsOut;
		
		var p = po.getParam(name);
		var io = t ? p.getIo2() : p.getIo1();
		var m = (DynIOModule) io.getModule();
		m.deleteDynIO(name, null);
		
		return io;
	}

	
	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.ALLTYPES_IN_OUT;
	}
	

	@Override
	public String rename(InOutInterface io, String name) {
		boolean in = io.getIOType() == IOType.IN;
		ParamHolderObject po = in ? paramsIn : paramsOut;
		name = DynIOModules.tryRenameIO(io, name);
		return po.rename(io, name);
	}
	
}
