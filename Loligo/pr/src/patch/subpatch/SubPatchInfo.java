package patch.subpatch;

import constants.Flow;
import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberOutputIF;

public class SubPatchInfo extends AbstractModule {

	private final SubPatch subpatch;
	private final NumberOutputIF out_rotation, out_x, out_y;
	
	
	public SubPatchInfo(SubPatch subpatch) {
		
		flowFlags.add(Flow.SOURCE);
		this.subpatch = subpatch;
		out_rotation = addOutput("Rotation", NumericAttribute.DEZIMAL_FREE);
		out_x = addOutput("X", NumericAttribute.DEZIMAL_FREE);
		out_y = addOutput("Y", NumericAttribute.DEZIMAL_FREE);
	}

	@Override
	public void processIO() {
		
		out_rotation.setValue(subpatch.getRotate());
		out_x.setValue(subpatch.getX());
		out_y.setValue(subpatch.getY());
	}

}
