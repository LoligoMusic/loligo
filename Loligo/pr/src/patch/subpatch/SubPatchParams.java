package patch.subpatch;

import constants.Flow;
import lombok.var;
import module.AbstractModule;
import module.inout.ColorInputIF;
import module.inout.TransformInput;

public class SubPatchParams extends AbstractModule {

	private final SubPatch subpatch;
	private final TransformInput in_trans;
	private final ColorInputIF in_tint;
	
	public SubPatchParams(SubPatch subpatch) {
		this.subpatch = subpatch;
		flowFlags.add(Flow.SINK);
		
		in_trans = addTransformInput();
		int c = 0xffffffff;
		in_tint = addColorInput().setDefaultColor(c);
		in_tint.setColor(c);
		subpatch.setTint(c);
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		subpatch.setTint(in_tint.getColor());
	}

	@Override
	public void processIO() {
		
		var v = in_trans.getMatrixInput();
		
		subpatch.setMatrix(v);
		
		subpatch.setTint(in_tint.getColorInput());
	}

}
