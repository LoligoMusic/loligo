package patch;

import gui.particles.prog.ParamHolder;
import patch.subpatch.SubPatch;

public interface ParamHolderDomain extends ParamHolder, Domain {

	public SubPatch getSubPatchModule();
}
