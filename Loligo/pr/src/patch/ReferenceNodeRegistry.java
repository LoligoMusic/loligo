package patch;

import java.util.Collection;
import java.util.Map;
import com.google.common.collect.Collections2;
import lombok.Getter;
import module.Module;
import module.loading.AbstractNodeRegistry;
import module.loading.NodeCategory;


public class ReferenceNodeRegistry<R> extends AbstractNodeRegistry<ReferenceNodeEntryIF<R>>{

	@Getter
	private final R ref;
	
	
	public ReferenceNodeRegistry(R ref, Map<String, ReferenceNodeEntryIF<R>> map, AbstractNodeRegistry<?> parent) {
	
		super(parent, map);
		
		this.ref = ref;
	}
	
	@Override
	public ReferenceNodeEntryIF<R> findEntry(String name, NodeCategory type) {
		
		ReferenceNodeEntryIF<R> e = super.findEntry(name, type);
		
		return e == null ? null : new ReferenceNodeEntry.Wrapper<>(this, e);
	}
	
	
	@Override
	public Module createInstance(ReferenceNodeEntryIF<R> entry) {
		Module m = entry.createInstance(ref);
		m.setName(entry.getName());
		return m;
	}
	
	
	@Override
	public Collection<? extends ReferenceNodeEntryIF<R>> loadEntries() {
	
		Collection<ReferenceNodeEntryIF<R>> en = 
			Collections2.transform(
					getEntryMap().values(), 
				e -> new ReferenceNodeEntry.Wrapper<R>(this, e)
			);
		
		return en;
	}

	
}
