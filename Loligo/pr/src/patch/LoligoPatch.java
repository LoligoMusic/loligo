package patch;

import java.net.URL;
import java.lang.Thread;
import audio.AudioOut;
import constants.PatchEvent;
import constants.PatchEvent.Type;
import constants.PatchEventManager;
import constraints.ConstraintContext;
import errors.ErrorHandler;
import gui.modulmenu.NodeMenu;
import history.History;
import lombok.Getter;
import lombok.Setter;
import module.ModuleManager;
import module.loading.AbstractNodeRegistry;
import module.loading.NodeEntry;
import module.signalFlow.SignalFlow;
import module.signalFlow.SignalFlowIF;
import pr.DisplayManager;
import pr.DisplayManagerIF;
import pr.RootClass;
import save.PatchData;
import save.SaveDataContext;


public class LoligoPatch extends AbstractDomain{

	
	static public void createSecondaryWindow(PatchBuilder pBuilder) {
		
		Loligo proc = new Loligo();
		//proc.setRenderer(PConstants.JAVA2D);
		proc.setFps(30);
		proc.startSketch();
		
		createPatch(pBuilder, proc);
		
	}
	
	
	static private void createPatch(PatchBuilder pBuilder, Loligo proc) {
		
		new Thread(() -> {
			
			LoligoPatch patch = 
				pBuilder
				.setProc(proc)
				.createPatch();
			
			proc.registerDisplay(patch.getDisplayManager());
			patch.secondaryWindow();
		
		}).run();
	}
	
	
	
	private final static String DEFAULT_NAME = "New Patch";
	
	
	
	
	@Getter
	private Loligo pApplet;
	@Getter@Setter
	private LoligoPatch parentPatch;
	@Getter
	private URL location;
	@Getter
	private boolean patchSaved = true;
	@Getter
	private final ErrorHandler errorHandler = new ErrorHandler();
	@Getter
	private DisplayManagerIF displayManager;
	@Getter
	private ConstraintContext constraintContext;
	@Getter
	private AudioOut audioManager;
	@Getter
	private final SignalFlowIF signalFlow = new SignalFlow();
	@Getter
	private boolean disposed = false;
	
	@Setter@Getter
	private AbstractNodeRegistry<? extends NodeEntry> nodeRegistry;
	@SuppressWarnings("unused")
	private NodeMenu nodeMenu;
	@Getter
	private final ModuleManager moduleManager;
	@Getter
	private final History history;
	@Getter@Setter
	private int currentModuleID;
	@Getter
	private String title = DEFAULT_NAME;
	
	
	public LoligoPatch() {
		moduleManager = new ModuleManager(this);
		history = new History(this);
	}
	
	
	public void init() {
		displayManager = initDisplayManager();
		audioManager = initAudioManager();
		
		//audioManager.initAudio();
		
		constraintContext = ConstraintContext.create(this);
		nodeMenu = new NodeMenu(this);
		
		history.init();
		history.setOnRunAction(a -> setPatchSaved(false));
	}
	
	public DisplayManagerIF initDisplayManager() {
		var dm = new DisplayManager(this);
		dm.setDm(dm);
		return dm;
	}
	
	public AudioOut initAudioManager() {
		var ao = new AudioOut();
		ao.connect();
		return ao;
	}
	
	
	public void setProc(Loligo proc) {
		this.pApplet = proc;
		displayManager.initGraphics(proc.g);
	}
	

	@Override
	public void reset() {
		super.reset();
		currentModuleID = 0;
		patchSaved = true;
		title = DEFAULT_NAME;
		setLocation(null);
	}
	
	
	public void setLocation(URL url) {
		this.location = url;
		setTitle(url == null ? title : url.getFile());
	}
	
	public void setPatchSaved(boolean patchSaved) {
		if(this.patchSaved != patchSaved) {
			this.patchSaved = patchSaved;
			setTitle(title);
		}
	}
	
	public void setTitle(String title) {
		this.title = title;
		
		if(pApplet != null) {
			String n = title + (patchSaved ? "" : "*");	
			n = n == null ? "New File" : n; 
			pApplet.getSurface().setTitle(n + " - Loligo");
		}
		PatchEventManager.instance.dispatchPatchEvent(this, Type.PATCH_RENAMED);
	}
	
	/**
	 * Kills secondary window while keeping this Patch and Main-Patch alive
	 */
	public void killWindow() {
		pApplet.getWindow().destroy();
		PatchEventManager.instance.dispatchPatchEvent(new PatchEvent(this, PatchEvent.Type.KILL_WINDOW));
	}
	
	@Override
	public void dispose() {
		super.dispose();
		killWindow();
		RootClass.backstage().removePatchId(domainID);
	}
	
	public void onDispose() {
		disposed = true;
		if(domainType == DomainType.MAIN)
			reset();
		dispose();
	}
	
	
	public void secondaryWindow() {
		displayManager.pauseProcessing(true);
	}

	@Override
	public void doDeserialize(PatchData data, SaveDataContext sdc) {
		
	}
	
	@Override
	public int getNewModuleID() {
		return currentModuleID++;
	}
	
	@Override
	public String toString() {
		return getTitle();
	}
}
