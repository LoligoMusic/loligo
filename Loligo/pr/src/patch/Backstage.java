package patch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import constants.PatchEvent.Type;
import lombok.Getter;
import constants.PatchEventManager;
import module.loading.NodeRegistry;
import pr.RootClass;
import save.RestoreDataContext;
import save.SaveDataContext;
import save.SaveJson;
import save.SaveMode;
import save.SaveTarget;


public class Backstage {

	private final Map<Integer, Domain> idMap = new HashMap<>();
	
	@Getter
	private final List<LoligoPatch> patches = new ArrayList<>();
	private final Loligo proc;
	private LoligoPatch activePatch;
	
	
	public Backstage(Loligo proc) {
		this.proc = proc;
	}
	
	
	public void init() {
		var p = createPatch();
		//loadPatch(p);
		proc.registerDisplay(p.getDisplayManager());
		
		proc.setWindowDestroyListener(this::endSession);
	}

	
	public PatchBuilder patchBuilder() {
		var pb = new PatchBuilder()
			.setNodeRegistry(NodeRegistry.DEFAULT_NODEREGISTRY)
			.setProc(proc)
			.setTitle("New Patch-" + patches.size())
			.setDomainType(DomainType.MAIN);
		return pb;
	}
	
	
	public LoligoPatch createPatch() {
		return createPatch(patchBuilder());
	}
	
	
	public LoligoPatch createPatch(PatchBuilder pb) {
		
		var patch = pb.createPatch();
		patches.add(patch);
		loadPatch2(patch);
		PatchEventManager.instance.dispatchPatchEvent(patch, Type.PATCH_CREATED);
		PatchEventManager.instance.dispatchPatchFocusEvent(patch, true);
		return patch;
	}
	
	
	public void loadPatch(LoligoPatch p) {
		loadPatch2(p);
		PatchEventManager.instance.dispatchPatchFocusEvent(p, true);
	}
	
	private void loadPatch2(LoligoPatch p) {
		
		if(activePatch != null) {
			PatchEventManager.instance.dispatchPatchFocusEvent(activePatch, false);
			activePatch.getAudioManager().disconnect();
			activePatch.getDisplayManager().removedFromDisplay();
		}
		activePatch = p;
		proc.registerDisplay(p.getDisplayManager());
		RootClass.signalFlowManager().setMain(p.getSignalFlow());
		
		p.getAudioManager().connect();
		
		p.getDisplayManager().getInput().reset();
		
		p.getDisplayManager().addedToDisplay(); //Moved to DisplayContainer.initGraphics
	}
	
	
	
	public void closePatch(LoligoPatch p) {
		
		boolean b = SaveJson.checkSaved(p);
		
		if(b) {
			p.reset();
			int idx = patches.indexOf(p);
			patches.remove(idx);
			idMap.remove(p.getDomainID());
			PatchEventManager.instance.dispatchPatchEvent(p, Type.PATCH_REMOVED);
			
			if(patches.isEmpty()) {
				createPatch();
			}else {
				idx = Math.min(idx, patches.size() - 1);
				var np = patches.get(idx);
				loadPatch(np);
			}
		}
	}
	
	
	public void nextPatch() {
		int i = patches.indexOf(activePatch);
		i = (i + 1) % patches.size();
		var p = patches.get(i);
		loadPatch(p);
	}

	
	public LoligoPatch getActivePatch() {
		return activePatch;
	}
	
	public void reloadSession() {
		new ArrayList<>(patches).stream().forEach(this::reloadPatch);
	}
	
	public void reloadPatch(LoligoPatch patch) {
		var sdc = new SaveDataContext(SaveMode.SERIALIZE);
		sdc.addPatch(patch);
		sdc.setSaveTarget(SaveTarget.MEMENTO);
		var pd = sdc.buildPatchData();
		
		closePatch(patch);
		
		var p = createPatch();
		
		var rdc = new RestoreDataContext(pd, p);
		rdc.restorePatch();
		
	}
		
	public boolean endSession() {
		
		for(var p : patches) 
			if(!SaveJson.checkSaved(p)) 
				return false;
		return true;
	}
	
	
	public List<Domain> getAllPatches() {
		return idMap.entrySet().stream()
					.map(e -> e.getValue())
					.collect(Collectors.toList());
	}
	
	
	public Domain getPatchFromId(int id) {
		return idMap.get(id);
	}
	
	
	public void putPatchId(int id, Domain p) {
		idMap.put(id, p);
	}
	
	
	public void removePatchId(int id) {
		idMap.remove(id);
	}
	
}
