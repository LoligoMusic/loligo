package patch;

import static constants.ModuleEvent.MODULE_ADD;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.ModuleDeleteMode;
import save.PatchData;
import save.SaveDataContext;

public abstract class AbstractDomain implements Domain {

	@Setter
	protected DomainType domainType = DomainType.MAIN;
	protected String name = "NewPatch";
	protected final List<Module> modules = new ArrayList<>();
	@Getter@Setter
	protected int domainID;
	
	
	public AbstractDomain() {
		super();
	}

	
	@Override
	public boolean addModule(Module m) {
		
		if(modules.contains(m)) 
			return false;
		
		modules.add(m);
		
		if(m.getModuleID() < 0)
			m.setModuleID(getNewModuleID());
		
		m.setDomain(this);
		m.postInit();
		
		getDisplayManager().addModuleDisplay(m);
		getSignalFlow().addModule(m);
		
		getModuleManager().dispatchModuleEvent(MODULE_ADD, m);
		m.onAdd();
		
		return true;
	}
	
	
	@Override
	public boolean removeModule(Module m, ModuleDeleteMode mode) {
		
		if(!modules.remove(m))
			return false;
		
		var man = getModuleManager();
		man.removeAllLinks(m);
		man.doDelete(m, mode);
		
		return true;
	}
	
	
	@Override
	public List<? extends Module> getContainedModules() {
		return modules;
	}
	
	public DomainType domainType() {
		return domainType;
	}

	@Override
	public void reset() {
		
		var h = getHistory();
		h.clear();
		h.setActive(false);
		getSignalFlow().suspendBuild();
		
		new ArrayList<>(getContainedModules()).forEach(m -> this.removeModule(m, ModuleDeleteMode.PATCH));
		
		h.setActive(true );
		getSignalFlow().resumeBuild();
	}
	
	
	@Override
	public boolean isProcessActive() {
		return getDisplayManager().isProcessActive();
	}
	
	@Override
	public void dispose() {
		
		for (Module m : modules) {
			m.onDelete(ModuleDeleteMode.PATCH);
		}
	}
	
	@Override
	public int getNewModuleID() {
		return -1;
	}
	
	@Override
	public void setCurrentModuleID(int id) { }
	
	@Override
	public int getCurrentModuleID() {return -1;}
	
	@Override
	public void doDeserialize(PatchData data, SaveDataContext sdc) {
		
	}
}