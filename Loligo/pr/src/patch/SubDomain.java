package patch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import audio.AudioOut;
import constants.Flow;
import constraints.ConstraintContext;
import errors.ErrorHandler;
import history.GuiHistory;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import module.Module;
import module.ModuleDeleteMode;
import module.ModuleManager;
import module.Readable;
import module.loading.AbstractNodeRegistry;
import module.loading.NodeEntry;
import module.signalFlow.ImmutableSignalFlow;
import module.signalFlow.SignalFlowContainer;
import module.signalFlow.SignalFlowIF;
import pr.DisplayManagerIF;
import pr.PassiveDisplayManager;


public class SubDomain extends AbstractDomain {

	
	public static SubDomain create(Module[] mds, Domain parent, SignalFlowContainer container, AudioOut audio, AbstractNodeRegistry<? extends NodeEntry> nodeRegistry, DomainType domainType) {
		val sd = new SubDomain(parent, container, audio, domainType);
		sd.setDisplayManager(new PassiveDisplayManager(sd));
		return create(sd, mds, nodeRegistry);
	}	
	
	public static SubDomain create(SubDomain sd, Module[] mds, AbstractNodeRegistry<? extends NodeEntry> nodeRegistry) {
	
		sd.setNodeRegistry(nodeRegistry);  //TODO supply SubDomain Builder _with SignalFLowContainer
		
		ImmutableSignalFlow isf = new ImmutableSignalFlow();
		sd.setSignalFlow(isf);
		
		List<Readable> rs = new ArrayList<>(mds.length);
		for (int i = 0; i < mds.length; i++) { 
			Module m = mds[i];
			if(m.checkFlag(Flow.READER)) {
				rs.add((Readable) m);
			}
		}
		Readable[] readers = rs.toArray(new Readable[rs.size()]);
		
		isf.setModules(mds);
		isf.setReaders(readers);
		
		isf.process();
		
		return sd;
	}
	
	
	@Getter
	private final SignalFlowContainer container;
	@Getter
	private final Domain parentDomain;
	
	@Getter@Setter
	private DisplayManagerIF displayManager;
	@Getter
	private final AudioOut audioManager;
	@Getter@Setter
	private SignalFlowIF signalFlow;
	
	@Getter@Setter
	private AbstractNodeRegistry<? extends NodeEntry> nodeRegistry;
	
	
	public SubDomain(Domain parent, SignalFlowContainer container) {
		
		this(parent, container, parent.getAudioManager(), DomainType.SUBPATCH);
	}
	
	public SubDomain(Domain parent, SignalFlowContainer container, AudioOut audioManager, DomainType domainType) {
		
		this.parentDomain = parent;
		this.container = container;
		this.audioManager = audioManager; 
		this.domainType = domainType; 
	}
	
	/*
	public void setAnimContainer(DisplayObjectIF d) {
		
		displayManager.setAnimContainer(d);
	}
	 */
	
	@Override
	public List<? extends Module> getContainedModules() {
		return Collections.unmodifiableList(getSignalFlow().getFlowList());
	}

	
	@Override
	public Loligo getPApplet() {
		return parentDomain.getPApplet();
	}

	
	public void removeFromContainer() {
		
		for (Module m : modules.toArray(new Module[modules.size()])) {
			
			container.removeModule(m);
			//if(m.checkFlag(Flow.ALIVE))
			m.onDelete(ModuleDeleteMode.PARTICLE_INSTANCE);
		}
		
		container.removeBlock(this);
	}
	


	@Override
	public ModuleManager getModuleManager() {
		return parentDomain.getModuleManager();
	}

	@Override
	public ConstraintContext getConstraintContext() {
		return parentDomain.getConstraintContext();
	}
	
	@Override
	public GuiHistory getHistory() {
		return parentDomain.getHistory();
	}

	@Override
	public ErrorHandler getErrorHandler() {
		return parentDomain.getErrorHandler();
	}

}
