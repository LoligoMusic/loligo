package patch;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import processing.awt.PSurfaceAWT.SmoothCanvas;

public class ProcWindowJava2D implements ProcWindow {

	private final Frame frame;
	
	private Consumer<ProcWindow> onDestroy;
	private Consumer<Boolean> onFocus = b -> {};
	private BiConsumer<Integer, Integer> onResize = (w, h) -> {};
	
	public ProcWindowJava2D(SmoothCanvas canvas) {
		frame = canvas.getFrame();
		frame.setResizable(true);
	}
	
	@Override
	public int getX() {
		return frame.getLocationOnScreen().x;
	}

	@Override
	public int getY() {
		return frame.getLocationOnScreen().y;
	}

	@Override
	public int getWidth() {
		return frame.getWidth();
	}

	@Override
	public int getHeight() {
		return frame.getHeight();
	}

	@Override
	public void setPosition(int x, int y) {
		frame.setLocation(x, y);
	}

	@Override
	public void setSize(int w, int h) {
		frame.setSize(w, h);
	}

	
	@Override
	public void initWindowListener() {
		
		var wa = new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				onDestroy.accept(ProcWindowJava2D.this);
			}
			
			@Override
			public void windowGainedFocus(WindowEvent e) {
				onFocus.accept(true);
			}
			
			@Override
			public void windowLostFocus(WindowEvent e) {
				onFocus.accept(false);
			}
		};
		
		frame.addWindowListener(wa);
	}
	

	@Override
	public void setWindowDestroyNotification(Consumer<ProcWindow> onDestroy) {
		this.onDestroy = onDestroy;
	}

	@Override
	public void setWindowDestroyListener(Consumer<ProcWindow> onDestroy) {
		
	}

	@Override
	public void setFocusListener(Consumer<Boolean> onFocus) {
		this.onFocus = onFocus;
	}

	@Override
	public void setResizeListener(BiConsumer<Integer, Integer> onResize) {
		this.onResize = onResize;
	}

	@Override
	public void requestFocus() {
		frame.requestFocus();
	}

	@Override
	public void destroy() {
		frame.dispose();
	}

}
