package patch;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import com.jogamp.nativewindow.WindowClosingProtocol.WindowClosingMode;
import com.jogamp.newt.event.WindowEvent;
import com.jogamp.newt.event.WindowListener;
import com.jogamp.newt.event.WindowUpdateEvent;
import com.jogamp.newt.opengl.GLWindow;
import lombok.Setter;


public class ProcWindowGL implements ProcWindow {

	private final GLWindow window;
	
	private Consumer<ProcWindow> onDestroy = w -> {};
	@Setter
	private Consumer<Boolean> onFocus = b -> {};
	private BiConsumer<Integer, Integer> onResize = (w, h) -> {};
	
	
	public ProcWindowGL(GLWindow window) {
		this.window = window;
		window.setDefaultCloseOperation(WindowClosingMode.DISPOSE_ON_CLOSE);
		window.setAlwaysOnTop(false);
	}
	
	@Override
	public void setResizeListener(BiConsumer<Integer, Integer> onResize) {
		this.onResize = onResize;
	}
	
	@Override
	public void setFocusListener(Consumer<Boolean> onFocus) {
		this.onFocus = onFocus;
	}
	
	@Override
	public int getX() {
		return window.getX();
	}

	@Override
	public int getY() {
		return window.getY();
	}

	@Override
	public int getWidth() {
		return window.getWidth();
	}

	@Override
	public int getHeight() {
		return window.getHeight();
	}

	@Override
	public void setPosition(int x, int y) {
		var p = window.convertToWindowUnits(new int[] {x, y});
		window.setPosition(p[0], p[1]);
	}
	
	@Override 
	public void setSize(int w, int h) {
		window.setSize(w, h);
	}

	@Override
	public void initWindowListener() {

		for(WindowListener l : window.getWindowListeners()) {
			if(l.getClass().getName().startsWith("processing"))
				window.removeWindowListener(l);
		}
		window.addWindowListener(new WindowAdapterNEWT());
	}

	@Override
	public void setWindowDestroyListener(Consumer<ProcWindow> onDestroy) {
		this.onDestroy = onDestroy;
	}
	
	@Override
	public void setWindowDestroyNotification(Consumer<ProcWindow> cb) {
		window.setWindowDestroyNotifyAction(() -> cb.accept(this));
	}

	@Override
	public void requestFocus() {
		window.requestFocus();
	}

	@Override
	public void destroy() {
		window.destroy();
	}
	
	
	
	public class WindowAdapterNEWT implements WindowListener {

		@Override
		public void windowResized(WindowEvent e) {
			onResize.accept(getWidth(), getHeight());
		}

		@Override
		public void windowMoved(WindowEvent e) {
			
		}

		@Override
		public void windowDestroyNotify(WindowEvent e) {
			
			//noLoop();
			
			//unregisterMethod("pre", this);
			//unregisterMethod("draw", this);
			//unregisterMethod("post", this);
			//unregisterMethod("mouseEvent", this);
			//unregisterMethod("keyEvent", this);
			
			for (WindowListener wl : window.getWindowListeners()) {
				
				window.removeWindowListener(wl);
			}
			
			onDestroy.accept(ProcWindowGL.this);
		}
		

		@Override
		public void windowDestroyed(WindowEvent e) {
			
		}

		@Override
		public void windowGainedFocus(WindowEvent e) {
			
			onFocus.accept(true);
		}

		@Override
		public void windowLostFocus(WindowEvent e) {
			
			onFocus.accept(false);
		}

		@Override
		public void windowRepaint(WindowUpdateEvent e) {
			
		}

	}
	

}
