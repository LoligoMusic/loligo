package sound;

import gui.DisplayGuiSender;
import gui.DropDownMenu;
import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.ugens.Add;
import net.beadsproject.beads.ugens.Static;
import net.beadsproject.beads.ugens.WavePlayer;

public class Wave {

	private static String[] bufferNames = Buffer.staticBufs.keySet().toArray(new String[Buffer.staticBufs.keySet().size()]);
	
	private Buffer buffer;
	private DropDownMenu bufferMenu;
	private int bufferIndex = bufferNames.length - 1;
	
	public Wave() {
		setBuffer(bufferIndex);
	}
	
	public DisplayGuiSender<Integer> getGui() {
		
		bufferMenu = new DropDownMenu(this::setBuffer, ()->bufferIndex, bufferNames);
		
		//bufferMenu.setWH(50, bufferMenu.getHeight());
		bufferMenu.adaptSize();
		bufferMenu.update();
		
		return bufferMenu;
	}
	
	public WavePlayer buildWave(AudioContext context, float frequency, UGen freqUgen) {
		
		if(freqUgen == null) {
			
			return new WavePlayer(context, frequency, buffer);
		}else {
			 
			Static s = new Static(context, frequency);
			Add freq = new Add(context, s, freqUgen);
			return new WavePlayer(context, freq, buffer);
		}
		
	}
	
	
	public void setBuffer(int i) {
		
		bufferIndex = i;
		buffer = Buffer.staticBufs.get(bufferNames[bufferIndex]);
		
		if(bufferMenu != null)
			bufferMenu.update();
	}
	
	public void setBuffer(String bufName) {
		
		int idx = 0;
		for (int i = 0; i < bufferNames.length; i++) 
			if(bufferNames[i].equals(bufName)) {
				idx = i;
				break;
			}
		
		setBuffer(idx);
	}
	
	public String getCurrentBufferName() {
		
		return bufferNames[bufferIndex];
	}
	
}
