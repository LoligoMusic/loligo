package sound;

import java.util.Map;
import java.util.function.Function;

import constants.DisplayFlag;
import gui.Dial;
import lombok.Getter;
import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.Bead;
import net.beadsproject.beads.ugens.Envelope;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PConstants;
import processing.core.PGraphics;
import util.Colors;


public class ADSR {
	
	private static final float MAXTIME = 1000;
	@Getter
	private float attackTime, decayTime, sustainTime, releaseTime, volume, sustainVolume;
	private Dial attackDial, decayDial, sustainDial, releaseDial, sustainVolDial;
	
	
	public ADSR() {
		
		attackTime = 10;
		decayTime = 10;
		volume = 1;
		sustainTime = 20;
		releaseTime = 500;
	}
	
	public DisplayObject[] getGui(){
		
		Function<Double, Float> ac = d -> 0 + d.floatValue() * MAXTIME;
		Function<Float, Double> up = d -> d.doubleValue() / MAXTIME;
		
		attackDial 		= new Dial(d -> setAttack	(ac.apply(d)), () -> up.apply(attackTime)	, "A");
		decayDial 		= new Dial(d -> setDecay	(ac.apply(d)), () -> up.apply(decayTime)	, "D");
		sustainDial 	= new Dial(d -> setSustain	(ac.apply(d)), () -> up.apply(sustainTime)	, "S");
		releaseDial 	= new Dial(d -> setRelease	(ac.apply(d)), () -> up.apply(releaseTime)	, "R");
		sustainVolDial  = new Dial(d -> setSustainVolume(d.floatValue()), ()-> (double) sustainVolume, "V");
		
		updateDials();
		Dial[] dials = {attackDial, decayDial, sustainDial, releaseDial, sustainVolDial};
		
		String[] mots = {"Attack", "Decay", "Sustain", "Release", "Volume"};
		for (int i = 0; i < mots.length; i++) 
			dials[i].setMouseOverText(mots[i]);
		
		return dials;
	}
	
	
	public void updateDials() {
		attackDial.update();
		decayDial.update();
		sustainDial.update();
		releaseDial.update();
		sustainVolDial.update();
	}
	
	
	public DisplayObjectIF getGraph() {
		
		DisplayObject g = new DisplayObject() {
			
			float all, sus, w, h, x, y;
			
			@Override
			public void render(PGraphics g) {
				
				g.fill(getColor());
				g.rect(getX(), getY(), getWidth(), getHeight());
				
				float s = 1f;
				
				h = getHeight();
				w = getWidth();
				x = getX();
				y = getY() + h;
				
				float x1 = x, x2 = attackTime * s;
				sus = h * sustainVolume;
				
				all = 4 * MAXTIME; //attackTime + decayTime + sustainTime + releaseTime;
				
				g.rectMode(PConstants.CENTER);
				
				if(attackTime > 0) {
					
					point(g, 0, 0);
					line(g, 0, 0, x2, h, 0xaa1c4766);
				}
				
				if(decayTime > 0) {
					x1 = x2;
					x2 += decayTime * s;
					
					point(g, x1, h);
					point(g, x2, sus);
					line(g, x1, h, x2, sus, 0xaa113866);
				}
				
				if(sustainVolume > 0) {
					x1 = x2;
					x2 += sustainTime * s;
					point(g, x2, sus);
					line(g, x1, sus, x2, sus, 0xaa0b2c66);
					
					if(releaseTime > 0) {
						x1 = x2;
						x2 += releaseTime * s;
						point(g, x2, 0);
						line(g, x1, sus, x2, 0, 0xaa072366);
					}
				}
				
				g.noStroke();
				g.fill(0xff999999);
				g.rectMode(PConstants.CORNER);
				
				//g.text(((int) all) + "ms", getX() + getWidth() - 45, getY() + getHeight() - 5);
				
				
			}
			
			void point(PGraphics g, float px, float py) {
				
				//g.rect(x + w * px / all, y - py, 2, 2);
			}
			
			void line(PGraphics g, float x1, float y1, float x2, float y2, int c) {
				
				float xf1 = x + w * x1 / all,
					  yf1 =	y - y1 ,
					  xf2 = x + w * x2 / all,
					  yf2 = y - y2;
				
				g.fill(c);
				g.beginShape();
				g.vertex(xf1, y);
				g.vertex(xf1, yf1);
				g.vertex(xf2, yf2);
				g.vertex(xf2, y);
				g.endShape();
				
				g.strokeWeight(1f);
				g.stroke(Colors.add(c, 50));
				g.line(xf1, yf1, xf2, yf2);
				g.noStroke();
			}
		};
		
		g.setColor(0xff111115);
		g.setFlag(DisplayFlag.RECT, true);
		g.setWH(220, 100);
		
		return g;
	}
	
	public ADSR setAttack(float duration) {
		attackTime = duration;
		if(attackDial != null)
			attackDial.update();
		return this;
	}
	
	public ADSR setDecay(float duration) {
		decayTime = duration;
		if(decayDial != null)
			decayDial.update();
		return this;
	}
	
	public ADSR setSustain(float duration) {
		sustainTime = duration;
		if(sustainDial != null)
			sustainDial.update();
		return this;
	}
	
	public ADSR setVolume(float value) {
		volume = value;
		return this;
	}
	
	
	public ADSR setSustainVolume(float value) {
		sustainVolume = value;
		if(sustainVolDial != null)
			sustainVolDial.update();
		return this;
	}
	
	public ADSR setRelease(float duration) {
		releaseTime = duration;
		if(releaseDial != null)
			releaseDial.update();
		return this; 
	}
	
	public Envelope buildEnvelope(AudioContext ac) {
		return buildEnvelope(ac, null, .5f);
	}
	
	public Envelope buildEnvelope(AudioContext ac, Bead trigger, float sustainFac) {
				
		Envelope e = new Envelope(ac);
		e.addSegment(volume, attackTime);
		float f = volume * sustainVolume;
		e.addSegment(f, decayTime);
		
		if(sustainTime > 0)
			e.addSegment(f, sustainTime * sustainFac);
		
		e.addSegment(0, releaseTime, trigger);
		
		return e;
	}

	/*
	@Override
	public void guiMessage(GuiSender<?> s) {
		
		if(s == attackDial)
			attackTime 	= 2 + attackDial.getValue().floatValue() * MAXTIME;
		else 
		if(s == decayDial)
			decayTime 	= 2 + decayDial.getValue().floatValue() * MAXTIME;
		else 
		if(s == sustainDial)
			sustainTime = sustainDial.getValue().floatValue() * MAXTIME;
		else 
		if(s == releaseDial)
			releaseTime = 2 + releaseDial.getValue().floatValue() * MAXTIME;
		else 
		if(s == sustainVolDial)
			sustainVolume = sustainVolDial.getValue().floatValue();
		//else 
		//if(s == volDial)
		//	volume = volDial.getValue().floatValue();
	}

	@Override
	public void updateSender(GuiSender<?> s) {
		
		if(s == null)
			return;
		
		attackDial		.setValue(new Double(attackTime 	/ MAXTIME));
		decayDial		.setValue(new Double(decayTime 		/ MAXTIME));
		sustainDial		.setValue(new Double(sustainTime 	/ MAXTIME));
		releaseDial		.setValue(new Double(releaseTime 	/ MAXTIME));
		sustainVolDial	.setValue(new Double(sustainVolume 	/ MAXTIME));
	}
	*/
	
	public void duplicateSettings(ADSR dupli) {
		
		dupli.setAttack(attackTime);
		dupli.setDecay(decayTime);
		dupli.setSustain(sustainTime);
		dupli.setRelease(releaseTime);
		dupli.setSustainVolume(sustainVolume);
		//dupli.updateSender(attackDial);
		
	}
	
	public void saveToMap(Map<String, Object> map) {
		
		map.put("attack", attackTime);
		map.put("decay", decayTime);
		map.put("sustain", sustainTime);
		map.put("release", releaseTime);
		map.put("sustainVolume", sustainVolume);
	}
	
	public void restoreFromMap(Map<String, Object> map) {
		
		setAttack(		 ((Number) map.get("attack")		).floatValue());
		setDecay(		 ((Number) map.get("decay")			).floatValue());
		setSustain(		 ((Number) map.get("sustain")		).floatValue());
		setRelease(		 ((Number) map.get("release")		).floatValue());
		setSustainVolume(((Number) map.get("sustainVolume") ).floatValue());
	}
	 
	
}
