package save;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gui.particles.prog.ParticleParam;
import lombok.Getter;
import lombok.Setter;
import patch.DomainType;

@JsonIgnoreProperties(ignoreUnknown = true)

public class PatchData {

	@JsonIgnore
	URI path;
	@Getter
	int version = 1;
	@Getter@Setter
	DomainType patchType;
	@Getter
	SaveTarget saveTarget;
	@Getter@Setter
	int id;
	String name, comment;
	@Getter@Setter
	int width, height;
	public int currentNodeId;
	@Getter
	List<ModuleData> nodes = new ArrayList<>();
	@Getter
	List<LinkData> links = new ArrayList<>();
	@Getter
	List<PathData> paths = new ArrayList<>();
	List<ConstrainableData> constrainables = new ArrayList<>();
	@Getter
	List<StackData> stacks = new ArrayList<>();
	List<String> assets;
	@Getter
	ParticleSystemData[] particleSystems;
	@Getter@Setter
	List<ParticleParam> particleParams; // Params for ParticlePatch
	
	
	
	public void addNode(ModuleData md) {
		nodes.add(md);
	}

	
	public List<ModuleData> getNodes(Predicate<ModuleData> filter) {
		return
		nodes.stream()
			 .filter(filter)
			 .toList();
	}
	 
	
	public void moduleInToOut() {
		nodes.forEach(ModuleData::moduleInToOut);
	}

	
	public void addLink(LinkData ld) {
		links.add(ld);
	}
	
	
	public void addAssets(Collection<String> assets) {
		assets = new ArrayList<>(assets);
	}

	
	public ModuleData getModuleByID(int id) {
		for(ModuleData md : nodes) {
			if(md.id == id)
				return md;
		}
		return null;
	}
}
