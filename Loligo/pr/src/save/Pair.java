package save;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Pair {
	
	@Getter@Setter
	public Object key, value;
	
	public static class Serializer extends StdSerializer<Object> {
		private static final long serialVersionUID = 1L;

		public Serializer() {
			this(null);
	    }
	   
	    public Serializer(Class<Object> t) {
	        super(t);
	    }

		@Override
		public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			
			//gen.writeString(h);
		}
	}
	
	
	public static class Deserializer extends StdDeserializer<Object> {
		private static final long serialVersionUID = 1L;

		public Deserializer() {
			this(null);
		}
		
		protected Deserializer(Class<Object> vc) {
			super(vc);
		}

		@Override
		public Integer deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
			String h = p.getText();
			int c = Integer.parseUnsignedInt(h, 16);
			return c;
		}
		
	}
}
