package save;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import javax.swing.JOptionPane;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

import gui.WaitingAnimation;
import module.Module;
import module.Modules;
import module.modules.particles.Shooter;
import patch.LoligoPatch;
import patch.subpatch.SubPatch;
import pr.RootClass;
import util.AsyncFileDialog;
import util.ConditionWatcher;
import util.Utils;



public final class SaveJson {

	public static final String LOLIGO_FILEENDING = "lol";
	public static final Charset CHARSET = Charset.forName("UTF-8");
	

	private SaveJson() {}
	
	private static Optional<File> lastSaveDir = Optional.empty();
	
	
	public static boolean checkSaved(LoligoPatch patch) {
		
		if(!patch.isPatchSaved()) {
			
			var w = patch.getPApplet().getWindow();
			var jf = Utils.newJFrameBasic(w);
			String msg = "Unsaved changes in " + patch.getTitle() + ". Save before closing?";
			int r = JOptionPane.showConfirmDialog(jf, msg, "", JOptionPane.YES_NO_CANCEL_OPTION);
			jf.dispose();
			
			switch(r) {
			case JOptionPane.YES_OPTION:
				SaveJson.createPatchSavePrompt(patch).run();
			case JOptionPane.NO_OPTION:
				return true;
			case JOptionPane.CANCEL_OPTION:
				return false;
			}
		}
		
		return true;
	}
	
	
	public static void autoSavePatch(LoligoPatch patch) {
		
		URL u = patch.getLocation();
		
		if(u != null) {
			try {
				File f = new File(u.toURI());
				writePatchToFile(patch, f);
			} catch (URISyntaxException e) {
				runPatchSavePrompt(patch);
			}
			
		}else {
			runPatchSavePrompt(patch);
		}
		
	}
	
	
	public static void runPatchSavePrompt(LoligoPatch patch) {
		var afd = createPatchSavePrompt(patch);
		afd.runAsync();
	}
	
	public static AsyncFileDialog createPatchSavePrompt(LoligoPatch patch) {
		
		Consumer<File[]> onReturn = files -> {
			
			if(files != null && files.length > 0 && files[0] != null) { 
				var f = files[0];
				lastSaveDir = Optional.of(f);
				writePatchToFile(patch, f);
			}
		};
		
		var afd = new AsyncFileDialog(patch.getDisplayManager(), onReturn);
		afd.setDialogTitle("Save Patch..");
		afd.useSaveDialog();
		lastSaveDir.ifPresent(afd::setDirectory);
		afd.setFileDescription("Loligo Patch");
		
		URL u = patch.getLocation();
		if(u != null) {
			try {
				File f = new File(u.toURI());
				afd.setDirectory(f);
			}catch(URISyntaxException e) {
			}
		}
		return afd;
	}
	
	
	public static void writePatchToFile(LoligoPatch patch, File file) {
		
		String str = savePatch(patch);
		writePatchToFile(str, file);
		try {
			patch.setLocation(file.toURI().toURL());
		} catch (MalformedURLException e) {}
		patch.setPatchSaved(true);
	}
	

	public static void writePatchToFile(String json, File file) {
		
			String n = file.getName();
			String e = '.' + LOLIGO_FILEENDING;
			if(!n.endsWith(e)) {
				n = n + e;
				var f = new File(file.getParentFile(), n);
				file.renameTo(f);
			}
			writeFile(json, file);
	}
	
	public static void writeFile(String t, File f) {
		try {
			com.google.common.io.Files.write(t, f, CHARSET);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void runPatchLoadPrompt(LoligoPatch patch) {
		
		Consumer<File[]> onReturn = files -> {
			
			if(files == null || files.length == 0 || files[0] == null) 
				return;
			 
			File f = files[0];
			lastSaveDir = Optional.of(f);
			
			loadPatchFromFile(f);
			
		};
		
		var afd = new AsyncFileDialog(patch.getDisplayManager(), onReturn);
		afd.setDialogTitle("Open Patch..");
		afd.setFileEndings(LOLIGO_FILEENDING);
		afd.setFileDescription("Loligo Patch");
		lastSaveDir.ifPresent(afd::setDirectory);
		afd.runAsync();
		
	}
	
	
	public static void loadPatchFromFile(File file) {
		
		try {
			URL u = file.toURI().toURL();
			for(var p : RootClass.backstage().getPatches()) {
				var loc = p.getLocation();
				if(loc != null && loc.equals(u)) {
					RootClass.backstage().loadPatch(p);
					return;
				}
			}
			
			loadPatchFromStream(u, pa -> pa.setLocation(u));
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public static void loadPatchFromStream(URL url) {
		loadPatchFromStream(url, p -> {});
	}
	
	public static void loadPatchFromStream(URL url, Consumer<LoligoPatch> callback) {
		try {
			var in = url.openStream();
			loadPatchFromStream(in, url, callback);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static String loadJsonFromStream(InputStream in) {
		
		var isr = new InputStreamReader(in, CHARSET);
		var bsr = new BufferedReader(isr);
		String s;
		var sb = new StringBuilder();
		
		try {
			while((s = bsr.readLine()) != null) {
				sb.append(s);
			}
		} catch(IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return sb.toString() ;
	}
	
	/**
	 * Reads patch asynchronously from a file and restores it from the patch's animation thread.
	 * @param patch
	 * @param file
	 */
	public static void loadPatchFromStream(InputStream in, URL url, Consumer<LoligoPatch> patchCallback) {
		
		//patch.getAudioManager().getMasterBead().clearInputConnections();
		//ParticleSystemRegistry.defaultParticleSystem().clearParticles();
		
		PatchData[] patchData = {null};
		
		new Thread(() -> {
			
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			try {
				String js = loadJsonFromStream(in);
				var pd = parseJsonToPatch(js);
				UpdateRegistry.update(pd);
				patchData[0] = pd;
				
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		
		}).start();
		
		BooleanSupplier fin = () -> patchData[0] != null;
		var patch = RootClass.backstage().getActivePatch();
		
		Runnable r = () -> {
			var pd = patchData[0];
			switch(pd.patchType) {
			case SUBPATCH		  -> loadSub(pd, patch);
			case PARTICLE_FACTORY -> loadShooter(pd, patch);
			default -> {
				var p = RootClass.backstage().createPatch();
				var rdc = new RestoreDataContext(pd, p);
				rdc.setLocation(url);
				loadNewPatch(rdc, url);
				patchCallback.accept(p);
			}}
		};
		showWaitingAnimation(patch, fin, r);
		
	}
	
	
	public static Module loadShooter(PatchData pd, LoligoPatch patch) {
		var m = patch.getNodeRegistry().createInstance(Shooter.class);
		m.setMasterPatch(pd);
		Modules.initializeModuleDefault(m, patch);
		var mp = patch.getDisplayManager().getInput().mousePos;
		m.getModuleGUI().setPos(mp.getX(), mp.getY());
		return m;
	}
	
	
	public static Module loadSub(PatchData pd, LoligoPatch patch) {
		var m = patch.getNodeRegistry().createInstance(SubPatch.class);
		Modules.initializeModuleDefault(m, patch);
		m.restorePatch(pd);
		var mp = patch.getDisplayManager().getInput().mousePos;
		m.getModuleGUI().setPos(mp.getX(), mp.getY());
		return m;
	}
	
	
	public static void showWaitingAnimation(LoligoPatch patch, BooleanSupplier finishCondition, Runnable finishAction) {
		
		var message = new WaitingAnimation().start(patch);
		
		new ConditionWatcher(patch.getDisplayManager(), finishCondition, 
				() -> {
					message.removeAnimation();
					if(finishAction != null)
						finishAction.run();
				}
			).run();
	}
	
	
	/**
	 * Offical way to load new patch. Resets blank patch + loads new one in a gracious way.
	 * @param rdc
	 */
	public static void loadNewPatch(RestoreDataContext rdc, URL url) {
		
		//TODO: Reset to blank patch, delete existing nodes + remnands in memory
		//TODO: reset + suspend history 
		//TODO suspend flow control
		
		rdc.getDomain().reset();
		
		rdc.restorePatch();
		
		if(url != null)
			rdc.getDomain().setLocation(url);
		
		//DisplayObjects.removeFromDisplay(message);
	}
	
	
	public static String savePatch(LoligoPatch patch) {
		
		var sdc = new SaveDataContext(SaveMode.SERIALIZE);
		sdc.addPatch(patch);
		sdc.setSaveTarget(SaveTarget.FILE);
		
		var pd = sdc.buildPatchData();
		
		String json = SaveJson.toJsonString(pd);
		
		return json;
	}
	
	
	public static void savetest() {
		
		var sdc = new SaveDataContext(SaveMode.SERIALIZE);
		sdc.addPatch(RootClass.mainPatch());
		sdc.setSaveTarget(SaveTarget.FILE);
		
		var patch = sdc.buildPatchData();
		
		String json = SaveJson.toJsonString(patch);
		
		PatchData pd = null;
		
		try {
			pd = SaveJson.parseJsonToPatch(json);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new RestoreDataContext(pd, RootClass.mainPatch()).restorePatch();
		
		System.out.println(json);
	}
	
	public static JsonMapper.Builder defaultObjectMapperBuilder() {
		var b = JsonMapper.builder();
		b.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		b.enable(SerializationFeature.INDENT_OUTPUT);
		b.serializationInclusion(Include.NON_EMPTY);
		b.enable(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY);
		//b.setDefaultTyping(new StdTypeResolverBuilder().)
		return b;
	}
	
	public static ObjectMapper defaultObjectMapper() {
		var mapper = defaultObjectMapperBuilder().build();
		return mapper;
	}
	
	public static String toJsonString(Object o) {
		
		var mapper = defaultObjectMapper();
		
		String json = null;
		
		try {
			json = mapper.writeValueAsString(o);
			System.out.println(json);
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return json;
	}
	
	
	public static String patchToJson(PatchData patch) {
		
		return patchToJson(patch, (com.fasterxml.jackson.databind.Module[]) null);
	}
	
	
	public static String patchToJson(PatchData patch, com.fasterxml.jackson.databind.Module... mods) {
		
		var mapper = defaultObjectMapper();
		
		if(mods != null)
			mapper.registerModules(mods);
		
		String json = null;
		try {
			json = mapper.writeValueAsString(patch);
			System.out.println(json);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	
	public static PatchData parseJsonToPatch(String json) throws IOException {
		var mapper = defaultObjectMapper();
		var p = mapper.readValue(json, PatchData.class);
		prepairPatch(p);
		return p;
	}
	
	public static void prepairPatch(PatchData pd) {
		pd.nodes.forEach(SaveJson::prepairModuleData);
	}
	
	public static void prepairModuleData(ModuleData md) {
		try {
			md.copyExtra();
			var c = Class.forName(md.node);
			var m = c.getDeclaredMethod("prepairData", ModuleData.class);
			m.invoke(null, md);
		} catch(NoSuchMethodException e) {
			
		} catch(ClassNotFoundException | SecurityException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	/*
	public static void zipAsset(ZipOutputStream zout, List<Asset> assets) throws IOException {

		for (Asset asset : assets) { 
			
			for (InputStream is : asset) {

				String p = asset.currentPath("/");

				System.out.println(p);
				ZipEntry entry = new ZipEntry(p);

				zout.putNextEntry(entry);

				try{

					Utils.copyStream(is, zout);
					is.close();
					
				}catch (IOException e) {

					System.err.println("Couldn't copy " + "\"" + p + "\"");
					throw e;
				}
			}
		}
	}
	
	public static void zipString(String json, ZipOutputStream zout, String entryName) throws IOException {
		ZipEntry je = new ZipEntry(entryName);
		zout.putNextEntry(je);
		
		byte[] b = json.getBytes(StandardCharsets.UTF_8);
		
		ByteArrayInputStream bis = new ByteArrayInputStream(b);
			
		Utils.copyStream(bis, zout);
		
		
	}
	
	 
	public static void ziptest() {
		
		try {
			
			SaveDataContext sdc = new SaveDataContext(SaveMode.SERIALIZE);
			sdc.addModules(RootClass.mainPatch().getContainedModules());
			
			saveZipFile(new File("C:/Users/Vanja/Desktop/zapzap.zip"), sdc.buildPatchData());
			
			//List<Asset> li = readZip(new File("C:/Users/Vanja/Desktop/zip.zip"));
			
			//for (Asset a : li) {
			//	a.loadData();
			//}
			
			//System.out.println(li);
			
			//saveToFolder(Paths.get("C:/Users/Vanja/Desktop/fuckfolder/out_folder/"), li);
			
			//zipAsset(new File("C:/Users/Vanja/Desktop/fuckfolder/zip_out.zip"), li);
			
			//writeZip(new File("C:/Users/Vanja/Desktop/zip.zip2"), li);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static void saveZipFile(File file, PatchData patch) throws FileNotFoundException, IOException {
		
		SimpleModule m = new SimpleModule();
		m.addSerializer(AssetData.class, new AssetData.ZipSerializer());
		
		String json = patchToJson(patch, m);
		String nfo = "version=1";
		
		try(
			FileOutputStream fos = new FileOutputStream(file);
			ZipOutputStream zout = new ZipOutputStream(fos);
				) {
			
			zipAsset(zout, AssetRegistry.getAssets());
			
			zipString(json, zout, "root.json");
			zipString(nfo, zout, ".nfo");
		}
	}
	
	
	public static void loadZipFile(File file) {
		
		//parseJsonToPatch(json);
		
	}

	
	public static List<Asset> readZip(File file) throws ZipException, IOException {
		
		try(ZipFile zipFile = new ZipFile(file)) {
		
			List<Asset> assets = new ArrayList<>();
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			
			String keyDir = "", subDir = "";
			
			while (entries.hasMoreElements()) {

				ZipEntry e = (ZipEntry) entries.nextElement();
				
				String name = e.getName();
				
				boolean b = false;
				
				int idx1 = name.indexOf('/');
				
				if(idx1 == -1) {
					
					b = true;
					keyDir = subDir = "";
				}else{
					
					keyDir = name.substring(0, idx1);
					
					int idx2 = name.indexOf('/', idx1 + 1);
					
					if(idx2 > -1) {
						
						String sd = name.substring(idx1 + 1, idx2 + 1);
						
						if(!sd.equals(subDir)) {
							b = true;
							name = name.substring(0, idx2 + 1);
							subDir = sd;
						}
					}else{
						b = true;
						subDir = "";
						
					}
				}
					
				if(b) {
					 
					if(keyDir.equals("img")) {
						
						AssetSource.Zip src = new AssetSource.Zip(file.toPath(), name);
						Asset ad = new ImageData(src);
						assets.add(ad);
					}
				}
			}
			
			return assets;
		}
	}
	
	public static void saveToFolder(Path file, List<Asset> assets) {
		
		for (Asset asset : assets) {
			
			for (InputStream is : asset.getFile().getInputStreams()) {
				
				File f = file.resolve(asset.getFile().getUrl().toString()).toFile();
				
				f.getParentFile().mkdirs();
				
				try(FileOutputStream fos = new FileOutputStream(f);) {
					
					Utils.copyStream(is, fos);
					
				} catch (IOException e) {
					System.err.println("Cant save " + file);
					e.printStackTrace();
				}
			}
		}
	}
	*/
	
}
