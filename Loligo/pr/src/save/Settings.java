package save;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Objects;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.val;
import patch.LoligoPatch;
import pr.RootClass;
import com.google.common.io.Files;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Settings {
	
	public static final String SETTINGS_FILE = "settings.json";
	public static Data DATA;
	
	public static void retain() {
		val d = toData();
		DATA = d;
		String json = SaveJson.toJsonString(d);
		SaveJson.writeFile(json, getSettingsFile());
	}
	
	public static Data toData() {
		Data d = new Data();
		d.fps = RootClass.mainProc().getFps();
		d.soundDevice = RootClass.audioManager().getMixer().getMixerInfo().getName();
		d.tabs = RootClass.backstage().getPatches().stream()
					.map(LoligoPatch::getLocation)
					.filter(Objects::nonNull)
					.map(URL::toExternalForm)
					.toArray(String[]::new);
		System.out.println(d);
		return d;
	}
	
	public static Data readSettingsFile() {
		File f = getSettingsFile();
		if(f.exists()) {
		
			try {
				System.out.println("Read settings from " + f);
				String json = Files.toString(f, Charset.forName("UTF-8"));
				return SaveJson.defaultObjectMapper().readValue(json, Data.class);
			} catch (IOException e) {
				e.printStackTrace();
				return defaultSettings();
			}
		}else {
			return defaultSettings();
		}
	}
	
	public static Data defaultSettings() {
		return new Data();
	}
	
	public static void loadSettings() {
		DATA = readSettingsFile();
	}
	
	public static File getSettingsFile() {
		File dir;
		try {
			dir = new File(Settings.class.getProtectionDomain().getCodeSource().getLocation().toURI());
			return new File(dir, SETTINGS_FILE);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getSoundDevice() {
		return DATA.soundDevice;
	}
	
	@ToString
	public static class Data {
		public int fps = 60;
		public String soundDevice;
		public String[] tabs = new String[0];
	}
}
