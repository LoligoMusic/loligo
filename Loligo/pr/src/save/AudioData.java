package save;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;

import lombok.Getter;
import net.beadsproject.beads.data.Sample;

public class AudioData extends Asset {
	
	@Getter
	private Sample[] samples;
	

	public AudioData(AssetSource file) {
		this((Sample[]) null, file);
	}
	
	public AudioData(Sample sample, AssetSource file) {
		this(new Sample[] {sample}, file);
	}
	
	public AudioData(Sample[] samples, AssetSource file) {
		
		super(file, MediaType.AUDIO);
		this.samples = samples;
	}
	
	
	@Override
	public void loadData() throws IOException{
		
		var streams = source.getInputStreams();
		var li = new ArrayList<>();
		
		for(var is : streams) {
			
			var buf = new BufferedInputStream(is);
			Sample s = new Sample(buf);
			buf.close();
			li.add(s);
		}
		
		samples = li.toArray(new Sample[li.size()]);
	}
	
}
