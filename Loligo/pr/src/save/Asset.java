package save;

import static save.Asset.Status.FAILED;
import static save.Asset.Status.SUCCESS;
import static save.Asset.Status.LOADING;
import static save.Asset.Status.NEW;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Consumer;

import lombok.Getter;


public abstract class Asset {
	
	public static enum Status {
		NEW, LOADING, SUCCESS, FAILED;
		
		public boolean finished() {return this == SUCCESS || this == FAILED;}
	};
	
	@Getter
	private final MediaType mediaType;
	protected final AssetSource source;
	//private final String subDir;
	private int numUsers = 0;
	@Getter
	private Status status = NEW;
	private Consumer<Status> onStatusChange;
	
	
	public static Asset restoreAsset(AssetData ad) {
		
		/*
		AssetSource src = null;
		
		if(ad.source.equals("zip")) {
			
			src = new AssetSource.Zip(Paths.get(ad.path), ad.zipEntry);
		}else
		if(ad.source.equals("file")) {
			
			src = new AssetSource.File(new File(ad.path));
		}
		*/
		
		Asset ass = null;
		URL url = null;
		try {
			url = new URL(ad.url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
		AssetSource as = new AssetSource.File(url);
		
		String ext = ad.url.substring(ad.url.lastIndexOf("."));
		
		if(ext.equals("img")) {
			
			ass = new ImageData(as);
		}else
		if(ext.equals("audio")) {
			
			//ass = new AudioData(as);
			ass = AssetRegistry.loadAudio(url);
		}
		
		ad.asset = ass;
		
		return ass;
	}
	
	public Asset(AssetSource file, MediaType type) {
		
		this.source = file;
		this.mediaType = type;
		//subDir = "";
	}
	
	/*
	public Asset(AssetSource file, String subDir) {
		
		this.source = file;
		//this.subDir = subDir;
	}
	*/
	
	public AssetSource getFile() {
		
		return source;
	}
	
	
	public void loadAsset() {
		
		try {
			setStatus(LOADING);
			
			loadData();
			
			setStatus(SUCCESS);
			
		} catch (IOException e) {
			
			setStatus(FAILED);
			e.printStackTrace();
		}
	}
	
	private void setStatus(Status status) {
		this.status = status;
		if(onStatusChange != null)
			onStatusChange.accept(status);
	}
	
	public void onStatusChange(Consumer<Status> cb) {
		onStatusChange = cb;
	}
	
	abstract protected void loadData() throws IOException;
	
	/*
	@Override
	public Iterator<InputStream> iterator() {
		
		source.resetIterator();
		return source;
	}
	
	
	public String currentPath(String separator) {
		
		String g = groupFolder() != null && groupFolder().length() > 0 ?
						separator + groupFolder() : ""; 
		
		return getDefaultSubDir() + g + separator + source.currentFileName();
	}
	
	
	public String groupFolder() {
		
		return source.groupFolder();
	}
	
	
	public String getDefaultSubDir() {
		
		return subDir;
	}
	*/
	
	public int getNumUsers() {
		
		return numUsers;
	}
	
	
	public void addUser() {
		
		numUsers++;
	}
	
	
	public int removeUser() {
		
		return numUsers--;
	}
	
	public String serialize() {
		
		return source.url == null ? "" : source.url.toExternalForm();
	}
	
	@Override
	public String toString() {
	
		return serialize();
	}
	
	
}

 