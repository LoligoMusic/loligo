package save;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="url")
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(of= {"url"})

public class AssetData {
	
	@Getter
	String url;
	//@Getter
	//String type;
	
	@JsonIgnore 
	@Getter
	Asset asset;
	
}

