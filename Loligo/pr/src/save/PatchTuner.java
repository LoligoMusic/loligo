package save;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import constants.Flow;
import lombok.var;
import module.Modules;

public class PatchTuner implements Consumer<SaveDataContext>{

	@Override
	public void accept(SaveDataContext t) {
		
		var mds = new ArrayList<>(t.dataMap.values());
		
		mds.stream().filter(this::isNodeDisposable);
		
	}
	
	public boolean isNodeDisposable(ModuleData md) {
		var m = md.moduleIn;
		return m.checkFlag(Flow.NONESSENTIAL);
	}
	
	public void io(PatchData pd, ModuleData md) {
		var m = md.moduleIn;
		if(m.checkFlag(Flow.THROUGHPUT)) {
			var in = Modules.getInputByName(m, "In");
			var out = Modules.getOutputByName(m, "Out");
		}
	}
	
	public void cutNode(PatchData pd, ModuleData md, String inputName, String outputName) {
		
		var links = pd.getLinks();
		LinkData ld = links.stream()
						   .filter(li -> li.nodeIn == md && li.input.equals(inputName))
						   .findAny()
						   .orElse(null);
		
		var ldIns = links.stream()
						 .filter(li -> li.nodeOut == md && li.output.equals(outputName))
						 .collect(Collectors.toList());
		
		links.remove(ld);
		links.removeAll(ldIns);
		pd.nodes.remove(md);
		
		if(ld != null) {
			ldIns.forEach(li -> {
				var ln = new LinkData(ld.nodeOut, ld.output, li.nodeIn, li.input);
				pd.addLink(ln);
			});
		}
		
	}
}
