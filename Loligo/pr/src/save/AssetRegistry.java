package save;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.imageio.ImageIO;

import pr.DisplayManagerIF;
import pr.RootClass;
import processing.core.PGraphics;
import processing.core.PImage;
import util.AsyncFileDialog;
import util.Images;
import util.Utils;


public class AssetRegistry {

	private static Map<URL, Asset> fileMap = new HashMap<>();
	
	private static ImageData errorImg;
	
	
	public static ImageData getErrorImage() {
		
		if(errorImg == null){
			
			URL iu = null;
			/*try {
				iu = new URL("about:defaultImage");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}*/
			AssetSource as = new AssetSource(iu);
			ImageData id = new ImageData(createErrorImage("No Image Loaded"), as);
			fileMap.put(iu, id);
			errorImg = id;
		}
		
		return errorImg;
	}
	
	private static PImage createErrorImage(String message) {
		
		PGraphics g = Images.createGraphics(200, 100);
		g.beginDraw();
		
		for (int i = 0; i < g.pixels.length; i++) 
			g.pixels[i] = RootClass.mainProc().color((int)(Math.random() * 50),(int)(Math.random() * 50),(int)(Math.random() * 50));
		
		g.updatePixels();
		g.color(200);
		g.text(message, 5, 5, g.width - 5, g.height - 5);
		g.endDraw();
		return g.get();
	}
	
	public static InputStream getImageInputStream(String message) {
		
		PImage p = createErrorImage(message);
		BufferedImage b = Images.PImageToBufferedImage(p);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream() {
		    @Override  public synchronized byte[] toByteArray() {
		        return this.buf;
		    }
		};
		
		try {
			ImageIO.write(b, "png", out);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return new ByteArrayInputStream(out.toByteArray(), 0, out.size());
	}
	
	
	public static void loadImageDialog(DisplayManagerIF dm, Consumer<ImageData> action) {
		
		Consumer<File[]> c = (f) -> {
			if(f.length > 0) {
				ImageData id = loadImage(f[0].toPath());
				action.accept(id);
			}
		};
		
		AsyncFileDialog afd = new AsyncFileDialog(dm, c);
		afd.allowImageFiles();
		afd.setDialogTitle("Open image file..");
		afd.runAsync();
	}
	
	public static void loadAudioDialog(Consumer<AudioData> action) {
		
		Consumer<File[]> c = (f) -> {
			
			if(f == null || f.length == 0)
				return;
			
			try {
				AudioData id = loadAudio(f[0].toURI().toURL());
				action.accept(id);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		
		AsyncFileDialog afd = new AsyncFileDialog(null, c);
		afd.setFileDescription("Audio files (wav, mp3)");
		afd.setFileEndings("wav", "mp3");
		afd.setDialogTitle("Open audio file..");
		afd.runAsync();
	}
	
	
	public static ImageData loadImage(String path) {
		
		try {
			URL u = new URL(path);
			return createImageData(u);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static ImageData loadImage(Path p) {
		
		p = p.normalize();
		
		if(p.isAbsolute()) {
			p = Utils.relativePath(p);
			//p = Utils.getJarPath().toPath().relativize(p);
		}
		
		URL url = null;
		try {
			url = p.toUri().toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		return createImageData(url);
	}
	
	
	public static ImageData createImageData(URL url) {
		
		ImageData dat = getImageData(url);
		loadImageData(dat);
		return dat;
	}
	
	public static ImageData loadImageData(ImageData dat) {
		if(dat.getStatus() == Asset.Status.NEW)
			dat.loadAsset();
		return dat;
	}
	
	public static ImageData createImageDataAsync(URL url, Consumer<ImageData> callback) {
		
		ImageData dat = getImageData(url);
		new Thread(
			() -> {
				loadImageData(dat);
				callback.accept(dat);
			}
		).start();
		return dat;
	}
	
	public static ImageData getImageData(URL url) {
		
		Asset a = fileMap.get(url);
		
		if(a instanceof ImageData id) {
			id.addUser();
			return id;
		}
		
		AssetSource as = new AssetSource.File(url);
		ImageData dat = new ImageData(as);
		
		fileMap.put(url, dat);
		return dat;
	}
	
	
	public static AsyncFileDialog createAudioDialog(Consumer<AudioData> action) {
		
		Consumer<File[]> c = (f) -> {
			
			if(f == null || f.length == 0)
				return;
			
			try {
				AudioData id = loadAudio(f[0].toURI().toURL());
				action.accept(id);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		};
		
		AsyncFileDialog afd = new AsyncFileDialog(null, c);
		afd.setFileDescription("Audio files (wav)");
		afd.setFileEndings("wav");
		afd.setDialogTitle("Open Audio file..");
		
		return afd;
	}
	
	
	public static AsyncFileDialog createSVGDialog(Consumer<SVGData> action) {
		
		Consumer<File[]> c = (f) -> {
			
			if(f == null || f.length == 0)
				return;
			
			try {
				SVGData id = loadSVG(f[0].toURI().toURL());
				action.accept(id);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		};
		
		AsyncFileDialog afd = new AsyncFileDialog(null, c);
		afd.setFileDescription("Vector graphics (svg)");
		afd.setFileEndings("svg");
		afd.setDialogTitle("Open SVG file..");
		
		return afd;
	}

	public static void loadSVGDialog(Consumer<SVGData> action) {
		AsyncFileDialog afd = createSVGDialog(action);
		afd.runAsync();
	}
	
	
	public static SVGData loadSVG(String path) {
		URL u;
		try {
			u = new URL(path);
			return loadSVG(u);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static SVGData loadSVG(URL url) {
		
		Asset a = fileMap.get(url);
		
		if(a instanceof SVGData id) {
			id.addUser();
			return id;
		}
		AssetSource as = new AssetSource.File(url);
		SVGData dat = new SVGData(as);
		
		dat.loadAsset();
		fileMap.put(url, dat);
		
		return dat;
	}
	
	
	public static AudioData loadAudio(URL url) {
		
		Asset a = fileMap.get(url);
		
		if(a instanceof AudioData id) {
			id.addUser();
			return id;
		}
		
		/*
		File file = p.toFile();
		
		if(!file.exists()) {
			//TODO file missing error
		}
		
		File[] ff = file.isDirectory() ? file.listFiles(f->{return !f.isDirectory();}) : 
										 new File[]{file};
		
		List<PImage> imgs = new ArrayList<>();
		
		for (File f : ff) {
			
			try {
				BufferedImage b = ImageIO.read(f);
				PImage pimg = Utils.bufferedImageToPImage(b);
				imgs.add(pimg);
			} catch (IOException e) {
				System.out.println("Couldn't load " + f);
				e.printStackTrace();
			}
		}
		
		PImage[] iarr = new PImage[imgs.size()];
		*/
		AssetSource as = new AssetSource.File(url);
		AudioData dat = new AudioData(as);
		
		dat.loadAsset();
		
		fileMap.put(url, dat);
		
		return dat;
	}
	
	
	public static void releaseAsset(URL url) {
		
		Asset ad = fileMap.get(url);
		
		if(ad != null && ad.removeUser() <= 0) {
			fileMap.remove(url);
		}
	}
	
	public static List<Asset> getAssets() {
		
		return new ArrayList<>(fileMap.values());
	}
		
	public static void restoreAssets(List<AssetData> ads) {
		
		for (AssetData ad : ads) {
			
			Asset ass = Asset.restoreAsset(ad);
			fileMap.put(ass.getFile().getUrl(), ass);
		}
	}
}
