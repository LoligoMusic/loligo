package save;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.var;
import module.Module;
import module.Modules;
import pr.RootClass;


public interface Reference {
	
	public static final Reference NULL = new Null();
	
	
	public static Reference.Id createIdReference(Module m) {
		
		int pi = m.getDomain().getDomainID(),
			ni = m.getModuleID();
		
		return new Reference.Id(pi, ni);
	}
	
	
	
	public Module resolve();
	
	
	
	@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Id implements Reference{
		
		public int patchId = -1, nodeId = -1;

		@Override
		public Module resolve() {
			var p = RootClass.backstage().getPatchFromId(patchId);
			return p == null? null : Modules.findModuleByID(p, nodeId);
		}
	}
	
	
	@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Node implements Reference {
		
		@JsonIdentityReference(alwaysAsId = true)
		public ModuleData node;
		
		@Override
		public Module resolve() {
			return node.getModuleOut();
		}
	}
	
	
	@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
	@NoArgsConstructor
	public static class Null implements Reference {
		@Override
		public Module resolve() {
			return null;
		}
	}
	
}
