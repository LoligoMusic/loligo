package save;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import lombok.Getter;


public class AssetSource {//implements Iterator<InputStream>{
	
	@Getter
	protected final URL url;
	
	public AssetSource(URL url) {
		
		this.url = url;
	}
	
	
	@Override
	public String toString() {
		return url.toExternalForm();
	}
	
	public InputStream[] getInputStreams() throws IOException {
		
		var uc = url.openConnection();
		uc.addRequestProperty("User-Agent", "Mozilla/4.0"); // Workaround for 403 with HttpConnections
		return new InputStream[] {uc.getInputStream()}; 
	}
	
	public boolean isDirectory() {
		return false;
	}
		
	
	
	public static class File extends AssetSource{

		public File(URL url) {
			super(url);
		}

		@Override
		public boolean isDirectory() {
			
			return new java.io.File(url.getFile()).isDirectory();
		}
	}
	
	public static class Zip extends AssetSource {


		public Zip(URL url) {
			
			super(url);
		}

		@Override
		public boolean isDirectory() {
			 String file = url.getFile();
		      int bangIndex = file.indexOf('!');
		      String jarPath = file.substring(bangIndex + 2);
		      try {
				file = new URL(file.substring(0, bangIndex)).getFile();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		   
			try(ZipFile zip = new ZipFile(file)) {
				
				ZipEntry entry = zip.getEntry(jarPath);
		    	boolean isDirectory = entry.isDirectory();
		    	if (!isDirectory) {
		        
					try(InputStream input = zip.getInputStream(entry)) {
						return input == null;
					} catch (IOException e) {
						e.printStackTrace();
						return false;
					}
		    	}
				
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		    return false;
		}
		
		/*
		@Override
		public boolean hasNext() {
			
			if(subFiles == null) {
				
				try {
					zipFile = new ZipFile(zipPath.toFile());
				} catch (IOException e1) {
					
					System.err.println("\"" + zipPath + "\" not found.");
					e1.printStackTrace();
					return false;
					
				} 
				
				if(zipDir.endsWith("/")) {
					
					subFiles = new ArrayList<>();
					
					for(final Enumeration<? extends ZipEntry> e = zipFile.entries(); e.hasMoreElements();) {
						
						ZipEntry ze = e.nextElement();
						
						if(ze.getName().startsWith(zipDir)) 
							subFiles.add(ze);
					}
					
				}else{
					ZipEntry entry = zipFile.getEntry(zipDir);
					subFiles = new ArrayList<>(1);
					subFiles.add(entry);
				}
			}
			
			if(i < subFiles.size()) {
				
				return true;
			}else{
				
				try {
					zipFile.close();	
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}
		}
		 */
		
		/*
		@Override
		public String groupFolder() {
			
			if(zipDir.endsWith("/")) {
				
				int i2 = zipDir.length() - 2,
					i1 = zipDir.lastIndexOf('/', i2) + 1;
				
				return zipDir.substring(i1 < 0 ? 0 : i1, i2);
			}
			
			return null;
		}
		*/
	
	}

	
	
}
