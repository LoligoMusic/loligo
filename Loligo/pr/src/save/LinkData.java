package save;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString

public class LinkData {

	@JsonIdentityReference(alwaysAsId = true)
	public ModuleData nodeOut;
	public String output;
	@JsonIdentityReference(alwaysAsId = true)
	public ModuleData nodeIn;
	public String input;
	
}
