package save;

import java.net.URL;

public interface AssetContainer {

	public AssetSource getAssetSource();
	public void loadAssetFromURL(URL u);
}
