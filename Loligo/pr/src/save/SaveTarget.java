package save;

public enum SaveTarget {

	FILE,
	CLIPBOARD,
	MEMENTO
}
