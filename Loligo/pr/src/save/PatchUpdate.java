package save;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.google.common.collect.Streams;

import lombok.Getter;
import lombok.var;

public abstract class PatchUpdate {

	@Getter
	private final int version;
	
	public PatchUpdate() {
		this(0);
	}
	
	public PatchUpdate(int version) {
	
		this.version = version;
	}
	
	public abstract void update(PatchData pd);
	
	
	
	public static class Node extends PatchUpdate {
		
		private final Consumer<ModuleData> consumer;
		private final String name;
		
		
		public Node(String name, Consumer<ModuleData> consumer) {
			this(0, name, consumer);
		}
		
		public Node(int version, String name, Consumer<ModuleData> consumer) {
			this.consumer = consumer;
			this.name = name;
		}
		
		@Override
		public void update(PatchData pd) {
			
			flatten(pd.nodes)
				.filter(n -> n != null && name.equals(n.name))
				.forEach(this::updateNode);
		}
		
		private Stream<ModuleData> flatten(List<ModuleData> list) {
			return  Streams.concat(list.stream(),
						list.stream()
						   .filter (m -> m.subpatch != null) 
						   .flatMap(m -> flatten(m.subpatch.nodes))
					);
		}
		
		public void updateNode(ModuleData md) {
			consumer.accept(md);
		}
		
		
		
		
		public static class AddVal extends Node {

			public AddVal(int version, String name, Consumer<ModuleData> consumer) {
				super(version, name, consumer);
			}
			
			public AddVal(String name, Consumer<ModuleData> consumer) {
				super(name, consumer);
			}
			
		}
		
		public static Node renameNode(String nodeName, String newName) {
			return new PatchUpdate.Node(nodeName, n -> n.name = newName); 
		}
		
		public static Node addVal(String nodeName, String key, Object value) {
			Consumer<ModuleData> c = m -> m.getMap().computeIfAbsent(key, s -> value);
			Node n = new Node(nodeName, c);
			return n;
		}
		
		public static Node renameVal(String nodeName, String key, String newKey) {
			Consumer<ModuleData> c = m -> {
				var mp = m.getMap();
				var v = mp.get(key);
				if(v != null) {
					mp.put(newKey, v);
					mp.remove(key);
				}
			};
			Node n = new Node(nodeName, c);
			return n;
		}
		
	}
	
	
}
