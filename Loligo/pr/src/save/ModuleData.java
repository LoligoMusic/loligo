package save;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.inout.InOutInterface;
import module.loading.NodeCategory;


@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
@JsonIgnoreProperties(ignoreUnknown = true)

public final class ModuleData {

	public int id; // id is only unique per patch, so jackson uses @id
	public int version = 1;
	public String node;
	public String name, label;
	public NodeCategory type;
	public boolean gui;
	public int[] xy;
	
	public PatchData subpatch;
	
	@JsonTypeInfo(use=Id.CLASS, include=As.WRAPPER_OBJECT)
	private Map<String, Object> map;
	@JsonTypeInfo(use=Id.CLASS, include=As.WRAPPER_OBJECT)
	public Object[] extra;
	transient public Object[] extraOut;
	@Getter@Setter
	public List<IOData> inputs, outputs;
	
	public String[] assets;
	
	@JsonIgnore
	@Setter
	transient Module moduleIn, moduleOut;
	transient int extraIdx = 0;
	
	public ModuleData() {
		
	}
	
	public ModuleData(Module module) {
		
		this.moduleIn = module;
	}
	 
	public ModuleData(ModuleData md) {
		copyFrom(md);
	}

	
	public IOData getInputData(InOutInterface io) {
		return getIOData(inputs, io);
	}
	
	public IOData getOutputData(InOutInterface io) {
		return getIOData(outputs, io);
	}
	
	private IOData getIOData(List<IOData> ios, InOutInterface io) {
		
		if(ios == null)
			return null;
		for (IOData d : ios) {
			if(d.io == io)
				return d;
		}
		return null;
	}
	
	@JsonInclude(Include.NON_NULL)
	public Map<String, Object> getMap() {
		return map;
	}
	
	public Object getExtraValue(String key) {
		return map.get(key);
	}
	
	public Number getExtraNumber(String key) {
		return (Number) getExtraValue(key);
	}
	
	public int getExtraInt(String key) {
		return getExtraNumber(key).intValue();
	}
	
	public float getExtraFloat(String key) {
		return getExtraNumber(key).floatValue();
	}
	
	public double getExtraDouble(String key) {
		return getExtraNumber(key).doubleValue();
	}
	
	public boolean getExtraBool(String key) {
		return (Boolean) getExtraValue(key);
	}
	
	public String getExtraString(String key) {
		return (String) getExtraValue(key);
	}
	
	public <T extends Enum<T>> T getExtraEnum(String key, Class<T> en) {
		String s = getExtraString(key);
		return Enum.valueOf(en, s);
	}
	
	public boolean contains(String key) {
		return map == null ? false :
			   map.containsKey(key);
	}
	
	public void putExtra(String key, Object value) {
		
		createMap();
		map.put(key, value);
	}
	
	public void moduleInToOut() {
		moduleOut = moduleIn;
	}
	
	public void copyFrom(ModuleData md) {
		this.moduleIn = md.moduleIn;
		this.node = md.node;
		this.gui = md.gui;
		this.xy = md.xy;
		this.map = md.map;
		this.extra = md.extra;
		this.extraOut = md.extraOut;
		this.inputs = md.inputs;
		this.outputs = md.outputs;
	}
	
	public Map<String, Object> createMap() {
		
		if(map == null)
			map = new HashMap<>();
		return map;
	}
	
	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	
	public boolean isGui() {
		return gui;
	}

	
	public void setGui(boolean gui) {
		this.gui = gui;
	}

	
	public int[] getXy() {
		return xy;
	}

	public void setXY(int x, int y) {
		this.xy = new int[] {x, y};
	}
	
	public void setXy(int[] xy) {
		this.xy = xy;
	}

	@JsonIgnore
	public Module getModuleIn() {
		return moduleIn;
	}
	
	@JsonIgnore
	public Module getModuleOut() {
		return moduleOut;
	}

	
	@Override
	public String toString() {
		return name;
	}
	
	public int getX() {
		return xy[0];
	}
	
	public int getY() {
		return xy[1];
	}
	
	public Reference toReference() {
		return new Reference.Node(this);
	}
	
	public ModuleData putExtra(Object o, int idx) {
		this.extra[idx] = o;
		this.extraOut[idx] = o;
		return this;
	}
	
	public ModuleData addExtras(Object... extra) {
		this.extra = extra;
		this.extraOut = extra;
		return this;
	}
	
	public void copyExtra() {
		if(extra != null) {
			this.extraOut = new Object[extra.length];
			for (int i = 0; i < extra.length; i++) {
				extraOut[i] = extra[i];
			}
		}
	}
	
	public Object getExtra(int i) {
		return extraOut[i];
	}
	
	public Object nextObject() {
		return extraOut[extraIdx++];
	}
	
	public Number nextNumber() {
		return (Number) nextObject();
	}
	
	public int nextInt() {
		return nextNumber().intValue();
	}
	
	public float nextFloat() {
		return nextNumber().floatValue();
	}
	
	public double nextDouble() {
		return nextNumber().doubleValue();
	}
	
	public boolean nextBool() {
		return (Boolean) nextObject();
	}
	
	public String nextString() {
		return (String) nextObject();
	}
	
	public void nextString(Function<String, Object> foo) {
		extraOut[extraIdx] = foo.apply((String) extra[extraIdx]);
		extraIdx++;
	}
	
	public void nextObject(Function<Object, Object> foo) {
		extraOut[extraIdx] = foo.apply(extra[extraIdx]);
		extraIdx++;
	}
	
	public ModuleData skip() {
		extraIdx++;
		return this;
	}

	void resetExtraCounter() {
		extraIdx = 0;
	}
}
