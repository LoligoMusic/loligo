package save;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import lombok.Getter;
import processing.core.PShapeSVG;
import processing.data.XML;


public class SVGData extends Asset {

	public static final String TYPE = "image";
	
	
	@Getter
	private PShapeSVG shape;
	
	
	public SVGData(AssetSource source) {
		
		this((PShapeSVG) null, source);
	}
	
	public SVGData(PShapeSVG shape, AssetSource source) {
		
		super(source, MediaType.SVG);
		this.shape = shape;
	}
	
	
	@Override
	public void loadData() throws IOException {

		InputStream[] is = source.getInputStreams();
		
		if(is != null && is.length > 0) {
			
			InputStream s = is[0];
			Reader r = new InputStreamReader(s);
			
			try {
				XML x = new XML(r);
				shape = new PShapeSVG(x);
				
			} catch (ParserConfigurationException | SAXException e) {
				throw new IOException(e);
			}
		}
		
		
	}

	
}
