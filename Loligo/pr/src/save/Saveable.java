package save;

public interface Saveable {

	public void restoreMemento(Object saveform);
	
	public Object toMemento();
	
}
