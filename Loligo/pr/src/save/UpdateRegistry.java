package save;

public class UpdateRegistry {

	private static PatchUpdate[] updates;
	
	
	private static void define() {
		
		updates = new PatchUpdate[] 
		
		{
			/*
			new ArithmeticUpdate(),
			
			renameNode("Sine", "Wave"),
			addVal("Comment", "width", 150),
			addVal("Comment", "fontSize", 12),
			addVal("Comment", "bgColor", 0),
			addVal("Comment", "fontColor", 0xffffffff),
			addVal("Comment", "align", "LEFT"),
			renameVal("Comment", "Comment", "comment"),
			addVal("Shape", "shape", DrawForm.ELLIPSE.name()),
			addVal("Shape", "blendMode", BlendMode.DEFAULT)
			 */
		};
	}
	
	public static void update(PatchData patch) {
		
		if(updates == null)
			define();
		
		for (PatchUpdate pu : updates) {
			
			pu.update(patch);
		}
	}
	
	
}
