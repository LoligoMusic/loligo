package save.updates;

import module.loading.ArithmeticEntry;
import module.loading.NodeCategory;
import save.ModuleData;
import save.PatchData;
import save.PatchUpdate;

public class ArithmeticUpdate extends PatchUpdate {

	@Override
	public void update(PatchData pd) {
		
		for (ModuleData md : pd.getNodes()) {
			if(md != null)
				substituteMathNode(md);
		}

	}

	private void substituteMathNode(ModuleData md) {
	
		if(md.getNode().equals("module.modules.math.Arithmetic")) {
			
			String mode = (String) md.getMap().get("mode");
			
			ArithmeticEntry ae = 
				  mode.equals("ADD") 	  ? ArithmeticEntry.ADD 	 : 
				  mode.equals("SUBTRACT") ? ArithmeticEntry.SUBTRACT : 
				  mode.equals("MULTIPLY") ? ArithmeticEntry.MULTIPLY :
				  mode.equals("DIVIDE")   ? ArithmeticEntry.DIVIDE 	 : 
				  mode.equals("SINE")     ? ArithmeticEntry.SINE 	 : null;
			
			md.name = ae.getName();
			md.type = NodeCategory.MATH;
		
		}else{
		
			//getClass().forName(md.getNode());
	
			
		}
		
	}

}
