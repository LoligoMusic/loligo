package save;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import processing.core.PImage;
import util.Images;

public class ImageData extends Asset {
	
	public static final String TYPE = "image";
	
	private PImage[] img;
	
	public ImageData(AssetSource source) {
		
		this((PImage[]) null, source);
	}
	
	public ImageData(final PImage img, AssetSource source) {
		
		this(new PImage[]{img}, source);
	}
	
	public ImageData(final PImage[] img, AssetSource source) {
		
		super(source, MediaType.IMAGE);
		this.img = img;
	}
	
	public PImage[] getImgs() {
		
		return img;
	}
	
	
	@Override
	protected void loadData() throws IOException{
		
		InputStream[] streams = source.getInputStreams();
		
		img = gif2(streams[0]);
		if(img != null && img.length > 0)
			return;

		List<PImage> li = new ArrayList<>();
		
		streams = source.getInputStreams(); // Reload inputstreams
		
		for (InputStream is : streams) {
			
			BufferedImage bimg = ImageIO.read(is);
			if(bimg != null) {
				PImage pimg = Images.bufferedImageToPImage(bimg);
				li.add(pimg);
			}
		}
		
		if(!li.isEmpty()) {
			
			PImage[] iarr = new PImage[li.size()];
			img = li.toArray(iarr);
		}
	}
	
	
	private PImage[] gif2(InputStream is) {
		
		ImageReader reader = ImageIO.getImageReadersBySuffix("GIF").next();
		
		ImageInputStream in = null;
		int n = 0;
		
		try {
			in = ImageIO.createImageInputStream(is);
			reader.setInput(in);
			n = reader.getNumImages(true);
			
			PImage[] imgs = new PImage[n];
			
			
			for (int i = 0; i < n; i++)
			{
				BufferedImage b = reader.read(i);
				imgs[i] = Images.bufferedImageToPImage(b);
			}
			
			for (int i = 1; i < n; i++) {
				restoreGifFrame(imgs[i - 1], imgs[i]);
			}
			
			return imgs;
			
		} catch (IOException e) {
			e.printStackTrace();
			return new PImage[0];
		}
	}
	
	private void restoreGifFrame(PImage first, PImage frame) {
		first.loadPixels();
		frame.loadPixels();
		int[] pxs = frame.pixels, pxs1 = first.pixels;
		for (int i = 0; i < pxs.length; i++) {
			int a = (pxs[i] >> 24) & 0xFF;
			if(a == 0) {
				pxs[i] = pxs1[i];
			}
		}
		frame.updatePixels();
	}

	
}