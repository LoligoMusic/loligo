package save;

import java.util.List;

import gui.particles.Particle;
import gui.particles.ParticleImpl;
import gui.particles.ParticleSystem;
import gui.particles.ParticleSystemRegistry;
import lombok.Getter;
import lombok.Setter;
import pr.RootClass;


//TODO: restore

public class ParticleSystemData {

	@Getter@Setter
	ParticleData[] particles;
	@Getter@Setter
	String name = "";
	
	public ParticleSystemData(ParticleSystem ps) {
		
		particles = new ParticleData[ps.numParticles()];
		
		for (int i = 0; i < particles.length; i++) {
			
			particles[i] = new ParticleData(ps.getParticles().get(i));
		}
	}
	
	public static ParticleSystemData[] buildParticleSystemData() {
		
		List<ParticleSystem> ps = ParticleSystemRegistry.getParticleSystems();
		
		if(ps.isEmpty())
			return null;
		
		ParticleSystemData[] systems = new ParticleSystemData[ps.size()];
		
		for (int i = 0; i < systems.length; i++) {
			
			systems[i] = new ParticleSystemData(ps.get(i));
		}
		
		return systems;
	}
	
	
	public void restoreParticleSystem() {
		
		var ps = ParticleSystemRegistry.getOrCreateParticleSystem(RootClass.mainDm(), name);
		
		//ParticleData psd = particles[0];
		
		//DefaultParticleFactory f = (DefaultParticleFactory) ps.getFactory();
		
		for (var pd : particles) {
			
			var p = new ParticleImpl(ps, ps.getDm(), pd.maxLifeTime, pd.dir[0], pd.dir[1]);
			p.setLifeTime(pd.lifeTime);
			p.setPos(pd.pos[0], pd.pos[1]);
			
			ps.addParticle(p);
		}
	}
		
	
	class ParticleData {
		
		int lifeTime, maxLifeTime;
		float size;
		float[] pos, dir;
		String color;
		
		public ParticleData(Particle p) {
			
			lifeTime = p.getLifeTime();
			maxLifeTime = p.getMaxLifeTime();
			//size = p.getSize();
			pos = new float[] {p.getX(), p.getY()};
			dir = new float[] {p.getDx(), p.getDy()};
			color = Integer.toHexString(p.getColor());
		}
	}
	
}
