package save;

import com.fasterxml.jackson.annotation.JsonIgnore;

import constraints.Constrainable;
import gui.Position;
import gui.paths.Path;
import gui.paths.PathEvent;
import gui.paths.PathObject;

public class ConstrainableData implements PathObject{

	public static enum Type {
		
		PATH_VERTEX,
		PATH_CONTROLPOINT,
		NODE;
	}
	
	public Type type;
	
	//NODE
	public ModuleData node; // target node
	//public PathData path; //pat
	public int segment; //path index (for treeconstraint)
	public double position; //segment pos
	
	//VERTEX | CONTROLPOINT
	public VertexData vertex;
	public VertexData controlPoint;
	
	
	public class VertexData {
		
		PathData path;
		int vertex;
		Integer controlPoint = null;  // Ignored for vertex
	}


	public Constrainable restore(RestoreDataContext rdc) {
		
		if(type == Type.NODE) {
			
			return (Constrainable) node.moduleOut;
		}
		return null;
	}

	@Override
	public void goToPathPosition(double d) {
		
		position = d;
	}

	//unneeded PathObject methods.
	@JsonIgnore
	@Override public float getX() {return 0;}
	@JsonIgnore
	@Override public float getY() {return 0;}
	@Override public void setPos(float x, float y) {}
	@Override public Position getPosObject() {return null;}
	@Override public void setPosObject(Position p) {}
	@Override public void onPathEvent(PathEvent e) {}
	@Override public Path getSegment() {return null;}
	@Override public void setSegment(Path path) {}
	@Override public void setPath(Path path) {}
	@Override public Path getPath() {return null;}
	@JsonIgnore
	@Override public double getPathPosition() {return 0;}
	@JsonIgnore
	@Override public double getTangent() {return 0;}
	@Override public void setTanget(double tangent) {}
	@Override public void updatePosition() {}
	
}
