package save;

import java.io.IOException;
import java.net.URL;
import lombok.var;

public enum MediaType {
	
	IMAGE		("image", "jpg", "jpeg", "png", "gif", "bmp"),
	SVG			("image", "svg"),
	LOLIGOPATCH	("text" , "lol"),
	PYSCRIPT	("text" , "py" ),
	AUDIO		("audio", "wav");
	
	public final String[] fileEndings;
	public final String mimeType;
	
	
	MediaType(String mimeType, String... endings) {
		this.mimeType = mimeType;
		this.fileEndings = endings;
	}
	
	
	public static MediaType find(URL url) {
		
		try {
			var c = url.openConnection();
			String cs = c.getContentType();
			if(cs != null && !cs.equals("content/unknown")) {
				String e = cs.substring(cs.indexOf('/') + 1);
				return forFileEnding(e);
			}else {
				String f = url.getFile();
				int i = f.lastIndexOf('.');
				if(i >= 0) {
					String e = f.substring(i + 1).toLowerCase();
					return forFileEnding(e);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public static MediaType forFileEnding(String e) {
		e = e.toLowerCase();
		for(var v : values()) {
			for(var n : v.fileEndings) {
				if(e.endsWith(n)) {
					return v;
				}
			}
		}
		return null;
	}
}
