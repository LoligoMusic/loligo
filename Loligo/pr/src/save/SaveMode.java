package save;

public enum SaveMode {

	SERIALIZE,
	DUPLICATION, 
	
	/**
	 *  Ignores subpatches
	 */
	SHALLOW
	
	
}
