package save;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import module.Module;
import module.inout.DataType;
import module.inout.InOutInterface;
import module.inout.Output;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(using = IOData.Serializer.class)
@JsonDeserialize(using = IOData.Deserializer.class)

public class IOData {
	
	public String name;
	public DataType type;
	public Object value;
	public boolean hidden = false;
	
	public transient Module partner;
	public transient Output output;
	public transient InOutInterface io;
	
	public IOData(String name, DataType type) {
		this.name = name;
		this.type = type;
	}
	

	
	public static class Serializer extends StdSerializer<IOData> {
		private static final long serialVersionUID = 1L;
		
		public Serializer() {
			this(null);
		}
		
		protected Serializer(Class<IOData> t) {
			super(t);
		}

		@Override
		public void serialize(IOData value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			
			gen.writeStartObject();
			gen.writeStringField("name", value.name);
			gen.writeObjectField("type", value.type);
			
			var v = value.value;
			if(v != null) {
				v = DataType.toSerializedForm(value.type, v);
				gen.writeObjectField("value", v);
			}
			if(value.hidden) 
				gen.writeBooleanField("hidden", true);
			
			gen.writeEndObject();
		}
	}
	
	
	public static class Deserializer extends StdDeserializer<IOData> {
		private static final long serialVersionUID = 1L;
		
		public Deserializer() {
			this(null);
		}
		
		protected Deserializer(Class<IOData> vc) {
			super(vc);
		}

		@Override
		public IOData deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
			var b = SaveJson.defaultObjectMapperBuilder();
			b.disable(MapperFeature.USE_ANNOTATIONS);
			var dat = b.build().readValue(p, IOData.class);
			dat.value = dat.type.fromSerializedForm(dat.value);
			return dat;
		}
	}
	
}
