package save;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class ColorJson {

	
	public static class Serializer extends StdSerializer<Integer> {
		private static final long serialVersionUID = 1L;

		public Serializer() {
		        this(null);
	    }
	   
	    public Serializer(Class<Integer> t) {
	        super(t);
	    }

		@Override
		public void serialize(Integer value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			String h = Integer.toHexString(value).toUpperCase();
			gen.writeString(h);
		}
	}
	
	public static class Deserializer extends StdDeserializer<Integer> {
		private static final long serialVersionUID = 1L;

		public Deserializer() {
			this(null);
		}
		
		protected Deserializer(Class<Integer> vc) {
			super(vc);
		}

		@Override
		public Integer deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
			String h = p.getText();
			int c = Integer.parseUnsignedInt(h, 16);
			return c;
		}
		
	}
}
