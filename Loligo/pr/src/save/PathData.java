package save;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import constraints.Constrainable;
import constraints.LoligoPosConstraint;
import constraints.PosConstraintContainer;
import gui.paths.Path;
import gui.paths.PathMementoFactory;
import gui.paths.PathMementoFactory.SegmentMemento;
import lombok.NoArgsConstructor;
import gui.paths.SegmentType;
import gui.paths.VertexMemento;

@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")

@NoArgsConstructor

public class PathData {

	transient public PosConstraintContainer container;
	
	@JsonIdentityReference(alwaysAsId = true)
	public ModuleData node;
	public PathMementoFactory.ConstraintMemento constraint;
	public ConstrainableData[] anims;
	
	
	public PathData(PosConstraintContainer container, ModuleData node) {
		
		this.container = container;
		this.node = node;
		
		
	}
	
	public void generateConstraintMemento() {
		
		constraint = new PathMementoFactory(container.getPosConstraint()).toMemento();
	}
	
	public void restoreConstraintMemento(PosConstraintContainer pcc) {
		
		container = pcc;
		LoligoPosConstraint pc = pcc.getPosConstraint();
		
		//if(pc.getPaths().isEmpty()) {
		 
			for (VertexMemento vm : constraint.vertices) {
				vm.node = pc.newNode();
				vm.node.setPos(vm.x, vm.y);
			}
			
			for (SegmentMemento sm : constraint.paths) {
				
				Path p = (Path) pc.createSegment(null, null, SegmentType.PATH);
				pc.addPath(p);
				p.restoreMemento(sm, null);
			}
		//}
		
		if(pc.gui != null)
			pc.gui.forceUpdateGui(); // TODO temporary solution
	}


	public void constrainables(SaveDataContext sdc) {
		
		List<Constrainable> cs = container.getConstrainables();
		ConstrainableData[] cds = new ConstrainableData[cs.size()];
		
		for (int i = 0; i < cds.length; i++) {
			cds[i] = cs.get(i).toConstrainableMemento(sdc, this);
		}
		
		anims = cds;
		
	}
	
}
