package save;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.PositionableModule;
import patch.Domain;


public class SaveDataContext {
	
	private List<Module> modules;
	
	final Map<Module, ModuleData> dataMap = new IdentityHashMap<>();
	final List<StackData> stacks = new ArrayList<>();
	final List<PathData> paths = new ArrayList<>();
	final Map<Asset, String> assets = new IdentityHashMap<>();
	final private SaveMode saveMode;
	@Setter@Getter
	private SaveTarget saveTarget;
	private Domain patch;
	@Getter
	private PatchData patchData;
	
	private final List<Consumer<SaveDataContext>> lastRestoreTasks = new ArrayList<>();
	
	//private List<IOData> particleParams;
	
	public SaveDataContext(SaveMode saveMode) {
		 
		this.saveMode = saveMode;
	}
	
	
	public SaveMode getSaveMode() {
		return saveMode;
	}
	
	
	public ModuleData tryFindData(Module m) {
		return dataMap.get(m);
	}
	
	public ModuleData findData(Module m) {
		
		var md = dataMap.get(m);
		
		if(md == null) {
			
			md = toData(m);
			if(md != null)
				dataMap.put(m, md);
		}
		
		return md;
	}
	
	public ModuleData toData(Module m) {
		var md = m.doSerialize(this);
		SaveJson.prepairModuleData(md);
		return md;
	}
	
	public String findData(Asset as) {
		
		return assets.computeIfAbsent(as, a -> a.serialize());
	}
	
	
	public void addStack(StackData sd) {
		
		stacks.add(sd);
	}
	
	
	public void addPath(PathData pd) {
		
		paths.add(pd);
	}
	
	
	public void addLateTask(Consumer<SaveDataContext> c) {
		
		lastRestoreTasks.add(c);
	}
	

	public PatchData buildPatchData() {
		
		var pd = new PatchData();
		
		if(patch != null) {
			var pr = patch.getPApplet();
			pd.width = pr.width;
			pd.height = pr.height;
			pd.id = patch.getDomainID();
			pd.currentNodeId = patch.getCurrentModuleID() + 1;
			pd.patchType = patch.domainType();
		}
		
		pd.saveTarget = saveTarget;
		
		
		pd.nodes = modules.stream()
						  .map(this::findData)
						  .filter(Objects::nonNull)
						  .collect(Collectors.toList());
		
		pd.links = buildLinks();
		pd.stacks = stacks;
		pd.paths = paths;
		patchData = pd;
		
		buildConstrainables();
		
		if(saveMode == SaveMode.SERIALIZE) {
			serializeAssets(AssetRegistry.getAssets());
			pd.addAssets(assets.values());
		}
		
		for (var r : lastRestoreTasks) 
			r.accept(this);
		
		//pd.particleParams = particleParams;
		
		//pd.particleSystems = ParticleSystemData.buildParticleSystemData(); //TODO don't do this for duplication
		
		return pd;
	}
	
	
	public void serializeAssets(final List<Asset> ast) {
		assets.clear();
		ast.forEach(this::findData);
	}
	
	
	public void addPatch(Domain patch) {
		this.patch = patch;
		addModules(patch.getContainedModules());
	}
	
	public void addPatch(Domain patch, List<? extends Module> modules) {
		this.patch = patch;
		addModules(modules);
	}
	
	public void addModule(Module module) {
		this.modules = Collections.singletonList(module);
	}
	
	public void addModules(List<? extends Module> modules) {
		this.modules = new ArrayList<>(modules);
	}
	
	
	private List<LinkData> buildLinks() {
		
		var links = new ArrayList<LinkData>();
		
		for (var md : dataMap.values()) {
			
			for (var io : md.getInputs()) {
				
				if(io.partner != null) {
				
					var mdOut = dataMap.get(io.partner);
					
					if(mdOut != null) {
						
						var link = new LinkData(mdOut, io.output.getName(), md, io.name);
						links.add(link);
					}else {
						System.err.println("Missing link partner for " + md + "/" + io);
					}
				}
			}
		}
		
		return links;
	}
	
	private void buildConstrainables() {
		
		for (int i = 0; i < paths.size(); i++) {
			var pd = paths.get(i);
			pd.constrainables(this);
		}
	}
	
	/*
	public void addParticeParams(List<IOData> params) {
		
		if(particleParams == null)
			particleParams = new ArrayList<>();
		
		particleParams.addAll(params);
	}
	*/
	
	public static void saveByAnnotations(ModuleData md, Class<?> clazz) {
		
		var fields = clazz.getDeclaredFields();
		
		var map = new HashMap<String, Object>();
 		
		for (Field f : fields) {
			if(f.isAnnotationPresent(RestoreField.class)) {
				
				try {
					
					String n = f.getName();
					
					String get = "get" + n.substring(0,1).toUpperCase() + n.substring(1);
					
					Method m = clazz.getMethod(get, (Class<?>[]) null);
					Object val = m.invoke(md.moduleIn, (Object[]) null);
					
					//Object val = f.get(md.module);
					
					String name = f.getDeclaredAnnotation(RestoreField.class).name();
					if(name.isEmpty())
						name = f.getName();
					
					map.put(name, val);
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void restoreByAnnotation(ModuleData md, Class<?> clazz){
		
		Field[] fields = clazz.getDeclaredFields();
		
		for (Field f : fields) {
			if(f.isAnnotationPresent(RestoreField.class)) {
				
				String name = f.getDeclaredAnnotation(RestoreField.class).name();
				if(name.isEmpty())
					name = f.getName();
				
				Object value = md.getMap().get(name);
				
				try {
					/*
					f.setAccessible(true);
					f.set(md.module, value);
					f.setAccessible(false);
					*/
					
					String set = "set" + name.substring(0,1).toUpperCase() + name.substring(1);
					
					Method[] mm = clazz.getDeclaredMethods();
					for (Method m : mm) {
						if(m.getName().equals(set)) {
							
							m.invoke(md.moduleOut, value);
							break;
						}
					}
					
				} catch (IllegalArgumentException | IllegalAccessException | SecurityException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void savePos(PositionableModule m, ModuleData md) {
		
		md.setXy(new int[]{(int)m.getX(), (int) m.getY()});
	}
	
	
	public Reference createReference(Module m) {
		
		if(m == null)
			return Reference.NULL;
		
		var md = tryFindData(m);
		
		if(md != null) {
			//relative ref
			return md.toReference();
		}else {
			//absolute ref
			return Reference.createIdReference(m);
		}
	}
	
	
	public void saveReference(ModuleData md, Module m, int index) {
		addLateTask(sdc -> {
			var r = createReference(m);
			md.putExtra(r, index);
		});
	}
}
