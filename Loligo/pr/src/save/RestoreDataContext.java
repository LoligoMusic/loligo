package save;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import audio.AudioModule;
import audio.AudioStack;
import constraints.Constrainable;
import constraints.LoligoPosConstraint;
import constraints.PosConstraintContainer;
import gui.paths.Path;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import module.AbstractModule;
import module.Anim;
import module.MissingNode;
import module.Module;
import module.ModuleDeleteMode;
import module.PositionableModule;
import patch.Domain;


@RequiredArgsConstructor
public class RestoreDataContext {
	@Getter
	private final PatchData patch;
	@Getter
	private final Domain domain;
	
	@Getter@Setter
	private URL location;
	@Getter
	private SaveMode saveMode = SaveMode.SERIALIZE;
	@Getter
	private boolean gui = true;
	
	private final List<Runnable> lastRestoreTasks = new ArrayList<>(); 
	
	
	public RestoreDataContext setSaveMode(SaveMode saveMode) {
		this.saveMode = saveMode;
		return this;
	}
	
	public RestoreDataContext disableGui() {
		this.gui = false;
		return this;
	}
	
	public int getPatchVersion() {
		return patch.getVersion();
	} 
	
	/**
	 * Code to be executed after Module initialization is completed
	 * @param r 
	 */
	public void addLateTask(Runnable r) {
		
		lastRestoreTasks.add(r);
	}
	
	public void restorePatch() {
		
		//AssetRegistry.restoreAssets(patch.assets); //TODO Assetrestore?
		
		if(location != null)
			domain.setLocation(location);
		
		if(gui)
			restorePatchProperties(patch.width, patch.height);
		
		domain.setDomainID(patch.id);
		
		instantiateNodes();
		addModules();
		
		for (ModuleData md : patch.nodes) {
			md.resetExtraCounter();
			md.moduleOut.doDeserialize(md, this);
		}
		restoreStacks();
		restorePaths();
		
		if(saveMode == SaveMode.SERIALIZE && patch.particleSystems != null) {
			
			for (var psd : patch.particleSystems) {
				psd.restoreParticleSystem();
			}
		}
		
		restoreLinks();
		
		for (var r : lastRestoreTasks) 
			r.run();
		
		//domain.getSignalFlow().process();
	}
	
	
	public void restorePatchProperties(int w, int h) {
		
		int id = patch.currentNodeId;
		if(id > domain.getCurrentModuleID())
			domain.setCurrentModuleID(id);
		
		if(w > 0 && h > 0) {
			
			domain.getPApplet().setWindowSize(w, h);
		}
		
	}

	public void instantiateNodes() {
		
		for (var md : patch.nodes) {
			
			if(md.name == null ) {
				md.moduleOut = new MissingNode("Missing");
			}else{
				
				md.moduleOut = domain.getNodeRegistry().createInstance(md.name, md.type);
				
				if(md.moduleOut == null) {
					
					md.moduleOut = new MissingNode(md.name);
					System.err.println("Node " + md.type + "/" + md.name + " not found.");
				}
			}
			
		}
		
	}
	
	public void addModules() {
		
		AbstractModule.noGUI(!gui); 
		
		for (var md : patch.nodes) {
			var m = md.moduleOut;
			m.setModuleID(md.id);
			domain.addModule(m);
		}
			
		AbstractModule.noGUI(false);
	}

	public ModuleData getModuleByID(int id) {
		return patch.getModuleByID(id);
	}
	
	public PathData getPathData(ModuleData md) {
	
		for (PathData pd : patch.paths) 
			if(pd.node == md)
				return pd;
		
		return null;
	}
	
	public void restorePaths() {
		
		for (PathData pd : patch.paths) {
			
			var pc = (PosConstraintContainer) pd.node.moduleOut;
			pc.doDeserializePath(pd, this);
			LoligoPosConstraint pcd = pc.getPosConstraint();
			
			if(pd.anims != null) {
				
				for(ConstrainableData cd : pd.anims) {
					
					Constrainable c = cd.restore(this);
					if(c == null) {
						System.err.println("Couldn't restore Constrainable of type " + cd.type + " in Path " + pd.toString() + ".");
					}else {
						Path s = pcd.getPaths().get(cd.segment);
						pcd.addFollower(c, s, cd.position);
					}
				}
			}
		}
	}
	

	public void restoreStacks() {
		for (StackData sd : patch.stacks) {
			
			var stack = (AudioStack) sd.stack.moduleOut;
			
			for (ModuleData md : sd.nodes) {
				AudioModule au;
				if(md.moduleOut instanceof AudioModule a) {
					au = a;
				}else {
					au = new MissingNode.Audio(md.moduleOut.getName());
					domain.removeModule(md.moduleOut, ModuleDeleteMode.NODE);
					md.moduleOut = au;
					domain.addModule(au);
				}
				stack.addModule(au);
			}
		}
	}
	

	public void restoreLinks() {
		
		domain.getSignalFlow().suspendBuild();
		
		for (LinkData link : patch.links) {
			
			var in = link.nodeIn.moduleOut.getInputs().stream()
					.filter(i -> i.getName().equals(link.input)).findAny();
			
			var out = link.nodeOut.moduleOut.getOutputs().stream()
					.filter(o -> o.getName().equals(link.output)).findAny();
			
			if(!in.isPresent()) {
				System.err.println(
						"Link failed: Missing Input \"" + 
						link.input + "\" in " + 
						link.nodeIn.moduleOut);
				continue;
			}
			if(!out.isPresent()) {
				System.err.println(
						"Link failed: Missing Output \"" + 
						link.output + "\" in " + 
						link.nodeOut.moduleOut);
				continue;
			}
			
			domain.getModuleManager().connect(in.get(), out.get());
		}
		
		domain.getSignalFlow().resumeBuild();
	}
	
	
	public void processPositionableModule(PositionableModule pm) {
		
	}
	
	public void processAnim(Anim anim) {
		
	}
	
	public void processAudioModule(AudioModule au) {
		
	}

	public void processModule(Module m) {
		
	}

	public void restoreReference(ModuleData data, Consumer<Module> cons) {
		addLateTask(() -> {
 			var r = (Reference) data.nextObject();
 			var a = r.resolve();
 			cons.accept(a);
 		});
	}

	public RestoreDataContext resetNodeIDs() {
		patch.nodes.forEach(md -> md.id = domain.getNewModuleID());
		return this;
	}
	
	
}
