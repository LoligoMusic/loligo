package module;

import pr.DisplayObjectIF;

public interface Anim extends PositionableModule{

	public DisplayObjectIF createAnim();

	public DisplayObjectIF getAnim();

	public void setAnim(DisplayObjectIF anim);

	int pipette(int x, int y);

}