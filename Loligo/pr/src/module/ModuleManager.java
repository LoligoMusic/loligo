package module;

import static constants.ModuleEvent.MODULE_REMOVE;
import static constants.SlotEvent.SLOT_CONNECT;
import static constants.SlotEvent.SLOT_DISCONNECT;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import constants.ModuleEvent;
import constants.ModuleEventContext;
import constants.ModuleEventListener;
import constants.SlotEvent;
import constants.SlotEventContext;
import constants.SlotEventListener;
import gui.CopyPaste;
import gui.DisplayObjects;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.Output;
import module.signalFlow.SignalFlowIF;
import patch.Domain;
import patch.DomainObject;
import patch.LoligoPatch;
import pr.DisplayObjectIF;
import pr.ModuleGUIWrapper;
import pr.RootClass;


public class ModuleManager {
	
	
	public static List<Module> getConnected(Module module) {
		return getConnected(Collections.singletonList(module));
	}
	
	public static List<Module> getConnected(List<Module> modules) {
		
		var list = new ArrayList<Module>();
		list.addAll(modules);
		
		int i = 0;
		while(i < list.size()) {
			var m = list.get(i);
			
			if(m.getOutputs() != null) {
				for (var out : m.getOutputs()) {
					var ins = out.getPartners();
					if(ins != null) {
						ins.stream().map(InOutInterface::getModule)
									.filter(n -> !list.contains(n))
									.forEach(list::add);
					}
				}
			}
			if(m.getInputs() != null) {
				for (var out : m.getInputs()) {
					var in = out.getPartner();
					if(in != null && !list.contains(in.getModule())) {
						list.add(in.getModule());
					}
				}
			}
			i++;
		}
		return list;
	}
	
	
	public static void deleteSelected(LoligoPatch patch) {
		
		RootClass.mainPatch().getHistory().execute(() -> {
		
			for (Module m : CopyPaste.getSelectedModules(patch)) { 
				patch.removeModule(m, ModuleDeleteMode.NODE);
			}
		});
	}
	
	public static void resetSelected(LoligoPatch patch) {
		
		RootClass.mainPatch().getHistory().execute(() -> {
			
			for (Module m : CopyPaste.getSelectedModules(patch)) { 
				
				m.resetModule();
				m.defaultSettings();
				m.processIO();
			}
		});
	}
	
	public static ModuleManager get(DomainObject d) {
		
		return d.getDomain().getModuleManager();
	}
	
	
	private final Domain domain;
	private final ModuleEventContext moduleEventContext;
	private final SlotEventContext slotEventContext;
	
	
	public ModuleManager(Domain domain){
		
		this.domain = domain;
		
		moduleEventContext = new ModuleEventContext();
		slotEventContext = new SlotEventContext();
	}
	
	public void dispatchSlotEvent(SlotEvent e, Input in, Output out) {
		
		slotEventContext.dispatchEvent(e, in, out);
	}
	
	public void addModuleEventListener(ModuleEventListener lis) {
		
		moduleEventContext.addListener(lis);
	}
	
	public void removeModuleEventListener(ModuleEventListener lis) {
		
		moduleEventContext.removeListener(lis);
	}
	
	public void addSlotEventListener(SlotEventListener lis) {
		
		slotEventContext.addListener(lis);
	}
	
	public void removeSlotEventListener(SlotEventListener lis) {
		
		slotEventContext.removeListener(lis);
	}
	
	
	/*
	public Module newModule(NodeEntry entry) {
		
		Module m = entry.createInstance();
		
		newModule(m);
		
		return m;
	}
	
	public void newModule(Module m) {
		
		if(domain.getContainedModules().contains(m))
			return;
		
		m.setDomain(domain);
		
		m.postInit();
		
		assignGuiElement(m, m.getGui());
		
		m.onAdd();
		moduleEventContext.dispatchModuleEvent(InputEvent.MODULE_ADD, m);

		domain.getDisplayManager().addModuleDisplay(m);
		
	}
	*/
	
	
	public void dispatchModuleEvent(ModuleEvent e, Module m) {
		
		moduleEventContext.dispatchModuleEvent(e, m);
	}
	
	/*
	static public void deleteModule(List<? extends AbstractModule> list) {
		for (int i = 0; i < list.size(); i++) 
			deleteModule(list.get(i));
	}
	*/
	
	public void deleteModule(List<? extends Module> mods) {
		
		if(!mods.isEmpty()) {
			
			var flow = mods.get(0).getDomain().getSignalFlow();
			flow.suspendBuild();
			
			for (int i = mods.size() - 1; i >= 0; i--) {
				domain.removeModule(mods.get(i), null);
			} 
			
			flow.resumeBuild();
		
		}
	}
	
	public void doDelete(Module m, ModuleDeleteMode mode){
		var mg = m.getModuleGUI();
		if(mg != null) {
			mg.onModuleDeleted();
			DisplayObjects.removeFromDisplay(m.getGui());
		}
		
		m.onDelete(mode);
		moduleEventContext.dispatchModuleEvent(MODULE_REMOVE, m);
	}

	public void removeAllLinks(Module m) {
		SignalFlowIF flow = m.getDomain().getSignalFlow();
		flow.suspendBuild();
		flow.removeModule(m);
		
		
		if (m.getInputs() != null) 
			for(Input s : m.getInputs())
				disconnect(s);
				
		if (m.getOutputs() != null)
			for(Output s : m.getOutputs())
				while(!s.getPartners().isEmpty()) //TODO
					disconnect(s.getPartners().get(0));
		
		flow.resumeBuild();
	}
	
	/*
	static public void connect(AudioModule m1, AudioModule m2){
		if(m1.beadOut != null && m2.beadIn != null) {
			Set<UGen> ins = m2.beadIn.getConnectedInputs();
		}
	}
	*/
	
	
	public Module getModuleFromGui(DisplayObjectIF d) {
		if(d instanceof ModuleGUIWrapper w) 
			return w.getModule();  
		return null;
	}
	
	public void connect(Input in, Output out) {
		
		if(in.getModule() == out.getModule()) 
			return;
			
		if(in.getPartner() != null)
			disconnect(in);
				
		in.setPartner(out);
			
		if(!out.getPartners().contains(in)) 
			out.addInput(in);
		
		if(in.getModule().getDomain() != null)
			in.getModule().getDomain().getSignalFlow().tryBuild();
		
		slotEventContext.dispatchEvent(SLOT_CONNECT, in, out);
	}
	
	public void connect(InOutInterface s1, InOutInterface s2){
		
		if(s1 instanceof Input in && s2 instanceof Output out)
			connect(in, out);
		
		else if(s1 instanceof Output out && s2 instanceof Input in)
			connect(in, out);
	}
	
	
	public void disconnect(Input in) {
		
		Output out = in.getPartner();
		
		in.removePartner();
				
		if(out != null){
			
			out.removeInput(in);
			in.getModule().getDomain().getSignalFlow().tryBuild();
			slotEventContext.dispatchEvent(SLOT_DISCONNECT, in, out);
		}
	}
	
	
	public void disconnect(Output out) {
		
		List<Input> ins = out.getPartners();
		
		if(ins != null) {
		
			out.getModule().getDomain().getSignalFlow().suspendBuild();
			
			int s = ins.size();
			for (int i = 0; i < s ; i++) {
				disconnect(ins.get(0));
			}
			
			out.getModule().getDomain().getSignalFlow().resumeBuild();
		}
	}
	
	public void disconnect(InOutInterface io) {
		if(io.getIOType() == IOType.IN)
			disconnect((Input) io);
		else
			disconnect((Output) io);
	}
	
	
	public Module duplicate(Module old) {
		
		//domain.getSignalFlow().build();
    	Module m = old.createInstance();
		m.setName(old.getName());
    	//if(m.box != null)
    	//	m.box.setName(name);
    	
		//newModule(m);
    	
		/*
		if(fullName != null)
    		m = DynClassLoader.getModule(fullName);
    	if(m == null)
    		m = DynClassLoader.getModule(getClass().getName());
    	*/
		List<Input> inSlots = old.getInputs();
		
    	if(inSlots != null)
			for (int i = 0; i < inSlots.size(); i++) {
				Input s2 = m.getInputs().get(i);
				Input s1 = inSlots.get(i);
				s2.copyValue(s1);
				if(s1.getPartner() == null)
					s2.copyValue(s1);//s2.setInput(s1.value);
				//else 
				//	ModuleManager.connect(s2, s1.getPartner());
			}
		
    	m.processIO();
		return m;
	
	}
	
}
