package module;

import static gui.DisplayObjects.removeFromDisplay;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import constants.Flow;
import gui.particles.prog.ParticleParam;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.val;
import lombok.var;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.NumberOutputIF;
import module.inout.NumericAttribute;
import module.inout.Output;
import patch.Domain;
import patch.DomainType;
import patch.ParamHolderDomain;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import save.IOData;
import save.ModuleData;
import util.Naming;

@NoArgsConstructor(access = AccessLevel.PRIVATE)

public final class Modules {
	
	public static void initializeModuleDefault(Module c, Domain patch) {
		c.setLabel(Naming.findFreeModuleName(c, patch));
		patch.addModule(c);
		c.defaultSettings();
	}
	
	public static Module findModuleByID(ModuleContainer p, int id) {
		
		for(Module m : p.getContainedModules()) {
			if(m.getModuleID() == id)
				return m;
		}
		return null;
	}
	
	public static List<Module> findModuleByName(ModuleContainer p, String name) {
		
		return p.getContainedModules()
			    .stream()
			    .filter(m -> m.getName().equals(name))
			    .collect(Collectors.toList());
	}
	
	public static boolean hasInputs(Module m) {
		var ios = m.getInputs();
		if(ios == null)
			return false;
		return ios.size() > 0;
	}
	
	public static boolean hasOutputs(Module m) {
		var ios = m.getOutputs();
		if(ios == null)
			return false;
		return ios.size() > 0;
	}
	
	public static boolean containsInputNamed(Module m, String name) {
		
		return getIOByName(m.getInputs(), name) != null;
	}
	
	
	public static boolean containsOutputNamed(Module m, String name) {
		
		return getIOByName(m.getOutputs(), name) != null;
	}
	
	
	public static Input getInputByName(Module m, String name) {
		
		return (Input) getIOByName(m.getInputs(), name);
	}
	
	
	public static Output getOutputByName(Module m, String name) {
		
		return (Output) getIOByName(m.getOutputs(), name);
	}
	
	public static InOutInterface getIOByName(Module m, IOType ioType, String name) {
		
		if(ioType == IOType.IN) {
			return getInputByName(m, name);
		}else {
			return getOutputByName(m, name);
		}
			
	}
	
	public static InOutInterface getIOByName(List<? extends InOutInterface> ios, String name) {
		
		if(ios != null) 
			for (InOutInterface o : ios) 
				if(o.getName().equals(name)) 
					return o;
		
		return null;
	}

	
	public static void restoreDynamicInputs(ModuleData md) {
		
		List<IOData> ins = md.getInputs();
		Module m = md.getModuleOut();
		
		for (IOData io : ins) {
			if(!containsInputNamed(m, io.name))
				m.addIO(addDynamicInput(m, io.name, io.type));
		}
	}
	
	public static void restoreDynamicOutputs(ModuleData md) {
		
		List<IOData> outs = md.getOutputs();
		Module m = md.getModuleOut();
		
		for (IOData io : outs) {
			if(!containsOutputNamed(m, io.name))
				m.addIO(addDynamicOutput(m, io.name, io.type));
		}
	}
	
	/*
	public static ParticleParam addParamConnection(ParamHolder ph1, ParamHolder ph2, String name, DataType type) {
		
		ParticleParam p = 
			ph1 != null && (p = ph1.getParam(name, IOType.IN))  != null ? p :
			ph2 != null && (p = ph2.getParam(name, IOType.OUT)) != null ? p :
																		  new ParticleParam(name, type, null);
		
		if(ph1 != null && ph1.module() != null) {
			InOutInterface in = addDynamicInput(ph1.module(), name, type);
			p.addInOut1(in);
			p.setValue(in.getValueObject());
			ph1.getParamHolderObject().addParam(p, IOType.IN);
		}
		
		if(ph2 != null && ph2.module() != null) {
			InOutInterface out = addDynamicOutput(ph2.module(), name, type);
			p.addInOut2(out);
			ph2.getParamHolderObject().addParam(p, IOType.OUT); 
		}
		
		return p;
	}
	*/
	
	public void removeParamConnection(ParticleParam param) {
		
		/*
		for(InOutInterface io : Arrays.asList(param.getIo1(), param.getIo2())) {
			
			ParamHolder h = (ParamHolder) io.getModule();
			h.removeParam(param);
			
		}
		
		InOutInterface io1 = param.getIo1(),
					   io2 = param.getIo2();
		
		m2 = io2.getModule(); 
		
		m1.removeIO(io1);
		m2.removeIO(io2);
		*/
		
	}
	
	
	public static InOutInterface addDynamicIO(Module module, String name, DataType type, IOType io) {
		
		return io == IOType.IN ? addDynamicInput(module, name, type) :
								 addDynamicOutput(module, name, type);
	}
	
	public static Input addDynamicNumericInput(Module module, String name, NumericAttribute a) {
		BiFunction<String, Module, Input> f = (n, m) -> DataType.createInput(n, m, a);
		return addDynamicInput(module, name, f);
	}
	
	public static Input addDynamicInput(Module module, String name, DataType type) {
		return addDynamicInput(module, name, type::createInput);
	}
	
	public static Input addDynamicInput(Module module, String name, BiFunction<String, Module, Input> inputSupplier) {
		
		Input io = (Input) Modules.getIOByName(module.getInputs(), name);
		
		if(io == null) {
			io = inputSupplier.apply(name, module);
			io.setOptional();
			module.addIO(io);
			initIOGUI(module, io);
		}
		return io;
	}
	
	public static Output addDynamicOutput(Module module, String name, BiFunction<String, Module, ? extends Output> outputSupplier) {
		Output io = (Output) Modules.getIOByName(module.getOutputs(), name);
		
		if(io == null) {
			io = outputSupplier.apply(name, module);
			io.setOptional();
			module.addIO(io);
			initIOGUI(module, io);
		}
		return io;
	}
	
	public static NumberOutputIF addDynamicNumericOutput(Module module, String name, NumericAttribute a) {
		BiFunction<String, Module, Output> f = (n, m) -> DataType.createOutput(n, m, a);
		return (NumberOutputIF) addDynamicOutput(module, name, f);
	}
	
	public static Output addDynamicOutput(Module module, String name, DataType type) {
		
		return addDynamicOutput(module, name, type::createOutput);
	}
	
	
	public static void removeDynamicInputs(Module module) {
		
		var ios1 = module.getInputs();
		if(ios1 != null) {
			Input[] ios = ios1.toArray(new Input[ios1.size()]);
			for (Input in : ios) 
				if(in.isOptional()) 
					module.removeIO(in);
		}
	}
	
	
	public static void removeDynamicOutputs(Module module) {
		
		var ios1 = module.getOutputs();
		if(ios1 != null) {
			Output[] ios = ios1.toArray(new Output[ios1.size()]);
			for (Output o : ios) 
				if(o.isOptional()) 
					module.removeIO(o);
		}
	}


	public static void initIOGUI(Module module, InOutInterface io) {
		
		if(module.getGui() != null) {
			
			DisplayObjectIF d = io.createGui();
			module.getGui().addChild(d);
			//Modules.positionSlots(module);
			module.getModuleGUI().positionSlots();
		}
	}
	
	public static void positionSlots(Module module) {
		if(module.checkFlag(Flow.ROUNDSLOTS)) 
			positionSlotsRound(module);
		else
			positionSlotsRect(module);
	}

	static public void positionSlotsRect(Module m){ 
		
		int rad = 6, minH = 40;
		
		DisplayObjectIF gui = m.getGui();
		
		List<Input> ins = m.getInputs();
		List<Output> outs = m.getOutputs();
		
		int max = Math.max(ins  == null ? 0 : ins.size(), 
						   outs == null ? 0 : outs.size());
		
		int h = Math.max(max * rad, ModuleGUI.HEIGHT_DEFAULT);
		h = Math.min(h, minH);
		
		gui.setWH(gui.getWidth(), h);
		
		for (int j = 0; j < 2; j++) {
			
			boolean in = j == 0;
			List<? extends InOutInterface> ss = in ? ins : outs;
			
			if(ss != null) {
				
				int i = 0;
				for (InOutInterface s : ss) {
					
					if(s != null) {
						
						var g = s.getGUI();
						if(g.isHidden()) {
							removeFromDisplay(g);
						}else {
							m.getModuleGUI().addChild(g);
							float rx = (in ? 0 + 2f : gui.getWidth() - 2f),
								  ry = (gui.getHeight() - (ss.size()-1) * rad) / 2 + i * rad;
							
							if(g != null)
								g.setPos(rx, ry);
							
							i++;
						}
					}
					
				}
			}
		}
		
	}
	
	
	public static void positionSlotsRound(Module m) {
		
		var gui = m.getGui();
		
		if(m.getInputs() != null) 
			round(m.getInputs(), gui.getWidth(), -1);
		
		if(m.getOutputs() != null) 
			round(m.getOutputs(), gui.getWidth(), 1);
	}
	
	
	private static void round(List<? extends InOutInterface> ios, float width, float mirror) {
		
		width = width / 2 + 5;
		
		float step = .5f * mirror;
		float w = 1.57f - step * (ios.size() - 1) / 2;
		
		for (InOutInterface s : ios) {
			
			var g = s.getGUI();
			
			if(g.isHidden()) {
				removeFromDisplay(g);
			}else {
				s.getGUI().setPos((float) Math.sin(w) * width * mirror,
						  (float) Math.cos(w) * width);
				w += step;
			}
		}
	}
	
	public static void iterateParentPatches(Domain d, Predicate<ParamHolderDomain> foo) {
		
		Domain dom = d;
		
		while (dom.domainType() == DomainType.SUBPATCH) {
			
			var subDom = (ParamHolderDomain) dom;
			
			if(!foo.test(subDom))
				break;
			
			dom = subDom.getParentDomain();
		}
	}
	
	
	public static void moveIOs(Module source, Module target) {
		moveInputs(source, target);
		moveOutputs(source, target);
	}
	
	public static void moveInputs(Module source, Module target) {
		var man = source.getDomain().getModuleManager();

		for (val in1 : source.getInputs()) {
			var in2 = getInputByName(target, in1.getName());
			if(in2 != null) {
				var out = in1.getPartner();
				if(out == null) {
					man.disconnect(in2);
				}else {
					man.connect(in2, out);
					man.disconnect(in1);
				}
			}
		}
		
	}
	
	public static void moveOutputs(Module source, Module target) {
		var man = source.getDomain().getModuleManager();
		
		for (val out1 : source.getOutputs()) {
			var out2 = getOutputByName(target, out1.getName());
			if(out2 != null) {
				var ins = out1.getPartners();
				if(ins == null || ins.isEmpty()) {
					man.disconnect(out2);
				}else {
					for (val in : ins) 
						man.connect(in, out2);
					man.disconnect(out1);
				}
			}
		}
		
	}
	
	public static List<InOutInterface> getAllIOs(Module m) {
		var li = new ArrayList<InOutInterface>();
		li.addAll(m.getInputs());
		li.addAll(m.getOutputs());
		return li;
	}

	
}
