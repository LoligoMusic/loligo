package module.loading;

import module.Module;

public interface NodeEntry {
	
	public String getName();

	public NodeCategory getType();

	public Module createInstance();
	
	public boolean isHidden(); 
}