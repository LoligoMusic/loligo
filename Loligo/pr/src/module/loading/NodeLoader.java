package module.loading;

import java.util.List;

public interface NodeLoader {

	List<NodeEntry> loadEntries();
}
