package module.loading;

import java.util.Map;

import gui.particles.modifiers.ParticleModifierEntry;
import gui.particles.prog.Collector;
import gui.particles.prog.Initial;
import gui.particles.prog.ParticleIn;
import gui.particles.prog.ParticleInfo;
import gui.particles.prog.ParticleLife;
import gui.particles.prog.ParticleMove;
import gui.particles.prog.ParticleOut;
import gui.particles.prog.ParticleParamHolder;
import gui.particles.prog.ParticleStats;
import module.modules.anim.CanvasNode;
import module.modules.math.Value;
import patch.IONode;
import patch.ReferenceNodeEntryIF;
import patch.ReferenceNodeRegistry;
import patch.subpatch.SubPatch;
import patch.subpatch.SubPatchInfo;
import patch.subpatch.SubPatchParams;


public class NodeRegistry {
	
	static public final AbstractNodeRegistry<NodeEntry> DEFAULT_NODEREGISTRY = defaultNodeRegistry();
	
	
	private static AbstractNodeRegistry<NodeEntry> defaultNodeRegistry() {
		
		AbstractNodeRegistry<NodeEntry> r = new AbstractNodeRegistry<>();
		
		addLoader(r, new InternalNodeLoader());
		addLoader(r, ArithmeticEntry.getLoader());
		addLoader(r, Value.loader());
		addLoader(r, ParticleModifierEntry.getLoader());
		addLoader(r, new SpecialNodeLoader());
		//addLoader(r, DynamicNodeLoader.createDefault());
		
		r.addEntry(new InternalNodeEntry(SubPatch::new		, "SubPatch",	NodeCategory.TOOLS	 ));
		r.addEntry(new InternalNodeEntry(CanvasNode::new	, "Canvas", 	NodeCategory.ANIM	 ));
		r.addEntry(new InternalNodeEntry(ParticleStats::new	, "PStats", 	NodeCategory.PARTICLE));
		r.addEntry(new InternalNodeEntry(Collector::new		, "Collect", 	NodeCategory.PARTICLE));
		
		return r; 
	}
	
	private static void addLoader(AbstractNodeRegistry<NodeEntry> reg, NodeLoader no) {
		
		Map<String, NodeEntry> map = reg.getEntryMap();
		
		for (NodeEntry ne : no.loadEntries()) {
			
			map.put(ne.getName(), ne);
		}
		
	}
	
	
	public static final Map<String, ReferenceNodeEntryIF<ParticleParamHolder>> PARTICLEFACTORY_ENTRY_MAP = 
		
		new MapBuilder<ParticleParamHolder>().
			newEntry("ParticleOut", NodeCategory.PARTICLE, p -> new ParticleOut(p)).
			newEntry("ParticleIn", NodeCategory.PARTICLE, p -> new ParticleIn(p)).
			newEntry("Info", NodeCategory.PARTICLE, p -> new ParticleInfo(p)).
			newEntry("Move", NodeCategory.PARTICLE, p -> new ParticleMove(p)).
			newEntry("Initial", NodeCategory.PARTICLE, p -> new Initial(p)).
			newEntry("Kill", NodeCategory.PARTICLE, p -> new ParticleLife(p)).
			getMap();
	
	/*
	public static final Map<String, ReferenceNodeEntryIF<ParticleParamHolder>> PARTICLEINSTANCE_ENTRY_MAP = 
			
		new MapBuilder<ParticleParamHolder>().
			newEntry("ParticleOut", NodeType.PARTICLE, p -> new PassiveNodeIn()).
			newEntry("ParticleIn", NodeType.PARTICLE, p -> new ParticleIn(p)).
			newEntry("Info", NodeType.PARTICLE, p -> new ParticleInfo(p)).
			getMap();
	*/
	
	public static final Map<String, ReferenceNodeEntryIF<SubPatch>> SUBPATCH_ENTRY_MAP = 
		
		new MapBuilder<SubPatch>().
			newEntry("In", NodeCategory.SPECIAL, p -> new IONode.In(p)).
			newEntry("Out", NodeCategory.SPECIAL, p -> new IONode.Out(p)).
			newEntry("Info", NodeCategory.SPECIAL,p -> new SubPatchInfo(p)).
			newEntry("Params", NodeCategory.SPECIAL,p -> new SubPatchParams(p)).
			getMap();
	
	
	public static ReferenceNodeRegistry<ParticleParamHolder> createParticleFactoryRegistry(ParticleParamHolder ref) {
		
		return new ReferenceNodeRegistry<>(ref, PARTICLEFACTORY_ENTRY_MAP, NodeRegistry.DEFAULT_NODEREGISTRY);
	}
	
	public static ReferenceNodeRegistry<ParticleParamHolder> createParticleInstanceRegistry(ParticleParamHolder ref) {
		
		return new ReferenceNodeRegistry<>(ref, PARTICLEFACTORY_ENTRY_MAP, NodeRegistry.DEFAULT_NODEREGISTRY);
	}
	
	public static ReferenceNodeRegistry<SubPatch> createSubPatchRegistry(SubPatch ref) {
		
		return new ReferenceNodeRegistry<>(ref, SUBPATCH_ENTRY_MAP, NodeRegistry.DEFAULT_NODEREGISTRY);
	}

	public static String getInternalModuleName(Class<?> clazz) {
		
		NodeInfo[] in = clazz.getAnnotationsByType(NodeInfo.class);
		
		if(in.length == 0)
			return "Unknown";
		NodeInfo an = in[0];
		return an.name().equals(NodeInfo.replaceString) ? 
				clazz.getSimpleName() : 
				an.name();
	}

}
