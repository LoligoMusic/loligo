package module.loading;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = { ElementType.TYPE })

public @interface NodeInfo {

	public static final String replaceString = "@className";
	
	String name() default replaceString;
	NodeCategory type();
	String[] tags() default {};
	boolean hidden() default false;
	boolean ignoreBuild() default false;
	String helpPatch() default "";
	
}
