package module.loading;

public abstract class AbstractNodeEntry implements NodeEntry {

	private final String name;
	private final NodeCategory type;
	private final boolean hidden;
	
	public AbstractNodeEntry(String name, NodeCategory type, boolean hidden) {
		
		this.name = name;
		this.type = type;
		this.hidden = hidden;
	}
	
	@Override
	public String getName() {
		
		return name;
	}

	@Override
	public NodeCategory getType() {
		
		return type;
	}

	@Override
	public boolean isHidden() {
	
		return hidden;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		return 
				obj instanceof NodeEntry e && 
				getType() == e.getType() && 
				getName().equals(e.getName());
	}

	@Override
	public String toString() {
	
		return getName() + "(" + getType() + ")";
	}
	
}
