package module.loading;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import org.jboss.forge.roaster.Roaster;
import org.jboss.forge.roaster.model.source.JavaClassSource;
import org.jboss.forge.roaster.model.source.MethodSource;

import lombok.var;
import util.Utils;


public class InternalNodeLoaderGenerator {

	public static final String path = "pr\\src\\module";//"C:\\Users\\FB1\\git\\loligo\\Loligo\\pr\\src\\module";//
	public static final String loaderName = "InternalNodeLoader";
	
	private static final String nodeEntry = InternalNodeEntry.class.getSimpleName(); 
	
	public static String generateJava() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("return new ");
		sb.append(nodeEntry);
		sb.append("[] {");
		
		
		Consumer<File> c = f -> {
		
			String n = f.getPath();
			
			if(f.isFile() && n.endsWith(".java")) {
				
				n = n.substring(n.indexOf("\\module\\") + 1, n.length() - 5);
				n = n.replace('\\', '.');
				
				var classes = new ArrayList<Class<?>>();
				
				Class<?> cla;
				try {
					cla = Class.forName(n);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					return;
				}
				
				classes.add(cla);
				var subClasses = Arrays.asList(cla.getDeclaredClasses());
				classes.addAll(subClasses);
				
				classes.forEach(s -> writeNodeEntry(sb, s));
			}
		};
		
		Utils.iterateFolder(path + "\\modules", c);
		
		sb.deleteCharAt(sb.length() - 1);
		sb.append("};");
		
		JavaClassSource jcs = Roaster.create(JavaClassSource.class);
		
		jcs.setName(loaderName);
		jcs.setPackage("module.loading");
		jcs.addImport(List.class);
		jcs.addImport(Arrays.class);
		jcs.addImport(NodeCategory.class);
		jcs.addInterface(NodeLoader.class);
				
		MethodSource<JavaClassSource> getter = jcs.addMethod();
		getter.setName("loadEntries");
		getter.setReturnType(List.class.getName() + "<" + NodeEntry.class.getName() + ">");
		getter.setBody("return Arrays.asList(fill());");
		getter.setPublic();
		
		MethodSource<JavaClassSource> ms = jcs.addMethod();
		System.out.println(sb);
		ms.setBody(sb.toString());
		ms.setStatic(true);
		ms.setName("fill");
		ms.setReturnType(NodeEntry[].class);
		
		System.out.println(sb);
		
		return jcs.toString();
		
	}

	private static void writeNodeEntry(StringBuilder sb, Class<?> cla) {
		
		NodeInfo[] in = cla.getAnnotationsByType(NodeInfo.class);
		
		if(in.length == 0)
			return;
		
		NodeInfo an = in[0];
		
		if(an.ignoreBuild())
			return;
		
		String cn = cla.getCanonicalName();//getName();
		
		sb.append("new ");
		sb.append(nodeEntry);
		sb.append("\r\n");
		sb.append("(() -> new ");
		sb.append(cn);
		sb.append("()");
		sb.append(',');
		sb.append("\r\n");
		sb.append('"');
		sb.append(NodeRegistry.getInternalModuleName(cla));
		sb.append('"');
		sb.append(',');
		sb.append("\r\n");
		sb.append(NodeCategory.class.getSimpleName());
		sb.append('.');
		sb.append(an.type());
		
		if(an.hidden()) {
			sb.append(", true");
		}
		
		/*
		if(an.tags().length > 0) {
		
			sb.append(',');
			
			for (int j = 0; j < an.tags().length; j++) {
				sb.append('"');
				sb.append(an.tags()[j]);
				sb.append('"');
				
				if(j < an.tags().length - 1)
					sb.append(',');
			}
		}
		*/
		
		sb.append("),");
	}
	
	public static void main(String[] args) {
		
		String c = generateJava();
		
		try {
			PrintWriter pw = new PrintWriter(new File(path + "\\loading\\" + loaderName + ".java").getCanonicalFile());
			pw.print(c);
			pw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*
		List<String> names = new ArrayList<>();
		
		Consumer<File> co = f -> {
			
			String n = f.getPath();
			
			if(f.isFile() && n.endsWith(".java")) {
				
				n = n.substring(n.indexOf("\\module\\") + 1, n.length() - 5);
				n = n.replace('\\', '.');
				
				names.add(n);
			}
		};
		
		Utils.iterateFolder(path + "\\modules", co);
		
		System.out.println(names);
		
		List<String> annotatedNames = new ArrayList<>();
		
		for (String n : names) {
			
			try {
				
				Class<?> c = Class.forName(n);
				InternalNode[] in = c.getAnnotationsByType(InternalNode.class);
				
				if(in.length > 0) {
					
					annotatedNames.add(n);
					
					System.out.println(c.getName());
					System.out.println(in[0].type());
				}
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		try {
			PrintWriter pw = new PrintWriter(new File(path + "\\nodeNames.SETTINGS"));
			
			for (int i = 0; i < annotatedNames.size(); i++) {
				
				pw.print(annotatedNames.get(i));
				
				if(i < annotatedNames.size() - 1)
					pw.print(',');
				
				pw.println();
			}
			
			
			
			pw.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		/*
			try {
		
				Class<?> cc = Class.forName("module.modules.math.Clock");
					
				String packageName = "module.modules";
	    		
	    		URL url = cc.getResource(packageName);
	    		
	    		File theDir = new File(url.toURI());
				
	            File[] theClasses = theDir.listFiles();
	            
	            for(int i=0; i<theClasses.length; i++) {
	            	
	                String c = theClasses[i].getName();
	            	String className = packageName.replace("/", ".") + "." + c.substring(0, c.contains(".")? c.lastIndexOf(".") : c.length()); 
	                
	            	if(!className.contains("$")) {
	                	System.out.println("Module found: " + className);
	                	
	                }
	            }
            
			} catch (ClassNotFoundException | URISyntaxException e) {
				
				e.printStackTrace();
			}
		
		
		*/
	}
	
	

}
