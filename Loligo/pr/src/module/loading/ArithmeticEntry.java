package module.loading;

import static module.inout.NumericAttribute.BOOL;
import static module.inout.NumericAttribute.DEZIMAL_FREE;
import static module.inout.NumericType.toBool;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

import module.Module;
import module.inout.NumericAttribute;
import module.modules.math.Maths;

public enum ArithmeticEntry implements NodeEntry {

	ADD				("+",		(a, b) -> a + b														),
	SUBTRACT		("-",		(a, b) -> a - b														),
	DIVIDE			("/",		(a, b) -> a / b														),
	MULTIPLY		("*", 		(a, b) -> a * b														),
	POW				("POW", 	Math::pow															),
	
	MAX				("Max", 	Math::max															),
	MIN				("Min", 	Math::min															),
	
	AND				("AND", 	(a, b) -> toBool(a) && toBool(b) ? 1d : 0	, 2, BOOL				),
	OR				("OR", 		(a, b) -> toBool(a) || toBool(b) ? 1d : 0	, 2, BOOL				),
	XOR				("XOR", 	(a, b) -> toBool(a) != toBool(b) ? 1d : 0	, 2, BOOL				),
	NOT				("NOT", 	(a, b) -> !toBool(a) ? 1d : 0				, 1, BOOL				),
	
	GREATER			(">", 		(a, b) -> a >  b ? 1d : 0					, 2, DEZIMAL_FREE, BOOL	),
	SMALLER			("<", 		(a, b) -> a <  b ? 1d : 0					, 2, DEZIMAL_FREE, BOOL	),
	GREATER_OR_EQUAL(">=", 		(a, b) -> a >= b ? 1d : 0					, 2, DEZIMAL_FREE, BOOL	),
	SMALLER_OR_EQUAL("<=", 		(a, b) -> a <= b ? 1d : 0					, 2, DEZIMAL_FREE, BOOL	),
	EQUAL			("=", 		(a, b) -> a == b ? 1d : 0					, 2, DEZIMAL_FREE, BOOL	),

	SINE			("Sin", 	(a, b) -> Math.sin(a)						, 1						),
	ABS				("Abs", 	(a, b) -> Math.abs(a)						, 1						),
	MODULO			("Modulo", 	(a, b) -> a % b														);
	
	public static NodeLoader getLoader() {
		
		return new NodeLoader() {
			
			@Override
			public List<NodeEntry> loadEntries() {
				EnumSet<ArithmeticEntry> en = EnumSet.allOf(ArithmeticEntry.class);
				return new ArrayList<>(en);
			}
		};
	}

	private final String name;
	private final DoubleBinaryOperator foo;
	private final int numOperands;
	private final NumericAttribute numAttIn, numAttOut;
	
	private ArithmeticEntry(String name, DoubleBinaryOperator foo) {
		
		this(name, foo, 2);
	}
	
	private ArithmeticEntry(String name, DoubleBinaryOperator foo, int numOperands) {
		
		this(name, foo, numOperands, DEZIMAL_FREE, DEZIMAL_FREE);
	}
	
	private ArithmeticEntry(String name, DoubleBinaryOperator foo, int numOperands, NumericAttribute numAtt) {
		
		this(name, foo, numOperands, numAtt, numAtt);
	}
	
	private ArithmeticEntry(String name, DoubleBinaryOperator foo, int numOperands, NumericAttribute numAttIn, NumericAttribute numAttOut) {
		
		this.name = name;
		this.foo = foo;
		this.numOperands = numOperands;
		this.numAttIn = numAttIn;
		this.numAttOut = numAttOut;
	}
	
	public NumericAttribute getNumberAttributeIn() {
		
		return numAttIn;
	}
	
	public NumericAttribute getNumberAttributeOut() {
		return numAttOut;
	}
	
	public DoubleBinaryOperator getFunction() {
		return foo;
	}
	
	public int getNumOperands() {
		return numOperands;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public NodeCategory getType() {
		return NodeCategory.MATH;
	}
	
	@Override
	public boolean isHidden() {
		return false;
	}
	
	@Override
	public Module createInstance() {
		return new Maths(this);
	}
	
}
