package module.loading;

public enum NodeStatus {
	VISIBLE,
	HIDDEN,
	IGNORE;
}
