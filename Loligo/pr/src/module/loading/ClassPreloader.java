package module.loading;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.fasterxml.jackson.databind.ObjectMapper;

import gui.CopyPaste;
import javassist.ClassPool;
import javassist.NotFoundException;
import lombok.var;
import save.RestoreDataContext;
import save.SaveDataContext;

public class ClassPreloader {

	@SuppressWarnings("unchecked")
	public static void preload() {
		
		Class<?>[] classes = {
			CopyPaste.class,
			SaveDataContext.class,
			RestoreDataContext.class,
			ObjectMapper.class
		};
			
		ClassPool cp = ClassPool.getDefault();
		var list = new ArrayList<Class<?>>();
		list.addAll(Arrays.asList(classes));
		
		int i = 0;
		while(i < list.size()) {
			
			Class<?> cl = list.get(i);
			String n = cl.getCanonicalName();
			
			try {
				
				Collection<String> cs = cp.get(n).getRefClasses();
				for(String s : cs) {
					Class<?> c = Class.forName(s);
					list.add(c);
				}
				
			} catch (NotFoundException | ClassNotFoundException e) {
				e.printStackTrace();
			}
			i++;
		}
	}
}
