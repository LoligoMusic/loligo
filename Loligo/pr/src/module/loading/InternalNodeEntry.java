package module.loading;

import java.util.function.Supplier;

import module.Module;

public class InternalNodeEntry extends AbstractNodeEntry {

	protected final Supplier<? extends Module> instanceProvider;
	
	public InternalNodeEntry(Supplier<? extends Module> instanceProvider, String name, NodeCategory type) {
		
		this(instanceProvider, name, type, false);
	}
	
	public InternalNodeEntry(Supplier<? extends Module> instanceProvider, String name, NodeCategory type, boolean hidden) {
		
		super(name, type, hidden);
		
		this.instanceProvider = instanceProvider;
	}


	public Supplier<? extends Module> getClazz() {
		
		return instanceProvider;
	}
	
	
	@Override
	public Module createInstance() {
	
		Module m = instanceProvider.get();
		m.setName(getName());
		
		return m;
	}
	
}
