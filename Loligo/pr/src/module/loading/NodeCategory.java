package module.loading;

public enum NodeCategory {

	
	ANIM,
	PARTICLE,
	//AUDIO,
	MATH,
	TOOLS,
	TRANSFORM,
	COLOR,
	READER,
	IO, // MIDI, OSC..
	
	PATH,
	AUDIO,
	AUDIO_FX,
	
	
	SPECIAL;
	
	
	public boolean instanceOf(NodeCategory type) {
		
		if(type == this)
			return true;
		else
		if(superType != null)
			return superType.instanceOf(type);
		else
			return false;
	}
	
	public final NodeCategory superType;
	
	NodeCategory() {
		
		superType = null;
	}
	
	NodeCategory(NodeCategory superType) {
		
		this.superType = superType;
	}
}
