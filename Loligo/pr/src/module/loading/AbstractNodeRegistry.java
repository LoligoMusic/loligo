package module.loading;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import lombok.var;
import module.Module;


public class AbstractNodeRegistry<N extends NodeEntry> {
	
	
	protected final AbstractNodeRegistry<?> parent;
	private final Map<String, N> map;
	
	
	public AbstractNodeRegistry() {
		
		this(null, new HashMap<>());
	}
	
	public AbstractNodeRegistry(AbstractNodeRegistry<?> parent, Map<String, N> map) {
		
		this.parent = parent;
		this.map = map;
	}
	
	
	public Map<String, N> getEntryMap() {
		
		return map;
	}
	
	
	/**Finds entry only in this Registry*/
	 
	public N findEntry(String name, NodeCategory type) {
		
		return getEntryMap().get(name);
	}
	
	
	/**Finds entry recursively in this and all parent Registries*/
	
	public final NodeEntry findEntryAll(String name, NodeCategory type) {
		
		N n = getEntryMap().get(name);
		
		return 
				n 		!= null ? n :
				parent 	!= null ? parent.findEntryAll(name, type) : 
								  null;
	}
	
	public final NodeEntry findEntryAll(Class<? extends Module> clazz) {
		
		var ans = clazz.getDeclaredAnnotation(NodeInfo.class);
		if(ans == null)
			return null;
		
		var n = NodeRegistry.getInternalModuleName(clazz);
		return findEntryAll(n, ans.type());
	}
	
	public void addEntry(N entry) {
		
		if(map.containsKey(entry.getName()))
			System.err.println("Name collision in NodeRegistry: " + entry.getName());
		
		map.put(entry.getName(), entry);
	}
	
	
	public Collection<? extends N> loadEntries() {
		
		return getEntryMap().values();
	}
	
	public final Collection<? extends NodeEntry> loadAllEntries() {
		
		Collection<? extends N> col = loadEntries();
		
		if(parent != null) {
			
			Iterable<? extends NodeEntry> it = Iterables.concat(loadEntries(), parent.loadAllEntries());
			return Lists.newArrayList(it);
		}else {
			return col;
		}
		
	}
	
	
	public <T extends Module> T createInstance(Class<T> clazz) {
		var en = findEntryAll(clazz);
		Module m = en == null ? null : en.createInstance();
		return clazz.cast(m);
	}
	
	public Module createInstance(N entry) {
		
		return entry.createInstance();
	}
	
	public Module createInstance(String name, NodeCategory type) {
		
		N n = map.get(name);
		
		return n	  != null ? createInstance(n) : 
			   parent != null ? parent.createInstance(name, type) : 
				   				null;
	}
	
	
}
