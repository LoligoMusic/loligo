package module.loading;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import module.Module;


public class DynamicNodeLoader implements NodeLoader{

	
	public static DynamicNodeLoader createDefault() {
		
		return new DynamicNodeLoader("C:\\Users\\Vanja\\Desktop\\TestNode.jar");
	}
	
	
	private String path;

	
	public DynamicNodeLoader(String path) {
		
		this.path = path;
	}
	
	
	@Override
	public List<NodeEntry> loadEntries() {
		
		return load(path).stream()
				  		 .map(c -> new DynamicNodeEntry(c, c.getSimpleName(), NodeCategory.MATH))
				  		 .collect(Collectors.toList());
	}
	
	
	static class DynamicNodeEntry extends AbstractNodeEntry {
		
		private final Class<?> clazz;
		
		public DynamicNodeEntry(Class<?> clazz, String name, NodeCategory type) {
			
			super(name, type, false);
			
			this.clazz = clazz;
		}

		@Override
		public Module createInstance() {
			
			try {
				Module m = (Module) clazz.newInstance();
				m.setName(getName());
				return m;
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
			return null;
		}
	}
	
	
	public static List<Class<?>> load(String pathToJar) {
		
		try(JarFile jarFile = new JarFile(pathToJar)) {
			
			Enumeration<JarEntry> e = jarFile.entries();
			URL u;
			
				u = new URL("jar:file:" + pathToJar+"!/");
			
			URL[] urls = {u};
			URLClassLoader cl = URLClassLoader.newInstance(urls);
			
			List<Class<?>> classes = new ArrayList<>();
			
			while (e.hasMoreElements()) {
			    JarEntry je = e.nextElement();
			    if(je.isDirectory() || !je.getName().endsWith(".class")){
			        continue;
			    }
			    
			    String className = je.getName().substring(0,je.getName().length()-6);// -6 because of .class
			    className = className.replace('/', '.');
			    Class<?> c = cl.loadClass(className);
			    classes.add(c);
			}
			
			return classes;
			
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		return new ArrayList<>(0);
	}

}
