package module.loading;

import java.util.ArrayList;
import java.util.List;

import audio.AudioStack;

public class SpecialNodeLoader implements NodeLoader {

	@Override
	public List<NodeEntry> loadEntries() {
		
		List<NodeEntry> entries = new ArrayList<>();
		
		NodeEntry stackentry = new InternalNodeEntry(()->{return new AudioStack();}, AudioStack.audioStackName, NodeCategory.AUDIO, true); 
		
		entries.add(stackentry);
		
		return entries;
	}

}
