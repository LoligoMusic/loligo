package module.loading;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import lombok.Getter;
import module.Module;
import patch.ReferenceNodeEntry;
import patch.ReferenceNodeEntryIF;

public class MapBuilder<T>  {
	
	@Getter
	private Map<String, ReferenceNodeEntryIF<T>> map = new HashMap<>();
	
	MapBuilder<T> newEntry(String name, NodeCategory type, Function<T, Module> foo) {
	
		ReferenceNodeEntryIF<T> p = new ReferenceNodeEntry<>(foo, name, type, false);
		map.put(name, p);
		return this;
	}
	
}