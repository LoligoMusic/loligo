package module;

import constants.Flow;
import constraints.FollowerObject;
import constraints.LoligoPosConstraint;
import gui.DragHandle;
import gui.Position;
import gui.selection.GuiType;
import pr.ModuleGUI;
import pr.Positionable;
import save.ConstrainableData;
import save.ModuleData;
import save.PathData;
import save.RestoreDataContext;
import save.SaveDataContext;

public abstract class AbstractPositionableModule extends AbstractModule implements PositionableModule {

	private Positionable pos;
	private FollowerObject followerObject;
	
	public AbstractPositionableModule() {
		
		this(new Position.NestedPosition());
	}
	
	public AbstractPositionableModule(Positionable pos) {
		
		this.pos = pos;
		flowFlags.add(Flow.ROUNDSLOTS);
	}

	@Override
	public ModuleGUI createGUI() {
		
		DragHandle h = new DragHandle(this, null);
		h.setGuiType(GuiType.ANIM);
		
		return ModuleGUI.createModuleBox(this, h);
	}
	
	@Override
	public void onAdd() {
	
		super.onAdd();
		
		if(getGui() != null)
			getDomain().getConstraintContext().assignConstraintGui(this);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
	
		super.onDelete(mode);
		
		LoligoPosConstraint pc = getContainingPosConstraint();
		if(pc != null)
			pc.removeFollower(this);
		
		getDomain().getConstraintContext().removeConstraintGui(getGui());
	}
	
	
	public float getX() {
		return pos.getX();
	}


	public float getY() {
		return pos.getY();
	}


	public void setPos(float x, float y) {
		pos.setPos(x, y);
	}


	public Position getPosObject() {
		return pos.getPosObject();
	}


	public void setPosObject(Position p) {
		pos.setPosObject(p);
	}


	public void setGlobal(float x, float y) {
		pos.setGlobal(x, y);
	}

	
	
	/*
	@Override
	public PosConstraint getContainingPosConstraint() {
		return posConstraint;
	}
	
	/*
	public void setContainingPosConstraint(PosConstraint posConstraint) {
		
		this.posConstraint = posConstraint;
	}
	*/
	
	@Override
	public void updatePosition() {
		
	}
	
	@Override
	public FollowerObject getFollowerObject() {
		
		return followerObject;
	}


	@Override
	public void setFollowerObject(FollowerObject followerObject) {
		this.followerObject = followerObject;
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc); 
		
		SaveDataContext.savePos(this, md);
		
		return md;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		setGlobal(data.xy[0], data.xy[1]);
		
		super.doDeserialize(data, rdc);
		rdc.processPositionableModule(this);
	}
	
	
	@Override
	public ConstrainableData toConstrainableMemento(SaveDataContext sdc, PathData pd) {
		
		ConstrainableData cd = new ConstrainableData();
		
		cd.type = ConstrainableData.Type.NODE;
		cd.node = sdc.findData(this);
		cd.segment = followerObject.getPosConstraint().getPaths().indexOf(followerObject.getPath());
		followerObject.copyPositionTo(cd);
		return cd;
	}
	
	
	@Override
	public void restoreMemento(ConstrainableData cd) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean checkType(ModuleType t) {
		return t == ModuleType.POSITIONABLE;
	}
}
