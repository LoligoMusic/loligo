package module;

import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicScrollBarUI;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.var;
import util.ComponentMover;
import util.ComponentResizer;
import util.Images;


@NoArgsConstructor(access = AccessLevel.PRIVATE)

public final class Console {

	private static GuiStream guiStream;
	@Getter
	private static PrintStream stream;
	@Getter
	private static JFrame frame;
	@Getter
	private static JTextArea textArea;
	
	
	public static void init() {
		EventQueue.invokeLater(() -> {	
			guiStream = new GuiStream();
			stream = new PrintStream(guiStream);
		});
	}
	
	
	public static void showConsole() {
		/*
		EventQueue.invokeLater(() -> {	
			stream = new PrintStream(new GuiStream());
			
			frame.setVisible(true);
			frame.toFront();
		});
		*/
		frame.setVisible(true);
		frame.toFront();
	}
	
	
	
	private static JTextArea createTextArea() {
			
		frame = new JFrame();
		frame.setTitle("Console");
		frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		var img = Images.loadBufferedImage("/resources/icon16.png");
		frame.setIconImage(img);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setResizable(true);
        frame.setUndecorated(true);
        new ComponentMover(frame, frame);
        new ComponentResizer(frame);
        
        
        var bar = new JPanel();
        bar.setLayout(new BorderLayout());
        frame.add(bar);
        
        var title = new JLabel("Console");
        title.setBorder(new EmptyBorder(0, 10, 0, 0));
        disableMouseForComponent(title);
        bar.add(title, BorderLayout.WEST);
        
        var barRight = new JPanel();
        barRight.setLayout(new BoxLayout(barRight, BoxLayout.X_AXIS));
        bar.add(barRight, BorderLayout.EAST);
        
        Icon ic = new ImageIcon(Images.loadBufferedImage("/resources/clear_console.png"), "Clear");
		var b = new JButton(ic);
		b.setPreferredSize(new Dimension(15, 15));
		b.setAlignmentX(Component.RIGHT_ALIGNMENT);
	    b.addActionListener(e -> textArea.setText("")); 
	    barRight.add(b);
	    
	    b.setBorderPainted(false);
	    b.setFocusPainted(false);
	    b.setContentAreaFilled(false);
	    
	    barRight.add(Box.createHorizontalStrut(20));
	    
	    Icon ic2 = new ImageIcon(Images.loadBufferedImage("/resources/delete.png"), "Close");
		var cl = new JButton(ic2);
		cl.setAlignmentX(Component.RIGHT_ALIGNMENT);
		cl.setPreferredSize(new Dimension(15, 15));
		cl.addActionListener(e -> frame.dispose());
		barRight.add(cl);
        
		var filter = new JTextField("*", 5);
		filter.addActionListener(e -> guiStream.setPattern(filter.getText()));
	    frame.add(filter);
		
		var tp = new JTextArea(10, 20);
		tp.setEditable(false);
		tp.setFocusable(true);
		tp.setBackground(Color.DARK_GRAY);
		tp.setSelectionColor(Color.WHITE);
		tp.setForeground(Color.WHITE);
		tp.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		UIManager.put("ScrollBar.width", 8);
		
		JScrollPane scroll = new JScrollPane(tp, VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_NEVER);
		
		
		var scrollui = new BasicScrollBarUI() {
			
			Color bg = Color.DARK_GRAY;
			Color th = new Color(0, 0, 0, 70);
			
			protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
				g.setColor(th);
				g.fillRoundRect(r.x, r.y, r.width, r.height, 4, 4);
			}
			protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
				g.setColor(bg);
				g.fillRect(r.x, r.y, r.width, r.height);
			}
			protected void paintDecreaseHighlight(Graphics g) {}
			protected void paintIncreaseHighlight(Graphics g) {}
			protected JButton createDecreaseButton(int orientation) {
				var b = new JButton();
				b.setPreferredSize(new Dimension(0, 0));
				return b;
			}
			protected JButton createIncreaseButton(int orientation) {
				return createDecreaseButton(orientation);
				//return new BasicArrowButton(orientation, bg, bg, Color.RED, bg);
			}
		};
		scroll.getVerticalScrollBar().setUI(scrollui);
		
        frame.add(scroll);
        
        textArea = tp;
        
        frame.pack();
        
        return tp;
	}


	private static class GuiStream extends OutputStream {
		
	    private final JTextArea textArea;
	    private final StringBuilder builder;
	    private Pattern pattern;
	    
	    public GuiStream() {
	    	textArea = createTextArea();
	    	builder = new StringBuilder();
	    	setPattern("");
	    }
	     
	    void setPattern(String regex) {
	    	System.out.println(regex);
	    	try {
	    		pattern = Pattern.compile(regex);
	    	} catch(PatternSyntaxException e) {
	    		System.err.println(e.getDescription());
	    	}
	    }
	    
	    @Override
	    public void write(int b) throws IOException {
	    	
	    	builder.append((char) b);
	    	
	    	if(b == '\n' || b == '\r') {
	    		String s = builder.toString();
		    	if(pattern.matcher(s).find() && textArea != null) {
		    		System.out.println(s);
		    		System.out.println("Endchar: " + b);
			        builder.replace(builder.length() - 1, builder.length(), System.lineSeparator());
		    		textArea.append(builder.toString());
			        
			        textArea.setCaretPosition(textArea.getDocument().getLength());
		    	}
		    	builder.setLength(0);
	    	}
	    }
	}
	
	
	public static void disableMouseForComponent(Component... components) {
	    for (Component c : components) {
	        if (c instanceof Container con) {
	            disableMouseForComponent(con.getComponents());
	        }
	        for (MouseListener l : c.getMouseListeners()) {
	            c.removeMouseListener(l);
	        }
	        for (MouseMotionListener l : c.getMouseMotionListeners()) {
	            c.removeMouseMotionListener(l);
	        }
	    }
	}
	
}
