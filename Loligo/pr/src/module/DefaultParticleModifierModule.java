package module;

import gui.particles.ModParam;
import gui.particles.PositionableParticleModifier;
import lombok.AllArgsConstructor;
import module.inout.NumberInputIF;

public class DefaultParticleModifierModule extends AbstractParticleModifierModule {

	@AllArgsConstructor
	private class Ins {

		final NumberInputIF input;
		final ModParam param;
	}
	
	private final Ins[] inputs;
	
	public DefaultParticleModifierModule(PositionableParticleModifier modifier) {
		super(modifier);
		
		ModParam[] params = modifier.getParams();
		inputs = new Ins[params.length];
		
		for (int i = 0; i < inputs.length; i++) {
			
			ModParam mp = params[i];
			NumberInputIF in = addInput(mp.getName(), mp.getNumType());
			
			inputs[i] = new Ins(in, mp);
		}
	}
	
	@Override
	public void defaultSettings() {
	
		super.defaultSettings();
		
		for (int i = 0; i < inputs.length; i++) {
			
			Ins in = inputs[i];
			
			double d = in.param.getDefaultValue();
			in.input.setValue(d);
			in.param.setValue(d);
		}
	}
	
	@Override
	public void processIO() {
		
		for (int i = 0; i < inputs.length; i++) {
			
			Ins in = inputs[i];
			
			if(in.input.changed()) {
				
				double d = in.input.getWildInput();
				in.param.setValue(d);
			}
		}
	}

}
