package module;

import gui.Position;
import gui.particles.ParticleSystem;
import gui.particles.ParticleSystemRegistry;
import gui.particles.PositionableParticleModifier;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import patch.DomainType;
import pr.ModuleGUI;

public abstract class AbstractParticleModifierModule extends AbstractPositionableModule {

	private ParticleSystem pSys = ParticleSystemRegistry.defaultParticleSystem();
	
	@Getter@Setter
	protected PositionableParticleModifier modifier;
	
	
	public AbstractParticleModifierModule() {
		
	}
	
	public AbstractParticleModifierModule(PositionableParticleModifier modifier) {
		
		this.modifier = modifier;
	}
	
	@Override
	public ModuleGUI createGUI() {
		var g = super.createGUI();
		g.addChild(modifier.createGui());
		return g;
	}
	
	@Override
	public void postInit() {
		super.postInit();
		
		Position pos = new Position.TransformPosition(this, getDomain().getDisplayManager().getTransform().getMultipliedMatrix());
		modifier.setPosObject(pos);
	}
	
	
	@Override
	public void onAdd() {
	
		super.onAdd();
		if(getDomain().domainType() != DomainType.PARTICLE_FACTORY)
			pSys.addModifier(modifier);
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
	
		super.onDelete(mode);
		pSys.removeModifier(modifier);
	}
	
	/*
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc);
		Map<String, Object> map = md.createMap();
		
		for (int i = 0; i < inputs.length; i++) {
			
			ModParam p = inputs[i].param;
			map.put(p.getName(), p.getValue());
		}
	
		return md;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
	
		super.doDeserialize(data, rdc);
		
		
	}
	*/
	
	

}
