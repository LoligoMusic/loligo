package module;

import constraints.Constrainable;
import pr.Positionable;

public interface PositionableModule extends Module, Positionable, Constrainable {

}
