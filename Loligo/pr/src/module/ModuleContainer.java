package module;

import java.util.List;

public interface ModuleContainer {
	
	public boolean addModule(Module m);
	
	public boolean removeModule(Module m, ModuleDeleteMode mode);
	
	public List<? extends Module> getContainedModules();
	
}
