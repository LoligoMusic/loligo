package module.dynio;

import java.util.function.DoubleConsumer;

import module.Module;
import module.inout.NumberInput;

public class DynamicNumberInput extends NumberInput {

	private final DoubleConsumer cons;
	
	public DynamicNumberInput(String name, Module module, DoubleConsumer cons) {
		super(name, module);
		
		this.cons = cons;
	}

	@Override
	public double getWildInput() {
		double d = super.getWildInput();
		cons.accept(d);
		return d;
	}
	
	

}
