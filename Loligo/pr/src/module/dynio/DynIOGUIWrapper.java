package module.dynio;

import static constants.SlotEvent.SLOT_STOPTARGET;

import constants.InputEvent;
import constants.SlotEvent;
import constants.SlotEventListener;
import gui.DisplayObjects;
import gui.ImageObject;
import lombok.var;
import module.inout.IOType;
import module.inout.InOutGUI;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.Output;
import pr.DisplayObjectIF;
import pr.EventSubscriber;
import pr.ModuleGUIWrapper;
import pr.userinput.UserInput;
import processing.core.PImage;
import util.Images;

public class DynIOGUIWrapper extends ModuleGUIWrapper implements SlotEventListener, EventSubscriber {
	
	private final static PImage ICON_MOUSEOVER = Images.fillAlphaImage(Images.loadPImage("/resources/plus.png"), 0xffffffff);
	
	final private DynIOModule module;
	final private DisplayObjectIF gui;
	private boolean hasMouseOver;
	private ImageObject img;
	private InOutInterface io_preview;
	private boolean over, overActivated;
	
	
	public DynIOGUIWrapper(DynIOModule m, DisplayObjectIF gui) {
		super(m, gui);
		module = m;
		this.gui = gui;
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		super.mouseEventThis(e, input);
		
		var io = input.getDomain().getDisplayManager().getLineContainer().getLineTarget();//input.getLineTarget();
		
		if(io != null && io.getModule() != module) {
			
			switch(e) {
			case DRAG -> {
				if(over) {
					float w = io.getGUI().getWidth(),
						  x1 = getX() + w,
						  x2 = getX() + getWidth() - w,
						  mx = input.mouseX;
						  
					if(mx > x1 && mx < x2) {
						if(!overActivated) {
							mouseOverDyn(io);
							overActivated = true;
						}
					}else {
						endMouseOver();
					}
				}
			}
			case OVER -> 
				over = true;
			case OVER_STOP -> {
				over = false;
				endMouseOver();
			}
			default -> {}
			}
		}
	}
	
	
	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		
		if(e == SLOT_STOPTARGET) {
			
			var io = in != null ? in : out;
			var inf = module.getDynIOInfo();
			
			if(hasMouseOver && inf.isCompatible(io)) {
				io_preview.getGUI().setColor(InOutGUI.DEFAULT_IO_COLOR);
				io_preview = null;
				endMouseOver();
			}
		}
	}
	
	
	private void mouseOverDyn(InOutInterface io) {
		
		hasMouseOver = true;
		var inf = module.getDynIOInfo();
		
		if(inf.isCompatible(io)) {
			
			io_preview = connect(io);
			io_preview.getGUI().setColor(0xffffffff);
			
			if(img == null) {
				img = new ImageObject(ICON_MOUSEOVER);
			}
			var iog = io_preview.getGUI();
			float xm = 4;
			boolean in = io_preview.getIOType() == IOType.IN;
			float x = iog.getX() + (in ? -img.getWidth() - xm: xm);
			float y = iog.getY() - img.getHeight() / 2 - 1;
			img.setPos(x, y);
			gui.addChild(img);
			
			gui.getDm().getDomain().getDisplayManager().getLineContainer().setShowMouseLine(false);
		}
	}
	
	
	private void endMouseOver() {
		overActivated = false;
		if(img != null)
			DisplayObjects.removeFromDisplay(img);
		hasMouseOver = false;
		
		if(io_preview != null) {
			module.deleteDynIO(io_preview.getName(), io_preview.getIOType());
			io_preview = null;
		}
		gui.getDm().getDomain().getDisplayManager().getLineContainer().setShowMouseLine(true);
	}
	
	
	private InOutInterface connect(InOutInterface io1) {
		var io2 = module.createDynIO(io1.getName(), io1.getType(), io1.getIOType().flip());
		module.getDomain().getModuleManager().connect(io1, io2);
		return io2;
	}

	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		gui.getDm().getDomain().getModuleManager().addSlotEventListener(this);
	}
	
	@Override
	public void removedFromDisplay() {
		super.removedFromDisplay();
		gui.getDm().getDomain().getModuleManager().removeSlotEventListener(this);
		endMouseOver();
	}

}
