package module.dynio;

import static constants.SlotEvent.SLOT_STOPTARGET;

import constants.InputEvent;
import constants.SlotEvent;
import constants.SlotEventListener;
import gui.DisplayObjects;
import gui.ImageObject;
import gui.text.TextBox;
import lombok.var;
import module.inout.InOutGUI;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.Output;
import pr.userinput.UserInput;
import processing.core.PImage;
import util.Images;

public class DynIOModuleGUI extends TextBox implements SlotEventListener{

	private final static PImage ICON_MOUSEOVER = Images.fillAlphaImage(Images.loadPImage("/resources/plus.png"), 0xffffffff);
	
	final private DynIOModule module;
	private boolean hasMouseOver;
	private ImageObject img;
	private InOutInterface io_preview;
	
	
	public DynIOModuleGUI(DynIOModule m) {
		super(m.getName(), true);
		module = m;
	}
	
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		super.mouseEventThis(e, input);
		
		InOutInterface io = input.getDomain().getDisplayManager().getLineContainer().getLineTarget();
		
		if(io != null && io.getModule() != module) {
			
			if(e == InputEvent.OVER) {
				mouseOverDyn(io);
			}else
			if(e == InputEvent.OVER_STOP) {
				endMouseOver();
			}
		}
	}
	
	
	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		
		if(e == SLOT_STOPTARGET) {
			
			InOutInterface io = in != null ? in : out;
			var inf = module.getDynIOInfo();
			
			if(hasMouseOver && inf.isCompatible(io)) {
				//connect(io);
				io_preview.getGUI().setColor(InOutGUI.DEFAULT_IO_COLOR);
				io_preview = null;
				endMouseOver();
			}
		}
	}
	
	
	private void mouseOverDyn(InOutInterface io) {
		
		hasMouseOver = true;
		var inf = module.getDynIOInfo();
		
		if(inf.isCompatible(io)) {
			
			io_preview = connect(io);
			io_preview.getGUI().setColor(0xffffffff);
			
			if(img == null) {
				img = new ImageObject(ICON_MOUSEOVER);
			}
			float x = getX() + (inf.isInput() ? -img.getWidth() - 3: getWidth() + 3);
			float y = io_preview.getGUI().getY() - img.getHeight() / 2 - 1;
			img.setPos(x, y);
			addChild(img);
			
			getDm().getDomain().getDisplayManager().getLineContainer().setShowMouseLine(false);
		}
	}
	
	
	private void endMouseOver() {
		if(img != null)
			DisplayObjects.removeFromDisplay(img);
		hasMouseOver = false;
		
		if(io_preview != null) {
			module.deleteDynIO(io_preview.getName(), null);
			io_preview = null;
		}else {
			
		}
		getDm().getDomain().getDisplayManager().getLineContainer().setShowMouseLine(true);
	}
	
	
	private InOutInterface connect(InOutInterface io1) {
		String n = DynIOModules.makeIOName(io1.getType());
		InOutInterface io2 = module.createDynIO(n, io1.getType(), null);
		module.getDomain().getModuleManager().connect(io1, io2);
		return io2;
	}

	
	@Override
	public void addedToDisplay() {
		super.addedToDisplay();
		getDm().getDomain().getModuleManager().addSlotEventListener(this);
	}
	
	
	@Override
	public void removedFromDisplay() {
		super.removedFromDisplay();
		getDm().getDomain().getModuleManager().removeSlotEventListener(this);
		endMouseOver();
	}
	
}
