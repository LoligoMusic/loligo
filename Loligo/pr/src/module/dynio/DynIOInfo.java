package module.dynio;

import static java.util.EnumSet.*;
import static module.inout.DataType.*;
import static module.inout.IOType.*;
import java.util.EnumSet;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;

@RequiredArgsConstructor
public class DynIOInfo {

	static final public DynIOInfo 
	
		NUMBER_IN 	  		= new DynIOInfo(of(IN), of(Number)),
		NUMBER_OUT 	  		= new DynIOInfo(of(OUT), of(Number)),
		NUMBER_IN_OUT   	= new DynIOInfo(of(IN, OUT), of(Number)),
		
		COLOR_IN 			= new DynIOInfo(of(IN), of(COLOR)),
		COLOR_OUT 			= new DynIOInfo(of(OUT), of(COLOR)),
		
		NUMBER_COLOR_IN		= new DynIOInfo(of(IN), of(Number, COLOR)),
		NUMBER_COLOR_IN_OUT = new DynIOInfo(of(IN, OUT), of(Number, COLOR)),
		
		TRANSFORM_IN 		= new DynIOInfo(of(IN), of(TRANSFORM)),
		AUDIO_IN 			= new DynIOInfo(of(IN), of(AUDIOCTRL)),
		
		UNIVERSAL_IN		= new DynIOInfo(of(IN), of(UNIVERSAL)),
		UNIVERSAL_OUT		= new DynIOInfo(of(OUT), of(UNIVERSAL)),
		UNIVERSAL_IN_OUT	= new DynIOInfo(of(IN, OUT), of(UNIVERSAL)),
		
		ALLTYPES_IN 	  	= new DynIOInfo(of(IN), of(Number, COLOR, TRANSFORM)),
		ALLTYPES_OUT 	  	= new DynIOInfo(of(OUT), of(Number, COLOR, TRANSFORM)),
		ALLTYPES_IN_OUT 	= new DynIOInfo(of(IN, OUT), of(Number, COLOR, TRANSFORM));
	
	
	@Getter
	private final EnumSet<IOType> ioTypes;
	@Getter
	private final EnumSet<DataType> dataTypes;
	
	
	public boolean isCompatible(InOutInterface io) {
		return isCompatible(io.getIOType(), io.getType());
	}
	
	public boolean isCompatible(IOType iot, DataType dt) {
		return ioTypes.contains(IOType.flip(iot)) && dataTypes.contains(dt);
	}
	
	public boolean isInput() {
		return ioTypes.contains(IOType.IN);
	}
	
	public boolean isOutput() {
		return ioTypes.contains(IOType.OUT);
	}
	
	public boolean isInputAndOutput() {
		return isInput() && isOutput();
	}
	
	public boolean hasType(DataType t) {
		return dataTypes.contains(t);
	}
}
