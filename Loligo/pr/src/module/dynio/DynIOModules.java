package module.dynio;

import java.util.function.BiPredicate;
import java.util.function.Predicate;
import lombok.var;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import save.IOData;
import save.ModuleData;
import util.Naming;

public final class DynIOModules {
	private DynIOModules() {}

	
	public static String tryRenameIO(InOutInterface io, String name) {
		return tryRenameIO(io, name, DynIOModules::ioNameExists);
	}

	
	public static String stripNumber(String n) {
		
		char c = n.charAt(n.length() - 1);
		if(Character.isDigit(c)) {
			n = n.substring(0, n.length() - 1);
		}
		return n;
	}
	

	public static String tryRenameIO(InOutInterface io, String name, BiPredicate<InOutInterface, String> exists) {
		
		Predicate<String> p = s -> exists.test(io, s);
		return Naming.findFreeName(name, p);
	}

	
	public static boolean ioNameExists(InOutInterface io, String name) {
		
		var m = io.getModule();
		var ios = io.getIOType() == IOType.IN ? m.getInputs() : m.getOutputs();
		return ios.stream().anyMatch(o -> o.getName().equals(name));
	}

	
	public static String makeIOName(DataType iot) {
		String s = iot.toString();
		return s.charAt(0) + s.substring(1).toLowerCase() + '1';
	}

	
	public static void restoreDynOutputs(DynIOModule m, ModuleData md, int offset) {
		var outs = md.outputs;
		for (int i = offset; i < outs.size(); i++) {
			IOData id = outs.get(i);
			m.createDynIO(id.name, id.type, IOType.OUT);
		}
	}

	
	public static void restoreDynInputs(DynIOModule m, ModuleData md, int offset) {
		var ins = md.inputs;
		for (int i = offset; i < ins.size(); i++) {
			IOData id = ins.get(i);
			m.createDynIO(id.name, id.type, IOType.IN);
		}
	}
	
}
