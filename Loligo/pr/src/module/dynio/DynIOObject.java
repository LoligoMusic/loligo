package module.dynio;

import static module.inout.IOType.IN;

import lombok.var;
import module.Modules;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import save.IOData;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;


public class DynIOObject<T extends InOutInterface> {

	private final DynIOModule module;
	private final DynIOInfo info;
	private final int optionalOffsetIn, optionalOffsetOut;
	
	public DynIOObject(DynIOModule module, DynIOInfo info) {

		this.module = module;
		this.info = info;
		
		var ins = module.getInputs();
		optionalOffsetIn = ins == null ? 0 : ins.size();
		var outs = module.getOutputs();
		optionalOffsetOut = outs == null ? 0 : outs.size();
	}
	
	
	public void doSerialize(ModuleData data, SaveDataContext sdc) {

	}
	
	
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		var ins = data.inputs;
		for (int i = optionalOffsetIn; i < ins.size(); i++) {
			IOData id = ins.get(i);
			createDynIO(id.name, id.type, IOType.IN);
		}
	}
	

	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		return ioType == IN ? Modules.addDynamicInput (module, name, type):
							  Modules.addDynamicOutput(module, name, type);
	}

	
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		// TODO Auto-generated method stub
		return null;
	}
	

	public DynIOInfo getDynIOInfo() {
		return info;
	}

}
