package module.dynio;

import gui.nodeinspector.NodeInspector;
import history.DynamicAction;
import lombok.var;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;


public final class DynIOAction extends DynamicAction {
	
	private DynIOAction(Runnable exe, Runnable undo) {
		super(exe, undo, "DynIO");
	}
	
	public static DynIOAction getCreate(DynIOModule mod, String name, DataType dataType, IOType ioType) {
		
		return new DynIOAction(
			() -> create(mod, null, name, dataType, ioType), 
			() -> remove(mod, name, ioType)
		);
	}
	
	public static DynIOAction getRemove(DynIOModule mod, InOutInterface io) {
		
		final String name = io.getName();
		final DataType dt = io.getType();
		final IOType iot = io.getIOType();
		final Object val = io.getValueObject();
		
		return new DynIOAction(
			() -> remove(mod, name, iot), 
			() -> create(mod, val, name, dt, iot)
		);
	}
	
	private static void create(DynIOModule mod, Object val, String name, DataType dataType, IOType ioType) {
		var io = mod.createDynIO(name, dataType, ioType);
		mod.rename(io, name);
		if(val != null)
			io.setValueObject(val);
		NodeInspector.updateFor(mod);
	}
	
	private static void remove(DynIOModule mod, String name, IOType ioType) {
		mod.deleteDynIO(name, ioType);
		NodeInspector.updateFor(mod);
	}
	
	public static DynamicAction getRename(InOutInterface io, String newName) {
		final String oldName = io.getName();
		return new DynamicAction(() -> rename(io, newName), () -> rename(io, oldName));
	}
	
	public static void rename(InOutInterface io, String name) {
		var m = (DynIOModule) io.getModule();
		m.rename(io, name);
		NodeInspector.updateFor(m);
	}
	
	
	@Override
	public String toString() {
		return "DynIOAction";
	}
}
