package module.dynio;

import module.Module;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;

public interface DynIOModule extends Module {
	
	public InOutInterface createDynIO(String name, DataType type, IOType ioType);
	
	public InOutInterface deleteDynIO(String name, IOType ioType);
	
	public default String rename(InOutInterface io, String name) {
		String n = DynIOModules.tryRenameIO(io, name);
		io.setName(n);
		return n;
	}
	
	public DynIOInfo getDynIOInfo();
}
