package module.signalFlow;

import java.util.Arrays;
import java.util.List;

import module.Module;
import module.Readable;
import module.modules.reader.Reader;

public class ImmutableSignalFlow implements SignalFlowIF {

	private Module[] modules;
	private Readable[] readers;
	private int pos = 0;
	private Runnable onProcessComplete = () -> {};
	
	public ImmutableSignalFlow() {
		
	}
	
	public ImmutableSignalFlow(Module[] modules, Reader[] readers) {
		
		this.modules = modules;
		this.readers = readers;
	}

	public void setModules(Module[] modules) {
		
		this.modules = modules;
	}
	
	public void setReaders(Readable[] readers) {
		this.readers = readers;
	}

	public Module[] getModules() {
		return modules;
	}
	
	@Override
	public void tryBuild() {
		
	}

	@Override
	public boolean processNext() {
		
		if(pos < modules.length) {
			
			modules[pos].processIO();
			pos++;
			return true;
		}else{
			pos = 0;
			onProcessComplete.run();
			return false;
		}
	}

	@Override
	public void process() {
		
		for (Module module : modules) 
			
			module.processIO();
		
		onProcessComplete.run();
	}

	@Override
	public void processReaders() {
		
		for (Readable r : readers) {
			
			r.read();
		}
	}

	@Override
	public void suspendBuild() {
		
	}

	@Override
	public void resumeBuild() {
		
	}

	@Override
	public void addModule(Module m) {
		
	}

	@Override
	public void removeModule(Module m) {
		
	}

	@Override
	public void build() {
		
	}

	@Override
	public boolean contains(Module m) {
		
		for (int i = 0; i < modules.length; i++) 
			if(modules[i] == m)
				return true;
		
		for (int i = 0; i < readers.length; i++) 
			if(readers[i] == m)
				return true;
		
		return false;
	}

	@Override
	public List<? extends Module> getFlowList() {
		
		return Arrays.asList(modules);
	}

	@Override
	public void onProcessComplete(Runnable r) {
		onProcessComplete = r;
	}

	
}
