package module.signalFlow;

import java.util.Stack;

public class SignalFlowManager {

	private Stack<SignalFlowIF> stack = new Stack<>();
	private SignalFlowIF main;
	private SignalFlowIF block;
	
	
	public SignalFlowManager() {
	
		
	}
	
	public final void setMain(SignalFlowIF main) {
		
		this.main = main;
		stack.clear();
		insertBlock(main);
	}
	
	public final void insertBlock(SignalFlowIF f) {
		
		if(f != null) {
			stack.push(f);
			block = f;
		}
	}
	
	
	public void processPatch() {
		
		block = stack.peek();
		
		while(!stack.isEmpty()) {
			
			if(!block.processNext()) {
				
				if(block == main) {
					break;
				}else{
					
					stack.pop();
					
					if(stack.isEmpty()) 
						break;
					else 
						block = stack.peek();
				}
			}
		}
	
	}
	
	public void processReaders() {
		
	}
}
