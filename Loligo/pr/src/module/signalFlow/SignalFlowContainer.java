package module.signalFlow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import constants.DisplayFlag;
import constants.Flow;
import constants.SlotEvent;
import constraints.FollowerObject;
import gui.Position;
import gui.nodeinspector.NodeInspector;
import module.Anim;
import module.Module;
import module.ModuleDeleteMode;
import module.Readable;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.Output;
import patch.Domain;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import pr.RootClass;
import save.ConstrainableData;
import save.ModuleData;
import save.PathData;
import save.RestoreDataContext;
import save.SaveDataContext;


public class SignalFlowContainer implements Anim, SignalFlowIF, Readable {

	private final List<Domain> blocks = new ArrayList<>();
	
	private EnumSet<Flow> flowFlags = EnumSet.of(Flow.PERMA, Flow.ISOLATED, Flow.READER);
	private String name;
	private Domain domain;
	//private SubDomain subDomain;
	private int pos;
	private DisplayObjectIF animContainer;
	private Runnable onProcessComplete = () -> {};

	public void addBlock(Domain sd) {
		
		blocks.add(sd);
		
		animContainer.addChild((DisplayObjectIF) sd.getDisplayManager());
	}
	
	public void removeBlock(Domain sd) {
		
		blocks.remove(sd);
		
		animContainer.removeChild((DisplayObjectIF) sd.getDisplayManager());
	}
	

	@Override
	public ModuleGUI createGUI() {
		
		return null;
	}

	@Override
	public void postInit() {
		
		animContainer = new DisplayObject();
		animContainer.setFlag(DisplayFlag.CLICKABLE, false);
		animContainer.setFlag(DisplayFlag.DRAWABLE, false);
	}
	
	@Override
	public void onProcessComplete(Runnable r) {
		onProcessComplete = r;
	}

	@Override
	public void onAdd() {
		
	}

	@Override
	public void onDelete(ModuleDeleteMode mode) {
		
	}

	@Override
	public ModuleGUI gui() {
		
		return null;
	}

	@Override
	public void positionSlots() {
		
	}

	@Override
	public List<Input> getInputs() {
		
		return Collections.emptyList();
	}

	@Override
	public List<Output> getOutputs() {
		
		return Collections.emptyList();
	}

	@Override
	public boolean checkFlag(Flow f) {
		
		return flowFlags.contains(f);
	}

	@Override
	public Domain getDomain() {
		
		return domain;
	}

	@Override
	public void setDomain(Domain domain) {
		
		this.domain = domain;
	}

	@Override
	public String getName() {
		
		return name;
	}

	@Override
	public void setName(String name) {
		
		this.name = name;
	}

	@Override
	public DisplayObjectIF getGui() {
		
		return null;
	}

	
	@Override
	public void processIO() {
		
		RootClass.signalFlowManager().insertBlock(this);
	}

	@Override
	public List<Input> getConnectedInputs() {
		return Collections.emptyList();
	}

	@Override
	public boolean isSubModule() {
		return false;
	}

	@Override
	public Module createInstance() {
		
		return null;
	}

	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tryBuild() {
		
	}

	@Override
	public boolean processNext() {
		
		if(pos < blocks.size()) {
			
			if(blocks.get(pos).getSignalFlow().processNext()) {
				
				return true;
			}else{
				
				pos++;
				return true;
			}
			
		}
		onProcessComplete.run();
		pos = 0;
		return false;
	}

	@Override
	public void process() {
		
		
	}

	@Override
	public void read() {
		
		processReaders();
	}
	
	@Override
	public void processReaders() {
		
		for (Domain b : blocks) {
			
			b.getSignalFlow().processReaders();
		}
	}

	@Override
	public void suspendBuild() {
		
	}

	@Override
	public void resumeBuild() {
		
	}

	@Override
	public void addModule(Module m) {
		
	}

	@Override
	public void removeModule(Module m) {
		
	}

	public void removeAllModules() {
		
	}
	
	@Override
	public void build() {
		
	}

	@Override
	public boolean contains(Module m) {
		
		return false;
	}

	
	
	//Anim
	
	@Override
	public float getX() {
		
		return 0;
	}

	@Override
	public float getY() {
		
		return 0;
	}

	@Override
	public void setPos(float x, float y) {
		
	}

	@Override
	public Position getPosObject() {
		
		return new Position();
	}

	@Override
	public void setPosObject(Position p) {
		
	}

	@Override
	public DisplayObjectIF createAnim() {
		
		return animContainer;
	}

	@Override
	public DisplayObjectIF getAnim() {
		
		return animContainer;
	}

	@Override
	public void setAnim(DisplayObjectIF anim) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FollowerObject getFollowerObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFollowerObject(FollowerObject followerObject) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePosition() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int pipette(int x, int y) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void addIO(Output out) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addIO(Input in) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<? extends Module> getFlowList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void defaultSettings() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ConstrainableData toConstrainableMemento(SaveDataContext sdc, PathData pd) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void restoreMemento(ConstrainableData cd) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public NodeInspector createGUISpecial() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeIO(Input io) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeIO(Output out) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ModuleGUI getModuleGUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String getLabel() {
		return null;
	}
	
	@Override
	public void setLabel(String label) {
		
	}

	@Override
	public int getModuleID() {
		return -1;
	}

	@Override
	public void setModuleID(int id) {
		
	}

	@Override
	public void resetModule() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onIOEventThis(SlotEvent e, InOutInterface io) {
		// TODO Auto-generated method stub
		
	}
}
