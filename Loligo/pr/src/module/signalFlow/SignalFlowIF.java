package module.signalFlow;

import java.util.List;

import module.Module;

public interface SignalFlowIF {

	void tryBuild();

	boolean processNext();

	void process();

	void processReaders();

	void suspendBuild();

	void resumeBuild();

	void addModule(Module m);

	void removeModule(Module m);

	void build();

	boolean contains(Module m);

	List<? extends Module> getFlowList();
	
	void onProcessComplete(Runnable r);

}