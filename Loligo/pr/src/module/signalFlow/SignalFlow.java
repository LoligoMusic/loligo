package module.signalFlow;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import constants.Flow;
import module.Module;
import module.Readable;
import module.inout.Input;
import module.inout.Output;


public class SignalFlow implements SignalFlowIF {
	
	final private List<FlowMod> modules = new ArrayList<>();
	final private ArrayList<Module> flowList = new ArrayList<>();
	final private ArrayList<Readable> readables = new ArrayList<>();
	
	private Runnable onProcessComplete = () -> {};
	private int suspendBuild = 0, pos = 0;
	
	
	@Override
	public void tryBuild() {
		
		if(suspendBuild == 0)
		
			build();
	}
	
	@Override
	public boolean processNext() {
		
		if(pos < flowList.size()) {
			
			flowList.get(pos).processIO();
			pos++;
			return true;
		}else{
			pos = 0;
			onProcessComplete.run();
			return false;
		}
	}
	
	@Override
	public void process() {
		
		for (Module m : flowList) 
			m.processIO();
		
		onProcessComplete.run();
	}
	
	@Override
	public void processReaders() {
		
		for (int i = 0; i < readables.size(); i++)
			readables.get(i).read();
	}
	
	@Override
	public void suspendBuild() {
		
		suspendBuild++;
	}
	
	@Override
	public void resumeBuild() {
		
		suspendBuild--;
		
		if(suspendBuild <= 0) {
			suspendBuild = 0;
			build();
		}
	}

	@Override
	public void addModule(Module m) {
		
		if(numTrav > 0) {
			
			throw new ConcurrentModificationException();
		}
		
		modules.add(new FlowMod(m));
		
		if(m.checkFlag(Flow.PERMA)) {
			flowList.add(m);
			if(m.checkFlag(Flow.READER))
				readables.add((Readable) m);
		}
	}
	
	@Override
	public void removeModule(Module m) {
		
		if(numTrav > 0) {
			
			throw new ConcurrentModificationException();
		}
		
		for (int i = 0; i < modules.size(); i++) 
			
			if(modules.get(i).module == m) {
				
				modules.remove(i);
				break; 
			}
		 
		flowList.remove(m);
		if(m.checkFlag(Flow.READER))
			readables.remove(m);
	}
	
	
	
	/* 
		L - Empty list that will contain the sorted elements
		S - Set of all nodes with no incoming edges
		while S is non-empty do
    		remove a node n from S
    		add n to tail of L
    		for each node m with an edge e from n to m do
        		remove edge e from the graph
        	if m has no other incoming edges then
            	insert m into S
			if graph has edges then
    			return error (graph has at least one cycle)
			else 
    			return L (a topologically sorted order)
	 */
	
	int numTrav = 0;
	
	@Override
	public void build() {
		
		readables.clear();
		flowList.clear();
		List<FlowMod> stack = new ArrayList<>(modules.size());
		
		numTrav++;
		
		for (FlowMod fm : modules) {
			
			fm.reset();
			
			Module m = fm.module;
			
			if( (m.checkFlag(Flow.SOURCE) && fm.noInputs) || 
				fm.noInputs || 
				(fm.module.checkFlag(Flow.PERMA) && fm.noInputs && fm.noOutputs)) {
				
				stack.add(fm);
			}
			
			if(m.checkFlag(Flow.READER))
				readables.add((Readable) m);
		}
		
		numTrav--;
		
		while(!stack.isEmpty()) {
			
			FlowMod fm = stack.remove(stack.size() - 1);
			
			flowList.add(fm.module);
			
			List<Input> ins = fm.module.getConnectedInputs();
			
			for (Input in : ins) {
				
				FlowMod fm2 = null;
				
				for (FlowMod mo : modules) 
					if(mo.module == in.getModule()) {
						fm2 = mo;
						break;
					}
				
				if(fm2.lastLink(fm.module))
					stack.add(fm2);
			}
		}
		
		//System.out.println(flowList);
	}
	
	
	@Override 
	public List<? extends Module> getFlowList() {
		
		return flowList;
	}
	
	
	private class FlowMod {
		
		Module module;
		List<Module> outs = new ArrayList<>();
		boolean noInputs, noOutputs;
		
		FlowMod(Module module) {
			
			this.module = module;
		}
		
		void reset() {
			
			outs.clear();
			noOutputs = true;
			
			if(module.getOutputs() != null) {
				
				for (Output o : module.getOutputs()) 
					
					if(!o.getPartners().isEmpty()) {
						
						noOutputs = false;
						break;
					}
			}
			
			if(module.getInputs() != null)
				for (Input in : module.getInputs()) {
					
					Output o = in.getPartner();
					if(o != null && !outs.contains(o.getModule())) 
						
						outs.add(o.getModule());
				}
			
			noInputs = outs.isEmpty();
		}
		
		boolean lastLink(Module m) {
			
			boolean b = outs.remove(m) && outs.isEmpty();
			
			return b;
		}
		
	}


	@Override
	public boolean contains(Module m) {
		
		return flowList.contains(m);
	}

	@Override
	public void onProcessComplete(Runnable r) {
		onProcessComplete = r;
	}

	

}
