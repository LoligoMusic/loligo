package module;

import java.util.List;

import constants.Flow;
import constants.SlotEvent;
import gui.Displayable;
import gui.nodeinspector.NodeInspector;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.Output;
import patch.Domain;
import patch.DomainObject;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;



public interface Module extends Displayable, DomainObject {
	
	public ModuleGUI createGUI();
	
	public NodeInspector createGUISpecial();

	/*
	 *  Initializes module-internal states 
	 */
	public void postInit();
	
	void resetModule();
	
	void defaultSettings();

	public void onAdd();

	public void onDelete(ModuleDeleteMode mode);

	public ModuleGUI gui();
	
	public ModuleGUI getModuleGUI();
	
	@Override
	default DisplayObjectIF getGui() {
	
		return getModuleGUI();
	}

	public void addIO(Output out);
	
	public void addIO(Input in);
	
	public void removeIO(Input io);
	
	void removeIO(Output out);
	
	public void positionSlots();

	public List<Input> getInputs();

	public List<Output> getOutputs();

	public boolean checkFlag(Flow f);

	public Domain getDomain();

	public void setDomain(Domain domain);

	public String getName();

	public void setName(String name);

	//public DisplayObjectIF getGui();

	//public void setGui(DisplayObject gui);

	public void delete();

	public void processIO();

	public List<Input> getConnectedInputs();

	public boolean isSubModule();
	
	public Module createInstance();

	public ModuleData doSerialize(final SaveDataContext sdc);

	public void doDeserialize(ModuleData data, RestoreDataContext rdc);

	public String getLabel();

	public void setLabel(String label);
	
	public int getModuleID();
	
	public void setModuleID(int id);
	
	public void onIOEventThis(SlotEvent e, InOutInterface io);
	
	public default boolean checkType(ModuleType t)  {
		return t == ModuleType.TOOL;
	}

}