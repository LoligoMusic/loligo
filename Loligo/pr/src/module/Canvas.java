package module;

import lombok.Getter;
import pr.DisplayObject;
import pr.RootClass;
import processing.core.PConstants;
import processing.core.PGraphics;


public class Canvas extends DisplayObject {
	
	private static Canvas deadCanvas;
	
	
	public static Canvas createDeadCanvas() {
	
		if(deadCanvas == null) {
			
			deadCanvas = new Canvas(false) {
				
				@Override
				public void render(PGraphics g) {}
				
				public void reset(int c) {}
			};
		}
		
		return deadCanvas;
	}
	
	
	public final static int TRANSPARENT = 0x00112233;
	
	public transient final PGraphics graphics;
	@Getter
	private final boolean isActive;
	
	
	private Canvas(boolean inactive) {
		
		graphics = null;
		isActive = false;
	}
	
	public Canvas() {
		
		isActive = true;
		
		graphics = RootClass.mainProc().createGraphics(RootClass.mainProc().width, RootClass.mainProc().height, PConstants.P2D);
		graphics.smooth(8);
		/*
		graphics.beginDraw();
		graphics.strokeCap(PConstants.ROUND);
		graphics.background(TRANSPARENT);
		graphics.endDraw();
		*/
	}
	
	@Override 
	public void render(PGraphics g) {
		
		g.image(graphics, 0, 0);
	}
	
	
	public void reset() {
		
		reset(TRANSPARENT);
	}
	
	public void reset(int color) {
		graphics.beginDraw();
		graphics.background(color);
		graphics.endDraw();
	}

	
	
	
	
}
