package module;


public interface Readable extends PositionableModule{
	public void read();
}
