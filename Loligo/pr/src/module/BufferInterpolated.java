package module;

import net.beadsproject.beads.data.Buffer;

public class BufferInterpolated extends Buffer {

	private static final long serialVersionUID = 1L;
	
	
	public BufferInterpolated(int size) {
		super(size);
	}
	
	
	@Override
	public float getValueFraction(float fraction) {
		
		int i1 = (int)(buf.length * fraction);
		
		float f1 = getValueIndex(i1),
			  f2 = getValueIndex(i1 + 1);
		
		float inc = 1f / buf.length;
		float c = inc * i1;
		
		return f1 + (f2 - f1) * ((fraction - c) * buf.length);
	}
	
}
