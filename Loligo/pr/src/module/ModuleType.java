package module;

import java.util.EnumSet;

public enum ModuleType {

	ANIM,
	POSITIONABLE,
	PATH,
	AUDIO,
	TOOL;
	
	public final static EnumSet<ModuleType> POSITIONABLES = EnumSet.of(ANIM, POSITIONABLE, PATH);
	
	public static boolean isPositionable(Module m) {
		return hasAnyType(m, POSITIONABLES);
	}
	
	
	public static boolean hasAnyType(Module m, EnumSet<ModuleType> ts) {
		
		for (ModuleType t : ts) {
			if(m.checkType(t))
				return true;
		}
		return false;
	}
	
}
