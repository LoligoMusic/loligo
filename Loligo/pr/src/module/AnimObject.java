package module;

import constants.Flow;
import gui.DisplayObjects;
import gui.GUIBox;
import gui.Position;
import lombok.var;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;


abstract public class AnimObject extends AbstractPositionableModule implements Anim {
	
	transient protected GUIBox guiBox;
	private DisplayObjectIF anim;
	
	
	public AnimObject() {
		
		flowFlags.add(Flow.SINK);
		flowFlags.add(Flow.ROUNDSLOTS);
	}
	
	
	@Override
	abstract public DisplayObjectIF createAnim();
	
	/*
	@Override
	public DisplayObjectIF createGUI() {
		
		DragHandle d = new DragHandle(this, new GUIBox(this));
		//d.setPosObject(getPosObject());
		d.setGuiType(GuiType.ANIM);
		
		guiBox = d.guiBox;
		guiBox.setGuiType(GuiType.WINDOW);
		
		return d;
	}
	*/
	
	@Override
	public void postInit() {
		
		super.postInit();
		anim = createAnim();
		anim.setPosObject(new Position.PositionTarget(this));
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		if(anim != null)
			DisplayObjects.removeFromDisplay(anim);
		if(guiBox != null)
			DisplayObjects.removeFromDisplay(guiBox);
	}
	

	@Override
	public DisplayObjectIF getAnim() {
		return anim;
	}


	@Override
	public void setAnim(DisplayObjectIF anim) {
		this.anim = anim;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
	
		super.doDeserialize(data, rdc);
		rdc.processAnim(this);
	}
	
	public int pipette(int x, int y) {
		return 0;
	}
	
	@Override
	public boolean checkType(ModuleType t) {
		return t == ModuleType.ANIM;
	}

	
	public static class Empty extends AnimObject {

		public Empty() {
			var d = new DisplayObject() {
				@Override public void render(PGraphics g) {}
			};
			setAnim(d);
		}
		
		@Override
		public DisplayObjectIF createAnim() {
			return getAnim();
		}

		@Override
		public void processIO() { }
		
	}
}

