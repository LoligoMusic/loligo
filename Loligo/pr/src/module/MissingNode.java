package module;

import audio.AudioModule;
import module.inout.Input;
import module.inout.Output;
import net.beadsproject.beads.ugens.Plug;
import pr.ModuleGUI;
import save.IOData;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

public class MissingNode extends AbstractModule {

	public static final int MISSING_COLOR = 0xffaa0000;
	
	
	private ModuleData moduleData;
	
	
	public MissingNode(String name) {
		
		setName(name);
	}
	
	@Override
	public ModuleGUI createGUI() {
		ModuleGUI g = super.createGUI();
		g.setColor(MISSING_COLOR);
		return g;
	}
	
	@Override
	public void processIO() {
		
	}

	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc);
		
		moduleData.inputs = md.inputs;
		moduleData.outputs = md.outputs;
		moduleData.setXy(md.getXy());
		
		return moduleData;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {

		super.doDeserialize(data, rdc);

		moduleData = data;
		
		if(moduleData.inputs != null) {
			for (IOData io : moduleData.inputs) {
				
				Input s = Modules.addDynamicInput(this, io.name, io.type);
				addIO(s);
			}
		}
		if(moduleData.outputs != null) {
			for (IOData io : moduleData.outputs) {
				
				Output s = Modules.addDynamicOutput(this, io.name, io.type);
				addIO(s);
			}
		}
	}

	

	public static class Audio extends AudioModule{
		
		public Audio(String name) {
			
			setName(name);
		}
		
		
		@Override
		public void beadConstruct() {
			beadIn = beadOut = new Plug(audioContext);
		}
	}
	
}
