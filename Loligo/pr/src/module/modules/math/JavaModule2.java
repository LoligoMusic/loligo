package module.modules.math;

import gui.nodeinspector.NodeInspector;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import module.pyscript.ScriptModuleDelegate;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.TOOLS)
public class JavaModule2 extends AbstractModule{
	
	private final ScriptModuleDelegate scriptModule = new ScriptModuleDelegate(this);
	
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		scriptModule.defaultSettings();
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		return scriptModule.createGUISpecial();
	}
	
	@Override
	public void processIO() {
		scriptModule.processIO();
	}
	
	@Override
	public void onAdd() {
		scriptModule.onAdd();
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		scriptModule.onDelete(mode);
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return scriptModule.doSerialize(sdc);
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		scriptModule.doDeserialize(data, rdc);
	}
	
}
