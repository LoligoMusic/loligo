package module.modules.math;

import constants.Flow;
import module.AbstractModule;
import module.inout.TransformInput;
import module.inout.TransformOutput;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import processing.core.PMatrix2D;

@NodeInfo(type = NodeCategory.TRANSFORM)

public class Multiply extends AbstractModule {

	private final TransformInput in_t1, in_t2;
	private final TransformOutput out_trans;
	
	
	public Multiply() {
		
		flowFlags.add(Flow.SOURCE);
		
		in_t1 = addTransformInput();
		in_t1.setName("Transform1");
		in_t2 = addTransformInput();
		in_t2.setName("Transform2");
		
		out_trans = addTransformOutput();
	}
	
	
	@Override
	public void processIO() {
		
		if(in_t1.changed() || in_t2.changed()) {
		
			PMatrix2D m = out_trans.getMatrix();
			
			m.set(in_t1.getMatrixInput());
			m.apply(in_t2.getMatrixInput());
		}
	}

}
