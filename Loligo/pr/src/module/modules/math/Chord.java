package module.modules.math;

import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import util.Notes;
import util.Notes.Scale;


@NodeInfo(type = NodeCategory.TOOLS)

public class Chord extends AbstractModule {

	private final NumberInputIF in_base;
	
	private final NumberOutputIF out1, out2, out3;
	private Scale scale = Scale.MAJOR;
	
	public Chord() {

		in_base = addInput("Note", NumericAttribute.NOTE).setDefaultValue(Notes.A4);
		out1 = addOutput("Note1", NumericAttribute.NOTE);
		out2 = addOutput("Note2", NumericAttribute.NOTE);
		out3 = addOutput("Note3", NumericAttribute.NOTE);
	}
	
	
	@Override
	public void processIO() {
		
		if(in_base.changed()) {
			
			double n = in_base.getWildInput();
			
			out1.setValue(n);
			out2.setValue(n + scale.degree(2));
			out3.setValue(n + scale.degree(4));
		}
		 
		
	}

}
