package module.modules.math;

import module.AbstractModule;
import module.inout.ColorInputIF;
import module.inout.ColorOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import util.Colors;

@NodeInfo(type=NodeCategory.COLOR)
public class Tint extends AbstractModule {

	private final ColorInputIF in1, in2;
	private final ColorOutputIF out;
	
	public Tint() {
		
		in1 = addColorInput();
		in2 = addColorInput();
		in2.setName("Tint");
		
		out = addColorOutput();
	}


	@Override
	public void processIO() {
		
		int c = Colors.multiply(in1.getColorInput(), in2.getColorInput());
		out.setColor(c);
	}

}
