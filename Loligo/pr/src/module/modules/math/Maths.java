package module.modules.math;

import java.util.function.DoubleBinaryOperator;
import module.AbstractModule;
import module.Module;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.ArithmeticEntry;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.MATH, helpPatch = "Arithmetic", ignoreBuild = true)
public class Maths extends AbstractModule {
	
	private final DoubleBinaryOperator foo;
	
	private final ArithmeticEntry calcMode;
	
	private final NumberInputIF in1, in2;
	private final NumberOutputIF out;
	
	
	public Maths(ArithmeticEntry calcMode) {
		
		in1 = addInput("In1",  calcMode.getNumberAttributeIn());
		
	
		in2 = calcMode.getNumOperands() > 1 ? addInput("In2",  calcMode.getNumberAttributeIn()) : null;
		
		out = addOutput("Out", calcMode.getNumberAttributeOut());
		
		this.calcMode = calcMode;
		foo = calcMode.getFunction();
		
		setName(calcMode.getName());
	}
	

	@Override
	public void processIO() {
		
		double v1 = in1.getWildInput(),
			   v2 = in2 == null ? 0 : in2.getWildInput();
		
		out.setValue(
				foo.applyAsDouble(v1, v2)
			);
	}
	

	@Override
	public Module createInstance() {
		
		return new Maths(calcMode);
	}
	
}
