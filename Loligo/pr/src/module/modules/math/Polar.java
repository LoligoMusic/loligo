package module.modules.math;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type = NodeCategory.MATH)
public class Polar extends AbstractModule {

	private final NumberInputIF in_length, in_angle;
	private final NumberOutputIF out_x, out_y;
	
	public Polar() {
		
		in_length = addInput("Length", DEZIMAL_FREE);
		in_angle = addInput("Angle", DEZIMAL_FREE);
		out_x = addOutput("X");
		out_y = addOutput("Y");
	}
	
	@Override
	public void processIO() {

		double len = in_length.getWildInput(),
			   w = in_angle.getWildInput();
		
		double x = Math.sin(w * Math.PI * 2) * len,
			   y = Math.cos(w * Math.PI * 2) * len;
		
		out_x.setValue(x);
		out_y.setValue(y);
	}

}
