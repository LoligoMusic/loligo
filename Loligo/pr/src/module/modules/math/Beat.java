package module.modules.math;

import static java.lang.Math.max;
import static java.lang.Math.min;

import constants.Flow;
import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.SwitchInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.TOOLS)
public class Beat extends AbstractModule {
	
	private final SwitchInputIF in_switch;
	private final NumberInputIF in_millis;
	private final NumberOutputIF out_beat, out_progress;
	
	private long startTime, nextBeat, lastBeat;
	private int interval, beatCount = 0;
	
	private long fri = (int) ((1000f / 60f) / 2f);
	
	public Beat() {

		flowFlags.add(Flow.SOURCE);
		
		in_switch = addSwitch();
		in_millis = addInput("Interval", NumericAttribute.INTEGER_POSITIVE).setDefaultValue(1000);
		out_beat = addOutput("Beat", NumericAttribute.BOOL);
		out_progress = addOutput("Progress", NumericAttribute.DEZIMAL_POSITIVE);
	}
	
	
	@Override
	public void processIO() {
		
		if(!in_switch.getSwitchState())
			return;
		
		long t = System.currentTimeMillis();
		
		if(in_switch.switchedOn()) {
			reset(t);
		}
		
		if(in_millis.changed()) {
			interval = (int) in_millis.getWildInput();
			reset(t);
		}
		
		long d = t - startTime;
		
		out_progress.setValue((double) d / interval);
		
		long dif = d - lastBeat;
		long idif = dif - interval;
		
		long m = max(-fri, min(fri, idif));
		
		if(d >= nextBeat - m) {
			beatCount++;
			lastBeat = nextBeat;
			nextBeat = interval * beatCount;
			out_beat.setValue(1);
		}else {
			out_beat.setValue(0);
		}
		
	}
	
	
	public void reset(long ts) {
		startTime = ts;
		beatCount = 0;
		nextBeat = interval * beatCount;
	}

}
