package module.modules.math;

import module.AbstractModule;
import module.inout.ColorOutputIF;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import util.Colors;

@NodeInfo(type = NodeCategory.COLOR, helpPatch = "ColorConversion")

public class RGB extends AbstractModule {

	private final NumberInputIF in_r, in_g, in_b, in_alpha;
	private final ColorOutputIF out_color;
	private int r = 0, g = 0, b = 0;
	private int a = 1;
	
	public RGB() {
		in_r = addInput("Red");
		in_g = addInput("Greed");
		in_b = addInput("Blue");
		in_alpha = addInput("Alpha");
		out_color = addColorOutput();
	}
	
	@Override
	public void processIO() {
		boolean boo = false;
		if(in_r.changed()) {
			r = (int) (in_r.getInput() * 255d);
			boo = true;
		}
		if(in_g.changed()) {
			g = (int) (in_g.getInput() * 255d);
			boo = true;
		}
		if(in_b.changed()) {
			b = (int) (in_b.getInput() * 255d);
			boo= true;
		}if(in_alpha.changed()) {
			a = (int) (in_alpha.getInput() * 255d);
			boo= true;
		}
		if(boo) 
			out_color.setColor(Colors.rgba(r, g, b, a));
	}

}
