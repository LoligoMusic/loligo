package module.modules.math;

import static module.inout.DataType.Number;
import static module.inout.IOType.IN;

import java.io.PrintStream;

import constants.Flow;
import lombok.Setter;
import lombok.var;
import module.AbstractModule;
import module.Console;
import module.Modules;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.dynio.DynIOModules;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.NumericAttribute;
import module.inout.SwitchInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import save.ModuleData;
import save.RestoreDataContext;

@NodeInfo(type=NodeCategory.TOOLS)

public class Print extends AbstractModule implements DynIOModule{

	private final SwitchInputIF in_active;
	@Setter
	private PrintStream printStream;
	
	
	public Print() {
		flowFlags.add(Flow.DYNAMIC_IO);
		in_active = addSwitch();
		
		printStream = Console.getStream();
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		
		createDynIO(DynIOModules.makeIOName(Number), Number, IN);
	}
	
	
	@Override
	public void processIO() {
		
		if(in_active.getSwitchState()) {
			
			boolean ch = false;
			
			for(Input in : getInputs()) {
				if(in.isOptional() && in.changed()) {
					in.updateValue();
					printInput(in);
					ch = true;
				}
			}
			
			if(ch)
				printStream.println("-");
			
		}
	}
	
	void printInput(Input in) {
		var la = getLabel();
		if(!la.isEmpty()) {
			printStream.print(la);
			printStream.print('/');
		}
		printStream.print(in.getName());
		printStream.print(": ");
		printStream.println(in.getValueObject());
	}

	
	@Override
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		var in = addInput("", NumericAttribute.DEZIMAL_FREE);
		rename(in, name);
		in.setOptional();
		Modules.initIOGUI(this, in);
		return in;
	}
	

	@Override
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		var in = Modules.getInputByName(this, name);
		if(in == null)
			return null;
		removeIO(in);
		return in;
	}
	

	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.NUMBER_IN;
	}
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		DynIOModules.restoreDynInputs(this, data, 1);
		super.doDeserialize(data, rdc);
	}
	
}
