package module.modules.math;

import constants.Flow;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.TransformOutput;

//@InternalNode(type = NodeCategory.TRANSFORM, helpPatch = "Transform")
public class Shear extends AbstractModule {

	private final NumberInputIF in_shearX, in_shearY;
	private final TransformOutput out_trans;
	
	
	public Shear() {
		
		flowFlags.add(Flow.SOURCE);
		
		out_trans = addTransformOutput();
		in_shearX = addInput("X", NumericAttribute.DEZIMAL_FREE);
		in_shearY = addInput("Y", NumericAttribute.DEZIMAL_FREE);
	}
	
	
	@Override
	public void processIO() {
		
		if(in_shearX.changed() || in_shearY.changed()) {
		
			float sx = (float) in_shearX.getWildInput(),
				  sy = (float) in_shearY.getWildInput();
		
			var m = out_trans.getMatrix();
			m.reset();
			m.shearX(sx);
			m.shearY(sy);
		}
		
	}
}
