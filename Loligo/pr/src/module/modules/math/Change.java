package module.modules.math;


import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo (type = NodeCategory.TOOLS)

public class Change extends AbstractModule {
	
	final private NumberOutputIF out;
	final private NumberInputIF in;
	private double temp;
	
	public Change() {
		
		in = addInput("In", NumericAttribute.DEZIMAL_FREE);
		out = addOutput("Out", NumericAttribute.BOOL);
		
	}

	@Override
	public void processIO() {
		
		//if(in.changed()) {
			
			double v = in.getWildInput();
			out.setValue(v != temp ? 1d : 0d);
			temp = v;
			
		//}
			

	}

}
