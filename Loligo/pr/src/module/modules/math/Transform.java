package module.modules.math;

import static module.modules.math.Translate.NORM_LENGTH;

import constants.Flow;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.TransformOutput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import processing.core.PConstants;

@NodeInfo(type = NodeCategory.TRANSFORM, helpPatch = "Transform")

public class Transform extends AbstractModule {
	
	private final NumberInputIF in_x, in_y, in_scaleX, in_scaleY, in_rotate;
	private final TransformOutput out_trans;
	
	
	public Transform() {
		
		flowFlags.add(Flow.SOURCE);
		
		out_trans = addTransformOutput();
		in_x = addInput("X", NumericAttribute.DEZIMAL_FREE);
		in_y = addInput("Y", NumericAttribute.DEZIMAL_FREE);
		in_scaleX = addInput("Scale X", NumericAttribute.DEZIMAL_FREE).setDefaultValue(1);
		in_scaleY = addInput("Scale Y", NumericAttribute.DEZIMAL_FREE).setDefaultValue(1);
		in_rotate = addInput("Rotate", NumericAttribute.DEZIMAL_FREE);
	}
	
	
	@Override
	public void processIO() {
		
		if(in_x.changed() || in_y.changed() || in_scaleX.changed() || in_scaleY.changed() || in_rotate.changed()) {
		
			float x  = (float) (in_x.getWildInput() * NORM_LENGTH),
				  y  = (float) (in_y.getWildInput() * NORM_LENGTH),
				  sx = (float) in_scaleX.getWildInput(),
				  sy = (float) in_scaleY.getWildInput(),
				  r  = (float) in_rotate.getWildInput() * PConstants.TAU;
		
			var m = out_trans.getMatrix();
			m.reset();
			m.rotate(r);
			m.translate(x, y);
			m.scale(sx, sy);
		}
		
	}
}
