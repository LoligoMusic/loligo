package module.modules.math;

import gui.nodeinspector.NodeInspector;
import module.AbstractModule;
import module.inout.ColorOutputIF;
import module.inout.NumberInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.DisplayObjectIF;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;


@NodeInfo(type = NodeCategory.COLOR)

public class ColorRamp extends AbstractModule{

	private final gui.ColorRamp ramp;
	private final NumberInputIF in_fac;
	private final ColorOutputIF out_color;
	
	public ColorRamp() {
		in_fac = addInput("In");
		out_color = addColorOutput();
		
		ramp = new gui.ColorRamp();
	}
	
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		ramp.reset();
	}
	
	@Override
	public void resetModule() {
		super.resetModule();
		ramp.reset();
	}
	
	@Override
	public NodeInspector createGUISpecial() {
	
		var n = NodeInspector.newInstance(this, ni -> {
		
			ni.createSection("Colorramp");
			DisplayObjectIF d = ramp.createGUI();
			d.setWH(40, 20);
			ni.addInspectorElement(d);
		});
		return n;
	}
	
	@Override
	public void processIO() {
		if(in_fac.changed() || ramp.changed())
			out_color.setColor((ramp.getColorAt(in_fac.getInput())));
	}
	
	
	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		var e = ramp.toMap();
		return super.doSerialize(sdc).addExtras(e.getKey(), e.getValue());
	}
	

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		var p = (double[]) data.nextObject();
		var c = (int[]	 ) data.nextObject();
		ramp.fromMap(p, c);
	}
}
