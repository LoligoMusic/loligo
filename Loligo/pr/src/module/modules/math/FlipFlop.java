package module.modules.math;

import static module.inout.NumericAttribute.BANG;
import static module.inout.NumericAttribute.BOOL;
import static module.inout.NumericType.toBool;

import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type = NodeCategory.TOOLS, helpPatch = "BinaryState")

public class FlipFlop extends AbstractModule {
	
	private final NumberInputIF in_set, in_reset;
	private final NumberOutputIF out;
	
	public FlipFlop() {
		
		in_set = addInput("Set", BANG);
		in_reset = addInput("Reset", BANG);
		
		out = addOutput("Out", BOOL);
	}
	

	@Override
	public void processIO() {
		
		if(toBool(in_set.getWildInput())) {
			
			out.setValue(1);
		}else
		if(toBool(in_reset.getWildInput())) {
			
			out.setValue(0);
		}
		
	}

}
