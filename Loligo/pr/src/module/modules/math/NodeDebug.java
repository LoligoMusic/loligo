package module.modules.math;

import java.io.PrintStream;

import module.AbstractModule;
import module.ModuleDeleteMode;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.ModuleGUI;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type=NodeCategory.TOOLS)

public class NodeDebug extends AbstractModule {

	private PrintStream print = System.out;
	
	public NodeDebug() {
		
		print("Constructor()");
	}
	
	private void print(String s) {
		print.println(s);
	}
	
	@Override
	public void processIO() {
		print("processIO()");
	}
	
	@Override
	public void onAdd() {
		super.onAdd();
		print("onAdd()");
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		print("onDelete()");
		super.onDelete(mode);
	}
	
	@Override
	public void defaultSettings() {
		print("defaultSettings");
		super.defaultSettings();
		
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		print("doSerialize()");
		return super.doSerialize(sdc);
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		print("doDeserialize()");
		super.doDeserialize(data, rdc);
	}
	
	@Override
	public ModuleGUI createGUI() {
		print("createGUI()");
		return super.createGUI();
	}

	@Override
	public void postInit() {
		print("postInit()");
		super.postInit();
	}
	
	
}
