package module.modules.math;

import module.AbstractModule;
import module.inout.NumberOutputIF;
import module.inout.TriggerInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.MATH)

public final class Random extends AbstractModule {
	
	private final TriggerInputIF in_trigger;
	private final NumberOutputIF out;
	
	
	public Random() {
		
		in_trigger = addTrigger();
		out = addOutput("Out");
		out.setValue(Math.random());
		
	}
	
	@Override
	public void processIO() {
		
		if(in_trigger.triggered()) 
			out.setValue(Math.random());
	}
	
}
