package module.modules.math;

import lombok.Getter;
import lombok.Setter;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.MATH)

public class Range extends AbstractModule {
	
	final private NumberInputIF in;
	final private NumberOutputIF out;
	
	@Getter@Setter
	private double max = 1, min = 0;
	
	public Range() {
		in = addInput("In");
		out = addOutput("Out");
	}
	
	/*
	@Override
	public DisplayObject createGUI() {
		GUIBox gb = (GUIBox) super.createGUI();
		
		slider = new Slider() {
			
			private float xMin, xMax;
			
			@Override public void mouseDragged(){
				if(getDm().getInput().getMouseX() < xMin + .5 * (xMax)) {
					min = (getDm().getInput().getMouseX() - getX()) / getWidth();
					if(min > max - .1)
						max += min - max + .101;
				}else{
					max = (getDm().getInput().getMouseX() - getX()) / getWidth();
					if(max < min + .1)
						min -= min - max + .101; 
				}
				if(min > 1)			min = 1;
				else if(min < 0)	min = 0;
				if(max > 1)			max = 1;
				else if(max < 0)	max = 0;
				in.setChanged();
			}
			
			@Override public void render(){
				getDm().g.noStroke();
				getDm().g.fill(50);
				getDm().g.rect(getX(), getY(), getWidth(), getHeight());
				xMin = (float)(getX() + getWidth() * min);
				xMax = (float)(getWidth() * (max - min));
				getDm().g.fill(160,100);
				getDm().g.rect(xMin, getY(), xMax, getHeight());
				getDm().g.stroke(250);
				getDm().g.strokeWeight(2);
				getDm().g.line(xMin, getY() - 1, xMin, getY() + getHeight() + 1);
				getDm().g.line(xMin + xMax, getY() - 1, xMin + xMax, getY() + getHeight() + 1);
				getDm().g.fill(200, 0, 0);
				getDm().g.noStroke();
				getDm().g.rect((float)(xMin + 2 + (xMax - 4) * in.getValue()), getY(), 1, getHeight());
			}
		};
		slider.setWH(60, 10);
		
		gb.addRow(slider);
		
		return gb;
	}
	*/
	
	@Override
	public void processIO() {
		if(in.changed()) 
			out.setValue(min + (max - min) * in.getInput());
	}
	

	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		return super.doSerialize(sdc).addExtras(min, max);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		min = data.nextDouble();
		max = data.nextDouble();
	}
}
