package module.modules.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import constants.Flow;
import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import gui.text.TextInputField;
import lombok.Getter;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.inout.DataType;
import module.inout.UniversalInputIF;
import module.inout.UniversalOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;


public abstract class SendReceive extends AbstractModule{

	protected static final Map<String, Send> sendMap = new ConcurrentHashMap<>();
	protected static final List<Receive> receivers = new ArrayList<>();
	
	protected static void addSend(String name, Send send) {
		
		if(name != null)
			sendMap.put(name, send);
		
		updateSends();
	}
	
	protected static void updateSends() {
		for (Receive r : receivers) {
			r.updateSend();
		}
	}
	
	protected static void removeSend() {
		
	}
	
	
	@Getter
	protected volatile DataType type = DataType.Number;
	@Getter
	protected volatile Object value = type.getDefaultValue();
	
	
	@Getter
	private String senderName;
	
	
	public void setSenderName(String name) {
		
		this.senderName = name;
		
		if(gui != null) {
			
			gui.setGUIName(getName() + " [" + name + "]");
		}
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
				.addExtras(getSenderName());
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		setSenderName(data.nextString());
	}
	
	@NodeInfo(type = NodeCategory.TOOLS, helpPatch = "SendReceive")
	public static class Send extends SendReceive {
		
		private final UniversalInputIF in;
		
		
		public Send() {
			
			in = addUniversalInput("In");
			setName("Send");
			flowFlags.add(Flow.SINK);
		}
		
		@Override
		public NodeInspector createGUISpecial() {
			
			var n = NodeInspector.newInstance(this, ni -> {
				
				var t = new TextInputField(this::setSenderName, this::getSenderName);
				t.update();
				ni.createSection("Value");
				ni.addInspectorElement(t);
			});
			
			return n;
		}
		
		@Override
		public void processIO() {
			if(in.changed()) {
				value = in.getValueObject();
				type = in.getDynamicType();
			}
		}
		
		@Override
		public void setSenderName(String name) {
			
			super.setSenderName(name);
			
			addSend(name, this);
		}
		
		
		@Override
		public void onAdd() {
			super.onAdd();
			
			String n = getSenderName();
			
			if(n != null)
				addSend(n, this);
		}
		
		@Override
		public void onDelete(ModuleDeleteMode mode) {
			super.onDelete(mode);
			
			String n = getSenderName();
			if(n != null)
				SendReceive.sendMap.remove(n, this);
		}

		
	}
	
	
	@NodeInfo(type = NodeCategory.TOOLS, helpPatch = "SendReceive")
	public static class Receive extends SendReceive {
		
		private final UniversalOutputIF out;
		private Send send;
		
		public Receive() {
			
			setName("Receive");
			flowFlags.add(Flow.SOURCE);
			
			out = addUniversalOutput("Value");
		}
		 
		private void updateSend() {
			
			if(send != null)
				setSenderName(send.getSenderName());
		}

		@Override
		public NodeInspector createGUISpecial() {
			
			var n = NodeInspector.newInstance(this, ni -> {
				
				var d = new DropDownMenu(i -> {}, () -> 0) {
					
					@Override
					public void setSelected(int i) {
						super.setSelected(i);
						String n = getStringValue();
						if(sendMap.containsKey(n))
							send = sendMap.get(getStringValue());
						setSenderName(n);
					}
					 
					@Override
					public void update() {
						clear();
						sendMap.keySet().stream()
										.sorted()
										.forEachOrdered(this::addItem);
					
						setStringValue(getSenderName());
					}
				};
				d.update();
				ni.createSection("Value");
				ni.addInspectorElement(d);
			});
			
			return n;
		}
		
		
		@Override
		public void setSenderName(String senderName) {
			super.setSenderName(senderName);
			send = sendMap.get(senderName);
		}
		
		@Override
		public void processIO() {
			
			if(send != null) {
				out.setOutput(send.getValue(), send.getType());
			}
		}

		
		@Override
		public void onAdd() {
			super.onAdd();
			
			receivers.add(this);
			
			updateSend();
		}
		
		@Override
		public void onDelete(ModuleDeleteMode mode) {
			super.onDelete(mode);
			
			receivers.remove(this);
		}
		
	}
	
}
