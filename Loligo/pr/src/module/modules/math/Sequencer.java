package module.modules.math;

import java.util.Arrays;
import java.util.function.Supplier;

import constants.DisplayFlag;
import gui.NoteSequencer;
import gui.SliderArray;
import gui.nodeinspector.NodeInspector;
import gui.text.CharProfile;
import gui.text.NumberInputField;
import lombok.var;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.DisplayObject;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.TOOLS)

public class Sequencer extends AbstractModule {

	private SliderArray sliderArr;
	private NumberInputField lengthInput;
	
	private double[] sequence;
	
	private final NumberInputIF in;
	private final NumberOutputIF out;
	private int index = 0;
	private double index_fac;
	
	
	public Sequencer() {
		
		in = addInput("Select");
		out = addOutput("Out");
		
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		
		sequence = new double[8];
		Arrays.fill(sequence, 0d);
	}
	
	@Override
	public NodeInspector createGUISpecial() {
	
		var ni = NodeInspector.newInstance(this, n -> {
		
			Supplier<Double[]> sup = () -> {
				var dd = new Double[sequence.length];
				for (int i = 0; i < sequence.length; i++) 
					dd[i] = sequence[i];
				return dd;
			};
			
			sliderArr = new SliderArray(sequence.length, this::setSequence, sup);
			
			var pointer = new DisplayObject() {
				
				public void render(PGraphics g) {
					
					float f = (float) sliderArr.getWidth() / sliderArr.getLength(),
						  x = sliderArr.getX() + f * index,
						  w = f - 1;
					
					g.fill(0x05ffffff);
					g.rect(x ,sliderArr.getY(), w, sliderArr.getHeight());
					
					g.fill(getColor());
					g.rect(x ,sliderArr.getY() + sliderArr.getHeight() + 1, w, 3);
					
				}
			};
			pointer.setColor(0x66ff1111);
			pointer.setFlag(DisplayFlag.CLICKABLE, false);
			sliderArr.addChild(pointer);
			
			sliderArr.setWH(n.getWidth(), 100);
			
			n.createSection("Sequencer");
			
			//n.addInspectorElement(sliderArr);
			
			
			n.addInspectorElement(new NoteSequencer.NodeSequencerGui(new NoteSequencer()));
			
			sliderArr.update();
			
			
			lengthInput = new NumberInputField(
											 i -> setSequenceLength(i.intValue()), 
											() -> (double) sequence.length);
			
			lengthInput.setCharProfile(CharProfile.INT);
			lengthInput.setRange(2, 64);
			lengthInput.update();
			
			n.addInspectorElement(lengthInput, 3);
		});
		
		
		return ni;
	}
	
	
	public void setSequenceLength(int len) {
		
		int tlen = sequence.length;
		sequence = Arrays.copyOf(sequence, len);
		
		if(len > tlen)
			Arrays.fill(sequence, tlen, len, 0d);
		
		if(sliderArr != null) {
			
			sliderArr.update();
			lengthInput.update();
		}
		index = Math.min(index, len - 1);
		updateOutput();
	}
	
	
	public void setSequence(Double[] f) {
		
		if(f.length != sequence.length) {
			sequence = new double[f.length];
		}
		
		for (int i = 0; i < f.length; i++) {
			sequence[i] = f[i];
		}
		
		if(sliderArr != null) {
			sliderArr.update();
			lengthInput.update();
		}
		updateOutput();
	}
	
	
	/*
	@Override
	public void guiMessage(GuiSender<?> s) {
		
		if(s == sliderArr)
			sequence = sliderArr.getValue();
	}

	@Override
	public void updateSender(GuiSender<?> s) {
		
		if(s == sliderArr)
			sliderArr.setValue(sequence);
	}
	*/


	@Override
	public void processIO() {
		
		
		if(in.changed()) {
			
			index_fac = in.getInput();
			updateOutput();
		}
	}


	private void updateOutput() {
		int i = (int) (sequence.length * index_fac);
		if(i >= sequence.length)
			i = sequence.length - 1;
		index = i;
		
		out.setValue(sequence[i]);
	}


	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.addExtras(sequence);
		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		sequence = (double[]) data.nextObject();
	}
}
