package module.modules.math;

import static module.inout.NumericAttribute.DEZIMAL_FREE;
import java.util.function.DoubleUnaryOperator;
import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import lombok.RequiredArgsConstructor;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Utils;

@NodeInfo(type=NodeCategory.MATH)
public class Round extends AbstractModule {

	@RequiredArgsConstructor
	private enum Mode {
		UP		(Math::ceil), 
		DOWN	(Math::floor), 
		NEAREST	(Math::round);
		
		final DoubleUnaryOperator fun;
		
		double apply(double d) {
			return fun.applyAsDouble(d);
		}
	}
	
	private final static Mode[] modes = Mode.values();
	private final static String[] names = Utils.formatedEnumString(modes);
	
	private final NumberInputIF in;
	private final NumberOutputIF out;
	private Mode mode;
	
	
	public Round() {
		in = addInput("In", DEZIMAL_FREE);
		out = addOutput("Out", DEZIMAL_FREE);
	}
	
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		mode = Mode.DOWN;
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {

		var ni = NodeInspector.newInstance(this, n -> {
			n.createSection("Rounding");
			DropDownMenu d = new DropDownMenu(i -> setMode(modes[i]), this::getMode, names);
			n.addLabeledEntry("Mode", d);
		});

		return ni;
	}
	
	public void setMode(Mode mode) {
		this.mode = mode;
	}
	
	public int getMode() {
		return mode.ordinal();
	}
	
	@Override
	public void processIO() {
		
		double d = in.getWildInput();
		d = mode.apply(d);
		out.setValue(d);
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.addExtras(mode);
		return md;
	}
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		mode = (Mode) data.nextObject();
	}

}
