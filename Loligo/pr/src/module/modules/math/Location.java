package module.modules.math;

import static module.inout.NumericAttribute.DEZIMAL_FREE;
import static module.modules.math.Translate.NORM_LENGTH;

import gui.CheckBox;
import gui.NodeSelector2;
import gui.nodeinspector.NodeInspector;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import module.AbstractModule;
import module.Module;
import module.ModuleType;
import module.PositionableModule;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import patch.Domain;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;


@NodeInfo(type = NodeCategory.TOOLS)
public class Location extends AbstractModule {

	
	private final NumberOutputIF out_x, out_y;
	@Getter
	private PositionableModule target;
	private Domain domain;
	
	@Getter@Setter
	private boolean global = true;
	
	public Location() {
		out_x = addOutput("X", DEZIMAL_FREE);
		out_y = addOutput("Y", DEZIMAL_FREE);
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		return NodeInspector.newInstance(this, n -> {

			var nf = new NodeSelector2(this::setTarget, this::getTarget);
			nf.setTypes(ModuleType.POSITIONABLES);
			
			n.addLabeledEntry("Target", nf);
			
			var cb = new CheckBox(this::setGlobal, this::isGlobal);
			//cb.setMouseOverText("Fuck you guys. I'm going home. \n Fuck fuck shit shit Arschfick... ");
			n.addLabeledEntry("Global position", cb);
			
		});
	}
	
	
	private void setTarget(Module m) {
		target = (PositionableModule) m;
		domain = m.getDomain();
	}
	
	
	@Override
	public void processIO() {
		
		if(target != null) {
			
			float x = target.getX(), y = target.getY();
			
			if(global) {
				var m = domain.getDisplayManager().getTransform().getMultipliedMatrix(); 
				float xg = m.multX(x, y);
				y = m.multY(x, y);
				x = xg;
			}
			
			out_x.setValue(x / NORM_LENGTH);
			out_y.setValue(y / NORM_LENGTH);
		}
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.addExtras((Object) null);
		sdc.saveReference(md, target, 0);
		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		rdc.restoreReference(data, m -> setTarget((PositionableModule) m));
	}

}
