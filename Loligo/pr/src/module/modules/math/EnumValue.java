package module.modules.math;

import static constants.SlotEvent.SLOT_CONNECT;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.SlotEvent;
import constants.SlotEventListener;
import gui.DropDownMenu;
import gui.GuiSender;
import gui.nodeinspector.NodeInspector;
import gui.selection.GuiType;
import lombok.var;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.inout.DataType;
import module.inout.EnumInput;
import module.inout.EnumOutput;
import module.inout.IOEnum;
import module.inout.IOEnum.IOEnumEntryIF;
import module.inout.Input;
import module.inout.Output;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.ModuleGUI;
import pr.ModuleGUIWrapper;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type=NodeCategory.TOOLS)
public class EnumValue extends AbstractModule implements SlotEventListener{

	private final EnumOutput out;	
	private final EnumInput in;
	private IOEnum enumType = IOEnum.EMPTY;
	private IOEnumEntryIF value = enumType.getValues().get(0);
	
	
	public EnumValue() {
	
		in = new EnumInput("In", this, enumType);
		out = new EnumOutput("Out", this, enumType);
		addIO(in);
		addIO(out);
	}
	
	@Override
	public NodeInspector createGUISpecial() {

		return NodeInspector.newInstance(this, n -> {
			
			var ns = IOEnum.ENUM_REGISTRY.stream().map(IOEnum::getName).toArray(String[]::new);
			
			Consumer<Integer> a = i -> setEnumType(IOEnum.ENUM_REGISTRY.get(i), 0); 
			Supplier<Integer> u = () -> IOEnum.ENUM_REGISTRY.indexOf(enumType);
			
			var dm = new DropDownMenu(a, u, ns);
			n.addLabeledEntry("Enum", dm);
		});
		
	}
	
	@Override
	public void processIO() {
		
		if(in.changed()) {
			
			setEnumValue( in.getEnumInput().getOrdinal()); 
		}
	}
	
	
	private final void setEnumType(IOEnum enumType, int index) {
		
		this.enumType = enumType;
		
		in.setEnumType(enumType);
		out.setEnumType(enumType);
		
		/*
		if(gui != null) {
			
			DropDownMenu d = (DropDownMenu) gui;
			d.clear();
			
			for(String s : IOEnum.names(enumType)) {
				d.addItem(s);
			}
			
			d.update();
			d.adaptSize();
		}
		*/
		
		setEnumValue(index);
		
	}
	
	
	public void setEnumValue(int i) {
		
		out.setValue(i);
		value = enumType.getValues().get(i);
		
		if(gui instanceof GuiSender<?> gs)
			gs.update();
			
	}
	
	
	@Override
	public ModuleGUI createGUI() {
		
		var d = new DropDownMenu(
			this::setEnumValue, 
			value::getOrdinal, 
			IOEnum.names(enumType));
		
		d.setFlag(DisplayFlag.DRAGABLE, true);
		d.setGuiType(GuiType.SIGNAL);
		
		return new ModuleGUIWrapper(this, d);
	}


	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		
		if(e == SLOT_CONNECT && this.out == out && in.getType() == DataType.ENUM) {
			
			//EnumInput ein = (EnumInput) in;
			//setEnumType(ein.getEnumType(), (int) ein.getWildInput());
		}
	}
	
	
	@Override
	public void onAdd() {
		super.onAdd();
		
		getDomain().getModuleManager().addSlotEventListener(this);
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		
		getDomain().getModuleManager().removeSlotEventListener(this);
		
	}

	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc);
		
		md.putExtra("EnumType", enumType.getName());
		md.putExtra("EnumValue", value.getName());
		
		return md;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		super.doDeserialize(data, rdc);
		
		String t = (String) data.getExtraValue("EnumType");
		String v = (String) data.getExtraValue("EnumValue");
		
		IOEnum e = IOEnum.getEnumByName(t);
		
		setEnumType(e, e.getValueByName(v).getOrdinal());
	}
}
