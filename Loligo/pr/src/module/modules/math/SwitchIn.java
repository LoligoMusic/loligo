package module.modules.math;

import static module.inout.DataType.UNIVERSAL;
import static module.inout.IOType.IN;

import java.util.ArrayList;
import java.util.List;

import constants.Flow;
import lombok.var;
import module.AbstractModule;
import module.Modules;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.dynio.DynIOModules;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.NumberInputIF;
import module.inout.UniversalInputIF;
import module.inout.UniversalOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import save.ModuleData;
import save.RestoreDataContext;

@NodeInfo(type = NodeCategory.TOOLS)

public class SwitchIn extends AbstractModule implements DynIOModule {
	
	private final UniversalOutputIF out;
	private final NumberInputIF in_select;
	private final List<UniversalInputIF> inputs = new ArrayList<>();
	
	public SwitchIn() {
		flowFlags.add(Flow.DYNAMIC_IO);
		in_select = addInput("Select");
		out = addUniversalOutput("Out");
	}

	@Override
	public void defaultSettings() {
		super.defaultSettings();
		createDynIO("In1", UNIVERSAL, IN);
		createDynIO("In2", UNIVERSAL, IN);
	}
	
	@Override
	public void processIO() {
		int s = inputs.size();
		int i = (int) (s * in_select.getWildInput());
		i %= s;
		UniversalInputIF in = inputs.get(i);
		out.setOutput(in.getValueObject(), in.getDynamicType());
	}
	
	
	@Override
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		var in = addUniversalInput("");
		rename(in, name);
		in.setOptional();
		Modules.initIOGUI(this, in);
		inputs.add(in);
		return in;
	}
	

	@Override
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		var in = Modules.getInputByName(this, name);
		if(in == null)
			return null;
		removeIO(in);
		inputs.remove(in);
		return in;
	}

	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.UNIVERSAL_IN;
	}
	

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		DynIOModules.restoreDynInputs(this, data, 1);
		super.doDeserialize(data, rdc);
	}
	
}
