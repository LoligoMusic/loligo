package module.modules.math;

import static module.inout.NumericAttribute.DEZIMAL_FREE;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(name="Map", type=NodeCategory.MATH)

public class MapValue extends AbstractModule {

	private final NumberInputIF in, in_min1, in_max1, in_min2, in_max2;
	private final NumberOutputIF out;
	
	
	public MapValue() {
		
		in 		= addInput("In"			, DEZIMAL_FREE);
		in_min1 = addInput("Minimum In"	, DEZIMAL_FREE);
		in_max1 = addInput("Maximum In"	, DEZIMAL_FREE);
		in_min2 = addInput("Minimum Out", DEZIMAL_FREE);
		in_max2 = addInput("Maximum Out", DEZIMAL_FREE);
		out 	= addOutput("Out"		, DEZIMAL_FREE);
		
	}
	
	@Override
	public void defaultSettings() {

		super.defaultSettings();

		in_min1.setValue(0);
		in_min2.setValue(0);
		in_max1.setValue(1);
		in_max2.setValue(1);
	}
	
	
	@Override
	public void processIO() {
		
		if(in.changed() 	 || 
		   in_min1.changed() || 
		   in_max1.changed() || 
		   in_min2.changed() || 
		   in_max2.changed()
		   ) {
			
			double  din  = in.getWildInput(),
					min1 = in_min1.getWildInput(),
					max1 = in_max1.getWildInput(),
					min2 = in_min2.getWildInput(),
					max2 = in_max2.getWildInput();
			
			double f;
			f = (din - min1) / (max1 - min1);
			f = min2 + f * (max2 - min2);
			
			out.setValue(f);
		}
	}

}
