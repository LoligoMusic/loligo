package module.modules.math;

import static module.inout.NumericAttribute.BOOL;
import static module.inout.NumericAttribute.DEZIMAL_FREE;
import static module.inout.NumericType.toBool;

import java.io.Serializable;
import java.util.Arrays;

import gui.CheckBox;
import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import lombok.Getter;
import lombok.Setter;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import save.ModuleData;
import save.RestoreDataContext;
import save.RestoreField;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.TOOLS)

public class Tween extends AbstractModule {
	
	private static final String LINEAR = "Linear", CUBIC = "Cubic";
	private static final String[] MODES = new String[]{LINEAR, CUBIC};
	
	private static final double MAX_SPEED = 200;
	private NumberInputIF in, in_speed, in_reset;
	private final NumberOutputIF out;
	
	@Getter@Setter@RestoreField
	private double fac, cur, key;
	@Getter@Setter@RestoreField
	private double step = 0; 
	@Getter@Setter@RestoreField
	private int period = 100;
	@Getter@Setter@RestoreField
	private boolean circular = false;
	
	transient private DropDownMenu menu_filter;
	private TweenFilter filter;
	
	
	public Tween() {
		
		in = addInput("In", DEZIMAL_FREE);
		in_speed = addInput("Speed");
		in_reset = addInput("Reset", BOOL);
		
		out = addOutput("Out", DEZIMAL_FREE);
		
		setFilter(LINEAR);
	}
	
	@Override
	public NodeInspector createGUISpecial() { 
		
		var ni = NodeInspector.newInstance(this, n -> {
		
			menu_filter = new DropDownMenu( this::setFilter, 
											()->Arrays.asList(MODES).indexOf(filter.name),
											new String[]{LINEAR, CUBIC});
			
			n.addLabeledEntry("Filter", menu_filter);
			CheckBox cb = new CheckBox(this::setCircular, this::isCircular);
			n.addLabeledEntry("Circular", cb);
			cb.update();
			menu_filter.update();
		});
		
		return ni;
	}
	
	
	@Override
	public void processIO() {
		
		boolean inChanged = in.changed(),
				speedChanged = in_speed.changed();
		
		if(toBool(in_reset.getInput())) {
			
			key = cur = in.getWildInput();
			step = period;
			filter.start(cur, key);
			out.setValue(cur);
		}else
		if(inChanged || speedChanged) {
			
			if(speedChanged)
				period = 1 + (int) ((MAX_SPEED - 1) * in_speed.getInput());
			
			if(inChanged)
				key = in.getWildInput();
			
			if(circular) {
				key %= 1;
				cur %= 1;
				double d = key - cur;
				
				key += d > .5  ? -1:
					   d < -.5 ?  1:
						   		  0;
			}
			
			filter.start(cur, key);
			step = 0;
		}
		
		if(step < period && getDomain().isProcessActive()) {
			
			step ++;
			
			fac = step >= period ? 
					1 :
					step / period;
			
			cur = filter.filter(fac);
			
			if(circular)
				cur %= 1;
			
			out.setValue(cur);
		}
		
	}
	
	
	private void setFilter(final int i) {
		
		setFilter(MODES[i]);
	}
	
	public void setFilter(final String name) {
		
		filter = switch (name) {
		case LINEAR -> new Linear();
		case CUBIC	-> new Spline();
		default		-> new Linear();
		};
		
		filter.start(cur, key);
		step = 0;
		
		if(menu_filter != null)
			menu_filter.update();
	}
	

	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		return super.doSerialize(sdc).addExtras(filter.name);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		setFilter(data.nextString()); 
	}
	
	
	private class Linear extends TweenFilter{

		public Linear() {
			super(LINEAR);
		}

		private static final long serialVersionUID = 1L;

		@Override
		double filter(double t) {
			
			return p0 + (p1 - p0) * t;
		}
		
		
	}
	
	
	private class Spline extends TweenFilter{
		
		private static final long serialVersionUID = 1L;
		double  m0 = 0, //start tangente
				m1 = 0, //end tangente
				t = 0;
				 
		public Spline() {
			super(CUBIC);
		}
		
		@Override
		public double filter(final double t) {
			
			this.t = t;
			
			final double tP2 = t * t,
					 	 tP3 = tP2 * t;
				
			return (2 * tP3 - 3 * tP2 + 1) * p0 + 
				   (-2 * tP3 + 3 * tP2   ) * p1 + 
				   (tP3 - 2 * tP2 + t    ) * m0 + 
				   (tP3 - tP2            ) * m1;
			
		}

		@Override
		public void start(final double p0, final double p1) {
			
			final double tP2 = t * t;
			
			m0 = ( 6 * tP2 - 6 * t    ) * this.p0 + 
				 (-6 * tP2 + 6 * t    ) * this.p1 + 
				 ( 3 * tP2 - 4 * t + 1) * m0 + 
				 ( 3 * tP2 - 2 * t    ) * m1;
			
			super.start(p0, p1);
		}

		
		
	}
	
	private abstract class TweenFilter implements Serializable{
		
		private static final long serialVersionUID = 1L;
		double p0, p1;
		final String name;
		
		public TweenFilter(final String name) {
			this.name = name;
		}
		
		abstract double filter(final double t);
		void start(final double p0, final double p1) {
			
			this.p0 = p0;
			this.p1 = p1;
		}
		
		
	}
	

}
