package module.modules.math;

import java.io.IOException;
import java.io.ObjectInputStream;

import lombok.Getter;
import lombok.Setter;
import module.AbstractModule;
import module.Module;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type = NodeCategory.TOOLS)

public class Delay extends AbstractModule {
	
	private NumberOutputIF out;
	private NumberInputIF in, in_length;
	
	private int delay = 100;
	static private final double maxDelayTime = 200;
	
	private double[] buffer;
	
	@Getter@Setter
	private int pos = 0, off = 0, index;
	
	
	public Delay() {
		
		in = addInput("In", NumericAttribute.DEZIMAL_FREE);
		in_length = addInput("Delay");
		out = addOutput("Out", NumericAttribute.DEZIMAL_FREE);
		
		
		buffer = new double[(int)maxDelayTime];
	}
	
	/*
	@Override
	public DisplayObject createGUI() {
		
		GUIBox gb = (GUIBox) super.createGUI();
		
		DisplayObject d = new DisplayObject() {
			
			@Override
			public void render() {
			
				dm.g.stroke(160);
				for (int i = 0; i < buffer.length; i++) {
					
					float x = getX() + i;
					
					
					dm.g.line(x, getY(),x, getY() + (float)buffer[i] * 20);
					
				}
				float x2 = getX() + index;
				
				dm.g.stroke(255);
				dm.g.line(x2, getY(), x2, getY() - 5);
				
				dm.g.noStroke();
			}
		};
		
		d.width = buffer.length;
		
		gb.addRow(d);
		
		return gb;
	}
	*/
	
	@Override
	public void processIO() {
		
		if(in_length.changed()) {
			
			int d = delay;
			
			setDelay((int)(in_length.getInput() * maxDelayTime));
			
			
			if(d < delay) {
				
				int end = index + delay;
				double v = buffer[(off + d) % buffer.length];
				
				for (int i = off + d; i < end; i++) 
					buffer[i % buffer.length] = v;
				
			}
			
			off = index;
			pos = 0;
		}
		
		
		if(delay < 1) 
			
			out.setValue(in.getInput());
		else {
			
			index = (off + pos % delay) % buffer.length;
			buffer[index] = in.getInput();
			
			pos++;
			
			index = (off + pos % delay) % buffer.length;
			out.setValue(buffer[index]);
		}
	}
	
	public void setDelay(final int frames) {
		
		
		delay = (int) (frames > maxDelayTime ? maxDelayTime :
					   frames < 0			 ? 0 :
											   frames);
				
		//if(delaySlider != null)
		//	delaySlider.update();
	}
	
	public int getDelay() {
		
		return delay;
	}
	
	
	@Override
	public Module createInstance() {
		return new Delay();
	}

	
	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
		
		ois.defaultReadObject();

		if(in_length == null)
			in_length = addInput("Delay");
		
		if(buffer == null) 
			buffer = new double[(int)maxDelayTime];
			
	}
	
}
