package module.modules.math;

import static util.Colors.blue;
import static util.Colors.green;
import static util.Colors.red;

import java.awt.Color;

import module.AbstractModule;
import module.inout.ColorInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.COLOR, name = "HSB-Split", helpPatch = "ColorConversion")

public class HSBSplit extends AbstractModule {
	
	private final ColorInputIF in_color;
	private final NumberOutputIF out_hue, out_satur, out_bright;
	private final float[] hsb = new float[3];
	
	public HSBSplit() {
		in_color = addColorInput();
		out_hue = addOutput("Hue");
		out_satur = addOutput("Saturation");
		out_bright = addOutput("Brightness");
	}
	
	@Override
	public void processIO() {
		
		if(in_color.changed()) {
		
			int c = in_color.getColorInput();
			Color.RGBtoHSB(red(c), green(c), blue(c), hsb);
			out_hue.setValue(hsb[0]);
			out_satur.setValue(hsb[1]);
			out_bright.setValue(hsb[2]);
		}
	}

}
