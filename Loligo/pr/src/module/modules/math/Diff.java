package module.modules.math;

import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.TOOLS)

public class Diff extends AbstractModule {

	private final NumberInputIF in;
	private final NumberOutputIF out;
	
	private double last;
	
	public Diff() {
		
		in = addInput("In", NumericAttribute.DEZIMAL_FREE); 
		out = addOutput("Diff");
	}
	
	@Override
	public void processIO() {
		
		double d = in.getWildInput();
		out.setValue(d - last);
		last = d;
	}

}
