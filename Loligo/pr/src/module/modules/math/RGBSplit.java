package module.modules.math;

import module.AbstractModule;
import module.Module;
import module.inout.ColorInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.COLOR, name = "RGB-Split", helpPatch = "ColorConversion")

public class RGBSplit extends AbstractModule {
	
	private ColorInputIF in_color;
	private NumberOutputIF out_red, out_green, out_blue;
	
	public RGBSplit() {
		
		in_color = addColorInput();
		out_red = addOutput("Red");
		out_green = addOutput("Green");
		out_blue = addOutput("Blue");
		
	}
	
	@Override
	public void processIO() {
		
		if(in_color.changed()) {
			int c = in_color.getColorInput();
			out_red.setValue(((c >> 16) & 0xff) / 255d);
			out_green.setValue((c >> 8 & 0xff) / 255d);
			out_blue.setValue((c & 0xff) / 255d);
		}
		
	}

	@Override
	public Module createInstance() {
		return new RGBSplit();
	}

}
