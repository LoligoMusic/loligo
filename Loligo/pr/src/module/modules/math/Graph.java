package module.modules.math;

import static constants.InputEvent.PRESSED;

import java.util.ArrayList;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayObjects;
import gui.Position;
import gui.nodeinspector.NodeInspector;
import gui.selection.GuiType;
import module.AbstractModule;
import module.Module;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.DisplayObject;
import pr.Positionable;
import pr.userinput.UserInput;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Colors;

@NodeInfo(type = NodeCategory.TOOLS)

public class Graph extends AbstractModule {
	
	final private NumberOutputIF out;
	final private NumberInputIF in;
	
	transient private DisplayObject area;
	final private ArrayList<Vertex> vertices = new ArrayList<Vertex>();
	
	int lineColor, vertexColor;
	private double pointer = 0, value = 0;
	private boolean needsUpdate;
	
	public Graph() {
		
		in = addInput("In");
		out = addOutput("Out");
		
		vertexColor = Colors.rgb(180, 180, 150);
		lineColor = vertexColor;
		
		
		//vertices.get(0).gui.lockX = vertices.get(1).gui.lockX = true;
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		initVerts();
	}
	
	@Override
	public void resetModule() {
		super.resetModule();
		vertices.clear();
	}
	
	private void initVerts() {
		
		Vertex v;
		v = new Vertex(0, 0);
		v.lockFirst = true;
		vertices.add(v);
		v = new Vertex(1, 1);
		v.lockLast = true;
		vertices.add(v);
		
		updateLinks();
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
	
		var ni = NodeInspector.newInstance(this, n -> {
		
			area = new DisplayObject() {
				
				private float pointerX;
				@Override public void render(PGraphics g) {
					
					float x = getX(), y = getY(), w = getWidth(), h = getHeight();
					
					g.fill(getColor());
					g.noStroke();
					g.rect(x, y, w, h);
					Vertex v1, v2;
					g.strokeWeight(1);
					g.stroke(lineColor);
					
					for (int i = 1; i < vertices.size(); i++) {
						v1 = vertices.get(i - 1);
						v2 = vertices.get(i);
						g.line(v1.gui.getX(), v1.gui.getY(), v2.gui.getX(), v2.gui.getY());
					}
					
					g.stroke(200, 20, 20, 100);
					g.fill(200, 20, 20, 100);
					pointerX = x + w * (float)pointer;
					float yy = y + h * (1 - (float)value);
					g.line(pointerX, yy, pointerX, y + h);
					g.ellipse(pointerX, yy, 2, 2);
					
					g.noStroke();
					
					/*
					g.fill(g.color(150));
					g.text("In", getX() + getWidth() - 13, getY() + getHeight() - 1);
					
					g.pushMatrix();
					g.translate(getX(), getY());
					g.rotate(PConstants.HALF_PI);
					g.text("Out", 2 , -2);//x + 2, y + 12
					g.popMatrix();
					*/
				}
				
				@Override
				public void mouseEventThis(constants.InputEvent e, pr.userinput.UserInput input) {
					if(e == PRESSED) {
					
						double x = (double)(getDm().getInput().getMouseX() - getX()) / area.getWidth(),
							   y = 1-(double)(getDm().getInput().getMouseY() - getY()) / area.getHeight();
						
						Vertex v = addVertex(x, y);
						
						getDm().getInput().injectDragTarget(v.gui);//setPressedTarget(v.gui);
					}
				}
			};
			
			area.setColor(Colors.rgb(110, 110, 90));
			area.setFlag(DisplayFlag.RECT, true); 
			area.setWH(0, 100);
			
			n.createSection("Graph");
			n.addInspectorElement(area);
			
			createVertexGui();
		
		});
		
		return ni;
	}
	
	private void createVertexGui() {
		
		for (var v : vertices) {
			if(v.gui == null)
				v.createGUI();
			area.addChild(v.gui);
			v.updateGUI();
		}
	}
	
	@Override
	public void processIO() {
		
		if(in.changed() || needsUpdate) {
			
			needsUpdate = false;
			
			pointer = in.getInput();
			
			value = calcOut(pointer);
			out.setValue(value);
		}
		
	}
	
	public double calcOut(double pointer) {
		
		Vertex v1 = null, v2 = null;
		double out = 0;
		for (int i = 1; i < vertices.size(); i++) {
			v2 = vertices.get(i);
			if(pointer <= v2.fx) {
				v1 = vertices.get(i - 1);
				double r = (pointer - v1.fx)  / (v2.fx - v1.fx);
				out = v1.fy + (v2.fy - v1.fy) * r;
				break;
			}
		}
		return out;
	}
	
	/*
	@Override
	public void render() {
		super.render();
		if(in.partner == null)
			pointer = pointer < 1 ? pointer + .05 : 0;
	}
	*/
	
	private Vertex addVertex(final double fx, final double fy) {
		
		Vertex v = new Vertex(fx, fy);
		
		if(createGUI && getGui() != null) 
			v.createGUI();
		
		
		for (int i = 1; i < vertices.size(); i++) 
			if(v.fx < vertices.get(i).fx) {
				
				vertices.add(i, v);
				break;
			}
		
		updateLinks();
		
		v.checkBounds();
		
		v.updateGUI();
		
		return v;
	}
	
	/*
	private void sortVertices() {
		
		Collections.sort(vertices, Comparator.comparingDouble(v -> v.fx));
	}
	*/
	
	private void updateLinks() {
		
		for (int i = 0; i < vertices.size(); i++) {
			
			Vertex v = vertices.get(i);
			v.last = i == 0 					? null : vertices.get(i - 1);
			v.next = i == vertices.size() - 1 	? null : vertices.get(i + 1);
		}
	}
	
	private void removeVertex(Vertex v) {
		
		int index = vertices.indexOf(v);
		if(index == 0 || index == vertices.size() - 1)
			return;
		
		vertices.remove(index);
		v.last.next = v.next;
		v.next.last = v.last;
		
		if(v.gui != null) {
			DisplayObjects.removeFromDisplay(v.gui);
			//MouseEventContext.instance.removeListener(v.gui);
		}
	}
	
	
	@Override
	public Module createInstance() {
		return new Graph();
	}
	
	

	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {

		var md = super.doSerialize(sdc);
		
		var verts = new double[vertices.size() * 2]; 
		int i = 0;
		for (Vertex v : vertices) {
			verts[i] = v.fx;
			verts[i + 1] = v.fy;
			i += 2;
		}
		
		md.addExtras(verts);

		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {

		super.doDeserialize(data, rdc);
		
		var verts = (double[]) data.nextObject();
		
		int len = verts.length / 2;
		
		for (int i = 0; i < len; i += 2) {
			
			double fx = verts[i],
				   fy = verts[i + 1];
			
			vertices.add(new Vertex(fx, fy));
		}
		
		updateLinks();
		
		vertices.get(0).lockFirst = true;
		vertices.get(vertices.size() - 1).lockLast = true;
		
		//if(rdc.isGui())
		//	createVertexGui();
	}
	
	
	private class Vertex implements Positionable{
		
		double fx, fy;
		boolean lockFirst = false, lockLast = false;
		Vertex last, next;
		transient DisplayObject gui;
		
		public Vertex(double fx, double fy) {
			
			this.fx = fx;
			this.fy = fy;
			
			checkBounds();
		}
		
		void createGUI() {
			
			gui = new DisplayObject() {
					
				@Override 
				public void mouseEventThis(InputEvent e, UserInput input) {
					if(e == InputEvent.PRESSED_RIGHT) {
						removeVertex(Vertex.this);
					}else
					if(e == InputEvent.DRAG) {
						Vertex.this.fx = (double) getPosObject().x / area.getWidth();
						Vertex.this.fy = 1 - (double) getPosObject().y / area.getHeight();
						
						if(checkBounds())
							updateGUI();
						
						needsUpdate = true;
						
						if(!getDomain().getSignalFlow().contains(Graph.this))
							processIO();
					}
				}
			};
				
			gui.setGuiType(GuiType.DRAGABLE_INPUT);
			gui.setWH(6, 6);
			gui.setColor(vertexColor);
			gui.setFlag(DisplayFlag.fixedToParent, true);
			gui.setFlag(DisplayFlag.fixedToParent, true);
			//gui.setFlag(DisplayFlag.Subscriber, true);
				
			area.addChild(gui);
			
			
			//if(index != 0 && index != vertices.size() - 1)
			//	gui.setFlag(DisplayFlag.Subscriber, true);
				
			Position pos = lockFirst ? new Position.Bounds(area, 0, area.getHeight()) :
						   lockLast  ? new Position.Bounds(new Position.PositionOffset(area, area.getWidth(), 0), 0, area.getHeight()) :
								       new Position.Bounds(area, area.getWidth(), area.getHeight());
			
			gui.setPosObject(pos);	
			
			updateGUI();
		}
		
		
		boolean checkBounds() {
			
			boolean b = false;
			if(lockFirst && fx != 0) { 			  fx = 0; b = true;}
			else if(lockLast && fx != 1){		  fx = 1; b = true;}
			if(last != null && fx <= last.fx){     fx = last.fx + Double.MIN_VALUE; b = true;}
			if(next != null && fx >= next.fx){     fx = next.fx - Double.MIN_VALUE ; b = true;}
			if(fy < 0) {			 			  fy = 0; b = true;}
			else if(fy > 1) {				 	  fy = 1; b = true;}
			if(fx < 0) {			 			  fx = 0; b = true;}
			else if(fx > 1) {				 	  fx = 1; b = true;}
			
			return b;
		}
		
		void updateGUI() {
			
			if(gui != null && area != null) 
				gui.setPos(area.getWidth()  * (float)fx, area.getHeight() * (1 - (float)fy));
		}

		@Override
		public float getX() {
			return 0;
		}

		@Override
		public float getY() {
			return 0;
		}

		@Override
		public void setPos(float x, float y) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Position getPosObject() {
			return null;
		}

		@Override
		public void setPosObject(Position p) {
			
		}
	}
	
	
	
}
