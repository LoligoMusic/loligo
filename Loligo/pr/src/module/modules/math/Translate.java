package module.modules.math;

import constants.Units;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.TransformOutput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type = NodeCategory.TRANSFORM, helpPatch = "Transform")
public class Translate extends AbstractModule {

	public static final float NORM_LENGTH = (float) (Units.LENGTH.normFactor * 2);
	
	private final NumberInputIF in_x, in_y;
	private final TransformOutput out_trans;
	
	
	public Translate() {
		in_x = addInput("X", NumericAttribute.DEZIMAL_FREE);
		in_y = addInput("Y", NumericAttribute.DEZIMAL_FREE);
		out_trans = addTransformOutput();
	}
	
	
	@Override
	public void processIO() {
		
		if(in_x.changed() || in_y.changed()) {
		
			var m = out_trans.getMatrix();
			m.reset();
			float x = (float) (in_x.getWildInput() * NORM_LENGTH), 
				  y = (float) (in_y.getWildInput() * NORM_LENGTH);
			m.translate(x, y);
		}
		
	}
}
