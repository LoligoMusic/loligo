package module.modules.math;

import java.util.Arrays;
import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.NumericType;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.TOOLS)

public class Buffer extends AbstractModule {

	private final static int DEFAULTSIZE = 2;
	
	private final NumberInputIF in_put, in_get, in_idx, in_len, in_value;
	private final NumberOutputIF out_value;
	
	private double[] buffer;
	private int index;
	
	public Buffer() {
	
		in_put = addInput("Insert", NumericAttribute.BOOL);
		in_get = addInput("Get", NumericAttribute.BOOL);
		in_idx = addInput("Index", NumericAttribute.INTEGER_FREE);
		in_len = addInput("Length", NumericAttribute.INTEGER_POSITIVE).setDefaultValue(DEFAULTSIZE);
		in_value = addInput("Value", NumericAttribute.DEZIMAL_FREE);
		out_value = addOutput("Out", NumericAttribute.DEZIMAL_FREE);
		
		buffer = new double[DEFAULTSIZE];
		
	}
	
	
	@Override
	public void processIO() {
		
		if(in_len.changed()) {
			
			resize((int) in_len.getWildInput());
		}
		
		if(in_idx.changed()) {
			
			updateIndex((int) in_idx.getWildInput());
		}
		
		if(NumericType.toBool(in_put.getWildInput())) {
			
			buffer[index] =  in_value.getWildInput();
		}
		
		if(NumericType.toBool(in_get.getWildInput())) {
			
			out_value.setValue(buffer[index]);
		}
		
	}

	private void resize(int len) {
		
		buffer = Arrays.copyOf(buffer, len);
		updateIndex(index);
	}
	
	private void updateIndex(int i) {
		
		index = i % buffer.length;
		index = index < 0 ? buffer.length + index : index;
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return  super.doSerialize(sdc)
				.addExtras((Object) buffer);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		buffer = (double[]) data.nextObject();
	}
}
