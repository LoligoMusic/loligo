package module.modules.math;

import static module.inout.NumericAttribute.BANG;
import static module.inout.NumericAttribute.BOOL;

import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.NumericType;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type = NodeCategory.TOOLS, helpPatch = "BinaryState")

public class TogEdge extends AbstractModule {

	private final NumberInputIF in;
	private final NumberOutputIF out_up, out_down;
	
	private boolean last;
	
	public TogEdge() {
		
		in = addInput("In", BOOL);
		out_up = addOutput("Up", BANG);
		out_down = addOutput("Down", BANG);
	}
	
	
	@Override
	public void processIO() {
		
		boolean b = NumericType.toBool(in.getWildInput());
		
		out_up.setValue(0);
		out_down.setValue(0);
		
		if(b != last) {
			
			(b ? out_up : out_down).setValue(1);
			last = b;
		}
	}

}
