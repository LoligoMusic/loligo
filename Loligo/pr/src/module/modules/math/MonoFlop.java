package module.modules.math;

import static module.inout.NumericAttribute.BANG;
import static module.inout.NumericAttribute.BOOL;
import static module.inout.NumericAttribute.INTEGER_POSITIVE;
import static module.inout.NumericType.toBool;

import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type=NodeCategory.TOOLS, helpPatch = "TimedState")

public class MonoFlop extends AbstractModule {

	private final NumberInputIF in_set, in_reset, in_time;
	private final NumberOutputIF out;
	
	private int count, max;
	private boolean active;
	
	public MonoFlop() {
		
		in_set = addInput("Set", BANG);
		in_time = addInput("Time", INTEGER_POSITIVE);
		in_reset = addInput("Reset", BANG);
		
		in_time.setDefaultValue(30);
		
		out = addOutput("Out", BOOL);
	}
	
	@Override
	public void processIO() {
		
		if(toBool(in_reset.getWildInput())) {
			
			reset();
		}else
		if(toBool(in_set.getWildInput())) {
			
			active = true;
			count = 0;
			out.setValue(1);
		}
		
		if(active) {
			
			max = (int) in_time.getWildInput();
			
			if(count >= max) {
				reset();
			}else 
			if(getDomain().isProcessActive()) {
				count++;
			}
		}
	}
	
	
	private void reset() {
		
		count = 0;
		active = false;
		out.setValue(0);
	}

}
