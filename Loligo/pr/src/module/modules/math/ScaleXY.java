package module.modules.math;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import constants.Flow;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.TransformOutput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type = NodeCategory.TRANSFORM, helpPatch = "Transform")
public class ScaleXY extends AbstractModule {

	private final NumberInputIF in_scaleX, in_scaleY;
	private final TransformOutput out_trans;
	
	
	public ScaleXY() {
		
		flowFlags.add(Flow.SOURCE);
		
		out_trans = addTransformOutput();
		in_scaleX = addInput("X", DEZIMAL_FREE).setDefaultValue(1);
		in_scaleY = addInput("Y", DEZIMAL_FREE).setDefaultValue(1);
	}
	
	
	@Override
	public void processIO() {
		
		if(in_scaleX.changed() || in_scaleY.changed()) {
		
			float sx = (float) in_scaleX.getWildInput(),
				  sy = (float) in_scaleY.getWildInput();
		
			var m = out_trans.getMatrix();
			m.reset();
			m.scale(sx, sy);
		}
		
	}
}
