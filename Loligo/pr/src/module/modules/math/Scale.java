package module.modules.math;

import constants.Flow;
import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.TransformOutput;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import processing.core.PMatrix2D;

@NodeInfo(type = NodeCategory.TRANSFORM, helpPatch = "Transform")
public class Scale extends AbstractModule {

	private final NumberInputIF in_scale;
	private final TransformOutput out_trans;
	
	
	public Scale() {
		
		flowFlags.add(Flow.SOURCE);
		
		out_trans = addTransformOutput();
		in_scale = addInput("Scale", NumericAttribute.DEZIMAL_FREE).setDefaultValue(1);
		
	}
	
	
	@Override
	public void processIO() {
		
		if(in_scale.changed()) {
		
			float s = (float) in_scale.getWildInput();
				 
			PMatrix2D m = out_trans.getMatrix();
			m.reset();
			m.scale(s, s);
		}
		
	}
}
