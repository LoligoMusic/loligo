package module.modules.math;

import java.util.Random;

import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.TriggerInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.MATH)

public class RandomSeed extends AbstractModule {

	private final TriggerInputIF trig, trig_reset;
	private final NumberInputIF in_seed;
	private final NumberOutputIF out;
	
	private final Random random;
	
	public RandomSeed() {
		
		trig = addTrigger();
		in_seed = addInput("Seed", NumericAttribute.INTEGER_FREE);
		trig_reset = addTrigger();
		out = addOutput("Value");
		
		random = new Random();
	}
	
	@Override
	public void defaultSettings() {
	
		super.defaultSettings();
		
		random.setSeed((long) in_seed.getDefaultValue());
	}
	
	@Override
	public void processIO() {
		
		if(in_seed.changed() || trig_reset.triggered()) {
			
			random.setSeed((long) in_seed.getWildInput());
			//out.setValue(r);
		}
		
		if(trig.triggered()) {
			
			double r = random.nextDouble();
			out.setValue(r);
		}
	}

}
