package module.modules.math;

import static module.inout.NumericAttribute.BANG;
import static module.inout.NumericAttribute.BOOL;
import static module.inout.NumericAttribute.DEZIMAL_FREE;
import static module.inout.NumericAttribute.INTEGER_FREE;
import static module.inout.NumericAttribute.NOTE;
import static module.inout.NumericAttribute.PUSH;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.Flow;
import constants.SlotEvent;
import constants.SlotEventListener;
import gui.DisplayGuiSender;
import gui.DropDownMenu;
import gui.Position;
import gui.Positionables;
import gui.nodeinspector.NodeInspector;
import gui.selection.GuiType;
import gui.text.NumberInputField;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import module.AbstractModule;
import module.Module;
import module.ModuleDeleteMode;
import module.Modules;
import module.inout.Input;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.NumericAttribute;
import module.inout.NumericType;
import module.inout.NumericType.SubType;
import module.inout.Output;
import module.loading.InternalNodeEntry;
import module.loading.NodeCategory;
import module.loading.NodeEntry;
import module.loading.NodeInfo;
import module.loading.NodeLoader;
import pr.EventSubscriber;
import pr.ModuleGUI;
import pr.ModuleGUIWrapper;
import save.ModuleData;
import save.PatchTuner;
import save.RestoreDataContext;
import save.SaveDataContext;
import save.SaveTarget;

@NodeInfo(type = NodeCategory.MATH, helpPatch = "ValueBoxes", ignoreBuild = true)
public class Value extends AbstractModule implements SlotEventListener{

	public static NodeLoader loader() {
		return new NodeLoader() {
			
			private List<NodeEntry> li = new ArrayList<>();
			
			@Override
			public List<NodeEntry> loadEntries() {
				addEntry(DEZIMAL_FREE);
				addEntry(INTEGER_FREE);
				addEntry(BOOL);
				addEntry(BANG, "Bang");
				addEntry(PUSH, "Push");
				addEntry(NOTE, "Note");
				return li;
			}
			
			private void addEntry(NumericAttribute att) {
				addEntry(att, att.numericType().name);
			}
			
			private void addEntry(NumericAttribute att, String name) {
				Supplier<Value> s = () -> new Value(att);
				NodeEntry e = new ValueEntry(s, name, NodeCategory.MATH);
				li.add(e);
			}
		};
	}
	
	private static class ValueEntry extends InternalNodeEntry {

		public ValueEntry(Supplier<? extends Module> su, String name, NodeCategory type) {
			super(su, name, type);
			
		}
		
		@Override
		public Module createInstance() {
			Module m = instanceProvider.get();
			m.setName("Number");
			return m;
		}
		
	}
	
	
	private final NumberInputIF in;
	private final NumberOutputIF out;
	@Getter
	private double value;
	//private NumericAttribute attributes;
	private NumericType.SubType type;
	@Getter@Setter
	private double minValue, maxValue;
	
	protected DisplayGuiSender.Numeric numField; 
	
	
	public Value(NumericAttribute attributes) {
	
		flowFlags.add(Flow.THROUGHPUT);
		
		//this.attributes = attributes;
		type = attributes.numericType();
		minValue = attributes.minValue();
		maxValue = attributes.maxValue();
		
		in = addInput("In", attributes);
		out = addOutput("Out", attributes);
		
	}
	
	public void setNumberAttribute(NumericType.SubType st) {
		
		type = st;
		
		if(gui != null) {
			
			var nf = numField;
			numField = createNumField();
			var mg = (ModuleGUIWrapper) gui;
			mg.setDisplayObject(numField);
			
			if(nf != null) {
				
				Modules.getAllIOs(this).forEach(io -> {
					var g = io.getGUI();
					Position p = Positionables.copyPosition(g);
					Positionables.subtract(p, nf);
					g.setPosObject(p);
					numField.addChild(io.getGUI());	
				});
				
				Positionables.copyPosition(nf, numField);
			}
			
			getDomain().getDisplayManager().addGuiObject(mg);
		}
	}
	
	@Override
	public void postInit() {
		flowFlags.add(Flow.PERMA);
		super.postInit();
	}
	
	@Override
	public ModuleGUI createGUI() {
		numField = createNumField();
		var g = new ModuleGUIWrapper.Sender<Double>(this, numField);
		if(numField instanceof NumberInputField ni) {
			ni.getTextField().setSelf(g);
			ni.setFlag(DisplayFlag.DRAGABLE, true);
		}
		
		return g;
	}
	
	private DisplayGuiSender.Numeric createNumField() {
		var a = new NumericAttribute(type, minValue, maxValue);
		numField = NumericAttribute.createGuiByType(a, this::insertValue, this::getValue);
		numField.setWH(70, 20);
		numField.setGuiType(GuiType.SIGNAL);
		return numField;
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
	
		var n = NodeInspector.newInstance(this, ni -> {
		
			ni.createSection("Interface");
			
			var ens = EnumSet.allOf(NumericType.class);
			var ssn = ens.stream().map(NumericType::name).toArray(String[]::new);
			var nts = new ArrayList<>(ens);
			
			Consumer<Integer> cn = i -> {
				var na = nts.get(i);
				var nt = na == NumericType.BOOLEAN ? NumericType.SubType.TOGGLE : 
													 NumericType.SubType.DECIMAL;
				setNumberAttribute(nt);
				ni.update();
			};
			Supplier<Integer> sn = () -> nts.indexOf(type.type);
			var nm = new DropDownMenu(cn, sn, ssn);
			nm.update();
			ni.addLabeledEntry("Type", nm);
			
			
			switch(type) {
				
			case INTEGER, DECIMAL:
				var md = selectSubType(SubType.INTEGER, SubType.DECIMAL);
				ni.addLabeledEntry("Mode", md);
				var nmin = new NumberInputField(this::setMinValue, this::getMinValue);
				var nmax = new NumberInputField(this::setMaxValue, this::getMaxValue);
				nmin.setNumericType(type);
				nmax.setNumericType(type);
				ni.addLabeledEntry("Range Min", nmin);
				ni.addLabeledEntry("Range Max", nmax);
				break;
			case TOGGLE, BANG, PUSH:
				var bm = selectSubType(SubType.TOGGLE, SubType.BANG, SubType.PUSH);
				ni.addLabeledEntry("Mode", bm);
				break;
			default:
				break;
			}
			
		});
		return n;
	}
	
	
	private DisplayGuiSender<?> selectSubType(SubType... subTypes) {
		
		var nas = Arrays.asList(subTypes);
		String[] ssd = nas.stream().map(SubType::name).toArray(String[]::new);
		
		var tb = (DisplayGuiSender.Numeric)(((ModuleGUIWrapper)getGui()).getDisplayObject());
		
		Consumer<Integer> c = i -> {
			var na = nas.get(i);
			setNumberAttribute(na);
			tb.setNumericType(na);
		};
		Supplier<Integer> s = () -> nas.indexOf(type);
		
		var md = new DropDownMenu(c, s, ssd);
		md.update();
		return md;
	}
	
	public void insertValue(final double value) {
		
		this.value = value;
		out.setValue(value);
		
		if(in.getPartner() == null) {
			in.setValue(value);
			
		}
		
		if(numField != null) {
			numField.update();
		}
	}
	
	
	@Override
	public void processIO() {
		
		if(in.changed()) {
			in.getInput();
			insertValue(in.getWildInput());
		}
	}

	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		
		if(numField != null) {
		
			/*
			if(out == this.out && in instanceof NumberInputIF) {
				
				NumberInputIF nin = (NumberInputIF) in;
				attributes = nin.getNumberAttributes();
				((ModuleGUIWrapper) getGui()).setDisplayObject(createGuiByType(attributes));
			}
			*/
			
			if(e.isConnectDisconnect && in == this.in) {
				numField.disable(e == SlotEvent.SLOT_CONNECT);
			}
		}
	}
	
	
	@Override
	public void onAdd() {
		super.onAdd();
		
		getDomain().getModuleManager().addSlotEventListener(this);
		
		if(numField instanceof EventSubscriber)	//TODO do this somehow in numField
			getDomain().getDisplayManager().getInput().addInputEventListener((EventSubscriber)numField);
			
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		
		getDomain().getModuleManager().removeSlotEventListener(this);
		
		if(numField instanceof EventSubscriber) //TODO do this somehow in numField
			getDomain().getDisplayManager().getInput().removeInputEventListener((EventSubscriber) numField);
			
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.addExtras(type.name(), minValue, maxValue);
		
		if(sdc.getSaveTarget() == SaveTarget.MEMENTO)
			sdc.addLateTask(s -> new PatchTuner().cutNode(s.getPatchData(), md, in.getName(), out.getName()));
		
		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		
		String snt = data.nextString();
		var st = NumericType.SubType.valueOf(snt);
		setNumberAttribute(st);
		
		if(type.type == NumericType.NUMBER) {
			minValue = data.nextDouble();
			maxValue = data.nextDouble();
		}
	}
	

}
