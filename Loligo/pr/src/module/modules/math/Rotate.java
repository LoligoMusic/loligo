package module.modules.math;

import constants.Flow;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.TransformOutput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import processing.core.PConstants;

@NodeInfo(type = NodeCategory.TRANSFORM, helpPatch = "Transform")
public class Rotate extends AbstractModule {

	private final NumberInputIF in_rotate;
	private final TransformOutput out_trans;
	
	
	public Rotate() {
		
		flowFlags.add(Flow.SOURCE);
		
		out_trans = addTransformOutput();
		in_rotate = addInput("Rotate", NumericAttribute.DEZIMAL_FREE);
	}
	
	
	@Override
	public void processIO() {
		
		if(in_rotate.changed()) {
		
			float r  = (float) in_rotate.getWildInput() * PConstants.TAU;
		
			var m = out_trans.getMatrix();
			m.reset();
			m.rotate(r);
		}
		
	}
}
