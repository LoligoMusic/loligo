package module.modules.math;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import util.ImprovedNoise;

@NodeInfo(type = NodeCategory.TOOLS)

public class Perlin extends AbstractModule {

	private final NumberInputIF in_x, in_y;
	private final NumberOutputIF out;
	
	public Perlin() {
		
		in_x = addInput("X", DEZIMAL_FREE);
		in_y = addInput("Y", DEZIMAL_FREE);
		out = addOutput("Out");
	}
	
	@Override
	public void processIO() {
		
		if(in_x.changed() || in_y.changed()) {
			
			double x = in_x.getWildInput(),
				   y = in_y.getWildInput();
			
			double n = ImprovedNoise.noise(x, y, 0);
			
			out.setValue(n);
		}
		
	}

}
