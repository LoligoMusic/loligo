package module.modules.math;

import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.NumericType;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.TOOLS, helpPatch = "BinaryState")

public class Toggle extends AbstractModule {

	private final NumberInputIF in_set, in_reset;
	private final NumberOutputIF out;
	
	private boolean state;
	
	public Toggle() {
		
		in_set = addInput("Set", NumericAttribute.BANG);
		in_reset = addInput("Reset", NumericAttribute.BANG);
		
		out = addOutput("Out", NumericAttribute.BOOL);
	}

	@Override
	public void processIO() {
		
		if(NumericType.toBool(in_set.getWildInput())) {
			
			state = !state;
			out.setValue(state ? 1d : 0d);
			
		}else
		if(state && NumericType.toBool(in_reset.getWildInput())) {
			
			state = false;  
			out.setValue(state ? 1d : 0d);
		}
		
	}


}
