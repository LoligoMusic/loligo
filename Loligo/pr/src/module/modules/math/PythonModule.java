package module.modules.math;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.python.core.Py;
import org.python.core.PyCode;
import org.python.core.PyDictionary;
import org.python.core.PyException;
import org.python.core.PyFunction;
import org.python.core.PyInstance;
import org.python.core.PyList;
import org.python.core.PyObject;
import org.python.core.PyString;
import org.python.jline.internal.InputStreamReader;
import org.python.util.PythonInterpreter;

import constants.Flow;
import constants.Timer;
import errors.LoligoException;
import gui.CheckBox;
import gui.nodeinspector.NodeInspector;
import gui.text.FileInputField;
import lombok.Getter;
import lombok.val;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.Modules;
import module.inout.Input;
import module.inout.NumericAttribute;
import module.inout.NumericType.SubType;
import module.inout.Output;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import module.pyscript.Script;
import module.pyscript.Script.Back;
import module.pyscript.Script.In;
import module.pyscript.Script.InColor;
import module.pyscript.Script.InNum;
import module.pyscript.Script.Out;
import module.pyscript.Script.OutColor;
import module.pyscript.Script.OutNum;
import pr.ModuleGUI;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.TOOLS)
public class PythonModule extends AbstractModule  {
	
	private static PyCode LOLIGO_LIB;
	
	
	URL url;
	PythonInterpreter interpreter;
	PyFunction function_loop, function_onDelete;
	WatchService watchService;
	Path fileName;
	String nodeName;
	@Getter
	boolean autoReload = true, success = false;
	
	private final List<In> ins = new ArrayList<>(); 
	private final List<Out> outs = new ArrayList<>();
	private Back[] oldIns, oldOuts;
	
	public PythonModule() {
		flowFlags.add(Flow.PERMA);
	}
	
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		
		setUrl(Script.class.getResource("test.py"));
		tryInit();
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		
		val ni = NodeInspector.newInstance(this, n -> {
			
			Consumer<URL> su = u -> {
				//loadUrl(u);
				setUrl(u);
				tryInit();
				Timer.createFrameDelay(n.getDm().getDomain(), t -> n.update());
				//n.update();
			};
			val f = new FileInputField(su, this::getUrl);
			f.getFileDialog().setFileEndings(".py");
			f.getFileDialog().setDialogTitle("Choose Script..");
			f.getFileDialog().setFileDescription("Python Script (.py)");
			n.addInspectorElement(f);
			
			n.createSection("Script");
			n.addInspectorElement(f);
			
			Consumer<Boolean> ar = b -> {
				setAutoReload(b);
				n.update();
			};
			val aut = new CheckBox(ar, this::isAutoReload);
			aut.setMouseOverText("Reload script automatically on change.");
			n.addLabeledEntry("Auto-reload", aut);
		});
		
		return ni;
	}
	
	
	private void clearIOs() {
		Modules.removeDynamicInputs(this);
		Modules.removeDynamicOutputs(this);
		ins.clear();
		outs.clear();
	}
	
	
	private void tryInit() {
		try {
			init();
			var g = getModuleGUI();
			if(g != null)
				g.unmarkError();
			success = true;
		}catch(PyException e) {
			onError(e);
		}catch(ClassCastException e) {
			onError(e);
		}
	}
	
	private Back[] collectInputBacks() {
		var oi = getInputs();
		var oldIns = this.oldIns != null ? this.oldIns :
					 oi == null ?  new Back[0] :
								   oi.stream().map(p -> new Back(p, p.getPartner(), p.getValueObject()))
											  .toArray(Back[]::new);
		return oldIns;
	}
	
	private Back[] collectOutputBacks() {
		
		var oo = getOutputs();
		
		if(this.oldOuts != null) {
			return this.oldOuts;
		}
		
		if(oo == null) {
			return new Back[0];
		}
		
		List<Back> outList = new ArrayList<>();
		for(Output out : oo) {
			List<Input> ins = out.getPartners();
			if(ins.isEmpty()) {
				var b = new Back(null, out, out.getValueObject());
				outList.add(b);
			}else {
				for(Input in : ins) {
					var b = new Back(in, out, null);
					outList.add(b);
				}
			}
		}
		return outList.toArray(Back[]::new);
	}
	
	private void init() {
		
		this.oldIns = collectInputBacks();
		this.oldOuts = collectOutputBacks();
					  
		clearIOs();
		
	    if(interpreter != null) 
	    	interpreter.close();
	    
	    Script.init();
	    interpreter = new PythonInterpreter(new PyDictionary(), Py.getSystemState());
	    
	    
	    if(url == null)
	    	return;
	    
	    
	    if(LOLIGO_LIB == null) {
			var ism = Script.class.getResourceAsStream("loligo.py");
			LOLIGO_LIB = interpreter.compile(new InputStreamReader(ism));
	    }
	    interpreter.exec(LOLIGO_LIB);
	    
	    var cd = loadUrl(url);
		interpreter.exec(cd);
		
		
		nodeName = ((PyString) interpreter.get("_node_name")).toString();
		if(getModuleGUI() != null)
			getModuleGUI().setGUIName(nodeName);
		
		function_loop = (PyFunction) interpreter.get("loop");
		function_onDelete = (PyFunction) interpreter.get("onDelete");
		
		setupIns(interpreter);
		setupOuts(interpreter);
		
		reconnect(oldIns, oldOuts, false);
		
	}
	
	
	public void reconnect(Back[] oldIns, Back[] oldOuts, boolean restoreMissingIOs) {
		if(oldIns != null) {
			for (Back pi : oldIns) {
				Output o = pi.out;
				Input newIn = Modules.getInputByName(this, pi.in.getName());
				if(newIn != null) {
					newIn.setValueObject(pi.value);
					if(o != null) 
						getDomain().getModuleManager().connect(newIn, o);
				}else
				if(restoreMissingIOs){
					addIO(pi.in);
					Modules.initIOGUI(this, pi.in);
					if(o != null) 
						getDomain().getModuleManager().connect(pi.in, o);
				}
			}
		}
		
		if(oldOuts != null) {
			for (Back out : oldOuts) {
				Input in = out.in;
				Output newOut = Modules.getOutputByName(this, out.out.getName());
				if(newOut != null && in != null) {
					getDomain().getModuleManager().connect(in, newOut);
				}else
				if(restoreMissingIOs){
					addIO(out.out);
					Modules.initIOGUI(this, out.out);
					if(in != null) 
						getDomain().getModuleManager().connect(in, out.out);
				}
			}
		}
		
		this.oldIns = null;
		this.oldOuts = null;
	}
	
	
	public void setupIns(PythonInterpreter interpreter) {
		
		PyList pins = (PyList) interpreter.get("inputs");
		
		if(pins != null) {
			
			for (int i = 0; i < pins.size(); i++) {
				
				In in = null;
				PyInstance po = (PyInstance) pins.get(i);
				String name = po.__getattr__("name").asString();
				String type = po.__getattr__("type").asString();
				double pmin = po.__getattr__("min" ).asDouble();
				double pmax = po.__getattr__("max" ).asDouble();
				PyObject def = po.__getattr__("value");
				
				switch(type) {
				case "int":
					var ini = new InNum();
					var ati = new NumericAttribute(SubType.INTEGER, pmin, pmax);
					ini.input = addInput(name, ati);
					ini.input.setDefaultValue(def.asDouble());
					in = ini;
					break;
				case "float":
					var inf = new InNum();
					var atf = new NumericAttribute(SubType.DECIMAL, pmin, pmax);
					inf.input = addInput(name, atf);
					inf.input.setDefaultValue(def.asDouble());
					in = inf;
					break;
				case "bool":
					var inb = new InNum();
					inb.input = addInput(name, NumericAttribute.BOOL);
					inb.input.setDefaultValue(def.asDouble());
					in = inb;
					break;
				case "bang":
					var ing = new InNum();
					ing.input = addInput(name, NumericAttribute.BANG);
					in = ing;
					break;
				case "toggle":
					var ino = new InNum();
					ino.input = addInput(name, NumericAttribute.PUSH);
					in = ino;
					break;
				case "color":
					var inc = new InColor();
					inc.input = addColorInput();
					inc.input.setName(name);
					inc.input.setDefaultColor(Script.asColor(def));
					inc.containerClass = interpreter.get("Color");
					in = inc;
					break;
				}
				in.pyObject = pins;
				in.getInput().setOptional();
				ins.add(in);
				Modules.initIOGUI(this, in.getInput());
			}
		}
	}
	
	
	public void setupOuts(PythonInterpreter interpreter) {
		
		PyList pouts = (PyList) interpreter.get("outputs");
		
		if(pouts != null) {
			
			for (int i = 0; i < pouts.size(); i++) {
				
				Out out = null;
				PyObject po = (PyInstance) pouts.get(i);
				String name = po.__getattr__("name").asString();
				String type = po.__getattr__("type").asString();
				double pmin = po.__getattr__("min" ).asDouble();
				double pmax = po.__getattr__("max" ).asDouble();
				
				switch(type) {
				case "int":
					OutNum oni = new OutNum();
					var ati = new NumericAttribute(SubType.INTEGER, pmin, pmax);
					oni.output = addOutput(name, ati);
					out = oni;
					break;
				case "float":
					OutNum onf = new OutNum();
					var atf = new NumericAttribute(SubType.DECIMAL, pmin, pmax);
					onf.output = addOutput(name, atf);
					out = onf;
					break;
				case "bool":
					OutNum onb = new OutNum();
					onb.output = addOutput(name, NumericAttribute.BOOL);
					out = onb;
					break;
				case "color":
					OutColor oc = new OutColor();
					oc.output = addColorOutput();
					oc.output.setName(name);
					out = oc;
					break;
				}
				out.pyObject = pouts;
				out.getOutput().setOptional();
				outs.add(out);
				Modules.initIOGUI(this, out.getOutput());
			}
		}
	}
	
	private void onError(Exception e) {
		success = false;
		var g = getModuleGUI();
		if(g != null) {
			var lo = new LoligoException(e, "Python error");
			g.markError(lo);
		}
		clearIOs();
		reconnect(oldIns, oldOuts, true);
		function_loop = null;
		System.out.println(e.getMessage());
		e.printStackTrace();
	}
	
	@Override
	public ModuleGUI gui() {
		var g = super.gui();
		if(nodeName != null)
			g.setGUIName(nodeName);
		return g;
	}
	
	
	public PyCode loadFile(File f) {
		
		try {
			url = f.toURI().toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
		return loadUrl(url);
	}
	
	public void setUrl(String u) {
		if(u != null) {
			try {
				url = new URL(u);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public PyCode loadUrl(URL u) {
		
		InputStream is = null;
		try {
			is = u.openStream();
		} catch (IOException e2) {
			e2.printStackTrace();
			return null;
		}
		val isr = new InputStreamReader(is);
		return interpreter.compile(isr, u.getFile());
	}
	
	public void setUrl(URL u) {
		url = u;
		if(autoReload)
			watch();
	}
	
	public URL getUrl() {
		return url;
	}
	
	public void watch() {
		
		if(url == null)
			return;
		
		if(watchService != null) {
			try {
				watchService.close();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				watchService = null;
			}
		}
		
		try {
			var p = Paths.get(url.toURI());
			if(!java.nio.file.Files.isDirectory(p)) {
				fileName = p.getFileName();
				p = p.getParent();
			}
			watchService = FileSystems.getDefault().newWatchService();
			p.register(watchService, ENTRY_MODIFY, ENTRY_CREATE, ENTRY_DELETE);
			
		} catch (IOException | URISyntaxException | IllegalArgumentException | FileSystemNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public void setAutoReload(boolean aut) {
		autoReload = aut;
		
		if(aut) {
			watch();
			reloadIfChange();
		}else 
		if(watchService != null){
			try {
				watchService.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			watchService = null;
		}
		
	}
	
	public void reloadIfChange() {
		if(watchService != null) {
			var wk = watchService.poll();
			if(wk != null) {
				for(val e : wk.pollEvents()) {
					if(e.context().equals(fileName)) {
						tryInit();
					}
				}
				wk.reset();
			}
		}
	}
	
	
	@Override
	public void processIO() {
		
		reloadIfChange();
		
		if(function_loop == null)
			return;
		
		try {
			
			for(val in : ins) 
				in.get();
			
			function_loop.__call__();
			
			for(val out : outs) 
				out.set();
			
		}catch(PyException e) {
			onError(e);
		}
		
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		
		if(function_onDelete != null)
			function_onDelete.__call__();
		
		if(interpreter != null) 
	    	interpreter.close();
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.createMap();
		md.putExtra("autoreload", autoReload);
		md.putExtra("url", url != null ? url.toString() : null);
		return md;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		autoReload = data.getExtraBool("autoreload");
		
		String u = data.getExtraString("url");
		setUrl(u);
		init();
		

		if(!success) {
			Modules.restoreDynamicInputs(data);
			Modules.restoreDynamicOutputs(data);
		}
		
		super.doDeserialize(data, rdc);
	}

}
