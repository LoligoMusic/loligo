package module.modules.math;

import static constants.Units.LENGTH;
import static module.inout.NumericAttribute.DEZIMAL_POSITIVE;

import constants.Flow;
import constants.PatchEvent;
import constants.PatchEventListener;
import constants.PatchEventManager;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.DisplayManagerIF;
import pr.RootClass;

@NodeInfo(type = NodeCategory.TOOLS)
public class PatchInfo extends AbstractModule implements PatchEventListener{

	private final NumberOutputIF out_width, out_height;
	private DisplayManagerIF dm;
	
	public PatchInfo() {
		
		flowFlags.add(Flow.SOURCE);
		
		out_width  = addOutput("Width" , DEZIMAL_POSITIVE);
		out_height = addOutput("Height", DEZIMAL_POSITIVE);
		
	}
	
	@Override
	public void postInit() {
		super.postInit();
		dm = RootClass.mainDm();
		updateValues();
	}
	
	@Override
	public void onAdd() {
		super.onAdd();
		PatchEventManager.instance.addListener(this);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		PatchEventManager.instance.removeListener(this);
	}
	
	@Override
	public void onPatchEvent(PatchEvent e) {
		if(e.type() == PatchEvent.Type.RESIZED) {
			updateValues();
		}
	}
	
	private void updateValues() {
		out_width.setValue(dm.getWidth() / LENGTH.normFactor);
		out_height.setValue(dm.getHeight() / LENGTH.normFactor);
	}
	
	@Override
	public void processIO() {
	
	}

}
