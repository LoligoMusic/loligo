package module.modules.math;

import java.util.HashMap;
import java.util.Map;

import constants.DisplayFlag;
import constants.Flow;
import gui.ColorPreview;
import gui.nodeinspector.NodeInspector;
import gui.paths.EnumDropDownMenu;
import gui.selection.GuiType;
import gui.text.MultiLineTextInputField;
import gui.text.NumberInputField;
import gui.text.TextBlock.AlignMode;
import gui.text.TextBlock.TextFlag;
import module.AbstractModule;
import module.inout.NumericType;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.ModuleGUI;
import pr.ModuleGUIWrapper;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;


@NodeInfo(type=NodeCategory.TOOLS)
public class Comment extends AbstractModule {

	private MultiLineTextInputField textBox;
	
	public Comment() {
		flowFlags.add(Flow.NONESSENTIAL);
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		if(textBox != null)
			textBox.setText("Comment");
	}
	
	@Override
	public ModuleGUI createGUI() {
		
		textBox = new MultiLineTextInputField(this::setText, () -> textBox.getText());
		textBox.setTextFlag(TextFlag.MULTILINE);
		textBox.setTextFlag(TextFlag.RESIZEABLE);
		textBox.setTextFlag(TextFlag.LIVEUPDATE);
		textBox.setTextSize(22);
		textBox.setPadding(3);
		textBox.setFlag(DisplayFlag.DRAGABLE, true);
		
		textBox.setWH(150, 100);
		textBox.setFixedWidth(true);
		
		textBox.setGuiType(GuiType.SIGNAL);
		textBox.setBackground(true);
		
		var g = new ModuleGUIWrapper.Sender<String>(this, textBox) {
			@Override public void positionSlots() {}
		};
		textBox.setSelf(g);
		return g;
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		
		return NodeInspector.newInstance(this, n -> {
			
			var cp1 = new ColorPreview(textBox::setTextColorInactive, textBox::getText_color_inactive);
			n.addLabeledEntry("Text Color", cp1);
			
			var cp2 = new ColorPreview(textBox::setColorInactive, textBox::getColor_inactive);
			n.addLabeledEntry("Background Color", cp2);
			
			var ns = new NumberInputField(s -> textBox.setTextSize(s.intValue()), () -> (double) textBox.getTextSize());
			ns.setNumericType(NumericType.SubType.INTEGER);
			ns.setRange(8, 32);
			n.addLabeledEntry("Size", ns);
			
			var dd = new EnumDropDownMenu<AlignMode>("", textBox::setAlignMode, textBox::getAlignMode, AlignMode.LEFT);
			n.addLabeledEntry("Align", dd);
			
		});
	}

	
	@Override
	public void processIO() {
		
	}

	
	public void setText(String txt) {
		
		if(textBox != null) {
			
			final String old = textBox.getText();
			
			getDomain()
				.getHistory()
				.addAction(() -> textBox.setValue(txt), 
						   () -> textBox.setValue(old))
				.execute();
		}
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		var data = super.doSerialize(sdc);
		if(textBox != null) {
			var md = new HashMap<>(7);
			md.put("comment"	, textBox.getText()				  );
			md.put("width"		, textBox.getWidth()			  );
			md.put("height"		, textBox.getHeight()			  );
			md.put("fontSize"	, textBox.getTextSize()			  );
			md.put("fontColor"	, textBox.getText_color_inactive());
			md.put("bgColor"	, textBox.getColor_inactive() 	  );
			md.put("align"		, textBox.getAlignMode().name()	  );
			data.addExtras(md);
		}
		return data;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		
		if(textBox != null) {
			var map = (Map<Object, Object>) data.nextObject();
			var txt	= (String)  map.get("comment"	);
			int w	= (Integer) map.get("width"		);
			int s	= (Integer) map.get("fontSize"	);
			int c	= (Integer) map.get("fontColor"	);
			int bg	= (Integer) map.get("bgColor"	);
			var a	= (String)  map.get("align"		);
			textBox.setWH(w, 0);
			textBox.setTextSize(s);
			textBox.setTextColorInactive(c);
			textBox.setColorInactive(bg);
			textBox.setAlignMode(AlignMode.valueOf(a));
			setText(txt);
		}
	}
	
	
	@Override
	public void positionSlots() {
	
	}
	
}
