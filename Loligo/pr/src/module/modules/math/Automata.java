package module.modules.math;

import static constants.InputEvent.CLICK;
import static module.inout.NumericAttribute.BANG;
import static module.inout.NumericAttribute.INTEGER_POSITIVE;
import static patch.DomainType.PARTICLE_INSTANCE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import constants.InputEvent;
import gui.text.TextBox;
import module.AbstractModule;
import module.Modules;
import module.inout.IOEnum;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.NumericType;
import module.inout.TriggerInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import patch.GuiDomain;
import patch.GuiDomainImpl;
import pr.ModuleGUI;
import pr.ModuleGUIWrapper;
import pr.userinput.UserInput;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.statemachine.Data;
import util.statemachine.StateMachine;
import util.statemachine.StateMachine.State;
import util.statemachine.StateMachineAgent;
import util.statemachine.StateMachineEvent;
import util.statemachine.StateMachineG;
import util.statemachine.StateMachineListener;

@NodeInfo(type = NodeCategory.TOOLS)
public class Automata extends AbstractModule implements StateMachineListener{
	
	public static final String FILE_DEFAULT = "/util/statemachine/statemachine_default.json";
	
	private StateMachineAgent agent;
	private final List<NumberInputIF> triggers = new ArrayList<>();
	private final TriggerInputIF trig_reset;
	private final NumberOutputIF out_state;
	private Data stateMachineData;
	private IOEnum stateEnum;
	private GuiDomain guiDomain;
	
	
	public Automata() {
		trig_reset = addTrigger();
		trig_reset.setName("Reset");
		out_state = addOutput("State", INTEGER_POSITIVE);
	}

	@Override
	public ModuleGUI createGUI() {
		
		var t = new TextBox(getName(), true) {
			@Override
			public void mouseEventThis(InputEvent e, UserInput input) {
				if(e == CLICK) {
					if(guiDomain != null)
						guiDomain.getPApplet().requestFocus();
				}
			}
		};
		ModuleGUI.initTextBox(t);
		
		var w = new ModuleGUIWrapper(this, t) {
			@Override
			public void enterEditMode() {
				
				var sg = new StateMachineG(agent) {
					@Override
					public void removedFromDisplay() {
						stateMachineData = Data.buildAll(agent.getStateMachine(), this);
					}
				};
				sg.setWH(400, 400);
				sg.init();
				sg.onEdit(g -> stateMachineData = Data.buildAll(agent.getStateMachine(), g));
				
				if(stateMachineData != null) {
					stateMachineData.restoreGui(sg);
				}
				
				guiDomain = GuiDomainImpl.createFor(getDomain(), sg, getLabel());
				guiDomain.getPApplet().setWindowResizeListener(sg::resize);
			}
		};
		return w;
	}
	
	
	private void initBlankStateMachine() {
		var s = new StateMachine();
		agent = new StateMachineAgent(s);
		s.addListener(this);
		s.addListener(agent);
	}
	
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		initBlankStateMachine();
		try {
			var in = getClass().getResourceAsStream(FILE_DEFAULT);
			stateMachineData = new ObjectMapper().readValue(in, Data.class);
			stateMachineData.restore(agent.getStateMachine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void onStateMachineEvent(StateMachineEvent e, State s, String a) {
		switch (e) {
		case STATE_ADDED	-> addState(s.getName());
		case STATE_REMOVED	-> removeState(s.getName());	
		case ACTION_ADDED	-> addAction(a);				
		case ACTION_REMOVED	-> removeAction(a);			
		}
	}
	
	private void addState(String s) {
		var ns = agent.getStateMachine().getStates().stream().map(State::getName).toArray(String[]::new);
		var lab = getLabel();
		
		/*
		if(stateEnum == null) {
			var en = IOEnum.getEnumByName(lab);
			if(en == null) {
				stateEnum = IOEnum.createEnum(getLabel(), ns);
			}
		}
		stateEnum.add(s);
		*/
	}
	
	
	private void removeState(String s) {
		
	}
	
	
	private void addAction(String s) {
		if(s.length() > 0) {
			var in = Modules.addDynamicNumericInput(this, s, BANG);
			triggers.add((NumberInputIF) in);
		}
	}
	
	private void removeAction(String a) {
		
		for (int i = 0; i < triggers.size(); i++) {
			var t = triggers.get(i);
			if(t.getName().equals(a)) {
				triggers.remove(i);
				removeIO(t);
				break;
			}
		}
	}
	
	/*
	@Override
	public NodeInspector createGUISpecial() {
		return NodeInspector.newInstance(this, n -> {
			n.createSection("StateMachine");
			var sg = new StateMachineG(agent) {
				@Override
				public void removedFromDisplay() {
					stateMachineData = Data.buildAll(agent.getStateMachine(), this);
				}
			};
			sg.setWH(300, 300);
			sg.init();
			n.addInspectorElement(sg);
			
			if(stateMachineData != null) {
				stateMachineData.restoreGui(sg);
			}
		});
	}
	*/
	
	@Override
	public void processIO() {
		
		if(trig_reset.triggered()) {
			agent.toEntryState();
			State s = agent.getCurrentState();
			setOutState(s);
		}
		
		for (var t : triggers) {
			if(NumericType.toBool(t.getInput())) {
				var s = agent.doAction(t.getName());
				setOutState(s);
				break;
			}
		}
	}
	
	
	private void setOutState(State s) {
		int i = agent.getStateMachine().getStates().indexOf(s);
		out_state.setValue(i);
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc).addExtras(
			stateMachineData == null ? null : stateMachineData
		);
	}
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		stateMachineData = (Data) data.nextObject();
		
		if(stateMachineData != null) {
			
			if(rdc.getDomain().domainType() == PARTICLE_INSTANCE) {
				
				agent = new StateMachineAgent(stateMachineData.stateMachine);
				
				var as = stateMachineData.actions;
				for (int i = 0; i < as.length; i++) 
					addAction(as[i].name);
				
			}else {
				initBlankStateMachine();
				stateMachineData.restore(agent.getStateMachine());
			}
		}
		super.doDeserialize(data, rdc);
	}


}
