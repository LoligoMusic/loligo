package module.modules.math;

import lombok.var;
import module.AbstractModule;
import module.inout.DataType;
import module.inout.SwitchInputIF;
import module.inout.UniversalInputIF;
import module.inout.UniversalOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import processing.core.PMatrix2D;

@NodeInfo(type = NodeCategory.TOOLS)

public class Hold extends AbstractModule {
	
	private final SwitchInputIF in_switch;
	private final UniversalInputIF in;
	private final UniversalOutputIF out; 

	public Hold() {
		in_switch = addSwitch();
		in = addUniversalInput("In");
		out = addUniversalOutput("Out");
	}
	
	@Override
	public void processIO() {
		
		if(in_switch.getSwitchState()) {
			
			var v = in.getValueObject();
			var t = in.getDynamicType();
			
			out.setOutput(v, t);
			
		}
		
		if(in_switch.switchedOff() && in.getDynamicType() == DataType.TRANSFORM) {
			var m = (PMatrix2D) in.getValueObject();
			var m2 = new PMatrix2D(m);
			out.setOutput(m2, DataType.TRANSFORM);
		}
		
	}
	
	
}
