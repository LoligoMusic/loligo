package module.modules.math;

import constants.Flow;
import constants.InputEvent;
import constants.SlotEvent;
import constants.SlotEventListener;
import gui.ColorPreview;
import gui.selection.GuiType;
import module.AbstractModule;
import module.Module;
import module.inout.ColorInputIF;
import module.inout.ColorOutputIF;
import module.inout.Input;
import module.inout.Output;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.ModuleGUI;
import pr.ModuleGUIWrapper;
import pr.userinput.UserInput;

@NodeInfo(type = NodeCategory.COLOR)

public class Color extends AbstractModule implements SlotEventListener {

	private ColorInputIF   in = addColorInput();
	private ColorOutputIF out = addColorOutput();
	private int color;
	
	
	public Color() {
		
		flowFlags.add(Flow.PERMA);
	}
	
	@Override
	public ModuleGUI createGUI() {
		
		ColorPreview d = new ColorPreview(in::setColor, () -> color) {
			
			@Override
			public void mouseEventThis(InputEvent e, UserInput input) {
				
				if(in.getNumConnections() == 0) {
					
					super.mouseEventThis(e, input);
				}
			}
		};
		
		d.setColor(color);
		d.setGuiType(GuiType.SIGNAL);
		
		return new ModuleGUIWrapper(this, d);
	}
	
	public void insertColor(final int color) {
		
		this.color = color;
		out.setColor(this.color);
		if(gui != null)
			gui.setColor(color);
	}
	
	@Override
	public void processIO() {
		
		if(in.changed()) 
			insertColor(in.getColorInput());
	}

	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		
	}
	
	@Override
	public Module createInstance() {
		
		return new Color();
	}

}
