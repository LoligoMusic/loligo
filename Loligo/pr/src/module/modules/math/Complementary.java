package module.modules.math;

import module.AbstractModule;
import module.inout.ColorInputIF;
import module.inout.ColorOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import util.Colors;

@NodeInfo(type=NodeCategory.COLOR)
public class Complementary extends AbstractModule {

	private final ColorInputIF in;
	private final ColorOutputIF out;
	
	public Complementary() {
		
		in = addColorInput();
		out = addColorOutput();
	}


	@Override
	public void processIO() {
		
		int c = Colors.complementary(in.getColorInput());
		out.setColor(c);
	}

}
