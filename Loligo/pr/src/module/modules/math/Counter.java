package module.modules.math;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import lombok.Getter;
import lombok.Setter;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.TriggerInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import save.RestoreField;

@NodeInfo(type = NodeCategory.TOOLS)

public class Counter extends AbstractModule {

	@RestoreField
	private int numSteps = 3;
	
	@Getter@Setter
	private double count = 0;
	
	private final TriggerInputIF trigger, in_reset;
	private final NumberInputIF in_incr;
	private final NumberOutputIF out;
	
	
	public Counter() {
		trigger = addTrigger();
		in_incr = addInput("Increment", DEZIMAL_FREE).setDefaultValue(1);
		in_reset = addTrigger();
		in_reset.setName("Reset");
		
		out = addOutput("Count", DEZIMAL_FREE);
	}
	
	
	@Override
	public void processIO() {
		
		if(in_reset.triggered()) {
			count = 0;
			out.setValue(0);
		}else
		if(trigger.triggered()) {
			count += in_incr.getWildInput();
			out.setValue(count);
		}
	}

	public void setNumSteps(final int i) {
		
		numSteps = i;
		
	}
	
	public int getNumSteps() {
		 return numSteps;
	}
	

}
