package module.modules.math;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.codehaus.commons.compiler.CompileException;
import org.codehaus.janino.ExpressionEvaluator;

import constants.DisplayFlag;
import constants.Flow;
import constants.Timer;
import errors.LoligoException;
import gui.DisplayObjects;
import gui.nodeinspector.DynIONodeInspector;
import gui.nodeinspector.NodeInspector;
import gui.text.TextInputField;
import lombok.Getter;
import module.AbstractModule;
import module.Modules;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.NumberInOut;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.DisplayObject;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Colors;


@NodeInfo(type = NodeCategory.MATH)
public class Expression extends AbstractModule implements DynIOModule{

	
	private class Expr {
		
		ExpressionEvaluator evaluator;
		NumberOutputIF out;
		@Getter 
		String expr = DEFAULT_EXPR;
		TextInputField textField;
		boolean compileSuccess;
		
		Expr(String expr, String name) {
			
			setExpression(expr);
			out = addOutput(name, DEZIMAL_FREE);
			initIO(out, 0);
		}
		
		void createEvaluator() {
			evaluator = new ExpressionEvaluator();
			evaluator.setExpressionType(double.class);
			evaluator.setDefaultImports("static java.lang.Math.*");
		}
		
		void setExpression(final String expr) {
			
			final String old = this.expr;
			this.expr = expr;
			
			Expression.this.updateExprs();
			
			getDomain().getHistory().addAction(
					() -> setExpression(expr), 
					() -> setExpression(old));
		}
		
		void updateExpression() {
			createEvaluator();
			evaluator.setParameters(variableNames, variableTypes);
			
			try {
				evaluator.cook(expr);
				compileSuccess = true;
				evalSuccess();
			} catch (CompileException e) {
				evalFail(e);
				compileSuccess = false;
				//System.out.println(e.getMessage());
			}
		}
		
		void evaluate() {
			if(compileSuccess) {
				try {
					double r = (Double) evaluator.evaluate(variableValues);
					out.setValue(r);
					evalSuccess();
				} catch (InvocationTargetException | IllegalArgumentException e) {
					evalFail(e);
				}
			}
		}
		
		void evalSuccess() {
			
			if(textField != null) {
				
				textField.setColor(Colors.grey(180));
				DisplayObjects.removeChildren(textField);
			}
		}
		
		void evalFail(Exception e) {

			var g = getModuleGUI();
			if(g != null) {
				
				String s = e.getMessage();
				int col = s.indexOf(':', 15);
				String sl = s.substring(15, col);
				int line = Integer.parseInt(sl);
				line = Math.max(line - 1, 0);
				
				if(textField != null) {
					
					textField.setColor(COLOR_ERROR_TEXTFIELD);
					DisplayObjects.removeChildren(textField);
					
					textField.setText(expr);
					float x = textField.textWidth(line);
					String ms = s.substring(col + 1, s.length());
					
					var d = new DisplayObject();
					d.setPos(x, 16);
					d.setWH(7, 2);
					d.setFlag(DisplayFlag.RECT, true);
					d.setFlag(DisplayFlag.fixedToParent, true);
					textField.addChild(d);
					
					textField.setTextBlockConsumer(tb -> tb.setWH(textField.getWidth(), textField.getHeight()));
					textField.setMouseOverText(ms);
				}
			}
			
		}
	}
	
	
	private static final int COLOR_ERROR_TEXTFIELD = Colors.rgb(230, 160, 160);
	private static final String DEFAULT_EXPR = "a + b";
	private static final int INPUT_LIMIT = 15;
	
	private final List<Expr> exprs = new ArrayList<>();
	private final List<NumberInputIF> inputs = new ArrayList<>();
	private Object[] variableValues;
	private String[] variableNames;
	private Class<?>[] variableTypes;
	
	
	public Expression() {
		flowFlags.add(Flow.DYNAMIC_IO);
	}
	
	
	@Override
	public void defaultSettings() {
		
		addExprInput("a", 0);
		addExprInput("b", 0);
		
		super.defaultSettings();
		
		var e = new Expr(DEFAULT_EXPR, "Result");
		exprs.add(e);
		
		initVars();
		
	}
	
	@Override
	public void resetModule() {
		super.resetModule();
		Modules.removeDynamicInputs(this);
	}
	
	@Override
	public void addIO(Input in) {
		super.addIO(in);
		inputs.add((NumberInputIF) in);
	}
	
	
	@Override
	public void removeIO(Input in) {
		super.removeIO(in);
		inputs.remove(in);
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		
		var n = new DynIONodeInspector(this) {
			
			@Override
			public void postInit() {
				super.postInit();
				Timer.createFrameDelay(getDomain(), t -> Expression.this.updateExprs()).start();
			}
		};
		
		n.setSpecial(ni -> {
			
			ni.createSection("Expression");
			
			for (var e : exprs) {
				
				Consumer<String> c = s -> {
					e.setExpression(s);
					updateExprs();
					
					if(!getDomain().getSignalFlow().getFlowList().contains(this))
						processIO();
				};
				
				Supplier<String> sup = () -> {
					return e.getExpr();
				};
				
				var tf = new TextInputField(c, sup);
				ni.addInspectorElement(tf, 1);
				tf.update();
				e.textField = tf;
			}
		});
		
		n.init();
		
		return n;
	}
	
	private String generateVariableName() {
		String n;
		int c = 0;
		while(true) {
			int p = 'a' + (inputs.size() + c) % ('z' - 'a');
			n = Character.toString((char) p);
			if(!varNameExists(n))
				return n;
			c++;
		}
	}
	
	private boolean varNameExists(String n) {
		for (var in : inputs) {
			if(in.getName().equals(n)) {
				return true;
			}
		}
		return false;
	}
	
	private NumberInputIF addExprInput(String name, double v) {
		var io = addInput(name, DEZIMAL_FREE);
		initIO(io, v);
		return io;
	}
	
	private Expr addExpr(String expr, String name) {
		var e = new Expr(expr, name);
		exprs.add(e);
		return e;
	}
	
	private void initIO(NumberInOut io, double v) {
		io.setOptional();
		io.setValue(v);
		Modules.initIOGUI(Expression.this, io);
	}
	
	
	@Override
	public void postInit() {
		super.postInit();
		
		for (Expr e : exprs) {
			if(e.textField != null)
				e.textField.update();
		}
	}
	
	
	@Override
	public void processIO() {
		
		int n = inputs.size();
		
		if(n != variableValues.length) 
			initVars();
		
		for (int i = 0; i < n; i++) 
			variableValues[i] = inputs.get(i).getWildInput();
		
		exprs.forEach(Expr::evaluate);
	}

	
	private void initVars() {
		
		int n = inputs.size();
		variableValues = new Object[n];
		variableNames = new String[n];
		variableTypes = new Class[n];
		
		updateVarNames();
		updateExprs();
	}
	
	
	private void updateExprs() {
		
		boolean b = true;
		for (var e : exprs) {
			e.updateExpression();
			b &= e.compileSuccess;
		}
		
		if(gui != null) {
			if(b)
				gui.unmarkError();
			else
				gui.markError(new LoligoException(new Exception(), "Invalid expression"));
		}
	}

	
	private void updateVarNames() {
		for (int i = 0; i < inputs.size(); i++) {
			variableNames[i] = inputs.get(i).getName();
			variableTypes[i] = double.class;
		}
	}
	
	private void removeExpr(Expr e) {
		exprs.remove(e);
		removeIO(e.out);
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		String[] es = new String[exprs.size()];
		for (int i = 0; i < es.length; i++) 
			es[i] = exprs.get(i).expr;
		
		return super.doSerialize(sdc)
				.addExtras((Object) es);
	}
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
	
		for(var io : data.inputs)
			addExprInput(io.name, 0);
		
		super.doDeserialize(data, rdc);
		
		var es = (String[]) data.nextObject();
		initVars();
		
		for (int i = 0; i < es.length; i++) {
			String n = data.outputs.get(i).name;
			addExpr(es[i], n);
		}
	}


	@Override 
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		
		if(ioType == IOType.IN) {
			
			if(getInputs().size() >= INPUT_LIMIT)
				return null;
			String n = generateVariableName();
			var io = addExprInput(n, 0);
			initVars();
			return io;
		}else {
			
			var e = addExpr("", name);
			DynIOModule.super.rename(e.out, name);
			updateExprs();
			return e.out;
		}
	}
	
	
	@Override 
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		
		//RootClass.mainPatch().getHistory().execute( () -> {
		
		if(ioType == IOType.IN) {
		
			if(getInputs().size() <= 1)
				return null;
		
			InOutInterface io = null;
			
			for(Input in : inputs) {
				if(in.getName().equals(name)) {
					io = in;
					removeIO(in);
					break;
				}
			}
			initVars();
			return io;
		}else {
			
			var ex = exprs.stream()
				.filter(e -> name.equals(e.out.getName()))
				.findAny()
				.orElse(null);
			
			if(ex != null)
				removeExpr(ex);
			return ex.out;
		}
		//});
	}
	
	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.NUMBER_IN_OUT;
	}
	
	@Override
	public String rename(InOutInterface io, String name) {
		String n = DynIOModule.super.rename(io, name);
		Expression.this.updateVarNames();
		exprs.forEach(Expr::updateExpression);
		return n;
	}
}
