package module.modules.math;

import module.AbstractModule;
import module.inout.TransformInput;
import module.inout.TransformOutput;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import processing.core.PMatrix2D;

@NodeInfo(type = NodeCategory.TRANSFORM)
public class Inverse extends AbstractModule {

	private final TransformInput in_trans;
	private final TransformOutput out_trans;
	
	
	public Inverse() {
		in_trans = addTransformInput();
		out_trans = addTransformOutput();
	}
	
	
	@Override
	public void processIO() {
		
		if(in_trans.changed()) {
		
			PMatrix2D m = out_trans.getMatrix();
			m.set(in_trans.getMatrixInput());
			m.invert();
		}
		
	}
}
