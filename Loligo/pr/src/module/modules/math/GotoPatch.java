package module.modules.math;

import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import lombok.var;
import module.AbstractModule;
import module.inout.TriggerInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import patch.Domain;
import patch.LoligoPatch;
import pr.RootClass;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.TOOLS)
public class GotoPatch extends AbstractModule {

	private final TriggerInputIF trigger;
	private LoligoPatch patch;
	
	
	public GotoPatch() {
		trigger = addTrigger();
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		return NodeInspector.newInstance(this, n -> {

			n.createSection("");
			var nd = new DropDownMenu(this::setPatch, this::getPatchIndex, this::updatePatchList);
			n.addLabeledEntry("Patch", nd);
			nd.update();
		});
	}
	
	
	
	private int getPatchIndex() {
		return RootClass.backstage().getPatches().indexOf(patch);
	}
	
	private String[] updatePatchList() {
		return RootClass.backstage().getPatches().stream()
					    .map(LoligoPatch::getTitle)
					    .toArray(String[]::new);
	}
	
	private void setPatch(String title) {
		for(LoligoPatch p : RootClass.backstage().getPatches()) {
			if(p.getTitle().equals(title)) {
				patch = p;
				break;
			}
		}
	}
	
	private void setPatch(int idx) {
		var pp = RootClass.backstage().getPatches();
		if(idx < 0 || idx >= pp.size())
			return;
		patch = pp.get(idx);
	}
	
	public void setPatch(Domain d) {
		this.patch = (LoligoPatch) d;
	}
	
	public Domain getPatch() {
		return patch;
	}
	
	
	@Override
	public void processIO() {
		
		if(trigger.triggered() && patch != null) {
			
			RootClass.backstage().loadPatch(patch);
			
		}
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		String s = patch == null ? "" : patch.getTitle();
		return super.doSerialize(sdc)
				.addExtras(s);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		String t = data.nextString();
		setPatch(t);
	}

}
