package module.modules.math;

import static java.lang.Math.max;
import static module.inout.NumericAttribute.INTEGER_FREE;
import static module.inout.NumericAttribute.NOTE;
import static util.Notes.degreeInScale;

import gui.CheckBox;
import gui.nodeinspector.NodeInspector;
import gui.paths.EnumDropDownMenu;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Notes;


@NodeInfo(type = NodeCategory.TOOLS)
public class ToneScale extends AbstractModule {

	private final NumberInputIF in, in_tonic;
	private final NumberOutputIF out;
	
	private int tonic;
	@Getter@Setter
	private Notes.Scale scale = Notes.Scale.MAJOR;
	
	@Getter@Setter
	private boolean round = true;
	
	
	public ToneScale() {
		
		in_tonic = addInput("Tonic", NOTE).setDefaultValue(440);
		in = addInput("Degree", INTEGER_FREE);
		out = addOutput("Note", NOTE);
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		return NodeInspector.newInstance(this, n -> {

			var nf = new CheckBox(b -> round = b, () -> round);
			n.addLabeledEntry("Round", nf);
			
			var ed = new EnumDropDownMenu<Notes.Scale>("", this::setScale, this::getScale, Notes.Scale.MAJOR);
			n.addLabeledEntry("Scale", ed);
		});
	}
	
	
	@Override
	public void processIO() {
		
		if(in.changed() || in_tonic.changed()) {
			
			double tf = in_tonic.getWildInput();
			float n = (float) in.getWildInput();
			
			int i = (int)n;
			
			int[] sc = scale.scale;
			
			tonic = (int) Notes.freqToNote(tf);
			float p1 = degreeInScale(i, sc) + tonic;
			float p;
			
			if(round) {
				p = p1;
			}else {
				float p2 = degreeInScale(i + 1, sc) + tonic;
				p = p1 + (p2 - p1) * (n % 1);
			}
			p = Notes.toFrequency(p);
			out.setValue(max(p, 0));
		}
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.addExtras(scale.toString());
		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		String s = data.nextString();
		scale = Notes.Scale.valueOf(s);
	}
}
