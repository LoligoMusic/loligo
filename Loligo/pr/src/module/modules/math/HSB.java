package module.modules.math;

import java.awt.Color;

import module.AbstractModule;
import module.inout.ColorOutputIF;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.COLOR, helpPatch = "ColorConversion")

public class HSB extends AbstractModule {

	private final NumberInputIF in_hue, in_satur, in_bright, in_alpha;
	private final ColorOutputIF out_color;
	private float h = 0, s = 0, b = 0;
	private int a = 1;
	
	public HSB() {
		in_hue = addInput("Hue");
		in_satur = addInput("Saturation");
		in_bright = addInput("Brightness");
		in_alpha = addInput("Alpha");
		out_color = addColorOutput();
	}
	
	@Override
	public void processIO() {
		boolean boo = false;
		if(in_hue.changed()) {
			h = (float) in_hue.getInput();
			boo = true;
		}
		if(in_satur.changed()) {
			s = (float) in_satur.getInput();
			boo = true;
		}
		if(in_bright.changed()) {
			b = (float) in_bright.getInput();
			boo= true;
		}if(in_alpha.changed()) {
			a = (int) (in_alpha.getInput() * 255d);
			boo= true;
		}
		if(boo) 
			out_color.setColor(a << 24 | Color.HSBtoRGB(h, s, b) & 0xFFFFFF);
	}

}
