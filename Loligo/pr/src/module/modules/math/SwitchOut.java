package module.modules.math;

import java.util.ArrayList;
import java.util.List;

import constants.Flow;
import lombok.var;
import module.AbstractModule;
import module.Modules;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.dynio.DynIOModules;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.NumberInputIF;
import module.inout.UniversalInputIF;
import module.inout.UniversalOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import save.ModuleData;
import save.RestoreDataContext;

@NodeInfo(type = NodeCategory.TOOLS)

public class SwitchOut extends AbstractModule implements DynIOModule{
	
	private final NumberInputIF in_select; 
	private final UniversalInputIF in;
	private final List<UniversalOutputIF> outputs = new ArrayList<>();
	
	private UniversalOutputIF tempOut;
	
	
	public SwitchOut() {
		flowFlags.add(Flow.DYNAMIC_IO);
		in_select = addInput("Select");
		in = addUniversalInput("In");
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		createDynIO("Out1", DataType.UNIVERSAL, IOType.OUT);
		createDynIO("Out2", DataType.UNIVERSAL, IOType.OUT);
	}
	
	@Override
	public void resetModule() {
		super.resetModule();
		Modules.removeDynamicInputs(this);
	}
	
	
	@Override
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		var out = addUniversalOutput("");
		rename(out, name);
		out.setOptional();
		Modules.initIOGUI(this, out);
		outputs.add(out);
		tempOut = out;
		return out;
	}
	

	@Override
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		var out = Modules.getOutputByName(this, name);
		if(out == null)
			return null;
		removeIO(out);
		outputs.remove(out);
		return out;
	}

	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.UNIVERSAL_OUT;
	}
	
	
	@Override
	public void processIO() {
		int s = outputs.size();
		int i = (int)(s * in_select.getInput());
		i %= s;
		var o = outputs.get(i);
		if(o != tempOut){
			var t = in.getDynamicType();
			tempOut.setOutput(t.getDefaultValue(), t);
			tempOut = o;
		}
		tempOut.setOutput(in.getValueObject(), in.getDynamicType());
	}
	

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		DynIOModules.restoreDynOutputs(this, data, 0);
		super.doDeserialize(data, rdc);
	}

}
