package module.modules.math;

import static java.lang.Math.PI;
import static util.Images.loadPImage;

import constants.Flow;
import constants.Units;
import gui.Button;
import gui.NodeSelector2;
import gui.nodeinspector.NodeInspector;
import lombok.Getter;
import lombok.var;
import module.AbstractModule;
import module.Module;
import module.ModuleType;
import module.PositionableModule;
import module.inout.NumberOutputIF;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import processing.core.PImage;
import processing.core.PMatrix2D;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Colors;


public abstract class NodeRelation extends AbstractModule {

	private final static PImage IMG_SWAP = loadPImage("/resources/swap.png", Colors.WHITE);
	
	private final NumberOutputIF out;
	@Getter
	protected PositionableModule module1, module2;
	private PMatrix2D matrix1, matrix2;
	protected float x1, y1, x2, y2;
	
	public NodeRelation() {
		flowFlags.add(Flow.SOURCE);
		out = addOutput("Out", NumericAttribute.DEZIMAL_FREE);
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		
		return NodeInspector.newInstance(this, n -> {
			
			var sel1 = new NodeSelector2(this::setModule1, this::getModule1);
			sel1.setTypes(ModuleType.POSITIONABLES);
			
			var sel2 = new NodeSelector2(this::setModule2, this::getModule2);
			sel2.setTypes(ModuleType.POSITIONABLES);
			
			n.createSection("Link");
			n.addLabeledEntry("Node 1", sel1);
			n.addLabeledEntry("Node 2", sel2);
			
			Runnable sr = () -> {
				swapNodes();
				sel1.update();
				sel2.update();
			};
			var b = new Button(sr, "");
			b.setImage(IMG_SWAP);
			b.setWH(16, 16);
			b.setMouseOverText("Swap");
			n.addLabeledEntry("", b);
		});
	}
	
	
	public void swapNodes() {
		
		var m = module1;
		module1 = module2;
		module2 = m;
		
		var mx = matrix1;
		matrix1 = matrix2;
		matrix2 = mx;
		
		float xt = x1, yt = y1;
		x1 = x2;
		x2 = xt;
		y1 = y2;
		y2 = yt;
	}
	
	
	@Override
	public void processIO() {
		
		if(module1 == null || module2 == null) 
			return;
		
		double d = compute();
		
		out.setValue(d);
	}
	
	abstract double compute();
	
	public void setModule1(Module m) {
		module1 = (PositionableModule) m;
		matrix1 = module1.getDomain().getDisplayManager().getTransform().getMultipliedMatrix();
	}
	
	public void setModule2(Module m) {
		module2 = (PositionableModule) m;
		matrix2 = module2.getDomain().getDisplayManager().getTransform().getMultipliedMatrix();
	}
	
	protected void updatePositions() {
		
		float xm, ym;
		
		xm = module1.getX();
		ym = module1.getY();
		x1 = matrix1.multX(xm, ym);
		y1 = matrix1.multY(xm, ym);
		
		xm = module2.getX();
		ym = module2.getY();
		x2 = matrix2.multX(xm, ym);
		y2 = matrix2.multY(xm, ym);
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.addExtras(null, null);
		sdc.saveReference(md, module1, 0);
		sdc.saveReference(md, module2, 1);
		return md;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		rdc.restoreReference(data, this::setModule1);
		rdc.restoreReference(data, this::setModule2);
	}

	@NodeInfo(type = NodeCategory.TOOLS)
	public static class Distance extends NodeRelation {

		@Override
		double compute() {
			
			updatePositions();
			float dx = x1 - x2,
				  dy = y1 - y2;
			
			return Math.sqrt(dx * dx + dy * dy) / Units.LENGTH.normFactor;
		}
		
	}
	
	@NodeInfo(type = NodeCategory.TOOLS)
	public static class Angle extends NodeRelation {

		@Override
		double compute() {
			
			updatePositions();
			float dx = x1 - x2,
				  dy = y1 - y2;
				
			double d = Math.atan2(dy, dx);
			d = (d + PI) / (PI * 2) + .25; 
			d %= 1;
			return d;
		}
		
	}
}
