package module.modules.math;

import static module.inout.NumericAttribute.BOOL;
import static module.inout.NumericAttribute.INTEGER_POSITIVE;
import static module.inout.NumericAttribute.NORMALIZED;
import static module.inout.NumericType.toBool;

import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type=NodeCategory.TOOLS, helpPatch = "TimedState")

public class TimerFlop extends AbstractModule {

	private final NumberInputIF in_set, in_reset, in_time;
	private final NumberOutputIF out, out_prog;
	
	private int count, max;
	private boolean active;
	
	public TimerFlop() {
		
		in_set = addInput("Set", BOOL);
		
		in_time = addInput("Time", INTEGER_POSITIVE);
		in_reset = addInput("Reset", BOOL);
		
		in_time.setDefaultValue(30);
		
		out = addOutput("Out", BOOL);
		out_prog = addOutput("Progress", NORMALIZED);
	}
	
	@Override
	public void processIO() {
		
		active = toBool(in_set.getWildInput());
			
		if(!active || toBool(in_reset.getWildInput())) {
			
			reset();
			
		}else {
			
			max = (int) in_time.getWildInput();
			
			if(count >= max) {
				
				out.setValue(1);
				out_prog.setValue(1); 
			}else 
			if(getDomain().isProcessActive()){
			
				count++;
				
				out_prog.setValue((double) count / max); 
			}
		}
		
	}
	
	
	private void reset() {
		
		count = 0;
		active = false;
		out.setValue(0);
		out_prog.setValue(0);
	}

}
