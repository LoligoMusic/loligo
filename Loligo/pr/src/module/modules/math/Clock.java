package module.modules.math;

import constants.DisplayFlag;
import constants.Flow;
import gui.DropDownMenu;
import lombok.Getter;
import lombok.Setter;
import module.AbstractModule;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.NumericAttribute;
import module.inout.SwitchInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type = NodeCategory.TOOLS)

public class Clock extends AbstractModule {
	
	static final int FORWARD = 0, REVERSE = 1, PINGPONG = 2, LINEAR = 0, SINE = 1, SAW = 2;
	final private NumberOutputIF out_clock, out_phase, out_count;
	final private NumberInputIF in_speed, in_reset;
	final private SwitchInputIF in_switch;
	
	@Getter@Setter
	private double position = 0, iteration = 0;
	
	private double duration = 30;
	
	private int direction = FORWARD, modulation = LINEAR, frameCount = 0;
	private boolean play = true;
	
	private DropDownMenu directionMenu, modulationMenu;
	
	
	public Clock() {
		
		flowFlags.add(Flow.PERMA);
		
		out_phase = addOutput("Phase");
		out_count = addOutput("Count", NumericAttribute.INTEGER_FREE);
		out_clock = addOutput("Out", NumericAttribute.DEZIMAL_POSITIVE);
		in_switch = addSwitch();
		in_speed = addInput("Speed");
		in_reset = addInput("Reset", NumericAttribute.BANG);
		
	}

	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		reset();
	}
	
	/*
	@Override
	public void onAdd() {
	
		super.onAdd();
		getDomain().getDisplayManager().addEnterFrameListener(this);
	}
	
	@Override
	public void onDelete() {
	
		super.onDelete();
		getDomain().getDisplayManager().removeEnterFrameListener(this);
	}
	*/
	
	/*
	@Override
	public DisplayObject createGUI() {
		
		GUIBox gb = (GUIBox) super.createGUI();
		
		timer = new Button(null, null, Button.PRESSED) {
			
			boolean turn = false;
			int lastRound = 0;
			
			@Override public void render() {
				
				getDm().g.noStroke();
				getDm().g.fill(getColor());//+ turn ? -c1 : c1));
				getDm().g.ellipseMode(PConstants.CORNER);
				getDm().g.ellipse(getX(), getY(), getWidth(), getWidth());
				getDm().g.fill(100, 90);//; + (turn ? c1 : - c1));
				
				if(lastRound != iteration) {
					turn = !turn;
					lastRound = (int)iteration;
				}
				if(turn)
					getDm().g.arc(getX(), getY(), getWidth(), getHeight(), -PConstants.HALF_PI, PConstants.TWO_PI * (float)position - PConstants.HALF_PI);
				else
					getDm().g.arc(getX(), getY(), getWidth(), getHeight(),PConstants.TWO_PI * (float)(position) - PConstants.HALF_PI, PConstants.TWO_PI - PConstants.HALF_PI);
				
				getDm().g.ellipseMode(PConstants.CENTER);
				
			}
			
			@SuppressWarnings("incomplete-switch")
			@Override public void mouseEvent(InputEvent e, UserInput input) {
				
				if(input.getOverTarget() == this) {
					
					switch(e) {
					case PRESSED:
						play(!play);
						break;
					case PRESSED_RIGHT:
						frameCount = 0;
					}
				}
			}
		};
		
		timer.setColor(RootClass.mainProc().color(20));
		timer.setFlag(DisplayFlag.RECT, true);
		timerSize = 30;
		timer.setWH((int) timerSize, (int) timerSize);
		
		//MouseEventContext.instance.addListener(timer);
		
		
		directionMenu = new DropDownMenu("Direction", this::setDirection, ()->{return direction;}, "Forward", "Reverse", "Ping-Pong");
		directionMenu.setSelected(0);
		//box.align(GUIBox.CENTER);
		//gb.setTop(3);
		gb.addRow(timer);
		//DisplayObject.setDimensions(60, 13);
		//DisplayObject.fill(PROC.color(100));
		//Slider sl = new Slider(this, "Speed");
		//sl.vertical = false;
		//gb.addRow(sl);
		modulationMenu = new DropDownMenu("Modulation", this::setModulation, ()->{return modulation;}, "Linear", "Sine", "Step");
		//updateSender(null);
		//modulationMenu.setSelected(0);
		gb.addRow(modulationMenu);
		gb.addRow(directionMenu);
		
		modulationMenu.update();
		directionMenu.update();
		
		//box.toggleSize.addTarget(this, "toggleSize", Button.CLICK);
		
		timerX = timer.getPosObject().x += 15;
		timerY = timer.getPosObject().y;
		
		return gb;
	}
	*/
	
	public void step() {
		
		if(frameCount >= duration ) {
			frameCount = 0;
			iteration++;
			//if(modulation == LINEAR)
			//	turn = !turn;
		}
		position = frameCount / duration;
		//if(play)
			frameCount++;
		
		if(direction != FORWARD)
			//if(direction == REVERSE)
				position = 1 - position;
			//else
			//	position = turn ? position : 1 - position;
		
		if(modulation != LINEAR){
			if(modulation == SINE)
				position = (Math.sin(position * Math.PI * 2 - 1.57) + 1) * .5;
			else
				position = position > .5 ? 1 : 0;
		}
	}
	
	@Override
	public void processIO() {
		
		if(in_switch.switchedOn()) 
			play(true);
		else if(in_switch.switchedOff())
			play(false);
		
		if(in_speed.changed()) {
			float s = 1 - (float)in_speed.getInput();
			//play = s < 1;
			
			duration = 4 + Math.floor(s * s * 300);
			frameCount = (int)(position * duration) + 1;
		}
		
		if(in_reset.getValue() > .5) {
			
			reset();
		}
		
		if(play && getDomain().isProcessActive()) {
			
			
			
			out_clock.setValue(position + iteration);
			out_phase.setValue(position);
			out_count.setValue(iteration);
			
			step();
		}	
		
		
	}
	
	public void play(boolean b) {
		
		if(play != b) {
			play = b;
			
			in_switch.setValue(b ? 1 : 0);
			
		}
	}
	
	public void reset() {
		
		position = iteration = frameCount = 0;
	}

	public void setModulation(final int i) {
		
		modulation = i;
		
		if(modulationMenu != null)
			modulationMenu.update();
		
		if(directionMenu != null) {
			
			if(modulation == LINEAR)
				directionMenu.setFlag(DisplayFlag.DRAWABLE, true);
			else
				directionMenu.setFlag(DisplayFlag.DRAWABLE, false);
		}
		
	}
	
	public void setDirection(final int i) {
		
		direction = i;
		if(directionMenu != null)
			directionMenu.update();
	}
	
	
}
