package module.modules.math;

import module.AbstractModule;
import module.Module;
import module.inout.ColorInputIF;
import module.inout.ColorOutputIF;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo (name = "Alpha", type = NodeCategory.COLOR)

public class Alpha extends AbstractModule {

	private final ColorInputIF in_color;
	private final ColorOutputIF out_color;
	private final NumberInputIF in_alpha;
	private int alpha = 255, rgb = 0xffffffff;
	
	
	public Alpha() {
		in_color = addColorInput();
		in_alpha = addInput("Alpha");
		out_color = addColorOutput();
	}
	
	
	@Override
	public void processIO() {
		
		boolean b = false;
		
		if(in_alpha.changed()) {
			alpha = (int)(in_alpha.getInput() * 255);
			b = true;
		}
		
		if(in_color.changed()) {
			rgb = in_color.getColorInput();
			b = true;
		}
		
		if(b)
			out_color.setColor(alpha << 24 | rgb & 0xFFFFFF);
	}
	
	
	@Override
	public Module createInstance() {
		return new Alpha();
	}

}
