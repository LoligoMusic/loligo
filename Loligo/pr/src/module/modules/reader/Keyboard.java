package module.modules.reader;

import constants.Flow;
import constants.InputEvent;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.inout.NumericAttribute;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.EventSubscriber;
import pr.userinput.UserInput;

@NodeInfo(type = NodeCategory.READER)

public class Keyboard extends AbstractModule implements EventSubscriber {

	public final static String chars = "1234567890qwertzuiop�asdfghjkl��yxcvbnm";
	
	private final NumberOutputIF out, out_trigger;
	private int nextNote = 0;
	private int trigger;
	
	
	public Keyboard() {
		
		flowFlags.add(Flow.SOURCE);
		
		out_trigger = addOutput("Hit", NumericAttribute.BOOL);
		out = addOutput("Freq", NumericAttribute.INTEGER_POSITIVE);
	}
	
	
	@Override
	public void processIO() {
		
		out_trigger.setValue(0);
		
		if(trigger > 0) {
			
			out.setValue(nextNote);
			out_trigger.setValue(1);
			
			trigger--;
		}
	}

	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		if(e == InputEvent.KEY) {
			
			int i = chars.indexOf(input.getLastChar());
			if(i >= 0) {
				
				nextNote = i;
				trigger++;
			}
		}
	}

	
	@Override
	public void onAdd() {
	
		super.onAdd();
		getDomain().getDisplayManager().getInput().addInputEventListener(this);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
	
		super.onDelete(mode);
		getDomain().getDisplayManager().getInput().removeInputEventListener(this);
	}
}
