package module.modules.reader;

import constants.Flow;
import module.AbstractModule;
import module.inout.NumericAttribute;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.RootClass;
import pr.userinput.UserInput;

@NodeInfo(type=NodeCategory.IO)
public class Mouse extends AbstractModule {
	
	private final NumberOutputIF out_x, out_y, out_clickRight, out_clickLeft;
	
	public Mouse() {
		flowFlags.add(Flow.SOURCE);
		out_x = addOutput("X");
		out_y = addOutput("Y");
		out_clickRight = addOutput("Right Click", NumericAttribute.BOOL);
		out_clickLeft = addOutput("Left Click", NumericAttribute.BOOL);
	}
	
	
	@Override
	public void processIO() {
		
		final UserInput input = RootClass.mainDm().getInput();
		
		out_x.setValue((double) input.getMouseX() / RootClass.mainProc().width);
		out_y.setValue((double) input.getMouseY() / RootClass.mainProc().height);
		
		out_clickLeft.setValue(input.leftDown  ? 1 : 0);
		out_clickRight.setValue(input.rightDown ? 1 : 0);
	}
	
}
