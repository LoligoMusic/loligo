package module.modules.reader;

import static module.inout.NumericAttribute.DEZIMAL_FREE;
import static processing.core.PConstants.PI;
import static processing.core.PConstants.TWO_PI;
import static util.Colors.blue;
import static util.Colors.green;
import static util.Colors.red;

import constants.DisplayFlag;
import constants.Flow;
import constants.Units;
import gui.DragHandle;
import gui.GUIBox;
import gui.selection.GuiType;
import module.AbstractPositionableModule;
import module.Readable;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.ModuleGUI;
import pr.ModuleGUIWrapper;
import pr.RootClass;
import pr.Transform;
import processing.core.PApplet;
import processing.core.PGraphics;
import util.Colors;

@NodeInfo(type = NodeCategory.READER)

public class Antenna extends AbstractPositionableModule implements Readable {
	
	private static final double NORM_LENGTH = Units.LENGTH.normFactor * 2;
	private static int SCANLINECOLOR = 0x88c8143c;
	
	NumberInputIF in_rad, in_radius;
	NumberOutputIF out_length;
	
	private float angle = 0, length = 0, maxLength, tx, ty, px = 1, py = 0, tolerance = 10, precision = 1;
	
	private boolean hasTarget;
	private Transform transform;
	
	public Antenna() {
		
		flowFlags.add(Flow.READER);
		flowFlags.add(Flow.SOURCE);
		flowFlags.add(Flow.PERMA);
		flowFlags.add(Flow.ROUNDSLOTS);
		
		in_radius = addInput("Radius", DEZIMAL_FREE).setDefaultValue(1);
		in_rad = addInput("Angle", DEZIMAL_FREE);
		out_length = addOutput("Length");
	}
	
	@Override
	public void postInit() {
		super.postInit();
		
		transform = getDomain().getDisplayManager().getTransform();
	}
	
	@Override
	public ModuleGUI createGUI() {
		
		var h = new DragHandle(this, new GUIBox(this)) {
			
			@Override public void render(PGraphics g) {
				
				super.render(g);
				
				if(hasTarget) {
					
					g.strokeWeight(2);
					g.stroke(getColor());
					g.line(getX(), getY(), tx, ty);
				}else{
					
					g.strokeWeight(1);
					g.stroke(SCANLINECOLOR);
					g.line(getX(), getY(), getX() + (px / precision) * maxLength, getY() + (py / precision) * maxLength);
				}
				g.noStroke();
			}
		};
		
		h.setWH(16, 16);
		h.setFlag(DisplayFlag.snapsToPath, true);
		h.setGuiType(GuiType.READER);
		ModuleGUIWrapper w = new ModuleGUIWrapper(this, h);
		
		return w;
	}
	
	@Override
	public void read() {
		
		var m = transform.getMultipliedMatrix();
		float x = getX(),
			  y = getY();
	    tx = m.multX(x, y);
	    ty = m.multY(x, y);
		
		int c1 = RootClass.mainProc().get((int)tx, (int)ty), 
			c2;
		
		float f, curLen = 0, intersectLen = 0;
		boolean _hitsScreen;
		
		while(curLen < maxLength) {
			
			c2 = nextPos();
			
			int d = Colors.difference(c1, c2);
			f = (red(d) + green(d) + blue(d)) / 3f;
			
			_hitsScreen = hitsScreen();
			if(_hitsScreen)
				break;

			if(f > tolerance) { //step back + measure more precisely
				
				hasTarget = !_hitsScreen && curLen <= maxLength;
				//if(!hasTarget)
				//	curLen = 0;

				//break;
				intersectLen += precision;

			}

			curLen += precision;

		}
		
		length = intersectLen;
			
	}

	private boolean hitsScreen() {
		boolean _hitsScreen;
		_hitsScreen = tx < 0 || tx > RootClass.mainProc().width || ty < 0 || ty > RootClass.mainProc().height;
		return _hitsScreen;
	}

	private int nextPos() {
		int c2;
		tx += px;
		ty += py;

		c2 = RootClass.mainProc().get(PApplet.round(tx), PApplet.round(ty));
		return c2;
	}
	
	@Override
	public void processIO() {
		
		if(in_rad.changed()) {
			
			angle = PI - (float)in_rad.getInput() * TWO_PI;
			px = PApplet.sin(angle) * precision;
			py = PApplet.cos(angle) * precision;
		}
		
		if(in_radius.changed()) {
			maxLength = (float) (in_radius.getWildInput() * NORM_LENGTH);
			if(maxLength <= 0)
				maxLength = Float.MIN_VALUE;
		}
		
		out_length.setValue(length / NORM_LENGTH);
		
	}
	
	
}
