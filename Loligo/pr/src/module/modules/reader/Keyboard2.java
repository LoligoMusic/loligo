package module.modules.reader;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.jogamp.newt.event.KeyEvent;

import constants.DisplayFlag;
import constants.Flow;
import constants.InputEvent;
import gui.Button;
import gui.nodeinspector.DynIONodeInspector;
import gui.nodeinspector.NodeInspector;
import gui.text.Fonts;
import gui.text.TextBox;
import gui.text.TextBlock.AlignMode;
import lombok.var;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.Modules;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.EventSubscriber;
import pr.userinput.UserInput;
import save.IOData;
import save.ModuleData;
import save.RestoreDataContext;


@NodeInfo(type = NodeCategory.READER)
public class Keyboard2 extends AbstractModule implements DynIOModule, EventSubscriber{
	
	private final static Map<Short, String> keyMap = new HashMap<>();
	
	
	private static void put(String c) {
		try {
			Field f = KeyEvent.class.getDeclaredField("VK_" + c.toUpperCase());
			short ke = f.getShort(null);
			keyMap.put(ke, c);
		} catch (IllegalAccessException | NoSuchFieldException | SecurityException ex) {
			System.err.println(ex);
		}
	}
	
	private static void put(char s, char e) {
		for(char i = s; i <= e; i++) {
			String c = String.valueOf(i);
			put(c);
		}
	}
	
	private static void buildKeyMap()
	{
		put('a', 'z');
		put('0', '9');
		
		for (int i = 1; i <= 12; i++) 
			put("F" + i);
		
		keyMap.put(KeyEvent.VK_ENTER		, "enter"	);
		keyMap.put(KeyEvent.VK_ESCAPE		, "escape"	);
		keyMap.put(KeyEvent.VK_ALT			, "tab"		);
		keyMap.put(KeyEvent.VK_BACK_SPACE	, "backspace");
		keyMap.put(KeyEvent.VK_ALT			, "alt"		);
		keyMap.put(KeyEvent.VK_CONTROL		, "ctrl"	);
		keyMap.put(KeyEvent.VK_CONTROL		, "control"	);
		keyMap.put((short) 16				, "shift"	);
		keyMap.put(KeyEvent.VK_SPACE		, "space"	);
		keyMap.put((short) 37				, "left"	);
		keyMap.put((short) 39				, "right"	);
		keyMap.put((short) 38				, "up"		);
		keyMap.put((short) 40				, "down"	);
		
	}
	
	private boolean captureKey = false;
	
	public Keyboard2() {
		if(keyMap.isEmpty())
			buildKeyMap();
		
		flowFlags.add(Flow.DYNAMIC_IO);
		
	}
	
	@Override
	public NodeInspector createGUISpecial() {

		NodeInspector ni = new DynIONodeInspector(this) {
			
			TextBox gb;
			Button autoKeyButton;
			
			@Override
			public Button createButton(DataType t, IOType iot) {
				autoKeyButton = new Button(this::autoKey, "");
				autoKeyButton.setImage(DynIONodeInspector.ICON_ADD_NUMBER, DynIONodeInspector.ICON_ADD_NUMBER_OVER);
				autoKeyButton.setMouseOverText("Add key");
				return autoKeyButton;
			}
			
			@Override
			public void postInit() {
				if(captureKey)
					autoKey();
			}
			
			private void autoKey() {
				
				if(gb == null) {
					gb = new TextBox("Press key..", true) {
						public void mouseEventThis(InputEvent e, UserInput input) {
							if(e == InputEvent.FOCUS_LOST) 
								endKey();
						};
					};
					gb.bgBox = true;
					gb.setColor(0x1022ff66);
					gb.setFont(Fonts.DEFAULT_DEMI_14);
					gb.setTextColor(0xcaffffff);
					gb.setWH(getContentWidth(), 30);
					gb.setAlignMode(AlignMode.CENTER);
				}
				
				if(!gb.checkFlag(DisplayFlag.onDisplay)) {
					addInspectorElement(gb);
					getDm().getInput().setFocusTarget(gb);
				}
				
				autoKeyButton.disable(true);
				captureKey = true;
			}
			
			private void endKey() {
				captureKey = false;
				if(gb != null)
					getDm().getInput().removeRemovableTarget(gb);
				autoKeyButton.disable(false);
				update();
			}
			
			@Override
			public void addedToDisplay() {
				if(!Modules.hasOutputs(Keyboard2.this)) 
					autoKey();
			}
			
			@Override
			public void removedFromDisplay() {
				endKey();
			}
		};
		
		ni.init();
		
		return ni;
	}
	
	@Override
	public void processIO() {


	}
	
	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		boolean k = e == InputEvent.KEY;
		int c = input.getLastKeyCode();
		var outs = getOutputs();
		
		if(k || e == InputEvent.KEY_RELEASED) {
			double v = k ? 1 : 0;
			String n = keyMap.get((short) c);
			
			if(n != null && outs != null) {
				String no = n.toLowerCase();
				outs.stream()
					.filter(o -> no.equals(o.getName()))
					.findAny()
					.ifPresent(o -> o.setValueObject(v));
			}
		}
		
		if(k && captureKey) {
			
			String n = keyMap.get((short) c);
			if(n != null) {
				
				boolean b = outs == null || outs.stream().noneMatch(o -> n.equals(o.getName()));
				if(b) {
					createDynIO(n, null, null);
					NodeInspector.updateFor(this);
				}
			}
		}
	}

	
	@Override
	public void onAdd() {
		super.onAdd();
		getDomain().getDisplayManager().getInput().addInputEventListener(this);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		getDomain().getDisplayManager().getInput().removeInputEventListener(this);
	}

	@Override 
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		var io = Modules.addDynamicNumericOutput(this, "", NumericAttribute.BOOL);
		rename(io, name);
		return io;
	}
	
	@Override 
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		var io = Modules.getOutputByName(this, name);
		removeIO(io);
		return io;
	}

	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.NUMBER_OUT;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		
		if(data.outputs != null) {
			for(IOData d : data.outputs) {
				createDynIO(d.name, null, null);
			}
		}
	}

}
