package module.modules.reader;

import java.util.ArrayList;

import constants.Flow;
import module.ModuleDeleteMode;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.AbstractMidiObj;
import themidibus.MidiBus;
import themidibus.SimpleMidiListener;

@NodeInfo(type = NodeCategory.IO)

public class MidiIn extends AbstractMidiObj implements SimpleMidiListener {
	 
	protected static ArrayList<MidiIn> ins = new ArrayList<MidiIn>();
	private final NumberOutputIF out_pitch, out_velo, out_cont;
	
	
	public MidiIn() {
		
		flowFlags.add(Flow.SOURCE);
		
		midiBus.addMidiListener(this);
		
		//out_hit = addOutput("Hit", NumberAttributes.BOOL);
		out_pitch = addOutput("Pitch");
		out_velo = addOutput("Velocity");
		out_cont = addOutput("Controller");
	}
	
	@Override
	public String[] availableDevices() {
	
		return MidiBus.availableInputs();
	}
	
	@Override
	public void setDevice(final int num) {
		
		removeDevice(ins);
		device = availableDevices()[num];
		midiBus.addInput(num);
	}
	
	@Override
	public void processIO() {
		
		out_pitch.setValue((pitch - 21d) / 88d);
		out_velo.setValue(velocity / 128d);
		out_cont.setValue(controller / 128d);
	}
	
	@Override
	public void controllerChange(int ch, int contr, int value) {
		
		if( (ch == channel || channel == -1) && contr == controllerID)
			controller = value;
	}
	
	@Override
	public void noteOff(int ch, int pit, int vel) {
		
		if(ch == channel || channel == -1)
			velocity = 0;
	}

	@Override
	public void noteOn(int ch, int pit, int vel) {
		
		if(ch == channel || channel == ALL) {
			
			velocity = vel;
			pitch = pit;
		}
	}

	@Override
	public void onAdd() {
	
		super.onAdd();
		
		ins.add(this);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
	
		ins.remove(this);
		
		super.onDelete(mode);
	}
	
}
