package module.modules.reader;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import constants.Flow;
import errors.LoligoException;
import gui.nodeinspector.NodeInspector;
import gui.text.CharProfile;
import gui.text.NumberInputField;
import gui.text.TextInputField;
import lombok.Getter;
import lombok.var;
import module.Modules;
import module.dynio.DynIOInfo;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.TriggerInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.osc.AbstractOscModule;
import pr.osc.OscManager;
import pr.osc.POut;
import save.IOData;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.IO, name = "OSC-Out")

public class OscOut extends AbstractOscModule {

	static OscManager manager = new OscManager();
	
	//private final NumberInputIF in_arg;
	private final TriggerInputIF trigger;
	
	
	@Getter
	private String ip;
	
	private InetAddress address;
	
	private POut oscOut;
	
	private boolean adressValid;
	private TextInputField ipTextField;
	
	private List<Object> values = new ArrayList<>();
	
	public OscOut() {
		
		flowFlags.add(Flow.SINK);
		trigger = addTrigger();
	}
	
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		setIp("localhost");
		createDynIO("Number1", DataType.Number, IOType.IN);
	}
	
	@Override
	public void resetModule() {
		super.resetModule();
		Modules.removeDynamicInputs(this);
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
	
		NodeInspector ni = NodeInspector.newInstance(this, n -> {
			
			n.createSection("Connection");
			
			ipTextField = new TextInputField(this::setIp, this::getIp);
			n.addLabeledEntry("Address", ipTextField);
			
			NumberInputField p = new NumberInputField(d -> setPort(d.intValue()), () -> (double) port);
			p.setWH(n.getWidth(), p.getHeight());
			p.setCharProfile(CharProfile.INT);
			n.addLabeledEntry("Port", p);
			
			TextInputField pa = new TextInputField(this::setOscPath, this::getOscPath);
			//ipTextField.setWH(200, ipTextField.getHeight());
			//pa.setWH(200, pa.getHeight());
			n.addLabeledEntry("Path", pa);
		});
		
		return ni;
	}

	public void setIp(String s) {
		
		ip = s;
		updateAddress();
	}
	
	public void setPort(int p) {
		
		port = p;
		updateAddress();
	}
	
	private void updateAddress() {
		
		try {
			address = InetAddress.getByName(ip);
			setAdressValid(true);
			
		} catch (UnknownHostException e) {
			
			LoligoException ex = new LoligoException(e, "Unknown host: " + ip);
			getDomain().getErrorHandler().dispatchErrorEvent(this, ex);
			
			setAdressValid(false);
		}
		
		try {
			oscOut = manager.findOut(address, port);
			
		} catch (LoligoException e) {
			
			getDomain().getErrorHandler().dispatchErrorEvent(this, e);
		}
	}
	
	
	public void setAdressValid(boolean v) {
		
		adressValid = v;
		
		if(ipTextField != null) {
			ipTextField.setColor(adressValid ? TextInputField.COLOR_ACTIVE : 0xffaa0909);
		}
	}
	
	
	@Override
	public void processIO() {
		
		if(trigger.triggered()) {
			
			var ins = getInputs();
			for (int i = 1; i < ins.size(); i++) {
				var in = ins.get(i);
				in.updateValue();
				values.set(i - 1, in.getValueObject());
 			}
			
			try {
				
				if(oscOut != null)
					oscOut.send(values, oscPath);
				
			} catch (Exception e) {
				
				System.out.println(e);
			}
		}
	}

	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
				.addExtras(ip, port, oscPath);
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
	
		super.doDeserialize(data, rdc);
		
		var ins = data.getInputs();
		for (int i = 1; i < ins.size(); i++) {
			IOData id = ins.get(i); 
			createDynIO(id.name, DataType.Number, IOType.IN);
		}
		
		ip 		= data.nextString();
		port 	= data.nextInt();
		oscPath = data.nextString();
		
		updateAddress();
	}
	
	
	@Override
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		var io = Modules.addDynamicInput(this, "", DataType.Number);
		rename(io, name);
		values.add(io.getValueObject());
		return io;
	}
	
	
	@Override
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		var io = Modules.getInputByName(this, name);
		removeIO(io);
		values.remove(values.size() - 1);
		return io;
	}
	
	
	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.NUMBER_IN;
	}
		
}
