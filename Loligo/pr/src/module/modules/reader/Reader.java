package module.modules.reader;

import constants.DisplayFlag;
import constants.Flow;
import gui.DragHandle;
import gui.GUIBox;
import gui.selection.GuiType;
import module.AbstractPositionableModule;
import module.Module;
import module.Readable;
import module.inout.ColorOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import patch.Loligo;
import pr.ModuleGUI;
import pr.ModuleGUIWrapper;
import pr.RootClass;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PMatrix2D;

@NodeInfo(type = NodeCategory.READER)

public class Reader extends AbstractPositionableModule implements Readable {
	
	public int readColor, readColorTemp = -1, red, green, blue; 
	public int readWidth = 10, readHeight = 10; 
	transient private PImage readImage;
	private final ColorOutputIF out_color;
	
	
	public Reader(){
		
		flowFlags.add(Flow.SOURCE);
		flowFlags.add(Flow.READER);
		flowFlags.add(Flow.ROUNDSLOTS);
		
		out_color = addColorOutput();
		
	}
	
	@Override
	public ModuleGUI createGUI() {
		
		DragHandle g = new DragHandle(this, new GUIBox(this)) {
			
			@Override public void render(PGraphics g) {
				
				g.fill(getColor());
				g.ellipse(getX(), getY(), getWidth(), getWidth());
				g.fill(readColor);
				g.noStroke();
				g.ellipse(getX(), getY(), 8, 8);
			}
			
			@Override
			public void setSelected(boolean b) {
				
				defaultSetSelected(b);
			}
		};
		g.setGuiType(GuiType.READER);
		g.setColor(RootClass.mainProc().color(0, 20, 150));
		g.setWH(20, 20);
		g.setFlag(DisplayFlag.snapsToPath, true);
		
		ModuleGUIWrapper w = new ModuleGUIWrapper(this, g);
		
		return w;
	}
	
	@Override
	public void read() {
		
		PMatrix2D m = getDomain().getDisplayManager().getTransform().getMultipliedMatrix();
		float x = getX(),
			  y = getY(),
			  xo = m.multX(x, y),
			  yo = m.multY(x, y);
		
		if(readWidth == 1 && readHeight == 1){
			
			readColor = RootClass.mainProc().get((int)xo, (int)yo);
			red = (readColor >> 16) & 0xFF;  
			green = (readColor >> 8) & 0xFF;   
			blue = readColor & 0xFF;
			
		}else{
			
			float r = 0, g = 0, b = 0;
			
			Loligo proc = RootClass.mainProc();
			
			if((xo < proc.width && xo > 0) && (yo < proc.height && yo > 0)) {
				
				readImage = proc.get((int)xo - (readWidth >> 1), 
									 (int)yo - (readHeight >> 1), 
									 readWidth, readHeight);
				
				readImage.loadPixels();
				
				for (int rgb : readImage.pixels) {
					
					r += (rgb >> 16) & 0xFF;  
					g += (rgb >> 8) & 0xFF;   
					b += rgb & 0xFF;
				}
				
				red = (int)(r / readImage.pixels.length);
				green = (int)(g / readImage.pixels.length);
				blue = (int)(b / readImage.pixels.length);
				readColor = 255 << 24 | red << 16 | green << 8 | blue;
				
			}else
				readColor = proc.color(0);
		}
	}
	
	@Override
	public void processIO() {
		
		if(readColor != readColorTemp) {
			
			readColorTemp = readColor;
			out_color.setColor(readColor);
		}
	}
	
	/*
	@Override
	public void onSlotConnect(AbstractSlot s1, AbstractSlot s2) {
		for(OutSlot s : outSlots)
			s.setOutput(0);
	}
	*/
	
	@Override
	public Module createInstance() {
		return new Reader();
	}

	
}

