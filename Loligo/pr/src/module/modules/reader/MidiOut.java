package module.modules.reader;

import static constants.SlotEvent.SLOT_DISCONNECT;

import java.util.ArrayList;

import javax.sound.midi.ShortMessage;

import constants.Flow;
import constants.SlotEvent;
import constants.SlotEventListener;
import module.ModuleDeleteMode;
import module.inout.Input;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.Output;
import module.inout.SwitchInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.AbstractMidiObj;
import themidibus.MidiBus;

@NodeInfo(type = NodeCategory.IO)

public class MidiOut extends AbstractMidiObj implements SlotEventListener{
	
	protected static ArrayList<MidiOut> outs = new ArrayList<MidiOut>();
	private final NumberInputIF in_pitch, in_velo, in_cont;
	private final SwitchInputIF in_midiOnOff;
	
	private int lastPitch, lastVelocity;
	
	public MidiOut() {
		
		flowFlags.add(Flow.SINK);
		
		in_midiOnOff = addSwitch();
		
		in_pitch = addInput("Pitch").setDefaultValue(.2);
		in_velo = addInput("Velocity").setDefaultValue(.5);
		in_cont = addInput("Instrument", NumericAttribute.INTEGER_7BIT);
	}
	
	@Override
	public void setDevice(final int num) {
		
		removeDevice(outs);
		device = availableDevices()[num];
		midiBus.addOutput(num);
	}
	
	
	@Override
	public String[] availableDevices() {
		
		return MidiBus.availableOutputs();
	}
	
	@Override
	public void processIO() {
		
		if(in_cont.changed()) {
			
			controller = (int) in_cont.getWildInput();
			//midiBus.sendControllerChange(channelID, controllerID, controller);
			midiBus.sendMessage(ShortMessage.PROGRAM_CHANGE, channelID, controller, 0);
		}
		
		if(in_velo.changed()) {
			
			velocity = (int)(in_velo.getInput() * 127);
		}
		
		if(in_pitch.changed()) {
			
			pitch = (int)(in_pitch.getInput() * 88 + 21);
			
		}
		
		if(in_midiOnOff.switchedOn()) {
			
			midiBus.sendNoteOn(channelID, pitch, velocity);
			lastPitch = pitch;
			lastVelocity = velocity;
			
		}else 
		if(in_midiOnOff.switchedOff()) {
			
			midiBus.sendNoteOff(channelID, lastPitch, lastVelocity);
		}
	}
	
	@Override
	public void slotEvent(SlotEvent e, Input in, Output out) {
		
		if(e == SLOT_DISCONNECT && in == in_pitch) 
			
			midiBus.sendNoteOff(channelID, pitch, velocity);
		
	}
	
	@Override
	public void onAdd() {
	
		super.onAdd();
		
		outs.add(this);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
	
		outs.remove(this);
		
		super.onDelete(mode);
	}

	
}

