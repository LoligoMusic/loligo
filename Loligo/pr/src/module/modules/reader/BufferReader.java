package module.modules.reader;

import constants.Flow;
import gui.Position;
import module.AbstractPositionableModule;
import module.BufferInterpolated;
import module.Module;
import module.Readable;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.data.BufferFactory;
import pr.RootClass;
import processing.core.PGraphics;
import processing.core.PImage;


public class BufferReader extends AbstractPositionableModule implements Readable {

	private static int idCount = 0;
	
	Buffer fac;
	private float[] raw;
	private int ilen = 12; 
	private final int id;
	
	private Position pos = new Position();
	
	
	public BufferReader() {
	
		flowFlags.add(Flow.SINK);
		flowFlags.add(Flow.PERMA);
		flowFlags.add(Flow.READER);
		
		id = idCount;
		idCount++;
		
		raw = new float[(int)(40 * 2 * 3.14f)];
		
		fac = new BufferInterpolated(raw.length * ilen);
		
		new BufferFactory() {
			
			@Override
			public String getName() {
				
				return "Dyn " + id;
			}
			
			@Override
			public Buffer generateBuffer(int bufferSize) {
				
				return fac;
			}
		}.getDefault();
		
	}

	
	@Override
	public void read() {
		
		/*
		PMatrix2D m = getDomain().getDisplayManager().getTransform().getMultipliedMatrix();
		float x = getX(),
			  y = getY(),
			  xo = m.multX(x, y),
			  yo = m.multY(x, y);
		*/
		
		int w = 20, h = 20;
		
		PGraphics g = RootClass.mainProc().g;
		
		PImage img = g.get((int)getX(), (int)getY(), w, h);
		img.loadPixels();
		
		
		float d = (float) Math.PI * 2 / raw.length;
		float c = 0;
		
		for (int i = 0; i < raw.length; i++) {
			
			c += d;
			
			float x = getX() + (float)Math.sin(c) * 40,
				  y = getY() + (float)Math.cos(c) * 40;
			
			float xf = x - (int)x,
				  yf = y - (int)y;
			
			float xb1 = g.brightness(g.get((int)x, (int)y)),
				  xb2 = g.brightness(g.get((int)x + 1, (int)y)),
				  yb1 = g.brightness(g.get((int)x, (int)y + 1)),
				  yb2 = g.brightness(g.get((int)x +1, (int)y + 1));
			
			float xi = xb1 + (xb2 - xb1) * xf,
				  yi = yb1 + (yb2 - yb1) * xf,
			
				  bb =	xi + (yi - xi) * yf;
			
			raw[i] = bb / 255f;
		}
		
		for (int i = 0; i < fac.buf.length; i++) {
			
			float r = raw.length * i / (float)fac.buf.length;
			int ri = (int) r;
			
			float p1 = raw[ri],
				  p2 = raw[(ri + 1) % raw.length];
			
			fac.buf[i] = p1 + (p2 - p1) * (r - ri);
		}
	}
	
	
	@Override
	public float getX() {
		
		return pos.getX();
	}

	@Override
	public float getY() {
		// TODO Auto-generated method stub
		return pos.getY();
	}

	@Override
	public void setPos(float x, float y) {
		
		pos.setPos(x, y);
		
	}

	@Override
	public Position getPosObject() {
		
		return pos;
	}

	@Override
	public void setPosObject(Position p) {

		pos = p;
		
	}

	@Override
	public Module createInstance() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void processIO() {
		// TODO Auto-generated method stub
		
	}

	
}
