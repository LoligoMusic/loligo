package module.modules.reader;

import constants.Flow;
import errors.LoligoException;
import gui.nodeinspector.DynIONodeInspector;
import gui.nodeinspector.NodeInspector;
import gui.text.CharProfile;
import gui.text.NumberInputField;
import gui.text.TextInputField;
import lombok.var;
import module.ModuleDeleteMode;
import module.Modules;
import module.dynio.DynIOInfo;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.osc.AbstractOscModule;
import pr.osc.PIn;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;


@NodeInfo(type = NodeCategory.IO, name = "OSC-In")

public class OscIn extends AbstractOscModule {

	private PIn oscIn;
	
	
	public OscIn() {
		flowFlags.add(Flow.SOURCE);
	}
	
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		createDynIO("Number1", DataType.Number, IOType.OUT);
	}
	
	@Override
	public void resetModule() {
		super.resetModule();
		Modules.removeDynamicOutputs(this);
	}
	
	@Override
	public NodeInspector createGUISpecial() {

		NodeInspector ni = new DynIONodeInspector(this);
		
		ni.setSpecial(n -> {
		
			n.createSection("Connection");
			
			NumberInputField p = new NumberInputField(d -> setPort(d.intValue()), () -> (double) port);
			p.setWH(n.getContentWidth(), p.getHeight());
			p.setCharProfile(CharProfile.INT);
			n.addLabeledEntry("Port", p);
			
			TextInputField t = new TextInputField(this::setOscPath, this::getOscPath);
			n.addLabeledEntry("Path", t);
		});
		
		ni.init();
		
		return ni;
	}

	@Override
	public void setOscPath(String path) {
		super.setOscPath(path);
		updateOSCPort();
	}
	
	@Override
	public void setPort(int port) {
		super.setPort(port);
		updateOSCPort();
	}
	
	
	private void updateOSCPort() {
		
		PIn old = oscIn;
		
		try {
			oscIn = OscOut.manager.findIn(port);
			
			if(checkFlag(Flow.ERROR)) {
				
				flowFlags.remove(Flow.ERROR);
				if(gui != null) {
					gui.unmarkError();
				}
			}
			
		} catch (LoligoException e) {
			
			getDomain().getErrorHandler().dispatchErrorEvent(this, e);
			flowFlags.add(Flow.ERROR);
			if(gui != null)
				gui.markError(e);
		}
		
		if(oscIn != null) {
			
			oscIn.addPath(oscPath);
			
			if(old != oscIn) {
				
				oscIn.addUser();
				
				if(old != null)
					old.removeUser();
			}
		}
	}
	
	
	@Override
	public void processIO() {
		if(oscIn != null) {
			oscIn.getValues(oscPath, values);
			var outs = getOutputs();
			if(outs != null) {
				for (int i = 0; i < outs.size(); i++) {
					outs.get(i).setValueObject(values[i]);
				}
			}
		}
	}

	@Override
	public void onAdd() {
		super.onAdd();
		updateOSCPort();
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		
		oscIn.removeUser();
		oscIn = null;
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
				.addExtras(oscPath, port);
	}
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		
		oscPath = data.nextString();
		port = data.nextInt();
		
		updateOSCPort();
	}

	
	@Override
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		var io = Modules.addDynamicOutput(this, "", DataType.Number);
		rename(io, name);
		setNumValues(getOutputs().size());
		return io;
	}
	
	
	@Override
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		var io = Modules.getOutputByName(this, name);
		removeIO(io);
		setNumValues(getOutputs().size());
		return io;
	}


	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.NUMBER_OUT;
	}
	

}
