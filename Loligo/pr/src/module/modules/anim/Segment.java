package module.modules.anim;

import constants.Units;
import lombok.var;
import module.AnimObject;
import module.inout.ColorInputIF;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.NumericType;
import module.inout.TransformInput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PConstants;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;
import util.Colors;

@NodeInfo(type = NodeCategory.ANIM)
public class Segment extends AnimObject {

	private static final float SIZE = (float) Units.LENGTH.normFactor;
	private static final int DEFAULT_RESOLUTION = 6;
	private static final NumericAttribute ATTR_RES = new NumericAttribute(NumericType.SubType.INTEGER, 3, 128);
	
	private float[] verts;
	private final TransformInput in_trans;
	private final NumberInputIF in_res, in_sec, in_rad;
	private final ColorInputIF in_col;
	
	
	public Segment() {

		in_trans = addTransformInput();
		in_res = addInput("Resolution", ATTR_RES).setDefaultValue(DEFAULT_RESOLUTION);
		in_sec = addInput("Sector", NumericAttribute.DEZIMAL_FREE).setDefaultValue(1);
		in_rad = addInput("Inner radius", NumericAttribute.DEZIMAL_FREE);
		in_col = addColorInput();
	}

	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		initVerts(DEFAULT_RESOLUTION);
	}
	
	
	private void initVerts(int res) {
		verts = new float[res * 4];
	}
	
	
	private void resolution(final int res) {
		if(res * 2 != verts.length) {
			initVerts(res);
		}
	}
	
	
	private void sector(float sec, float rad) {
		
		int res = verts.length,
			res2 = res / 2;
		
		float step = PConstants.TWO_PI * sec / (res2 -2 );
		float in = SIZE * rad;
		
		for (int i = 0; i < res2; i += 2) {
			float s = PConstants.PI - step * i;
			float sin = (float) Math.sin(s),
				  cos = (float) Math.cos(s);
			verts[i] 	 		 = SIZE * sin;
			verts[i + 1] 		 = SIZE * cos;
			verts[res - (i + 2)] = in * sin;
			verts[res - (i + 1)] = in * cos;
		}
	}
	
	
	@Override
	public DisplayObjectIF createAnim() {
		
		var d = new DisplayObject() {
			public void render(PGraphics g) {
				
				g.pushMatrix();
				g.translate(getX(), getY());
				g.applyMatrix(in_trans.getMatrix());
				
				int c = Colors.multiply(getColor(), g.tintColor);
				g.fill(c);
				g.noStroke();
				g.beginShape();
				
				for (int i = 0; i < verts.length; i += 2) {
					g.vertex(verts[i], verts[i + 1]);
				}
				g.endShape();
				g.popMatrix();
			} 
		};
		
		return d;
	}

	
	@Override
	public void processIO() {

		in_trans.getMatrixInput();
		
		boolean re = in_res.changed();
		if(re) {
			int r = (int) in_res.getWildInput();
			resolution(r + 1);
		}
		
		if(re || in_sec.changed() || in_rad.changed()) {
			float f = (float) in_sec.getWildInput();
			float r = (float) in_rad.getWildInput();
			sector(f, r);
		}
		
		getAnim().setColor(in_col.getColorInput());
	}
	
	
	@Override
	public int pipette(int x, int y) {
		return 0;
	}
	

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {

		super.doDeserialize(data, rdc);
		initVerts((int) in_res.getWildInput());
	}

	
}
