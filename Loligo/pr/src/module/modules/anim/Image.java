package module.modules.anim;

import java.net.URL;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import gui.BlendMode;
import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import gui.text.FileInputField;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.var;
import module.AnimObject;
import module.ModuleDeleteMode;
import module.inout.ColorInputIF;
import module.inout.IOEnum;
import module.inout.NumberInputIF;
import module.inout.TransformInput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;
import processing.core.PImage;
import save.AssetContainer;
import save.AssetRegistry;
import save.AssetSource;
import save.ImageData;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Colors;

@NodeInfo(type = NodeCategory.ANIM)

public class Image extends AnimObject implements AssetContainer {
	
	@RequiredArgsConstructor
	private enum Origin {
		TOP_LEFT	((img, pimg) -> setOff(img, 0, 0)),
		TOP_RIGHT	((img, pimg) -> setOff(img, pimg.width, 0)),
		BOTTOM_LEFT	((img, pimg) -> setOff(img, 0, pimg.height)),
		BOTTOM_RIGHT((img, pimg) -> setOff(img, pimg.width, pimg.height)),
		CENTER		((img, pimg) -> setOff(img, pimg.width / 2, pimg.height / 2));
		
		final BiConsumer<Image, PImage> cons;
		
		void setOrigin(Image img, PImage pimg) {
			cons.accept(img, pimg);
		}
		
		static void setOff(Image img, float x, float y) {
			img.offX = x;
			img.offY = y;
		}
	}
	
	public static final String[] EXTENSIONS = {"gif", "png", "jpg", "tif"};
	
	
	public static boolean isSupportedImageExtension(String name) {
		for (final String ext : EXTENSIONS) 
             if (name.endsWith("." + ext)) 
                return true;
         return false;
	}
	
	/*
	public static Image createImage(LoligoPatch patch, URL url) {
		
		ImageData id = AssetRegistry.loadImage(url);
		Image image = new Image();
		patch.addModule(image);
		
		image.setImages(id);
		return image;
	}
	*/
	@Getter
	private AssetSource assetSource;
	@Getter@Setter
	private boolean preserveAspect = true;
	
	private final TransformInput in_trans;
	private final NumberInputIF in_pos;
	private final ColorInputIF in_tint;
	private int currentFrame = 0;
	private float offX, offY;
	private int tint = 0xffffffff;
	private Origin origin = Origin.CENTER;
	private PImage[] rgbs = new PImage[1];
	private BlendMode blendMode = BlendMode.DEFAULT;
	
	
	public Image() {
		
		in_trans = addTransformInput();
		in_pos = addInput("Frame");
		in_tint = addColorInput().setDefaultColor(0xffffffff);
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		
		ImageData id = AssetRegistry.getErrorImage();
		setImages(id);
	}
	
	public void setImages(ImageData id) {
		rgbs = id.getImgs();
		assetSource = id.getFile();
	}
	
	public void setImages(URL url) {
		AssetRegistry.createImageDataAsync(url, this::setImages);
	}
	
	@Override
	public void loadAssetFromURL(URL url) {
		setImages(url);
	}
	
	public PImage createErrorImage(String msg) {
		ImageData id = AssetRegistry.getErrorImage();
		assetSource = id.getFile();
		return id.getImgs()[0];
	}
	
	@Override
	public int pipette(int x, int y) {
		
		return rgbs[currentFrame].get(x, y);
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		
		var ni = NodeInspector.newInstance(this, n -> {
			
			var db = new DropDownMenu(
									i -> blendMode = BlendMode.values()[i], 
									() -> blendMode.ordinal(),
									IOEnum.enumToStringArray(blendMode));
			db.update();
			n.addLabeledEntry("Blend", db);
			
			var dc = new DropDownMenu(
					i -> origin = Origin.values()[i], 
					() -> origin.ordinal(),
					IOEnum.enumToStringArray(origin));
			dc.update();
			n.addLabeledEntry("Origin", dc);
			
			n.setTop(5);
			n.createSection("Path");
			
			Supplier<URL> up = () -> {
				if(assetSource != null)
					return assetSource.getUrl();
				return null;
			};
			
			var f = new FileInputField(this::setImages, up);
			f.getFileDialog().allowImageFiles();
			f.update();
			n.addInspectorElement(f);
		});
		
		return ni;
	}
	
	
	@Override
	public DisplayObjectIF createAnim() {
		var d = new DisplayObject() {
			@Override public void render(PGraphics g) {
				
				if(rgbs == null || rgbs.length == 0)
					return;
				
				PImage image = rgbs[currentFrame < rgbs.length ? currentFrame : rgbs.length - 1];
				if(image != null) {
					
					g.push();
					g.tint(Colors.multiply(tint, g.tintColor));
					g.blendMode(blendMode.value);
					origin.setOrigin(Image.this, image);
					g.translate(getX(), getY());
					g.applyMatrix(in_trans.getMatrix());
					g.translate(-offX, -offY);
					
					g.image(image, 0, 0);
					g.tint(Colors.WHITE);
					g.pop();
				}
			}
		};
		return d;
	}
	
	
	@Override
	public void processIO() {
		
		if(in_pos.changed())
			currentFrame = (int)(in_pos.getInput() % 1 * rgbs.length);
		
		tint = in_tint.getColorInput();
		
		in_trans.getMatrixInput();
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		if(assetSource != null)
			AssetRegistry.releaseAsset(assetSource.getUrl());
	}
	
	
	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		var s = assetSource == null ? null : assetSource.toString();
		md.addExtras(blendMode, origin, s);
		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		
		blendMode = BlendMode.valueOf(data.nextString());
		origin 	  = Origin.valueOf(data.nextString());
		
		String p = data.nextString();
		ImageData id = AssetRegistry.loadImage(p);
		if(id != null) {
			PImage[] pi = id.getImgs();
			rgbs = pi != null ? pi : AssetRegistry.getErrorImage().getImgs();
			assetSource = id.getFile();
		}
	}
}
