package module.modules.anim;

import java.util.List;

import com.google.common.collect.Lists;

import constants.DisplayFlag;
import constants.InputEvent;
import constraints.Constrainable;
import constraints.LoligoPosConstraint;
import constraints.PosConstraintContainer;
import gui.Position;
import gui.selection.GuiType;
import module.AbstractPositionableModule;
import module.Module;
import module.ModuleContainer;
import module.ModuleDeleteMode;
import module.ModuleType;
import pr.DisplayObjectIF;
import pr.ModuleGUI;
import save.ModuleData;
import save.PathData;
import save.RestoreDataContext;
import save.SaveDataContext;

public abstract class AbstractPathModule extends AbstractPositionableModule
		implements PosConstraintContainer, ModuleContainer {

	protected LoligoPosConstraint constraint; // delegate

	public AbstractPathModule() {

	}
	
	protected void setConstraint(LoligoPosConstraint constraint) {
		this.constraint = constraint;
	}

	@Override
	public ModuleGUI createGUI() {

		ModuleGUI h = super.createGUI();
		h.setGuiType(GuiType.CONSTRAINT);

		DisplayObjectIF sg = constraint.createGUI();

		sg.setFlag(DisplayFlag.fixedToParent, true);

		h.addChild(sg);

		return h;
	}

	public void onConstraintEvent(InputEvent e, LoligoPosConstraint posConstraint) {

	}

	@Override
	public void updatePosition() {

		/*
		 * for (Constrainable c : getConstrainables()) {
		 * 
		 * c.updatePosition(); }
		 */
	}

	@Override
	public LoligoPosConstraint getPosConstraint() {
		return constraint;
	}

	/*
	 * 
	 * @Override public boolean addModule(Module m) {
	 * 
	 * return constraint.addModule(m); }
	 * 
	 * @Override public boolean removeModule(Module m) {
	 * 
	 * return constraint.removeModule(m); }
	 * 
	 */
	@Override
	public List<Constrainable> getConstrainables() {
		return Lists.transform(constraint.getFollowers(), f -> f.getTarget());
	}

	@Override
	public float getX() {
		return constraint.getX();
	}

	@Override
	public float getY() {
		return constraint.getY();
	}

	@Override
	public void setPos(float x, float y) {
		constraint.setPos(x, y);
	}

	@Override
	public Position getPosObject() {
		return constraint.getPosObject();
	}

	@Override
	public void setPosObject(Position p) {
		constraint.setPosObject(p);
	}

	@Override
	public void setGlobal(float x, float y) {
		constraint.setGlobal(x, y);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		constraint.removeAllFollowers();
	}

	@Override
	public List<? extends Module> getContainedModules() {

		return getConstrainables().stream()
								  .filter(c -> c instanceof Module)
								  .map(c -> (Module) c)
								  .toList();

	}

	@Override
	public boolean addModule(Module m) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeModule(Module m, ModuleDeleteMode mode) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean checkType(ModuleType t) {
		return t == ModuleType.PATH;
	}

	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {

		ModuleData md = super.doSerialize(sdc);

		PathData pd = doSerializePath(md);
		sdc.addPath(pd);

		return md;
	}

	@Override
	public PathData doSerializePath(ModuleData md) {

		PathData pd = new PathData(this, md);
		pd.generateConstraintMemento();

		return pd;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {

		super.doDeserialize(data, rdc);

		// PathData pd = rdc.getPathData(data);
		// doDeserializePath(pd, rdc); This is called in RestoreDataContext.restorePaths
	}

	@Override
	public void doDeserializePath(PathData pd, RestoreDataContext rdc) {
		
		pd.restoreConstraintMemento(this);
	}

}