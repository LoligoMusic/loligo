package module.modules.anim;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import constants.Flow;
import constants.Units;
import constraints.PosConstraintOffset;
import gui.paths.CircularSegment;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import save.ModuleData;
import save.PathData;
import save.RestoreDataContext;

@NodeInfo(type = NodeCategory.PATH)

public class RingPath extends AbstractPathModule {
	
	//final private ArrayList<Ring> rings;
	
	final private CircularSegment ring;
	private gui.paths.Path path;
	//final public InnerRingPath ring;
	private final NumberInputIF in_radius, in_pos;
	private PosConstraintOffset constraint;
	
	public RingPath() {
		
		flowFlags.add(Flow.SINK);
		flowFlags.add(Flow.ROUNDSLOTS);
		
		super.constraint = constraint = new PosConstraintOffset(this);
		
		
		path = constraint.getPath();
		
		ring = new CircularSegment(constraint, path);
		
		path.addSegment(0, ring);
		
		path.updateLength();
		
		ring.setRadius(100);
		
		
		
		in_radius = addInput("Radius", DEZIMAL_FREE).setDefaultValue(1);
		in_pos = addInput("Position", DEZIMAL_FREE);
		//in_pos.value = 0;
		//ring.updateFollowerPos();
		//roundSlotPosition = true;
		
	}
	int f = 0;
	
	@Override
	public void processIO() {
		
		boolean c = false;
		
		if(in_radius.changed()) {
			
			ring.setRadius((float) (in_radius.getWildInput() * Units.LENGTH.normFactor));
			c = true;
		}
		
		if(in_pos.changed()) {
			
			constraint.setOffset(-in_pos.getWildInput());
			c = true;
		}
		
		if(c)
			constraint.updatePosConstraint();
	}
	
	
	@Override
	public PathData doSerializePath(ModuleData md) {
	
		return new PathData(this, md);
	}
	
	@Override
	public void doDeserializePath(PathData pd, RestoreDataContext rdc) {
		
	}
}
	

