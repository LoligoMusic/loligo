package module.modules.anim;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;

import gui.CopyPaste;
import gui.NodeSelector2;
import gui.nodeinspector.NodeInspector;
import lombok.var;
import module.Anim;
import module.AnimObject;
import module.Module;
import module.ModuleType;
import module.Modules;
import module.inout.ColorInputIF;
import module.inout.TransformInput;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import patch.Domain;
import patch.LoligoPatch;
import patch.ParamHolderDomain;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Colors;

@NodeInfo(type=NodeCategory.ANIM)
public class Dupli extends AnimObject {

	public static Dupli create(LoligoPatch patch) {
		
		var in = patch.getDisplayManager().getInput();
		var mods = CopyPaste.getSelectedModules(patch);
		
		if(mods.isEmpty())
			return null;
		
		var m = mods.get(0);
		if(m instanceof AnimObject) {
			
			Dupli c = patch.getNodeRegistry().createInstance(Dupli.class);
			Modules.initializeModuleDefault(c, patch);
			
			c.source = (AnimObject) m;
			c.setPos(in.getMouseX(), in.getMouseY());
			
			return c;
		}
		return null;
	}
	
	
	private static final Anim EMPTY = new AnimObject.Empty();
	
	
	
	protected Anim source;
	final protected TransformInput in_trans;
	final protected ColorInputIF in_color;
	
	
	
	public Dupli() {
		this(EMPTY);
	}
	
	
	public Dupli(Anim source) {
		
		this.source = source;
		in_trans = addTransformInput();
		in_color = addColorInput().setDefaultColor(Colors.WHITE);
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		var ni = NodeInspector.newInstance(this, n -> {
			
			var ns = new NodeSelector2(this::setSource, this::getSource) {
				@Override public String labelPatch(Domain p) {
					String s = super.labelPatch(p);
					s = p == getDomain() ? s + " (this)" : s;
					return s;
				}
			};
			
			List<Module> exs = new ArrayList<>();
			
			Predicate<ParamHolderDomain> f = dom -> {
				var mod = dom.getSubPatchModule();
				exs.add(mod);
				return true;
			};
			Modules.iterateParentPatches(getDomain(), f);
			
			exs.add(Dupli.this);
			
			ns.exclude(exs.toArray(new Module[exs.size()]));
			ns.setTypes(EnumSet.of(ModuleType.ANIM));
			
			n.addLabeledEntry("Target", ns);
		});
		return ni;
	}
	
	public void setSource(Anim a) {
		source = a == null ? EMPTY : a;
	}
	
	public void setSource(Module m) {
		setSource((Anim) m);
	}
	
	
	public Anim getSource() {
		return source == EMPTY ? null : source;
	}
	
	
	@Override
	public DisplayObjectIF createAnim() {
		return new DupliAnim();
	}

	
	@Override
	public void processIO() {
		in_trans.getMatrixInput();
		in_color.getColorInput();
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc)
				.addExtras((Object) null);
		sdc.saveReference(md, getSource(), 0);
		return md;
	} 
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		rdc.restoreReference(data, this::setSource);
	}

	
	protected class DupliAnim extends DisplayObject {
		@Override
		public void render(PGraphics g) {
			
			g.push();

			g.translate(getX(), getY());
			g.applyMatrix(in_trans.getMatrix());
			g.translate(-source.getX(), -source.getY());
			
			g.tint(in_color.getColor());
			
			source.getAnim().render(g);
			g.tint(Colors.WHITE);
			g.pop();
		}
	}
	
}
