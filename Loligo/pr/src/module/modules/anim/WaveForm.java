package module.modules.anim;

import constants.DisplayFlag;
import module.AnimObject;
import module.inout.ColorInputIF;
import module.inout.TransformInput;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.RootClass;
import processing.core.PGraphics;

@NodeInfo(type=NodeCategory.ANIM)

public class WaveForm extends AnimObject {
	
	transient private float[] buffer = {0};
	private double step;
	final private ColorInputIF in_color;
	final private TransformInput in_trans;
	
	public WaveForm() {
		
		in_color = addColorInput();
		in_trans = addTransformInput();
		buffer =  RootClass.audioManager().getMasterBead().getOutBuffer(0);
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		
		setAnim(new DisplayObject() {
			
			@Override public void render(PGraphics g){
				
				buffer =  getDomain().getAudioManager().getMasterBead().getOutBuffer(0);
				
				g.pushMatrix();
				g.translate(getX(), getY());
				g.applyMatrix(in_trans.getMatrix());
				
				g.stroke(getColor());
				int j = 0;
				
				for(int i = 0; j < buffer.length; i++){
					
					float h = buffer[j] * getHeight();
					g.line(i, -h, i, h);
					j += step;
				}
				g.noStroke();
				g.popMatrix();
			}
		});
		getAnim().setFlag(DisplayFlag.RECT, true);
		getAnim().setWH(256, 100);
		
		step = buffer.length / getAnim().getWidth();
		return getAnim();
	}
	
	@Override
	public void processIO() {
		getAnim().setColor(in_color.getColorInput());
		in_trans.getMatrixInput();
	}
	

	@Override
	public int pipette(int x, int y) {
		return 0;
	}
	
	
}
