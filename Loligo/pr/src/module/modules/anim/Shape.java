package module.modules.anim;

import static constants.DisplayFlag.CLICKABLE;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

import constants.Units;
import gui.BlendMode;
import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import lombok.Setter;
import module.AnimObject;
import module.inout.ColorInputIF;
import module.inout.EnumInput;
import module.inout.IOEnum;
import module.inout.TransformInput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PConstants;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Colors;

@NodeInfo(type = NodeCategory.ANIM)

public class Shape extends AnimObject {
	
	private static final IOEnum BLEND_ENUM = IOEnum.createEnum(BlendMode.DEFAULT);
	private static final float SIZE = (float) Units.LENGTH.normFactor;
	
	private final EnumInput in_blend;
	private final TransformInput in_trans;
	private final ColorInputIF in_color;
	private ShapeAnim shapeAnim;
	private BlendMode blendMode = BlendMode.DEFAULT;
	
	public Shape() {
		
		in_trans = addTransformInput();
		in_color = addColorInput();
		in_blend = new EnumInput("Blend", this, BLEND_ENUM);
		addIO(in_blend);
	}
	
	@Override
	public int pipette(int x, int y) {
		
		return 0;
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		
		if(shapeAnim == null)
			shapeAnim = new ShapeAnim(DrawForm.ELLIPSE); 
		setAnim(shapeAnim);
		return getAnim();
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		
		var ni = NodeInspector.newInstance(this, n -> {
			
			var m = new DropDownMenu(this::setForm, () -> shapeAnim.form.ordinal(), IOEnum.enumToStringArray(DrawForm.ELLIPSE));
			n.addLabeledEntry("Shape", m);
			m.update();
		});
		
		return ni;
	}
	
	
	public void setForm(DrawForm f) {
		
		shapeAnim.form = f;
	}
	
	public void setForm(int i) {
		DrawForm d =DrawForm.values()[i];
		setForm(d);
	}
	
	
	@Override
	public void processIO() {
		
		in_trans.getMatrixInput();
		
		//if(in_color.changed()) {
			
		shapeAnim.setColor(in_color.getColorInput());
		//}
		
		if(in_blend.changed())
			blendMode = BlendMode.blendValues[in_blend.getEnumInput().getOrdinal()];
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
			.addExtras(
				shapeAnim.form.name(), 
				blendMode.name()
			);
	}

	public static void prepairData(ModuleData data) {
		data.nextString(DrawForm::valueOf);
		data.nextString(BlendMode::valueOf);
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		setForm((DrawForm) data.nextObject());
		blendMode = (BlendMode) data.nextObject(); 
	}

	
	public enum DrawForm {
		ELLIPSE{
			@Override void draw(PGraphics g) {
				float s = SIZE * 2;
				g.ellipse(0, 0, s, s);
			};
		},
		TRIANGLE{
			final float p = PConstants.TWO_PI / 3, q = PConstants.PI, s = SIZE;
			float x1 = (float) sin(q) 		  * s, y1 = (float) cos(q) 		   * s, 
				  x2 = (float) sin(q + p) 	  * s, y2 = (float) cos(q + p) 	   * s, 
				  x3 = (float) sin(q + p * 2) * s, y3 = (float) cos(q + p * 2) * s;
					
			@Override void draw(PGraphics g) {
				g.triangle(x1, y1, x2, y2, x3, y3);
			};
		},
		RECTANGLE{
			@Override void draw(PGraphics g) {
				float sm = -SIZE, s2 = SIZE * 2;
				g.rect(sm, sm, s2, s2);
			};
		};
		
		abstract void draw(PGraphics g);
	}
	
	
	private class ShapeAnim extends DisplayObject {
		
		@Setter
		DrawForm form;
		
		public ShapeAnim(DrawForm form) {
			
			setFlag(CLICKABLE, false);
			this.form = form;
		}
		
		@Override
		public void render(PGraphics g) {
			
			g.push();
			int c = Colors.multiply(getColor(), g.tintColor);
			g.fill(c);
			
			g.translate(getX(), getY());
			g.applyMatrix(in_trans.getMatrix());
			
			g.noStroke();
			g.blendMode(blendMode.value);
			form.draw(g);
			
			g.pop();
			
		}
	};
}
