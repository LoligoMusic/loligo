package module.modules.anim;

import constants.Timer;
import gui.NodeSelector2;
import gui.nodeinspector.NodeInspector;
import lombok.Getter;
import lombok.var;
import module.AnimObject;
import module.Module;
import module.ModuleType;
import module.PositionableModule;
import module.inout.ColorInputIF;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.RootClass;
import processing.core.PGraphics;
import processing.core.PMatrix2D;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.ANIM)
public class Line extends AnimObject {

	private final NumberInputIF in_weight, in_section;
	private final ColorInputIF in_color;
	
	@Getter
	private PositionableModule target = this;
	private float weight, section;
	private int color;
	
	private PMatrix2D matrix, matrixTarget;
	private boolean crossDomain;
	
	public Line() {
		
		in_weight =	addInput("Weight").setDefaultValue(.5);
		in_section = addInput("Section", NumericAttribute.DEZIMAL_FREE).setDefaultValue(1);
		in_color = addColorInput();
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		
		var dom = getDomain();
		var n = (NullObject) dom.getNodeRegistry().createInstance(NullObject.class);
		n.setLabel(getLabel() + "-target");
		dom.addModule(n);
		setTarget(n);
		
		Timer.createFrameDelay(dom, 
			t -> {
				int p = 100;
				n.setPos(this).add(p, p);
				dom.getDisplayManager().getInput().selection.add(n.getGui());
			}
		).start();
	}
	
	@Override
	public void postInit() {
		super.postInit();
		matrix = getDomain().getDisplayManager().getTransform().getMultipliedMatrix();
	}
	
	@Override
	public NodeInspector createGUISpecial() {
	
		return NodeInspector.newInstance(this, n -> {

			var sel = new NodeSelector2(this::setTarget, this::getTarget);
			sel.setTypes(ModuleType.POSITIONABLES);
			
			n.createSection("Target");
			//sel.toInspector(n);
			n.addLabeledEntry("Target", sel);
			sel.update();
		});
	}
	
	public void setTarget(Module t) {
		setTarget((PositionableModule) t);
	}
	
	public void setTarget(PositionableModule t) {
		target = t != null ? t : this;
		
		var tf = target.getDomain();
		matrixTarget = tf.getDisplayManager().getTransform().getMultipliedMatrix();
		crossDomain = getDomain() != tf;
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		
		return new DisplayObject() {
			PMatrix2D mv = new PMatrix2D();
			@Override
			public void render(PGraphics g) {
				
				if(crossDomain && g != RootClass.mainDm().getGraphics())
					return;
				
				float x1 = getX(),
					  y1 = getY(),
					  xm = matrix.multX(x1, y1),
					  ym = matrix.multY(x1, y1),
					  
					  tx = target.getX(),
					  ty = target.getY(),
					  txm = matrixTarget.multX(tx, ty),
					  tym = matrixTarget.multY(tx, ty),
					  
					  dx = txm - xm,
					  dy = tym - ym,
							  
					  x2 = xm + dx * section,
					  y2 = ym + dy * section;
				
				
				g.push();
				
				mv.set(matrix);
				mv.invert();
				g.applyMatrix(mv);
				
				g.noFill();
				g.stroke(color);
				g.strokeWeight(weight);
				
				g.line(xm, ym, x2, y2);
				
				g.pop();
			}
		};
	}

	@Override
	public void processIO() {
		color = in_color.getColorInput();
		weight = (float) in_weight.getInput() * 30;
		section = (float) in_section.getWildInput();
	}

	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		md.addExtras((Object) null);
		sdc.saveReference(md, target, 0);
		return md;
	} 
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		rdc.restoreReference(data, this::setTarget);
	}
	
	
}
