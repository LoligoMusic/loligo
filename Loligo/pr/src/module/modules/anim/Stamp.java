package module.modules.anim;

import gui.CopyPaste;
import lombok.var;
import module.AnimObject;
import module.Canvas;
import module.Modules;
import module.inout.TriggerInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import patch.LoligoPatch;
import pr.DisplayObjectIF;
import pr.RootClass;
import processing.core.PGraphics;

@NodeInfo(type=NodeCategory.ANIM)
public class Stamp extends Dupli{

	public static Stamp create(LoligoPatch patch) {
		
		var in = patch.getDisplayManager().getInput();
		var mods = CopyPaste.getSelectedModules(patch);
		
		if(mods.isEmpty())
			return null;
		
		var m = mods.get(0);
		if(m instanceof AnimObject ao) {
			Stamp c = patch.getNodeRegistry().createInstance(Stamp.class);
			Modules.initializeModuleDefault(c, patch);
			
			c.source = ao;
			c.setPos(in.getMouseX(), in.getMouseY());
			return c;
		}
		return null;
	}
	
	
	private Canvas canvas;
	private PGraphics graphics;
	
	private final TriggerInputIF in_trigger;
	private boolean isTriggered;
	
	public Stamp() {
		
		in_trigger = addTrigger();
	}
	
	@Override
	public void postInit() {
		super.postInit();
		
		canvas = RootClass.mainDm().canvas();
		graphics = canvas.graphics;
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		
		var d = new DupliAnim() {
			@Override
			public void render(PGraphics g) { 
				if(isTriggered) {
					graphics.beginDraw();
					super.render(graphics);
					graphics.endDraw();
					isTriggered = false;
				}
			}
		};
		
		return d;
	}
	
	@Override
	public void processIO() {
		super.processIO();
		
		if(in_trigger.triggered()) {
			isTriggered = true;
		}
	}
	
}
