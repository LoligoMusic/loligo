package module.modules.anim;

import constants.Flow;
import constraints.ConstrainableObject;
import constraints.PosConstraintOffset;
import gui.CheckBox;
import gui.DropDownMenu;
import gui.Position;
import gui.nodeinspector.NodeInspector;
import gui.paths.CurveSegment;
import gui.paths.Node;
import gui.paths.PathEvent;
import gui.paths.PathEventEnum;
import gui.paths.PathEventManager;
import gui.paths.SegmentType;
import gui.paths.SimplePathObject;
import gui.paths.SimplePathObject.Mapping;
import gui.paths.gui.PathNodeGui;
import lombok.Getter;
import lombok.var;
import module.inout.IOEnum;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.DisplayObjectIF;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.PATH)

public class Path extends AbstractPathModule {
	
	private final NumberInputIF in_pos, in_tight;
	private final PosConstraintOffset constraint;
	@Getter
	private float tightness = 0;
	private Mapping mapping; 
	
	
	public static class CP extends ConstrainableObject {
		
		gui.paths.PosConstraint pc;
		
		public CP(Node n) {
			this.pc = n.getPosConstraint();
			setPosObject(new Position.PositionOffset(n));
			setPos(15, 15);
		}
		
		@Override
		public void updatePosition() {
			pc.updatePosConstraint();
		}
	}
	
	public Path() {
		
		flowFlags.add(Flow.SINK);
		flowFlags.add(Flow.ROUNDSLOTS);
		
		constraint = new PosConstraintOffset(this) {
			
			@Override public void onPathEvent(PathEvent e) {
				super.onPathEvent(e);
				
				if(e.constraint == constraint.getConstraint()) {
					if(e.type == PathEventEnum.SEGMENT_ADD) {
						gui.paths.Segment s = ((PathEvent.SegmentAddRemove) e).segment;
						onSegmentAdded(s);
					}else
					if(e.type == PathEventEnum.UPDATE_OBJECTS) {
						updateMapping();
					}
				}
			}
			
			@Override public DisplayObjectIF createGUI() {
				if(gui == null) {
					PathNodeGui pc = new PathNodeGui(Path.this, this);
					pc.editMode(false);
					gui = pc;
				}
				return gui;
			}
		};
		
		setConstraint(constraint);
		constraint.setPathListener(PathEventManager.instance());
		
		in_pos = addInput("Position", NumericAttribute.DEZIMAL_FREE);
		in_tight = addInput("Tightness", NumericAttribute.DEZIMAL_FREE);
	}
	
	
	@Override
	public void defaultSettings() {
		
		super.defaultSettings();
		
		Node 
		v1 = constraint.newNode(),
		v2 = constraint.newNode();
		v1.setPos(10, 10);
		v2.setPos(200, 250);
		
		gui.paths.Segment s = createSegment(v1, v2);
		
		constraint.getPath().addSegment(0, s);
		constraint.getPath().updateLength();
		
		constraint.gui.forceUpdateGui();
		
		mapping = Mapping.WRAP;
	}
	
	@Override
	public void resetModule() {
		super.resetModule();
		
//		var p = constraint.getPath();
//		int ns = p.numSegments();
//		for (int i = 0; i < ns; i++) 
//			constraint.getPath().removeSegment(0);
		
		var nds1 = constraint.getNodes();
		Node[] nds = nds1.toArray(new Node[nds1.size()]);
		for (Node n : nds) 
			constraint.removeNode(n);
		
	}

	
	private gui.paths.Segment createSegment(Node v1, Node v2) {
		return constraint.createSegment(v1, v2, SegmentType.CURVE);
	}
	
	public void onSegmentAdded(gui.paths.Segment s) {
		CurveSegment cs = (CurveSegment) s;
		cs.setCurveTightness(tightness);
		
		gui.paths.Path path = constraint.getPath();
		int numSegs = path.numSegments();
		
		for (int i = 0; i < numSegs; i++) {
			CurveSegment ss = (CurveSegment) path.getSegment(i);
			ss.setCurveTightness(tightness);
		}
	}
	
	
	public void setClosed(boolean closed) {
		
		gui.paths.Path path = constraint.getPath();
		int numSegs = path.numSegments();
		
		if(!closed && isClosed() && numSegs > 1) {
			
			path.removeSegment(path.getSegment(numSegs - 1));
		}else
		if(closed && !isClosed()) {
			
			Node v1 = path.getSegment(0).getNodeIn(),
				 v2 = path.getSegment(numSegs - 1).getNodeOut();
			
			gui.paths.Segment s = createSegment(v2, v1);
			
			constraint.getPath().addSegment(numSegs, s);
			constraint.getPath().updateLength();
		}
		
		constraint.dispatchEvent(new PathEvent.UpdateObjectsEvent(path));
	}
	
	public boolean isClosed() {
		return constraint.getPath().getNodeIn().numLinks() > 1;
	}
	
	public void setTightness(float tightness) {
		this.tightness = tightness;
		
		gui.paths.Path path = constraint.getPath();
		int numSegs = path.numSegments();
		
		for (int i = 0; i < numSegs; i++) {
			CurveSegment s = (CurveSegment) path.getSegment(i);
			s.setCurveTightness(tightness);
		}
		if(constraint.gui != null)
			constraint.gui.forceUpdateGui();
		constraint.dispatchEvent(new PathEvent.UpdateObjectsEvent(path));
	}
	
	public void setMapping(int i) {
		mapping = Mapping.values()[i];
		updateMapping();
	}
	
	private void updateMapping() {
		constraint.getFollowers().forEach(f -> ((SimplePathObject) f.getPathObject()).setMapping(mapping)); 
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector ni = NodeInspector.newInstance(this, n -> {
			
			CheckBox c = new CheckBox(this::setClosed, this::isClosed);
			n.addLabeledEntry("Closed", c);
			c.update();
			
			DropDownMenu m = new DropDownMenu(this::setMapping, () -> mapping.ordinal(), IOEnum.enumToStringArray(Mapping.WRAP));
			n.addLabeledEntry("Overflow", m);
			m.update();
			
		});
		
		return ni; 
	}
	
	
	@Override
	public void processIO() {
		
		boolean c = false;
		
		if(in_pos.changed()) {
			constraint.setOffset(in_pos.getWildInput());
			c = true;
		}
		if(in_tight.changed()) {
			setTightness((float) in_tight.getWildInput()); 
		}
		
		if(c)
			constraint.updatePosConstraint();
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
				.addExtras(mapping.name());
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		String s = data.nextString();
		mapping = Mapping.valueOf(s);
	}
	
}
	


