package module.modules.anim;

import constants.Flow;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.inout.ColorInputIF;
import module.inout.SwitchInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import patch.DomainType;
import pr.RootClass;

@NodeInfo(type = NodeCategory.ANIM)
public class Background extends AbstractModule {

	private final SwitchInputIF in_switch;
	private final ColorInputIF in_col;
	
	public Background() {
		
		flowFlags.add(Flow.SINK);
		
		in_switch = addSwitch();
		in_col = addColorInput();
		in_col.setDefaultColor(0xff000000);
	}
	
	
	@Override
	public void processIO() {
		
		if(getDomain().domainType() != DomainType.PARTICLE_FACTORY && in_switch.getSwitchState()) {
			RootClass.mainDm().setColor(in_col.getColorInput());
		}
	}
	
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		getDomain().getDisplayManager().setColor(0xff000000);
	}

}
