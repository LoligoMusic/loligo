package module.modules.anim;

import module.AnimObject;
import module.inout.ColorInputIF;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.RootClass;
import processing.core.PGraphics;
import processing.core.PMatrix2D;
import util.Colors;

@NodeInfo(type = NodeCategory.ANIM)
public class Tracer extends AnimObject {

	private static final int MAX_LENGTH = 50;
	
	private int targetLength = MAX_LENGTH, length = targetLength, color;
	private float weight = 2;

	private final ColorInputIF in_col;
	private final NumberInputIF in_weight, in_length;
	
	
	public Tracer() {
		
		in_col = addColorInput();
		in_weight = addInput("Weight").setDefaultValue(.1);
		in_length = addInput("Length").setDefaultValue(.5);
	}
	
	private class S {
		int color;
		float x, y, w;
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		
		DisplayObject d = new DisplayObject() {
			
			S[] buffer = new S[MAX_LENGTH];
			int idx;
			
			{
				for (int i = 0; i < buffer.length; i++) 
					buffer[i] = new S();
			}
			
			@Override
			public void render(PGraphics g) {
				
				if(g != RootClass.mainDm().getGraphics())
					return;
				
				int r = idx;
				PMatrix2D m = getDm().getTransform().getMultipliedMatrix();
				S s = buffer[r];
				s.x = m.multX(getX(), getY());
				s.y = m.multY(getX(), getY());
				s.w = weight;
				s.color = color;
				
				g.noFill();
				
				//length += Integer.signum(targetLength - length);
				length = targetLength;
				
				float x1 = s.x, y1 = s.y, x2 = 0, y2 = 0;
				
				g.resetMatrix();
				
				for (int i = 0, k = 0; i < length; i++) {
					
					k = (MAX_LENGTH + idx - i) % MAX_LENGTH;
					s = buffer[k];
					
					g.strokeWeight(s.w);
					
					int c = Colors.multiply(s.color, g.tintColor);
					//c = g.lerpColor(c, 0xff000000, (float)i / MAX_LENGTH); //Fade to black
					g.stroke(c);
					x2 = s.x;
					y2 = s.y;
					g.line(x1, y1, x2, y2);
					x1 = x2;
					y1 = y2;
				}
				
				idx = (idx + 1) % MAX_LENGTH;
				
				g.noStroke();
				g.applyMatrix(getDm().getTransform().getMatrix());
			}
		};
		
		return d;
	}

	@Override
	public void processIO() {
		
		color = in_col.getColorInput();
		weight = 20f * (float) in_weight.getInput();
		
		if(in_length.changed()) {
			targetLength = 1 + (int) (in_length.getInput() * (MAX_LENGTH - 1));
		
		}
	}

	@Override
	public int pipette(int x, int y) {
		// TODO Auto-generated method stub
		return 0;
	}

}
