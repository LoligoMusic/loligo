package module.modules.anim;

import module.AnimObject;
import module.Canvas;
import module.inout.ColorInputIF;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.RootClass;
import processing.core.PGraphics;
import processing.core.PMatrix2D;

@NodeInfo(type = NodeCategory.ANIM)

public class Draw extends AnimObject {

	private Canvas canvas;
	private PGraphics graphics;
	private DisplayObject anim;
	
	private final NumberInputIF in_size;
	private final ColorInputIF in_color;
	private final double maxSize = 20;
	private boolean isDrawing;
	
	
	public Draw() {
		
		in_color = addColorInput();
		in_size = addInput("Thickness").setDefaultValue(.2);
	}
	
	
	@Override
	public void postInit() {
	
		super.postInit();
		
		switch (getDomain().domainType()) {
		case SUBPATCH:
			canvas = RootClass.mainDm().canvas();
			graphics = canvas.graphics;
			break;
		case PARTICLE_FACTORY:
			break;
		default:
			canvas = getDomain().getDisplayManager().canvas();
			graphics = canvas.graphics;
			break;
		}
	}
	
	
	public DisplayObjectIF createAnim() {
		
		anim = new DisplayObject() {
		
			PMatrix2D m;
			float x1 = 0, y1 = 0, x2, y2;
			
			void coords1() {
				float x = getX(), y = getY();
				x1 = m.multX(x, y);
				y1 = m.multY(x, y);
				
			}
			
			void coords2() {
				float x = getX(), y = getY();
				x2 = m.multX(x, y);
				y2 = m.multY(x, y);
			}
			
			Runnable r = () -> {
				
				if(isDrawing) {
					
					coords2();
					graphics.beginDraw();
					
					graphics.stroke(getColor());
					graphics.strokeWeight(getWidth());
					graphics.line(x1, y1, x2, y2);
					
					x1 = x2;
					y1 = y2;
					
					graphics.noStroke();
					graphics.fill(getColor());
					graphics.endDraw();
				}
			};
			
			Runnable start = () -> {
				m = getDomain().getDisplayManager().getTransform().getMultipliedMatrix();
				coords1();
				//painter.start(x1, y1);
				if(graphics != null) {
					r.run();
					start = r;
				}
			};
			
			
			@Override public void render(PGraphics g) {
				
				if(g == RootClass.mainDm().getGraphics())
					start.run();
			}
		};
		
		return anim;
	};
	
	
	@Override
	public void processIO() {
		
		anim.setColor(in_color.getColorInput());
			
		if(in_size.changed()) {
			
			double s = in_size.getInput();
			
			isDrawing = s > 0;
			
			int f = (int)(s * maxSize);
			anim.setWH(f, f);
		}
		
	}
	
	 
	@Override
	public int pipette(int x, int y) {
		return 0;
	}
	
}
