package module.modules.anim;

import module.AbstractPositionableModule;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.ANIM)
public class NullObject extends AbstractPositionableModule {

	@Override
	public void processIO() {}

}
