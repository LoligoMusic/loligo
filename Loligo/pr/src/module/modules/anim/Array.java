package module.modules.anim;

import static module.inout.NumericAttribute.INTEGER_POSITIVE;

import lombok.var;
import module.inout.NumberInputIF;
import module.inout.TransformInput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import pr.DisplayObjectIF;
import processing.core.PGraphics;
import processing.core.PMatrix2D;
import util.Colors;


@NodeInfo(type = NodeCategory.ANIM)
public class Array extends Dupli {

	private final TransformInput in_transOffset;
	private final NumberInputIF in_length;
	private final PMatrix2D offset = new PMatrix2D();
	
	public Array() {
		in_transOffset = addTransformInput();
		in_transOffset.setName("Offset");
		in_length = addInput("Length", INTEGER_POSITIVE).setDefaultValue(3);
	}
	
	
	@Override
	public DisplayObjectIF createAnim() {
		
		var d = new DupliAnim() {
			@Override
			public void render(PGraphics g) {
				
				PMatrix2D m = in_transOffset.getMatrix();
				offset.reset();
				
				int len = (int) in_length.getValue();
				
				
				g.push();
				g.tint(Colors.WHITE);
				
				for (int i = 0; i < len; i++) {
					g.resetMatrix();
					g.translate(getX(), getY());
					
					g.applyMatrix(in_trans.getMatrix());
					g.applyMatrix(offset);
					g.translate(-source.getX(), -source.getY());
					
					if(i > 0) 
						g.tint(Colors.multiply(g.tintColor, in_color.getColor()));
					
					source.getAnim().render(g);
					
					offset.apply(m);
				}
				g.tint(Colors.WHITE);
				g.pop();
				
			}
		};
		
		return d;
	}
	
	@Override
	public void processIO() {
		super.processIO();
		
		in_transOffset.getMatrixInput();
		in_length.getWildInput();
	}
	
	
}
