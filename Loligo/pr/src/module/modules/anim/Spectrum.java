package module.modules.anim;

import static java.lang.Math.max;

import java.util.Arrays;

import constants.Units;
import gui.nodeinspector.NodeInspector;
import gui.text.CharProfile;
import gui.text.NumberInputField;
import lombok.var;
import module.AnimObject;
import module.ModuleDeleteMode;
import module.inout.ColorInputIF;
import module.inout.TransformInput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.analysis.featureextractors.FFT;
import net.beadsproject.beads.analysis.featureextractors.PowerSpectrum;
import net.beadsproject.beads.analysis.segmenters.ShortFrameSegmenter;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.EnterFrameListener;
import processing.core.PGraphics;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.ANIM)
public class Spectrum extends AnimObject implements EnterFrameListener {
	
	private PowerSpectrum ps;
	private final TransformInput in_trans;
	private final ColorInputIF in_color;
	private float[] data;
	private int offset;
	
	public Spectrum() {
		in_trans = addTransformInput();
		in_color = addColorInput();
	}
	
	@Override
	public void defaultSettings() {
		
		super.defaultSettings();
		data = new float[32];
	}
	
	@Override
	public void postInit() {
		super.postInit();
		constructFFT();
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		
		setAnim(new DisplayObject() {
			@Override 
			public void render(PGraphics g) {
				
				g.fill(getColor());
				g.strokeWeight(1);
				g.pushMatrix();
				g.translate(getX(), getY());
				g.applyMatrix(in_trans.getMatrix());
				
				float w = (float) getWidth() / data.length;
				
				for(int i = 0; i < data.length; i++) {
					float v = data[i];
					float h = - v *.5f; 
					g.rect(0, h, w * i, v);
				}
				
				g.noStroke();
				g.popMatrix();
			}
		});
		getAnim().setWH((int) Units.LENGTH.normFactor * 2, 0);
		
		return getAnim();
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {

		NodeInspector ni = NodeInspector.newInstance(this, n -> {
			
			NumberInputField nf = new NumberInputField(d -> setLength(d.intValue()), () -> (double) data.length);
			nf.setCharProfile(CharProfile.INT);
			nf.setRange(1, 512);
			nf.update();
			n.addLabeledEntry("Width", nf);
			
			NumberInputField no = new NumberInputField(d -> offset = d.intValue(), () -> (double) offset);
			no.setCharProfile(CharProfile.INT);
			no.setRange(0, 512);
			no.update();
			n.addLabeledEntry("Offset", no);
		});
		

		return ni;
	}
	
	
	private void constructFFT() {
		var am = getDomain().getAudioManager();
		ShortFrameSegmenter sfs = new ShortFrameSegmenter(am.getAC());
		sfs.addInput(am.getMasterBead());
		FFT fft = new FFT();
		sfs.addListener(fft);
		ps = new PowerSpectrum();
		fft.addListener(ps);
		am.getMasterBead().addDependent(sfs);
	}
	
	private void setLength(int len) {
		
		if(len != data.length) {
			data = new float[len];
		}
	}
	
	private void resample(float[] ff) {
		
		float w = (float) data.length / ff.length;
		Arrays.fill(data, 0);
		int len = ff.length - offset;
		
		for (int i = 0; i < len; i++) {
			
			int j = (int) (i * w);
			data[j] = max(ff[i + offset], data[j]);
		}
	}

	@Override
	public void processIO() {
		in_trans.getMatrixInput();
		getAnim().setColor(in_color.getColorInput());
	}
	
	
	@Override
	public void enterFrame() {
		float[] d = ps.getFeatures();
		if(d != null)
			resample(d);
	}
	
	
	@Override
	public void onAdd() {
		super.onAdd();
		getDomain().getDisplayManager().addEnterFrameListener(this);
	}
	

	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		getDomain().getDisplayManager().removeEnterFrameListener(this);
	}
	

	@Override
	public int pipette(int x, int y) {
		return 0;
	}

	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
			.addExtras(offset, data.length);
		
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		offset = data.nextInt();
		int len = data.nextInt();
		setLength(len);
	}
	
}

