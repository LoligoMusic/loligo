package module.modules.anim;

import constants.Flow;
import module.AbstractModule;
import module.Canvas;
import module.inout.ColorInputIF;
import module.inout.TriggerInputIF;
import patch.Domain;
import patch.DomainType;

public class CanvasNode extends AbstractModule {
	
	private Canvas canvas;
	
	private final TriggerInputIF in_set;
	private final ColorInputIF in_col;
	
	
	public CanvasNode() {
	
		flowFlags.add(Flow.SINK);
		
		in_set = addTrigger();
		
		in_col = addColorInput();
		in_col.setColor(Canvas.TRANSPARENT);
	}
	
	
	@Override
	public void postInit() {
	
		super.postInit();
		
		Domain d = getDomain();
		
		canvas = d.domainType() == DomainType.PARTICLE_FACTORY ? Canvas.createDeadCanvas() :
																 d.getDisplayManager().canvas();
	}

	
	@Override
	public void processIO() {
		
		if(in_set.triggered()) {
			
			canvas.reset(in_col.getColorInput());
		}
	}
	

}
