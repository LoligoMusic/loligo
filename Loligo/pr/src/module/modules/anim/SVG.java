package module.modules.anim;

import java.net.MalformedURLException;
import java.net.URL;

import gui.nodeinspector.NodeInspector;
import gui.text.FileInputField;
import lombok.Getter;
import lombok.var;
import module.AnimObject;
import module.ModuleDeleteMode;
import module.inout.TransformInput;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import processing.core.PGraphics;
import processing.core.PMatrix2D;
import processing.core.PShape;
import save.Asset.Status;
import save.AssetContainer;
import save.AssetRegistry;
import save.AssetSource;
import save.ModuleData;
import save.RestoreDataContext;
import save.SVGData;
import save.SaveDataContext;


@NodeInfo(type = NodeCategory.ANIM)
public class SVG extends AnimObject implements AssetContainer{

	private PShape shape;
	@Getter
	private AssetSource assetSource;
	private final TransformInput in_trans;
	private final PMatrix2D matrix = new PMatrix2D();
	
	
	public SVG() {
		
		in_trans = addTransformInput();
	}

	@Override
	public void defaultSettings() {
		super.defaultSettings();
		shape = emptyShape();
	}
	
	private PShape emptyShape() {
		return getDomain().getPApplet().createShape();
	}
	
	@Override
	public void loadAssetFromURL(URL f) {
		
		if(f == null)
			return;
		SVGData s = AssetRegistry.loadSVG(f);
		assetSource = s.getFile();
		shape = s.getStatus() == Status.SUCCESS ? s.getShape(): emptyShape();
	}
	
	
	public URL getURL() {
		return assetSource == null ? null : assetSource.getUrl();
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {

		NodeInspector ni = NodeInspector.newInstance(this, n -> {
			n.createSection("Path");
			var f = new FileInputField(SVG.this::loadAssetFromURL, SVG.this::getURL);
			f.setFileDialog(AssetRegistry.createSVGDialog(null));
			
			n.addInspectorElement(f);
		});
		
		return ni;
	}
	
	@Override
	public DisplayObjectIF createAnim() {
		
		var d = new DisplayObject() {
			@Override
			public void render(PGraphics g) {

				g.pushMatrix();
				g.translate(getX(), getY());
				g.applyMatrix(matrix);
				g.shape(shape);
				g.popMatrix();
			}
		};
		
		return d;
	}

	@Override
	public void processIO() {

		//if(in_trans.changed()) {
			in_trans.getMatrixInput(matrix);
		//}
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		super.onDelete(mode);
		if(assetSource != null)
			AssetRegistry.releaseAsset(assetSource.getUrl());
	}
	
	@Override
	public int pipette(int x, int y) {
		return 0;
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		String p = assetSource == null ? "" : assetSource.toString();
		return super.doSerialize(sdc).addExtras(p);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);

		String p = data.nextString();
		
		try {
			URL u = new URL(p);
			loadAssetFromURL(u);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	
}
