package module.modules.audio;

import audio.AudioManager;
import audio.AudioModule;
import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import gui.text.NumberInputField;
import module.inout.AudioInput;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.WavePlayer;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.Notes;

@NodeInfo(type = NodeCategory.AUDIO)

public class Wave extends AudioModule{
	
	
	static {
		/*
		new BufferFactory() {
			
			@Override
			public String getName() {
				return "Buzz";
			}
			
			@Override
			public Buffer generateBuffer(int bufferSize) {
				
				Buffer b = new Buffer(128) {
				
					public float getValueFraction(float f) {
						
						float a = 0;
						
						if(f > .1f)
							a = 1;
						else if(f > .2f){
							a = -1;
						}
						
						return a;
					}
				};
				
				return b;
			}
		}.getDefault();*/
		
		//bufferNames = Buffer.staticBufs.keySet().toArray(new String[Buffer.staticBufs.keySet().size()]);
		
	}
	
	
	public static final String[] bufferNames = Buffer.staticBufs.keySet().toArray(new String[Buffer.staticBufs.keySet().size()]);
	private static final float maxGlideValue = 200;
	
	private int bufferIndex = bufferNames.length - 1;
	private float glideTime = 60f;
	
	//private Glide gainGlide, freqGlide;
	private WavePlayer wp;
	
	private final AudioInput in_freq, in_gain;
	
	
	private DropDownMenu bufferMenu;
	private NumberInputField glideSlider;
	
	
	public Wave() {
		
		in_freq = addControlInput("Pitch", NumericAttribute.NOTE).setDefaultValueAudio(Notes.A4);
		in_gain = addControlInput("Gain", NumericAttribute.NORMALIZED);//addControlInput("Gain", NumericAttribute.NORMALIZED);
	}
	
	@Override
	public void processIO() {
		
		super.processIO();
		
		//in_freq.updateValue();
		wp.setFrequency(in_freq.getUGen());
	}

	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector ni = NodeInspector.newInstance(this, n -> {
		
			bufferMenu = new DropDownMenu(this::setBuffer, () -> bufferIndex, bufferNames);
			bufferMenu.setWH(50, bufferMenu.getHeight());
			bufferMenu.adaptSize();
			
			glideSlider = new NumberInputField(
					d -> setGlideTime(d.floatValue() * maxGlideValue + Float.MIN_VALUE), 
					()-> (double) glideTime / maxGlideValue);
			glideSlider.setRange(0, 1);
			
			n.addLabeledEntry("Waveform", bufferMenu);
			n.addLabeledEntry("Glide", glideSlider);
		});
		return ni;
	}
	
	@Override
	public void postInit() {
		
		super.postInit();
		setBuffer(bufferIndex);
	}
	
	@Override
	public void beadConstruct() {
		
		//gainGlide = new Glide(audioContext, 0f, 20);
		//freqGlide = new Glide(audioContext, 500, glideTime);
		
		wp = new WavePlayer(audioContext, AudioManager.DUMMY_UGEN, Buffer.staticBufs.get(bufferNames[bufferIndex]));
		
		
		
		setBuffer(bufferIndex);
		 
		Gain gain = new Gain(audioContext, 1, AudioManager.DUMMY_UGEN);
		
		gain.addInput(wp);
		beadIn = wp;
		beadOut = gain;
		
		in_freq.onChangeAudio(wp::setFrequency);
		in_gain.onChangeAudio(gain::setGain);
		
		//addGlideInput(in_freq, freqGlide, 20, 6000).setIsPitch().setUpdateImmediately();
		//addGlideInput(in_gain, gainGlide, 0, .2f);
		
	}

	public UGen getFreqUgen() {
		
		return null;//freqGlide;
	}
	
	@Override
	public void insertFreqUgen(UGen ugen) {
		
		if(ugen == null) {
			
			//wp.setFrequency(freqGlide);
			
		}else {
			
			//Add freq = new Add(audioContext, freqGlide, ugen);
			
			//wp.setFrequency(freq);
		}
		
		
	}
	
	public void setBuffer(int i) {
		bufferIndex = i;
		Buffer buf = Buffer.staticBufs.get(bufferNames[bufferIndex]);
		
		
		wp.setBuffer(buf);
		if(bufferMenu != null)
			bufferMenu.update();
	}
	
	public void setGlideTime(float g) {
		
		glideTime = g; 
		//freqGlide.setGlideTime(glideTime);
		
		if(glideSlider != null)
			glideSlider.update();
	}


	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		return super.doSerialize(sdc).addExtras(
			glideTime, 
			bufferNames[bufferIndex]
		);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		
		setGlideTime(data.nextFloat());
		String w = data.nextString();
		
		for (int i = 0; i < bufferNames.length; i++) {
			if(bufferNames[i].equals(w)) {
				setBuffer(i);
				break;
			}
		}
		
	} 
	
}
