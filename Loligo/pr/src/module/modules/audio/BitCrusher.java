package module.modules.audio;

import static audio.AudioType.EFFECT;
import static module.inout.NumericAttribute.NORMALIZED;

import audio.AudioManager;
import audio.AudioModule;
import lombok.Setter;
import module.inout.AudioInput;
import module.inout.NumberInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.Plug;
import net.beadsproject.beads.ugens.RMS;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class BitCrusher extends AudioModule {
	
	private final AudioInput in_bit, in_sample;
	private final NumberInputIF in_smooth;
	private BF bf;
	
	public BitCrusher() {
		
		setAudioType(EFFECT);
		
		in_bit = addControlInput("BitDepth", NORMALIZED);
		in_sample = addControlInput("SampleRate", NORMALIZED);
		in_smooth = addInput("Smooth");
	}
	
	@Override
	public void processIO() {
		super.processIO();
		
		bf.smooth = 1 + (int) ((bf.smps.length - 2) * in_smooth.getValue());
	}
	
	@Override
	public void beadConstruct() {
		
		var plug = new Plug(audioContext, 1);
		
		bf = new BF(audioContext, plug);
		in_bit.onChangeAudio(bf::setBitController);
		in_sample.onChangeAudio(bf::setSampleController);
		
		bf.addInput(plug);
		
		beadIn = plug;
		beadOut = bf;
	}
	
	
	private class BF extends UGen {
		
		@Setter
		UGen sampleController = AudioManager.DUMMY_UGEN, bitController = AudioManager.DUMMY_UGEN;
		final RMS rms;
		final float[] smps = new float[15];
		int smooth;
		int smpsIdx;
		
		float samp = 0, sc = 0;
		
		public BF(AudioContext ac, UGen input) {
			super(ac, 1, 1);
			rms = new RMS(ac, 1, 512);
			rms.addInput(input);
		}
		
		@Override 
		public void calculateBuffer() {
			
			sampleController.update();
			bitController.update();
			rms.update();
			
			float[] bitBuf = bitController.getOutBuffer(0),
					sampBuf = sampleController.getOutBuffer(0),
					rmsBuf = rms.getOutBuffer(0);
			
			float sum;
			
			for (int i = 0; i < bufferSize; i++) {
				
				if(sc >= 1) {
					
					float bits = bitBuf[i]; 
					bits = mapBits(bits);
					
					float bi = bufIn[0][i];
					
					float s = ((int) (bi * bits + 8) - 8) / bits; //+-8 = fast floor
					
					samp = s * rmsBuf[i];//(samp + s) / 2;
					sc--;
				}
				float skipSamples = sampBuf[i];
				skipSamples = mapSamples(skipSamples);
				sc += skipSamples;
				
				smpsIdx = (smpsIdx + 1) % smps.length;
				smps[smpsIdx] = samp;
				sum = 0;
				for (int j = 0; j < smooth; j++) {
					sum += smps[j];
				}
				
				bufOut[0][i] = sum / smooth;
			}
		}
		
		private final float mapBits(float f) {
			//float highestBitrate = (float) Math.pow(2, 16);
			
			//if(f > 1) {
			//	return highestBitrate;
			//}else {
			f = 1 - f;
			f = f * f * f;
			return 1 - f * 1024;//1f - (float) Math.log(Math.pow(f + (1 - f) * .001, .7)) * 100;
			//}
		}
		
		private final float mapSamples(float f) {
			float maxSamp = .8f;
			return (float)Math.pow(1 - f * maxSamp, 4) ;
		}
	}
	
}
