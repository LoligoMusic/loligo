package module.modules.audio;

import static audio.AudioType.SOURCE;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer.Info;

import audio.AudioManager;
import audio.AudioModule;
import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import module.inout.AudioInput;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.core.io.JavaSoundAudioIO;
import net.beadsproject.beads.ugens.Gain;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.AUDIO)

public class Micro extends AudioModule{
	
	private DropDownMenu mixerMenu;
	
	private Info[] inf = AudioSystem.getMixerInfo();
	private int mixerIndex;
	private final AudioInput in_gain;
	
	public Micro() {
		setAudioType(SOURCE); 
		in_gain = addControlInput("Gain", NumericAttribute.NORMALIZED);
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector n = NodeInspector.newInstance(this, ni -> {
			
			ni.createSection("Input Device");
			
			var d = new DropDownMenu(this::chooseInputDevice, () -> mixerIndex, this::audioInputNames);
			
			ni.addInspectorElement(d);
			ni.postInit();
		});
		
		return n;
	}
	
	
	public void chooseInputDevice(int i) {
		
		JavaSoundAudioIO jav = (JavaSoundAudioIO) audioContext.getAudioIO();
		jav.selectMixer(i);
		
		mixerIndex = i;
		if(mixerMenu != null)
			mixerMenu.update();
	}
	
	@Override
	public void beadConstruct() {

		UGen mic = audioContext.getAudioInput(new int[] {2});
		
		Gain g = new Gain(audioContext, 1, AudioManager.DUMMY_UGEN);
		
		in_gain.onChangeAudio(g::setGain);
		
		g.addInput(0, mic, 0);
		g.addInput(0, mic, 0);
		
		beadIn = beadOut = g;
	}
	
	
	public String[] audioInputNames() {
		var names = new String[inf.length];
		for (int i = 0; i < names.length; i++) {
			names[i] = inf[i].getName();
		}
		return names;
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		String d = inf[mixerIndex].getName();
		return super.doSerialize(sdc).addExtras(d);
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		String d = data.nextString();
		for (int i = 0; i < inf.length; i++) 
			if(d.equals(inf[i].getName())) {
				mixerIndex = i;
				return;
			}
	}

}
