package module.modules.audio;

import static audio.AudioType.EFFECT;
import audio.AudioModule;
import module.inout.NumberInputIF;
import module.inout.SwitchInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.ugens.Plug;
import net.beadsproject.beads.ugens.TapIn;
import net.beadsproject.beads.ugens.TapOut;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Retrigger extends AudioModule {
	
	private static final int MAX_SAMPLES = 2000, MIN_SAMPLES = 20;
	private final SwitchInputIF sw;
	private final NumberInputIF in_length;
	private transient TapIn ti;
	private transient TapOut to; 
	
	public Retrigger() {
		setAudioType(EFFECT);
		sw = addSwitch();
		in_length = addInput("length");
		//in_length.max = MAX_LENGTH;
	}
	
	@Override
	public void processIO() {
		if(in_length.changed()) {
			float f = MIN_SAMPLES + (float)in_length.getInput() * (MAX_SAMPLES - MIN_SAMPLES);
			to.setDelay(f);
		}
		
		if(sw.switchedOn()) {
			
			beadOut.clearInputConnections();
			beadOut.addInput(to);
		}else 
		if(sw.switchedOff()) {
			
			beadOut.clearInputConnections();
			beadOut.addInput(beadIn);
		}
	}
	
	@Override
	public void beadConstruct() {
		beadIn = new Plug(audioContext);
		beadOut = new Plug(audioContext);
		
		ti = new TapIn(audioContext, MAX_SAMPLES);
	
		to = new TapOut(audioContext, ti, 0) {
			
			int numSamples, count = numSamples;
			float sampsPerMS = (float) audioContext.msToSamples(1);
			final float steps = 150;
			boolean b = false;
			
			@Override public void calculateBuffer() {
				
				float[] bi = bufOut[0];
				ti.fillBufferLinear(bi, count);
				count -= bufferSize;
				if(count < 0) {
					count = numSamples;
					b = true;
					for (int i = 0; i < steps; i++) { 
						int t = bi.length - 1 - i;
						bi[t] = bi[t] * (i / steps);
					}
				}else if(b) {
					b = false;
					for (int i = 0; i < steps; i++) 
						bi[i] = bi[i] * (i / steps);
				}
			}
			
			@Override
			public TapOut setDelay(float delay) {
				numSamples = (int) (delay * sampsPerMS + .5);
				return super.setDelay(delay);
			}
		};
		to.removeDependent(ti);
		ti.addInput(beadIn);
		beadOut.addInput(beadIn);
		beadIn.addDependent(ti);
	}

	
}
