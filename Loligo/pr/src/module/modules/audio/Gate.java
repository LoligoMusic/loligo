package module.modules.audio;

import audio.AudioModule;
import static audio.AudioType.EFFECT;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.ugens.BiquadFilter;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.Plug;
import net.beadsproject.beads.ugens.RMS;

@NodeInfo(type=NodeCategory.AUDIO_FX)
public class Gate extends AudioModule {

	private final NumberInputIF in_attack, in_release, in_treshold;
	
	private float treshold, attack, release;
	
	public Gate() {
		
		setAudioType(EFFECT);
		
		in_treshold = addInput("Treshold");
		in_attack = addInput("Attack");
		in_release = addInput("Release");
	}

	@Override
	public void processIO() {
	
		attack = (float) in_attack.getInput() * 100f;
		release = (float) in_release.getInput() * 100f;
		treshold = (float) in_treshold.getInput();
	}
	
	@Override
	public void beadConstruct() {
		
		Plug plug = new Plug(audioContext, 1);
		
		/*
		Power p = new Power();
		ShortFrameSegmenter sfs = new ShortFrameSegmenter(audioContext);
		sfs.addListener(p);
		sfs.addInput(plug);
		*/
		
		net.beadsproject.beads.ugens.Gain gain = new net.beadsproject.beads.ugens.Gain(audioContext, 1);
		gain.addInput(plug);
		
		Glide gainGlide = new Glide(audioContext, 0);
		gain.setGain(gainGlide);
		
		RMS rms = new RMS(audioContext, 1, 500);
		rms.addInput(plug);
		
		BiquadFilter bf = new BiquadFilter(audioContext, 1, BiquadFilter.BUTTERWORTH_LP) {
			
			@Override
			public void calculateBuffer() {
				super.calculateBuffer();
				
				for (int i = 0; i < bufferSize; i++) {
					
					if(bufOut[0][i] > treshold) {
						
						gainGlide.setGlideTime(attack);
						gainGlide.setValue(1);
						
					}else {
						gainGlide.setGlideTime(release);
						gainGlide.setValue(0);
					}
				}
				
			}
		}.setFrequency(31);
		bf.addInput(rms);
		
		gain.addDependent(bf);
		
		beadIn = plug;
		beadOut = gain;
	}

}
