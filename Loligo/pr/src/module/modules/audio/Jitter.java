package module.modules.audio;

import static audio.AudioType.CONTROL;
import static module.inout.NumericAttribute.DEZIMAL_POSITIVE;

import audio.AbstractAudioModule;
import module.inout.AudioOutput;
import module.inout.NumberInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;
import util.Simplex;

@NodeInfo(type = NodeCategory.AUDIO)
public class Jitter extends AbstractAudioModule {

	private final NumberInputIF in_freq;
	private final AudioOutput out;
	private Simp simp;
	
	public Jitter() {
		setAudioType(CONTROL);
		
		in_freq = addInput("Frequency", DEZIMAL_POSITIVE).setDefaultValue(.01);
		out = addControlOutput("Out");
	}
 	
	@Override
	public void processIO() {
		simp.freq = in_freq.getWildInput();
		
	}
	
	@Override
	public void beadConstruct() {
		
		simp = new Simp(audioContext);
		out.setUGen(simp);
	}
	
	private class Simp extends UGen{
		
		double freq, p;
		
		Simp(AudioContext ac) {
			super(ac, 1);
		}
		
		@Override
		public void calculateBuffer() {
			
			var bout = bufOut[0];
			
			for (int i = 0; i < bufferSize; i++) {
				p += freq;
				bout[i] = (float) (.5 + Simplex.generate(p * .001f) / 2);
			}
		}
	};

}
