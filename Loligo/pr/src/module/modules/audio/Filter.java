package module.modules.audio;

import static audio.AudioType.EFFECT;
import static module.inout.NumericAttribute.DEZIMAL_POSITIVE;
import static module.inout.NumericAttribute.NORMALIZED;

import audio.AudioModule;
import module.Module;
import module.inout.AudioInput;
import module.inout.EnumInput;
import module.inout.IOEnum;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.ugens.BiquadFilter;
import net.beadsproject.beads.ugens.BiquadFilter.Type;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Filter extends AudioModule {
	
	private static final Type[] types = Type.values();
	private static final IOEnum biquadFilters = IOEnum.createEnum(BiquadFilter.AP);
	//private static final NumberAttributes INT_RANGED = NumberAttributes.INTEGER_POSITIVE.inRange(0, types.length - 1);
	
	private final AudioInput in_freq, in_q, in_gain;
	private final EnumInput in_type;
	
	private BiquadFilter filter;
	
	public Filter() {
		
		setAudioType(EFFECT);
         
		in_freq = addControlInput("Frequency", DEZIMAL_POSITIVE).setDefaultValueAudio(.1);
		in_gain = addControlInput("Gain", NORMALIZED).setDefaultValueAudio(.5);
		in_q 	= addControlInput("Q", NORMALIZED).setDefaultValueAudio(.5);
		
		in_type = new EnumInput("Filter", this, biquadFilters);
		addIO(in_type);
    }

	@Override
	public void processIO() {
	
		super.processIO();
		
		if(in_type.changed()) {
			
			int i = (int) in_type.getWildInput();
			filter.setType(types[i]);
		}
	}
	
	
	@Override
	public void beadConstruct() {
		
		filter = new BiquadFilter(audioContext, 1, BiquadFilter.LP);
        
        in_freq.onChangeAudio(filter::setFrequency);
        in_q.onChangeAudio(filter::setQ);
        in_gain.onChangeAudio(filter::setGain);
        
        beadIn = filter;
        beadOut = filter;
   }
	
	@Override
	public Module createInstance() {
		return new Filter();
	}
}
