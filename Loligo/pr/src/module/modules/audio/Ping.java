package module.modules.audio;

import static module.inout.NumericAttribute.NOTE;

import audio.AudioModule;
import gui.nodeinspector.NodeInspector;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.TriggerInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.events.KillTrigger;
import net.beadsproject.beads.ugens.Envelope;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Throughput;
import net.beadsproject.beads.ugens.WavePlayer;
import pr.DisplayObjectIF;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import sound.ADSR;
import sound.Wave;

@NodeInfo(type = NodeCategory.AUDIO)

public class Ping extends AudioModule {
	
	
	private final NumberInputIF in_freq, in_gain, in_duration;
	private final TriggerInputIF in_trigger;
	
	private float volume = .5f, pitch = 500, duration = 100;
	transient private Throughput main;
	
	private Wave wave = new Wave();
	private ADSR adsr = new ADSR();
	
	private UGen freqUgen;
	
	public Ping() {
		in_trigger = addTrigger();
		in_freq = addInput("Pitch", NOTE).setDefaultValue(440);
		in_duration = addInput("Duration", NumericAttribute.DEZIMAL_POSITIVE).setDefaultValue(.5);
		in_gain = addInput("Gain").setDefaultValue(.5);
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		adsr.setAttack(10).setSustainVolume(.5f).setDecay(5).setSustain(100);
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector ni = NodeInspector.newInstance(this, n -> {
			
			n.addLabeledEntry("Waveform", wave.getGui());
			n.setTop(5);
			n.createSection("ADSR");
			DisplayObjectIF g = adsr.getGraph();
			n.addInspectorElement(g);
			n.setTop(3);
			n.addRow(adsr.getGui());
			adsr.updateDials();
		});
		
		return ni;
	}
	
	@Override
	public void beadConstruct() {
		beadIn = beadOut = main = new Throughput(audioContext);
	}
 	
	@Override
	public void processIO() {
		
		if(in_trigger.triggered()) {
			pitch = (float) in_freq.getWildInput();
			volume = (float) in_gain.getInput(); 
			duration = (float) in_duration.getWildInput(); 
			
			playNote();
			
		}
	}
	
	private void playNote() {
		
		WavePlayer wp = wave.buildWave(audioContext, pitch, freqUgen); //new WavePlayer(audioContext, pitch, Buffer.SAW);
		Gain gain = new Gain(audioContext, 1);
		
		adsr.setVolume(volume);
		
		Envelope env = adsr.buildEnvelope(audioContext, new KillTrigger(gain), duration);
		
		gain.setGain(env);
		
		gain.addInput(wp);
		main.addInput(gain);
	}
	
	
	@Override
	public void insertFreqUgen(UGen ugen) {
		
		freqUgen = ugen;
	}
	
	 

	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		return super.doSerialize(sdc).addExtras(
				wave.getCurrentBufferName(),
				adsr.getAttackTime(),
				adsr.getDecayTime(),
				adsr.getSustainTime(),
				adsr.getReleaseTime(),
				adsr.getVolume()
		);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		wave.setBuffer	(data.nextString());
		adsr.setAttack	(data.nextFloat());
		adsr.setDecay	(data.nextFloat());
		adsr.setSustain	(data.nextFloat());
		adsr.setRelease	(data.nextFloat());
		adsr.setVolume	(data.nextFloat());
	}
}
