package module.modules.audio;

import audio.AudioModule;
import audio.AudioType;
import module.inout.AudioOutput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.ugens.Plug;

@NodeInfo(type = NodeCategory.AUDIO_FX)
public class StackOut extends AudioModule {

	private final AudioOutput out;
	
	public StackOut() {
		setAudioType(AudioType.EFFECT);
		out = addControlOutput("Out");
	}
	
	@Override
	public void beadConstruct() {
		beadIn = beadOut = new Plug(audioContext, 1);
		out.setUGen(beadOut);
	}

}
