package module.modules.audio;

import audio.AbstractAudioModule;
import audio.AudioType;
import lombok.Setter;
import module.inout.AudioInput;
import module.inout.AudioOutput;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;

@NodeInfo(type = NodeCategory.AUDIO)
public class MultAudio extends AbstractAudioModule {

	private final AudioInput in1, in2;
	private final AudioOutput out;
	
	public MultAudio() {
		setAudioType(AudioType.CONTROL);
		
		in1 = addControlInput("In1", NumericAttribute.DEZIMAL_FREE);
		in2 = addControlInput("In2", NumericAttribute.DEZIMAL_FREE);
		out = addControlOutput("Out");
	}
	
	@Override
	public void beadConstruct() {
		
		var m = new M(audioContext);
		in1.onChangeAudio(m::setUgen1);
		in2.onChangeAudio(m::setUgen2);
		out.setUGen(m);
		
	}
	
	private class M extends UGen {
		
		@Setter
		UGen ugen1, ugen2;
		
		public M(AudioContext ac) {
			super(ac, 1);
		}
		
		@Override
		public void calculateBuffer() {
			
			ugen1.update();
			ugen2.update();
			float[] f1 = ugen1.getOutBuffer(0);
			float[] f2 = ugen2.getOutBuffer(0);
			float[] bout = bufOut[0];
			
			for (int i = 0; i < bufferSize; i++) {
				bout[i] = f1[i] * f2[i];
			}
		}
	}

}
