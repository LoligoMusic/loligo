package module.modules.audio;

import audio.AudioModule;
import audio.MixGlide;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.TapIn;
import net.beadsproject.beads.ugens.TapOut;
import net.beadsproject.beads.ugens.Throughput;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Echo extends AudioModule {
	
	
	@Override
	public void beadConstruct() {
		
		Throughput 	input = new Throughput(audioContext, 1),
					output = new Throughput(audioContext, 1);
		
		TapIn delayIn = new TapIn(audioContext, 2000);
		TapOut delayOut = new TapOut(audioContext, delayIn, 500.0f);
		
		Glide glide_delay = new Glide(audioContext);
		delayOut.setDelay(glide_delay);
		
		Glide glide_feedback = new Glide(audioContext);
		MixGlide glide_mix = new MixGlide(audioContext);
		
		Gain delayGain  	= new Gain(audioContext, 1, glide_mix),
			 feedbackGain 	= new Gain(audioContext, 1, glide_feedback),
			 dryGain 		= new Gain(audioContext, 1, glide_mix.getMixPartner());
		 
		dryGain.addInput(input);
		delayIn.addInput(input);
		delayIn.addInput(feedbackGain);
		
		delayGain.addInput(delayOut);
		feedbackGain.addInput(delayOut);
		
		output.addInput(delayGain);
		output.addInput(dryGain);
		
		beadIn = input;
		beadOut = output;
		
		addGlideInput(addInput("Dry/Wet"), glide_mix);
		addGlideInput(addInput("Sustain"), glide_feedback);
		addGlideInput(addInput("Delay"	), glide_delay, 0, 2000);
	}
	
	
	

}
