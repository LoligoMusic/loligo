package module.modules.audio;

import static module.inout.NumericAttribute.NORMALIZED;
import static module.inout.NumericAttribute.NOTE;

import audio.AudioManager;
import audio.AudioModule;
import module.inout.AudioInput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.TrapezoidWave;

@NodeInfo(type=NodeCategory.AUDIO)

public class Pulse extends AudioModule {

	private final AudioInput in_freq, in_attack, in_decay, in_duty, in_gain;
	
	public Pulse() {
		
		in_freq 	= addControlInput("Pitch",  NOTE);
		in_gain 	= addControlInput("Gain",   NORMALIZED).setDefaultValueAudio( 0);
		in_attack 	= addControlInput("Attack", NORMALIZED).setDefaultValueAudio(.1);
		in_decay 	= addControlInput("Decay",  NORMALIZED).setDefaultValueAudio(.1);
		in_duty 	= addControlInput("Duty",   NORMALIZED).setDefaultValueAudio(.5);
	}
	
	/*
	@Override
	public DisplayObjectIF createGUISpecial() {
		
		NodeInspector ni = NodeInspector.newInstance(this, n -> {
			
			DisplayObject d = new DisplayObject() {
				
				float w, h, x, y;
				float[] ff = new float[8];
				float att, dec, dut;
				
				@Override
				public void render() {
					
					PGraphics g = getDm().g;
					
					h = getHeight();
					w = getWidth();
					x = getX();
					y = getY();
					
					g.fill(getColor());
					g.rect(x, y, w, h);
					
					att = (float)in_attack.getValue();
					dec = (float)in_decay.getValue();
					dut = (float)in_duty.getValue();
					float s = att + dec + dut;
					att = w * att / s;
					dec = w * dec / s;
					dut = w * dut / s;
					
					ff[0] = x;
					ff[1] = y + h;
					
					ff[2] = x + att;
					ff[3] = y;
					
					ff[4] = ff[2] + dut;
					ff[5] = y;
					
					ff[6] = ff[4] + dec;
					ff[7] = y + h;
					
					g.noFill();
					g.stroke(0xffffffff);
					g.strokeWeight(.5f);
					
					for (int i = 0; i <= 4; i += 2) {
						g.line(ff[i], ff[i + 1], ff[i + 2], ff[i + 3]);
					}
					
				}
				
			};
			
			d.setWH(0, 100);
			d.setColor(0xff111115);
			d.setFlag(DisplayFlag.RECT, true);
			n.addInspectorElement(d);
			
		}
				
		);
		
		return ni;
	}
	*/
	
	
	@Override
	public void beadConstruct() {
		
		var wave = new TrapezoidWave(audioContext);
		var gain = new Gain(audioContext, 1, AudioManager.DUMMY_UGEN);
		gain.addInput(wave);
		
		in_freq		.onChangeAudio(wave::setFrequency);
		in_gain		.onChangeAudio(gain::setGain);
		in_attack	.onChangeAudio(wave::setAttack);
		in_decay	.onChangeAudio(wave::setDecay);
		in_duty		.onChangeAudio(wave::setDutyCycle);
		
		beadIn = wave;
		beadOut = gain;
	}
	
	/*
	@Override
	public void insertFreqUgen(UGen ugen) {
		
		if(ugen == null) {
			
			wave.setFrequency(glide_freq);
			
		}else {
			
			Add freq = new Add(audioContext, glide_freq, ugen);
			wave.setFrequency(freq);
		}
		
		
	}
	*/

}
