package module.modules.audio;

import static audio.AudioType.EFFECT;

import audio.AudioModule;
import module.inout.NumericAttribute;
import net.beadsproject.beads.ugens.CombFilter;
import net.beadsproject.beads.ugens.Glide;

//@InternalNode(type=NodeType.AUDIO_FX)

public class AllPass extends AudioModule {
	
	public AllPass() {
		setAudioType(EFFECT);
	}
	
	
	@Override
	public void beadConstruct() {
		
		Glide glide_a = new Glide(audioContext),
				glide_g = new Glide(audioContext),
						glide_h = new Glide(audioContext),
			  glide_del = new Glide(audioContext);
		
		CombFilter a = new CombFilter(audioContext, 2000);
		a.setA(glide_a).setDelay(glide_del).setG(glide_g).setH(glide_h);
		
		addGlideInput(addInput("del", NumericAttribute.DEZIMAL_POSITIVE), glide_del, 0, 2000);
		addGlideInput(addInput("A", NumericAttribute.DEZIMAL_POSITIVE), glide_a);
		addGlideInput(addInput("G", NumericAttribute.DEZIMAL_POSITIVE), glide_g);
		addGlideInput(addInput("H", NumericAttribute.DEZIMAL_POSITIVE), glide_h);
		
		beadIn = beadOut = a;
	}

}
