package module.modules.audio;

import audio.AbstractAudioModule;
import audio.AudioType;
import module.inout.AudioOutput;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;

@NodeInfo(type = NodeCategory.AUDIO)
public class Glide extends AbstractAudioModule {

	private final NumberInputIF in_value, in_time;
	private final AudioOutput out;
	private net.beadsproject.beads.ugens.Glide glide;
	
	public Glide() {
		
		setAudioType(AudioType.CONTROL);
		
		in_value = addInput("In", NumericAttribute.DEZIMAL_POSITIVE);
		in_time = addInput("Time", NumericAttribute.DEZIMAL_POSITIVE);
		
		out = addControlOutput("Out");
	}
	
	@Override
	public void processIO() {
		float v = (float) in_value.getWildInput();
		float t = (float) in_time.getWildInput();
		glide.setGlideTime(t * 1000);
		glide.setValue(v);
	}
	
	@Override
	public void beadConstruct() {
		
		glide = new net.beadsproject.beads.ugens.Glide(audioContext);
		out.setUGen(glide);
	}
	
}
