package module.modules.audio;

import static audio.AudioType.CONTROL;
import audio.AbstractAudioModule;
import audio.AudioManager;
import constants.Flow;
import lombok.var;
import module.Module;
import module.Modules;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.inout.AudioInput;
import module.inout.AudioOutput;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.Plug;

@NodeInfo(type = NodeCategory.AUDIO)
public class Mix extends AbstractAudioModule implements DynIOModule{

	private final static String inputName = "In";
	
	private final AudioOutput out;
	private Plug plug;
	
	public Mix() {
		setAudioType(CONTROL);
		flowFlags.add(Flow.DYNAMIC_IO);
		
		out = addControlOutput("Out");
	}
	
	@Override
	public void defaultSettings() {
		createDynIO(inputName, DataType.AUDIOCTRL, IOType.IN);
		createDynIO(inputName, DataType.AUDIOCTRL, IOType.IN);
		super.defaultSettings();
	}
	
	@Override
	public void beadConstruct() {
		plug = new Plug(audioContext, 1);
		out.setUGen(plug);
	}

	@Override
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		var in = new In(inputName, this);
		in.init();
		in.setOptional();
		addIO(in);
		rename(in, inputName);
		Modules.initIOGUI(this, in);
		return in;
	}

	@Override
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		var in = Modules.getInputByName(this, name);
		if(in == null)
			return null;
		removeIO(in);
		((In)in).remove();
		return in;
	}

	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.AUDIO_IN;
	}
	
	
	private class In extends AudioInput {

		UGen ugen = AudioManager.DUMMY_UGEN;
		
		In(String name, Module module) {
			super(name, module, NumericAttribute.DEZIMAL_FREE);
		}
		
		private void setUgen(UGen u) {
			remove();
			ugen = u;
			plug.addInput(u);
		}
		
		void init() {
			onChangeAudio(this::setUgen);
			setChanged();
		}
		
		void remove() {
			plug.removeAllConnections(ugen);
		}
	}

}
