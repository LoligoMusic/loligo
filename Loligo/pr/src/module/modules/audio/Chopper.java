package module.modules.audio;

import static audio.AudioType.EFFECT;

import audio.AudioModule;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.core.UGen;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Chopper extends AudioModule {
	
	private float rate, intens = .5f, maxRate = 80000;
	private final NumberInputIF in_rate, in_intens;
	
	public Chopper() {
		setAudioType(EFFECT);
		
		in_rate = addInput("Rate");
		in_intens = addInput("Intensity");
	}
	
	@Override
	public void beadConstruct() {
		UGen tremolo = new UGen(audioContext, 1, 1){
			int c = 0;
			float up = 1;
			@Override
			public void calculateBuffer() {
				//glideUgen.update();
				for(int i = 0; i < bufferSize; i++){
					
					bufOut[0][i] = bufIn[0][i] * (1f - intens * up);
						
					c++;
					if(c > rate) {
						up = (up + 1) % 2;
						c = -1;
					}
				}
			}
		};
		
		beadIn = tremolo;
		beadOut = tremolo;
	}
	
	@Override
	public void processIO() {
		if(in_rate.changed()) 
			rate = (float) (maxRate - in_rate.getInput() * maxRate);
		if(in_intens.changed()) 
			intens = (float) (in_intens.getInput());
	}

}


