package module.modules.audio;

import audio.AudioModule;
import gui.particles.ParticleSystemRegistry;
import module.ModuleDeleteMode;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type = NodeCategory.AUDIO)

public class PAudio extends AudioModule {

	private static int instanceCount;
	
	@Override
	public void onAdd() {
		
		if(instanceCount == 0) {
			ParticleSystemRegistry.defaultParticleSystem()
				.getParticleAudioManager().disconnect();
		}
		
		instanceCount++;
		
		super.onAdd();
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		
		super.onDelete(mode);
		
		instanceCount--;
		
		if(instanceCount == 0) {
			ParticleSystemRegistry.defaultParticleSystem()
			 .getParticleAudioManager().connect();
		}
	}
	
	@Override
	public void beadConstruct() {
		
		beadIn = 
		beadOut = ParticleSystemRegistry.defaultParticleSystem()
				 .getParticleAudioManager().getMasterBead();
		
	}

}
