package module.modules.audio;

import static audio.AudioType.EFFECT;

import audio.AudioModule;
import gui.Dial;
import gui.nodeinspector.NodeInspector;
import lombok.Getter;
import module.inout.NumberInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Reverb extends AudioModule {
	
	private net.beadsproject.beads.ugens.Reverb rev;
	
	private transient Dial sizeDial, dampDial, earlyDial, lateDial;
	@Getter
	private float size, damp, early, late;
	private final NumberInputIF in_wet;
	
	public Reverb() {
		
		setAudioType(EFFECT);
		in_wet = addInput("Dry/Wet");
		
		size = .5f;
		damp = .7f;
		early = 1;
		late = 1;
		
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
	
		NodeInspector n = NodeInspector.newInstance(this);
		
		sizeDial = new Dial( d->setRoomSize ( d.floatValue()), ()->(double)size , "size");
		dampDial = new Dial( d->setDamp     ( d.floatValue()), ()->(double)damp , "damp");
		earlyDial = new Dial(d->setEarlyRefl( d.floatValue()), ()->(double)early, "early");
		lateDial = new Dial( d->setLateRefl ( d.floatValue()), ()->(double)late , "late");
		
		updateParams();
		
		n.setTop(8);
		
		n.addRow(sizeDial, dampDial, earlyDial, lateDial);
			
		return n;
	}
	
	@Override
	public void beadConstruct() {
		
		/*
		Throughput input = new Throughput(audioContext, 1);
		Throughput output = new Throughput(audioContext, 1);
		
		MixGlide wetGainGlide = new MixGlide();
		Glide dryGainGlide = wetGainGlide.getMixPartner();
		Gain dryGain = new Gain(audioContext, 1, dryGainGlide);
		Gain wetGain = new Gain(audioContext, 1, wetGainGlide);
		*/
		
		rev = new net.beadsproject.beads.ugens.Reverb(audioContext, 1);
		
		crossFader(rev, in_wet);
		
		//build(in_wet, rev);
		/*
		beadIn = beadOut = ug;
		
		
		dryGain.addInput(input);
		wetGain.addInput(input);
		rev.addInput(wetGain);
		output.addInput(dryGain);
		output.addInput(rev);
		
		beadIn = input;
		beadOut = output;
		
		addGlideInput(in_wet, wetGainGlide);
		*/
		updateParams();
	}
	
	public void setRoomSize(final float s) {
		size = s;
		rev.setSize(size);
		if(sizeDial != null)
			sizeDial.update();
	}
	
	public void setDamp(final float s) {
		damp = s;
		rev.setDamping(damp);
		if(dampDial != null)
			dampDial.update();
	}
	
	public void setEarlyRefl(final float s) {
		early = s;
		rev.setEarlyReflectionsLevel(early);
		if(earlyDial != null)
			earlyDial.update();
	}
	
	public void setLateRefl(final float s) {
		late = s;
		rev.setLateReverbLevel(late);
		if(lateDial != null)
			lateDial.update();
	}
	
	private void updateParams() {
		setDamp(damp);
		setEarlyRefl(early);
		setLateRefl(late);
		setRoomSize(size);
	}
	
	
	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		return super.doSerialize(sdc)
					.addExtras(size, damp, early, late);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		setRoomSize (data.nextFloat());
		setDamp	    (data.nextFloat());
		setEarlyRefl(data.nextFloat());
		setLateRefl (data.nextFloat());
	}
	
	
}
