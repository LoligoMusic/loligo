package module.modules.audio;

import audio.AudioModule;
import static audio.AudioType.EFFECT;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.ugens.Clip;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Limiter extends AudioModule {
	
	private final NumberInputIF in_range;
	private Clip clip;
	
	public Limiter() {
		setAudioType(EFFECT);
		
		in_range = addInput("Treshhold");
	}
	
	@Override
	public void processIO() {
		
		float f = (float) in_range.getInput();
		clip.setMinimum(-f);
		clip.setMaximum(f);
	}
	
	@Override
	public void beadConstruct() {
		
		clip = new Clip(audioContext);
		beadOut = beadIn = clip;
	}
	
}
