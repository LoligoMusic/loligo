package module.modules.audio;

import static audio.AudioManager.DUMMY_UGEN;
import static module.inout.NumericAttribute.NORMALIZED_PN;
import static module.loading.NodeCategory.AUDIO_FX;
import java.util.function.Consumer;
import audio.AudioModule;
import audio.AudioType;
import module.inout.AudioInput;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.Plug;

@NodeInfo(type = AUDIO_FX)
public class StackIn extends AudioModule {
	
	private final AudioInput in;
	private UGen currentInput = DUMMY_UGEN;
	
	public StackIn() {
		setAudioType(AudioType.SOURCE);
		in = addControlInput("In", NORMALIZED_PN);
	}
	
	@Override
	public void beadConstruct() {
		
		beadIn = beadOut = new Plug(audioContext, 1);
		
		Consumer<UGen> c = g -> {
			beadIn.removeAllConnections(currentInput);
			currentInput = g;
			beadIn.addInput(g);
		};
		
		in.onChangeAudio(c);
	}

}
