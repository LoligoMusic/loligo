package module.modules.audio;

import static audio.AudioType.EFFECT;
import static module.inout.NumericAttribute.DEZIMAL_POSITIVE;

import java.util.function.Consumer;

import audio.AudioModule;
import module.inout.AudioInput;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.Add;
import net.beadsproject.beads.ugens.BiquadFilter;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class PeakEQ extends AudioModule {
	
	private final AudioInput in_freq, in_q, in_gain;
	
	public PeakEQ() {
		setAudioType(EFFECT);
		
		in_freq = addControlInput("Frequency", DEZIMAL_POSITIVE).setDefaultValueAudio(100);
		in_q = addControlInput("Q", DEZIMAL_POSITIVE).setDefaultValueAudio(.5f);
		in_gain = addControlInput("Gain", DEZIMAL_POSITIVE);
	}
	
	@Override
	public void beadConstruct() {
		
		BiquadFilter filter = new BiquadFilter(audioContext, 1, BiquadFilter.PEAKING_EQ);
		
		in_freq.onChangeAudio(filter::setFrequency);
		in_gain.onChangeAudio(filter::setGain);
		
		Consumer<UGen> c = g -> {
			var a = new Add(audioContext, 1, 0.001f);
			a.addInput(g);
			filter.setQ(a);
		};
		in_q.onChangeAudio(c);
		
		beadIn = beadOut = filter;
	}
	
}
