package module.modules.audio;

import static audio.AudioType.SOURCE;

import java.util.List;
import audio.AudioModule;
import constants.AudioStackContext;
import constants.AudioStackContext.AudioStackEvent;
import constants.AudioStackListener;
import module.ModuleDeleteMode;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.Plug;
import net.beadsproject.beads.ugens.WavePlayer;

@NodeInfo(type=NodeCategory.AUDIO_FX)
public class Vibrato extends AudioModule implements AudioStackListener{

	private AudioModule freqTarget;
	private Gain vibGain;
	
	private final NumberInputIF in_intens, in_freq;
	
	public Vibrato() {
		
		in_intens = addInput("Intensity");
		in_freq = addInput("Frequency");
		
	}

	@Override
	public void beadConstruct() {
		
		beadIn = beadOut = new Plug(audioContext);
		
		Glide vibFreqGlide = new Glide(audioContext, 10, 20);
		Glide vibIntensGlide = new Glide(audioContext, .5f, 20);
		WavePlayer vibrato = new WavePlayer(audioContext, vibFreqGlide, Buffer.SINE);
		vibGain = new Gain(audioContext, 1, vibIntensGlide);
		vibGain.addInput(vibrato);
		
		addGlideInput(in_freq, vibFreqGlide, 0, 100);
		addGlideInput(in_intens, vibIntensGlide, 0, 100);
	}
	
	@Override
	public void addedToStack() {
		super.addedToStack();
		
		findTarget();
	}
	
	@Override
	public void removedFromStack() {
		super.removedFromStack();
		
		if(freqTarget != null)
			freqTarget.insertFreqUgen(null);
	}
	
	private void findTarget() {
		
		List<AudioModule> mods = getStack().getContainedModules();
		
		int mi = mods.indexOf(this);
		
		for (int i = mi - 1; i >= 0 ; i--) {
			
			AudioModule m = mods.get(i);
			
			if(m.getAudioType() == SOURCE) {
				
				setFreqTarget(m);
				return;
			}
		}
		
		if(freqTarget != null) {
			freqTarget.insertFreqUgen(null);
			freqTarget = null;
		}
	}
	
	private void setFreqTarget(AudioModule m) {
		
		if(freqTarget != null)
			freqTarget.insertFreqUgen(null);
		
		freqTarget = m;
		m.insertFreqUgen(vibGain);
	}


	@Override
	public void audioStackEvent(AudioStackEvent ase) {
		
		if(getStack() != null && getStack() == ase.stack() && ase.module() != this) {
		
			findTarget();
		}
	}
	
	@Override
	public void onAdd() {
	
		super.onAdd();
		
		AudioStackContext.instance.addListener(this);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		
		super.onDelete(mode);
		AudioStackContext.instance.removeListener(this);
	}

}
