package module.modules.audio;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.function.Consumer;
import java.util.function.Supplier;

import audio.AudioManager;
import audio.Recorder;
import errors.LoligoException;
import gui.nodeinspector.NodeInspector;
import gui.text.FileInputField;
import lombok.Getter;
import lombok.Setter;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.inout.SwitchInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import pr.RootClass;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;
import util.AsyncFileDialog;

@NodeInfo(type=NodeCategory.AUDIO_FX)
public class Record extends AbstractModule {

	private final SwitchInputIF in_switch;
	
	private final Recorder recorder;
	
	@Getter@Setter
	private File file;
	
	public Record() {
		
		in_switch = addSwitch();
		
		AudioManager am = RootClass.audioManager();
		recorder = new Recorder(am.getAC(), am.getMasterBead());
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector ni = NodeInspector.newInstance(this, 
		
			n -> {
			
				Consumer<URL> cf = u -> {
					
					if(u == null)
						return;
					
					try {
						File f = new File(u.toURI());
						setFile(f);
					} catch (URISyntaxException e) {
						if(getModuleGUI() != null)
							getModuleGUI().unmarkError();
						e.printStackTrace();
					}
				};
				
				Supplier<URL> sup = () -> {
					
					if(file == null)
						return null;
					
					try {
						URL u = file.toURI().toURL();
						return u;
					} catch (MalformedURLException e) {
						e.printStackTrace();
						return null;
					}
				};
				
				Consumer<String> ce = s -> {
					if(getModuleGUI() != null)
						getModuleGUI().markError(new LoligoException(null, s));
				};
				
				FileInputField fif = new FileInputField(cf, sup);
				fif.setOnInvalidPath(ce);
				
				AsyncFileDialog afd = fif.getFileDialog();
				afd.setDialogTitle("Select save location");
				afd.useSaveDialog();
				afd.setFileDescription("Audio File (*.wav)");
				afd.setFileEndings("wav");
				afd.setFileName("audio-recording.wav");
				
				n.addLabeledEntry("Save Path", fif);
				
				fif.update();
			
		});
		
		return ni;
	}
	
	
	@Override
	public void processIO() {
		
		if(in_switch.switchedOn()) {
			
			recorder.startRecord(file);
		}
		
		if(in_switch.switchedOff()) {
			
			recorder.stopRecord();
		}
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		recorder.stopRecord();
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		String f = file == null ? "" : file.getPath();
		return super.doSerialize(sdc).addExtras(f);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		String s = data.nextString();
		file = new File(s);
	}

}
