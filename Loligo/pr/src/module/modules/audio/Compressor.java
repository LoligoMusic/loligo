package module.modules.audio;

import audio.AudioModule;
import static audio.AudioType.EFFECT;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;

@NodeInfo(type=NodeCategory.AUDIO_FX)

public class Compressor extends AudioModule {

	net.beadsproject.beads.ugens.Compressor compressor;
	private final NumberInputIF in_ratio, in_treshhold;
	
	public Compressor() {
		
		setAudioType(EFFECT);
		in_ratio = addInput("Ratio", NumericAttribute.DEZIMAL_POSITIVE.inRange(0, 10));
		in_treshhold = addInput("Treshhold");
	}
	
	@Override
	public void processIO() {
		
		compressor.setRatio((float) in_ratio.getWildInput())
			 	  .setThreshold((float) in_treshhold.getWildInput());
	
	}
	
	@Override
	public void beadConstruct() {
		
		beadIn = beadOut = 
		compressor = new net.beadsproject.beads.ugens.Compressor(audioContext, 1);
	}

}
