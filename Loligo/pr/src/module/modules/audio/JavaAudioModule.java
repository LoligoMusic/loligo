package module.modules.audio;

import audio.AudioModule;
import gui.nodeinspector.NodeInspector;
import module.AbstractModule;
import module.ModuleDeleteMode;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import module.pyscript.JScript;
import module.pyscript.ScriptModuleDelegate;
import net.beadsproject.beads.ugens.Function;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.Plug;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;


@NodeInfo(type = NodeCategory.AUDIO_FX)
public class JavaAudioModule extends AudioModule{
	
	private final ScriptModuleDelegate scriptModule = new ScriptModuleDelegate(this) {
		public void reload() {
			super.reload();
			script = getScript();
		};
	};
	
	private JScript.IF script;
	
	@Override
	public void beadConstruct() {
		beadIn = new Plug(audioContext);
		
		script = scriptModule.getScript();
		
		System.out.println("SR:" + audioContext.getSampleRate());
		
		Glide g = new Glide(audioContext);
		 
		beadOut = new Function(beadIn) {
			
			double d = 0;
			double r = 1d / audioContext.getSampleRate();
			
			@Override
			public float calculate() {
				d++;
				return script.audio(d * r, x[0]);
			}
		};
		
		beadOut.addDependent(g);
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		scriptModule.defaultSettings();
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		return scriptModule.createGUISpecial();
	}
	
	@Override
	public void processIO() {
		super.processIO();
		scriptModule.processIO();
	}
	
	@Override
	public void onAdd() {
		scriptModule.onAdd();
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		scriptModule.onDelete(mode);
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return scriptModule.doSerialize(sdc);
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		scriptModule.doDeserialize(data, rdc);
	}

	
	
}
