package module.modules.audio;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import audio.AbstractAudioModule;
import audio.AudioType;
import module.inout.AudioInput;
import module.inout.AudioOutput;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.ZMap;

@NodeInfo(type = NodeCategory.AUDIO)
public class MapAudio extends AbstractAudioModule {

	private final AudioInput in;
	private final AudioOutput out;
	private final NumberInputIF in_min1, in_max1, in_min2, in_max2;
	private ZMap map;
	
	public MapAudio() {
		setAudioType(AudioType.CONTROL);
		
		in = addControlInput("In", NumericAttribute.DEZIMAL_FREE);
		out = addControlOutput("Out");
		in_min1 = addInput("Minimum In"	, DEZIMAL_FREE);
		in_max1 = addInput("Maximum In"	, DEZIMAL_FREE).setDefaultValue(1);
		in_min2 = addInput("Minimum Out", DEZIMAL_FREE);
		in_max2 = addInput("Maximum Out", DEZIMAL_FREE).setDefaultValue(1);
	}
	
	@Override
	public void processIO() {
		super.processIO();
		
		float min1 = (float) in_min1.getWildInput(),
		 	  max1 = (float) in_max1.getWildInput(),
		 	  min2 = (float) in_min2.getWildInput(),
		 	  max2 = (float) in_max2.getWildInput();
		
		map.setRanges(min1, max1, min2, max2);
	}
	
	private void setUgen(UGen u) {
		map.clearInputConnections();
		map.addInput(u);
	}
	
	@Override
	public void beadConstruct() {
		map = new ZMap(audioContext, 1);
		in.onChangeAudio(this::setUgen);
		out.setUGen(map);
	}

}
