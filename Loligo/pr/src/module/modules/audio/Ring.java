package module.modules.audio;

import static audio.AudioType.EFFECT;
import audio.AudioModule;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.Mult;
import net.beadsproject.beads.ugens.WavePlayer;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Ring extends AudioModule {
	
	
	private static final double maxRing = 100;
	private final NumberInputIF in_ring;
	
	
	public Ring() {
		setAudioType(EFFECT);
		in_ring = addInput("Ring");
		//in_ring.max = maxRing;
	}
	
	
	@Override
	public void beadConstruct() {
		
		Glide freqGlide = new Glide(audioContext, 50, 50);
		final WavePlayer wp = new WavePlayer(audioContext, freqGlide, Buffer.SINE);
		
		Mult mult = new Mult(audioContext, 1, wp);
		beadIn = beadOut = mult;
		
		addGlideInput(in_ring, freqGlide, 0, (float)maxRing);
		
	}

}
