package module.modules.audio;

import static module.inout.NumericAttribute.DEZIMAL_POSITIVE;

import audio.AbstractAudioModule;
import audio.AudioManager;
import audio.AudioType;
import gui.DropDownMenu;
import gui.nodeinspector.NodeInspector;
import module.inout.AudioInput;
import module.inout.AudioOutput;
import module.inout.NumberInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.ugens.Add;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.Mult;
import net.beadsproject.beads.ugens.WavePlayer;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.AUDIO)
public class LFO extends AbstractAudioModule {

	private final AudioInput in_freq;
	private final NumberInputIF in_min, in_max;
	private final AudioOutput out;
	private float offset, width;
	private WavePlayer wp;
	private Glide glideOffset, glideWidth;
	private int bufferIndex;
	
	public LFO() {
		setAudioType(AudioType.CONTROL);
		in_freq = addControlInput ("Frequency", DEZIMAL_POSITIVE).setDefaultValueAudio(1);
		in_min 	= addInput		  ("Minimum"  , DEZIMAL_POSITIVE);
		in_max 	= addInput		  ("Maximum"  , DEZIMAL_POSITIVE).setDefaultValue(1);
		out 	= addControlOutput(	"Out");
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		return NodeInspector.newInstance(this, ni -> {
			var bufferMenu = new DropDownMenu(this::setBuffer, () -> bufferIndex, Wave.bufferNames);
			bufferMenu.setWH(50, bufferMenu.getHeight());
			bufferMenu.adaptSize();
			ni.addLabeledEntry("Waveform", bufferMenu);
		});
	}
	
	@Override
	public void processIO() {
		super.processIO();
		
		float min = (float) in_min.getWildInput();
		float max = (float) in_max.getWildInput();
		width = max - min;
		offset = min + width ;
		glideOffset.setValue(offset);
		glideWidth.setValue(width);
		
	}
	
	public void setBuffer(int i) {
		bufferIndex = i;
		Buffer buf = Buffer.staticBufs.get(Wave.bufferNames[bufferIndex]);
		wp.setBuffer(buf);
	}
	
	UGen g;
	
	@Override
	public void beadConstruct() {
		
		glideOffset = new Glide(audioContext);
		glideWidth = new Glide(audioContext);
		
		wp = new WavePlayer(audioContext, AudioManager.DUMMY_UGEN, Buffer.SINE);
		
		var mult = new Mult(audioContext, 1, glideWidth);
		mult.addInput(wp);
		
		var add = new Add(audioContext, 1, glideOffset);
		add.addInput(mult);
		
		out.setUGen(add);
		g = mult;
		in_freq.onChangeAudio(wp::setFrequency);
		
	}

	
	

	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		return super.doSerialize(sdc)
				.addExtras(bufferIndex);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		setBuffer(data.nextInt());
	}
	
	
}
