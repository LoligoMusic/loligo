package module.modules.audio;

import audio.AudioManager;
import audio.AudioModule;
import lombok.var;
import module.inout.AudioInput;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.ugens.Gain;

@NodeInfo(type = NodeCategory.AUDIO)

public class Noise extends AudioModule {

	private final AudioInput in_gain;
	
	
	public Noise() {

		in_gain = addControlInput("Gain", NumericAttribute.NORMALIZED);
		in_gain.setDefaultValue(0);
	}
	
	
	@Override
	public void beadConstruct() {

		var noise = new net.beadsproject.beads.ugens.Noise(audioContext);
		
		Gain gain = new Gain(audioContext, 1, AudioManager.DUMMY_UGEN);
		
		gain.addInput(noise);
		beadIn = noise;
		beadOut = gain;
		
		in_gain.onChangeAudio(gain::setGain);
		
	}

}
