package module.modules.audio;

import static audio.AudioType.EFFECT;
import audio.AudioModule;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.Mult;
import net.beadsproject.beads.ugens.WavePlayer;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Tremolo extends AudioModule {
	
	private final NumberInputIF in_rate, in_intens;
	
	
	public Tremolo() {
		
		setAudioType(EFFECT);
		in_rate = addInput("Rate");
		in_intens = addInput("Intensity");
	}
	
	
	@Override
	public void beadConstruct() {
		
		Glide intensGlide = new Glide(audioContext, 0, 20),
		rateGlide = new Glide(audioContext, 2f, 40);
		
		addGlideInput(in_rate, rateGlide, 0, 22);
		addGlideInput(in_intens, intensGlide, 0, 0.5f);
		
		final WavePlayer wp = new WavePlayer(audioContext, rateGlide, Buffer.SINE) {
			
			@Override public void calculateBuffer() {
				super.calculateBuffer();
				intensGlide.update();
				
				float[] buf = bufOut[0];
				
				for (int i = 0; i < buf.length; i++) {
					
					float f = intensGlide.getValue(0, i);
					
					buf[i] = 1 - (buf[i] * f + f);
				}
			}
		};
		
		beadIn = beadOut = new Mult(audioContext, 1, wp);
	}

	
}
