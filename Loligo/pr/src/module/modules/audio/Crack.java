package module.modules.audio;

import audio.AudioModule;
import audio.AudioType;
import lombok.val;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.data.Pitch;
import net.beadsproject.beads.ugens.WavePlayer;
import util.Notes;

@NodeInfo(type = NodeCategory.AUDIO)
public class Crack extends AudioModule {

	private final NumberInputIF in_len, in_fac;
	
	public Crack() {
		setAudioType(AudioType.SOURCE);
		
		in_len = addInput("Length", NumericAttribute.NOTE);
		in_fac = addInput("Fac");
		
	}
	
	@Override
	public void beadConstruct() {
		
		float sr = audioContext.getSampleRate();
		
		val f = new UGen(audioContext, 1, 1) {
			
			float c = 0, v;
			boolean duty;
			
			@Override
			public void calculateBuffer() {
				
				float n = (float) in_len.getWildInput();
				float len = sr / Pitch.mtof(n);
				float fac = (float) in_fac.getWildInput() * 100;
				float len1 = fac,
					  len2 = len - fac;
				
				val b = bufOut[0];
				
				for (int i = 0; i < b.length; i++) {
					
					c++;
					
					if(c > len) {
						c = 0;
						duty = !duty;
						len = duty ? len1 : len2;
						v = (duty ? 1f : 0f) * .04f;
					}
					
					b[i] = v;
					
				}
				
			}
			
		};
		
		beadIn = beadOut = f;
		
	}

}
