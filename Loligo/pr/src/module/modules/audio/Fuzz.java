package module.modules.audio;

import java.io.IOException;
import static audio.AudioType.EFFECT;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;

import audio.AudioModule;
import module.Module;
import module.inout.InOut;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.Delta;
import net.beadsproject.beads.ugens.Noise;
import net.beadsproject.beads.ugens.Plug;
import save.ModuleData;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class Fuzz extends AudioModule {
	
	private NumberInputIF in_fuzz, in_wet, in_foo;
	private transient float fuzz_val = 0, val;
	
	public Fuzz() {
		setAudioType(EFFECT);
		
		in_fuzz = addInput("Fuzz", NumericAttribute.DEZIMAL_FREE);
		in_foo = addInput("Foo", NumericAttribute.DEZIMAL_FREE);
		in_wet = addInput("Mix");
	}

	@Override
	public void processIO() {
		
		super.processIO();
		
		if(in_fuzz.changed())
			fuzz_val = (float) in_fuzz.getWildInput();
		
		val = (float) in_foo.getWildInput();
	}
	
	
	@SuppressWarnings("unused")
	@Override
	public void beadConstruct() {
		
		Plug p = new Plug(audioContext, 1);
		Delta delta = new Delta(audioContext, p);
		
		UGen fuzz = new UGen(audioContext, 1, 1){
			
			
			private float fac, d, last;
			private final float e = (float)Math.E;
			Noise noise = new Noise(audioContext);
			
			
			@Override public void calculateBuffer() {
				noise.update();
				delta.update();
				
				float f = fuzz_val, del;
				float[] bin = bufIn[0],
						bout = bufOut[0];
				
				boolean positive = true;
				int c = 0;
				
				for(int i = 0; i < bufferSize; i++) {
					
					
					del = delta.getValue(0, i);
					
					d = bin[i] ;
					
					boolean b = del > 0;
					if(b != positive)
						c++;
					
					
					d = c % val < .1f ? d : f*d*d*d;
					
					//d = Math.min(d, last * f);
					
					positive = b;
					
					//d*=.5f;
					//last = 1*val*Math.abs(del) < 1 ? (Math.abs(d) > .01f ? last*f : d*f) : d*d*10;
					//d = last;
					
					//d += (del*val)+ d * d * d * f;//+=  del*del < .00001f ? noise.getValue(0, i) * f : 0;
					
					bout[i] = d;
					
				}
			}
			
		};
		
		fuzz.addInput(p);
		
		beadIn = p;
		beadOut = fuzz;
		
		/*
		final MixGlide mixGlide = new MixGlide();
		final Glide mixGlide2 = mixGlide.getMixPartner();
		
		Throughput inPlug = new Throughput(audioContext);
		Throughput outPlug = new Throughput(audioContext);
		Gain gainWet = new Gain(audioContext, 1, (Glide)mixGlide);
		Gain gainDry = new Gain(audioContext, 1, mixGlide2);
		
		fuzz.addInput(inPlug);
		gainWet.addInput(fuzz);
		gainDry.addInput(inPlug);
		outPlug.addInput(gainWet);
		outPlug.addInput(gainDry);
		
		beadIn = inPlug;
		beadOut = outPlug;
		*/
		
	}

	@Override
	public Module createInstance() {
		return new Fuzz();
	}
	
	
	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
		
		ois.defaultReadObject();

		try {
			Field f = InOut.class.getDeclaredField("name");
			f.setAccessible(true);
			f.set(in_wet, "Mix");
			f.setAccessible(false);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {

		ModuleData md = super.doSerialize(sdc);
		
		SaveDataContext.saveByAnnotations(md, Fuzz.class);

		return md;
	}

	
}


