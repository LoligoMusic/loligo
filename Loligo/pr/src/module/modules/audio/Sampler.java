 package module.modules.audio;

import java.net.MalformedURLException;
import java.net.URL;

import audio.AudioModule;
import audio.AudioType;
import gui.nodeinspector.NodeInspector;
import gui.text.FileInputField;
import lombok.Getter;
import lombok.var;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.TriggerInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.SamplePlayer;
import net.beadsproject.beads.ugens.SamplePlayer.LoopType;
import net.beadsproject.beads.ugens.Static;
import save.AssetContainer;
import save.AssetRegistry;
import save.AssetSource;
import save.AudioData;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type=NodeCategory.AUDIO)

public class Sampler extends AudioModule implements AssetContainer{

	private NumberInputIF in_pos, in_rate, in_gain, in_select;
	private TriggerInputIF in_trigger;
	private Gain gain;
	private float rate = 1;
	private Glide posGlide, rateGlide;
	
	private AudioData audioData;
	//private List<Sample> samples = new ArrayList<>();
	private int sampleIndex;
	@Getter
	private AssetSource assetSource;
	
	
	public Sampler() {
		
		setAudioType(AudioType.SOURCE);
		
		in_trigger = addTrigger();
		in_pos = addInput("Pos");
		in_gain = addInput("Gain", NumericAttribute.DEZIMAL_POSITIVE).setDefaultValue(.2f);
		in_rate = addInput("Rate", NumericAttribute.DEZIMAL_POSITIVE).setDefaultValue(1f);
		in_select = addInput("Select", NumericAttribute.INTEGER_POSITIVE);
	}
	
	
	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector ni = NodeInspector.newInstance(this, n -> {
				n.createSection("Path");
				var f = new FileInputField(Sampler.this::loadAssetFromURL, Sampler.this::getURL);
				f.setFileDialog(AssetRegistry.createAudioDialog(null));
				n.addInspectorElement(f);
			}
		);
		return ni;
	}
	
	@Override
	public void beadConstruct() {
		
		Glide gainGlide = new Glide(audioContext, .1f);
		rateGlide = new Glide(audioContext, 1, 50);
		gain = new Gain(audioContext, 1, gainGlide);
		posGlide = new Glide(audioContext, 1, 16000f);
		
		
		addGlideInput(in_gain, gainGlide);
		addGlideInput(in_rate, rateGlide);
		addGlideInput(in_pos, posGlide, 0, 6690);
		
		build(gain);
	}
	
	float temp;
	long tempTime;
	@Override
	public void processIO() {
		
		super.processIO();
		
		if(in_select.changed()) {
			//posGlide.setValueImmediately(0);
			sampleIndex = normalToIndex((float)in_select.getInput());
			//playSample(sampleIndex);
		}
		
		rate = (float) in_rate.getWildInput();
		
		float p = (float) in_pos.getWildInput();
		
		//float diff = p - temp;
		temp = p;
		
		if(in_trigger.triggered()) {
			
			playSample(sampleIndex);
		}
	}
	
	@Override
	public void loadAssetFromURL(URL f) {
		
		if(f == null)
			return;
		audioData = AssetRegistry.loadAudio(f);
		assetSource = audioData.getFile();
	}
	
	
	public URL getURL() {
		
		return assetSource == null ? null : assetSource.getUrl();
	}
	
	
	private int normalToIndex(float f) {
		
		if(audioData == null)
			return 0;
		
		return Math.round(f * (audioData.getSamples().length - 1));
	}
	
	private void playSample(int s) {
		
		if(audioData == null || s >= audioData.getSamples().length || rate <= 0)
			return;
		
		SamplePlayer p = new SamplePlayer(audioContext, audioData.getSamples()[s]);
		//p.setPosition(posGlide);
		
		p.setRate(new Static(audioContext, rate));
		
		//p.setSample(samples.get(s));
		p.setLoopType(LoopType.NO_LOOP_FORWARDS);
		p.setToLoopStart();
		p.setKillOnEnd(true);
		gain.clearInputConnections();
		gain.addInput(p);
		
	}
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		
		ModuleData md = super.doSerialize(sdc);
		
		if(audioData != null) 
			md.assets = new String[] {audioData.serialize()};
		
		return md;
	}
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
	
		super.doDeserialize(data, rdc);
		
		if(data.assets != null && data.assets.length > 0) {
			
			try {
				audioData = AssetRegistry.loadAudio(new URL(data.assets[0]));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

}
