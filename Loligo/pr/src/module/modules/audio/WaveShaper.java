package module.modules.audio;

import static audio.AudioType.EFFECT;

import java.util.function.Consumer;
import java.util.function.Supplier;

import audio.AudioModule;
import gui.SliderArray;
import gui.nodeinspector.NodeInspector;
import module.inout.AudioInput;
import module.inout.NumericAttribute;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import net.beadsproject.beads.core.UGen;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.AUDIO_FX)

public class WaveShaper extends AudioModule {
	
	private final static int SHAPE_LEN = 10;
	
	private final AudioInput in_mix, in_preGain;
	private float[] shape;
	private transient SliderArray sliderArr;
	private transient net.beadsproject.beads.ugens.WaveShaper ws;

	
	public WaveShaper() {
		
		setAudioType(EFFECT);
		in_mix = addControlInput("Mix", NumericAttribute.NORMALIZED).setDefaultValueAudio(.5);
		in_preGain = addControlInput("PreGain", NumericAttribute.DEZIMAL_POSITIVE).setDefaultValueAudio(.5);
	}
	
	@Override
	public void defaultSettings() {
		super.defaultSettings();
		float[] f = {0.0f, 0.9f, 0.1f, 0.9f, -0.9f, 0.0f, -0.9f, 0.9f, -0.3f, -0.9f, -0.5f};
		shape = f;
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector n = NodeInspector.newInstance(this);
		
		Supplier<Double[]> up = () -> {
			Double[] v = new Double[sliderArr.getValue().length];
			
			for (int i = 0; i < v.length; i++) 
				v[i] = (double) shape[i];
			
			return v; 
		};
		
		sliderArr = new SliderArray(SHAPE_LEN, this::setWaveShape, up);
		sliderArr.setRange(1, -1);
		sliderArr.setWH(150, 100);
		
		sliderArr.update();
		n.setTop(5);
		n.createSection("Waveshaper");
		n.addInspectorElement(sliderArr);
		
		return n;
	}
	
	
	@Override
	public void beadConstruct() {
		
		ws = new net.beadsproject.beads.ugens.WaveShaper(audioContext, shape);
		
		in_mix.onChangeAudio(ws::setWetMix);
		
		Consumer<UGen> c = u -> {
			
			var inv = new UGen(audioContext, 1, 1) {
				@Override 
				public void calculateBuffer() {
					for (int i = 0; i < bufIn.length; i++) {
						bufOut[0][i] = 1 / bufIn[0][i];
					}
				}
			};
			inv.addInput(u);
			
			ws.setPreGain(u);
			ws.setPostGain(inv);
		};
		in_preGain.onChangeAudio(c);
		
		beadIn = beadOut = ws;
	}
	
	public void setWaveShape(final Double[] ds) { 
		
		Float[] fs = new Float[ds.length];
		
		for (int i = 0; i < ds.length; i++) {
			
			fs[i] = ds[i].floatValue();
		}
		
		setWaveShape(fs);
	}
	
	public void setWaveShape(final float[] vals) {

		shape = vals;
		ws.setShape(vals);
		
		if(sliderArr != null)
			sliderArr.update();
	}
	
	private void setWaveShape(final Float[] f) {
		
		float[] ar = new float[f.length];
		for (int i = 0; i < ar.length; i++) 
			ar[i] = f[i];
		setWaveShape(ar);
	}

	
	@Override
	public ModuleData doSerialize(final SaveDataContext sdc) {
		return super.doSerialize(sdc).addExtras((Object) shape);
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		super.doDeserialize(data, rdc);
		var ff = (float[]) data.nextObject();
		setWaveShape(ff);
	}
	
}
