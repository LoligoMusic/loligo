package module.modules.audio;

import audio.AudioModule;
import static audio.AudioType.EFFECT;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.ugens.Glide;

@NodeInfo(type=NodeCategory.AUDIO_FX)

public class Gain extends AudioModule {
	
	private final NumberInputIF in_gain;
	private Glide glide_gain;
	
	public Gain() {
		
		setAudioType(EFFECT);
		in_gain = addInput("Gain", NumericAttribute.DEZIMAL_POSITIVE).setDefaultValue(1);
	}
	
	@Override
	public void processIO() {
		
		glide_gain.setValue((float) in_gain.getWildInput()); 
	}
	
	@Override
	public void beadConstruct() {
		
		glide_gain = new Glide(audioContext);
		
		beadIn = beadOut = new net.beadsproject.beads.ugens.Gain(audioContext, 1, glide_gain);
	}

}
