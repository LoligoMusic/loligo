package module.modules.audio;

import static audio.AudioType.EFFECT;
import audio.AudioModule;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import net.beadsproject.beads.analysis.segmenters.ShortFrameSegmenter;
import net.beadsproject.beads.ugens.Plug;

@NodeInfo(type=NodeCategory.AUDIO_FX)

public class Power extends AudioModule {
	
	private final NumberOutputIF out;
	private Float powerValue;
	private net.beadsproject.beads.analysis.featureextractors.Power power;
	
	
	public Power() {
		
		out = addOutput("Power");
		setAudioType(EFFECT);
	}
	
	@Override
	public void processIO() {
		
		powerValue = power.getFeatures();
		if(powerValue != null)
			out.setValue(powerValue * 10);
	}
	
	@Override
	public void beadConstruct() {
		
		Plug plug = new Plug(audioContext);
		power = new net.beadsproject.beads.analysis.featureextractors.Power();
		ShortFrameSegmenter sfs = new ShortFrameSegmenter(audioContext);
		
		sfs.addInput(plug);
		plug.addDependent(sfs);
		sfs.addListener(power);
		beadIn = beadOut = plug;
	}
	
}
