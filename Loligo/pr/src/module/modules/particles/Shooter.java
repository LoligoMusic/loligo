package module.modules.particles;

import static module.Modules.getInputByName;
import static processing.core.PConstants.PI;
import static processing.core.PConstants.TWO_PI;

import java.util.List;

import constants.Flow;
import constants.InputEvent;
import gui.DragHandle;
import gui.particles.Particle;
import gui.particles.ParticleSystem;
import gui.particles.ParticleSystemRegistry;
import gui.particles.prog.ParamHolderObject;
import gui.particles.prog.ParticleParam;
import gui.particles.prog.ParticleParamHolder;
import gui.particles.prog.ProgParticleFactory;
import gui.selection.GuiType;
import module.AbstractPositionableModule;
import module.Module;
import module.ModuleDeleteMode;
import module.Modules;
import module.dynio.DynIOGUIWrapper;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.NumberInputIF;
import module.inout.NumericAttribute;
import module.inout.TriggerInputIF;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import patch.Domain;
import patch.IONode;
import patch.PatchContainer;
import pr.ModuleGUI;
import pr.userinput.UserInput;
import save.IOData;
import save.ModuleData;
import save.PatchData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.PARTICLE)


public class Shooter extends AbstractPositionableModule implements PatchContainer, ParticleParamHolder, DynIOModule{

	private final NumberInputIF in_angle, in_speed, in_lifetime, in_count;//, in_size;
	private final TriggerInputIF in_trigger;

	private ParticleSystem pSystem = ParticleSystemRegistry.defaultParticleSystem();
	private final ProgParticleFactory pfactory;
	
	private final ParamHolderObject paramHolder = new ParamHolderObject();
	
	public Shooter() {
		
		flowFlags.add(Flow.SINK);
		flowFlags.add(Flow.DYNAMIC_IO);
		
		in_trigger = addTrigger();
		in_angle = addInput("Angle").setDefaultValue(.5);
		in_speed = addInput("Speed").setDefaultValue(.5);
		in_count = addInput("Count", NumericAttribute.INTEGER_POSITIVE).setDefaultValue(1);
		in_lifetime = addInput("Lifetime", NumericAttribute.INTEGER_POSITIVE).setDefaultValue(50);

		pfactory = new ProgParticleFactory(this);
		pfactory.setParticleSystem(pSystem);
		
		paramHolder.setParamHolder(this);
	}

	@Override
	public void defaultSettings() {
	
		super.defaultSettings();
		pfactory.insertDefaultProgram();
	}
	
	@Override
	public void addParam(ParticleParam p, IOType io) {
		
		p.createInput(this);
	}
	

	@Override
	public ParticleParam getParam(String name, IOType io) {
	
		return paramHolder.getParam(name);
	}
	
	@Override
	public List<ParticleParam> getParams() {
		return paramHolder.getParams();
	}
	
	@Override
	public List<ParticleParam> copyParams() {
		return paramHolder.copyParams();
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectIn() {
		return paramHolder;
	}
	
	@Override
	public ParamHolderObject getParamHolderObjectOut() {
		return null;
	}
	
	@Override
	public Module module() {
		return this;
	}
	
	@Override
	public void removeParam(ParticleParam param, IOType io) {
		removeIO(param.getIo1());
	}
	
	
	@Override
	public Particle getParticle() {
		return null; //TODO implement FakeParticle for preview
	}
	
	
	@Override
	public ModuleGUI createGUI() {
		
		var h = new DragHandle(this, null) {
			@Override
			public void mouseEventThis(InputEvent e, UserInput input) {
				if(e == InputEvent.CLICK) {
					var p = pfactory.getPatch();
					if(p != null)
						p.getPApplet().requestFocus();
				}
			}
		};
		h.setGuiType(GuiType.ANIM);
		var w = new DynIOGUIWrapper(this, h) {
			@Override
			public void enterEditMode() {
				pfactory.createPatchWindow();
			}
		};
		return w;
	}

	
	@Override
	public void processIO() {
		
		paramHolder.getParams().forEach(ParticleParam::get);
		
		
		if(in_trigger.triggered()) {
		
			float angle = PI - TWO_PI * (float) in_angle.getInput(), 
				  speed = (float) in_speed.getInput() * 20;
			int lifetime = (int) in_lifetime.getWildInput();
			
			
			var t = getDomain().getDisplayManager().getTransform();
			var m = t.getMultipliedMatrix();
			
			float x  = getX(), 
				  y  = getY(),
				  xo = m.multX(x, y),
				  yo = m.multY(x, y),
				  dx = (float) Math.sin(angle) * speed,
				  dy = (float) Math.cos(angle) * speed;
			
			pfactory.setInitX(xo)
					.setInitY(yo)
					.setDx(dx)
					.setDy(dy)
					.setMaxLifeTime(lifetime);
			
			int count = (int) in_count.getWildInput();
			
			for (int i = 0; i < count; i++) {
				var p = pfactory.createParticle();
				pSystem.addParticle(p);
			}
			
		}
		
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
	
		super.onDelete(mode);
		
		pfactory.dispose();
	}
	
	public void setMasterPatch(PatchData pd) {
		pfactory.setMasterCopy(pd);
	}
	
	
	@Override
	public Domain getPatch() {
		
		return pfactory.getPatch();
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
	
		ModuleData md = super.doSerialize(sdc);
		PatchData pd = pfactory.getMasterCopy();
		
		var ps = paramHolder.copyParams();
		pd.setParticleParams(ps);
		
		md.subpatch = pd;
		
		return md;
	}
	
	
	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		setMasterPatch(data.subpatch);
		
		super.doDeserialize(data, rdc);
	}

	
	@Override
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		return dynIOAction(name, type, true);
	}

	
	@Override
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		return dynIOAction(name, null, false);
	}
	
	
	private InOutInterface dynIOAction(String name, DataType type, boolean add) {
		
		var patch = pfactory.getPatch();
			
		if(patch != null) {
			var ms = Modules.findModuleByName(patch, "ParticleOut");
			if(ms.isEmpty()) {
				var m = patch.getNodeRegistry().createInstance("ParticleOut", NodeCategory.PARTICLE);
				patch.addModule(m);
				ms.add(m);
			}
			var m = (IONode) ms.get(0);
			var iof = add ? m.createDynIO(name, type, null) :
							m.deleteDynIO(name, null);
			String name2 = iof.getName();
			pfactory.setUpdate();
			return getInputByName(this, name2);
		}else {
		
			var p = paramHolder.addParam(name, IOType.IN, type);
			var pd = pfactory.getMasterCopy();
			
			pd.getNodes().stream()
						 .filter(m -> "ParticleOut".equals(m.name))
						 .findAny()
						 .ifPresent(m -> {
							 IOData io = new IOData(name, type);
							 if(add)
								 m.outputs.add(io);
							 else
								 m.outputs.remove(io);
						 });
			
			if(add) {
				var in = p.createInput(this);
				in.setOptional();
				return in;
			}else {
				var in = p.getIo1();
				removeParam(p, IOType.IN);
				return in;
			}
		}
	}

	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.ALLTYPES_IN;
	}

	@Override
	public String rename(InOutInterface io, String name) {
		
		var patch = pfactory.getPatch();
		String oldName = io.getName();
		String n = paramHolder.rename(io, name);
		
		if(patch == null) {
			
			var pd = pfactory.getMasterCopy();
			pd.getNodes().stream()
			  .filter(m -> "ParticleOut".equals(m.name))
			  .flatMap(md -> md.getOutputs().stream())
			  .filter(iod -> iod.name == oldName)
			  .findAny()
			  .ifPresent(iod -> iod.name = n);
		}
		
		return n;
	}

}
