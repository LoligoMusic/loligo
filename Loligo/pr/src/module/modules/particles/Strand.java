package module.modules.particles;

import gui.Position;
import gui.particles.DefaultParticleFactory;
import gui.particles.ParticleSystem;
import gui.particles.ParticleSystemRegistry;
import gui.particles.StrandObject;
import module.AnimObject;
import module.inout.ColorInputIF;
import module.inout.NumericAttribute;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.loading.NodeInfo;
import module.loading.NodeCategory;
import patch.DomainType;
import pr.DisplayObjectIF;

@NodeInfo(type = NodeCategory.PARTICLE)

public class Strand extends AnimObject{

	private final NumberInputIF in_angle, in_speed, in_size;
	private final ColorInputIF in_color;
	private final NumberOutputIF out_length, out_crook;
	
	//private Position pos = new Position();
	
	private ParticleSystem pSystem = ParticleSystemRegistry.defaultParticleSystem();
	private StrandObject strand;
	
	public Strand() {
		
		in_angle = addInput("Angle", NumericAttribute.DEZIMAL_FREE);
		in_speed = addInput("Speed").setDefaultValue(.3);
		in_color = addColorInput();
		in_size = addInput("Weight").setDefaultValue(.1);
		out_length = addOutput("Length", NumericAttribute.DEZIMAL_POSITIVE);
		out_crook = addOutput("Crook", NumericAttribute.DEZIMAL_POSITIVE);
	}
	

	@Override
	public DisplayObjectIF createAnim() {
		
		strand = new StrandObject(pSystem, new DefaultParticleFactory());
		strand.setPosObject(new Position.PositionTarget(this));
		
		if(getDomain().domainType() == DomainType.PARTICLE_FACTORY) 
			strand.setInactive();
		else
			pSystem.addParticle(strand);
			
		
		
		return strand.getDisplayObject();
	}
	

	@Override
	public void processIO() {
		
		//if(in_trigger.triggered()) {
		
			float angle = (float)Math.PI * 2 * (float)(-in_angle.getInput() + .5), 
				  speed = (float) in_speed.getInput() * 20,
				  size  = (float) in_size.getInput() * 30;
			
			int col = in_color.getColorInput();
		
			DefaultParticleFactory pfactory = strand.getParticleFactory();
			
			pfactory.setInitX(getX())
					.setInitY(getY())
					.setDx((float) Math.sin(angle) * speed)
					.setDy((float) Math.cos(angle) * speed)
					.setColor(col)
					.setSize(size);
			
			out_length.setValue(strand.getStrandLength());
			out_crook.setValue(strand.getCrookedness());
			
			//Particle p = pfactory.createParticle();
			
			//p.setPosObject(getPosObject());
			
			//pSystem.addParticle(p);
		//}
		
	}
	

	@Override
	public int pipette(int x, int y) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*

	@Override
	public float getX() {
		
		return pos.getX();
	}



	@Override
	public float getY() {
		
		return pos.getY();
	}



	@Override
	public void setPos(float x, float y) {
		
		pos.setPos(x, y);
	}



	@Override
	public Position getPosObject() {
		
		return pos;
	}



	@Override
	public void setPosObject(Position p) {
		
		pos = p;
	}
	
	*/
	
}
