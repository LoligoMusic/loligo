package module;

import static module.inout.NumericAttribute.DEZIMAL_POSITIVE;

import java.util.ArrayList;
import java.util.List;

import constants.Flow;
import constants.Units;
import gui.nodeinspector.DynIONodeInspector;
import gui.nodeinspector.NodeInspector;
import gui.particles.Particle;
import gui.particles.PositionableParticleModifier;
import gui.particles.prog.ParticleParam;
import lombok.var;
import module.dynio.DynIOInfo;
import module.dynio.DynIOModule;
import module.inout.ColorInput;
import module.inout.ColorInputIF;
import module.inout.DataType;
import module.inout.IOType;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.NumberInput;
import module.inout.NumberInputIF;
import save.IOData;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;


public class ParamSetter extends AbstractParticleModifierModule implements DynIOModule{

	private final List<Input> paramInputs = new ArrayList<>();
	private final PSetter paramSetter;
	private final NumberInputIF in_distance;
	
	public ParamSetter() {
		
		flowFlags.add(Flow.DYNAMIC_IO);
		paramSetter = new PSetter();
		setModifier(paramSetter);
		
		in_distance = addInput("Distance", DEZIMAL_POSITIVE).setDefaultValue(1);
		
	}
	
	
	@Override
	public void defaultSettings() {
	
		clearParams();
		addParamNumberInput("Size").setDefaultValue(.4);
		addParamColorInput("Color").setDefaultColor(0xff00ff11);
		super.defaultSettings();
	}
	
	@Override
	public void resetModule() {
		super.resetModule();
		Modules.removeDynamicInputs(this);
	}

	public NumberInputIF addParamNumberInput(String name) {
		NumberInputIF in = new NumberInput(name, this);
		addParamInput(in);
		return in;
	}
	
	public ColorInputIF addParamColorInput(String name) {
		ColorInputIF in = new ColorInput(name, this);
		addParamInput(in);
		return in;
	}
	
	public Input addParamInput(String name, DataType type) {
		
		Input in = type.createInput(name, this);
		addParamInput(in);
		return in;
	}
	
	public void addParamInput(Input in) {
		paramInputs.add(in);
		in.setOptional();
		addIO(in);
		Modules.initIOGUI(this, in);
	}
	
	public void removeParamInput(Input in) {
		paramInputs.remove(in);
		removeIO(in);
	}
	
	public void clearParams() {
		while(!paramInputs.isEmpty()) {
			Input in = paramInputs.get(paramInputs.size() - 1);
			removeParamInput(in);
		}
	}
	
	@Override
	public void processIO() {
		
		float d = (float) (in_distance.getWildInput() * Units.LENGTH.normFactor);
		paramSetter.getMultiplicator().setMaxDistance(d);
		
		for (Input in : paramInputs) {
			in.updateValue();
		}
	}
	
	@Override
	public NodeInspector createGUISpecial() {
		
		NodeInspector n = new DynIONodeInspector(this);
		n.init();
		return n;
	}
	
	
	@Override
	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = super.doSerialize(sdc);
		int off = getInputs().size() - paramInputs.size();
		md.addExtras(off);
		return md;
	}

	@Override
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {

		int off = data.nextInt();
		
		List<IOData> ios = data.getInputs();
		for (int i = off; i < ios.size(); i++) {
			var io = ios.get(i);
			addParamInput(io.name, io.type);
		}
		
		super.doDeserialize(data, rdc);
	}

	
	
	private class PSetter extends PositionableParticleModifier {
		
		@Override
		public void modify(Particle par) {
			
			List<ParticleParam> ps = par.getParams();
			double m = multiplicator(par);
			
			if(ps != null && m > 0) {
				
				for (int i = 0; i < paramInputs.size(); i++) {
					
					Input in = paramInputs.get(i);
					
					for(ParticleParam p : ps) {
					
						if(p.getName().equals(in.getName())) {
							
							Object v = in.getValueObject();
							p.setValue(v);
							break;
						}
					}
				}
			}
			
		}
		
	}

	
	@Override 
	public InOutInterface createDynIO(String name, DataType type, IOType ioType) {
		var io = addParamInput(name, type);
		rename(io, name);
		return io;
	}
	
	@Override 
	public InOutInterface deleteDynIO(String name, IOType ioType) {
		for(Input in : paramInputs) {
			if(in.getName().equals(name)) {
				removeParamInput(in);
				return in;
			}
		}
		return null;
	}

	@Override
	public DynIOInfo getDynIOInfo() {
		return DynIOInfo.NUMBER_COLOR_IN;
	}

}
