package module;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import constants.DisplayFlag;
import constants.Flow;
import constants.SlotEvent;
import gui.nodeinspector.NodeInspector;
import lombok.Getter;
import lombok.Setter;
import module.inout.ColorInput;
import module.inout.ColorInputIF;
import module.inout.ColorOutput;
import module.inout.ColorOutputIF;
import module.inout.InOutInterface;
import module.inout.Input;
import module.inout.NumberInput;
import module.inout.NumberInputIF;
import module.inout.NumberOutput;
import module.inout.NumberOutputIF;
import module.inout.NumericAttribute;
import module.inout.Output;
import module.inout.SwitchInput;
import module.inout.SwitchInputIF;
import module.inout.TransformInput;
import module.inout.TransformOutput;
import module.inout.TriggerInput;
import module.inout.TriggerInputIF;
import module.inout.UniversalInput;
import module.inout.UniversalInputIF;
import module.inout.UniversalOutput;
import module.inout.UniversalOutputIF;
import patch.Domain;
import pr.ModuleGUI;
import save.IOData;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

public abstract class AbstractModule implements Module {
	
	public static boolean createGUI = true;
	
	private Domain domain;
	private List<Input> inSlots;
	private List<Output> outSlots; 
	
	@Getter@Setter
	private String name, label = "";
	
	@Getter@Setter
	private int moduleID = -1;
	
	protected EnumSet<Flow> flowFlags = EnumSet.noneOf(Flow.class);
	protected ModuleGUI gui;

	
	public static void noGUI(boolean b) {
		createGUI = !b;
	}
	
	@Override
	public ModuleGUI createGUI() {
		
		return ModuleGUI.createModuleBox(this);
	}
	
	@Override
	public NodeInspector createGUISpecial() {
	
		return NodeInspector.newInstance(this);
	}
	
	/*
	 *  Initializes module-internal states 
	 */
	@Override
	public void postInit() {
		if(createGUI)
			gui();
	}
	
	@Override
	public void resetModule() {
		
	}
	
	@Override
	public void defaultSettings() {
		
		List<Input> ins = getInputs();
		
		if(ins != null)
			for (Input in : getInputs()) {
				
				if(in.getNumConnections() == 0) {
					in.resetDefault();
				}
			}
	}
	
	@Override
	public void onAdd() {
		
		flowFlags.add(Flow.ALIVE);
	}
	
	@Override
	public void onDelete(ModuleDeleteMode mode) {
		
		flowFlags.remove(Flow.ALIVE);
	}
	
	
	@Override
	public ModuleGUI gui() { // NO_UCD (use default)
		
		if(gui == null) {
			
			gui = createGUI();
			gui.setFlag(DisplayFlag.CLICKABLE, true);
			gui.setFlag(DisplayFlag.DRAGABLE, true);
		}
		
		List<Input> ins = getInputs();
		
		if(ins != null)
			for (Input s : ins) 
				if(s.getGUI() == null)
					gui.addChild(s.createGui());
		
		List<Output> outs = getOutputs();
		
		if(outs != null)
			for (Output s : outs)
				if(s.getGUI() == null)
					gui.addChild(s.createGui());
		
		gui.positionSlots();
		
		return gui;
	}
	
	
	@Override
	public void positionSlots(){
		
		if(!checkFlag(Flow.ROUNDSLOTS)) {
			
			for (int j = 0; j < 2; j++) {
				
				boolean in = j == 0;
				List<? extends InOutInterface> ss = in ? inSlots : outSlots;
				
				if(ss != null) {
					
					int i = 0;
					for (InOutInterface s : ss) {
						
						if(s != null) {
							
							float ry = gui.checkFlag(DisplayFlag.RECT)  ? gui.getHeight() - ss.size() * 6f + 6f * i :
																			 i * 6f - (ss.size() - 1) * 6f * .5f ;
							
							float rx = gui.checkFlag(DisplayFlag.RECT)  ? (in ? 0 : gui.getWidth()) :
									   gui.checkFlag(DisplayFlag.ROUND) ? gui.getWidth() * .5f * (in ? -1 : 1) :
									   0;
							
							s.getGUI().setPos(rx, ry);
						}
						
						i++;
					}
				}
			}
		} else if(inSlots != null){
			float w, step = .5f;
			w = -1.57f - step * (inSlots.size() - 1);
			for (Input s : inSlots) {
				w += step;
				s.getGUI().setPos((float)Math.sin(w) * gui.getWidth(),
								  (float)Math.cos(w) * gui.getWidth());
			}
		}
	}
	
	protected NumberInputIF addInput(String name) {
		
		return addInput(name, NumericAttribute.NORMALIZED);
	}
	
	protected NumberInputIF addInput(String name, NumericAttribute numType) {
		
		NumberInputIF s = new NumberInput(name, this, numType);
		addIO(s);
		return s;
	}
	
	protected NumberOutputIF addOutput(String name) {
		return addOutput(name, NumericAttribute.NORMALIZED);
	}
	
	protected NumberOutputIF addOutput(String name, NumericAttribute attributes) {
		NumberOutputIF s = new NumberOutput(name, this, attributes);
		addIO(s);
		return s;
	}
	
	protected TriggerInputIF addTrigger() {
		TriggerInputIF s = new TriggerInput("Trigger", this);
		addIO(s);
		return s;
	}
	
	protected SwitchInputIF addSwitch() {
		SwitchInputIF s = new SwitchInput("Switch", this);
		addIO(s);
		return s;
	}
	
	protected ColorInputIF addColorInput() {
		ColorInputIF s = new ColorInput("Color", this);
		addIO(s);
		return s;
	}
	
	protected ColorOutputIF addColorOutput() {
		ColorOutputIF s = new ColorOutput("Color", this);
		addIO(s);
		return s;
	}
	
	protected TransformInput addTransformInput() {
		TransformInput t = new TransformInput("Transform", this);
		addIO(t);
		return t;
	}
	
	protected TransformOutput addTransformOutput() {
		TransformOutput t = new TransformOutput("Transform", this);
		addIO(t);
		return t;
	}
	
	
	protected UniversalInputIF addUniversalInput(String name) {
		
		UniversalInputIF s = new UniversalInput(name, this);
		addIO(s);
		return s;
	}
	
	
	protected UniversalOutputIF addUniversalOutput(String name) {
		
		UniversalOutputIF s = new UniversalOutput(name, this);
		addIO(s);
		return s;
	}
	
	@Override
	public void removeIO(Input in) {
		
		if(inSlots != null) {
			
			inSlots.remove(in);
			getDomain().getModuleManager().disconnect(in);
			if(gui != null) {
				gui.removeChild(in.getGUI());
				gui.positionSlots();
			}
			
		}
	}
	
	@Override
	public void removeIO(Output out) {
		
		if(outSlots != null) {
			
			outSlots.remove(out);
			
			getDomain().getModuleManager().disconnect(out);
			
			if(gui != null) {
				gui.removeChild(out.getGUI());
				gui.positionSlots();
			}
			
		}
	}
	
	@Override
	public void addIO(Output s) { 
		if(outSlots == null)
			outSlots = new ArrayList<>();
		outSlots.add(s);
	}
	
	@Override
	public void addIO(Input s) { 
		
		if(inSlots == null)
			inSlots = new ArrayList<>();
		inSlots.add(s);
	}
	
	@Override
	public List<Input> getInputs() {
		return inSlots;
	}

	@Override
	public List<Output> getOutputs() {
		return outSlots;
	}
	
	@Override
	public boolean checkFlag(Flow f) {
		return flowFlags.contains(f);
	}
	
	@Override
	public Domain getDomain() {
		return domain;
	}

	@Override
	public void setDomain(Domain domain) {
		this.domain = domain;
	}


	@Override
	public ModuleGUI getModuleGUI() {
		return gui;
	}


	@Override
	public void delete() {
		
		if(domain != null)
			domain.removeModule(this, null);
	}
	
	@Override
	abstract public void processIO();
    
	
	@Override @Deprecated
    public Module createInstance() {
		return null;
	}
    
    
    @Override
	public List<Input> getConnectedInputs() {
    	
    	List<Input> li = new ArrayList<>();
    	
    	if(outSlots != null)
	    	
    		for (Output o : outSlots) 
				
	    		li.addAll(o.getPartners());
	    
    	return li;
    }
    
    
    @Override
	public boolean isSubModule() {
    	
    	return false;
    }
    
    @Deprecated
    public Module duplicate(){
		
    	return getDomain().getModuleManager().duplicate(this);
	}
    
    
    @Override
    public void onIOEventThis(SlotEvent e, InOutInterface io) {
    	
    }
    

   @Override
    public ModuleData doSerialize(final SaveDataContext sdc) {
    	
	    var map = new ModuleData(this);
    	
	    map.id = moduleID;
	    map.node = getClass().getName();
	    
	    map.name = getName();
	    map.label = label;
	    //map.type = ;
    	
    	
    	map.gui = gui != null;
    	//map.put("maximized", value);
    	
    	map.xy = gui != null ? new int[]{(int) gui.getX(), (int)gui.getY()} :
    						   new int[]{0, 0};
    	
    	 
    	var ins = new ArrayList<IOData>();
    	var outs = new ArrayList<IOData>();
    	
    	if(inSlots != null)
	    	for (Input in : inSlots) {
				
	    		ins.add(in.doSerialize());
	    	}
    	
    	if(outSlots != null)
	    	for (Output out : outSlots) {
				
	    		outs.add(out.doSerialize());
			}
    	
    	map.inputs = ins;
    	map.outputs = outs;
    	
    	return map;
    }
    
   private void checkHidden(List<IOData> ioDatas, List<? extends InOutInterface> slots) {
	   if(ioDatas != null && slots != null) {
		   for (IOData io : ioDatas) {
			   if(io.hidden) {
				   var p = Modules.getIOByName(slots, io.name);
				   p.getGUI().setHidden(true);
			   }
		   }
	   }
   }
   
   @Override 
   public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
	   
	   label = data.label;
	   
	   if(gui != null) {
		   gui.setGlobal(data.xy[0], data.xy[1]);
		   
		   checkHidden(data.inputs, inSlots);
		   checkHidden(data.outputs, outSlots);
		   Modules.positionSlots(this);
	   }
	   
	   if(data.inputs != null && inSlots != null) {
		   
		   for (IOData io : data.inputs) {
			   
			   var in = (Input) Modules.getIOByName(inSlots, io.name);
			  
			   if(in == null) {
				   System.err.println("Input with name \"" + io.name + "\" in " + this + " not found." );
			   }else {
				   in.doDeserialize(io);
				   in.setChanged();
			   }
				  
		   }
	   }
	   
	   rdc.processModule(this);
	   
   }
   
   @Override
   public String toString() {
	   return getLabel();
   }
    
}
