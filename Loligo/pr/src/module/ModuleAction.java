package module;

import static constants.ModuleEvent.MODULE_REMOVE;
import static java.util.Collections.singletonList;

import constants.ModuleEvent;
import history.Action;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import patch.Domain;
import patch.LoligoPatch;
import save.PatchData;
import save.RestoreDataContext;
import save.SaveDataContext;
import save.SaveMode;
import save.SaveTarget;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ModuleAction {
	
	public static class Add implements Action {
		Module module;
		PatchData data;
		ModuleEvent operation;
		
		public Add(Module module, ModuleEvent operation) {
			this.module = module;
			this.operation = operation;
			if(operation == MODULE_REMOVE) 
				createData();
		}
		
		@Override
		public void undo() {
			switch(operation) {
			case MODULE_ADD    -> delete();
			case MODULE_REMOVE -> add();
			}
		}

		@Override
		public void execute() {
			switch(operation) {
			case MODULE_ADD    -> add();
			case MODULE_REMOVE -> delete();
			}
		}
		
		private void add() {
			restoreData();
			module.getDomain().addModule(module);
		}
		
		private void delete() {
			createData();
			module.getDomain().removeModule(module, ModuleDeleteMode.NODE);
		}
		
		private void createData() {
			var sdc = new SaveDataContext(SaveMode.DUPLICATION);
			sdc.setSaveTarget(SaveTarget.MEMENTO);
			sdc.addModule(module);
			data = sdc.buildPatchData();
		}
		
		private void restoreData() {
			var p = (LoligoPatch) module.getDomain();
			new RestoreDataContext(data, p).restorePatch();
			var md = data.getNodes().get(0);
			this.module = md.getModuleOut();
		}
		
	}
	
	
	@RequiredArgsConstructor
	public static class Move implements Action {
		final Domain patch;
		final int moduleID, dx, dy;
		
		private void move(int x, int y) {
			var m = Modules.findModuleByID(patch, moduleID);
			if(m == null)
				return;
			var d = m.getModuleGUI();
			if(d == null)
				return;
			patch.getDisplayManager().getInput().mouseDrag.setOriginRelative(singletonList(d), x, y, true);
		}
		
		@Override
		public void execute() {
			move(dx, dy);
		}
		
		@Override
		public void undo() {
			move(-dx, -dy);
		}
	}
	
	
}
