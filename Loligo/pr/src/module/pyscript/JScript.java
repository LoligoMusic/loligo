package module.pyscript;

import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;
import static module.pyscript.JScript.Type.AUTO;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;

import org.codehaus.commons.compiler.CompileException;
import org.codehaus.commons.compiler.util.ResourceFinderClassLoader;
import org.codehaus.commons.compiler.util.resource.MapResourceCreator;
import org.codehaus.commons.compiler.util.resource.MapResourceFinder;
import org.codehaus.janino.ClassLoaderIClassLoader;
import org.codehaus.janino.Compiler;
import org.codehaus.janino.SimpleCompiler;

import com.google.common.io.PatternFilenameFilter;

public class JScript {
	
	public interface IF {
		default void evaluate() {}
		default void onAdd()    {}
		default void onDelete() {}
		default float audio(double time, float input) {return 0;}
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	public @interface LoligoNode {
	}
	
	public static enum Type {AUTO, DECIMAL, INTEGER, BOOLEAN, COLOR};
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	public @interface Input { 
		Type  type() default AUTO;
		double min() default NEGATIVE_INFINITY;
		double max() default POSITIVE_INFINITY;
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	public @interface Output { 
		
	}


	public static final IF EMPTY = new IF() {};
	
	public static void main(String[] args) {
		//var is = JScript.class.getResourceAsStream("Test.loljava");
		//toClass(is);
		try {
			var f = compile(new File("C:\\Users\\eveli\\Desktop\\shit"));
			f.evaluate();
		} catch (CompileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
	
	
	public static ClassLoader createJarClassLoader(File dir) {
		//"jar:file:" + pathToJar+"!/"
		var jars = dir.listFiles(new PatternFilenameFilter(".*\\.jar"));
		URL[] urls = new URL[jars.length];
		
		try {
			for (int i = 0; i < urls.length; i++) {
				var f = jars[i];
				var u = f.toURI().toURL();
				//String s = "jar:" + u.getProtocol() + ":/" + u.getPath() + "!/";
				//u = new URL(s);
				System.out.println(u);
				urls[i] = u;
			}
		}catch(MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
		
		var cl = new URLClassLoader(urls, ClassLoader.getSystemClassLoader());//URLClassLoader.newInstance(urls);
		try {
			var c = Class.forName("helloworld.Main", true, cl);
			System.out.println(c);
			//System.out.println(cl.loadClass("HelloWorld.Main"));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cl;
	}
	
	
	public static IF compile(File dir) throws ClassNotFoundException, CompileException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {

		var files = dir.listFiles((d, f) -> f.endsWith("java"));
		
		Compiler comp = new Compiler();
		
		var jcl = createJarClassLoader(dir);
		if(jcl != null)
			comp.setIClassLoader(new ClassLoaderIClassLoader(jcl));
		
		comp.setSourcePath(new File[] {dir});
		var classes = new HashMap<String, byte[]>();
		comp.setClassFileCreator(new MapResourceCreator(classes));
		comp.compile(files);
		 
		var cl = new ResourceFinderClassLoader(new MapResourceFinder(classes), jcl);
		
		//Thread.currentThread().setContextClassLoader(cl);
		
		for (String s : classes.keySet()) {
			s = s.substring(0, s.indexOf('.'));
			try {
				var z = cl.loadClass(s);
				if(z.isAnnotationPresent(LoligoNode.class)) {
					return (IF) z.getDeclaredConstructor().newInstance();
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	
	public static IF toClass(InputStream is) {
		
		SimpleCompiler comp = new SimpleCompiler();
		
		try {
			comp.cook(is);
			var cl = comp.getClassLoader();
			Class<?> clas = cl.loadClass("Testi");
			var o = (IF) clas.getDeclaredConstructor().newInstance();
			return o;

		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException | IOException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		} catch (CompileException e) {
			System.out.println(e);
		}
		
		return null;
		
	}
	
}
