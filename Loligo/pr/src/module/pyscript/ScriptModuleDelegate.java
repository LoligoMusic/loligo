package module.pyscript;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static module.inout.NumericType.toBool;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.codehaus.commons.compiler.CompileException;

import gui.nodeinspector.NodeInspector;
import gui.text.FileInputField;
import lombok.Getter;
import lombok.val;
import module.Module;
import module.ModuleDeleteMode;
import module.Modules;
import module.inout.DataType;
import module.inout.Input;
import module.inout.NumberInput;
import module.inout.NumberInputIF;
import module.inout.NumberOutput;
import module.inout.NumericAttribute;
import module.inout.Output;
import module.loading.NodeCategory;
import module.loading.NodeInfo;
import save.ModuleData;
import save.RestoreDataContext;
import save.SaveDataContext;

@NodeInfo(type = NodeCategory.TOOLS)
public class ScriptModuleDelegate {

	public record Out(Field field, Output output) {
		
		void setValue(JScript.IF script) throws IllegalArgumentException, IllegalAccessException {
			var v = field.get(script);
			output.setValueObject(v);
		}
	}
	
	public record In(Field field, Input input) {
		
		void setValue(JScript.IF script) throws IllegalArgumentException, IllegalAccessException {
			if(input.changed()) {
				//input.updateValue();
				if(input.getType() == DataType.Number) {
					var v = ((NumberInputIF) input).getWildInput();
					var t = field.getType();
					Object oc = 
						t == double .class ? 		 v :
						t == float  .class ? (float) v :
						t == int    .class ? (int) 	 v :
						t == long   .class ? (long)  v :
						t == boolean.class ? toBool(v) : null;
					field.set(script, oc);
				}
			}
		}
	}
	
	public record InPair(In in, Output output) {}
	public record OutPair(Out out, Input[] inputs) {}
	
	private final Module module;
	
	@Getter
	private URL url;
	private WatchService watchService;
	@Getter
	private boolean autoReload = false, success = false;
	private Path fileName, nodeName;
	@Getter
	private JScript.IF script = JScript.EMPTY;
	private final List<In> ins = new ArrayList<>();
	private final List<Out> outs = new ArrayList<>();
	
	
	public ScriptModuleDelegate(Module module) {
		this.module = module;
	}
	
	
	public void defaultSettings() {
		
		resetIO();
		
		//var u = JScript.class.getResource("Test2.loljava");
		//loadUrl(u);
		init();
	}
	
	
	public NodeInspector createGUISpecial() {
		
		val ni = NodeInspector.newInstance(module, n -> {
			
			Consumer<URL> cu = u -> {
				loadUrl(u);
				reload();
			};
			
			val f = new FileInputField(cu, this::getUrl);
			f.getFileDialog().setFileEndings(".loljava");
			f.getFileDialog().setDialogTitle("Choose Script..");
			f.getFileDialog().setFileDescription("Java File (.loljava)");
			n.addInspectorElement(f);
			
			n.createSection("Script");
			n.addInspectorElement(f);
			
			/*
			Consumer<Boolean> ar = b -> {
				//setAutoReload(b);
				//n.update();
			};
			val aut = new CheckBox(ar, this::isAutoReload);
			//aut.setMouseOverText("Reload script automatically on change.");
			
			n.addLabeledEntry("Auto-reload", aut);
			
			
			Runnable txt = () -> {
				new Thread( () -> {
					var p = new LoligoPatch();
					LoligoPatch.createGuiOnlyWindow(p);
				}).start();
			};
			
			var tb = new Button(txt, "");
			n.addLabeledEntry("Edit", tb);
			*/
		});
		
		return ni;
	}
	
	
	public void init() {
		
		for (Field f : getFields()) {
			
			var ian = f.getAnnotation(module.pyscript.JScript.Input.class);
			if(ian != null) {
				addScriptInput(f, ian);
			}else {
				
				var oan = f.getAnnotation(module.pyscript.JScript.Output.class);
				if(oan != null) {
					addScriptOutput(f, oan);
				}
			}
		}
	}
	
	
	public void reload() {
		
		if(url == null)
			return;
		
		boolean ac = module.getDomain().getHistory().isActive();
		module.getDomain().getHistory().setActive(false);
		
		loadUrl(url);
		
		var oldIns  = 
			ins.stream()
			   .map(i -> new InPair(i, i.input.getPartner()))
			   .toArray(InPair[]::new);
		
		var oldOuts = 
			outs.stream()
				.map(o -> new OutPair(o, o.output.getPartners().toArray(Input[]::new)))
				.toArray(OutPair[]::new);
		
		resetIO();
		
		for (Field f : getFields()) {
			
			var ian = f.getAnnotation(module.pyscript.JScript.Input.class);
			if(ian != null) {
				reloadInput(f, ian, oldIns);
			}else {
				var oan = f.getAnnotation(module.pyscript.JScript.Output.class);
				if(oan != null) {
					reloadOutput(f, oan, oldOuts);
				}
			}
		}
		
		module.getDomain().getHistory().setActive(ac);
	}
	
	
	private Field[] getFields() {
		Class<?> clas = null;
		clas = script.getClass(); 
		return clas.getDeclaredFields();
	}
	
	
	private boolean compareFields(Field f1, Field f2) {
		return 
			f1.getType() == f2.getType() && 
			f1.getName().equals(f2.getName());
	}
	
	
	public void reloadInput(Field f, module.pyscript.JScript.Input an, InPair[] oldIns) {

		for(var p : oldIns) {
			var in = p.in;
			if(compareFields(f, in.field)) {
				addScriptInput(f, in.input);
				var out = p.output;
				if(out != null)
					module.getDomain().getModuleManager().connect(in.input, out);
				
				return;
			}
		}
		
		addScriptInput(f, an);
	}
	
	
	public void reloadOutput(Field f, module.pyscript.JScript.Output an, OutPair[] oldOuts) {
		
		for(var p : oldOuts) {
			var o = p.out;
			if(compareFields(f, o.field)) {
				addScriptOutput(f, o.output);
				for(var in : p.inputs)
					module.getDomain().getModuleManager().connect(in, o.output);
				
				return;
			}
		}
		
		addScriptOutput(f, an);
	}
	
	
	public void addScriptInput(Field f, module.pyscript.JScript.Input an) {
		
		var c = f.getType();
		var n = f.getName();
		NumericAttribute a = null;
		
		if(c == float.class || c == double.class || c == Float.class || c == Double.class) {
			a = NumericAttribute.DEZIMAL_FREE;
		}else
		if(c == int.class || c == Integer.class || c == long.class || c == Long.class) {
			a = NumericAttribute.INTEGER_FREE;
		}else
		if(c == boolean.class || c == Boolean.class) {
			a = NumericAttribute.BOOL;
		}
		
		Input ip = new NumberInput(n, module, a);
		addScriptInput(f, ip);
	}

	
	private void addScriptInput(Field f, Input ip) {
		module.addIO(ip);
		Modules.initIOGUI(module, ip);
		In in = new In(f, ip);
		ins.add(in);
	}
	
	
	public void addScriptOutput(Field f, module.pyscript.JScript.Output an) {

		var c = f.getType();
		var n = f.getName();
		NumericAttribute a = 
			c == double.class  ? NumericAttribute.DEZIMAL_FREE :
		    c == int.class 	   ? NumericAttribute.INTEGER_FREE :
		    c == boolean.class ? NumericAttribute.BOOL 		   : 
		    					 null;
		var op = new NumberOutput(n, module, a);
		addScriptOutput(f, op);
	}
	
	
	private void addScriptOutput(Field f, Output op) {
		module.addIO(op);
		Modules.initIOGUI(module, op);
		Out out = new Out(f, op);
		outs.add(out);
	}
	
	
	public DataType getDataType(Field f) {
		var t = f.getType();
		if(t == Double.class) {
			return DataType.Number;
		}else
		if(t == Integer.class) {
			return DataType.COLOR;
		}
		return null;
	}

	
	public void resetIO() {

		for (int i = ins.size() - 1; i >= 0; i--) {
			var in = ins.remove(i);
			module.removeIO(in.input);
		}
		
		for (int i = outs.size() - 1; i >= 0; i--) {
			var out = outs.remove(i);
			module.removeIO(out.output);
		}
	}
	
	
	public void processIO() {
		
		try {
			
			for(In in : ins)
				in.setValue(script);
			
			script.evaluate();
			
			for(Out out : outs)
				out.setValue(script);
			
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public JScript.IF loadUrl(URL u) {
		
		url = u;
		/*
		InputStream is = null;
		try {
			is = u.openStream();
		} catch (IOException e2) {
			e2.printStackTrace();
			return null;
		}
		*/
		
		File dir;
		try {
			dir = new File(u.toURI()).getParentFile();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return JScript.EMPTY;
		}
		
		try {
			script = JScript.compile(dir);
		} catch (ClassNotFoundException | CompileException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException
				| IOException e) {
			e.printStackTrace();
		}
		
		
		
		if(script == null) {
			script = JScript.EMPTY;
		}
		
		return script;
	}
	
	
	public void setUrl(URL u) {
		url = u;
		if(autoReload)
			watch();
	}
	
	
	public void watch() {
		
		if(url == null)
			return;
		
		if(watchService != null) {
			try {
				watchService.close();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				watchService = null;
			}
		}
		
		try {
			var p = Paths.get(url.toURI());
			if(!java.nio.file.Files.isDirectory(p)) {
				fileName = p.getFileName();
				p = p.getParent();
			}
			watchService = FileSystems.getDefault().newWatchService();
			p.register(watchService, ENTRY_MODIFY, ENTRY_CREATE, ENTRY_DELETE);
			
		} catch (IOException | URISyntaxException | IllegalArgumentException | FileSystemNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public void onAdd() {
		script.onAdd();
	}
	

	public void onDelete(ModuleDeleteMode mode) {
		script.onDelete();
	}
	

	public ModuleData doSerialize(SaveDataContext sdc) {
		var md = module.doSerialize(sdc);
		md.addExtras(url == null ? "" : url.toExternalForm());
		return md;
	}

	
	public void doDeserialize(ModuleData data, RestoreDataContext rdc) {
		
		String u = data.nextString();
		if(u != null) {
			try {
				URL ur = new URL(u);
				loadUrl(ur);
				reload();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		
		module.doDeserialize(data, rdc);
	}

	
}
