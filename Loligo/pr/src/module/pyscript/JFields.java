package module.pyscript;

import java.lang.reflect.Field;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import module.inout.NumberInOut;
import module.inout.NumberInputIF;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class JFields {

	protected final Field field;
	
	public abstract void set(Object script) throws IllegalArgumentException, IllegalAccessException;
	//public abstract void setOut(Object script) throws IllegalArgumentException, IllegalAccessException;
	
	public static abstract class Num extends JFields{
		
		protected final NumberInOut io;
		
		public Num(Field f, NumberInOut io) {
			super(f);
			this.io = io;
		}
	}
	
	
	public static class Double extends Num {
		
		public Double(Field f, NumberInOut io) {
			super(f, io);
		}
		
		@Override
		public void set(Object script) throws IllegalArgumentException, IllegalAccessException {
			double v = io.getValue();
			field.set(script, v);
		}
	}
	
	
	
}
