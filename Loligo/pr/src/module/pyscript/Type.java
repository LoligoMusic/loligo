package module.pyscript;

import lombok.RequiredArgsConstructor;
import module.inout.DataType;

@RequiredArgsConstructor
public enum Type {
	AUTO, 
	DECIMAL, 
	INTEGER, 
	BOOLEAN, 
	COLOR;
	
	//public final DataType dataType; 
}
