
#constants
FLOAT 		= 'float'
INT 		= 'int'
COLOR 		= 'color'
BOOL 		= 'bool'
BANG 		= 'bang'

MIN_VALUE 	= -99999
MAX_VALUE 	= 99999

class _IOList(list):
	pass

class Color:
	def __init__(self, r=1., g=1., b=1., a=1.):
		self.r = r
		self.g = g
		self.b = b
		self.a = a

def rgba(r=1., g=1., b=1., a=1.):
	return Color(r,g,b,a)

DEFAULT_COLOR= Color(1., 1., 1., 1.)

_DEFAULTS = {FLOAT:0., INT:0, COLOR:DEFAULT_COLOR, BOOL:0, BANG:0}


class _IO:
	def __init__(self, n, t, v, min, max):
		self.name  = n
		self.value = v
		self.type  = t
		self.min   = min
		self.max   = max


inputs  = _IOList()
outputs = _IOList()

_node_name     = 'PyThing'
_node_help     = ''
_node_category = ''

def pluginInfo(name='MyNode', category='Math', help='Testnode'):
	global _node_name, _node_help, _node_category
	_node_name = name
	_node_help = help
	_node_category = category


def input(name, type=FLOAT, value=None, min=MIN_VALUE, max=MAX_VALUE):
	if not value:
		value = _DEFAULTS[type]
	io = _IO(name, type, value, min, max)
	setattr(inputs, name, value)
	inputs.append(io)
	return io


def output(name, type=FLOAT, value=None, min=MIN_VALUE, max=MAX_VALUE):
	if not value:
		value = _DEFAULTS[type]
	io = _IO(name, type, value, min, max)
	setattr(outputs, name, value)
	outputs.append(io)
	return io
