package module.pyscript;

import static util.Colors.alpha;
import static util.Colors.blue;
import static util.Colors.green;
import static util.Colors.red;

import java.util.Properties;
import org.python.core.Py;
import org.python.core.PyBoolean;
import org.python.core.PyFloat;
import org.python.core.PyInteger;
import org.python.core.PyObject;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import module.inout.ColorInputIF;
import module.inout.ColorOutputIF;
import module.inout.Input;
import module.inout.NumberInputIF;
import module.inout.NumberOutputIF;
import module.inout.NumericType;
import module.inout.Output;
import util.Colors;

public class Script {
	
	public static boolean isInit;
	
	
	public static void init() {
		if(!isInit) {
			isInit = true;
			Properties props = new Properties();
		    props.setProperty("python.console.encoding", "UTF-8");
		    props.setProperty("python.import.site", "false");
		    PythonInterpreter.initialize(System.getProperties(), props, new String[0]);
		    
		    var sys = Py.getSystemState();
			sys.path.append(new PyString("D:\\git\\loligo\\Loligo\\pr\\src\\util"));
		}
	}
	
	@NoArgsConstructor
	@AllArgsConstructor
	@Data
	public static class Back {
		public Input in;
		public Output out;
		public Object value;
	}
	
	public static abstract class IO {
		public PyObject pyObject;
	}
	
	public static abstract class In extends IO{
		public abstract void get();
		public abstract Input getInput();
	}
	
	public static abstract class Out extends IO{
		public abstract void set();
		public abstract Output getOutput();
	}
	
	public static class OutNum extends Out {
		@Getter
		public NumberOutputIF output;
		@Override
		public void set() {
			double d = pyObject.__getattr__(output.getName()).asDouble();
			output.setValue(d);
		}
	}
	
	public static class OutColor extends Out {
		@Getter
		public ColorOutputIF output;
		@Override
		public void set() {
			var po = pyObject.__getattr__(output.getName());
			int c = asColor(po);
			output.setColor(c);
		}
	}
	
	public static int asColor(PyObject ps) {
		var r = (float) ps.__getattr__("r").asDouble();
		var g = (float) ps.__getattr__("g").asDouble();
		var b = (float) ps.__getattr__("b").asDouble();
		var a = (float) ps.__getattr__("a").asDouble();
		return Colors.rgbaNorm(r, g, b, a);
	}
	
	public static  class InNum extends In {
		@Getter
		public NumberInputIF input;
		@Override
		public void get() {
			double v = input.getWildInput();
			var t = input.getNumberAttributes().numericType();
			var p = t.type == NumericType.BOOLEAN    ? new PyBoolean(v > .5) :
					t == NumericType.SubType.DECIMAL ? new PyFloat(v) :
				    t == NumericType.SubType.INTEGER ? new PyInteger((int) v) : new PyInteger((int) v);
			pyObject.__setattr__(input.getName(), p);
		}
	}
	
	
	public static class InColor extends In {
		@Getter
		public ColorInputIF input;
		public PyObject containerClass;
		@Override
		public void get() {
			int c = input.getColorInput();
			var r = pfloat(red  (c));
			var g = pfloat(green(c));
			var b = pfloat(blue (c));
			var a = pfloat(alpha(c));
			PyObject[] ca = {r, g, b, a};
			var cp = containerClass.__call__(ca);
			pyObject.__setattr__(input.getName(), cp);
		}
		
		private PyFloat pfloat(int c) {
			return new PyFloat(c / 255f);
		}
	}
	
	
	
	
	
	
}
