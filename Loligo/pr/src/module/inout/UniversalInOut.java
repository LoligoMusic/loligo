package module.inout;

import static module.inout.NumericAttribute.DEZIMAL_FREE;

import java.util.function.Consumer;
import java.util.function.Supplier;

import gui.DisplayGuiSender;
import gui.Slider;
import processing.core.PMatrix2D;

public interface UniversalInOut extends InOutInterface {

	public DataType getDynamicType();
	
	
	public static DisplayGuiSender<?> CREATE__INPUT_UNIVERSAL(UniversalInOut io, IOGuiMode mode) {
		
		DataType t = io.getDynamicType();
		if(t == null)
			return new Slider();
		
		Consumer<Object> co = o -> {
			if(o != null) {
				io.setValueObject(o);
				InOutInterface.updateModule(io.getModule());
			}
		};
		
		DisplayGuiSender<?> gs = null;
		
		switch (t) {
		case Number:
			Consumer<Double> cn = co::accept;
			Supplier<Double> sn = () -> (Double) io.getValueObject();
			gs = NumericAttribute.createGuiByType(DEZIMAL_FREE, cn, sn);
			break;
		case COLOR:
			Consumer<Integer> ci = co::accept;
			Supplier<Integer> si = () -> (Integer) io.getValueObject();
			gs = ColorInOut.CREATE_GUI_ELEMENT(ci, si, mode);
			break;
		case TRANSFORM:
			Consumer<PMatrix2D> ct = co::accept;
			Supplier<PMatrix2D> st = () -> (PMatrix2D) io.getValueObject();
			gs = TransformInOut.CREATE_GUI_OBJECT(ct, st, mode);
			break;
		default:
			break;
		}
		
		InOutInterface.INPUT_MOD(io, gs);
		
		return gs;
	}
}
