package module.inout;

import pr.RootClass;

public class ColorToNumMediator extends AbstractColorMediator implements NumMediator {

	
	public ColorToNumMediator(final ColorOutputIF output) {
	
		super(output);
	}
	
	public static double colorToNum(int c) {
		
		return RootClass.mainProc().brightness(c) / 255d;
	}
	
	@Override
	public double receiveNum() {
		
		value = output.getColor();
		
		return colorToNum(value);
	}

}
