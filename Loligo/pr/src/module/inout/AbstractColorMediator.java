package module.inout;

public abstract class AbstractColorMediator implements InOutMediator{

	protected final ColorOutputIF output;
	protected int value = 0;
	
	
	public AbstractColorMediator(final ColorOutputIF output) {
		
		this.output = output;
	}
	
	
	@Override
	public Output getOutput() {
	
		return output;
	}
	

	@Override
	public boolean hasChanged() {
	
		return value != output.getColor();
	}
	
	
}
