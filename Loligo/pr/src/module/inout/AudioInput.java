package module.inout;

import java.util.function.Consumer;
import java.util.function.Supplier;

import audio.AbstractAudioModule;
import gui.DisplayGuiSender;
import module.Module;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.ugens.Glide;

public class AudioInput extends NumberInput {

	private static final Consumer<UGen> DONOTHING = u -> {};
	private static final float DEFAULT_GLIDETIME = 30;
	
	private UGen ugen;
	
	private Consumer<UGen> onChange = DONOTHING;
	
	public AudioInput(String name, Module module) {
		this(name, module, NumericAttribute.NORMALIZED);
	}
	
	public AudioInput(String name, Module module, NumericAttribute attr) {
		super(name, module, attr);
		setDefaultValue(attr.defaultValue());
	}
	
	public void onChangeAudio(Consumer<UGen> c) {
		onChange = c;
		if(ugen != null)
			onChange.accept(ugen);
	}
	
	@Override
	public void setChanged() {
		super.setChanged();
		
		updateValue();
		onChange.accept(ugen);
	}
	
	
	public boolean isAudioMode() {
		
		return mediator != null && mediator.getOutput().getType() == DataType.AUDIOCTRL;
	}
 	
	public void updateAudio() {
		try {
			((Glide) ugen).setValue((float) getWildInput());
		}catch(ClassCastException e) {
			//e.printStackTrace();
			System.out.println(ugen.toString());
		}
	}
	
	@Override
	public void updateValue() {
		
		//var am = (AudioModule) getModule();
		
		if(isAudioMode()) {
			
			ugen = ((AudioMediator)mediator).getUGen();
			
		}else {
			
			if(ugen == null) {
				resetUGen();
			}
			//am.addAudioInput(this);
		}
	}
	
	/*
	@Override
	public double getInput() {
		
		if(getPartner() == null) {
			return super.getInput();
		}else {
			return getValue();
		}
	}
	*/
	
	@Override
	public void setPartner(Output output) {
		super.setPartner(output);
		setChanged();
		
		if(output.getType() != DataType.AUDIOCTRL)
			((AbstractAudioModule) getModule()).addAudioInput(this);
	}
	

	private void resetUGen() {
		var ac = getModule().getDomain().getAudioManager().getAC();
		ugen = new Glide(ac, (float) getValue(), DEFAULT_GLIDETIME);
	}
 	
	@Override
	public boolean removePartner() {
		
		resetUGen();
		boolean b = super.removePartner();
		setChanged();
		((AbstractAudioModule) getModule()).removeAudioInput(this);
		return b;
	}
	
	
	public UGen getUGen() {
		return ugen;
	}
	
	
	@Override
	public void setValueObject(Object v) {
		if(v instanceof UGen ug) {
			ugen = ug;
			setChanged();
		}else {
			super.setValueObject(v);
		}
	}
	
	@Override
	public Object getValueObject() {
		return ugen;
	}
	
	public AudioInput setDefaultValueAudio(double v) {
		setDefaultValue(v);
		return this;
	}
 	
	@Override
	public DataType getType() {
		return DataType.AUDIOCTRL;
	}
	
	
	@Override
	public DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
		
		DisplayGuiSender<Double> gs;
		
		if(isAudioMode()) {
			
			gs = new AudioOutput.AutoUpdateNumberInputField(ugen::getValueDouble);
			InOutInterface.INPUT_MOD(this, gs);
			gs.disable(true);
				
		}else{
			
			Consumer<Double> c = d -> ((Glide) ugen).setValue(d.floatValue());
			Supplier<Double> s = () -> ugen != null ? ugen.getValueDouble() : 0;
			gs = NumericAttribute.createGuiByType(getNumberAttributes(), c, s);
		}
		
		InOutInterface.INPUT_MOD(this, gs);
		return gs;
	}
	
}
