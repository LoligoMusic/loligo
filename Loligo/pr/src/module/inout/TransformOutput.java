package module.inout;

import module.Module;
import processing.core.PMatrix2D;

public class TransformOutput extends AbstractOutput implements TransformInOut{

	private final PMatrix2D matrix = new PMatrix2D();
	
	public TransformOutput(String name, Module module) {
		super(name, module);
	}

	@Override
	public Object getUniversalValue() {
		return matrix;
	}

	@Override
	public DataType getType() {
		return DataType.TRANSFORM;
	}

	@Override
	public void setMatrix(PMatrix2D m) {
		matrix.set(m);
	}

	@Override
	public PMatrix2D getMatrix() {
		return matrix;
	}

}
