package module.inout;

import static constants.DisplayFlag.DRAGABLE;
import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;
import static module.inout.NumericType.SubType.DECIMAL;
import static module.inout.NumericType.SubType.INTEGER;
import static module.loading.NodeCategory.MATH;

import java.util.function.Consumer;
import java.util.function.Supplier;

import gui.DisplayGuiSender;
import gui.ToggleButton;
import gui.text.CharProfile;
import gui.text.NoteInputField;
import gui.text.NumberInputField;
import lombok.var;
import module.inout.NumericType.SubType;
import module.loading.NodeEntry;
import patch.LoligoPatch;

public record NumericAttribute(NumericType.SubType numericType, double minValue, double maxValue, double defaultValue) {
	
	private final static double NINF = -Double.MAX_VALUE, PINF = Double.MAX_VALUE;
	
	
	public static final NumericAttribute 
		DEZIMAL_FREE		= new NumericAttribute(DECIMAL, 		NINF, PINF, 0), 
		DEZIMAL_POSITIVE	= new NumericAttribute(DECIMAL, 		0d	, PINF	), 
		NORMALIZED 			= new NumericAttribute(DECIMAL, 		0d	, 1d	),
		NORMALIZED_PN 		= new NumericAttribute(DECIMAL, 		-1d	, 1d,  0),
		NOTE 				= new NumericAttribute(SubType.NOTE,	21d	, 127d	),
		INTEGER_FREE		= new NumericAttribute(INTEGER, 		NINF, PINF, 0),
		INTEGER_POSITIVE	= new NumericAttribute(INTEGER, 		0d	, PINF	),
		INTEGER_7BIT		= new NumericAttribute(INTEGER, 		0d	, 127	),
		INTEGER_BYTE		= new NumericAttribute(INTEGER, 		0d	, 255	),
		ENUM				= new NumericAttribute(INTEGER, 	 	NINF, PINF, 0),
		BOOL				= new NumericAttribute(SubType.TOGGLE, 	0d	, 1d	),
		BANG				= new NumericAttribute(SubType.BANG, 	0d	, 1d	),
		PUSH				= new NumericAttribute(SubType.PUSH, 	0d	, 1d	);
	
	
	public static NumericAttribute getAttributeByType(NumericType.SubType t) {
		return switch (t) {
		case DECIMAL -> DEZIMAL_FREE;
		case INTEGER -> INTEGER_FREE;
		case TOGGLE  -> BOOL;
		case BANG    -> BANG;
		case PUSH 	 -> PUSH;
		default		 -> DEZIMAL_FREE;
		};
	}
	

	
	public NumericAttribute(NumericType.SubType numericType, double minValue, double maxValue) {
		this(numericType, minValue, maxValue, minValue);
	}
	
	
	public boolean isRanged() {
		
		return minValue > NEGATIVE_INFINITY || maxValue < POSITIVE_INFINITY;
	}
	
	public NumericAttribute inRange(double min, double max) {
		
		return new NumericAttribute(numericType, min , max);
	}
	
	public NumericAttribute normalizedRange() {
		
		return new NumericAttribute(numericType, 0 , 1);
	}
	
	public NodeEntry getIONodeEntry(LoligoPatch p) {
		var reg = p.getNodeRegistry();
		return 	
			this == NOTE ? reg.findEntryAll("Note", MATH) :
			this == BANG ? reg.findEntryAll("Bang", MATH) :
						   numericType.getIONodeEntry(p);
	}

	
	public static DisplayGuiSender.Numeric createGuiByType(NumericAttribute attr, Consumer<Double> c, Supplier<Double> s) {
		
		var numType = attr.numericType();
		
		return 
		switch (numType) {
			case DECIMAL, INTEGER -> {
				var t = new NumberInputField (c, s);
				t.setCharProfile(numType == NumericType.SubType.DECIMAL ? CharProfile.DEZIMAL : 
																  		  CharProfile.INT);
				t.setRange(attr.minValue(), attr.maxValue());
				
				var tf = t.getTextField();
				tf.setFlag(DRAGABLE, true);
				tf.setAutoFit(true);
				yield t;
			}
			case TOGGLE, PUSH, BANG -> new ToggleButton(c, s, numType);
			case NOTE -> new NoteInputField(c, s);
		};
		
	}
	
}
