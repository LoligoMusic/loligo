package module.inout;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;

import gui.DisplayGuiSender;
import module.AbstractModule;
import util.Utils;



public class TriggerInput extends NumberInput implements TriggerInputIF {
	
	private static final int version = 1;
	
	transient private double marker = .1;
	transient private TriggerModes.Crossing triggerOP = new TriggerModes.Crossing(this);
	
	transient private MarkerGUI markerHandle;
	
	
	public TriggerInput(String name, AbstractModule module) {
		
		super(name, module, NumericAttribute.BANG);
		
	}
	
	@Override
	public boolean triggered() {
		
		return triggerOP.triggererTest(marker);
	}
	
	@Override
	public void setMarkerPos(double d) {
		
		d = d > 1 ? 1 :
			d < 0 ? 0 : d;
		marker = d;
		
		if(markerHandle != null)
			markerHandle.update();
	}
	
	@Override
	public void setMode(int i) {
		
		if(i == 0) {
			triggerOP.left = true;
			triggerOP.right = false;
		}else 
		if(i == 1) {
			triggerOP.left = false;
			triggerOP.right = true;
		}else 
		if(i == 2) {
			triggerOP.left = 
			triggerOP.right = true;
		}
		
	}
	
	@Override
	public DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
		return super.createInputElement(mode);
		
		/*
		Slider s = (Slider) 
		
		markerHandle = new MarkerGUI( s, this::setMarkerPos, ()->{return marker;});
		markerHandle.setGuiType(GuiType.DRAGABLE_INPUT);
		
		Supplier<Integer> up = ()->{
			return triggerOP.left && triggerOP.right ? 2:
				   triggerOP.left 					 ? 0:
													   1;
		};
		
		mc = new MultipleChoice(this::setMode, up);
		
		ListObject li = mc.getList();
		li.setVertical(false);
		li.itemMargin = 4;
		li.setFlag(DisplayFlag.drawable, false);
		
		mc.addElement(icon_L);
		mc.addElement(icon_R);
		mc.addElement(icon_LR);
		
		li.setFlag(DisplayFlag.fixedToParent, true);
		s.addChild(li);
		li.setPos(0,s.getHeight() + 3);
		
		mc.update();
		markerHandle.update();
		
		return s;
		*/
	}
	
	
	private void writeObject(ObjectOutputStream stream) throws IOException {
		
		Map<String, Object> map = Utils.newVersionMap(version);
		
		map.put("marker", marker);
		map.put("left", triggerOP.left);
		map.put("right", triggerOP.right);
		
		stream.writeObject(map);
	}
    
    private void readObject(ObjectInputStream ois)
			throws ClassNotFoundException, IOException {
		
    	@SuppressWarnings("unchecked")
		Map<String, Object> map = (Map<String, Object>) ois.readObject();
		
		marker = (double) map.get("marker");
		
		triggerOP = new TriggerModes.Crossing(this);
		
		triggerOP.left = (boolean) map.get("left");
		triggerOP.right = (boolean) map.get("right");
    	
	}
    

}
