package module.inout;

public interface SwitchInputIF extends NumberInputIF{

	public void setSwitchPoint(double pos);

	public boolean switchedOn();

	public boolean switchedOff();

	public boolean getSwitchState();

}