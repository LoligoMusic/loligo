package module.inout;

import lombok.Getter;
import lombok.Setter;
import module.Module;
import save.IOData;

public abstract class InOut implements InOutInterface {
	
	@Getter@Setter
	private String name;
	private Module module;
	protected InOutGUI gui;

	private boolean isOptional = false;
	
	
	public InOut(String name, Module module) {
		
		this.name = name;
		this.module = module;
	}
	
	
	@Override
	abstract public DataType getType();
	
	@Override
	abstract public int getNumConnections();
	
	
	@Override
	public Module getModule() {
		
		return module;
	}
	
	
	@Override
	public InOutGUI getGUI() {
	
		return gui;
	}
	
	@Override
	public boolean isOptional() {
	
		return isOptional;
	}
	
	@Override
	public void setOptional() {
		
		isOptional = true;
	}
    
    
    @Override
    public IOData doSerialize() {
		
    	IOData data = new IOData();
    	data.name = name;
    	data.type = getType();
    	data.io = this;
    	if(gui != null)
    		data.hidden = gui.isHidden();
    	return data;
	}
    
    @Override
    public String toString() {
    
    	return "Input: " + getName() + ", Value: "; 
    }
}
