package module.inout;

public class UniversalToNumMediator extends XToUniversalMediator implements NumMediator {

	public UniversalToNumMediator(Output out) {
		
		super(out);
	}

	@Override
	public double receiveNum() {
		
		value = out.getUniversalValue();
		
		DataType type = ((UniversalOutputIF) out).getDynamicType();
		
		double d = value == null ? 0 :
				   type == DataType.Number ? (Double) value :
				   type == DataType.COLOR  ? ColorToNumMediator.colorToNum((Integer) value) : 0;
		
		return d;
	}

}
