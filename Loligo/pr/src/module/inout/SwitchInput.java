package module.inout;

import java.util.function.Consumer;

import gui.DisplayGuiSender;
import gui.ToggleButton;
import module.AbstractModule;

public class SwitchInput extends NumberInput implements SwitchInputIF {

	private boolean state, switchedOn, switchedOff;
	private double switchPoint = .5; 
	
	
	public SwitchInput(String name, AbstractModule module) {
		super(name, module, NumericAttribute.BOOL);
		setDefaultValue(1);
	}
	
	
	@Override
	public void setSwitchPoint(double pos) {
		
		switchPoint = pos;
	}

	
	@Override
	public boolean switchedOn() {
		
		num();
		
		if(switchedOn) {
			switchedOn = false;
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean switchedOff() {
		
		num();
		
		if(switchedOff) {
			switchedOff = false;
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean getSwitchState() {
		
		num(); 
		
		return state;
	}
	
	
	private void num() {
		
		double d = getInput();
		
		if(d <= switchPoint) {
			if(state) {
				switchedOff = true;
				switchedOn = false;
				state = false;
			}
		}else{
			if(!state) {
				switchedOn = true;
				switchedOff = false;
				state = true;
			}
		}
	}
	
	
	@Override
	public DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
	
		Consumer<Double> c = d -> {
			setValue(d);
			if(!getModule().getDomain().getSignalFlow().contains(getModule()))
				getModule().processIO();
		};
		ToggleButton t = new ToggleButton(c, this::getValue);
		return t;
	}
	
}
