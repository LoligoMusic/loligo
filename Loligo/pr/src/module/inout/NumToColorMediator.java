package module.inout;

import pr.RootClass;


public class NumToColorMediator extends AbstractNumberMediator implements ColorMediator {

	public NumToColorMediator(NumberOutputIF output) {
		
		super(output);
	}

	public static int numToColor(double d) {
		
		if(d < 0)
			d = -d;
			
		if(d > 1) 
			d -= (int) d;
		
		return RootClass.mainProc().color((int)(255d * d));
	}
	
	
	@Override
	public int receiveColor() {
		
		value = output.getValue();
		
		return numToColor(value);
	}


	@Override
	public void setColor(int value) {
		
		this.value = value;
	}

}
