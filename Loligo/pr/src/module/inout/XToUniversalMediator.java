package module.inout;

public class XToUniversalMediator implements InOutMediator {

	protected Output out;
	protected Object value;
	
	public XToUniversalMediator(Output out) {
		
		this.out = out;
	}

	public Object receiveUniversal() {
		
		value = out.getUniversalValue();
		return value;
	}
	
	@Override
	public Output getOutput() {
		
		return out;
	}

	@Override
	public boolean hasChanged() {
		
		Object o = out.getUniversalValue();
		
		if(o == null)
			return value != null;
		
		if(value == null)
			return o != null;
		
		return !o.equals(value);
	}

}
