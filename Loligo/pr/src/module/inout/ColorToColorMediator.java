package module.inout;

public class ColorToColorMediator extends AbstractColorMediator implements ColorMediator {

	public ColorToColorMediator(ColorOutputIF output) {
		super(output);
		
	}

	@Override
	public int receiveColor() {
	
		value = output.getColor();
		return value;
	}

	@Override
	public void setColor(int value) {
		
		this.value = value;
	}

}
