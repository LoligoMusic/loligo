package module.inout;

import gui.DisplayGuiSender;
import module.AbstractModule;
import save.IOData;

public class UniversalInput extends AbstractInput implements UniversalInputIF{

	private DataType type = DataType.Number;
	private Object value = type.getDefaultValue();
	
	
	public UniversalInput(String name, AbstractModule module) {
		
		super(name, module);
	}

	@Override
	public Object getValueObject() {
		
		if(mediator != null)
			value = ((XToUniversalMediator)mediator).receiveUniversal();
		
		return value;
	}
	
	@Override
	public void setValueObject(Object value) {
		
		this.value = value;
		setChanged();
	}
	
	@Override
	public DataType getDynamicType() {
		
		if(mediator != null) {
			
			type = mediator.getOutput().getType();
			if(type == DataType.UNIVERSAL) 
				type = ((UniversalOutputIF) mediator.getOutput()).getDynamicType();
		}
		
		return type;
	}
	
	@Override
	public void copyValue(Input s1) {
		
		if(s1.getType() == DataType.UNIVERSAL)
			setValueObject(((UniversalInputIF) s1).getValueObject());
	}
	

	@Override
	public DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
		return UniversalInOut.CREATE__INPUT_UNIVERSAL(this, mode);
	}

	@Override
	public DataType getType() {
		return DataType.UNIVERSAL;
	}
	
	
	@Override
	public IOData doSerialize() {
		var data = super.doSerialize();
		data.type = getDynamicType();
		data.value = value;
		return data;
	}

	@Override
	public void doDeserialize(IOData io) {
		
		type = io.type;
		value = io.value;
	}

	@Override
	public void updateValue() {
		getValueObject();
	}


}
