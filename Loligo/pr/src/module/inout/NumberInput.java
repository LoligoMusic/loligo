package module.inout;

import constants.DisplayFlag;
import module.Module;
import save.IOData;

public class NumberInput extends AbstractInput implements NumberInputIF {
	
	private double value = 0,
				   wild = value,
				   defaultValue = value;
	
	private final NumericAttribute numType;
	
	public NumberInput(String name, Module module) {
		
		this(name, module, NumericAttribute.NORMALIZED);
	}
	
	
	public NumberInput(String name, Module module, NumericAttribute numType) {
		
		super(name, module);
		
		this.numType = numType;
	}
	
	
	@Override
	public double getWildInput() {
		
		getInput();
		
		return wild;
	}
	
	@Override
	public double getInput() { //getNormalized()
		
		if(mediator != null) 
			wild = ((NumMediator) mediator).receiveNum();
		else
			return value;
		
		value = normalize(wild);
		
		if(gui != null && gui.checkFlag(DisplayFlag.onDisplay))
			gui.update();
		
		return value;
	}
	
	@Override
	public void updateValue() {
	
		getInput();
	}
	
	
	@Override
	final public void setValue(double value) {
		
		wild = value;
		this.value = normalize(value);
		setChanged();
	}
	
	public static double normalize(double d) {
		
		if(d < 0 || d > 1)
			d = d - Math.floor(d);
		
		return d;
	}
	
	/*
	 * (non-Javadoc)
	 * @see module.inout.NumberInOut#getValue()
	 */
	@Override
	public double getValue() {
		return wild;
	}
	
	@Override
	public DataType getType() {
		
		return DataType.Number;
	}
	
	@Override
	final public NumberInputIF setDefaultValue(double defaultValue) {
		
		this.defaultValue = defaultValue;
		return this;
	}
	
	@Override
	public double getDefaultValue() {
		
		return defaultValue;
	}
	
	
	@Override
	public void copyValue(Input s1) {
		
		if(s1 instanceof NumberInputIF n) {
			setValue(n.getValue());
		}
		
	}

	
	@Override
	public IOData doSerialize() {
		
		IOData data = super.doSerialize();
		
		data.value = wild;
		
		return data;
	}

	@Override
	public void doDeserialize(IOData io) {
		
		setValue((double) io.value);
	}
	
	@Override
	public String toString() {
		
		return super.toString() + wild;
	}


	@Override
	public NumericAttribute getNumberAttributes() {
		return numType;
	}
	
}