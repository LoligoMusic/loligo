package module.inout;

import constants.DisplayFlag;
import module.Module;

public class ColorOutput extends AbstractOutput implements ColorOutputIF {
	
	private int value = 0;
	
	
	public ColorOutput(String name, Module module) {
		
		super(name, module);
		
	}

	@Override
	public int getColor() {
		
		return value;
	}
	
	@Override
	public Object getUniversalValue() {
		
		return value;
	}
	
	@Override
	public void setColor(int value) {
		
		this.value = value;
		
		if(gui != null && gui.checkFlag(DisplayFlag.onDisplay))
			gui.update();
	}

	@Override
	public DataType getType() {
		return DataType.COLOR;
	}
	
}
