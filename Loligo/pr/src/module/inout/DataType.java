package module.inout;

import static module.inout.NumericAttribute.DEZIMAL_FREE;
import static module.inout.NumericAttribute.INTEGER_POSITIVE;

import java.util.function.BiFunction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.var;
import module.Module;
import module.loading.NodeCategory;
import module.loading.NodeEntry;
import module.modules.math.Color;
import module.modules.math.EnumValue;
import module.modules.math.Transform;
import patch.LoligoPatch;
import processing.core.PMatrix2D;
import util.Colors;

public enum DataType {

	Number((s, m) -> new NumberInput(s, m, DEZIMAL_FREE), NumberOutput::new, 0d),
	COLOR (	ColorInput::new	, ColorOutput::new , 0xffffffff	),
	TRANSFORM (TransformInput::new, TransformOutput::new, new PMatrix2D()),
	UNIVERSAL,
	SPREAD,
	ENUM((s, m) -> new NumberInput(s, m, INTEGER_POSITIVE), NumberOutput::new, 0d), 
	AUDIOCTRL;
	
	
	public static NodeEntry getIOEntry(LoligoPatch p, InOutInterface io) {
		
		var reg = p.getNodeRegistry();
		
		return 
		switch(io.getType()) {
		case Number -> { 
			var nio = (NumberInOut) io;
			var at = nio.getNumberAttributes();
			yield at.getIONodeEntry(p);
		}
		case COLOR 		-> reg.findEntryAll(Color.class.getSimpleName(), NodeCategory.COLOR);
		case ENUM 		-> reg.findEntryAll(EnumValue.class.getSimpleName(), NodeCategory.MATH);
		case TRANSFORM 	-> reg.findEntryAll(Transform.class.getSimpleName(), NodeCategory.MATH);
		default 		-> null;
		};
	}
	
	public static Object toSerializedForm(DataType t, Object o) {
		
		
		
		return switch(t) {
		case COLOR -> Colors.toHexString((Integer) o);
		default    -> o;
		};
	}
	
	public static Object fromSerializedForm(DataType t, Object o) {
		
		if(o == null)
			return null;
		
		return switch(t) {
		case COLOR 	   -> Colors.hex((String) o);
		case TRANSFORM -> {
			var dd = (Object[]) o;
			var ff = new float[dd.length];
			for (int i = 0; i < dd.length; i++) 
				ff[i] = ((Double) dd[i]).floatValue();
			yield ff;
		}
		default 	   -> o;
		};
	}
	
	private DataType() {
		
		this.inputFoo = null;
		this.outputFoo = null;
		this.defaultValue = null;
	}
	
	private DataType(BiFunction<String, Module, Input> inputFoo, BiFunction<String, Module, Output> outputFoo) {
		
		this(inputFoo, outputFoo, null);
	}
	
	private DataType(BiFunction<String, Module, Input> inputFoo, BiFunction<String, Module, Output> outputFoo, Object defaultValue) {
		
		this.inputFoo = inputFoo;
		this.outputFoo = outputFoo;
		this.defaultValue = defaultValue;
					   
	}
	
	private final BiFunction<String, Module, Input> inputFoo;
	private final BiFunction<String, Module, Output> outputFoo;
	
	public Object fromSerializedForm(Object o) {
		return fromSerializedForm(this, o);
	}
	
	public Object toSerializedForm(Object o) {
		return toSerializedForm(this, o);
	}
	
	
	@JsonIgnore@Getter
	private final Object defaultValue;
	
	
	public Input createInput(String name, Module module) {
		return inputFoo.apply(name, module);
	}
	
	public static Input createInput(String name, Module module, NumericAttribute att) {
		return new NumberInput(name, module, att);
	}
	
	public Output createOutput(String name, Module module) {
		return outputFoo.apply(name, module);
	}
	
	public static Output createOutput(String name, Module module, NumericAttribute att) {
		return new NumberOutput(name, module, att);
	}
}
