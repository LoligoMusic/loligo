package module.inout;

public class UniversalToColorMediator extends XToUniversalMediator implements ColorMediator {
	
	private DataType type;
	
	public UniversalToColorMediator(Output out) {
		
		super(out);
	}
	

	@Override
	public int receiveColor() {
		
		value = out.getUniversalValue();
		
		type = ((UniversalOutput) out).getDynamicType();
		
		int c = 0;
		
		try {
			
		c = value == null ? 0 :
			type == DataType.Number ? NumToColorMediator.numToColor((Double) value) :
			type == DataType.COLOR  ? (Integer) value : 0;
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
				
				
		return c;
	}
	

	@Override
	public void setColor(int value) {
		
		this.value = value;
	}

}
