package module.inout;

import static module.inout.DataType.*;
import java.util.EnumSet;
import java.util.function.Function;


public enum MediatorEnum { 
	
	NUM_NUM(		Number, 	Number, 	o -> new NumToNumMediator((NumberOutputIF) o)),
	ENUM_ENUM(		ENUM, 		ENUM, 		o -> new NumToNumMediator((NumberOutputIF) o)),
	NUM_ENUM(		Number, 	ENUM, 		o -> new NumToNumMediator((NumberOutputIF) o)),
	ENUM_NUM(		ENUM, 		Number, 	o -> new NumToNumMediator((NumberOutputIF) o)),
	NUM_COLOR(		Number, 	COLOR, 		o -> new ColorToNumMediator((ColorOutputIF) o)),
	COLOR_NUM(		COLOR, 		Number, 	o -> new NumToColorMediator((NumberOutputIF) o)),
	COLOR_COLOR(	COLOR, 		COLOR, 		o -> new ColorToColorMediator((ColorOutputIF) o)),
	UNI_COLOR(		COLOR, 	UNIVERSAL, 		o -> new UniversalToColorMediator(o)),
	COLOR_UNI(		UNIVERSAL, 	COLOR, 		o -> new XToUniversalMediator(o)),
	NUM_UNI(		UNIVERSAL, 	Number, 	o -> new XToUniversalMediator(o)),
	UNI_NUM(		Number, 	UNIVERSAL, 	o -> new UniversalToNumMediator(o)),
	UNI_UNI(		UNIVERSAL, 	UNIVERSAL, 	o -> new XToUniversalMediator(o)),
	TRANS_TRANS(	TRANSFORM, 	TRANSFORM, 	o -> new TransformToTransformMediator((TransformOutput) o)),
	NUM_TRANS(		TRANSFORM, 	Number, 	o -> new NumToTransformMediator((NumberOutputIF) o)),
	UNI_TRANS(		UNIVERSAL, 	TRANSFORM, 	o -> new XToUniversalMediator(o)),
	TRANS_UNI(		TRANSFORM, 	UNIVERSAL, 	o -> new UniversalToTransformMediator(o)),
	NUM_AUDIOCTRL(  AUDIOCTRL,  Number, 	o -> new NumToNumMediator((NumberOutputIF) o)),
	COLOR_AUDIOCTRL(AUDIOCTRL,  COLOR, 		o -> new ColorToNumMediator((ColorOutputIF) o)),
	AUDIO_AUDIO(	AUDIOCTRL,  AUDIOCTRL, 	o -> new AudioMediator((AudioOutput) o)),
	AUDIO_NUM(		Number,  	AUDIOCTRL, 	o -> new AudioToNumMediator((AudioOutput) o));
	
	public static InOutMediator findMediator(final Input in, final Output out) {
		 
		if(out.getType() == SPREAD) {
			
			if(in.getType() == SPREAD) {
				
				return null;//new NumSpreadToNumSpreadMediator((NumSpreadOutputIF) out);
			}else {
				//SpreadWrapper.wrapModule(in.getModule(), (SpreadOutput) out, in);
				return null;
			}
		}
		
		return findMediator(in.getType(), out);
	}
	
	public static InOutMediator findMediator(final DataType inType, final Output out) {
		
		for (MediatorEnum m : meds)
			
			if(m.in == inType && m.out == out.getType())
				return m.mediatorFunction.apply(out);
		
		return null;
	}
	
	
	
	
	private static EnumSet<MediatorEnum> meds = EnumSet.allOf(MediatorEnum.class);
	public final DataType in, out;
	private final Function<Output, InOutMediator> mediatorFunction;
	
	
	private MediatorEnum(DataType in, DataType out, Function<Output, InOutMediator> mediatorFunction) {
		
		this.in = in;
		this.out = out;
		this.mediatorFunction = mediatorFunction;
	}
	
	
	
}
