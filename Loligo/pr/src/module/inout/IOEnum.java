package module.inout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import com.google.common.collect.ImmutableList;

import lombok.Data;
import lombok.Getter;
import lombok.val;
import lombok.var;


@Data
public class IOEnum {

	private final String name;
	private final List<IOEnumEntryIF> values;
	
	
	private IOEnum(String name, List<IOEnumEntryIF> values) {
		
		this.name = name;
		this.values = values;
	}
	
	public IOEnumEntryIF getValueByName(String n) {
		
		for(IOEnumEntryIF e : values)
			if(e.getName().equals(n))
				return e;
		
		return null;
	}
	
	public IOEnumEntryIF add(String name) {
		for(val v : values) 
			if(v.getName().equals(name)) 
				return v;
		
		var e = new IOEnumEntry(name, values.size());
		values.add(e);
		return e;
	}
	
	public IOEnumEntryIF remove(String name) {
		for(int i = 0; i < values.size(); i++) { 
			val v = values.get(i);
			if(v.getName().equals(name)) {
				values.remove(v);
				return v;
			}
		}
		return null;
	}
	
	
	//------------------------------------Static------------------------------------------------
	
	
	private final static List<IOEnum> ENUM_REGISTRY_DATA = new ArrayList<>();
	
	public final static List<IOEnum> ENUM_REGISTRY = Collections.unmodifiableList(ENUM_REGISTRY_DATA);
	
	public final static IOEnum EMPTY = createEnum("Empty", " - ");
	
	
	public static abstract class IOEnumEntryIF {
		
		public abstract int getOrdinal();
		public abstract String getName();
		
		@Override
		public String toString() {
			return "#" + getOrdinal() + ": " + getName();
		}
		
		@Override
		public boolean equals(Object obj) {
		 
			return super.equals(obj);
		}
	}
	
	
	public static class IOEnumEntryEnum extends IOEnumEntryIF{
		
		@Getter
		final Enum<?> enumValue;
		
		public IOEnumEntryEnum(Enum<?> en) {
			
			this.enumValue = en;
		}
		
		@Override
		public int getOrdinal() {
			return enumValue.ordinal();
		}
		
		@Override
		public String getName() {
			return enumValue.toString();
		}
	}
	
	
	public static class IOEnumEntry extends IOEnumEntryIF{
		
		@Getter
		final String name;
		@Getter
		final int ordinal;
		
		public IOEnumEntry(String name, int ordinal) {
			
			this.name = name;
			this.ordinal = ordinal;
		}
	}
	
	
	public static IOEnum createEnum(String name, String... names) {
		
		IOEnumEntryIF[] ee = new IOEnumEntryIF[names.length];
		
		for (int i = 0; i < names.length; i++) {
			
			ee[i] = new IOEnumEntry(names[i], i);
		}
		 
		List<IOEnumEntryIF> s = ImmutableList.copyOf(ee);
		return createEnum(name, s);
	}
	
	public static IOEnum createEnum(Enum<?> en) {
		
		String name = en.getDeclaringClass().getSimpleName();
		
		EnumSet<? extends Enum<?>> ee = EnumSet.allOf(en.getDeclaringClass());
		
		List<IOEnumEntryIF> values = ee.stream()
				.map(IOEnumEntryEnum::new)
				.collect(ImmutableList.toImmutableList());
		
		return createEnum(name, values);
	}
	
	public static IOEnum createEnum(String name, List<IOEnumEntryIF> values) {
		
		IOEnum e = new IOEnum(name, values);
		
		ENUM_REGISTRY_DATA.add(e);
		
		return e;
	}
	
	public static void removeEnum(IOEnum ioe) {
		ENUM_REGISTRY_DATA.remove(ioe);
	}
	
	public static String[] names(IOEnum e) {
		
		return e.getValues()
				.stream()
				.map(IOEnumEntryIF::getName)
				.toArray(String[]::new);
	}
	
	public static String[] enumToStringArray(Enum<?> en) {
		
		return EnumSet.allOf(en.getDeclaringClass())
				.stream()
				.map(Enum::name)
				.map(s -> s.substring(0, 1) + s.substring(1).toLowerCase())
				.map(s -> s.replace("_", " "))
				.toArray(String[]::new);
	}
	
	
	public static class Builder {
		
		private final List<IOEnumEntryIF> elements = new ArrayList<>();
		private String name;
		
		public Builder setName(String name) {
			
			this.name = name;
			return this;
		}
		
		public Builder addEnum(String name) {
			
			IOEnumEntry e = new IOEnumEntry(name, elements.size());
			elements.add(e);
			return this;
		}
		
		public Builder addEnums(String... names) {
			
			for (int i = 0; i < names.length; i++) {
				addEnum(names[i]);
			}
			return this;
		}
			
		public IOEnum build() {
		
			List<IOEnumEntryIF> set = ImmutableList.copyOf(elements);
			
			return new IOEnum(name, set);
			
		}
	}


	public static IOEnum getEnumByName(String t) {
		
		for(IOEnum e : ENUM_REGISTRY)
			if(e.getName().equals(t))
				return e;
		
		return null;
	}
	
	
}
