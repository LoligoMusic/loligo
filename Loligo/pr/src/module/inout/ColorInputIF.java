package module.inout;

public interface ColorInputIF extends Input, ColorInOut{

	public int getColorInput();
	
	public ColorInputIF setDefaultColor(int c);
	
	public int getDefaultColor();
	
	public default void resetDefault() {
		
		setColor(getDefaultColor());
	}
}