package module.inout;

import java.util.ArrayList;
import java.util.List;

import constants.SlotEvent;
import module.Module;
import save.IOData;


public abstract class AbstractInput extends InOut implements Input {
	
	transient protected InOutMediator mediator;
	transient protected boolean changed = true;
	
	private List<InputListener> listeners;
	
	
	public AbstractInput(String name, Module module) {
		
		super(name, module);
		
	}

	@Override
	public void setPartner(Output output) {
		
		if(output == null) {
			//removePartner();
			
			return;
		}
		
		mediator = findMediator(output);
		
		getModule().onIOEventThis(SlotEvent.SLOT_CONNECT, this);
		
		if(mediator != null && gui != null) 
			gui.connected();
	}
	
	@Override
	public boolean removePartner() {
		
		boolean b = mediator != null;
		
		getModule().onIOEventThis(SlotEvent.SLOT_DISCONNECT, this);
		
		mediator = null;
		
		if(b && gui != null)
			gui.disconnected();
		
		return b;
	}
	
	public Output getPartner() {
		
		return mediator == null ? null : mediator.getOutput();
	}
	
	@Override
	public int getNumConnections() {
		
		return getPartner() == null ? 0 : 1;
	}
	
	@Override
	public boolean changed() {
		
		if(mediator != null)
			
			return mediator.hasChanged();
		else {
			boolean b = changed;
			changed = false;
			return b;
		}
	}
	
	@Override
	public void setChanged() {
		
		changed = true;
		
		if(listeners != null)
			for (InputListener lis : listeners) 
				lis.onInputChanged(this);
		
	}
	
	public InOutMediator getMediator() {
		
		return mediator;
	}
	
	public void setMediator(InOutMediator m) {
		
		mediator = m;
	}
	
	@Override
	public InOutMediator findMediator(final Output o) {
		
		if(o == null) 
			return null;
		
		InOutMediator m = MediatorEnum.findMediator(this, o);
		
		return m;		
	}
	
	@Override
	public void addInputListener(InputListener lis) {
		
		if(listeners == null)
			listeners = new ArrayList<>();
			
		listeners.add(lis);
	}
	
	@Override
	public void removeInputListener(InputListener lis) {
		
		if(listeners != null)
			listeners.remove(lis);
	}
	
	
	@Override
	public InOutGUI createGui() {
		
		return gui = new InputGUI(this, createInputElement(IOGuiMode.NODE));
	}	
	
	@Override
	public IOType getIOType() {
	
		return IOType.IN;
	}
	
	
	@Override
	public IOData doSerialize() {
		
		IOData data = super.doSerialize();
		
		data.output = getPartner();
		data.partner = getPartner() == null ? null : getPartner().getModule();
		
		return data;
	}
	
}
