package module.inout;

public interface ColorMediator extends InOutMediator{
	
	public int receiveColor();
	
	public void setColor(int value);
}
