package module.inout;

import processing.core.PConstants;
import processing.core.PMatrix2D;

public class NumToTransformMediator extends AbstractNumberMediator implements TransformMediator {

	private final PMatrix2D matrix = new PMatrix2D();
	
	public NumToTransformMediator(NumberOutputIF output) {
		super(output);
	}
	

	@Override
	public PMatrix2D recieveTransform() {
		double d = output.getValue();
		return numToTransform(matrix, d);
	}

	
	public static PMatrix2D numToTransform(PMatrix2D matrix, double d) {
		float r = (float) d;
		matrix.reset();
		matrix.rotate(r * PConstants.TAU);
		return matrix;
	}
	

}
