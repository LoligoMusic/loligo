package module.inout;

import module.Module;

public enum IOType {
	
	IN ((m, n, t) -> t.createInput (n, m)),
	OUT((m, n, t) -> t.createOutput(n, m));
	
	private final Tri foo;
	
	
	private IOType(Tri foo) {
		
		this.foo = foo;
	}
	
	public InOutInterface createIO(Module m, String name, DataType type) {
		
		return foo.createIO(m , name, type);
	}
	
	public IOType flip() {
		return this == IN ? OUT : IN;
	}
	
	@FunctionalInterface
	private interface Tri {
		
		InOutInterface createIO(Module m, String name, DataType t);
	}
	
	public static IOType flip(IOType io) {
		return io == IN ? OUT : IN;
	}
}
