package module.inout;

import module.Module;
import save.IOData;

public interface Input extends InOutInterface {

	
	public Module getModule();
	
	public Output getPartner();
	
	public void setPartner(Output output);
	
	public boolean removePartner();
	
	public boolean changed();
	
	public void setChanged();
	
	public InOutMediator getMediator() ;
	
	public void setMediator(InOutMediator m);

	public void copyValue(Input s1);

	public InOutMediator findMediator(Output o);

	void addInputListener(InputListener lis);

	void removeInputListener(InputListener lis);

	void doDeserialize(IOData io);

	void resetDefault();
}
