package module.inout;

import constants.InputEvent;
import gui.DisplayGuiSender;
import pr.RootClass;
import pr.userinput.UserInput;

class InputGUI extends InOutGUI {
	
	
	public InputGUI(InOutInterface slot, DisplayGuiSender<?> inputElement) {
		
		super(slot, inputElement);
	}
	
	
	@Override
	public void positionInputElement() {
		if(inputElement != null)
			inputElement.setPos(getX() - inputElement.getWidth() - 10, getY());
	}
 	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {

		switch(e) {
		case DRAG_START -> {
			
			Output partner = ((Input)io).getPartner();
			var lc = getInOut().getModule().getDomain().getDisplayManager().getLineContainer();
			
			if(partner != null) {
				io.getModule().getDomain().getModuleManager().disconnect((Input)io);
				lc.setLineTarget(partner);
				//getDm().getInput().setLineTarget(partner);
			}else {
				lc.setLineTarget((Input)io);
				//getDm().getInput().setLineTarget((Input)io);
				//super.mouseStartDrag();
			}
		}
		case OVER -> {
			textOffset = -RootClass.mainProc().textWidth(io.getName()) - 7;
			super.mouseEventThis(e, input);
		}
		default -> super.mouseEventThis(e, input);
		}
	}

	
	@Override
	public void connected() {
		
		super.connected();
		if(inputElement != null)
			inputElement.disable(true);
	}
	
	
	@Override
	public void disconnected() {
		
		super.disconnected();
		if(inputElement != null)
			inputElement.disable(false);
	}
}