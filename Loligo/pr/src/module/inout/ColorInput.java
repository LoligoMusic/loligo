package module.inout;

import constants.DisplayFlag;
import module.Module;
import save.IOData;
import util.Colors;

public class ColorInput extends AbstractInput implements ColorInputIF {
	
	private int defaultValue = Colors.rgb(200, 20, 20), value = defaultValue; 
	
	
	public ColorInput(String name, Module module) {
		super(name, module);
		
	}
	
	@Override
	public void setColor(int value) {
		
		this.value = value;
		setChanged();
		
	}
	
	@Override
	public int getColorInput() {
		
		if(mediator != null) 
			
			value = ((ColorMediator)mediator).receiveColor();
		
		if(gui != null && gui.checkFlag(DisplayFlag.onDisplay))
			gui.update();
		
		return value;
	}
	
	@Override
	public int getColor() {
	
		return value;
	}
	
	@Override
	public void updateValue() {
		
		getColorInput();
	}
	
	@Override
	public DataType getType() {
		
		return DataType.COLOR;
	}
	

	
	@Override
	public void copyValue(Input s1) {
		
		if(s1 instanceof ColorInput c) {
			
			value = c.value;
		}
		
	}
	
	@Override
	public IOData doSerialize() {
		
		IOData data = super.doSerialize();
		data.value = value;
		return data;
	}

	@Override
	public void doDeserialize(IOData data) {
		
		value = (Integer) data.value;
	}
	
	@Override
	public String toString() {
	
		return super.toString() + "0x" + Integer.toHexString(value);
	}

	@Override
	public ColorInputIF setDefaultColor(int c) {
		defaultValue = c;
		return this;
	}

	@Override
	public int getDefaultColor() {
		
		return defaultValue;
	}
	
}
