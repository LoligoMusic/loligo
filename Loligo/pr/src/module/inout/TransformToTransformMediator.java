package module.inout;

import processing.core.PMatrix2D;

public class TransformToTransformMediator extends AbstractTransformMediator implements TransformMediator {

	
	public TransformToTransformMediator(TransformOutput output) {
		super(output);
	}

	
	@Override
	public PMatrix2D recieveTransform() {
		return ((TransformOutput) output).getMatrix();
	}

}
