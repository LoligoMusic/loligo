package module.inout;

import gui.DisplayGuiSender;
import module.Module;


public class UniversalOutput extends AbstractOutput implements UniversalOutputIF {

	
	private DataType type = DataType.Number;
	private Object value = type.getDefaultValue();
	
	
	
	public UniversalOutput(String name, Module module) {
		
		super(name, module);
	}

	
	@Override
	public void setOutput(Object value, DataType type) {
		
		this.value = value;
		this.type = type;
	}
	
	
	@Override
	public Object getUniversalValue() {
		
		return value;
	}

	@Override
	public DataType getType() {
		
		return DataType.UNIVERSAL;
	}
	
	
	@Override
	public DataType getDynamicType() {
		return type;
	}
	
	@Override
	public DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
		return UniversalInOut.CREATE__INPUT_UNIVERSAL(this, mode);
	}


	@Override
	public Object getValueObject() {
		return value;
	}


	@Override
	public void setValueObject(Object value) {
		this.value = value; 
	}

}
