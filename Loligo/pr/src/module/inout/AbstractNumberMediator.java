package module.inout;

public abstract class AbstractNumberMediator implements InOutMediator {

	protected final NumberOutputIF output;
	protected double value = Double.NaN;
	
	
	public AbstractNumberMediator(final NumberOutputIF output) {
		
		this.output = output;
	}
	
	@Override
	public Output getOutput() {
	
		return output;
	}

	@Override
	public boolean hasChanged() {
	
		return value != output.getValue();
	}

}
