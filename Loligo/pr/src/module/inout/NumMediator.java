package module.inout;

public interface NumMediator extends InOutMediator{
	
	public double receiveNum();
}
