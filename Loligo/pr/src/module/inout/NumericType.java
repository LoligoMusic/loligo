package module.inout;

import java.util.function.DoubleUnaryOperator;
import lombok.RequiredArgsConstructor;
import module.loading.NodeCategory;
import module.loading.NodeEntry;
import patch.LoligoPatch;

public enum NumericType {
	
	//DEZIMAL("Number", v -> v),
	//INTEGER("Integer", Math::floor),
	NUMBER,
	BOOLEAN;
	
	@RequiredArgsConstructor
	public static enum SubType {
		DECIMAL	("Number"	, NUMBER	, v -> v				), 
		INTEGER	("Integer"	, NUMBER	, Math::floor			), 
		NOTE	("Note"		, NUMBER	, v -> v				), 
		TOGGLE	("Toggle"	, BOOLEAN	, NumericType::toBoolD	), 
		PUSH	("Push"		, BOOLEAN	, NumericType::toBoolD	), 
		BANG	("Bang"		, BOOLEAN	, NumericType::toBoolD	);
		
		public final String name;
		public final NumericType type;
		private final DoubleUnaryOperator convertFunction;
		
		public double convert(double v) {
			return convertFunction.applyAsDouble(v);
		}
		
		public NodeEntry getIONodeEntry(LoligoPatch p) {
			return p.getNodeRegistry().findEntryAll(name, NodeCategory.MATH);
		}
	}
	
	

	public static boolean toBool(double v) {
		return v >= .5;
	}
	
	public static double toBoolD(double v) {
		return toBool(v) ? 1d : 0d;
	}
	
}
