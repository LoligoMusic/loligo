package module.inout;

import java.util.function.Consumer;

import gui.DisplayGuiSender;
import module.Module;
import save.IOData;

public class DynamicInput<T> implements Input, DynamicIO {

	private final Input input;
	private final Consumer<T> consumer;
	
	public DynamicInput(String name, Module module, DataType type, Consumer<T> consumer) {
		
		input = type.createInput(name, module);
		this.consumer = consumer;
	}
	
	@Override
	public void dynamicProcess() {
		
		@SuppressWarnings("unchecked")
		T v = (T) input.getValueObject();
		consumer.accept(v);
	}

	public Module getModule() {
		return input.getModule();
	}

	public Output getPartner() {
		return input.getPartner();
	}

	public Object getValueObject() {
		return input.getValueObject();
	}

	public void setPartner(Output output) {
		input.setPartner(output);
	}

	public boolean removePartner() {
		return input.removePartner();
	}

	public boolean changed() {
		return input.changed();
	}

	public void setChanged() {
		input.setChanged();
	}

	public void setValueObject(Object v) {
		input.setValueObject(v);
	}

	public InOutMediator getMediator() {
		return input.getMediator();
	}

	public void updateValue() {
		input.updateValue();
	}

	public void setMediator(InOutMediator m) {
		input.setMediator(m);
	}

	public void copyValue(Input s1) {
		input.copyValue(s1);
	}

	public InOutMediator findMediator(Output o) {
		return input.findMediator(o);
	}

	public DataType getType() {
		return input.getType();
	}

	public IOType getIOType() {
		return input.getIOType();
	}

	public void addInputListener(InputListener lis) {
		input.addInputListener(lis);
	}

	public String getName() {
		return input.getName();
	}

	public void removeInputListener(InputListener lis) {
		input.removeInputListener(lis);
	}

	public InOutGUI createGui() {
		return input.createGui();
	}

	public void doDeserialize(IOData io) {
		input.doDeserialize(io);
	}

	public void resetDefault() {
		input.resetDefault();
	}

	public InOutGUI getGUI() {
		return input.getGUI();
	}

	public DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
		return input.createInputElement(mode);
	}

	public int getNumConnections() {
		return input.getNumConnections();
	}

	public IOData doSerialize() {
		return input.doSerialize();
	}

	@Override
	public boolean isOptional() {
		
		return input.isOptional();
	}

	@Override
	public void setOptional() {
		input.setOptional();
	}

	@Override
	public void setName(String name) {
		input.setName(name);
		
	}
}
