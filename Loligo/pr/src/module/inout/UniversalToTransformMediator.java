package module.inout;

import processing.core.PMatrix2D;

public class UniversalToTransformMediator extends XToUniversalMediator implements TransformMediator {
	
	private DataType type;
	private final PMatrix2D matrix = new PMatrix2D();
	
	public UniversalToTransformMediator(Output out) {
		
		super(out);
	}
	

	@Override
	public PMatrix2D recieveTransform() {
		
		value = out.getUniversalValue();
		type = ((UniversalOutput) out).getDynamicType();
		return uniToTransform(matrix, value, type);
	}
	
	
	public static PMatrix2D uniToTransform(PMatrix2D matrix, Object value, DataType type) {
		if(type == DataType.TRANSFORM) {
			return (PMatrix2D) value;
		}
		
		try {
			double v = type == DataType.Number ? (double) value :
					   type == DataType.COLOR  ? ColorToNumMediator.colorToNum((Integer) value) : 0;
			return NumToTransformMediator.numToTransform(matrix, v);
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
		return matrix;
	}
	

}
