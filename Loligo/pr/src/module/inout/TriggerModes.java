package module.inout;

import java.io.Serializable;

import pr.RootClass;

public class TriggerModes {

	static abstract class TriggerMode implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 8457109390426972104L;
		NumberInputIF slot;
		private final String name;
		
		public TriggerMode(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
		public abstract boolean triggererTest(double marker);
		abstract TriggerMode getClone();
		
		
	}

	public static class Crossing extends TriggerMode {
		private static final long serialVersionUID = -7953136398841283887L;
		private double tempValue, tempWild, wild;
		public boolean left, right = true;
	
		public Crossing(NumberInputIF slot) {
			super("Crossing");
			this.slot = slot;
			tempValue = slot.getValue();
			wild = tempWild =  slot.getWildInput();
		}
		
		private boolean crossToRight(double border) {
			double v = slot.getValue();
			boolean a = Math.floor(wild) > Math.floor(tempWild) && v >= border && v != 1;
			boolean b = wild > tempWild && tempValue < border && v >= border;
			return a || b;
		}
		
		private boolean crossToLeft(double border) {
			double v = slot.getValue();
			boolean a = Math.floor(wild) < Math.floor(tempWild) && v <= border && v != 1;
			boolean b = wild < tempWild && tempValue > border && v <= border;
			return a || b;
		}
		
		@Override public boolean triggererTest(double border) {
			wild = slot.getWildInput();
			boolean b =
			(left  && crossToLeft(border)) || 
			(right && crossToRight(border));
			tempValue = slot.getInput() ;
			tempWild = wild;
			return b;
		}

		@Override TriggerMode getClone() {
			return null;
		}
		@Override
		public String toString() {
			
			return left && right ? "left+right" : left ? "left" : "right";
		}
	}

	static class Change extends TriggerMode {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5477950807973825432L;
		private int frame, f;
		boolean b = false;
		
		public Change() {
			super("Change");
		}
		
		@Override public boolean triggererTest(double d) {
			f = RootClass.mainDm().getFrameCount();
			b = f - frame > 1;
			frame = f;
			return b;
		}
		
		@Override TriggerMode getClone() {
			return new Change();
		}
	}

}
