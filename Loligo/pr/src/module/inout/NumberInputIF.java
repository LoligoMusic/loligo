package module.inout;

public interface NumberInputIF extends Input, NumberInOut{

	public double getWildInput();

	public double getInput();

	public NumberInputIF setDefaultValue(double defaultValue);

	public double getDefaultValue();

	@Override
	default public void resetDefault() {
		
		setValue(getDefaultValue());
	}
}