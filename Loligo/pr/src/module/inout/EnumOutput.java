package module.inout;

import lombok.Getter;
import lombok.Setter;
import module.Module;
import module.inout.IOEnum.IOEnumEntryIF;

public class EnumOutput extends NumberOutput {

	@Getter@Setter
	private IOEnum enumType;
	
	public EnumOutput(String name, Module module, IOEnum type) {
		
		super(name, module, NumericAttribute.INTEGER_POSITIVE);
		
		enumType = type;
	}

	public void setEnumOutput(IOEnumEntryIF e) {
		
		setValue(e.getOrdinal());
	}
	
	@Override
	public DataType getType() {
		
		return DataType.ENUM;
	}
}
