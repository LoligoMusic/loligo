package module.inout;


import gui.DisplayGuiSender;
import module.Module;
import save.IOData;

public interface InOutInterface {

	/**
	 *  @return Value of this IO. Call updateValue first for Input to recieve new value from its Output.
	 */
	public Object getValueObject();
	
	public void setValueObject(Object v);
	
	
	/**
	 *  Makes sure the value of this IO is updated.
	 */
	public void updateValue();
	
	public DataType getType();
	
	public IOType getIOType();

	public abstract String getName();
	
	public abstract void setName(String name);

	public abstract Module getModule();
	
	public abstract InOutGUI createGui();

	public abstract InOutGUI getGUI();
	
	public DisplayGuiSender<?> createInputElement(IOGuiMode mode);

	public int getNumConnections();
	
	public IOData doSerialize();
	
	public boolean isOptional();
	
	public void setOptional();
	
	public static void INPUT_MOD(InOutInterface nio, DisplayGuiSender<?> n) {
		
		if(nio.getIOType() == IOType.IN) {
			
			n.disable(nio.getNumConnections() > 0);
			
		}else {
			
			n.disable(true);
		}
	}
	
	public static void updateModule(Module m) {
		if(m.getDomain() != null && !m.getDomain().getSignalFlow().contains(m))
			m.processIO();
	}

}