package module.inout;

import constants.DisplayFlag;
import lombok.var;
import module.Module;
import processing.core.PMatrix2D;
import save.IOData;

public class TransformInput extends AbstractInput implements TransformInOut{

	private final PMatrix2D matrix = new PMatrix2D(); //scaleX, scaleY, rotation
	
	public TransformInput(String name, Module module) {
		super(name, module);
	}

	@Override
	public void copyValue(Input s1) {
		
		PMatrix2D m = ((TransformInput) s1).getMatrix();
		matrix.set(m);
	}
	
	public PMatrix2D getMatrixInput() {
		if(mediator != null) {
			PMatrix2D m = ((TransformMediator) mediator).recieveTransform();
			matrix.set(m);
		}
		
		if(gui != null && gui.checkFlag(DisplayFlag.onDisplay))
			gui.update();
		
		return matrix;
	}
	
	public void getMatrixInput(PMatrix2D m) {
		getMatrixInput();
		m.set(matrix);
	}
	
	@Override
	public Object getValueObject() {
		return getMatrixInput();
	}
	
	@Override
	public boolean removePartner() {
		var b = super.removePartner();
		matrix.reset();
		return b;
	}
	
	@Override
	public IOData doSerialize() {
		var io = super.doSerialize();
		io.value = matrix.get(new float[6]);
 		return io;
	}
	
	@Override
	public void doDeserialize(IOData io) {
		var ns = (float[]) io.value;
		matrix.set(ns);
	}
	

	@Override
	public void resetDefault() {
		matrix.reset();
	}


	@Override
	public void updateValue() {
		// TODO Auto-generated method stub

	}

	
	@Override
	public DataType getType() {
		return DataType.TRANSFORM;
	}


	@Override
	public void setMatrix(PMatrix2D m) {
		matrix.set(m);
		setChanged();
	}

	@Override
	public PMatrix2D getMatrix() {
		return matrix;
	}

}
