package module.inout;

import java.util.function.Consumer;
import java.util.function.Supplier;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.GuiSender;
import gui.GuiSenderObject;
import gui.Position;
import gui.Slider;
import pr.DisplayObject;
import pr.DisplayObjectIF;
import pr.userinput.UserInput;
import processing.core.PGraphics;

public class MarkerGUI extends DisplayObject implements GuiSender<Double> {
	
	final private DisplayObjectIF slider;
	protected final GuiSenderObject<Double> sender;
	final private int handleY;
	
	public MarkerGUI(Slider slider, Consumer<Double> action, Supplier<Double> updateAction) {
		
		this.slider = slider;
		
		setFlag(DisplayFlag.DRAGABLE, true);
		setFlag(DisplayFlag.fixedToParent, true);
		
		setColor(0xffee1111);
		setWH(8, 8);
		handleY = slider.getHeight() + 2;
		getPosObject().y = handleY;
		slider.addChild(this);
		//setPosObject(new Position.Bounds(getPosObject(), slider.width, 0));
		setPosObject(new Position.Bounds(new Position.PositionOffset(slider, 0, 11), slider.getWidth(), 0));
		sender = new GuiSenderObject<Double>(this, action, updateAction, 0.06);
	}
	
	@Override
	public void render(PGraphics g) {
		super.render(g);
		g.stroke(getColor());
		g.strokeWeight(1);
		g.line(getX(), getY(), getX(), getY() - handleY);
		g.strokeWeight(0);
	}
	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		if(e == InputEvent.DRAG) {
			sender.message((double) (getPosObject().x / slider.getWidth()));
		}
	}
	
	@Override public Double getValue() {
		
		return sender.getValue();
	}

	@Override public void setValue(Double v) {
		
		sender.setValue(v);
		setPos((float)(slider.getWidth() * v), getY());
	}

	@Override
	public void update() {
		
		sender.update();
	}
	
	@Override
	public void disable(boolean b) {
		
	}
}
