package module.inout;

import java.util.ArrayList;
import java.util.List;

import constants.SlotEvent;
import gui.DisplayGuiSender;
import module.Module;


public abstract class AbstractOutput extends InOut implements Output {

	
	private final List<Input> partners;
	
	
	public AbstractOutput(String name, Module module) {
		
		super(name, module);
		
		partners = new ArrayList<Input>();
	}

	public List<Input> getPartners() {
		
		return partners;
	}

	@Override
	public int getNumConnections() {
		
		return partners.size();
	}
	
	@Override
	public void addInput(Input in) {
		
		partners.add(in);
		
		getModule().onIOEventThis(SlotEvent.SLOT_CONNECT, this);
		
		if(partners.size() == 1 && gui != null)
			gui.connected();
		
	}

	@Override
	public boolean removeInput(Input in) {
		
		boolean b = partners.remove(in);
		
		getModule().onIOEventThis(SlotEvent.SLOT_DISCONNECT, this);
		
		if(b && partners.size() == 0 && gui != null)
			gui.disconnected();
		
		return b;
	}
	
	@Override
	public void updateValue() {
		
	}
	
	@Override
	public IOType getIOType() {
		return IOType.OUT;
	}
	
	@Override
	public InOutGUI createGui() {
		
		DisplayGuiSender<?> g = createInputElement(IOGuiMode.NODE);
		g.disable(true);
		
		return gui = new InOutGUI(this, g);
	}
	
	
}
