package module.inout;

import processing.core.PMatrix2D;

public interface TransformMediator extends InOutMediator {

	public PMatrix2D recieveTransform();
}
