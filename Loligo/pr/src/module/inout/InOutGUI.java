 package module.inout;

import constants.DisplayFlag;
import constants.InputEvent;
import gui.DisplayGuiSender;
import gui.selection.GuiType;
import gui.text.TextBox;
import lombok.Getter;
import lombok.Setter;
import pr.DisplayObject;
import pr.userinput.UserInput;
import util.Colors;

public class InOutGUI extends DisplayObject {
	
	public static final int DEFAULT_IO_COLOR = Colors.grey(100);
	
	protected float textOffset = 5;
	private TextBox tb;
 	protected final InOutInterface io;
 	@Getter@Setter
	private boolean hidden = false;
	protected DisplayGuiSender<?> inputElement;
	
	
	public InOutGUI(InOutInterface io, DisplayGuiSender<?> inputElement) {
		
		this.io = io;
		//this.inputElement = inputElement;
		setGuiType(GuiType.INPUT);
		setWH(5, 5);
		setColor(DEFAULT_IO_COLOR);
		setFlag(DisplayFlag.fixedToParent, true);
	}
	
	
	public InOutInterface getInOut() {
		return io;
	}

	
	@Override
	public void mouseEventThis(InputEvent e, UserInput input) {
		
		switch(e) {
		case CLICK 		-> showInputElement();
		case OVER_STOP 	-> removeChild(tb);
		case DRAG_START -> {
			input.getDomain().getDisplayManager().getLineContainer().setLineTarget((Output)io);
		}
		case OVER -> {
			tb = new TextBox(io.getName(), true);
			tb.setFlag(DisplayFlag.fixedToParent, true);
			addChild(tb);
			tb.setPos(textOffset, -10);
		}
		case RELEASED 	-> {
			var lc = input.getDomain().getDisplayManager().getLineContainer();
			io.getModule().getDomain().getModuleManager().connect(io, lc.getLineTarget());
			lc.clearLineTarget();
		}
		default -> {}
		}
	}
	
	
	public void showInputElement() {
		
		inputElement = io.createInputElement(IOGuiMode.NODE);
		
		getDm().getInput().setRemovableTarget(inputElement);
		inputElement.update();
		
		positionInputElement();
	}
	
	public void positionInputElement() {
		if(inputElement != null)
			inputElement.setPos(getX() + 10, getY());
	}
	
	
	public void update() {
		
		if(inputElement != null) {
			inputElement.update();
			positionInputElement();
		}
	}
	
	
	
	
	
	public void connected() {
		
	}
	
	
	public void disconnected() {
		
	}
	
}
