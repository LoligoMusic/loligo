package module.inout;

import java.util.function.Consumer;
import java.util.function.Supplier;

import gui.ColorPicker;
import gui.ColorPreview;
import gui.DisplayGuiSender;

public interface ColorInOut extends InOutInterface {
	
	public void setColor(int value);

	public int getColor();

	@Override
	public default Object getValueObject() {
		
		return getColor();
	}

	@Override
	public default void setValueObject(Object v) {
		
		setColor(((Number) v).intValue());
	}
	
	@Override
	default DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
	
		return CREATE_GUI_COLOR(this, mode);
	}
	
	public static DisplayGuiSender<Integer> CREATE_GUI_COLOR(ColorInOut cio, IOGuiMode mode) {
		
		Consumer<Integer> c = i ->
		{
			cio.setColor(i);
			InOutInterface.updateModule(cio.getModule());
		};
		
		Supplier<Integer> su = cio::getColor;
		
		var s = CREATE_GUI_ELEMENT(c, su, mode);
		
		InOutInterface.INPUT_MOD(cio, s);
		
		return s;
	}

	
	public static DisplayGuiSender<Integer> CREATE_GUI_ELEMENT(Consumer<Integer> c, Supplier<Integer> s, IOGuiMode mode) {
		return mode == IOGuiMode.INSPECTOR ? new ColorPreview(c, s) :
											 new ColorPicker(c, s);
	}
	
}
