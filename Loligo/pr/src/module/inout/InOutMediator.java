package module.inout;

public interface InOutMediator {
	
	public Output getOutput();
	public boolean hasChanged();
}
