package module.inout;

public interface TriggerInputIF extends NumberInputIF{

	public boolean triggered();

	public void setMarkerPos(double d);

	public void setMode(int i);

}