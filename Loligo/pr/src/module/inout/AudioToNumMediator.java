package module.inout;

public class AudioToNumMediator extends AudioMediator implements NumMediator {

	public AudioToNumMediator(AudioOutput audioOutput) {
		super(audioOutput);
	}

	@Override
	public double receiveNum() {
		return getUGen().getValueDouble();
	}

}
