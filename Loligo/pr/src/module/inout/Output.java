package module.inout;

import java.util.List;


public interface Output  extends InOutInterface {

	
	public List<Input> getPartners();
	
	public void addInput(Input in);
	
	public boolean removeInput(Input in);
	
	public Object getUniversalValue();
	
}
