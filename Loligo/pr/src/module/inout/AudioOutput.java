package module.inout;

import java.util.function.Supplier;

import gui.DisplayGuiSender;
import gui.text.NumberInputField;
import lombok.Getter;
import module.Module;
import net.beadsproject.beads.core.UGen;
import pr.EnterFrameListener;

public class AudioOutput extends AbstractOutput{

	@Getter
	private UGen uGen;
	
	public AudioOutput(String name, Module module) {
		super(name, module);
	}

	@Override
	public Object getUniversalValue() {
		return uGen;
	}

	@Override
	public Object getValueObject() {
		return uGen;
	}

	@Override
	public void setValueObject(Object v) {
		
	}
	
	public void setUGen(UGen ug) {
		uGen = ug;
		for(Input in : getPartners())
			in.setChanged();
	}
 
	
	@Override
	public DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
		
		var g = new AutoUpdateNumberInputField(uGen::getValueDouble);
		InOutInterface.INPUT_MOD(this, g);
		return g;
	}
	

	@Override
	public DataType getType() {
		return DataType.AUDIOCTRL;
	}

	
	public static class AutoUpdateNumberInputField extends NumberInputField {

		private final EnterFrameListener u = this::update;
		
		public AutoUpdateNumberInputField(Supplier<Double> updateAction) {
			super(d -> {}, updateAction);
		}
		
		@Override
		public void addedToDisplay() {
			super.addedToDisplay();
			getDm().addEnterFrameListener(u);
		}
		
		@Override
		public void removedFromDisplay() {
			super.removedFromDisplay();
			getDm().removeEnterFrameListener(u);
		}
	}
}
