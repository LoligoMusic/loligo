package module.inout;

public interface InputListener {

	public void onInputChanged(Input in);
	
}
