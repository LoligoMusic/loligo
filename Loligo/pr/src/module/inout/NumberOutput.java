package module.inout;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import module.Module;

public class NumberOutput extends AbstractOutput implements NumberOutputIF {

	private final NumericAttribute attributes;
	private double value = 0;
	
	public NumberOutput(String name, Module module) {
		
		this(name, module, NumericAttribute.NORMALIZED);
	}
	
	public NumberOutput(String name, Module module, NumericAttribute attributes) {
		
		super(name, module);
		this.attributes = attributes;
	}

	@Override
	public double getValue() {
		
		return value;
	}
	
	@Override
	public Object getUniversalValue() {
		
		return value;
	}
	
	@Override
	public void setValue(double value) { //setNumberOutput
		
		this.value = value;
		
		if(gui != null)
			gui.update();
	}

	@Override
	public DataType getType() {
		
		return DataType.Number;
	}

	
	private void writeObject(ObjectOutputStream stream) throws IOException {
		
		stream.defaultWriteObject();
		
		stream.writeDouble(value);
		
	}
	
	
	private void readObject(ObjectInputStream ois)
				throws ClassNotFoundException, IOException {
			
		ois.defaultReadObject();
			
		value = ois.readDouble();
		
		
	}


	@Override
	public NumericAttribute getNumberAttributes() {
		return attributes;
	}
	

}
