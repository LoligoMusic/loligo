package module.inout;

import processing.core.PMatrix2D;

public class AbstractTransformMediator implements InOutMediator {

	protected final TransformOutput output;
	protected final PMatrix2D matrix = new PMatrix2D();
	
	public AbstractTransformMediator(final TransformOutput output) {
		this.output = output;
	}
	
	@Override
	public Output getOutput() {
		return output;
	}

	@Override
	public boolean hasChanged() {
		return !matrix.equals(output.getValueObject());
	}

}
