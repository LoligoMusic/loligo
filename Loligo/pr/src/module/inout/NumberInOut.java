package module.inout;

import java.util.function.Consumer;
import java.util.function.Supplier;

import gui.DisplayGuiSender;

public interface NumberInOut extends InOutInterface {

	public void setValue(double value);

	/**
	 * @return Current value of this Input. Doesn't update value from output.
	 */
	public double getValue();
	
	@Override
	public default Object getValueObject() {
		
		return getValue();
	}
	
	@Override
	public default void setValueObject(Object v) {
		
		setValue(((Number) v).doubleValue()); 
	}
	
	
	public NumericAttribute getNumberAttributes();
	
	@Override
	default DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
	
		return CREATE_INPUT_NUM(this);
	}
	
	
	public static DisplayGuiSender<Double> CREATE_INPUT_NUM(NumberInOut nio) {
		
		Consumer<Double> c = d -> {
			nio.setValue(d);
			InOutInterface.updateModule(nio.getModule());
		};
		Supplier<Double> s = nio::getValue;
		
		var na = nio.getNumberAttributes();
		var gs = NumericAttribute.createGuiByType(na, c, s);
		InOutInterface.INPUT_MOD(nio, gs);
		return gs;
	}
	
}
