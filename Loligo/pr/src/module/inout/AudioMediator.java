package module.inout;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.beadsproject.beads.core.UGen;

@RequiredArgsConstructor
public class AudioMediator implements InOutMediator{

	@Getter
	private final AudioOutput audioOutput;
	
	
	@Override
	public Output getOutput() {
		return audioOutput;
	}
	
	public UGen getUGen() {
		return audioOutput.getUGen();
	}
 
	@Override
	public boolean hasChanged() {
		return true; //TODO implement this maybe?
	}

}
