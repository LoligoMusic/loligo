package module.inout;

public class NumToNumMediator extends AbstractNumberMediator implements NumMediator {

	public NumToNumMediator(NumberOutputIF output) {
		
		super(output);
		
	}

	@Override
	public double receiveNum() {
		
		return value = output.getValue();
	}

}
