package module.inout;

import java.util.function.Consumer;
import java.util.function.Supplier;

import gui.DisplayGuiSender;
import gui.TransformEditor;
import lombok.val;
import pr.DisplayObject;
import processing.core.PMatrix2D;
import util.Colors;

public interface TransformInOut extends InOutInterface {

	/*
	public static float[] newTransformArray() {
		return new float[3];
	}
	
	public static float[] copyValues(float[] src, float[] dst) {
		for (int i = 0; i < src.length; i++) {
			dst[i] = src[i];
		}
		return dst;
	}
	*/
	
	public void setMatrix(PMatrix2D v);
	
	public PMatrix2D getMatrix();
	
	@Override
	public default Object getValueObject() {
		return getMatrix();
	}

	@Override
	public default void setValueObject(Object v) {
		setMatrix((PMatrix2D) v);
	}

	@Override
	default DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
	
		return CREATE_INPUT_TRANSFORM(this, mode);
	}
	
	
	public static DisplayGuiSender<?> CREATE_INPUT_TRANSFORM(TransformInOut nio, IOGuiMode mode) {
		
		Consumer<PMatrix2D> c = d -> {
			nio.setMatrix(d);
			InOutInterface.updateModule(nio.getModule());
		};
		
		Supplier<PMatrix2D> sup = ()-> new PMatrix2D();
		
		DisplayGuiSender<PMatrix2D> ds = CREATE_GUI_OBJECT(c, sup, mode);
		InOutInterface.INPUT_MOD(nio, ds);
		
		return ds;
	}

	public static DisplayGuiSender<PMatrix2D> CREATE_GUI_OBJECT(Consumer<PMatrix2D> c, Supplier<PMatrix2D> sup, IOGuiMode mode) {
		val d = new DisplayObject();
		d.setColor(Colors.grey(60));
		return 
			mode == IOGuiMode.NODE 	 	? new TransformEditor(c, sup) 		  :
			mode == IOGuiMode.INSPECTOR ? new TransformEditor.Preview(c, sup) :
									 	  null;
		
	}
}
