
package module.inout;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import gui.DisplayGuiSender;
import gui.DropDownMenu;
import lombok.Getter;
import module.Module;
import module.inout.IOEnum.IOEnumEntryIF;

public class EnumInput extends NumberInput {

	@Getter
	private IOEnum enumType;
	
	
	public EnumInput(String name, Module module, IOEnum enumType) {
		
		super(name, module, NumericAttribute.DEZIMAL_POSITIVE);
		
		this.enumType = enumType;
		
	}
	

	public IOEnumEntryIF getEnumInput() {
		
		int i = (int) getWildInput();
		
		List<IOEnumEntryIF> vs = enumType.getValues();
		
		return vs.get(i % vs.size());
	}
	
	
	public void setEnumType(IOEnum en) {
		
		this.enumType = en;
	}
	
	
	@Override
	public DisplayGuiSender<?> createInputElement(IOGuiMode mode) {
		
		Consumer<Integer> c = v -> {
			setValue(v);
			InOutInterface.updateModule(getModule());
		};
		Supplier<Integer> u = () -> (int) getValue();
		
		var d = new DropDownMenu(c, u, IOEnum.names(enumType)) {
			@Override
			public void onUpdate() {
				super.onUpdate();
				gui.positionInputElement();
			}
		};
		if(mode == IOGuiMode.NODE) {
			d.enableAutoAdapt();
		}
		
		return d;
	}


	@Override
	public DataType getType() {
		
		return DataType.ENUM;
	}

}
