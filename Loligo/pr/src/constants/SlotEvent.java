package constants;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum SlotEvent {
	SLOT_CONNECT(true),
	SLOT_DISCONNECT(true),
	SLOT_STARTTARGET(false),
	SLOT_STOPTARGET(false);
	
	public final boolean isConnectDisconnect;
	
}
