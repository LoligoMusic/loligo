package constants;

import module.Module;

public interface ModuleEventListener {
	public void moduleEvent(ModuleEvent e, Module m);
}
