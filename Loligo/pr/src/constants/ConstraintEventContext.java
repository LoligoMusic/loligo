package constants;

import gui.paths.PathEvent;
import gui.paths.PathListener;

public class ConstraintEventContext extends AbstractEventManager<PathListener> implements PathListener{
	
	static public final ConstraintEventContext instance = new ConstraintEventContext();
	
	public void eventOn(PathEvent pathEvent) {
		for (PathListener cel : listeners) 
			cel.onPathEvent(pathEvent);
	}

	@Override
	public void onPathEvent(PathEvent e) {
		
		eventOn(e);
	}

	
	
}
