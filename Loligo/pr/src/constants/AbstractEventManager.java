package constants;

import java.util.ArrayList;

public abstract class AbstractEventManager <T> {
	
	protected AbstractEventManager<T> instance;
	protected ArrayList<T> listeners = new ArrayList<T>();
	private ArrayList<T> removedListeners = new ArrayList<T>();
	
	
	public void addListener(T asl) {
		
		if(asl == null)
			System.err.println("Null can't be a listener");
		else {
			
			if(!listeners.contains(asl)) {
				
				listeners.add(asl);
			}
			removedListeners.remove(asl);
		}
			
			
	}
	
	
	public void removeListener(T asl) {
		
		removedListeners.add(asl);
	}

	
	public void updateRemoved() {
		
		if(!removedListeners.isEmpty()) {
			listeners.removeAll(removedListeners);
			removedListeners.clear();
		}
	}
	
	
	@Override
	public String toString() {
		
		return listeners.toString();
	}
}
