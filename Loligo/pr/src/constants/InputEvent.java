package constants;

import static java.util.EnumSet.of;

import java.util.EnumSet;
import java.util.function.Function;

import lombok.RequiredArgsConstructor;

public enum InputEvent {
	DRAG, 
	DRAG_START, 
	DRAG_STOP, 
	DROPPED, 
	DROP_STOP, 
	DROP_OVER,
	DRAG_DROPPED,
	DRAG_OVER,
	DRAG_OVER_STOP,
	PRESSED, 
	RELEASED, 
	MOVE, 
	OVER,
	OVER_STOP,
	OVER_MOVE,
	FOCUS_GAINED,
	FOCUS_LOST,
	CLICK, 
	DOUBLECLICK,
	CLICK_RIGHT, 
	PRESSED_RIGHT, 
	RELEASED_RIGHT, 
	DRAG_START_RIGHT,
	DRAG_STOP_RIGHT,
	DRAG_RIGHT,
	KEY, 
	KEY_RELEASED,
	KEY_AUTOREPEAT,
	SELECT_ADD,
	SELECT_REMOVE, 
	MOUSEWHEEL,
	PRESSED_CENTER,
	RELEASED_CENTER
	;
	
	private static final EnumSet<InputEvent> RELEASE_EVENT 	  = of(RELEASED, DRAG_STOP, DROPPED);
	private static final EnumSet<InputEvent> DRAG_EVENT 	  = of(DRAG, DRAG_START, DRAG_STOP);
	private static final EnumSet<InputEvent> DRAG_EVENT_RIGHT = of(DRAG_RIGHT, DRAG_START_RIGHT, DRAG_STOP_RIGHT);
	private static final EnumSet<InputEvent> DROP_EVENT 	  = of(DROPPED, DROP_STOP, DROP_OVER);
	private static final EnumSet<InputEvent> CLICK_EVENT 	  = of(CLICK, DOUBLECLICK, PRESSED, RELEASED, OVER, OVER_STOP);
	private static final EnumSet<InputEvent> RIGHTCLICK_EVENT = of(CLICK_RIGHT, PRESSED_RIGHT, RELEASED_RIGHT);
	private static final EnumSet<InputEvent> FOCUS_EVENT 	  = of(FOCUS_GAINED, FOCUS_LOST);
	
	public boolean 
		isDragEvent, isDragEventRight,isDropEvent, isClickEvent, isRightClickEvent, 
		isConstraintEvent, isSlotEvent, isAudioStackEvent, isReleaseEvent, 
		isFocusEvent, isSelectionEvent;
	
	static{
		for (InputEvent e : EnumSet.allOf(InputEvent.class)) {
			e.isDragEvent = DRAG_EVENT.contains(e);
			e.isDragEventRight = DRAG_EVENT_RIGHT.contains(e);
			e.isDropEvent = DROP_EVENT.contains(e);
			e.isClickEvent = CLICK_EVENT.contains(e);
			e.isRightClickEvent = RIGHTCLICK_EVENT.contains(e);
			e.isReleaseEvent = RELEASE_EVENT.contains(e);
			e.isSelectionEvent = e == SELECT_ADD || e == SELECT_REMOVE;
			e.isFocusEvent = FOCUS_EVENT.contains(e);
		}
	}

	@RequiredArgsConstructor
	public enum MouseButton {
		RMB(m -> m.RIGHT),
		LMB(m -> m.LEFT);
		
		private final Function<MouseAction, InputEvent> foo;
		
		public InputEvent forAction(MouseAction m) {
			return foo.apply(m);
		}
	}
	
	@RequiredArgsConstructor
	public enum MouseAction {
		
		CLICK		(InputEvent.CLICK		, InputEvent.CLICK_RIGHT		), 
		//DOUBLECLICK (InputEvent.DOUBLECLICK, InputEvent.DOUBLECLICK		),
		PRESSED		(InputEvent.PRESSED		, InputEvent.PRESSED_RIGHT		), 
		RELEASED	(InputEvent.RELEASED	, InputEvent.RELEASED_RIGHT		), 
		DRAG_START  (InputEvent.DRAG_START	, InputEvent.DRAG_START_RIGHT	),
		DRAG_STOP	(InputEvent.DRAG_STOP	, InputEvent.DRAG_STOP_RIGHT	),
		DRAG		(InputEvent.DRAG		, InputEvent.DRAG_RIGHT			);
		
		public final InputEvent LEFT, RIGHT;
	}
	
}
