package constants;

import lombok.var;
import patch.GuiDomain;
import pr.DisplayContainerIF;

public class Timer {
	
	public static Timer createFrameDelay(GuiDomain domain, TimerListener t) {
		var lis = new Listener.EnterFrame(domain);
		return new Timer(lis, 1, 1, t);
	}
	
	public static Timer createFrameDelay(Listener listener, TimerListener t) {
		
		return new Timer(listener, 1, 1, t);
	}
	
	private final Listener eventListener;
	
	final private int frames, repeat;
	final private TimerListener listener;
	protected int count = -1, repeatCount = 1;
	
	
	public Timer(GuiDomain d, int millis) {
		this(d, millis, null);
	}
	
	public Timer(GuiDomain d, int millis, TimerListener tl) {
		this(d, millis, 1, tl);
	}
	
	public Timer(GuiDomain d, int millis, int repeat, TimerListener tl) {
		this(new Listener.EnterFrame(d), millis, repeat, tl);
	}
	
	
	public Timer(Listener eventListener, int millis) {
		this(eventListener, millis, null);
	}
	
	public Timer(Listener eventListener, int millis, TimerListener tl) {
		this(eventListener, millis, 1, tl);
	}
	
	
	public Timer(Listener eventListener, int millis, int repeat, TimerListener tl) {
		
		this.eventListener = eventListener;
		eventListener.setListener(this::trigger);
		
		this.repeat = repeat;
		this.listener = tl;
		frames = (int)(millis * (getDm().getGuiDomain().getPApplet().frameRate / 1000f));
		
		//dm = RootClass.mainDm();
	}
	
	
	public void trigger() {
		count++;
		if(count >= frames) {
			if(repeat < 0 || repeatCount < repeat) {
				repeatCount++;
				count = 0;
			}else{
				eventListener.remove();//dm.removeEnterFrameListener(this);
			}
			timerEvent();
			if(listener != null) 
				listener.timerEvent(this);
		}
	}
	
	public void start() {
		eventListener.add();//addEnterFrameListener(this);
	}
	
	public void pause() {
		eventListener.remove();//dm.removeEnterFrameListener(this);
	}
	
	public void reset() {
		eventListener.remove();//dm.removeEnterFrameListener(this);
		count = -1;
		repeatCount = 1;
	}
	
	public void timerEvent() {}
	
	public int getIntervall() {
		return frames;
	}
	
	public int getTimesRepeated() {
		return repeatCount;
	}
	
	public DisplayContainerIF getDm() {
		return eventListener.getPatch().getDisplayManager();
	}
}
