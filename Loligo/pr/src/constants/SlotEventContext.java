package constants;

import module.inout.Input;
import module.inout.Output;

public class SlotEventContext extends AbstractEventManager<SlotEventListener> {
	
	//static public final SlotEventContext instance = new SlotEventContext();
	
	public void dispatchEvent(SlotEvent e, Input in, Output out) {
		
		updateRemoved();
		
		int len = listeners.size();
		
		for (int i = 0; i < len; i++) {
			
			listeners.get(i).slotEvent(e, in, out);
			
		}
		
		/*
		for (SlotEventListener sle : listeners) 
			sle.slotEvent(e, in, out);
		*/
	}
}
