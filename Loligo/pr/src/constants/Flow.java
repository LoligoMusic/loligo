package constants;

public enum Flow {
	
	SOURCE,
	SINK,
	PERMA,
	READER,
	ROUNDSLOTS, 
	SPREAD,
	ISOLATED, 
	/**
	 * Indicates that the Node is registered in the Domain and hasn't been removed, yet.
	 */
	ALIVE,
	/**
	 * Indicates that the Node isn't working as intended due to an error.
	 */
	ERROR, 
	DYNAMIC_IO,
	/**
	 * Indicates that node does nothing but forward its input values
	 */
	THROUGHPUT,
	/**
	 * Node can be optimized away in Particles and Subpatches without GUI
	 */
	NONESSENTIAL
	
	;
	;
}
