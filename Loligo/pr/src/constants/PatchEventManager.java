package constants;

import constants.PatchEvent.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import patch.LoligoPatch;

public class PatchEventManager extends AbstractEventManager<PatchEventListener> {

	static public final PatchEventManager instance = new PatchEventManager();
	
	@Getter@Setter
	private LoligoPatch focusedPatch;
	
	
	public void dispatchPatchFocusEvent(LoligoPatch p, boolean focus) {
		
		PatchEvent.Type t = null;
		
		if(focus) {
			t = PatchEvent.Type.FOCUS_GAINED;
			focusedPatch = p;
		}else {
			t = PatchEvent.Type.FOCUS_LOST;
			focusedPatch = null;
		}
		
		var e = new PatchEvent(p, t);
		dispatchPatchEvent(e);
	}
	
	public void dispatchPatchEvent(LoligoPatch p, Type t) {
		dispatchPatchEvent(new PatchEvent(p, t));
	}
	
	public void dispatchPatchEvent(PatchEvent e) {
			
		updateRemoved();
		
		for (int i = 0; i < listeners.size(); i++) {
			listeners.get(i).onPatchEvent(e);
		}
	}

	public void dispatchPatchResizeEvent(LoligoPatch patch) {
		dispatchPatchEvent(new PatchEvent(patch, PatchEvent.Type.RESIZED));
	}
	
}
