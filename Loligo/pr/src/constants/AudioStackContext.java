package constants;

import audio.AudioModule;
import audio.AudioStack;


public class AudioStackContext extends AbstractEventManager<AudioStackListener>{
	
	static public final AudioStackContext instance = new AudioStackContext(); 
	
	
	public void dispatchAudioStackEvent(Type e, AudioModule module, AudioModule adjacentModule, AudioStack stack, int index) {
		
		updateRemoved();
		
		AudioStackEvent ase = new AudioStackEvent(e, module, adjacentModule, stack, index);
		
		for (AudioStackListener asl : listeners) 
			asl.audioStackEvent(ase);
	}
	
	
	public record AudioStackEvent(Type event, AudioModule module, AudioModule adjacentModule, AudioStack stack, int index) {}
	
	public enum Type {
		AUDIOSTACK_ADD,
		AUDIOSTACK_REMOVE,
	}

}
