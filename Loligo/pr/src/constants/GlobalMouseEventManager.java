package constants;

import patch.GuiDomain;
import pr.EventSubscriber;
import pr.userinput.UserInput;

public class GlobalMouseEventManager extends AbstractEventManager<EventSubscriber> implements GuiDomainEventListener, EventSubscriber{

	public static final GlobalMouseEventManager instance = new GlobalMouseEventManager();
	
	private GuiDomain currentDomain;
	
	
	public GlobalMouseEventManager() {
		GuiDomainEventManager.instance.addListener(this);
	}
	
	
	@Override
	public void onGuiDomainEvent(GuiDomainEvent e) {
		var d = e.domain();
		switch(e.type()) {
		case FOCUS_GAINED -> addDomain(d);
		case FOCUS_LOST   -> removeDomain(d);
		default -> {}
		}
	}


	private void addDomain(GuiDomain d) {
		currentDomain = d;
		currentDomain.getDisplayManager().getInput().addInputEventListener(this);
	}

	
	private void removeDomain(GuiDomain d) {
		if(currentDomain != null) {
			currentDomain.getDisplayManager().getInput().removeInputEventListener(this);
			currentDomain = null;
		}
	}


	@Override
	public void mouseEvent(InputEvent e, UserInput input) {
		
		updateRemoved();
		
		for (int i = 0; i < listeners.size(); i++) {
			listeners.get(i).mouseEvent(e, input);
		}
		
	}
	
}
