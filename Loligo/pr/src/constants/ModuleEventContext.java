package constants;

import module.Module;


public class ModuleEventContext extends AbstractEventManager<ModuleEventListener> {
	
	//static public final ModuleEventContext instance = new ModuleEventContext(); 
	
	public void dispatchModuleEvent(ModuleEvent e, Module m) {
		updateRemoved();
		for (ModuleEventListener asl : listeners) 
			asl.moduleEvent(e, m);
	}
}
