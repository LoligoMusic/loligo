package constants;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Units {
	
	LENGTH("pixels",50),
	TIME("ms", 1000);
	
	public final String name;
	public final double normFactor;
	public final float normFactorF;
	
	private Units(String name, double f) {
		this.name = name;
		this.normFactor = f;
		this.normFactorF = (float) f;
	}
	
}
