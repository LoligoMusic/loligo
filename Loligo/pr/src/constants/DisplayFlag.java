package constants;

public enum DisplayFlag {

	DRAWABLE, 
	CLICKABLE, 
	DRAGABLE, 
	/**Can have Dragables dropped on**/
	DROPABLE, 
	fixedToParent, 
	/** Object attached to PosConstraints + AudioStacks. */
	lockedPosition, 
	mouseOver, 
	lockX, 
	lockY, 
	snapsToPath, 
	onDisplay,
	/** Auto-subscribe to InputEvents when added to Display */
	Subscriber,
	/** Auto-subscribe to InputEvents when selected */
	SUBSCRIBE_ON_SELECT,
	RECT,
	ROUND,
	selected;
}
