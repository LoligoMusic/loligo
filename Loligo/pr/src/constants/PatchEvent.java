package constants;

import patch.LoligoPatch;


public record PatchEvent(LoligoPatch patch, Type type) {
	
	public static enum Type {
		PATCH_CREATED,
		PATCH_REMOVED,
		FOCUS_GAINED,
		FOCUS_LOST, 
		PATCH_RENAMED,
		KILL_WINDOW, 
		RESIZED;
	}
}
