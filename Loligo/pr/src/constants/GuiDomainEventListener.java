package constants;

public interface GuiDomainEventListener {
	void onGuiDomainEvent(GuiDomainEvent e);
}
