package constants;

import pr.EnterFrameListener;

public class EnterFrameContext extends AbstractEventManager<EnterFrameListener> {

	public void dispatchEnterFrameEvent() {
		
		updateRemoved();
		
		for (int i = 0; i < listeners.size(); i++) {
			listeners.get(i).enterFrame();
		}
	}
}
