package constants;

public interface TimerListener {
	public void timerEvent(Timer t);
}
