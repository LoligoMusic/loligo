package constants;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import patch.Domain;
import patch.GuiDomain;
import pr.EnterFrameListener;
import pr.ExitFrameListener;

@RequiredArgsConstructor
public abstract class Listener {
	
	@Getter
	protected final GuiDomain patch;
	@Setter
	protected Runnable listener;
	
	public abstract void add();
	public abstract void remove();
	
	
	public static class EnterFrame extends Listener implements EnterFrameListener{
		
		public EnterFrame(GuiDomain p) {
			super(p);
		}
		
		@Override
		public void add() {
			patch.getDisplayManager().addEnterFrameListener(this);
		}
		
		@Override
		public void remove() {
			patch.getDisplayManager().removeEnterFrameListener(this);
		}

		@Override
		public void enterFrame() {
			listener.run();
		}
	}
	
	
	public static class ExitFrame extends Listener implements ExitFrameListener{
		
		public ExitFrame(Domain p) {
			super(p);
		}
		
		@Override
		public void add() {
			patch.getDisplayManager().addExitFrameListener(this);
		}
		
		@Override
		public void remove() {
			patch.getDisplayManager().removeExitFrameListener(this);
		}

		@Override
		public void exitFrame() {
			listener.run();
		}
	}
	
	
	
}
