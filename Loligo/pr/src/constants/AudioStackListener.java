package constants;

import constants.AudioStackContext.AudioStackEvent;

public interface AudioStackListener {
	public void audioStackEvent(AudioStackEvent ase);
}
