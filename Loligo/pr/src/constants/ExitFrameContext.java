package constants;

import java.util.Iterator;

import pr.ExitFrameListener;

public class ExitFrameContext extends AbstractEventManager<ExitFrameListener> {

	public void dispatchExitFrameEvent() {
		
		updateRemoved();
		
		for (Iterator<ExitFrameListener> iterator = listeners.iterator(); iterator.hasNext();) {
			iterator.next().exitFrame();
		}
	}
}
