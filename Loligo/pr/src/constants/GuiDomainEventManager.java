package constants;

import patch.GuiDomain;

public class GuiDomainEventManager extends AbstractEventManager<GuiDomainEventListener> implements PatchEventListener{
	
	public final static GuiDomainEventManager instance = new GuiDomainEventManager();
	
	public GuiDomainEventManager() {
		PatchEventManager.instance.addListener(this);
	}
	
	
	public void dispatchGuiDomainEvent(GuiDomain dom, GuiDomainEvent.Type type) {
		var e = new GuiDomainEvent(dom, type);
		dispatchGuiDomainEvent(e);
	}
	
	
	public void dispatchGuiDomainEvent(GuiDomainEvent e) {
		
		updateRemoved(); 
		
		for (int i = 0; i < listeners.size(); i++) {
			listeners.get(i).onGuiDomainEvent(e);
		}
	}


	@Override
	public void onPatchEvent(PatchEvent e) {
		
		var p = e.patch();
		
		switch(e.type()) {
		case FOCUS_GAINED  -> dispatchGuiDomainEvent(p, GuiDomainEvent.Type.FOCUS_GAINED); 
		case FOCUS_LOST	   -> dispatchGuiDomainEvent(p, GuiDomainEvent.Type.FOCUS_LOST); 
		case PATCH_CREATED -> dispatchGuiDomainEvent(p, GuiDomainEvent.Type.CREATED); 
		case PATCH_REMOVED -> dispatchGuiDomainEvent(p, GuiDomainEvent.Type.REMOVED);
		default 		   -> {}
		};
		
	}
	
}
