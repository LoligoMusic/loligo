package constants;

public class ExceptionListenerContext extends AbstractEventManager<ExceptionListenerContext.Subscriber> {

	public interface Subscriber {
		boolean onException(Throwable e);
	}
	
	
	public boolean eventOn(Throwable e) {
		
		boolean r = false;
		
		for (int i = 0; i < listeners.size(); i++) {
			Subscriber s = listeners.get(i);
			if(s.onException(e))
				r = true;
		}
		return r;
	}
	
}
