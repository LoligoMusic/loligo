package constants;

import constraints.FollowerObject;
import constraints.LoligoPosConstraint;

public interface ConstraintEventListener {
	public void constraintEvent(InputEvent e, LoligoPosConstraint constraint, FollowerObject follower);
}
