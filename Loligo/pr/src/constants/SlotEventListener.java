package constants;

import module.inout.Input;
import module.inout.Output;

public interface SlotEventListener {
	public void slotEvent(SlotEvent e, Input in, Output out);
}
