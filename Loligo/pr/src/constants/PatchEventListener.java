package constants;

public interface PatchEventListener {
	void onPatchEvent(PatchEvent e);
}
