package constants;

import patch.GuiDomain;

public record GuiDomainEvent(GuiDomain domain, Type type) {
	
	public enum Type {
		CREATED,
		REMOVED,
		FOCUS_GAINED,
		FOCUS_LOST, 
		KILL_WINDOW, 
		RESIZED;
	}
}