package constants;

import pr.EventSubscriber;
import pr.userinput.UserInput;

public class MouseEventContext extends AbstractEventManager<EventSubscriber> {

	private InputEvent currentEvent;
	
	public void eventOn(InputEvent e, UserInput input) {
		
		currentEvent = e;
		
		for (int i = 0; i < listeners.size(); i++) {
			
			EventSubscriber es = listeners.get(i);
			es.mouseEvent(e, input);
		}
	}
	
	public InputEvent getCurrentEvent() {
		
		return currentEvent;
	}
}
