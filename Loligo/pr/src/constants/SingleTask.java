package constants;

import patch.GuiDomain;
import pr.ExitFrameListener;

public record SingleTask(GuiDomain domain, ExitFrameListener task) implements ExitFrameListener {

	public static void run(GuiDomain domain, ExitFrameListener task) {
		new SingleTask(domain, task).start();
	}
	
	public void start() {
		
		domain.getDisplayManager().addExitFrameListener(this);
	}
	
	@Override
	public void exitFrame() {
		
		task.exitFrame();
		domain.getDisplayManager().removeExitFrameListener(this);
	}

}
