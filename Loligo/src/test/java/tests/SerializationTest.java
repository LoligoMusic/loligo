package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import module.Module;
import module.ModuleManager;
import module.Modules;
import module.loading.AbstractNodeRegistry;
import module.loading.NodeEntry;
import module.loading.NodeRegistry;
import patch.Domain;
import patch.Loligo;
import patch.LoligoPatch;

class SerializationTest {

	private final AbstractNodeRegistry<?> registry = NodeRegistry.DEFAULT_NODEREGISTRY;
	
	private LoligoPatch domain;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		domain = new LoligoPatch();
		domain.setProc(new MockPApplet());
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testDoSerialize() {
		
		Collection<? extends NodeEntry> col = domain.getNodeRegistry().loadAllEntries();
		
		for(NodeEntry ne : col) {
			Module m = ne.createInstance();
			domain.addModule(m);
		}
		
	}

	@Test
	void testDoDeserialize() {
		
		
	}

}
