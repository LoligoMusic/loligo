package tests;

import java.net.MalformedURLException;
import java.net.URL;

public class Main {

	public static void main(String[] args) throws MalformedURLException {
		
		var u = new URL("jar:file:/D:\\git\\loligo\\Loligo\\src\\test\\java\\tests\\PApplet.jar!/tests/Papplet.class");
		var c = new MockClassloader("Mocker", new URL[] {u}, Thread.currentThread().getContextClassLoader());
		
		Thread.currentThread().setContextClassLoader(c);
		
		MockClassloader.main(null);
	}
}
