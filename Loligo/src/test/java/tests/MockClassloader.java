package tests;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import processing.core.PApplet;

public class MockClassloader extends URLClassLoader{

	public static void main(String[] args) {
			
			System.out.println("System: " + MockClassloader.getSystemClassLoader());
			
			var sys = MockClassloader.getSystemClassLoader();
		
			System.out.println("Context: " + Thread.currentThread().getContextClassLoader());
			
			PApplet p = new PApplet();
			var c = p.getClass().getClassLoader();
			System.out.println(c);
			p.alpha(1);
	}
	
	static URL[] urls() {
		URL u = null;
		try {
			u = new URL("jar:file:/D:\\git\\loligo\\Loligo\\src\\test\\java\\tests\\PApplet.jar!/tests/Papplet.class");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return new URL[] {u};
	}
	
	public MockClassloader(ClassLoader parent) {
		this("MockClassloader", urls(), parent);
	}
	
	public MockClassloader(String name, URL[] urls, ClassLoader parent) {
		super(name, urls, parent);
		
	}

	
	@Override
	protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		System.out.println("Loading class : " + name);
		if(name.equals("processing.core.PApplet")) {
			Class<?> c = super.loadClass("tests.Papplet", resolve);
			return c;
		}
		
		return super.loadClass(name, resolve);
	}
	
	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		System.out.println("Finding class : " + name);
		return super.findClass(name);
	}
	
	
	@SuppressWarnings("unused")
	private void appendToClassPathForInstrumentation(String s) {
		
	}
	
}
